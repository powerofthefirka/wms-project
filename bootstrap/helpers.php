<?php

use Carbon\Carbon;
use App\Models\UserSetting;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

if (!function_exists('dot')) {
    function dot($key)
    {
        $key = str_replace(['[', ']'], ['.', ''], $key);

        return $key;
    }
}

if (!function_exists('user_settings')) {
    function user_settings($key, $default = null)
    {
        $cacheKey = 'user_setting_' . \auth()->user()->id . '_' . $key;
        $cachedSetting = Cache::get($cacheKey);

        if (!$cachedSetting && !empty($default)) {
            $cachedSettingDb = UserSetting::where('user_id', auth()->user()->id)
                ->where('key', $key)->first();

            if (!$cachedSettingDb) {
                UserSetting::saveSettings([$key => $default]);
                $cachedSetting = $default;
            } else {
                $cachedSetting = $cachedSettingDb->value;
                Cache::put($cacheKey, $cachedSettingDb->value);
            }
        }

        return $cachedSetting;
    }
}

if (!function_exists('get_timezone')) {
    function  get_timezone()
    {
        $timezone = user_settings('timezone', env('DEFAULT_TIMEZONE'));

        return $timezone;
    }
}

if (!function_exists('get_date_format')) {
    function get_date_format()
    {
        $dateFormat = user_settings('date_format', env('DEFAULT_DATE_FORMAT'));

        return $dateFormat;
    }
}

if (!function_exists('get_hidden_columns')) {
    function get_hidden_columns($key)
    {
        $hiddenColumns = user_settings($key, '[]');

        return $hiddenColumns;
    }
}

if (!function_exists('localized_date')) {
    function localized_date($date)
    {
        $dateFormat = get_date_format();
        $timezone = get_timezone();

        return Carbon::parse($date)->timezone($timezone)->format($dateFormat);
    }
}

if (!function_exists('localized_date_time')) {
    function localized_date_time($date)
    {
        $dateFormat = get_date_format();
        $timezone = get_timezone();

        return Carbon::parse($date)->timezone($timezone)->format($dateFormat . ' H:i:s');
    }
}

if (!function_exists('dimension_unit')) {
    function dimension_unit($customer = null)
    {
        if (empty($customer))
            return null;

        return $customer->dimensions_unit;
    }
}

if (!function_exists('with_dimension_unit')) {
    function with_dimension_unit($customer, $number = null)
    {
        if (empty($number))
            return null;

        return $number . ' ' . dimension_unit($customer);
    }
}

if (!function_exists('weight_unit')) {
    function weight_unit($customer = null)
    {
        if (empty($customer))
            return null;

        return $customer->weight_unit;
    }
}

if (!function_exists('with_weight_unit')) {
    function with_weight_unit($customer, $number = null)
    {
        if (empty($number))
            return null;

        return $number . ' ' . weight_unit($customer);
    }
}

if (!function_exists('currency')) {
    function currency($customer = null)
    {
        if (empty($customer))
            return null;

        return $customer->currency ? $customer->currency->code : $customer->currency_code;
    }
}

if (!function_exists('with_currency')) {
    function with_currency($customer, $number = null)
    {
        if (empty($number))
            return null;

        return $number . ' ' . currency($customer);
    }
}

if (!function_exists('label_format')) {
    function label_format($value = null)
    {
        if ($value)
            return ' (' . $value . ') ';
    }
}

if (!function_exists('current_page')) {
    function current_page($link = null, $parent = false)
    {
        $routes = [
            'home' => [
                route('home')
            ],
            'orders' => [
                route('orders.index'),
                route('orders.create'),
                route('shipments.index'),
                route('shipping_carriers.index'),
                route('shipping_methods.index')
            ],
            'products' => [
                route('products.index'),
                route('products.create')
            ],
            'returns' => [
                route('returns.index'),
                route('returns.create')
            ],
            'purchase_orders' => [
                route('purchase_orders.index'),
                route('purchase_orders.create'),
                route('vendors.index')
            ],
            'customers' => [
                route('customers.index'),
                route('customers.create'),
                route('users.index'),
                route('users.create')
            ],
            'billings' => [
                route('billings.customers'),
                route('billings.bills'),
                route('billings.billing_profiles'),
                route('billings.product_profiles'),
                route('billings.reconcile'),
                route('billings.exports'),
            ],
            'domains' => [
                route('three_pls.index'),
                route('three_pls.create'),
                route('pricing_plans.index'),
            ],
            'warehouses' => [
                route('warehouses.index')
            ],
            'locations' => [
                route('locations.index')
            ]
        ];

        $current = Request::url();
        $class = '';

        if ($parent) {
            foreach ($routes as $key => $route) {
                if ($key === $parent && in_array($current, $route)) {
                    $class = 'active';
                }
            }
        }

        if ($current === $link) {
            $class = 'active';
        }

        return $class;
    }
}

if (!function_exists('site_title')) {
    function site_title()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        if ($domain && $domain->title) {
            return $domain->title;
        }

        return config('app.name');
    }
}

if (!function_exists('site_favicon')) {
    function site_favicon()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        if ($domain && $domain->favicon) {
            return $domain->favicon->source;
        }

        return asset('argon') . '/img/brand/favicon.png';
    }
}

if (!function_exists('login_logo')) {
    function login_logo()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        if ($domain && $domain->logo) {
            return $domain->logo->source;
        }

        return asset('argon') . '/img/brand/logo.png';
    }
}

if (!function_exists('get_domain')) {
    function get_domain()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        return  $domain->domain ?? config('app.url');
    }
}

if (!function_exists('sidebar_logo')) {
    function sidebar_logo()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        if ($domain && $domain->logo) {
            return $domain->logo->source;
        }

        return asset('argon') . '/img/brand/yourlogo.png';
    }
}

if (!function_exists('primary_color')) {
    function primary_color()
    {
        $domain = \App\Models\Domain::where('domain', app()->request->getHost())->first();

        if ($domain && $domain->primary_color) {
            return $domain->primary_color;
        }

        return env('DEFAULT_COLOR_PRIMARY');
    }
}

if (!function_exists('route_method')) {
    function route_method()
    {
        if (empty(Route::current())) {
            return '';
        }

        return Route::current()->methods()[0] ?? '';
    }
}

if (!function_exists('messaging_script')) {
    function messaging_script()
    {
        if (!auth()->user() || auth()->user()->isAdmin() || empty(session('customer_id'))) {
            return "";
        }

        return app()->messengerChat->render(
            auth()->user()->customers->find(session('customer_id')),
            auth()->user()
        );
    }
}

if (!function_exists('titles_and_fields')) {
    function titles_and_fields($data = [], $columns)
    {
        if (empty($data)) {
            return [Arr::pluck($columns, 'column'), Arr::pluck($columns, 'title')];
        }

        return [array_keys($data), array_values($data)];
    }
}

if (!function_exists('utc_date_time')) {
    function utc_date_time($date)
    {
        $timezone = get_timezone();

        return Carbon::parse($date, $timezone)->timezone('UTC')->format('Y-m-d H:i:s');
    }
}

if (!function_exists('weight_unit_conversion'))
{
    function weight_unit_conversion($value, $defaultUnit = '', $requestedUnit = '')
    {
        $value = (float) $value;

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit']) {
            return $value / 16;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit']) {
            return $value / 0.45359237;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit']) {
            return $value * 0.00220462;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit']) {
            return $value * 453.59237;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit']) {
            return $value / 0.0022046;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit']) {
            return $value * 1000;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit']) {
            return $value * 16;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit']) {
            return $value / 0.02834952;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit']) {
            return $value / 0.03527396195;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['lb']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit']) {
            return $value * 0.45359237;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['oz']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit']) {
            return $value * 0.02834952;
        }

        if ($defaultUnit == \App\Models\Customer::WEIGHT_UNITS['g']['unit'] && $requestedUnit == \App\Models\Customer::WEIGHT_UNITS['kg']['unit']) {
            return $value / 1000;
        }

        return $value;
    }
}

if (!function_exists('dimensions_unit_conversion'))
{
    function dimensions_unit_conversion($value, $defaultUnit = '', $requestedUnit = '', $cubic = false)
    {
        $value = (float) $value;

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit']) {
            return  $cubic ? ($value * 16.387064) : ($value * 2.54);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit']) {
            return  $cubic ? ($value / 16.387064) : ($value / 2.54);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit']) {
            return  $cubic ? ($value * 28316.846592) : ($value * 30.48);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit']) {
            return  $cubic ? ($value / 28316.846592) : ($value / 30.48);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit']) {
            return  $cubic ? ($value * 1000000) : ($value * 100);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['cm']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit']) {
            return  $cubic ? ($value / 1000000) : ($value / 100);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit']) {
            return  $cubic ? ($value * 1728) : ($value * 12);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit']) {
            return  $cubic ? ($value / 1728) : ($value / 12);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit']) {
            return  $cubic ? ($value * 61023.744094732) : ($value * 39.370079);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['inch']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit']) {
            return  $cubic ? ($value / 61023.744094732) : ($value / 39.370079);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit']) {
            return  $cubic ? ($value * 35.3146667215) : ($value * 3.280840);
        }

        if ($defaultUnit == \App\Models\Customer::DIMENSIONS_UNITS['feet']['unit'] && $requestedUnit == \App\Models\Customer::DIMENSIONS_UNITS['m']['unit']) {
            return  $cubic ? ($value / 35.3146667215) : ($value / 3.280840);
        }

        return $value;
    }
}

if (!function_exists('getConvertedProductWeight'))
{
    function get_converted_product_weight($productWeight)
    {
        $weightUnit = explode(' ', $productWeight);
        $weightUnit = end($weightUnit);

        return weight_unit_conversion((float) $productWeight, $weightUnit, env('DEFAULT_WEIGHT_UNIT'));
    }
}

if (!function_exists('userHasEmailNotification')) {
    function userHasEmailNotification($key)
    {
        return in_array($key, json_decode(auth()->user()->email_notifications));
    }
}

if (!function_exists('hasEmailNotificationEnabled')) {
    function hasEmailNotificationEnabled($user, $key)
    {
        return in_array($key, json_decode($user->email_notifications));
    }
}
