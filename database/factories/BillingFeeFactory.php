<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BillingFee;
use Faker\Generator as Faker;

$factory->define(BillingFee::class, function (Faker $faker) {
    return [
        'billing_profile_id' => 1,
        'name' => $faker->unique()->name
    ];
});
