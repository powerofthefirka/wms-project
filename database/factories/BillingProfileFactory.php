<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BillingProfile;
use Faker\Generator as Faker;

$factory->define(BillingProfile::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'monthly_cost' => $faker->randomNumber(4),
        'per_user_cost' => $faker->randomNumber(4),
        'per_purchase_order_received_cost' => $faker->randomNumber(4),
        'per_product_cost' => $faker->randomNumber(4),
        'per_shipment_cost' => $faker->randomNumber(4),
        'per_return_cost' => $faker->randomNumber(4)
    ];
});
