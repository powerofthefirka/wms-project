<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PurchaseOrderItem;
use Faker\Generator as Faker;

$factory->define(PurchaseOrderItem::class, function (Faker $faker) {
    return [
        'purchase_order_id' => 1,
        'product_id' => 1,
        'quantity' => $faker->randomNumber(2),
        'quantity_received' => $faker->randomNumber(2),
        'sku' => $faker->randomNumber(5),
        'name' => $faker->word,
        'price' => $faker->randomNumber(4),
        'customs_price' => $faker->randomNumber(4),
        'weight' => $faker->randomNumber(2),
        'width' => $faker->randomNumber(2),
        'height' => $faker->randomNumber(2),
        'length' => $faker->randomNumber(2),
        'country_of_origin' => $faker->country,
        'hs_code' => $faker->randomNumber(8),
        'barcode' => $faker->randomNumber(8),
        'replacement_value' => $faker->randomNumber(8),
        'product_type' => 0
    ];
});
