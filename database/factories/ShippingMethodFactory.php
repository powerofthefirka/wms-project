<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ShippingMethod;
use Faker\Generator as Faker;

$factory->define(ShippingMethod::class, function (Faker $faker) {
    return [
        'shipping_carrier_id' => 1,
        'name' => $faker->name(),
        'title' => $faker->name(),
        'cost' => $faker->randomNumber(2),
        'wms_id' => $faker->randomNumber(2),
        'is_visible' => $faker->numberBetween(0, 1)
    ];
});
