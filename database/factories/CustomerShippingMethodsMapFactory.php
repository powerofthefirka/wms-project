<?php

use App\Models\CustomerShippingMethodsMap;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CustomerShippingMethodsMap::class, function (Faker $faker) {
    return [
        'shipping_method_pattern' => $faker->text,
        'shipping_service' => $faker->word,
        'shipping_service_carrier_id' => $faker->numberBetween(1, 10),
        'shipping_service_method_code' => $faker->word,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
