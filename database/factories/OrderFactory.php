<?php

use App\Models\ContactInformation;
use App\Models\Customer;
use App\Models\Order;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'customer_id' => 1,
        'shipping_contact_information_id' => null,
        'billing_contact_information_id' => null,
        'number' => $faker->unique()->randomNumber(7),
        'ordered_at' => now(),
        'required_shipping_date_at' => now()->addDays(14),
        'notes' => '',
        'priority' => 0,
        'shipping_price' => $faker->randomNumber(2),
        'tax' => $faker->randomNumber(2),
        'discount' => $faker->randomNumber(2),
        'shipment_type' => 0,
        'shipping_carrier_id' => 1,
        'shipping_method_id' => 1,
        'fraud_hold' => $faker->numberBetween(0, 1),
        'address_hold' => $faker->numberBetween(0, 1),
        'payment_hold' => $faker->numberBetween(0, 1),
        'operator_hold' => $faker->numberBetween(0, 1),
        'subtotal' => $faker->randomNumber(2),
        'total' => $faker->randomNumber(2),
        'is_cancelled' => 0,
        'status' => 'pending',
        'item_count' => 0,
        'shipping_carrier_title' => '',
        'shipping_method_title' => '',
        'shipping_lat' => 0,
        'shipping_lng' => 0
    ];
});
