<?php

use App\Models\Location;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'pickable' => $faker->numberBetween(0, 1),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
