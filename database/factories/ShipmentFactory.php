<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Shipment;
use Faker\Generator as Faker;

$factory->define(Shipment::class, function (Faker $faker) {
    return [
        'order_id' => 1,
        'number' => $faker->unique()->randomNumber(7),
        'tracking_code' => $faker->unique()->randomNumber(8),
        'tracking_url' => '',
        'distinct_items' => 0,
        'quantity_shipped' => 0,
        'line_item_total' => 0,
        'shipped_at' => null
    ];
});
