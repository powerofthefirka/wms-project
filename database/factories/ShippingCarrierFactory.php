<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ShippingCarrier;
use Faker\Generator as Faker;

$factory->define(ShippingCarrier::class, function (Faker $faker) {
    return [
        'customer_id' => 1,
        'name' => $faker->name(),
        'wms_id' => $faker->randomNumber(2),
        'tracking_url' => ''
    ];
});
