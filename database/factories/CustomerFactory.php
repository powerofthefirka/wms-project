<?php

use App\Models\ContactInformation;
use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now()
    ];
});
