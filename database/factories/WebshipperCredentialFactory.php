<?php

use App\Models\WebshipperCredential;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(WebshipperCredential::class, function (Faker $faker) {
    return [
        'api_base_url' => 'https://automagical-aps.api.webshipper.io/v2',
        'api_key' => '19627a6d283d9645e236ed467f1e16ff2042297844e4839a2920a58609a88e23',
        'created_at' => now(),
        'updated_at' => now()
    ];
});
