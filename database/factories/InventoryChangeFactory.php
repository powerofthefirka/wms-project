<?php

use App\Models\InventoryChange;
use Faker\Generator as Faker;

$factory->define(InventoryChange::class, function (Faker $faker) {
    $quantity = $faker->randomNumber(2);

    return [
        'location_id' => 1,
        'product_id' => 1,
        'previous_on_hand' => $quantity,
        'change_in_on_hand' => $faker->numberBetween(0, $quantity),
        'changed_at' => now()
    ];
});
