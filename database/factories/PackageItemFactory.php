<?php

use App\Models\PackageItem;
use Faker\Generator as Faker;

$factory->define(PackageItem::class, function (Faker $faker) {
    return [
        'package_id' => 1,
        'order_item_id' => 1,
        'quantity' => $faker->randomNumber(2)
    ];
});
