<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Package;
use Faker\Generator as Faker;

$factory->define(Package::class, function (Faker $faker) {
    return [
        'shipment_id' => 1,
        'shipping_box_id' => 1,
        'tracking_number' => $faker->unique()->randomNumber(8),
        'tracking_url' => '',
        'shipping_carrier_id' => null,
        'shipping_method_id' => null,
        'price' => $faker->randomNumber(4),
    ];
});
