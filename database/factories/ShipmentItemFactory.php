<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ShipmentItem;
use Faker\Generator as Faker;

$factory->define(ShipmentItem::class, function (Faker $faker) {
    return [
        'shipment_id' => 1,
        'order_item_id' => 1,
        'quantity' => $faker->randomNumber(2)
    ];
});
