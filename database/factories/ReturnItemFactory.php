<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ReturnItem;
use Faker\Generator as Faker;

$factory->define(ReturnItem::class, function (Faker $faker) {
    $quantity = $faker->randomNumber(2);

    return [
        'return_id' => 1,
        'order_item_id' => 1,
        'quantity' => $quantity,
        'quantity_received' => $faker->numberBetween(0, $quantity)
    ];
});
