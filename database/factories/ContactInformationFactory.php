<?php

use App\Models\ContactInformation;
use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(ContactInformation::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'company_name' => $faker->company,
        'address' => $faker->address,
        'address2' => '',
        'zip' => $faker->postcode,
        'city' => $faker->city,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'country_id' => Country::first()->id ?? null,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
