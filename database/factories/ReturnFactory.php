<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Return_;
use Faker\Generator as Faker;

$factory->define(Return_::class, function (Faker $faker) {
    return [
        'order_id' => 1,
        'number' => $faker->unique()->randomNumber(7),
        'requested_at' => now(),
        'status' => '',
        'reason' => '',
        'shipping_carrier_id' => null,
        'shipping_method_id' => null,
        'shipping_carrier_title' => '',
        'shipping_method_title' => '',
        'create_label' => $faker->numberBetween(0, 1)
    ];
});
