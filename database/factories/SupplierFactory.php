<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'customer_id' => 1,
        'currency' => $faker->currencyCode,
        'internal_note' => '',
        'default_po_note' => ''
    ];
});
