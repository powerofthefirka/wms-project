<?php

use App\Models\Customer;
use App\Models\Product;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    $quantityOnHand = $faker->randomNumber(3);
    $quantityAllocated = $faker->numberBetween(0, $quantityOnHand);
    $quantityAvailable = $quantityOnHand - $quantityAllocated;
    $quantityBackordered = $quantityAvailable > 0 ? 0 : $faker->randomNumber(1);

    return [
        'customer_id' => 1,
        'sku' => $faker->randomNumber(5),
        'name' => $faker->word,
        'price' => $faker->randomNumber(4),
        'customs_price' => $faker->randomNumber(4),
        'weight' => $faker->randomNumber(2),
        'width' => $faker->randomNumber(2),
        'height' => $faker->randomNumber(2),
        'length' => $faker->randomNumber(2),
        'country_of_origin' => $faker->country,
        'barcode' => $faker->randomNumber(8),
        'hs_code' => $faker->randomNumber(8),
        'replacement_value' => $faker->randomNumber(8),
        'notes' => $faker->text,
        'quantity_on_hand' => $quantityOnHand,
        'quantity_allocated' => $quantityAllocated,
        'quantity_available' => $quantityAvailable,
        'quantity_backordered' => $quantityBackordered,
        'created_at' => now(),
        'updated_at' => now(),
        'product_type' => 0,
        'is_kit' => $faker->numberBetween(0, 1),
        'reorder_amount' => $faker->randomNumber(2)
    ];
});
