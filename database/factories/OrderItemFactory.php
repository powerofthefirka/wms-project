<?php

use App\Models\Order;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(App\Models\OrderItem::class, function (Faker $faker) {
    $quantity = $faker->randomNumber(2);

    return [
        'order_id' => 1,
        'product_id' => 1,
        'quantity' => $quantity,
        'quantity_shipped' => $faker->numberBetween(0, $quantity),
        'quantity_backordered' => 0,
        'sku' => '',
        'name' => '',
        'price' => '',
        'customs_price' => '',
        'weight' => '',
        'width' => '',
        'height' => '',
        'length' => '',
        'country_of_origin' => '',
        'barcode' => '',
        'replacement_value' => '',
        'product_type' => ''
    ];
});
