<?php

use App\Models\CustomerShippingCarriersMap;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CustomerShippingCarriersMap::class, function (Faker $faker) {
    return [
        'shipping_carrier_pattern' => $faker->text,
        'shipping_service' => $faker->word,
        'shipping_service_carrier_id' => $faker->numberBetween(1, 10),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
