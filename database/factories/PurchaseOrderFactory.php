<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PurchaseOrder;
use Faker\Generator as Faker;

$factory->define(PurchaseOrder::class, function (Faker $faker) {
    return [
        'customer_id' => 1,
        'warehouse_id' => 1,
        'supplier_id' => 1,
        'number' => $faker->randomNumber(6),
        'status' => 'pending',
        'ordered_at' => $faker->dateTime,
        'expected_at' => $faker->dateTime,
        'delivered_at' => $faker->dateTime,
        'notes' => '',
        'priority' => 0,
        'shipping_carrier_id' => null,
        'shipping_method_id' => null,
        'shipping_price' => 0,
        'shipping_carrier_title' => '',
        'shipping_method_title' => '',
        'is_cancelled' => 0,
        'tracking_number' => $faker->unique()->randomNumber(8),
        'tracking_url' => ''
    ];
});
