<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ThreePl;
use Faker\Generator as Faker;

$factory->define(ThreePl::class, function (Faker $faker) {
    return [
        'created_at' => now(),
        'updated_at' => now()
    ];
});
