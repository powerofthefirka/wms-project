<?php

use App\Models\Webhook;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Webhook::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'url' => $faker->url,
        'secret_key' => $faker->word,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
