<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\UserRole;
use App\Models\ContactInformation;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(User::class)->create([
            'id' => 1,
            'email' => 'admin@3pld.com',
            'user_role_id' => UserRole::ROLE_ADMINISTRATOR,
        ]);
        $this->createContactInformation($admin);

        $creator = factory(User::class)->create([
            'id' => 2,
            'email' => 'creator@3pld.com',
            'user_role_id' => UserRole::ROLE_MEMBER,
        ]);
        $this->createContactInformation($creator);

        $member = factory(User::class)->create([
            'id' => 3,
            'email' => 'member@3pld.com',
            'user_role_id' => UserRole::ROLE_MEMBER,
        ]);
        $this->createContactInformation($member);
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}
