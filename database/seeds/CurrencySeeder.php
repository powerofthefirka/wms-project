<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCurrency('Euro', 'EUR');
        $this->createCurrency('United States Dollar', 'USD');
        $this->createCurrency('British pound sterling', 'GPB');
    }

    private function createCurrency($name, $code)
    {
        \App\Currency::create([
            'name' => $name,
            'code' => $code
        ]);
    }
}
