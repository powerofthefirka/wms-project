<?php

use App\Models\User;
use App\Models\Image;
use App\Models\Order;
use App\Models\Country;
use App\Models\Package;
use App\Models\Product;
use App\Models\Return_;
use App\Models\ThreePl;
use App\Models\Customer;
use App\Models\Shipment;
use App\Models\Supplier;
use App\Models\OrderItem;
use App\Models\Warehouse;
use App\Models\BillingFee;
use App\Models\ReturnItem;
use App\Models\PackageItem;
use App\Models\ShippingBox;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\ShipmentItem;
use App\Models\PurchaseOrder;
use App\Jobs\CalculateBillJob;
use App\Models\BillingProfile;
use App\Models\ShippingMethod;
use Igaster\LaravelCities\Geo;
use App\Models\InventoryChange;
use App\Models\ShippingCarrier;
use Illuminate\Database\Seeder;
use App\Models\CustomerUserRole;
use App\Models\PurchaseOrderItem;
use App\Models\ContactInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SandboxDataSeeder extends Seeder
{
    const ENV_PREFIX = 'SANDBOX_';

    private $oldestDay;

    private $purchaseOrderFrequencyInDays;
    private $purchaseOrderExpectedDateOffsetMin;
    private $purchaseOrderExpectedDateOffsetMax;
    private $purchaseOrderDeliveredDateOffsetMin;
    private $purchaseOrderDeliveredDateOffsetMax;
    private $purchaseOrderLineQuantityMin;
    private $purchaseOrderLineQuantityMax;
    private $purchaseOrderLineQuantityReceivedOffsetMin;
    private $purchaseOrderLineQuantityReceivedOffsetMax;

    private $ordersPerDayMin;
    private $ordersPerDayMax;
    private $orderLinesPerOrderMin;
    private $orderLinesPerOrderMax;
    private $orderLineQuantityMin;
    private $orderLineQuantityMax;
    private $orderFulfillLaterThanDays;

    private $shipmentsPerOrderMin;
    private $shipmentsPerOrderMax;

    private $packagesPerShipmentMin;
    private $packagesPerShipmentMax;

    private $inventoryChangesPerProductMin;
    private $inventoryChangesPerProductMax;

    private $returnsPerDayMin;
    private $returnsPerDayMax;

    private $customers = [];

    private $homeCountry;

    private $shippingPriceMin;
    private $shippingPriceMax;

    private $shippingCarriers = [];

    private $shippingBoxes = [];

    private $internationalShippingMethodWmsId;

    private $shipmentPackageItemsUses = [];

    private $billRate;
    private $billPeriod;
    private $billWeightRate;
    private $billVolumeRate;
    private $billItemRate;
    private $billBinRate;
    private $billVolumeUnit;
    private $billChargePeriodically;
    private $billIfNoOtherFeeApplies;
    private $billBaseShippingCost;
    private $billPercentageOfCost;
    private $billItemRatesTo;
    private $billItemRatesFrom;
    private $billFirstItemRate;
    private $billRestOfTheItemsRate;
    private $firstOfAdditionalSkusRate;
    private $adHocUnit;
    private $billShippingPrice;
    private $billShippingWeightTo;
    private $billShippingWeightFrom;
    private $billShippingWeightUnit;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var ThreePl
     */
    private $threePl;
    /**
     * @var ShippingMethod[]
     */
    private $nationalShippingMethods;
    /**
     * @var ShippingMethod
     */
    private $internationalShippingMethod;
    /**
     * @var Warehouse
     */
    private $warehouse;
    /**
     * @var Supplier[]
     */
    private $suppliers = [];

    /**
     * @var Product[]
     */
    private $products = [];

    /**
     * Run the database seeds.
     *
     * @param Customer|null $customer
     * @return void
     */
    public function run(?Customer $customer)
    {
        $this->loadSettings();

        if (!$customer) {
            $this->cleanup();

            foreach ($this->customers as $customer) {
                $this->createThreePl($customer);
                $this->createCustomer($customer);
                $this->createUsers($customer);
                $this->createShippingCarriersAndMethods();
                $this->createShippingBoxes();
                $this->createWarehouse();
                $this->createProducts();
                $this->createSuppliers();
                $this->createPurchaseOrders();
                $this->createOrders();
                $this->createReturns();
                $this->createBillingProfile();
            }
        } else {
            $this->customer = $customer;
            $this->warehouse = $customer->warehouses->first();
            $this->products = $customer->products;
            $this->suppliers = $customer->suppliers;

            $this->createPurchaseOrders();
            $this->createOrders();
            $this->createReturns();
        }
    }

    private function loadSettings()
    {
        $prefixLength = strlen(self::ENV_PREFIX);

        foreach ($_ENV as $key => $value) {
            if (strpos($key, self::ENV_PREFIX) === 0) {
                $key = substr($key, $prefixLength);

                $parts = preg_split('/_(\d)_/', $key, null, PREG_SPLIT_DELIM_CAPTURE);

                $property = null;

                foreach ($parts as $position => $part) {
                    $name = Str::camel(strtolower($part));

                    if (count($parts) == 1) {
                        if (property_exists($this, $name)) {
                            $this->$name = $value;
                        }

                        continue;
                    }

                    if ($position == 0) {
                        $property = &$this->$name;
                    }

                    if ($position && $position < count($parts) - 1) {
                        if (empty($property[$name])) {
                            $property[$name] = [];
                        }

                        $property = &$property[$name];
                    }

                    if ($position == count($parts) - 1) {
                        $property[$name] = $value;
                        unset($property);
                    }
                }
            }
        }
    }

    private function cleanup()
    {
        foreach ($this->customers as $customer) {
            Customer::whereHas('contactInformation', function ($query) use ($customer) {
                $query->where('name', $customer['name']);
            })->forceDelete();

            foreach ($customer['users'] as $user) {
                User::where('email', $user['email'])->forceDelete();
            }
        }
    }

    private function createCustomer($customer)
    {
        $this->customer = factory(Customer::class)->create(['3pl_id' => $this->threePl->id]);
        $this->createContactInformation($this->customer, [
            'name' => $customer['name']
        ]);
    }

    private function createUsers($customer)
    {
        foreach ($customer['users'] as $user) {
            $email = $user['email'];
            $password = $user['password'];

            /**
             * @var User $user
             */
            $user = factory(User::class)->create([
                'email' => $email,
                'password' => Hash::make($password)
            ]);

            $this->createContactInformation($user, [
                'name' => $email,
                'email' => $email
            ]);

            $this->customer->users()->syncWithoutDetaching([$user->id => ['role_id' => CustomerUserRole::ROLE_DEFAULT]]);
            $this->threePl->users()->syncWithoutDetaching([$user->id]);
        }
    }

    private function createShippingCarriersAndMethods()
    {
        $this->internationalShippingMethod = null;
        $this->nationalShippingMethods = [];

        foreach ($this->shippingCarriers as $carrier) {
            /**
             * @var ShippingCarrier $shippingCarrier
             */
            $shippingCarrier = factory(ShippingCarrier::class)->create([
                'customer_id' => $this->customer->id,
                'name' => $carrier['name'],
                'wms_id' => $carrier['wmsId'],
            ]);

            $image = new Image();
            $image->source = $carrier['logo'];
            $image->filename = $carrier['logo'];
            $image->object()->associate($shippingCarrier);
            $image->save();

            foreach ($carrier['methods'] as $method) {
                /**
                 * @var ShippingMethod $shippingMethod
                 */
                $shippingMethod = factory(ShippingMethod::class)->create([
                    'name' => $method['name'],
                    'title' => $method['name'],
                    'shipping_carrier_id' => $shippingCarrier->id,
                    'cost' => $method['cost'],
                    'wms_id' => $method['wmsId']
                ]);

                if ($method['wmsId'] == $this->internationalShippingMethodWmsId) {
                    $this->internationalShippingMethod = $shippingMethod;
                } else {
                    $this->nationalShippingMethods[] = $shippingMethod;
                }
            }
        }
    }

    private function createShippingBoxes()
    {
        foreach ($this->shippingBoxes as $box) {
            /**
             * @var ShippingBox $shippingBox
             */
            $shippingBox = factory(ShippingBox::class)->create([
                'customer_id' => $this->customer->id,
                'name' => $box['name'],
                'wms_id' => $box['wmsId'],
            ]);
        }
    }

    private function createWarehouse()
    {
        $this->warehouse = factory(Warehouse::class)->create([
            'customer_id' => $this->customer->id
        ]);
        $this->createContactInformation($this->warehouse, [
            'name' => 'Primary'
        ]);
    }

    private function createProducts()
    {
        $this->products = [];

        $csv = fopen(storage_path('sandbox/products.csv'), 'r');

        $header = [];

        while ($row = fgetcsv($csv)) {
            if (empty($header)) {
                $header = $row;
                continue;
            }

            $row = array_combine($header, $row);

            $product = factory(Product::class)->create([
                'customer_id' => $this->customer->id,
                'sku' => Arr::get($row, 'SKU'),
                'name' => Arr::get($row, 'Name'),
                'price' => str_replace(',', '.', Arr::get($row, 'Price')),
                'customs_price' => str_replace(',', '.', Arr::get($row, 'Customs Value')),
                'weight' => str_replace(',', '.', Arr::get($row, 'Weight')),
                'width' => str_replace(',', '.', Arr::get($row, 'Width')),
                'height' => str_replace(',', '.', Arr::get($row, 'Height')),
                'length' => str_replace(',', '.', Arr::get($row, 'Length')),
                'country_of_origin' => Arr::get($row, 'Country Of Manufacture'),
                'hs_code' => Arr::get($row, 'Tariff Code'),
                'replacement_value' => str_replace(',', '.', Arr::get($row, 'Customs Value')),
                'notes' => Arr::get($row, 'Product Note'),
                'quantity_on_hand' => 0,
                'quantity_allocated' => 0,
                'quantity_available' => 0,
                'quantity_backordered' => 0
            ]);

            $thumbnail = Arr::get($row, 'Thumbnail');

            if ($thumbnail) {
                $image = new Image();
                $image->source = $thumbnail;
                $image->filename = $thumbnail;
                $image->object()->associate($product);
                $image->save();
            }

            $this->createInventoryChanges($product);

            $this->products[] = $product;
        }
    }

    private function createInventoryChanges(Product $product)
    {
        $inventoryChangesPerProduct = rand($this->inventoryChangesPerProductMin, $this->inventoryChangesPerProductMax);

        for ($i = 0; $i < $inventoryChangesPerProduct; $i++) {
            factory(InventoryChange::class)->create([
                'product_id' => $product->id
            ]);
        }
    }

    private function createSuppliers()
    {
        $this->suppliers = [];

        foreach ($this->products as $product) {
            $productName = explode(' ', $product->name);
            array_pop($productName);

            $supplierName = implode(' ', $productName);

            if (!isset($this->suppliers[$supplierName])) {
                /**
                 * @var Supplier $supplier
                 */
                $supplier = factory(Supplier::class)->create([
                    'customer_id' => $this->customer->id
                ]);
                $this->createContactInformation($supplier, [
                    'name' => $supplierName
                ]);

                $this->suppliers[$supplierName] = $supplier;
            }

            $this->suppliers[$supplierName]->products()->syncWithoutDetaching($product->id);
        }

        $this->suppliers = array_values($this->suppliers);
    }

    private function createPurchaseOrders()
    {
        $date = now()->subDays($this->oldestDay);

        for ($i = 0; $i < floor($this->oldestDay / $this->purchaseOrderFrequencyInDays); $i++) {
            $expectedDate = $date->clone()->addDays(rand($this->purchaseOrderExpectedDateOffsetMin, $this->purchaseOrderExpectedDateOffsetMax));
            $deliveredDate = $expectedDate->clone()->addDays(rand($this->purchaseOrderDeliveredDateOffsetMin, $this->purchaseOrderDeliveredDateOffsetMax));

            $supplier = $this->suppliers[$i % count($this->suppliers)];

            $purchaseOrder = factory(PurchaseOrder::class)->create([
                'customer_id' => $this->customer->id,
                'warehouse_id' => $this->warehouse->id,
                'supplier_id' => $supplier->id,
                'number' => strtolower($this->customer->contactInformation->name) . '-' . ($i + 1),
                'status' => now()->diffInDays($date) < 30 ? 'pending' : 'closed',
                'ordered_at' => $date,
                'expected_at' => $expectedDate,
                'delivered_at' => $deliveredDate
            ]);

            foreach ($supplier->products as $key => $product) {
                $quantity = rand($this->purchaseOrderLineQuantityMin, $this->purchaseOrderLineQuantityMax);

                factory(PurchaseOrderItem::class)->create([
                    'purchase_order_id' => $purchaseOrder->id,
                    'product_id' => $product->id,
                    'quantity' => $quantity,
                    'quantity_received' => $quantity - rand($this->purchaseOrderLineQuantityReceivedOffsetMin, $this->purchaseOrderLineQuantityReceivedOffsetMax),
                    'sku' => $product->sku,
                    'name' => $product->name,
                    'price' => $product->price,
                    'customs_price' => $product->customs_price,
                    'weight' => $product->weight,
                    'width' => $product->width,
                    'height' => $product->height,
                    'length' => $product->length,
                    'country_of_origin' => $product->country_of_origin,
                    'hs_code' => $product->hs_code,
                    'barcode' => $product->barcode,
                    'replacement_value' => $product->replacement_value,
                    'product_type' => $product->product_type
                ]);
            }

            $date->addDays($this->purchaseOrderFrequencyInDays);
        }
    }

    private function createOrders()
    {
        $date = now()->subDays($this->oldestDay);

        for ($i = 0; $i < $this->oldestDay; $i++) {
            $numberOfOrdersPerDay = rand($this->ordersPerDayMin, $this->ordersPerDayMax);
            dump(now()->toDateTimeString() . ' ' . $date->toDateTimeString() . ', number of orders: ' . $numberOfOrdersPerDay);

            for ($j = 0; $j < $numberOfOrdersPerDay; $j++) {
                $fraudHold = rand(0, 99) == 0;
                $addressHold = rand(0, 99) == 0;
                $paymentHold = rand(0, 99) == 0;
                $operatorHold = rand(0, 99) == 0;
                $isCancelled = rand(0, 49) == 0;

                $fulfillFully = !$fraudHold && !$addressHold && !$paymentHold && !$operatorHold && !$isCancelled && $date->lessThan(now()->subDays($this->orderFulfillLaterThanDays)) && rand(0, 500) > 1;

                /**
                 * @var Order $order
                 *
                 */
                $order = factory(Order::class)->create([
                    'customer_id' => $this->customer->id,
                    'number' => strtolower($this->customer->contactInformation->name) . '-' . $date->format('ymd') . $j,
                    'ordered_at' => $date->toDateTimeString(),
                    'required_shipping_date_at' => $date->clone()->addDays(14),
                    'notes' => '',
                    'priority' => rand(0, 99) == 0,
                    'shipping_price' => rand($this->shippingPriceMin, $this->shippingPriceMax) . '.' . rand(0, 99),
                    'tax' => 0,
                    'discount' => 0,
                    'shipping_carrier_id' => null,
                    'shipping_method_id' => null,
                    'fraud_hold' => $fraudHold,
                    'address_hold' => $addressHold,
                    'payment_hold' => $paymentHold,
                    'operator_hold' => $operatorHold,
                    'is_cancelled' => $isCancelled,
                    'shipping_carrier_title' => '',
                    'shipping_method_title' => '',
                ]);

                $shippingContactInformation = $this->createContactInformation($order);

                $billingContactInformation = $shippingContactInformation->replicate();
                unset($billingContactInformation->latitude);
                unset($billingContactInformation->longitude);
                $billingContactInformation->save();

                if ($shippingContactInformation->country->code == $this->homeCountry) {
                    $shippingMethod = Arr::random($this->nationalShippingMethods);
                } else {
                    $shippingMethod = $this->internationalShippingMethod;
                }

                $order->update([
                    'shipping_contact_information_id' => $shippingContactInformation->id,
                    'billing_contact_information_id' => $billingContactInformation->id,
                    'shipping_lat' => $shippingContactInformation->latitude,
                    'shipping_lng' => $shippingContactInformation->longitude,
                    'shipping_carrier_id' => $shippingMethod->shippingCarrier->id,
                    'shipping_method_id' => $shippingMethod->id,
                    'shipping_carrier_title' => $shippingMethod->shippingCarrier->title,
                    'shipping_method_title' => $shippingMethod->title,
                ]);

                $products = $this->products;
                $subtotal = 0;

                $numberOfOrderLinesPerOrder = rand($this->orderLinesPerOrderMin, $this->orderLinesPerOrderMax);

                for ($k = 0; $k < $numberOfOrderLinesPerOrder; $k++) {
                    $productKey = array_rand($products);
                    $product = $products[$productKey];
                    $quantity = rand($this->orderLineQuantityMin, $this->orderLineQuantityMax);

                    $subtotal += $quantity * $product->price;

                    factory(OrderItem::class)->create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'quantity' => $quantity,
                        'quantity_shipped' => 0,
                        'quantity_backordered' => 0,
                        'sku' => $product->sku,
                        'name' => $product->name,
                        'price' => $product->price,
                        'customs_price' => $product->customs_price,
                        'weight' => $product->weight,
                        'width' => $product->width,
                        'height' => $product->height,
                        'length' => $product->length,
                        'country_of_origin' => $product->country_of_origin,
                        'hs_code' => $product->hs_code,
                        'barcode' => $product->barcode,
                        'replacement_value' => $product->replacement_value,
                        'product_type' => $product->product_type
                    ]);

                    unset($products[$productKey]);
                }

                if ($fulfillFully) {
                    $numberOfShipmentsPerOrder = rand($this->shipmentsPerOrderMin, $this->shipmentsPerOrderMax);

                    for ($k = 0; $k < $numberOfShipmentsPerOrder; $k++) {
                        /**
                         * @var Shipment $shipment
                         */
                        $shipment = factory(Shipment::class)->create([
                            'order_id' => $order->id,
                            'number' => $order->number . '-' . ($k + 1),
                            'created_at' => $order->ordered_at->addDays(rand(0, 14)),
                            'shipped_at' => $order->ordered_at->addDays(rand(14, 18))
                        ]);

                        $this->createContactInformation($shipment);

                        foreach ($order->orderItems as $orderItem) {
                            if ($k == $numberOfShipmentsPerOrder - 1) {
                                $quantityToShip = $orderItem->quantity - $orderItem->quantity_shipped;
                            } else {
                                $quantityToShip = rand(0, $orderItem->quantity - $orderItem->quantity_shipped);
                            }

                            factory(ShipmentItem::class)->create([
                                'shipment_id' => $shipment->id,
                                'order_item_id' => $orderItem->id,
                                'quantity' => $quantityToShip
                            ]);
                        }

                        $shipment->save();

                        $this->createPackages($shipment);

                        $order->refresh();
                    }
                }

                if (rand(0, 9) == 0) {
                    $order->discount = $subtotal * 0.1;
                }

                $order->calculateOrder();
            }

            $date->addDay();
        }
    }

    private function createReturns()
    {
        $date = now()->subDays($this->oldestDay);

        for ($i = 0; $i < $this->oldestDay; $i++) {
            $returnsPerDay = rand($this->returnsPerDayMin, $this->returnsPerDayMax);

            $orders = Order::where('customer_id', $this->customer->id)
                ->where('status', 'fulfilled')
                ->whereBetween('ordered_at', [$date->toDateString() . ' 00:00:00', $date->toDateString() . ' 23:59:59'])->inRandomOrder()->limit($returnsPerDay)->get();

            foreach ($orders as $order) {
                /**
                 * @var Return_ $return
                 */
                $return = factory(Return_::class)->create([
                    'order_id' => $order->id,
                    'warehouse_id' => $this->warehouse->id,
                    'requested_at' => $date->clone()->addDays(2),
                    'status' => array_keys(Return_::STATUS_ICONS)[rand(0, count(Return_::STATUS_ICONS) - 2)],
                    'shipping_carrier_id' => $order->shippingCarrier->id,
                    'shipping_method_id' => $order->shippingMethod->id,
                    'shipping_carrier_title' => $order->shippingCarrier->name,
                    'shipping_method_title' => $order->shippingMethod->name
                ]);

                foreach ($order->orderItems as $orderItem) {
                    $quantity = rand(0, $orderItem->quantity_shipped);

                    if ($quantity) {
                        factory(ReturnItem::class)->create([
                            'return_id' => $return->id,
                            'order_item_id' => $orderItem->id,
                            'quantity' => $quantity,
                            'quantity_received' => $quantity
                        ]);
                    }
                }
            }

            $date->addDay();
        }
    }

    private function createPackages(Shipment $shipment)
    {
        $packagesPerShipment = rand($this->packagesPerShipmentMin, $this->packagesPerShipmentMax);

        for ($i = 0; $i < $packagesPerShipment; $i++) {
            $shippingBoxes = ShippingBox::where('customer_id', $this->customer->id)->get();

            foreach ($shippingBoxes as $shippingBox) {
                /**
                 * @var Package $package
                 */
                $package = factory(Package::class)->create([
                    'shipment_id' => $shipment->id,
                    'shipping_box_id' => $shippingBox->id,
                    'shipping_carrier_id' => $shipment->order->shippingCarrier->id,
                    'shipping_method_id' => $shipment->order->shippingMethod->id,
                ]);

                $addedLines = false;
                foreach ($shipment->shipmentItems as $shipmentItem) {
                    $availableQuantity = $shipmentItem->quantity;
                    if (!isset($this->shipmentPackageItemsUses[$shipment->id][$shipmentItem->id]['remaining'])) {
                        $this->shipmentPackageItemsUses[$shipment->id][$shipmentItem->id]['remaining'] = $availableQuantity;
                    } else {
                        $availableQuantity = $this->shipmentPackageItemsUses[$shipment->id][$shipmentItem->id]['remaining'];
                    }

                    $quantity = rand(0, $availableQuantity);
                    if ($i == ($packagesPerShipment - 1) && $this->shipmentPackageItemsUses[$shipment->id][$shipmentItem->id]['remaining'] > 0) {
                        $quantity = $availableQuantity;
                    }

                    if ($quantity) {
                        $addedLines = true;
                        factory(PackageItem::class)->create([
                            'package_id' => $package->id,
                            'order_item_id' => $shipmentItem->order_item_id,
                            'quantity' => $quantity
                        ]);

                        $this->shipmentPackageItemsUses[$shipment->id][$shipmentItem->id]['remaining'] -= $quantity;
                    }
                }

                if (!$addedLines) {
                    $package->forceDelete();
                }
            }
        }
    }

    private function createContactInformation($object, $data = []): ContactInformation
    {
        $address = $this->getAddress();

        $data = array_merge($data, [
            'object_type' => get_class($object),
            'object_id' => $object->id,
            'city' => $address->name,
            'country_id' => Country::where('code', $address->country)->first()->id ?? 1,
        ]);

        $contactInformation = factory(ContactInformation::class)->create($data);

        $contactInformation->latitude = $address->lat;
        $contactInformation->longitude = $address->long;

        return $contactInformation;
    }

    private function getAddress(): Geo
    {
        if (rand(0, 9) == 0) {
            $country = Country::inRandomOrder()->first()->code;
        } else {
            $country = $this->homeCountry;
        }

        $geo = Geo::where('country', $country)->whereIn('level', ['PPLC', 'PPLA', 'PPLA2'])->orderBy('population', 'desc')->limit(100)->get();

        if ($geo->count()) {
            return $geo->random();
        } else {
            return $this->getAddress();
        }
    }

    private function createThreePl($customer)
    {
        $this->threePl = factory(ThreePl::class)->create();
        $this->createContactInformation($this->threePl, [
            'name' => $customer['name']
        ]);
    }

    private function createBillingProfile()
    {
        $customer = Customer::where('3pl_id', $this->threePl->id)->orderBy('id', 'asc')->first();

        if ($customer && !empty($customer->billing_profile_id))
            return;

        $billingProfile = factory(BillingProfile::class)->create();

        $customer->billing_profile_id = $billingProfile->id;
        $customer->update();

        $this->createBillingFee($billingProfile);
        $this->createBill($customer);
    }

    private function createBill($customer)
    {
        $request = [
            "start_date" => now()->subDays($this->oldestDay),
            "end_date" => now(),
            "customer" => $customer->id
        ];

        CalculateBillJob::dispatch($request, $customer);
    }

    private function createBillingFee($billingProfile)
    {
        $settings = [
            "recurring" => [
                "rate" => $this->billRate,
                "period" => $this->billPeriod,
                "charge_periodically" => $this->billChargePeriodically
            ],
            "receiving_by_po" => [
                "rate" => $this->billRate
            ],
            "receiving_by_item" => [
                "product_profiles" => "[]",
                "weight_rate" => $this->billWeightRate,
                "volume_rate" => $this->billVolumeRate
            ],
            "receiving_by_line" => [
                "product_profiles" => "[]",
                "weight_rate" => $this->billWeightRate,
                "volume_rate" => $this->billVolumeRate
            ],
            "storage_by_location" => [
                "location_types" => "[]",
                "rate" => $this->billRate,
                "period" => $this->billPeriod
            ],
            "storage_by_product" => [
                "location_types" => "[]",
                "product_profiles" => "[]",
                "period" => $this->billPeriod,
                "item_rate" => $this->billItemRate,
                "bin_rate" => $this->billBinRate,
                "volume_rate" => $this->billVolumeRate,
                "volume_unit" => $this->billVolumeUnit
            ],
            "shipments_by_box" => [
                "shipping_boxes" => "[]",
                "rate" => $this->billRate
            ],
            "shipments_by_shipping_label" => [
                "if_no_other_fee_applies" => $this->billIfNoOtherFeeApplies,
                "carriers_and_methods" => "[]",
                "methods_selected" => "{}",
                "base_shipping_cost" => $this->billBaseShippingCost,
                "percentage_of_cost" => $this->billPercentageOfCost
            ],
            "shipments_by_picking_fee" => [
                "item_rates" => [
                    [
                        "to" => $this->billItemRatesTo,
                        "from" => $this->billItemRatesFrom,
                        "rate" => $this->billRate
                    ],
                    "first_item_rate" => $this->billFirstItemRate,
                    "rest_of_the_items" => $this->billRestOfTheItemsRate
                ],
                "without_profile" => "1",
                "product_profiles" => "[]",
                "first_of_additional_skus_rate" => $this->firstOfAdditionalSkusRate
            ],
            "shipments_by_pickup_picking_fee" => [
                "item_rates" => [
                    [
                        "to" => $this->billItemRatesTo,
                        "from" => $this->billItemRatesFrom,
                        "rate" => $this->billRate
                    ],
                    "first_item_rate" => $this->billFirstItemRate,
                    "rest_of_the_items" => $this->billRestOfTheItemsRate
                ],
                "product_profiles" => "[]",
                "first_of_additional_skus_rate" => $this->firstOfAdditionalSkusRate
            ],
            "returns" => [
                "item_rates" => [
                    [
                        "to" => $this->billItemRatesTo,
                        "from" => $this->billItemRatesFrom,
                        "rate" => $this->billRate
                    ],
                    "first_item_rate" => $this->billFirstItemRate,
                    "rest_of_the_items" => $this->billRestOfTheItemsRate
                ],
                "product_profiles" => "[]",
                "first_of_additional_skus_rate" => $this->firstOfAdditionalSkusRate
            ],
            "ad_hoc" => [
                "rate" => $this->billRate,
                "unit" => $this->adHocUnit,
                "description" => "Add Ad hoc Fee"
            ],
            "shipping_rates" => [
                "description" => "Add Shipping rates Fee",
                "shipping_prices" => [
                    [
                        "price" => $this->billShippingPrice,
                        "weight_to" => $this->billShippingWeightTo,
                        "weight_from" => $this->billShippingWeightFrom,
                        "weight_unit" => $this->billShippingWeightUnit
                    ]
                ],
                "shipping_carrier_id" => ""
            ]
        ];

        foreach (BillingFee::BILLLING_FEE_TYPES as $key => $value) {
            $billingFee = factory(BillingFee::class)->create([
                'billing_profile_id' => $billingProfile->id,
                'name' => $value['title'],
                'type' => $key,
                'settings' => $settings[$key]
            ]);
        }
    }
}
