<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('countries')->truncate();
        DB::table('user_roles')->truncate();
        DB::table('customer_user_roles')->truncate();
        DB::table('users')->truncate();
        DB::table('customers')->truncate();
        DB::table('currencies')->truncate();

        $this->call([
            CountriesSeeder::class,
            UserRolesTableSeeder::class,
            CustomerUserRolesTableSeeder::class,
            UsersTableSeeder::class,
            CurrencySeeder::class
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Artisan::call('passport:client', array(
            '--personal' => true,
            '--name' => 'Personal Access Token Client'
        ));
    }
}
