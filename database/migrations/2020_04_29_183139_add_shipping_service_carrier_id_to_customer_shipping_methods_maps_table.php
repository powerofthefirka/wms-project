<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingServiceCarrierIdToCustomerShippingMethodsMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_shipping_methods_maps', function (Blueprint $table) {
            $table->integer('shipping_service_carrier_id')->after('shipping_service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_shipping_methods_maps', function (Blueprint $table) {
            $table->dropColumn('shipping_service_carrier_id');
        });
    }
}
