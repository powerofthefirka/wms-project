<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHoldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('fraud_hold')->default(0);
            $table->boolean('address_hold')->default(0);
            $table->boolean('payment_hold')->default(0);
            $table->boolean('operator_hold')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('fraud_hold');
            $table->dropColumn('address_hold');
            $table->dropColumn('payment_hold');
            $table->dropColumn('operator_hold');
        });
    }
}
