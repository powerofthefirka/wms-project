<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create3plPricingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('3pl_pricing_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('3pl_id');
            $table->string("pricing_plan_name");
            $table->enum('billing_period', ['yearly', 'monthly'])->default('yearly');
            $table->decimal('monthly_price', 12, 2)->nullable();
            $table->decimal('initial_fee', 12, 2)->nullable();
            $table->decimal('three_pl_billing_fee', 12, 2)->nullable();
            $table->integer('number_of_monthly_orders')->nullable();
            $table->decimal('price_per_additional_order', 12, 2)->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('3pl_id')
                ->references('id')
                ->on('3pls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('3pl_pricing_plans');
    }
}
