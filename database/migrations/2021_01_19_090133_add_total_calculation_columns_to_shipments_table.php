<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalCalculationColumnsToShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->decimal('distinct_items', 12)->nullable();
            $table->decimal('quantity_shipped', 12)->nullable();
            $table->decimal('line_item_total', 12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('distinct_items');
            $table->dropColumn('quantity_shipped');
            $table->dropColumn('line_item_total');
        });
    }
}
