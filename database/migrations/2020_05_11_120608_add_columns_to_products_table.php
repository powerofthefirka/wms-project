<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('custom_price', 12)->default(0)->after('price');
            $table->decimal('weight', 12)->after('custom_price');
            $table->decimal('width', 12)->after('weight');
            $table->decimal('height', 12)->after('width');
            $table->decimal('length', 12)->after('height');
            $table->string('country_of_origin')->after('length');
            $table->string('image')->nullable()->after('country_of_origin');
            $table->bigInteger('hs_code')->nullable()->after('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('custom_price');
            $table->dropColumn('weight');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('length');
            $table->dropColumn('country_of_origin');
            $table->dropColumn('image');
            $table->dropColumn('hs_code');
        });
    }
}
