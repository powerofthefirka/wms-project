<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tasks');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string("taskable_type")->nullable();
            $table->unsignedInteger("taskable_id")->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('task_type_id');
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
            $table->index(["taskable_type", "taskable_id"], "taskable");

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('task_type_id')
                ->references('id')
                ->on('task_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }
}
