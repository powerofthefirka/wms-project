<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ThreePl;
use App\Models\Location;

class CreateCustomerLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('location_id');
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->insertData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_locations');
    }

    public function insertData()
    {
        foreach (ThreePl::all() as $threePl) {
            if ($threePl->locationTypes->count() == 0) {
                continue;
            }

            $locationIds = Location::whereIn('location_type_id', $threePl->locationTypes->pluck('id'))->pluck('id');

            foreach ($threePl->customers as $customer) {
                $customer->locations()->sync($locationIds);
            }
        }
    }
}
