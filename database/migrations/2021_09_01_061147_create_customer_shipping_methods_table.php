<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ThreePl;
use App\Models\ShippingMethod;

class CreateCustomerShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_shipping_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('shipping_method_id');
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('shipping_method_id')
                ->references('id')
                ->on('shipping_methods')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->insertData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_shipping_methods');
    }

    public function insertData()
    {
        foreach (ThreePl::all() as $threePl) {
            if ($threePl->shippingCarriers->count() == 0) {
                continue;
            }

            $shippingMethodIds = ShippingMethod::whereIn('shipping_carrier_id', $threePl->shippingCarriers->pluck('id'))->pluck('id');

            foreach ($threePl->customers as $customer) {
                $customer->shippingMethods()->sync($shippingMethodIds);
            }
        }
    }
}
