<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create3plUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('3pl_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('3pl_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('3pl_id')
                ->references('id')
                ->on('3pls')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('3pl_user');
    }
}
