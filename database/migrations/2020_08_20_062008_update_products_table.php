<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('customs_price')->nullable()->change();
            $table->string('weight')->nullable()->change();
            $table->string('width')->nullable()->change();
            $table->string('height')->nullable()->change();
            $table->string('length')->nullable()->change();
            $table->string('country_of_origin')->nullable()->change();
            $table->string('barcode')->nullable()->change();
            $table->string('replacement_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('customs_price')->nullable(false)->change();
            $table->string('weight')->nullable(false)->change();
            $table->string('width')->nullable(false)->change();
            $table->string('height')->nullable(false)->change();
            $table->string('length')->nullable(false)->change();
            $table->string('country_of_origin')->nullable(false)->change();
            $table->string('barcode')->nullable(false)->change();
            $table->string('replacement_value')->nullable(false)->change();
        });
    }
}
