<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalSubtotalTaxDiscountToPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->decimal('subtotal', 12)->default(0);
            $table->decimal('discount', 12)->default(0);
            $table->decimal('tax', 12)->default(0);
            $table->decimal('total', 12)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropColumn('total');
            $table->dropColumn('tax');
            $table->dropColumn('discount');
            $table->dropColumn('subtotal');
        });
    }
}
