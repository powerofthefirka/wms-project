<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Customer;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;

class Add3pldIdToShippingCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_carriers', function (Blueprint $table) {
            $table->unsignedInteger('3pl_id')->nullable();

            $table->foreign('3pl_id')
                ->references('id')
                ->on('3pls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->updateCarriersAndMethods();

        Schema::table('shipping_carriers', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);

//            $table->dropColumn('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_carriers', function (Blueprint $table) {
            $table->dropForeign(['3pl_id']);

            $table->dropColumn('3pl_id');
        });

        Schema::table('shipping_carriers', function (Blueprint $table) {
            $table->unsignedInteger('customer_id')->nullable();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function updateCarriersAndMethods()
    {
        $customerIds = Customer::get()->pluck('id');

        foreach (ShippingCarrier::whereIn('customer_id', $customerIds)->get() as $carrier) {
            if (empty($carrier->name) || empty($carrier->wms_id)) {
                $carrier->delete();
                continue;
            }

            $carrier->update(['name' => trim($carrier->name), 'wms_id' => trim($carrier->wms_id), '3pl_id' => $carrier->customer->threePl->id]);
        }

        $carriersGroupBy3pl = ShippingCarrier::whereIn('customer_id', $customerIds)->groupBy('3pl_id')->get();

        foreach ($carriersGroupBy3pl as $key => $carrierGroupBy3pl) {
            $carriers = ShippingCarrier::where('3pl_id', $carrierGroupBy3pl->threePl->id)->get();
            $distinctCarriers = ShippingCarrier::where('3pl_id', $carrierGroupBy3pl->threePl->id)->groupBy('wms_id')->get();

            $methods = ShippingMethod::whereIn('shipping_carrier_id', $carriers->pluck('id'))->get();

            foreach ($methods as $method) {
                if (empty($method->name) || empty($method->wms_id)) {
                    $method->delete();
                    continue;
                }

                $method->update(['name' => trim($method->name), 'wms_id' => trim($method->wms_id), 'title' => trim($method->title)]);

                $distinctCarrier = $distinctCarriers->where('wms_id', $method->shippingCarrier->wms_id)->first();

                try {
                    $method->update(['shipping_carrier_id' => $distinctCarrier->id]);
                } catch (\Throwable $th) {
                    $distinctCarrier = ShippingCarrier::whereIn('id', $distinctCarriers->pluck('id'))->whereRaw('LOWER(`wms_id`) LIKE ? ', [trim(strtolower($method->shippingCarrier->wms_id)).'%'])->first();
                    $method->update(['shipping_carrier_id' => $distinctCarrier->id]);
                }
            }

            foreach ($distinctCarriers as $distinctCarrier) {
                $distinctMethods = ShippingMethod::where('shipping_carrier_id', $distinctCarrier->id)->groupBy('wms_id')->get();

                ShippingMethod::where('shipping_carrier_id', $distinctCarrier->id)->whereNotIn('id', $distinctMethods->pluck('id'))->delete();
            }

            ShippingCarrier::where('3pl_id', $carrierGroupBy3pl->threePl->id)->whereNotIn('id', $distinctCarriers->pluck('id'))->delete();
        }
    }
}
