<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDefaultsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_informations', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('zip')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('phone')->nullable()->change();
        });

        Schema::table('location_types', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('wms_id')->nullable()->change();
        });

        Schema::table('invoice_statuses', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->string('primary_color')->nullable()->change();
            $table->string('secondary_color')->nullable()->change();
            $table->string('logo_url')->nullable()->default("''")->change();
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->string('code')->nullable()->change();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('sku')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->decimal('price', 12)->nullable()->default(0)->change();
            $table->integer('quantity_on_hand')->nullable()->default(0)->change();
            $table->integer('quantity_allocated')->nullable()->default(0)->change();
            $table->integer('quantity_available')->nullable()->default(0)->change();
            $table->integer('product_type')->nullable()->default(0)->change();

            $table->decimal('weight', 12)->nullable()->default(0)->charset(null)->change();
            $table->decimal('width', 12)->nullable()->default(0)->charset(null)->change();
            $table->decimal('height', 12)->nullable()->default(0)->charset(null)->change();
            $table->decimal('length', 12)->nullable()->default(0)->charset(null)->change();
        });

        Schema::table('product_images', function (Blueprint $table) {
            $table->text('source')->nullable()->change();
            $table->text('filename')->nullable()->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->decimal('shipping_price', 12)->nullable()->default(0)->change();
            $table->decimal('tax', 12)->nullable()->default(0)->change();
            $table->decimal('discount', 12)->nullable()->default(0)->change();
            $table->integer('priority')->nullable()->default(0)->change();
            $table->integer('shipment_type')->nullable()->default(0)->change();
            $table->boolean('is_cancelled')->nullable()->default(0)->change();
            $table->string('status')->nullable()->default('pending')->change();
            $table->boolean('fraud_hold')->nullable()->default(0)->change();
            $table->boolean('address_hold')->nullable()->default(0)->change();
            $table->boolean('payment_hold')->nullable()->default(0)->change();
            $table->boolean('operator_hold')->nullable()->default(0)->change();
            $table->unsignedInteger('item_count')->nullable()->default(0)->change();
            $table->integer('discount_type')->nullable()->default(0)->change();
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->string('sku')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->float('quantity')->nullable()->default(0)->change();
            $table->float('quantity_shipped')->nullable()->default(0)->change();
            $table->decimal('price', 12)->nullable()->default(0)->change();
            $table->decimal('customs_price', 12)->nullable()->default(0)->change();
            $table->integer('product_type')->nullable()->default(0)->change();
            $table->float('quantity_pending')->nullable()->default(0)->change();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->string('priority')->nullable()->change();
            $table->decimal('shipping_price', 12)->nullable()->default(0)->change();
        });

        Schema::table('purchase_order_items', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('sku')->nullable()->change();
            $table->float('quantity')->nullable()->default(0)->change();
            $table->float('quantity_received')->nullable()->default(0)->change();
            $table->decimal('price', 12)->nullable()->default(0)->change();
            $table->decimal('customs_price', 12)->nullable()->default(0)->change();
            $table->integer('product_type')->nullable()->default(0)->change();
        });

        Schema::table('returns', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->text('reason')->nullable()->change();
            $table->boolean('create_label')->nullable()->default(1)->change();
        });

        Schema::table('return_items', function (Blueprint $table) {
            $table->float('quantity')->nullable()->default(0)->change();
            $table->float('quantity_received')->nullable()->default(0)->change();
        });

        Schema::table('shipment_items', function (Blueprint $table) {
            $table->float('quantity')->nullable()->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_informations', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->string('address')->nullable(false)->change();
            $table->string('zip')->nullable(false)->change();
            $table->string('city')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
            $table->string('phone')->nullable(false)->change();
        });

        Schema::table('invoice_statuses', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->string('primary_color')->nullable(false)->change();
            $table->string('secondary_color')->nullable(false)->change();
            $table->string('logo_url')->nullable(false)->default("''")->change();
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->string('title')->nullable(false)->change();
            $table->string('code')->nullable(false)->change();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('sku')->nullable(false)->change();
            $table->string('name')->nullable(false)->change();
            $table->decimal('price', 12)->nullable(false)->default(0)->change();
            $table->integer('quantity_on_hand')->nullable(false)->default(0)->change();
            $table->integer('quantity_allocated')->nullable(false)->default(0)->change();
            $table->integer('quantity_available')->nullable(false)->default(0)->change();
            $table->integer('product_type')->nullable(false)->default(0)->change();
            $table->string('weight')->default(0)->change();
            $table->string('width')->default(0)->change();
            $table->string('height')->default(0)->change();
            $table->string('length')->default(0)->change();
        });

        Schema::table('product_images', function (Blueprint $table) {
            $table->text('source')->nullable(false)->change();
            $table->text('filename')->nullable(false)->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('number')->nullable(false)->change();
            $table->decimal('shipping_price', 12)->nullable(false)->default(0)->change();
            $table->decimal('tax', 12)->nullable(false)->default(0)->change();
            $table->decimal('discount', 12)->nullable(false)->default(0)->change();
            $table->integer('priority')->nullable(false)->default(0)->change();
            $table->integer('shipment_type')->nullable(false)->default(0)->change();
            $table->boolean('is_cancelled')->nullable(false)->default(0)->change();
            $table->string('status')->nullable(false)->default('pending')->change();
            $table->boolean('fraud_hold')->nullable(false)->default(0)->change();
            $table->boolean('address_hold')->nullable(false)->default(0)->change();
            $table->boolean('payment_hold')->nullable(false)->default(0)->change();
            $table->boolean('operator_hold')->nullable(false)->default(0)->change();
            $table->unsignedInteger('item_count')->nullable(false)->default(0)->change();
            $table->integer('discount_type')->nullable(false)->default(0)->change();
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->string('sku')->nullable(false)->change();
            $table->string('name')->nullable(false)->change();
            $table->float('quantity')->nullable(false)->default(null)->change();
            $table->float('quantity_shipped')->nullable(false)->default(null)->change();
            $table->decimal('price', 12)->nullable(false)->default(0)->change();
            $table->decimal('customs_price', 12)->nullable(false)->default(0)->change();
            $table->integer('product_type')->nullable(false)->default(0)->change();
            $table->float('quantity_pending')->nullable(false)->default(0)->change();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('number')->nullable(false)->change();
            $table->string('priority')->nullable(false)->change();
            $table->decimal('shipping_price', 12)->nullable(false)->default(0)->change();
        });

        Schema::table('purchase_order_items', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->string('sku')->nullable(false)->change();
            $table->float('quantity')->nullable(false)->default(null)->change();
            $table->float('quantity_received')->nullable(false)->default(null)->change();
            $table->decimal('price', 12)->nullable(false)->default(0)->change();
            $table->decimal('customs_price', 12)->nullable(false)->default(0)->change();
            $table->integer('product_type')->nullable(false)->default(0)->change();
        });

        Schema::table('returns', function (Blueprint $table) {
            $table->string('number')->nullable(false)->change();
            $table->text('reason')->nullable(false)->change();
            $table->boolean('create_label')->nullable(false)->default(1)->change();
        });

        Schema::table('return_items', function (Blueprint $table) {
            $table->float('quantity')->nullable(false)->default(null)->change();
            $table->float('quantity_received')->nullable(false)->default(null)->change();
        });

        Schema::table('shipment_items', function (Blueprint $table) {
            $table->float('quantity')->nullable(false)->default(0)->change();
        });
    }
}
