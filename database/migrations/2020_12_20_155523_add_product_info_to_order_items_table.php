<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductInfoToOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->string('sku');
            $table->string('name');
            $table->decimal('price', 12)->default(0);
            $table->decimal('customs_price', 12)->default(0);
            $table->string('weight')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('length')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->string('barcode')->nullable();
            $table->string('replacement_value')->nullable();
            $table->integer('product_type')->default(0);
            $table->bigInteger('hs_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('sku');
            $table->dropColumn('name');
            $table->dropColumn('price');
            $table->dropColumn('customs_price');
            $table->dropColumn('weight');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('length');
            $table->dropColumn('country_of_origin');
            $table->dropColumn('barcode');
            $table->dropColumn('replacement_value');
            $table->dropColumn('product_type');
            $table->dropColumn('hs_code');
        });
    }
}
