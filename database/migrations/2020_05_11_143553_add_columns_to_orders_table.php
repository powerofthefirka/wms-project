<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('shipping_price', 12)->default(0)->after('shipping_date_before_at');
            $table->decimal('tax', 12)->default(0)->after('shipping_price');
            $table->decimal('discount', 12)->default(0)->after('tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipping_price');
            $table->dropColumn('tax');
            $table->dropColumn('discount');
        });
    }
}
