<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingCarrierIdAndShippingMehtodIdToShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->unsignedInteger('shipping_carrier_id')->after('tracking_link')->nullable();
            $table->unsignedInteger('shipping_method_id')->after('shipping_carrier_id')->nullable();
            

            $table->foreign('shipping_carrier_id')
                ->references('id')
                ->on('shipping_carriers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('shipping_method_id')
                ->references('id')
                ->on('shipping_methods')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropForeign('shipments_shipping_carrier_id_foreign');
            $table->dropColumn('shipping_carrier_id');   

            $table->dropForeign('shipments_shipping_method_id_foreign');
            $table->dropColumn('shipping_method_id');
        });
    }
}
