<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomCustomerShippingModalColumnsTo3plsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('3pls', function (Blueprint $table) {
            $table->boolean('custom_shipping_modal')->default(0)->nullable();
            $table->text('custom_shipping_modal_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('3pls', function (Blueprint $table) {
            $table->dropColumn('custom_shipping_modal');
            $table->dropColumn('custom_shipping_modal_text');
        });
    }
}
