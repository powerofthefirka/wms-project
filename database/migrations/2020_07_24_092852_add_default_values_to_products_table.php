<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValuesToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price', 12)->default(0)->change();
            $table->decimal('weight', 12)->default(0)->change();
            $table->decimal('width', 12)->default(0)->change();
            $table->decimal('height', 12)->default(0)->change();
            $table->decimal('length', 12)->default(0)->change();
            $table->string('country_of_origin')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price', 12)->default(null)->charset(null)->change();
            $table->decimal('weight', 12)->default(null)->charset(null)->change();
            $table->decimal('width', 12)->default(null)->charset(null)->change();
            $table->decimal('height', 12)->default(null)->charset(null)->change();
            $table->decimal('length', 12)->default(null)->charset(null)->change();
            $table->string('country_of_origin')->nullable(false)->change();
        });
    }
}
