<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarrierMethodPriceColumnsToPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->unsignedInteger('shipping_carrier_id')->nullable();
            $table->unsignedInteger('shipping_method_id')->nullable();
            

            $table->foreign('shipping_carrier_id')
                ->references('id')
                ->on('shipping_carriers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('shipping_method_id')
                ->references('id')
                ->on('shipping_methods')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->decimal('price', 12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {            
            $table->dropForeign('packages_shipping_carrier_id_foreign');
            $table->dropColumn('shipping_carrier_id');   

            $table->dropForeign('packages_shipping_method_id_foreign');
            $table->dropColumn('shipping_method_id');
            
            $table->dropColumn('price');
        });
    }
}
