<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuantityColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('quantity_backordered')->nullable();
            $table->boolean('is_kit')->nullable();
            $table->integer('quantity_on_po')->nullable();
            $table->integer('quantity_sell_ahead')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('quantity_backordered');
            $table->dropColumn('is_kit');
            $table->dropColumn('quantity_on_po');
            $table->dropColumn('quantity_sell_ahead');
        });
    }
}


