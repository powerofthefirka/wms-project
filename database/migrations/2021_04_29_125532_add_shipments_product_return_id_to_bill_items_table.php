<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShipmentsProductReturnIdToBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_items', function (Blueprint $table) {

            $table->unsignedInteger('purchase_order_item_id')->nullable();
            $table->foreign('purchase_order_item_id')
                ->references('id')
                ->on('purchase_order_items');

            $table->unsignedInteger('purchase_order_id')->nullable();
            $table->foreign('purchase_order_id')
                ->references('id')
                ->on('purchase_orders');

            $table->unsignedInteger('return_item_id')->nullable();
            $table->foreign('return_item_id')
                ->references('id')
                ->on('return_items');

            $table->unsignedInteger('package_id')->nullable();
            $table->foreign('package_id')
                ->references('id')
                ->on('packages');

            $table->unsignedInteger('package_item_id')->nullable();
            $table->foreign('package_item_id')
                ->references('id')
                ->on('package_items');

            $table->unsignedInteger('location_type_id')->nullable();
            $table->foreign('location_type_id')
                ->references('id')
                ->on('location_types');

            $table->unsignedBigInteger('inventory_change_id')->nullable();
            $table->foreign('inventory_change_id')
                ->references('id')
                ->on('inventory_changes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_items', function (Blueprint $table) {
            $table->dropForeign('bill_items_purchase_order_item_id_foreign');
            $table->dropColumn('purchase_order_item_id');

            $table->dropForeign('bill_items_purchase_order_id_foreign');
            $table->dropColumn('purchase_order_id');

            $table->dropForeign('bill_items_return_item_id_foreign');
            $table->dropColumn('return_item_id');

            $table->dropForeign('bill_items_package_id_foreign');
            $table->dropColumn('package_id');

            $table->dropForeign('bill_items_package_item_id_foreign');
            $table->dropColumn('package_item_id');

            $table->dropForeign('bill_items_location_type_id_foreign');
            $table->dropColumn('location_type_id');

            $table->dropForeign('bill_items_inventory_change_id_foreign');
            $table->dropColumn('inventory_change_id');
        });
    }
}
