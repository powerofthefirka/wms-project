<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToBillingShippingAddressesToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('shipping_contact_information_id')->nullable()->unsigned()->change();
            $table->integer('billing_contact_information_id')->nullable()->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_shipping_contact_information_id_foreign');
            $table->dropForeign('orders_billing_contact_information_id_foreign');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('shipping_contact_information_id')->nullable(false)->unsigned()->change();
            $table->integer('billing_contact_information_id')->nullable(false)->unsigned()->change();

            $table->foreign('shipping_contact_information_id')
                ->references('id')
                ->on('contact_informations')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('billing_contact_information_id')
                ->references('id')
                ->on('contact_informations')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }
}
