<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_carrier_id');
            $table->string("name");
            $table->string("wms_id");
            $table->decimal('cost', 12)->default(0);
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('shipping_carrier_id')
                ->references('id')
                ->on('shipping_carriers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_methods');
    }
}
