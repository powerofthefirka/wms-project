<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Customer;
use App\Models\LocationType;
use App\Models\Location;

class Add3pldIdToLocationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_types', function (Blueprint $table) {
            $table->unsignedInteger('3pl_id')->nullable();

            $table->foreign('3pl_id')
                ->references('id')
                ->on('3pls')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->updateLocationsAndTypes();
        
        Schema::table('location_types', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            
            $table->dropColumn('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_types', function (Blueprint $table) {
            $table->dropForeign(['3pl_id']);

            $table->dropColumn('3pl_id');
        });

        Schema::table('location_types', function (Blueprint $table) {
            $table->unsignedInteger('customer_id')->nullable();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function updateLocationsAndTypes()
    {
        $customerIds = Customer::get()->pluck('id');

        foreach (LocationType::whereIn('customer_id', $customerIds)->get() as $locationType) {
            $locationType->update(['name' => trim($locationType->name), 'wms_id' => trim($locationType->wms_id), '3pl_id' => $locationType->customer->threePl->id]);          
        }
        
        $typesGroupBy3pl = LocationType::whereIn('customer_id', $customerIds)->groupBy('3pl_id')->get();
        
        foreach ($typesGroupBy3pl as $key => $typeGroupBy3pl) {
            $types = LocationType::where('3pl_id', $typeGroupBy3pl->threePl->id)->get();
            $distinctTypes = LocationType::where('3pl_id', $typeGroupBy3pl->threePl->id)->groupBy('wms_id')->get();

            $locations = Location::whereIn('location_type_id', $types->pluck('id'))->get();
            
            foreach ($locations as $location) {    
                $location->update(['name' => trim($location->name)]);  

                $distinctType = $distinctTypes->where('wms_id', $location->locationType->wms_id)->first();
                
                try {
                    $location->update(['location_type_id' => $distinctType->id]);
                } catch (\Throwable $th) {
                    $distinctType = LocationType::whereIn('id', $distinctTypes->pluck('id'))->whereRaw('LOWER(`wms_id`) LIKE ? ', [trim(strtolower($location->locationType->wms_id)).'%'])->first();
                    $location->update(['location_type_id' => $distinctType->id]);
                }
            }

            foreach ($distinctTypes as $distinctType) {
                $distinctLocations = Location::where('location_type_id', $distinctType->id)->groupBy('name')->get();
                
                Location::where('location_type_id', $distinctType->id)->whereNotIn('id', $distinctLocations->pluck('id'))->delete();
            }

            LocationType::where('3pl_id', $typeGroupBy3pl->threePl->id)->whereNotIn('id', $distinctTypes->pluck('id'))->delete();
        }
    }
}
