<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->decimal('monthly_price_for_yearly_billing_period', 12, 2)->nullable();
            $table->decimal('monthly_price_for_monthly_billing_period', 12, 2)->nullable();
            $table->decimal('initial_fee', 12, 2)->nullable();
            $table->decimal('three_pl_billing_fee', 12, 2)->nullable();
            $table->integer('number_of_monthly_orders')->nullable();
            $table->decimal('price_per_additional_order', 12, 2)->nullable();
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_plans');
    }
}
