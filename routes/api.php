<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api-log'], 'as' => 'api.'], function() {
	Route::get('user/{user}/customers', 'Api\UserController@customers')->name('user.customers');
	Route::get('user/{user}/webhooks', 'Api\UserController@webhooks')->name('user.webhooks');
	Route::get('user/access_tokens','Api\UserController@getTokens')->name('user.access_tokens');
	Route::put('user/access_tokens','Api\UserController@accessTokens')->name('user.update_access_tokens');
	Route::delete('user/access_tokens/{token}', 'Api\UserController@deleteAccessToken')->name('user.delete_access_tokens');
	Route::apiResource('user', 'Api\UserController');

    Route::post('supplier/{supplier}/products', 'Api\SupplierController@products');
	Route::apiResource('supplier', 'Api\SupplierController');

	Route::get('order/filter', 'Api\OrderController@filter')->name('order.filter');

	Route::post('tasks/picking_batches', 'Api\BatchPickingController@pickingBatches')->name('picking_batches');
	Route::post('picking_batches/{picking_batch}/pick', 'Api\BatchPickingController@pick');

	Route::apiResource('order', 'Api\OrderController');

	Route::get('customer/{customer}/webshipper/carriers', 'Api\CustomerController@webshipperCarriers')->name('webshipper.carriers');
	Route::post('order/{order}/ship', 'Api\OrderController@ship')->name('order.ship');

    Route::get('order/{order}/history', 'Api\OrderController@history')->name('order.history');
    Route::get('order_item/{order_item}/history', 'Api\OrderController@itemHistory')->name('order.itemHistory');

	Route::get('purchase_order/filter', 'Api\PurchaseOrderController@filter')->name('purchase_order.filter');
	Route::apiResource('purchase_order', 'Api\PurchaseOrderController');
	Route::post('purchase_order/{purchase_order}/receive', 'Api\PurchaseOrderController@receive')->name('purchase_order.receive');

    Route::get('purchase_order/{purchase_order}/history', 'Api\PurchaseOrderController@history')->name('purchase_order.history');
    Route::get('purchase_order_item/{purchase_order_item}/history', 'Api\PurchaseOrderController@itemHistory')->name('purchase_order.itemHistory');

	Route::get('return/filter', 'Api\ReturnController@filter')->name('return.filter');
	Route::apiResource('return', 'Api\ReturnController');
	Route::post('return/{return}/receive', 'Api\ReturnController@receive')->name('return.receive');

    Route::get('return/{return}/history', 'Api\ReturnController@history')->name('return.history');
    Route::get('return_item/{return_item}/history', 'Api\ReturnController@itemHistory')->name('return.itemHistory');

	Route::apiResource('webhook', 'Api\WebhookController');

	Route::apiResource('inventory_log', 'Api\InventoryLogController');

	Route::get('product/filter', 'Api\ProductController@filter')->name('product.filter');

	Route::apiResource('product', 'Api\ProductController');

    Route::get('product/{product}/history', 'Api\ProductController@history')->name('product.history');

    Route::apiResource('warehouse', 'Api\WarehouseController');

    Route::apiResource('location', 'Api\LocationController');

    Route::apiResource('location_types', 'Api\LocationTypeController');

    Route::get('customer/{customer}/warehouses', 'Api\CustomerController@warehouses')->name('customer.warehouses');
	Route::get('customer/{customer}/users', 'Api\CustomerController@users')->name('customer.users');
	Route::get('customer/{customer}/tasks', 'Api\CustomerController@tasks')->name('customer.tasks');
	Route::get('customer/{customer}/products', 'Api\CustomerController@products')->name('customer.products');
	Route::get('customer/{customer}/user', 'Api\CustomerController@listUsers')->name('customer.list_users');
	Route::put('customer/{customer}/user', 'Api\CustomerController@updateUsers')->name('customer.update_users');
	Route::delete('customer/{customer}/user/{user}', 'Api\CustomerController@detachUser')->name('customer.detach_user');
	Route::apiResource('customer', 'Api\CustomerController');

	Route::apiResource('webshipper_credential', 'Api\WebshipperCredentialController');

	Route::apiResource('customer_shipping_carriers_map', 'Api\CustomerShippingCarriersMapController');

	Route::apiResource('customer_shipping_methods_map', 'Api\CustomerShippingMethodsMapController');

	Route::apiResource('billing_profile', 'Api\BillingProfileController');

	Route::apiResource('invoice', 'Api\InvoiceController');

	Route::apiResource('shipping_carrier', 'Api\ShippingCarrierController');
	Route::apiResource('shipping_method', 'Api\ShippingMethodController');

	Route::apiResource('inventory_changes', 'Api\InventoryChangeController');
});
