<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Arr;

Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => false]);

Route::get('dashboard', 'HomeController@index')->name('home');
Route::get('pricing', 'PageController@pricing')->name('page.pricing');
Route::get('lock', 'PageController@lock')->name('page.lock');
Route::view('/terms-privacy', 'auth.privacy');
Route::group(['middleware' => ['auth', 'session']], function () {
    Route::get('coordinates', function () {
        $o = Order::whereNull('shipping_lat')->get(); foreach ($o as $order) { $order->getMapCoordinates(); };

        return 'done';
    });

    Route::post('data/export', 'HomeController@export')->name('data.export');

    Route::get('dashboard', 'HomeController@index')->name('home');
    Route::get('dashboard/orders_revenue', 'HomeController@totalRevenue')->name('dashboard.orders_revenue');
    Route::get('dashboard/purchase_orders_received', 'HomeController@purchaseOrdersReceived')->name('dashboard.purchase_orders_received');
    Route::get('dashboard/late_orders', 'HomeController@lateOrders')->name('dashboard.late_orders');
    Route::get('global-search', 'HomeController@globalSearch')->name('global.search');
    Route::resource('roles', 'RoleController', ['except' => ['show', 'destroy']]);
    Route::get('users/data-table', 'UserController@dataTable')->name('users.dataTable');
    Route::resource('users', 'UserController', ['except' => ['show']]);

    Route::post('user-settings/hide-columns', 'UserSettingController@hideColumns')->name('user_settings.hide_columns');
    Route::post('user-settings/dashboard_settings', 'UserSettingController@dashboardSettings')->name('user_settings.dashboard_settings');

    Route::get('profiles', ['as' => 'profiles.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profiles', ['as' => 'profiles.update', 'uses' => 'ProfileController@update']);
    Route::put('profiles/password', ['as' => 'profiles.password', 'uses' => 'ProfileController@password']);
    Route::put('profiles/theme', ['as' => 'profiles.theme', 'uses' => 'ProfileController@theme']);
    Route::put('profiles/access_tokens', ['as' => 'profiles.access_tokens', 'uses' => 'ProfileController@accessTokens']);
    Route::put('profiles/email_notifications', 'ProfileController@emailNotifications')->name('profiles.email_notifications');
    Route::delete('profiles/access_tokens/{token}', ['as' => 'profiles.delete_token', 'uses' => 'ProfileController@destroy']);

    Route::get('customers/data_table', 'CustomerController@dataTable')->name('customers.data_table');
    Route::get('customers/filter_3pls', 'CustomerController@filter3Pls')->name('customers.filter_3pls');
    Route::get('customers/filter_currencies', 'CustomerController@filterCurrencies')->name('customers.filter_currencies');
    Route::delete('customers/{customer}/detach_user/{user}', 'CustomerController@detachUser')->name('customers.detach_user');
    Route::get('customers/{customer}/users', 'CustomerController@edit')->name('customers.edit_users');
    Route::get('customers/{customer}/users/create', 'CustomerController@edit')->name('customers.create_user');
    Route::post('customers/{customer}/users/store', 'CustomerController@storeUser')->name('customers.store_user');
    Route::get('customers/{customer}/integration_credentials', 'CustomerController@editShiphero')->name('customers.edit_shiphero_credentials');
    Route::post('customers/{customer}/users/update', 'CustomerController@updateUsers')->name('customers.update_users');
    Route::post('customers/{customer}/integrations_credentials/update', 'CustomerController@updateShipheroCredentials')->name('customers.update_shiphero_credentials');
    Route::get('customers/{customer}/toggle_integration', 'CustomerController@toggleIntegrationStatus')->name('customers.toggle_integration_status');
    Route::get('customers/{customer}/filter_users', 'CustomerController@filterUsers')->name('customers.filter_users');
    Route::get('customers/{customer}/shipping_carriers', 'CustomerController@shippingCarriers')->name('customers.shipping_carriers');
    Route::get('customer-currency-unit/{customer}/', 'CustomerController@getCurrencyAndUnit')->name('customer_currency_unit');
    Route::get('customers/{customer}/messenger_credentials', 'CustomerController@editMessenger')->name('customers.edit_messenger_credentials');
    Route::post('customers/{customer}/messenger_credentials/update', 'CustomerController@updateMessengerCredentials')->name('customers.update_messenger_credentials');
    Route::get('customers/disable/{customer}', 'CustomerController@disable')->name('customers.disable');
    Route::get('customers/enable/{customer}', 'CustomerController@enable')->name('customers.enable');
    Route::get('customers/settings', 'CustomerController@settings')->name('customers.settings');
    Route::post('customers/settings', 'CustomerController@updateSettings')->name('customers.update_settings');
    Route::resource('customers', 'CustomerController');

    Route::get('products/data_table', 'ProductController@dataTable')->name('products.data_table');
    Route::get('products/{product}/preview', 'ProductController@preview')->name('products.preview');
    Route::get('products/{product}/editable_preview', 'ProductController@editablePreview')->name('products.editable_preview');
    Route::get('product/delete_product_image', 'ProductController@deleteProductImage')->name('products.delete_product_image');
    Route::get('products/filter_customers', 'ProductController@filterCustomers')->name('products.filter_customers');
    Route::get('products/export', 'ProductController@export')->name('products.export');
    Route::post('products/import', 'ProductController@import')->name('products.import');
    Route::get('products/change_log', 'ProductController@changeLog')->name('products.change_log');
    Route::get('products/log_data_table/{product_id?}', 'ProductController@changeLogDataTable')->name('products.log_data_table');
    Route::resource('products', 'ProductController');

    Route::get('orders/filter_customers', 'OrderController@filterCustomers')->name('orders.filter_customers');
    Route::get('orders/filter_products/{customer}', 'OrderController@filterProducts')->name('orders.filter_products');
    Route::get('orders/data_table', 'OrderController@dataTable')->name('orders.data_table');
    Route::get('orders/dashboard-data-table', 'OrderController@dashboardDataTable')->name('orders.dashboard_data_table');
    Route::get('orders/orders_by_cities', 'HomeController@ordersByCities');
    Route::get('orders/orders_by_cities_limited', 'HomeController@ordersByCities');
    Route::get('orders/orders_by_country', 'HomeController@ordersByCountry');
    Route::get('orders/orders_received_count', 'HomeController@ordersReceivedCalc');
    Route::get('orders/orders_shipped_count', 'HomeController@shipmentsCalc');

    Route::get('orders/{order}/carrier/{carrier}', 'OrderController@carrier');
    Route::get('orders/{order}/carriers', 'OrderController@carriers');
    Route::get('orders/{order}/preview', 'OrderController@preview')->name('orders.preview');
    Route::get('orders/{order}/editable_preview', 'OrderController@editablePreview')->name('orders.editable_preview');
    Route::get('orders/export', 'OrderController@export')->name('orders.export');
    Route::post('orders/import', 'OrderController@import')->name('orders.import');
    Route::get('orders/shipping_methods/{customer}', 'OrderController@shippingMethods')->name('orders.shipping_methods');
    Route::get('orders/total_orders_by_statuses', 'OrderController@totalOrdersByStatuses');
    Route::post('orders/{order}/cancel', 'OrderController@cancel')->name('orders.cancel');
    Route::post('orders/{order}/reship', 'OrderController@reship')->name('orders.reship');
    Route::get('orders/change_log', 'OrderController@changeLog')->name('orders.change_log');
    Route::get('orders/log_data_table/{order_id?}', 'OrderController@changeLogDataTable')->name('orders.log_data_table');
    Route::resource('orders', 'OrderController');

    Route::get('purchase_orders/data_table', 'PurchaseOrderController@dataTable')->name('purchase_orders.data_table');
    Route::get('purchase_orders/filter_products/{supplier}', 'PurchaseOrderController@filterProducts')->name('purchase_orders.filter_products');
    Route::get('purchase_orders/filter_customers', 'PurchaseOrderController@filterCustomers')->name('purchase_orders.filter_customers');
    Route::get('purchase_orders/filter_locations', 'PurchaseOrderController@filterLocations')->name('purchase_orders.filter_locations');
    Route::get('purchase_orders/filter_warehouses/{customer}', 'PurchaseOrderController@filterWarehouses')->name('purchase_orders.filter_warehouses');
    Route::get('purchase_orders/filter_suppliers/{customer}', 'PurchaseOrderController@filterSuppliers')->name('purchase_orders.filter_suppliers');
    Route::get('purchase_orders/{purchase_order}/preview', 'PurchaseOrderController@preview')->name('purchase_orders.preview');
    Route::get('purchase_orders/{purchase_order}/editable_preview', 'PurchaseOrderController@editablePreview')->name('purchase_orders.editable_preview');
    Route::get('purchase_orders/export', 'PurchaseOrderController@export')->name('purchase_orders.export');
    Route::post('purchase_orders/import', 'PurchaseOrderController@import')->name('purchase_orders.import');
    Route::get('purchase_orders/coming_in', 'HomeController@purchaseOrdersCalc');
    Route::get('purchase_orders/quantity_calc', 'HomeController@purchaseOrdersQuantityCalc');
    Route::get('purchase_orders/total_purchase_orders_by_statuses', 'PurchaseOrderController@totalPurchaseOrdersByStatuses');
    Route::post('purchase_orders/{purchase_order}/cancel', 'PurchaseOrderController@cancel')->name('purchase_orders.cancel');
    Route::get('purchase_orders/auto_pending/{supplier}', 'PurchaseOrderController@autoPending')->name('purchase_orders.auto_pending');
    Route::get('purchase_orders/auto_all/{supplier}', 'PurchaseOrderController@autoAll')->name('purchase_orders.auto_all');
    Route::get('purchase_orders/auto_backordered/{supplier}', 'PurchaseOrderController@autoBackordered')->name('purchase_orders.auto_backordered');
    Route::get('purchase_orders/change_log', 'PurchaseOrderController@changeLog')->name('purchase_orders.change_log');
    Route::get('purchase_orders/log_data_table/{purchase_order_id?}', 'PurchaseOrderController@changeLogDataTable')->name('purchase_orders.log_data_table');

    Route::resource('purchase_orders', 'PurchaseOrderController');

    Route::get('returns/data_table', 'ReturnController@dataTable')->name('returns.data_table');
    Route::get('returns/filter_order_products/{order_id}', 'ReturnController@filterOrderProducts')->name('returns.filter_order_products');
    Route::get('returns/get_order_products/{order_id}', 'ReturnController@getOrderProducts')->name('returns.get_order_products');
    Route::get('returns/filter_orders/{customer}', 'ReturnController@filterOrders')->name('returns.filter_orders');
    Route::get('returns/filter_locations', 'ReturnController@filterLocations')->name('returns.filter_locations');
    Route::get('returns/{return}/preview', 'ReturnController@preview')->name('returns.preview');
    Route::get('returns/{return}/editable_preview', 'ReturnController@editablePreview')->name('returns.editable_preview');
    Route::get('returns/returns-count', 'HomeController@returnsCalc');
    Route::get('returns/total_returns_by_statuses', 'ReturnController@totalReturnsByStatuses');

    Route::resource('returns', 'ReturnController');

    Route::get('warehouses/data_table', 'WarehouseController@dataTable')->name('warehouse.data_table');
    Route::get('warehouses/{warehouse}/edit/location', 'WarehouseController@edit')->name('warehouses.edit_warehouse_location');
    Route::post('warehouses/{warehouse}/add_users', 'WarehouseController@addCustomers')->name('warehouse.add_customers');
    Route::get('warehouses/filter_customers', 'WarehouseController@filterCustomers')->name('warehouses.filter_customers');
    Route::get('warehouse-by-customer/{customer}', 'WarehouseController@getWarehouseByCustomer')->name('warehouse_by_customer');
    Route::resource('warehouses', 'WarehouseController', ['except' => ['show']]);

    Route::get('warehouses/{warehouse}/edit/location/create', 'LocationController@create')->name('warehouse_location.create');
    Route::post('warehouses/{warehouse}/edit/location/store', 'LocationController@store')->name('warehouse_location.store');
    Route::delete('warehouses/{warehouse}/edit/warehouse/location/{location}', 'LocationController@destroy')->name('warehouse_location.destroy');
    Route::get('warehouses/{warehouse}/edit/location/{location}', 'LocationController@edit')->name('warehouse_location.edit');
    Route::post('warehouses/{warehouse}/edit/location/{location}/update', 'LocationController@update')->name('warehouse_location.update');

    //    Session customer
    Route::get('forget', 'Controller@forgetCustomer')->name('controller.forget_customer');
    Route::get('customer/set/{id}', 'Controller@sessionSetCustomer')->name('controller.session_set_customer');

    Route::get('vendor-by-customer/{customer}', 'SupplierController@getVendorByCustomer')->name('vendor_by_customer');
    Route::get('vendors/data_table', 'SupplierController@dataTable')->name('vendors.data_table');
    Route::get('vendors/filter_customers', 'SupplierController@filterCustomers')->name('vendors.filter_customers');
    Route::resource('vendors', 'SupplierController');

    Route::get('profiles/webhook/filter_customers', 'WebhookController@filterCustomers')->name('webhook.filter_customers');
    Route::get('profiles/webhook/filter_users', 'WebhookController@filterUsers')->name('webhook.filter_users');
    Route::resource('profiles/webhook', 'WebhookController');

    Route::get('locations/data_table', 'LocationController@dataTable')->name('customer.data_table');
    Route::get('locations/{warehouse}', 'LocationController@index')->name('locations.index');
    Route::get('locations/filter_locations', 'LocationController@filterLocations')->name('locations.filter_locations');
    Route::get('locations/filter_products/{location?}', 'LocationController@filterProducts')->name('locations.filter_products');
    Route::post('locations/transfer/product/{location}', 'LocationController@transfer')->name('locations.transfer');
    Route::resource('locations', 'LocationController')->except(['create', 'store']);

    Route::get('inventory_logs/data_table', 'InventoryLogController@dataTable')->name('inventory_log.data_table');
    Route::resource('inventory_logs', 'InventoryLogController');

    Route::get('shipments/data_table', 'ShipmentController@dataTable')->name('shipment.data_table');
    Route::get('shipments/{shipment}/items_shipped', 'ShipmentController@itemsShipped')->name('shipments.items_shipped');
    Route::resource('shipments', 'ShipmentController')->except(['create', 'store', 'edit', 'update', 'destroy']);

    Route::get('webshipper_credential/data_table', 'WebshipperCredentialController@dataTable');
    Route::get('webshipper_credential/filterCustomers', 'WebshipperCredentialController@filterCustomers')->name('webshipper_credential.filterCustomers');
    Route::resource('webshipper_credential', 'WebshipperCredentialController');

    Route::get('shipping_carrier_mapping/data_table', 'CustomerShippingCarriersMapController@dataTable');
    Route::get('shipping_carrier_mapping/filter_customers', 'CustomerShippingCarriersMapController@filterCustomers')->name('shipping_carrier_mapping.filter_customers');
    Route::get('shipping_carrier_mapping/{customer}/carriers', 'CustomerShippingCarriersMapController@carriers');
    Route::get('shipping_carrier_mapping/{customer}/{service}/mapped_carriers', 'CustomerShippingCarriersMapController@mappedCarriers');
    Route::resource('shipping_carrier_mapping', 'CustomerShippingCarriersMapController');

    Route::get('shipping_method_mapping/data_table', 'CustomerShippingMethodsMapController@dataTable');
    Route::get('shipping_method_mapping/filter_customers', 'CustomerShippingMethodsMapController@filterCustomers')->name('shipping_method_mapping.filter_customers');
    Route::get('shipping_method_mapping/{customer}/carrier/{carrier}', 'CustomerShippingMethodsMapController@carrier');
    Route::resource('shipping_method_mapping', 'CustomerShippingMethodsMapController');

    Route::get('shipping_boxes/data_table', 'ShippingBoxController@dataTable')->name('shipping_boxes.data_table');
    Route::get('shipping_boxes/{shipping_box}/preview', 'ShippingBoxController@preview')->name('shipping_boxes.preview');
    Route::get('shipping_boxes/{shipping_box}/editable_preview', 'ShippingBoxController@editablePreview')->name('shipping_boxes.editable_preview');
    Route::resource('shipping_boxes', 'ShippingBoxController');

    Route::get('location_types/data_table/{three_pl?}', 'LocationTypeController@dataTable')->name('location_types.data_table');
    Route::get('location_types/{location_type}/preview', 'LocationTypeController@preview')->name('location_types.preview');
    Route::get('location_types/{location_type}/editable_preview', 'LocationTypeController@editablePreview')->name('location_types.editable_preview');
    Route::get('location_types/filter_3pls', 'LocationTypeController@filter3Pls')->name('location_types.filter_3pls');
    Route::resource('location_types', 'LocationTypeController');

    Route::get('shipping_carriers/{shipping_carrier}/preview', 'ShippingCarrierController@preview')->name('shipping_carriers.preview');
    Route::get('shipping_carriers/{shipping_carrier}/editable_preview', 'ShippingCarrierController@editablePreview')->name('shipping_carriers.editable_preview');
    Route::get('shipping_carriers/data_table', 'ShippingCarrierController@dataTable')->name('shipping_carriers.data_table');
    Route::get('shipping_carriers/{shipping_carrier}/shipping_methods', 'ShippingCarrierController@shippingMethods')->name('shipping_carriers.shipping_methods');
    Route::get('shipping_carriers/filter_3pls', 'ShippingCarrierController@filter3Pls')->name('shipping_carriers.filter_3pls');

    Route::resource('shipping_carriers', 'ShippingCarrierController');

    Route::get('shipping_methods/{shipping_method}/preview', 'ShippingMethodController@preview')->name('shipping_methods.preview');
    Route::get('shipping_methods/{shipping_method}/editable_preview', 'ShippingMethodController@editablePreview')->name('shipping_methods.editable_preview');
    Route::get('shipping_methods/data_table', 'ShippingMethodController@dataTable')->name('shipping_method.data_table');
    Route::get('shipping_methods/get_carriers/{three_pl?}', 'ShippingMethodController@getShippingCarriers')->name('shipping_method.get_carriers');
    Route::resource('shipping_methods', 'ShippingMethodController');

    Route::get('api_logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('billing_profiles/data_table', 'BillingProfileController@dataTable')->name('billing_profiles.data_table');
    Route::resource('billing_profiles', 'BillingProfileController');
    Route::post('billing_profiles/{id}/clone', 'BillingProfileController@clone')->name('billing_profiles.clone');

    Route::get('billings', 'BillingController@index')->name('billings.index');
    Route::get('billings/customers', 'BillingController@customers')->name('billings.customers');
    Route::get('billings/customers/{customer}/bills', 'BillingController@customerBillsEdit')->name('billings.customer_bills');
    Route::get('billings/customers/{customer}/bills/data_table', 'BillingController@customerBillsDataTable');
    Route::get('billings/customers/{customer}/bills/{bill}/items', 'BillingController@customerBillItems')->name('billings.customer_bill_items');

    Route::get('billings/customers/bills/{bill}/items/unbilled', 'BillingController@unbilled')->name('billings.unbilled');
    Route::get('bills/{bill}/unbilled_shipments', 'BillingController@unBilledShipments');
    Route::get('bills/{bill}/unbilled_purchase_orders', 'BillingController@unBilledPurchaseOrders');
    Route::get('bills/{bill}/unbilled_purchase_order_items', 'BillingController@unBilledPurchaseOrderItems');
    Route::get('bills/{bill}/unbilled_locations', 'BillingController@unBilledLocations');
    Route::get('bills/{bill}/unbilled_packages', 'BillingController@unBilledPackages');
    Route::get('bills/{bill}/unbilled_package_items', 'BillingController@unBilledPackageItems');
    Route::get('bills/{bill}/unbilled_inventory_changes', 'BillingController@unBilledInventoryChanges');
    Route::get('bills/{bill}/return_items', 'BillingController@unBilledReturnItems');

    Route::get('billings/bills', 'BillingController@bills')->name('billings.bills');
    Route::get('billings/bills/data_table', 'BillingController@billsDataTable');

    Route::get('billings/billing_profiles', 'BillingController@billingProfiles')->name('billings.billing_profiles');
    Route::get('billings/reconcile', 'BillingController@reconcile')->name('billings.reconcile');
    Route::get('billings/exports', 'BillingController@exports')->name('billings.exports');
    Route::get('billings/product_profiles', 'BillingController@productProfiles')->name('billings.product_profiles');

    Route::get('invoices/data_table', 'InvoiceController@dataTable')->name('invoices.data_table');
    Route::get('invoices/invoice_statuses', 'InvoiceController@invoiceStatuses')->name('invoices.invoice_statuses');
    Route::get('invoices/{direct_url}', 'InvoiceController@view')->name('invoice.direct_url');
    Route::get('invoices/{invoice}/print_preview', 'InvoiceController@print_preview')->name('invoices.print_preview');
    Route::get('invoices/{invoice}/preview', 'InvoiceController@preview')->name('invoices.preview');
    Route::get('invoices/{invoice}/editable_preview', 'InvoiceController@editablePreview')->name('invoices.editable_preview');
    Route::resource('invoices', 'InvoiceController');

    Route::get('invoice_statuses/filter_customers', 'InvoiceStatusController@filterCustomers')->name('invoice_statuses.filter_customers');
    Route::resource('invoice_statuses', 'InvoiceStatusController');

    Route::get('product_profiles/data_table', 'ProductProfileController@dataTable');
    Route::get('product_profiles/product_data_table/{product_profile}', 'ProductProfileController@productDataTable');
    Route::get('product_profiles/{product_profile}/product_list', 'ProductProfileController@productList');
    Route::post('product_profiles/{product_profile}/save_product_selection', 'ProductProfileController@saveProductSelection');
    Route::resource('product_profiles', 'ProductProfileController');

    Route::get('billing_fees/data_table', 'BillingFeeController@dataTable');
    Route::get('billing_fees', 'BillingFeeController@index');
    Route::get('billing_fees/billing_profile/{billing_profile}/add_fee/{type}', 'BillingFeeController@create')->name('billing_fees.create');
    Route::get('billing_fees/billing_profile/{billing_profile}/billing_fee/{billing_fee}/edit', 'BillingFeeController@edit')->name('billing_fees.edit');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/recurring', 'BillingFeeController@recurringStore')->name('billing_fees.recurring.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/recurring', 'BillingFeeController@recurringUpdate')->name('billing_fees.recurring.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/receiving_by_hour', 'BillingFeeController@receivingByHourStore')->name('billing_fees.receiving_by_hour.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/receiving_by_hour', 'BillingFeeController@receivingByHourUpdate')->name('billing_fees.receiving_by_hour.update');


    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/receiving_by_po', 'BillingFeeController@receivingByPoStore')->name('billing_fees.receiving_by_po.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/receiving_by_po', 'BillingFeeController@receivingByPoUpdate')->name('billing_fees.receiving_by_po.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/receiving_by_item', 'BillingFeeController@receivingByItemStore')->name('billing_fees.receiving_by_item.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/receiving_by_item', 'BillingFeeController@receivingByItemUpdate')->name('billing_fees.receiving_by_item.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/receiving_by_line', 'BillingFeeController@receivingByLineStore')->name('billing_fees.receiving_by_line.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/receiving_by_line', 'BillingFeeController@receivingByLineUpdate')->name('billing_fees.receiving_by_line.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/storage_by_location', 'BillingFeeController@storageByLocationStore')->name('billing_fees.storage_by_location.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/storage_by_location', 'BillingFeeController@storageByLocationUpdate')->name('billing_fees.storage_by_location.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/storage_by_product', 'BillingFeeController@storageByProductStore')->name('billing_fees.storage_by_product.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/storage_by_product', 'BillingFeeController@storageByProductUpdate')->name('billing_fees.storage_by_product.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/shipments_by_box', 'BillingFeeController@shipmentByBoxtStore')->name('billing_fees.shipments_by_box.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/shipments_by_box', 'BillingFeeController@shipmentByBoxUpdate')->name('billing_fees.shipments_by_box.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/shipments_by_shipping_label', 'BillingFeeController@shipmentsByShippingLabelStore')->name('billing_fees.shipments_by_shipping_label.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/shipments_by_shipping_label', 'BillingFeeController@shipmentsByShippingLabelUpdate')->name('billing_fees.shipments_by_shipping_label.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/shipments_by_picking_fee', 'BillingFeeController@shipmentsByPickingFeeStore')->name('billing_fees.shipments_by_picking_fee.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/shipments_by_picking_fee', 'BillingFeeController@shipmentsByPickingFeeUpdate')->name('billing_fees.shipments_by_picking_fee.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/shipments_by_pickup_picking_fee', 'BillingFeeController@shipmentsByPickupPickingFeeStore')->name('billing_fees.shipments_by_pickup_picking_fee.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/shipments_by_pickup_picking_fee', 'BillingFeeController@shipmentsByPickupPickingFeeUpdate')->name('billing_fees.shipments_by_pickup_picking_fee.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/returns', 'BillingFeeController@returnsStore')->name('billing_fees.returns.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/returns', 'BillingFeeController@returnsUpdate')->name('billing_fees.returns.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/ad_hoc', 'BillingFeeController@adHocStore')->name('billing_fees.ad_hoc.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/ad_hoc', 'BillingFeeController@adHocUpdate')->name('billing_fees.ad_hoc.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/add_fee/shipping_rates', 'BillingFeeController@shippingRatesStore')->name('billing_fees.shipping_rates.store');
    Route::post('billing_fees/billing_profile/{billing_profile}/update_fee/{billing_fee}/shipping_rates', 'BillingFeeController@shippingRatesUpdate')->name('billing_fees.shipping_rates.update');

    Route::post('billing_fees/billing_profile/{billing_profile}/import/{type}', 'BillingFeeController@import')->name('billing_fees.import');
    Route::get('billing_fees/billing_profile/{billing_profile}/export/{type}', 'BillingFeeController@export')->name('billing_fees.export');

    Route::delete('billing_fees/destroy/{billing_fee}/billing_profile/{billing_profile}/', 'BillingFeeController@destroy')->name('billing_fees.destroy');
    Route::get('billing_fees/carriers_and_methods', 'BillingFeeController@carriersAndMethods');
    Route::get('billing_fees/{billing_fee}/carrier/{shipping_carrier}', 'BillingFeeController@getCarrierMethods');

    Route::get('bills/{bill}/export_csv', 'BillController@exportToCsv');
    Route::post('bills/{bill}/ad_hoc', 'BillController@adHoc')->name('bills.ad_hoc');
    Route::post('bills/{bill}/recalculate', 'BillController@recalculate')->name('bills.recalculate');
    Route::resource('bills', 'BillController');

    Route::get('three_pls/data_table', 'ThreePlController@dataTable')->name('three_pls.data_table');
    Route::get('three_pls/{three_pl}/users', 'ThreePlController@edit')->name('three_pls.edit_users');
    Route::delete('three_pls/{three_pl}/detach_user/{user}', 'ThreePlController@detachUser')->name('three_pls.detach_user');
    Route::post('three_pls/{three_pl}/users/update', 'ThreePlController@updateUsers')->name('three_pls.update_users');
    Route::get('three_pls/{three_pl}/filter_users', 'ThreePlController@filterUsers')->name('three_pls.filter_users');
    Route::get('three_pls/{three_pl}/pricing_plan', 'ThreePlController@edit')->name('three_pls.edit_pricing_plan');
    Route::put('three_pls/{three_pl}/pricing_plan', 'ThreePlController@updatePricingPlan')->name('three_pls.update_pricing_plan');
    Route::get('three_pls/{three_pl}/pricing_plan/{name}', 'ThreePlController@getPricingPlan')->name('three_pls.get_pricing_plan');
    Route::get('three_pls/{three_pl}/pricing_plan/{name}/{billing_period}', 'ThreePlController@getMonthlyPrice')->name('three_pls.get_monthly_price');
    Route::get('three_pls/pricing_plans/data_table', 'PricingPlanController@dataTable')->name('pricing_plans.data_table');
    Route::resource('three_pls/pricing_plans', 'PricingPlanController');
    Route::get('three_pls/{three_pl}/billing_report', 'ThreePlController@edit')->name('three_pls.billing_report');
    Route::get('three_pls/{three_pl}/billing_report_data_table', 'ThreePlController@billingReportDataTable')->name('three_pls.billing_report_data_table');
    Route::get('three_pls/{three_pl}/billing_report/orders_price', 'ThreePlController@getBillingReportOrdersPrice')->name('three_pls.billing_report_orders_price');
    Route::get('three_pls/{three_pl}/billing_report/export', 'ThreePlController@exportBillingReportToCsv')->name('three_pls.export_billing_report_to_csv');
    Route::get('three_pls/disable/{three_pl}', 'ThreePlController@disable')->name('three_pls.disable');
    Route::get('three_pls/enable/{three_pl}', 'ThreePlController@enable')->name('three_pls.enable');
    Route::get('three_pls/{three_pl}/all_shipping_methods', 'ThreePlController@getAllShippingMethods')->name('three_pls.all_shipping_methods');
    Route::get('three_pls/{three_pl}/all_locations', 'ThreePlController@getAllLocations')->name('three_pls.all_locations');
    Route::get('three_pls/{three_pl}/domains/data_table', 'DomainController@dataTable')->name('domains.data_table');
    Route::get('three_pls/currencies', 'CurrencyController@index')->name('currencies.index');
    Route::get('three_pls/currencies/data_table', 'CurrencyController@currenciesDataTable');
    Route::get('three_pls/currencies/create', 'CurrencyController@create')->name('currencies.create');
    Route::get('three_pls/currencies/{id}/edit', 'CurrencyController@edit')->name('currencies.edit');
    Route::post('three_pls/currencies/', 'CurrencyController@update')->name('currencies.update');
    Route::put('three_pls/currencies/', 'CurrencyController@store')->name('currencies.store');
    Route::resource('three_pls/{three_pl}/domains', 'DomainController');
    Route::resource('three_pls', 'ThreePlController');

    Route::post('user_widgets/save', 'UserWidgetController@createUpdate')->name('user_widgets.save');
    Route::get('user_widgets/get', 'UserWidgetController@getWidgets')->name('user_widgets.get_widgets');

    Route::get('sync_shipping_methods', function () {
        foreach (\App\Models\ThreePl::all() as $threePl) {
            if ($threePl->shippingCarriers->count() == 0) {
                continue;
            }

            $shippingMethodIds = \App\Models\ShippingMethod::whereIn('shipping_carrier_id', $threePl->shippingCarriers->pluck('id'))->pluck('id');

            foreach ($threePl->customers as $customer) {
                $customer->shippingMethods()->sync($shippingMethodIds);
            }
        }
    });

    Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);
});
