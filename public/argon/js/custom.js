$(function () {
    $(document).on('click', '.toggle-sidebar-button', function () {
        toggleSidebar($(this));
    });

    function toggleSidebar (button) {
        const body = $('body');

        button.toggleClass('active');
        body.toggleClass('sidebar-expanded');
        body.toggleClass('sidebar-expanded-mobile');

        if (body.hasClass('sidebar-expanded')) {
            Cookies.set('sidebar-expanded', 'true');
        } else {
            Cookies.remove('sidebar-expanded');
        }

        recalculateDataTableWidth();
    }

    $(document).on('click', '.sidebar-overlay', function (event) {
        if ($('.expand-global-search').length) {
            $('body').removeClass('expand-global-search');
        } else {
            if ($('.show-preview').length) {
                $('.preview-close-button').trigger('click');
            } else {
                $('.toggle-sidebar-button').trigger('click');
            }
        }
    });

    $(document).on('click', '.nav-link', function (event) {

        const button =  $(this);
        const windowWidth = window.outerWidth;
        const submenu =  button.parent().find('.submenu');

        if (windowWidth < 895 && submenu.length) {
            event.preventDefault();
            $('.nav-link').removeClass('active');
            button.addClass('active');
        }
    });

    $(document).on('click', '.close-global-search-button', function (event) {
        $('body').removeClass('show-global-search').removeClass('expand-global-search');
    });

    $(document).on('click', '.show-global-search-button', function (event) {
        $('body').addClass('show-global-search');
    });

    $('#global-search-input').on('keyup', debounce(function() {
        let input = $('#global-search-input').val();

        $('.global-search').addClass('with-loader with-smaller-loader with-opacity-background');

        $.ajax({
            method:'GET',
            url: '/global-search',
            data: {
                term: input
            }
        })
        .done(function (results) {
            if (!$('expand-global-search').length) {
                $('body').addClass('expand-global-search');
            }

            $('#global-search-results').html(results);
            loading = false;

            $('.global-search').removeClass('with-loader with-smaller-loader with-opacity-background');
        })
    }, 500));

    $(document).on('click', '.show-global-search-button', function (event) {
        $('body').addClass('show-global-search');
    });
});
