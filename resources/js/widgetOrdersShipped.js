window.WidgetOrdersShipped = function () {
    $(document).ready(function () {
        $.ajax({
            url: "/orders/orders_shipped_count",
            context: document.body
        }).done(function(data) {
            $('.orders-shipped-count').html(data)
        });
    })
};
