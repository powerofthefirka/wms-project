window.Unbilled = function () {
    $(document).ready(function() {
        $('#unbilled_shipments').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_shipments',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled shipments",
                        "data": function(data) {
                            let description =  data.tracking_code ? data.tracking_code : 'no -tracking code'
                            let href = '/shipments/' + data.id + '/items_shipped'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by Shipping Rate)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unbilled_pos').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_purchase_orders',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Purchase Orders",
                        "data": function(data) {
                            let description =  data.sku
                            let href = '/purchase_orders/' + data.id + '/edit'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by Receiving By Line or Receiving By Item)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unbilled_po_items').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_purchase_order_items',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Purchase Order Items",
                        "data": function(data) {
                            let description =  data.po_item_name
                            let href = '/purchase_orders/' + data.id + '/edit'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by Shipping Rate)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unBilled_locations').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_locations',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled locations",
                        "data": function(data) {
                            let description =  data.name

                            return description+ ' (can be billed by Storage By Location. If the location type is selected, but not billed, it can mean that there are no inventory changes)'
                        }
                    },
                ],
            }
        );

        $('#unbilled_return_items').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/return_items',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Return Items",
                        "data": function(data) {
                            let description =  data.return_item_name
                            let href = '/returns/' + data.id + '/edit'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by Returns)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unbilled_packages').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_packages',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Packages",
                        "data": function(data) {
                            let description =  data.tracking_number
                            let href = '/shipments/' + data.id + '/items_shipped'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by Shipments By Box)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unbilled_package_items').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_package_items',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Packages Items",
                        "data": function(data) {
                            let description =  data.order_item_sku
                            let href = '/shipments/' + data.id + '/items_shipped'

                            return '<a href="'+href+'" class="btn btn-link btn-sm">'+description+ ' (can be billed by shipments pickup picking fee or shipments picking fee)'+'</a>'
                        }
                    },
                ],
            }
        );

        $('#unbilled_inventory_changes').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/bills/'+ billId +'/unbilled_inventory_changes',
                },
                responsive: true,
                pagingType: "numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columnDefs: [
                    { orderable: false, targets: 0 }
                ],
                preDrawCallback: function() {
                    $(this).closest('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function() {
                    $(this).closest('.table-card').removeClass('with-loader with-opacity-background');
                },
                columns: [
                    {"title": "Unbilled Inventory Changes",
                        "data": function(data) {
                            let description =  data.product_sku

                            return description+ ' (can be billed by shipments pickup picking fee or shipments picking fee)'
                        }
                    },
                ],
            }
        );

    });
};
