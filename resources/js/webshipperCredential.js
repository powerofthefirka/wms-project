window.WebshipperCredential = function () {
    $(document).ready(function() {
        $('#webshipper-credential-table').DataTable(
            {
                serverSide: true,
                ajax: '/webshipper_credential/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [

                    {
                        "title": "API Base URL",
                        "data": "api_base_url",
                        "name": "webshipper_credentials.api_base_url"
                    },

                    {
                        "title": "API Key",
                        "data": "api_key",
                        "name": "webshipper_credentials.api_key"
                    },
                    {
                        "title": "Customer",
                        "data": function (data) {
                            return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                        },
                        "name": "customer_contact_information.name"
                    },
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit" style="display: inline-block"> Edit </a>';

                            let deleteButton =
                                '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                                '<input type="hidden" name="_method" value="delete">' +
                                '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                                ' <button type="button" class="btn btn-danger" data-confirm-action="Are you sure you want to delete this credential?">Delete</button>'+
                                '</form>';

                            return editButton + deleteButton
                        },
                    },
                ],
            });

        $(document).on('click', '#webshipper-credential-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });

    });
};
