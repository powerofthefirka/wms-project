window.InventoryLog = function (event) {
    $(document).ready(function(e) {
        $('#inventory-log-table').DataTable(
            {
                serverSide: true,
                ajax: '/inventory_logs/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {
                        "title": "User",
                        "name": "users_contact_information.name",
                        "data": function (data) {
                            return '<a href="' + data.user['url'] + '" style="display: inline-block">' + data.user['name'] + '</a>';
                        }
                    },
                    {
                        "orderable": false,
                        "title": "From",
                        "name": "",
                        "data": function (data) {
                            return data.from['url'] === '#' ? data.from['name'] : '<a href="' + data.from['url'] + '" style="display: inline-block">' + data.from['name'] + '</a>';
                        }
                    },
                    {
                        "orderable": false,
                        "title": "To",
                        "name": "",
                        "data": function (data) {
                            return data.destination['url'] === '#' ? data.destination['name'] : '<a href="' + data.destination['url'] + '" style="display: inline-block">' + data.destination['name'] + '</a>';
                        }
                    },
                    {
                        "title": "Product",
                        "name": "products.name",
                        "data": function (data) {
                            return data.product['id'] === '#' ? 'Deleted/' + data.product['name'] : '<a href="' + data.product['url'] + '" style="display: inline-block">'+ data.product['name'] + '</a>';
                        }
                    },
                    {
                        "title": "Quantity",
                        "name": "quantity",
                        "data": "quantity"
                    },
                ],
            });


    });
};
