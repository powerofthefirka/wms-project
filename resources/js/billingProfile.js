window.BillingProfile = function () {
    $(document).ready(function() {
        $('#billing-profile-table').DataTable(
            {
                serverSide: true,
                ajax: '/billing_profiles/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {
                        "title": "Name",
                        "data": "name",
                        "name": "billing_profiles.name"
                    },
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            console.log(data);
                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-sm btn-primary edit" style="display: inline-block"> Edit </a>';

                            let deleteButton =
                                '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                                '<input type="hidden" name="_method" value="delete">' +
                                '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                                ' <button type="button" class="btn btn-sm btn-danger" data-confirm-action="Are you sure you want to delete this task type?">Delete</button>'+
                                '</form>';

                            let cloneButton =
                                '<form action="' + data.link_clone.url + '" method="post" style="display: inline-block; margin-right: 8px">' +
                                '<input type="hidden" name="_method" value="post">' +
                                '<input type="hidden" name="_token" value=' + data.link_clone.token + '>' +
                                ' <button type="button" class="btn btn-sm btn-info" data-confirm-action="Are you sure you want to clone this billing profile?">Clone</button>'+
                                '</form>';

                            return editButton + cloneButton + deleteButton
                        },
                    },
                ],
            });

        $(document).on('click', '#billing-profile-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });

    });
};
