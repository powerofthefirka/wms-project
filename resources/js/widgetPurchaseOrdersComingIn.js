window.WidgetPurchaseOrdersComingIn = function () {
    $(document).ready(function () {
        $.ajax({
            url: "/purchase_orders/coming_in",
            context: document.body
        }).done(function(data) {
            $('.purchase-orders-coming-in').html(data.poCount)
        });
    })
};
