window.CountryFilter = function () {
    $(function() {
        let countryFilter = $('#countries-filter-input');
        let countryList = $('#all-countries-list');
        let selectedCountriesList = $('#selected-countries-list');
        let selectedCountriesListField = $('#selected-countries-field');

        countryFilter.on('keyup', function () {
            let query = $(this).val().toLowerCase();
            countryList.find('.list-group-item').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(query) > -1)
            });
        });

        countryList.find('.list-group-item').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();

            let countryRow = $(this);
            selectedCountriesList.append(countryRow.clone());
            selectedCountriesList.find('.empty-line').addClass('d-none');
            selectedCountriesListField.find('option[value="' + countryRow.data('country_id') + '"]').attr('selected', 'selected');
            countryRow.addClass('d-none');
        });

        $(document).on('click', '.list-group-item:not(.empty-line)', function (event) {
            event.preventDefault();
            event.stopPropagation();

            let countryRow = $(this);
            countryList.find('.list-group-item[data-country_id="' + countryRow.data('country_id') + '"]').removeClass('d-none');
            selectedCountriesListField.find('option[value="' + countryRow.data('country_id') + '"]').attr('selected', false);
            countryRow.remove();
            if (selectedCountriesList.children().length <= 1) {
                selectedCountriesList.find('.empty-line').removeClass('d-none');
            }
        });
    });
};
