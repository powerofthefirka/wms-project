window.ImageDropzone = function (event) {
    const dzb = $('#dropzone-body');
    const isMultiple = dzb.attr("data-multiple");
    let images = dzb.attr("data-images");

    Dropzone.options.dropzoneBody = {
        autoProcessQueue: false,
        uploadMultiple: isMultiple,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove',
        parallelUploads: isMultiple ? 10 : 1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        maxFiles: isMultiple ? 10 : 1,
        url: $('#dropzone-body').attr("data-url"),
        maxfilesexceeded: function(file) {
            this.removeAllFiles();
            this.addFile(file);
        },
        init: function () {
            const myDropzone = this;

            $("#" + $('#dropzone-body').attr("data-button")).click(function (e) {
                $('#' + $('#dropzone-body').attr("data-button")).attr("disabled", true);
                e.preventDefault();
                e.stopPropagation();

                if (myDropzone.getQueuedFiles().length > 0) {
                    myDropzone.processQueue();
                } else {
                    $('#' + $('#dropzone-body').attr("data-form")).submit();
                }
            });

            this.on('sending', function (file, xhr, formData) {
                const data = $('#' + $('#dropzone-body').attr("data-form")).serializeArray();
                $.each(data, function (key, el) {
                    formData.append(el.name, el.value);
                });
            });

            if (images) {
                const parsedImages = JSON.parse(images);

                if ( parsedImages.id ) {
                    addImage(myDropzone, parsedImages);
                } else {
                    for (i = 0; i < parsedImages.length; i++) {
                        addImage(myDropzone, parsedImages[i]);
                    }
                }
            }
        },
        success: function (file, response) {
            window.location.replace($('#dropzone-body').attr("data-redirect"));
        },
        error: function (file, response) {
            $('#' + $('#dropzone-body').attr("data-button")).removeAttr("disabled");

            const errors = response.errors;
            let ajaxErrors = [];

            for (var key in errors) {
                ajaxErrors.push(errors[key]);
                ajaxErrors.push("<br>");
            }

            if (ajaxErrors.length > 0) {
                ajaxMessageBox(ajaxErrors, false)
            }

            this.removeAllFiles();
        },
        removedfile: function (file, response) {
            if (file.id) {
                $.get('/product/delete_product_image/', { id: file.id }, function(data, status){
                    if(data.success){
                        file.previewElement.remove();
                    } else {
                        alert('Error, try to restart page.');
                    }
                });
            } else {
                file.previewElement.remove();
            }
        }
    }

    function addImage (myDropzone, parsedImage) {
        myDropzone.emit("addedfile", parsedImage);
        myDropzone.emit("thumbnail", parsedImage, parsedImage.source);
        myDropzone.emit("complete", parsedImage);
    }
};
