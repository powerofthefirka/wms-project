window.ShippingCarrierMapping = function () {
    $(document).ready(function() {
        let shippingServiceSelect = $('.enabled-for-customer[name="shipping_service"]');
        let carrierSelect = $('.enabled-for-customer[name="shipping_service_carrier_id"]');
        let customerSelect = $('.customer_id');
        let enabledForCustomer = $('.enabled-for-customer');

        function toggleInputs(){
            if (customerSelect.val() === '' || customerSelect.val() ===  null) {
                shippingServiceSelect.prop('disabled', true);
                shippingServiceSelect.append(new Option('Select', 'title', true, false));
                shippingServiceSelect[0].options[0].disabled = true;

                carrierSelect.prop('disabled', true);
                carrierSelect.append(new Option('Select', 'title', true, false));
                carrierSelect[0].options[0].disabled = true;
            } else {
                enabledForCustomer.prop('disabled', false);
            }
        }
        toggleInputs();

        customerSelect.on('change', function (event) {
            let customerId = customerSelect.val();
            let selectedService = shippingServiceSelect.val();
            let selectedCarrier = carrierSelect.val();

            shippingServiceSelect.empty();
            carrierSelect.empty();

            toggleInputs();

            if (customerId) {

                $.map([{key:'webshipper', value:'Webshipper'}], function(service) {
                    let selected = service.key === selectedService;
                    shippingServiceSelect.append(new Option(service.value, service.key, selected, selected));
                })

                selectedService = shippingServiceSelect.val();

                generateCarriers(customerId, selectedService, selectedCarrier);
            }
        }).trigger('change');

        shippingServiceSelect.on('change', function () {
            let customerId = customerSelect.val();
            let selectedService = shippingServiceSelect.val();
            let selectedCarrier = carrierSelect.val();

            carrierSelect.empty();

            generateCarriers(customerId, selectedService, selectedCarrier);
        }).trigger('ajaxSelectOldValueUrl:toggle');

        function generateCarriers(customer, service, selectedCarrier) {
            if (service == "webshipper") {
                $.get("/shipping_carrier_mapping/" + customer + "/carriers", function(data, status){
                    $.map( data, function(result) {
                        let selected = Number(result.id) === Number(selectedCarrier);
                        carrierSelect.append(new Option(result.alias, result.id, selected, selected));
                    })
                });
            }
        }

        $('#shipping-carrier-mapping-table').DataTable(
            {
                serverSide: true,
                ajax: '/shipping_carrier_mapping/data_table',
                responsive: true,
                columns: [
                    {
                        "title": "Shipping Service",
                        "data": "shipping_service",
                        "name": "customer_shipping_carriers_maps.shipping_service"
                    },
                    {
                        "orderable": false,
                        "title": "Carrier Name",
                        "name": "",
                        "data": function (data) {
                            return data.carrier_name;
                        }
                    },
                    {
                        "title": "Customer",
                        "data": function (data) {
                            return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                        },
                        "name": "customer_contact_information.name"
                    },
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit" style="display: inline-block"> Edit </a>';

                            let deleteButton =
                                '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                                '<input type="hidden" name="_method" value="delete">' +
                                '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                                ' <button type="button" class="btn btn-danger" data-confirm-action="Are you sure you want to delete this credential?">Delete</button>'+
                                '</form>';

                            return editButton + deleteButton
                        },
                    },
                ],
            });

        $(document).on('click', '#shipping-carrier-mapping-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });

    });
};
