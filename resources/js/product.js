window.ProductForm = function (event) {
    $(document).ready(function(e) {
        getCustomerConfig();

        let customerSelect = $('.customer_id');
        let supplierSelect = $('.supplier_select');

        function customerSelection() {
            if (window.location.href.search('/edit') < 0) {
                if ($('.customer_id').val() > 0) {
                    $(supplierSelect).removeAttr('disabled');
                } else {
                    $(supplierSelect).attr('disabled', true);
                }
            }
        }

        customerSelect.on('select2:select', function (event) {
            customerSelection();
            setCustomerToVendorUrl($(supplierSelect));

            $('#item_container').find('.remove-item-button').trigger('click')

            checkForDoubles(supplierSelect)
        });

        function setCustomerToVendorUrl(supplierSelect) {
            let customerId = customerSelect.val();

            if (customerId) {
                changeSelectInputUrlAjax($(supplierSelect), customerId);
            }
        }

        function changeSelectInputUrlAjax(selectInputToChange, value) {
            selectInputToChange.select2('destroy');
            let url = selectInputToChange.data('ajax--url');
            let urlLast = url.substring(url.lastIndexOf('/'));

            if (urlLast == '/filter_suppliers') {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url') + '/' + value);
            } else {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url').replace(/\/\w+?$/, '/' + value));
            }

            selectInputToChange.select2();
        }

        let columns = [
            {
                "title": "",
                "data": function () {
                    return '<i class="ni ni-zoom-split-in preview-button"></i>'
                },
                "name": "products.id",
                "orderable": false
            },
            {
                "title": "Name",
                "data": function (data) {
                    let icon = '\n' +
                        '\n' +
                        '        <div class="media align-items-center">\n' +
                        '            <a href="' + data['link_edit'] + '" class="avatar rounded-circle mr-3">\n' +
                        '                <img alt="Product Image" onerror="this.src=\'/images/product-placeholder.png\'" style="object-fit: cover; height: 50px; width: 50px;" src="' + data['source'] + '">\n' +
                        '            </a>\n' +
                        '            <div class="media-body">\n' +
                        '                <span class="name mb-0 text-sm">' + data['name'] + '<span>\n' +
                        '            </div>\n' +
                        '        </div>';
                    return icon


                },
                "name": "products.name"
            },
            {
                "title": "SKU",
                "name": "products.sku",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data.sku + '</a>';

                    return editLink
                },
            },
            {"title": "On Hand", "data": "quantity_on_hand", "name": "products.quantity_on_hand"},
            {"title": "Available", "data": "quantity_available", "name": "products.quantity_available"},
            {"title": "Allocated", "data": "quantity_allocated", "name": "products.quantity_allocated"},
            {"title": "Backorder", "data": "quantity_backordered", "name": "products.quantity_backordered"},
            {"title": "On PO", "data": "quantity_on_po", "name": "products.quantity_on_po"},
            {"title": "Sell Ahead", "data": "quantity_sell_ahead", "name": "products.quantity_sell_ahead"},
            {"title": "Barcode", "data": "barcode", "name": "products.barcode"},
            {"title": "Weight", "data": "weight_with_unit", "name": "products.weight"},
            {"title": "Kit", "data": "is_kit", "name": "products.is_kit"},
            {"title": "Price", "data": "price", "name": "products.price"},
            {"title": "Value", "data": "replacement_value", "name": "products.replacement_value"},
            {"title": "Product note", "data": "notes", "name": "products.notes"},
            {"title": "Country of manufacture", "data": "country_of_origin", "name": "products.country_of_origin"},
            {"title": "Tariff Code", "data": "hs_code", "name": "products.hs_code"},

        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#product-table').DataTable(
            {
                serverSide: true,
                ajax: '/products/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[1, 'desc']],
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function (row, data, dataIndex) {
                    let tableId = "#product-table"
                    var dtable = $(tableId).dataTable().api()

                    $(dtable.column('products.price:name').header()).text('Price' + ' (' + data.currency + ')');
                    $(dtable.column('products.replacement_value:name').header()).text('Value' + ' (' + data.currency + ')');
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete(tableId, 'user-settings/hide-columns',  'products_table_hide_columns');
                    // END Hide/Show columns

                    $('img').on('error', function() {
                        $(this).attr('src', '/images/product-placeholder.png');
                    });
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#product-table .preview-button', function (event) {
            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "products/" + id + "/preview");
        });

        if($('input[name="product_type"]').val() >= 0){
            var type = $('input[name="product_type"]').val();
            $('.product-type-button[data-type="' + type + '"]').addClass('active');
        }

        $(document).on('click', '.product-type-button', function (event) {
            var buttons = $('.product-type-button');
            var current = $(this);
            var type = current.attr('data-type');
            var input = $('input[name="product_type"]');

            buttons.removeClass('active');
            current.addClass('active');
            input.val(type);
        });

        $(document).on('click', '.delete-product-image-button', function (event) {
            event.preventDefault();
            var button = $(this);
            var id = button.attr('data-image-id');
            $.get('/products/delete_product_image/', { id: id }, function(data, status){
                if(data.success){
                    button.parent().remove();
                } else {
                    alert('Error, try to restart page.');
                }
            });
        });

        $(document).on('click', '.remove-item-button', function (event) {
            let tr = $(this).closest('tr');

            if($(this).closest('tbody').find('tr').length == 1) {
                tr.hide();
                tr.find('.reset_on_delete').attr('disabled', true).val('')
            } else {
                tr.remove();
            }

            event.preventDefault();
        });

        function addSupplier() {
            let lastOrderItemFields = $('.order-item-fields:last');

            if (lastOrderItemFields.find('.customer_supplier').val() == '') {
                lastOrderItemFields.remove()
            }

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);

            orderItemFields.find('span').html('');
        }

        supplierSelect.on('select2:select', function (e) {
            let data = e.params.data;

            addSupplier();

            let orderItems = $('.order-item-fields')
            let orderItem = orderItems.last();

            orderItem.find('.customer_supplier').attr('disabled', false).val(data.id);
            orderItem.find('.supplier_name').html(data.text);
            orderItem.removeClass('d-none');
            orderItem.show();

            $(this).val(null).trigger('change');
        });

        let changeLogColumns = [
            {"title": "Date", "data": "created_at", "name": "object_changes.created_at"},
            {
                "title": "Product Name",
                "data": function(data) {
                    return '<a href="' + data.product['url'] + '" style="display: inline-block"> ' + data.product['name'] + ' </a>'
                },
                "name": "products.name"
            },
            {"title": "Property", "data": "column", "name": "object_changes.column"},
            {"title": "Old Value", "data": "old_value", "name": "object_changes.old_value"},
            {"title": "New Value", "data": "new_value", "name": "object_changes.new_value"},
        ];

        if ($('#change-log-product-id').val()) {
            changeLogColumns.splice(1, 1);
        }

        $('#product-change-log-table').DataTable({
            serverSide: true,
            ajax: '/products/log_data_table/' + $('#change-log-product-id').val(),
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[1, 'desc']],
            search: {
                search: JSON.stringify( {filterArray:[
                        {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                    ]
                })
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: changeLogColumns,
        });

        function checkForDoubles(item) {
            item.select2({
                templateResult: function (result) {
                    let prodIds = []

                    $('#item_container').find('.customer_supplier').each(function(){
                        prodIds.push($(this).val())
                    })

                    if(result.id) {
                        if(prodIds.includes(result.id.toString())) {
                            return ;
                        }
                    }

                    return result.text
                }
            });
        }

        checkForDoubles(supplierSelect)
        customerSelection()
    });
};
