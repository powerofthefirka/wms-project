window.Invoice = function () {
    $(document).ready(function() {
        checkDeleteButton();
        deleteItemFromTableButton();

        let columns = [
            {
                "title": "",
                "data": function () {
                    return '<i class="ni ni-zoom-split-in preview-button"></i>'
                },
                "name": "invoices.id"
            },
            {
                "title": "Customer Name",
                "data": function (data) {
                    return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                },
                "name": "contact_informations.name"
            },
            {
                "title": "Invoice ID",
                "data": 'id',
                "name": "invoices.id"
            },
            {
                "title": "Total Lines",
                "data": 'total_invoice_lines',
                "name": "total_invoice_lines"
            },
            {
                "title": "Direct Url",
                "data": function (data) {
                    return '<a href="' + data.direct_url['url'] + '">' + data.direct_url['name'] + '</a>'
                },
                "name": "direct_url"
            }
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#invoices-table').DataTable(
            {
                serverSide: true,
                ajax: '/invoices/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#invoices-table', 'user-settings/hide-columns',  'invoices_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#invoices-table .preview-button', function (event) {
            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "invoices/" + id + "/preview");
        });

        if($('#fill-information').val()) {
            $('.billing_contact_information').hide();
        }

        $('#fill-information').click(function () {
            if ($(this)[0].checked) {
                $('.billing_contact_information').show();
                $('.sizing').addClass('col-xl-4').removeClass('col-xl-6');

            }else{
                $('.billing_contact_information').hide();
                $('.sizing').addClass('col-xl-6').removeClass('col-xl-4');
            }
        });

        $('#add_item').click(function (event) {
            event.preventDefault();
            let lastOrderItemFields = $('.order-item-fields:not(.order-item-deleted):last');

            lastOrderItemFields.find('select').select2('destroy');

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            $('.order-item-fields:last').find('input[type=hidden]').remove();
            $('.order-item-fields:last').show();

            lastOrderItemFields.find('select').select2();
            orderItemFields.find('select').select2();

            checkDeleteButton();
        })

    });
};
