window.ShippingCarrier = function () {
    $(document).ready(function() {
        let columns = [
            {
                "title": "Name",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['name'] + '</a>';

                    return editLink
                },
                "name": "shipping_carriers.name"
            },
            {
                "title": "3PL",
                "data": function (data) {
                    return '<a href="' + data.three_pl['url'] + '">' + data.three_pl['name'] + '</a>'
                },
                "name": "threepl_contact_information.name",
            },
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#shipping-carrier-table').DataTable(
            {
                serverSide: true,
                ajax: '/shipping_carriers/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#shipping-carrier-table', 'user-settings/hide-columns',  'shipping_carrier_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#shipping-carrier-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "shipping_carriers/" + id + "/preview");
        });
    });
};
