window.DomainForm = function (event) {
    $(document).ready(function(e) {
        let columns = [
            {"title": "Id", "data": "id", "name": "domains.id"},
            {
                "title": "Title",
                "name": "domains.title",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data.title + '</a>';

                    return editLink
                },
            },
            {"title": "Domain", "data": "domain", "name": "domains.domain"},
            {
                "orderable": false,
                "title": "Actions",
                "data": function (data) {
                    let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-sm btn-secondary edit" style="display: inline-block"> Edit </a>';

                    let deleteButton =
                        '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                        '<input type="hidden" name="_method" value="delete">' +
                        '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                        '<button type="button" class="btn btn-primary btn-sm text-white remove-item-button" data-confirm-action="Are you sure you want to delete this domain?">×</button>'+
                        '</form>';

                    return editButton + deleteButton
                },
            },
        ];

        let threePlId = $('#three-pl-id').val();

        $('#domain-table').DataTable(
        {
            serverSide: true,
            ajax: '/three_pls/' + threePlId + '/domains/data_table',
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[1, 'desc']],
            search: {
                search: $('#global-search-input').val()
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            initComplete: function (row, data, dataIndex) {
                console.log(1);
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: columns,
        });
    });
};
