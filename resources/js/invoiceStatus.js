window.InvoiceStatus = function () {
    $(document).ready(function() {
        $(document).on('click', '#invoice-status-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });
    });
};
