window.ShippingBox = function () {
    $(document).ready(function() {
        let columns = [
            {
                "title": "Name",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['name'] + '</a>';

                    return editLink
                },
                "name": "shipping_boxes.name"
            },
            {"title": "WMS ID", "data": "wms_id", "name": "shipping_boxes.wms_id"},
            {
                "title": "Customer Name",
                "data": function (data) {
                    return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                },
                "name": "customer_contact_information.name",
            },
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#shipping-box-table').DataTable(
            {
                serverSide: true,
                ajax: '/shipping_boxes/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"p>>',
                "language": {
                    "paginate": {
                        "previous": "<",
                        "next": ">"
                    }
                },
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#shipping-box-table', 'user-settings/hide-columns',  'shipping_box_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#shipping-box-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "shipping_boxes/" + id + "/preview");
        });
    });
};
