window.SearchByCustomer = function () {
    $(document).ready(function() {
        getCustomerConfig();

        let userCustomerSelect = $('.user-customer');
        let warehouseClass = userCustomerSelect.attr("data-warehouse-class");
        let vendorClass = userCustomerSelect.attr("data-vendor-class");
        let defaultWarehouse = userCustomerSelect.attr("data-default-warehouse");
        let defaultVendor = userCustomerSelect.attr("data-default-vendor");
        let enWarehouse = userCustomerSelect.attr("data-warehouse-list");
        let enVendor = userCustomerSelect.attr("data-vendor-list");

        if ($('.search-by-customer option').length == 1) {
            $(".search-by-customer").addClass('d-none');
        }

        userCustomerSelect.on('change', function () {
            let customerId = userCustomerSelect.val();

            if (customerId == undefined) {
                $(".search-by-customer").addClass('d-none');
                $(".select-warehouse").addClass('d-none');
                $(".select-vendor").addClass('d-none');
            } else {
                if (enWarehouse) {
                    $.get('/warehouse-by-customer/' + customerId, function(data) {
                        $("." + warehouseClass).html(data);

                        if ($("." + warehouseClass + " option").length > 0) {
                            $(".select-warehouse").removeClass('d-none');

                            $("." + warehouseClass + " option").each(function() {
                                if ($(this).val() === defaultWarehouse) {
                                    $("." + warehouseClass).val(defaultWarehouse);
                                }
                            });
                        } else {
                            $(".select-warehouse").addClass('d-none');
                        }
                    });
                }

                if (enVendor) {
                    $.get('/vendor-by-customer/' + customerId, function(data) {
                        $("." + vendorClass).html(data);

                        if ($("." + vendorClass + " option").length > 0) {
                            $(".select-vendor").removeClass('d-none');

                            $("." + vendorClass + " option").each(function() {
                                if ($(this).val() === defaultVendor) {
                                    $("." + vendorClass).val(defaultVendor);
                                }
                            });

                            $(".select-vendor").trigger('vendors:added')
                        } else {
                            $(".select-vendor").addClass('d-none');
                            $(".select-vendor").trigger('vendors:empty')
                        }
                    });
                }
            }
        }).trigger('change');
    });
};
