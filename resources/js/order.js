window.Order = function () {
    $(document).ready(function() {
        getCustomerConfig();
        checkDeleteButton();
        deleteItemFromTableButton();
        removeItemFromTableButton();
        customerSelection();
        let customerSelect = $('.customer_id');
        let productSelect = $('.product_id');

        function customerSelection() {
            if ($('.customer_id').val() > 0) {
                $('.product_id').removeAttr('disabled');
            } else {
                $('.product_id').attr('disabled', true);
            }
        }

        customerSelect.on('change', function (event) {
            $('a[data-target="#shipping-modal"]').hide()

            let customerId = customerSelect.val();
            customerSelection();

            if (customerId && productSelect.length > 0) {
                changeSelectInputUrlAjax($(productSelect), customerId);
                let customShippingModal = $('#custom-shipping-modal').val();

                $.get("/orders/shipping_methods/" + customerId, { custom_shipping_modal: customShippingModal }, function(data, status){
                    $('.shipping-methods-list').replaceWith(data)

                    function checkShipping() {
                        let carrierId = $('input[name="shipping_carrier_id"]').val();
                        let methodId = $('input[name="shipping_method_id"]').val();
                        let button = $('input[id="shipping-method-' + carrierId + '-' + methodId + '"]');
                        let tr = button.closest('tr');

                        if (button.length) {
                            $('a[data-target="#shipping-modal"]').text(tr.find('td:eq(1)').text() + ' - ' + tr.find('td:eq(2)').text());
                            button.prop('checked', true);
                        }
                    }

                    checkShipping();
                    $('a[data-target="#shipping-modal"]').show()
                });
            }
        }).trigger('change');

        function changeSelectInputUrlAjax(selectInputToChange, value) {
            selectInputToChange.select2('destroy');
            let url = selectInputToChange.data('ajax--url');
            let urlLast = url.substring(url.lastIndexOf('/'));

            if (urlLast == '/filter_products') {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url') + '/' + value);
            } else {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url').replace(/\/\w+?$/, '/' + value));
            }

            selectInputToChange.select2();
        }

        let columns = [
            {
                "title": "",
                "data": function () {
                    return '<i class="ni ni-zoom-split-in preview-button"></i>'
                },
                "name": "orders.id",
                "orderable": false
            },
            {
                "title": "Status",
                "data": function (data) {
                   let status = data['status'].replace("_", " ");
                   let icon = 'sts-cancelled';

                   if (status == 'pending') {
                       icon = 'sts-pending';
                   } else if (status == 'fulfilled') {
                       icon = 'sts-fulfilled';
                   } else if (status == 'on hold') {
                       icon = 'sts-on-hold';
                   } else if (status == 'priority') {
                       icon = 'sts-priority';
                   } else if (status == 'backorder') {
                       icon = 'sts-backorder';
                   } else {}

                   return '<span class="badge badge-dot mr-4">\n' +
                       '                      <i class="' + icon + '"></i>\n' +
                       '                      <span class="status text-capitalize">' + status + '</span>\n' +
                       '                    </span>';
                },
                "name": "orders.status",
            },
            {
                "title": "Order Number",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['number'] + '</a>';
                    return editLink
                },
                "name": "orders.number"
            },
            {"title": "Date", "data": "ordered_at", "name": "orders.ordered_at"},
            {
                "title": "Customer",
                "data": function (data) {
                    return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                },
                "name": "customer_contact_information.name",
                "visible": !$('body').attr('data-current-customer')
            },
            {"title": "Total", "data": "total_with_currency", "name": "orders.total"},
            {"title": "Items", "data": "item_count", "name": "orders.item_count"},
            {"title": "Totes", "data": "totes", "name": "orders.item_count.totes"},
            {"title": "Delivery Method", "data": "shipping_method", "name": "shipping_methods.name"},
            {"title": "City", "data": "shipping_city", "name": "contact_informations.city"},
            {"title": "Zip Code", "data": "shipping_zip", "name": "contact_informations.zip"},
            {"title": "Country", "data": "shipping_country", "name": "countries.code"},
            {
                "title": "Fraud Hold",
                "name": "orders.fraud_hold",
                "data": function(data) { return data.fraud_hold === 1 ? 'YES' : 'NO'; }
            },
            {
                "title": "Address Hold",
                "name": "orders.address_hold",
                "data": function(data) { return data.address_hold === 1 ? 'YES' : 'NO'; }
            },
            {
                "title": "Payment Hold",
                "name": "orders.payment_hold",
                "data": function(data) { return data.payment_hold === 1 ? 'YES' : 'NO'; }
            },
            {
                "title": "Operator Hold",
                "name": "orders.operator_hold",
                "data": function(data) { return data.operator_hold === 1 ? 'YES' : 'NO'; }
            }
        ];

        // START Hide/Show columns
        if ($('body').attr('data-current-customer')) {
            columns.forEach(function (value, number) {
                if(value.name == 'customer_contact_information.name') {
                    columns.splice(number, 1);
                }
            })
        }
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        let dataTableUrl = '/orders/data_table';
        let fromOrdersLateTable = window.location.href.search('from_orders_late');

        if(fromOrdersLateTable > 0) {
            dataTableUrl = dataTableUrl + '?from_orders_late=1'
        }

        $('#orders-table').DataTable(
            {
                serverSide: true,
                ajax: dataTableUrl,
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[2, 'desc']],
                search: {
                    search: JSON.stringify( {filterArray:[
                            {columnName:"table_search", value: $('#global-search-input').val()},
                            {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                        ]
                    })
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function (row, data, dataIndex) {
                    var dtable = $("#orders-table").dataTable().api()
                    let table = $("#orders-table")

                    table.on('draw.dt', function (){
                        $(table).css('opacity', 1)
                    })

                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#orders-table', 'user-settings/hide-columns',  'orders_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#orders-table .preview-button', function (event) {
            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "orders/" + id + "/preview")
        });

        $('#dashboard-orders-table').DataTable(
            {
                serverSide: true,
                ajax: '/orders/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 10,
                sDom: '<"top">rt',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function()
                {
                    var dtable = $("#dashboard-orders-table").dataTable().api()
                    $("#dashboard-orders-search")
                        .unbind() // Unbind previous default bindings
                        .bind("input", function(e) { // Bind our desired behavior
                            // If the length is 3 or more characters, or the user pressed ENTER, search
                            if(this.value.length >= 3) {
                                // Call the API search function
                                dtable.search(this.value).draw();
                            }
                            // Ensure we clear the search if they backspace far enough
                            if(this.value == "") {
                                dtable.search("").draw();
                            }
                            return;
                        });
                },
                columns: [
                    {
                        "title": "Number",
                        "data": "number",
                        "name": "number"
                    },
                    {
                        "title": "Customer Name",
                        "data": function (data) {
                            return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                        },
                        "name": "customer_contact_information.name",
                        "visible": !$('body').attr('data-current-customer')
                    },
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-sm btn-primary edit" style="display: inline-block"> Edit </a>';

                            return editButton;
                        },
                    },
                ],
            });

        if($('#fill-information').val()) {
            $('.billing_contact_information').hide();
        }

        $('#fill-information').click(function () {
            if ($(this)[0].checked) {
                $('.billing_contact_information').show();
                $('.sizing').addClass('col-xl-6').removeClass('col-xl-12');

            } else {
                $('.billing_contact_information').hide();
                $('.sizing').addClass('col-xl-6').removeClass('col-xl-12');
            }
        });

        function roundNumber (number) {
            return Number.parseFloat(number).toFixed(2);
        }

        function checkOrderItems() {
            let orderItems = $('.order-item-fields');

            orderItems.each(function() {
                let quantity = $(this).find('.order-item-quantity input').val();
                let price = Number($(this).find('.order-item-price span').html());
                let total = roundNumber(price * quantity);

                $(this).find('.order-item-total input').html(total);
            });
        }

        checkOrderItems();

        let shippingMethod = '';

        $(document).on('click', 'label[for*="shipping-method"]', function (event) {
            let carrierId = $(this).attr('data-carrier-id');
            let methodId = $(this).attr('data-method-id');
            let price = $(this).attr('data-price');
            let input = $(this).closest('input');
            let shippingMethods = $(".shipping-methods-table tr");
            let tr = $(this).closest('tr');

            $('input[name="shipping_carrier_id"]').val(carrierId);
            $('input[name="shipping_method_id"]').val(methodId);
            $('input[name="shipping_price"]').val(price);

            $("input[id*='shipping-method']").prop('checked', false);
            input.prop('checked', true);

            shippingMethods.removeClass('active');
            tr.addClass('active');

            shippingMethod = tr.find('td:eq(1)').text() + ' - ' + tr.find('td:eq(2)').text();
            generateOrderOverview();
        });

        $(document).on('click', '.shipping-method-btn', function (event) {
            if (shippingMethod) {
                $('a[data-target="#shipping-modal"]').text(shippingMethod);
            }
        });

        $(document).on('change', '.order-item-quantity-input', function (event) {
            let orderItem = $(this).closest('.order-item-fields');
            let quantity = $(this).val();
            let price = Number(orderItem.find('.order-item-price span').html());
            let total = roundNumber(price * quantity);

            orderItem.find('.order-item-total span').html(total);

            generateOrderOverview();
        });

        $('.order-item-total span').each(function () {
            $(this).closest('.order-item-fields').find('.order-item-quantity-input').trigger('change');
        })

        $(document).on('click', '.remove-order-item', function (event) {
            event.preventDefault();
            let orderItem = $(this).closest('.order-item-fields');
            let createForm = $(this).closest('.order-item-fields').find('[type="hidden"][name*="[order_item_id]"]').length === 0;

            if (orderItem.siblings().length > 0 && createForm) {
                orderItem.remove();
            } else {
                orderItem.find('.order-item-quantity input').val(0);
                orderItem.find('.order-item-quantity input').attr('min', 0);
                orderItem.removeClass('d-block').addClass('d-none');
            }

            generateOrderOverview();
        });

        function addOrderItem () {
            let lastOrderItem = $('.order-item-fields').last();

            if(lastOrderItem.hasClass('d-none')){
                return;
            }

            let lastOrderItemHtml = lastOrderItem[0].outerHTML;
            let index = lastOrderItemHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(lastOrderItemHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            $('.order-item-fields:last').find('input[type=hidden]').remove();
            $('.order-item-fields:last').show();
        }

        productSelect.on('select2:select', function (e) {
            let data = e.params.data;
            let orderItems = $('.order-item-fields');
            let skip = false;

            orderItems.each(function() {
                if ($(this).find('.order-item-product-id input').val() == data.id) {
                    skip = true;
                }
            });

            if (!skip) {
                addOrderItem();
                orderItems = $('.order-item-fields')

                let orderItem = orderItems.last();
                let defaultSrc = orderItem.find('img').attr('data-default-src');

                orderItem.find('.order-item-product-id input').val(data.id);
                orderItem.find('.order-item-name').html('Name: ' + data.name + '<br>Sku: ' + data.sku);
                orderItem.find('.order-item-price span, .order-item-total span').html(data.price);
                orderItem.find('.order-item-quantity input').val(1);

                orderItem.find('.order-item-image img').attr('src', defaultSrc);

                if (data.image) {
                    orderItem.find('.order-item-image img').attr('src', data.image);
                }

                orderItem.removeClass('d-none');

                generateOrderOverview();
            }

            $(this).val(null).trigger('change');
        });

        $('.discount-percent-button, .discount-amount-button').on('click', function () {
            let button = $(this);
            let buttons = $('.discount-percent-button, .discount-amount-button');
            let holders = $('.discount-percent-holder, .discount-amount-holder');
            let discountType = $('input[name="discount_type"]');

            buttons.each(function(){
                $(this).removeClass('btn-primary').addClass('btn-secondary');
            });

            button.addClass('btn-primary');

            holders.each(function(){
                $(this).addClass('d-none');
            });

            if (button.hasClass('discount-percent-button')) {
                $('.discount-percent-holder').removeClass('d-none');
                discountType.val(0);
            } else {
                $('.discount-amount-holder').removeClass('d-none');
                discountType.val(1);
            }

            generateOrderOverview();
        })

        $(document).on('click', '.save-discount', function () {
            generateOrderOverview();
        })

        function generateOrderOverview () {
            let overviewSubtotal = $('.overview-subtotal strong');
            let overviewShipping = $('.overview-shipping strong');
            let overviewDiscount = $('.overview-discount strong');
            let overviewTotal = $('.overview-total strong');
            let orderItems = $('.order-item-fields');
            let discount = 0;
            let subtotal = 0;
            let total = 0;

            orderItems.each(function() {
                if (!$(this).hasClass('d-none')) {
                    subtotal += parseFloat(Number($(this).find('.order-item-total span').html()));
                }
            });

            let shipping = parseFloat($("#input-shipping_price").val()?$("#input-shipping_price").val():0);

            overviewSubtotal.text(roundNumber(subtotal));
            overviewShipping.text(roundNumber(shipping));

            subtotal = subtotal + shipping;

            let discountType = $('input[name="discount_type"]').val();
            let discountAmount = parseFloat($('input[name="discount_amount"]').val());
            let discountPercent = $('input[name="discount_percent"]').val();

            if (discountType == 1) {
                discount = discountAmount;
            } else {
                if (!discountPercent && discountAmount) {
                    discount = discountAmount;

                    if (subtotal) {
                        $('input[name="discount_percent"]').val(roundNumber(discount * 100 / subtotal));
                    }
                } else {
                    discount = subtotal * discountPercent / 100;
                }
            }

            $('input[name="discount"], input[name="discount_amount"]').val(roundNumber(discount));

            if (subtotal) {
                $('input[name="discount_percent"]').val(roundNumber(discount * 100 / subtotal));
            }

            overviewDiscount.text(roundNumber(discount));

            total = subtotal - discount;

            overviewTotal.text(roundNumber(total));
        }

        generateOrderOverview();

        function generateAddressInformation () {
            let shippingAddress = $('.shipping-address');
            let billingAddress = $('.billing-address');
            let shippingFields = $('.shipping_contact_information .form-group');
            let billingFields = $('.billing_contact_information .form-group');
            let shippingInformation = '';
            let billingInformation = '';

            shippingAddress.addClass('d-none');
            billingAddress.addClass('d-none');

            let differentBillingInformation = $('input[name="differentBillingInformation"]');

            shippingFields.each(function() {
                let input = $(this).find('input, select');

                if (input.val()) {
                    if (input.is('input')) {
                        shippingInformation += input.val() + '<br>';
                    } else {
                        shippingInformation += input.find( "option:selected" ).text() + '<br>';
                    }
                }
            });

            billingFields.each(function() {
                let input = $(this).find('input, select');

                if (input.val()) {
                    if (input.is('input')) {
                        billingInformation += input.val() + '<br>';
                    } else {
                        billingInformation += input.find( "option:selected" ).text() + '<br>';
                    }
                }
            });

            shippingInformation = shippingInformation.slice(0, -2);
            billingInformation = billingInformation.slice(0, -2);

            if (shippingInformation) {
                shippingAddress.removeClass('d-none').find('p').html(shippingInformation);
            }

            if (differentBillingInformation.is(':checked') || billingAddress.attr("data-is-differ-billing")) {
                if (billingInformation) {
                    billingAddress.removeClass('d-none').find('p').html(billingInformation);
                }
            }
        }

        generateAddressInformation();

        $(document).on('click', '.save-address', function (event) {
            generateAddressInformation();
        });

        if($('input[name="shipment_type"]').val() >= 0){
            var type = $('input[name="shipment_type"]').val();
            $('.shipment-type-button[data-type="' + type + '"]').addClass('btn-primary text-white');
        }

        $(document).on('click', '.shipment-type-button', function (event) {
            var buttons = $('.shipment-type-button');
            var current = $(this);
            var type = current.attr('data-type');
            var input = $('input[name="shipment_type"]');

            buttons.removeClass('btn-primary text-white').addClass('btn-secondary');
            current.addClass('btn-primary text-white');
            input.val(type);
        });

        $("select[name='product_id']").select2({
            escapeMarkup: function(markup) {
                return markup;
            },
            templateResult: function (result) {
                let prodIds = []

                $('.order-item-product-id').find('input').each(function(){
                    prodIds.push($(this).val())
                })

                if(result.id) {
                    if(prodIds.includes(result.id.toString())) {
                        return ;
                    }
                }

                 return result.html
            },
            templateSelection: function(result) {
                return result.text;
            }
        });

        let selectAll = false;

        $('#select-all-items').click(function(event) {
            if(!this.selectAll) {
                this.selectAll = true;
                $(this).removeClass('btn-success');
                $(this).addClass('btn-secondary');
                $(this).text('Deselect all');

                $('#reship_item_container :checkbox').each(function() {
                    this.checked = true;
                });
            } else {
                this.selectAll = false;
                $(this).removeClass('btn-secondary');
                $(this).addClass('btn-success');
                $(this).text('Select all');

                $('#reship_item_container :checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

        $(".order-reship-btn").click(function( event ) {
            event.preventDefault();
            let checkboxes = document.querySelectorAll('#reship_item_container :checked');

            if(checkboxes.length == 0) {
                ajaxMessageBox('Please select at least one checkbox!', false)
            } else {
                $('#order-reship').submit();
            }
        });

        let changeLogColumns = [
            {"title": "Date", "data": "created_at", "name": "object_changes.created_at"},
            {
                "title": "Order number",
                "data": function(data) {
                    return '<a href="' + data.order['url'] + '" style="display: inline-block"> ' + data.order['number'] + ' </a>'
                },
                "name": "orders.number"
            },
            {"title": "Product", "data": "product_name", "name": "order_items.name"},
            {"title": "Property", "data": "column", "name": "object_changes.column"},
            {"title": "Old Value", "data": "old_value", "name": "object_changes.old_value"},
            {"title": "New Value", "data": "new_value", "name": "object_changes.new_value"},
        ];

        if ($('#change-log-order-id').val()) {
            changeLogColumns.splice(1, 1);
        }

        $('#order-change-log-table').DataTable({
            serverSide: true,
            ajax: '/orders/log_data_table/' + $('#change-log-order-id').val(),
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[1, 'desc']],
            search: {
                search: JSON.stringify( {filterArray:[
                        {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                    ]
                })
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: changeLogColumns,
        });
    });
};
