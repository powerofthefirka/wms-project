
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.GridStack = require('gridstack/dist/gridstack-h5');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('click', 'button[data-confirm-action]', function(event) {
    event.preventDefault();
    event.stopPropagation();

    let button = this;

    $('#confirm-dialog .modal-body').text($(this).data('confirm-action'));
    $('#confirm-dialog .confirm-button').off('click');
    $('#confirm-dialog .confirm-button').on('click', function() {
        button.form.submit();
    });

    $('#confirm-dialog').modal();
});

const anchor = window.location.hash;
$(`a[href="${anchor}"]`).tab('show');

function routeFix(element) {
    let ajaxUrl = $(element).data('ajax--url');
    let selectedValue = element.value;

    let url = ajaxUrl + '?term=' + selectedValue;
    let optionText = element.options[0] !== undefined ? element.options[0].text.length : 0;

    if (ajaxUrl && optionText === 0 && element.value.length !== 0) {

        let select = element
        $.get(url, function(data, status){
            let results = data.results;

            select.append(new Option(results[0].text, results[0].id, 'selected', 'selected'));
        });
    }
}

window.select2routeFixer = function (){
    $("[data-toggle='select']").each(function(i, item) {
        routeFix(item);
        const fixRouteAfter = $(item).data('fixRouteAfter')
        if (fixRouteAfter) {

            $(fixRouteAfter).on('ajaxSelectOldValueUrl:toggle', function() {
                routeFix(item)
            });
        }
    });
}

select2routeFixer();

window.recalculateDataTableWidth = function() {
    let count = 0;
    let interval = setInterval(function() {
        if (count > 250) {
            clearInterval(interval);
        }

        $(window).trigger('resize');

        count++;
    }, 1);
}

window.onbeforeunload =  function() {
    if ($('body').hasClass('edited-in-preview')) {
        return false
    }
};

$(document).on('click', '.toggle-filter-button', function(event) {
    event.preventDefault();
    event.stopPropagation();

    let button = $(this);
    let filter = $('.filter');

    button.toggleClass('active');
    filter.toggleClass('d-none');
});

$(document).on('click','a', function(event){
    if($('body').hasClass('edited-in-preview')) {
        event.preventDefault()
        Preview.loadPreview(event)
    } else {
        return true
    }
})

$('img').on('error', function () {
    $(this).attr('src', '/images/product-placeholder.png');
});

$(document).on('error', 'img', function () {
    $(this).attr('src', '/images/product-placeholder.png');
});
