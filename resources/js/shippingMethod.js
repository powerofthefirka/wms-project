window.ShippingMethod = function () {
    $(document).ready(function() {
        let shippingCarrier = $('.enabled-for-3pl[name="shipping_carrier_id"]');
        let selectedCarrier = shippingCarrier.val();
        shippingCarrier.empty();

        let columns = [
            {
                "title": "Name",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['name'] + '</a>';

                    return editLink
                },
                "name": "shipping_methods.name"
            },
            {
                "title": "Shipping Carrier",
                "data": function (data) {
                    return '<a href="' + data.shipping_carrier['url'] + '">' + data.shipping_carrier['name'] + '</a>'
                },
                "name": "shipping_carriers.name",
            },
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $.get("/shipping_methods/get_carriers/", function(data, carrier){
            let results = data.results;
            $.map( results, function(result) {
                let selected = Number(result.id) === Number(selectedCarrier);
                shippingCarrier.append(new Option(result.text, result.id, selected, selected));
            })
        });

        $('#shipping-method-table').DataTable(
            {
                serverSide: true,
                ajax: '/shipping_methods/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#shipping-method-table', 'user-settings/hide-columns',  'shipping_method_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#shipping-method-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "shipping_methods/" + id + "/preview");
        });
    });
};
