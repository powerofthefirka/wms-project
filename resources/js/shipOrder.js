window.ShipOrder = function (event) {
    $(document).ready(function(e) {
        hideItems();
        checkDeleteButton();
        dateTimePicker();
        deleteItemFromTableButton();

        $('#add_item').click(function (event) {
            event.preventDefault();
            let lastOrderItemFields = $('.order-item-fields:not(.order-item-deleted):last');

            lastOrderItemFields.find('select').select2('destroy');

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            $('.order-item-fields:last').find('input[type=hidden]').remove();
            $('.order-item-fields:last').show();

            lastOrderItemFields.find('select').select2();
            orderItemFields.find('select').select2();
            orderItemFields.find('select').val('').trigger('change');
            orderItemFields.find('input[type=number]').val(0).trigger('change');
            orderItemFields.find('.received-quantity').val(0).trigger('change');

            let orderId = $('.order-select').val();
            $('.orderProduct').select2('destroy');
            $('.orderProduct').data('ajax--url', $('.orderProduct').data('ajax--url').replace(/\/\w+?$/, '/' + orderId));
            $('.orderProduct').select2();

            checkDeleteButton();
        });

        let carrierSelect = $('.enabled-for-order[name="webshipper_carrier_id"]');
        let shippingMethodSelect = $('.enabled-for-order[name="webshipper_shipping_method"]');
        let orderSelect = $('.order-select');
        let enabledForOrder = $('.enabled-for-order');

        function toggleInputs(){
            if (orderSelect.val() === '' || orderSelect.val() ===  null) {
                carrierSelect.prop('disabled', true);
                carrierSelect.append(new Option('Select', 'title', true, false));
                carrierSelect[0].options[0].disabled = true;

                shippingMethodSelect.prop('disabled', true);
                shippingMethodSelect.append(new Option('Select', 'title', true, false));
                shippingMethodSelect[0].options[0].disabled = true;
            } else {
                enabledForOrder.prop('disabled', false);
            }
        }
        toggleInputs();

        orderSelect.on('change', function () {
            let orderId = orderSelect.val();
            let selectedCarrier = carrierSelect.val();

            carrierSelect.empty();
            shippingMethodSelect.empty();

            toggleInputs();

            if (orderId) {
                $.get("/orders/" + orderId + "/carriers", function(data, status){
                    $.map( data, function(result) {
                        let selected = result.mapped;
                        carrierSelect.append(new Option(result.mappedName, result.id, selected, selected));
                    })

                    selectedCarrier = carrierSelect.val();

                    generateShippingMethods(orderId, selectedCarrier);
                });
            }

            $('#item_container, #add_item').show();

            $('.order-item-fields').not(':first').hide().addClass('order-item-deleted');

            $('.reset-value').text('');
            $('.reset-value').val(0);

            $('.orderProduct').select2('destroy');
            $('.orderProduct').data('ajax--url', $('.orderProduct').data('ajax--url').replace(/\/\w+?$/, '/' + orderId));
            $('.orderProduct').select2();
            hideItems();

        }).trigger('ajaxSelectOldValueUrl:toggle');

        carrierSelect.on('change', function () {
            let orderId = orderSelect.val();
            let selectedCarrier = carrierSelect.val();

            shippingMethodSelect.empty();

            generateShippingMethods(orderId, selectedCarrier);
        }).trigger('ajaxSelectOldValueUrl:toggle');

        function generateShippingMethods(orderId, selectedCarrier){
            $.get("/orders/" + orderId + "/carrier/" + selectedCarrier, function(data, status){
                $.map( data, function(result) {
                    let selected = result.mapped;
                    shippingMethodSelect.append(new Option(result.mappedName, result.service_code, selected, selected));
                })
            });
        }

        $(document).on('select2:select', '.orderProduct', function () {
            let productId = $(this).val();
            let productLocation = $(this).parent().parent().parent().find('.productLocation');

            productLocation.select2('destroy');
            productLocation.data('ajax--url', productLocation.data('ajax--url').replace(/\/\w+?$/, '/' + productId));
            productLocation.select2();
        });

        let columns = [
            {
                "title": "Order Number",
                "name": "orders.number",
                "data": function (data) {
                    return '<a href="/orders/' + data.order['id'] + '/edit" style="display: inline-block">' + data.order['number'] + '</a>'
                },
            },
            {
                "title": "Shipment date",
                "name": "shipments.shipped_at",
                "data": "shipped_at"
            },
            {
                "title": "Order date",
                "name": "orders.ordered_at",
                "data": "order_date"
            },
            {
                "title": "Tracking Number",
                "data": function (data) {
                    return '<a href="' + data.link_view['url'] + '">' + data.link_view['tracking_code'] + '</a>'
                },
                "name": "shipments.tracking_code",
            },
            {
                "title": "Email",
                "data": "order_shipping_email",
                "name": "shipping_contact_information.email"
            },
            {
                "title": "Address",
                "name": "shipping_contact_information.address",
                "data": "order_shipping_address"
            },
            {
                "title": "Address2",
                "name": "shipping_contact_information.address2",
                "data": "order_shipping_address2"
            },
            {
                "title": "City",
                "name": "shipping_contact_information.city",
                "data": "order_shipping_city"
            },
            {
                "title": "Zip",
                "name": "shipping_contact_information.zip",
                "data": "order_shipping_zip"
            },
            {
                "title": "Country",
                "name": "countries.title",
                "data": "order_shipping_country"
            },
            {
                "title": "Company",
                "name": "shipping_contact_information.company_name",
                "data": "order_shipping_company_name"
            },
            {
                "title": "Phone",
                "data": "order_shipping_phone",
                "name": "shipping_contact_information.phone"
            },
            {
                "title": "Distinct items",
                "data": "distinct_items",
                "name": "shipments.distinct_items"
            },
            {
                "title": "Quantity shipped",
                "data": "quantity_shipped",
                "name": "shipments.quantity_shipped"
            },
            {
                "title": "Line item total",
                "data": "item_total_with_currency",
                "name": "shipments.line_item_total"
            },
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#shipment-table').DataTable(
            {
                serverSide: true,
                ajax: '/shipments/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[1, 'desc']],
                search: {
                    search: JSON.stringify( {filterArray:[
                            {columnName:"table_search", value: $('#global-search-input').val()},
                            {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                        ]
                    })
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#shipment-table', 'user-settings/hide-columns',  'shipments_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            }
        );
    });
};
