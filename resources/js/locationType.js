window.LocationType = function () {
    $(document).ready(function() {
        let columns = [
            {
                "title": "Name",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['name'] + '</a>';

                    return editLink
                },
                "name": "location_types.name"
            },
            {"title": "WMS ID", "data": "wms_id", "name": "location_types.wms_id"},
            {"title": "Daily Storage Cost", "data": "daily_storage_cost", "name": "location_types.daily_storage_cost"},
            {
                "title": "3PL",
                "data": function (data) {
                    return '<a href="' + data.three_pl['url'] + '">' + data.three_pl['name'] + '</a>'
                },
                "name": "threepl_contact_information.name",
            }
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#location-type-table').DataTable(
            {
                serverSide: true,
                ajax: '/location_types/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"p>>',
                "language": {
                    "paginate": {
                        "previous": "<",
                        "next": ">"
                    }
                },
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#location-type-table', 'user-settings/hide-columns',  'location_type_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#location-type-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "location_types/" + id + "/preview");
        });
    });
};
