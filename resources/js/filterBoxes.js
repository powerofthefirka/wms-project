window.FilterBoxes = function () {
    $(document).ready(function() {
        let fetchNewStatusBoxes = true

        function fetchStatusBoxes () {
            $.ajax({
                url: statusesUrl,
                data: {date_filter: $('input[name="dates_between"]').val()},
                success: function (data) {
                    $('.filter-boxes').replaceWith(data)
                }
            });
        }
        fetchStatusBoxes()

        $(document).on('click', '.filter-by-status-button', function (event) {
            $('input[name="table_search"]').val('')

            let filterArray = [];
            let BoxFilterName = $(this).attr('data-filter');

            $('.table_filter').map(function (key, filter) {
                let filterName = $(filter).attr('name');
                let filterValue = $(filter).val();

                if(filterName == 'table_search') {
                    filterArray.push({columnName: filterName, value: BoxFilterName})
                } else {
                    filterArray.push({columnName: filterName, value: filterValue})
                }
            });

            let filters = {
                filterArray
            }

            fetchNewStatusBoxes = false
            $('#' + $('#table-id').val() ).dataTable().api().search(JSON.stringify(filters)).draw()
            fetchNewStatusBoxes = true

        });

        let fetchCount = 0

        $('#' + $('#table-id').val() ).on('search.dt', function (event) {
            if (fetchNewStatusBoxes && fetchCount < 1) {
                fetchCount += 1
                fetchStatusBoxes()
            } else {
                fetchCount = 0
            }
        })
    })
}
