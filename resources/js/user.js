window.User = function () {
    $(document).ready(function() {
        $('#users-table').DataTable(
            {
                serverSide: true,
                ajax: '/users/data-table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {"title": "Name", "data": "name", "name": "contact_informations.name"},
                    {"title": "Email", "data": "email", "name": "contact_informations.email"},
                    {"title": "Customer Role", "data": "customer_role", "name": "customer_user_roles.name"},
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit" style="display: inline-block"> Edit </a>';

                            let deleteButton =
                                '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                                '<input type="hidden" name="_method" value="delete">' +
                                '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                                ' <button type="button" class="btn btn-danger" data-confirm-action="Are you sure you want to delete this user?">Delete</button>'+
                                '</form>';

                            return editButton + deleteButton
                        },
                    },
                ],
            }
        );

        $(document).on('click', '#users-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });

        $(document).on('keyup', '#users-search', debounce(function() {
                var dtable = $("#users-table").dataTable().api();

                if(this.value.length >= 3) {
                    dtable.search(this.value).draw();
                }

                if(this.value == "") {
                    dtable.search("").draw();
                }
            }, 500)
        );
    });
};
