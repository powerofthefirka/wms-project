window.WidgetPurchaseOrdersReceivedQuantity = function () {
    $(document).ready(function () {
        $.ajax({
            url: "/purchase_orders/quantity_calc",
            context: document.body
        }).done(function(data) {
            $('.purchase-orders-quantity-received').html(data.poQuantityReceived)
        });
    })
};
