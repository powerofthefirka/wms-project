window.ThreePlForm = function (event) {
    $(document).ready(function(e) {
        let columns = [
            {"title": "Name", "data": "three_pl_name", "name": "contact_informations.name"},
            {"title": "Address", "data": "three_pl_address", "name": "contact_informations.address"},
            {"title": "Zip", "data": "three_pl_zip", "name": "contact_informations.zip"},
            {"title": "City", "data": "three_pl_city", "name": "contact_informations.city"},
            {"title": "Email", "data": "three_pl_email", "name": "contact_informations.email"},
            {"title": "Phone", "data": "three_pl_phone", "name": "contact_informations.phone"},
            {
                "orderable": false,
                "class":"text-center",
                "title": "Actions",
                "data": function (data) {
                    let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit btn-sm" style="display: inline-block"> Edit </a>';
                    let disableButton = '<a href="' + data['link_disable'] + '" class="btn btn-' + (data['active'] ? 'danger' : 'primary') + ' disable btn-sm ml-2" style="display: inline-block"> ' + (data['active'] ? 'Disable' : 'Enable' ) + ' </a>';

                    let deleteButton =
                        '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                        '<input type="hidden" name="_method" value="delete">' +
                        '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                        ' <button type="button" class="btn btn-danger btn-sm" data-confirm-action="Are you sure you want to delete this 3PL?">Delete</button>'+
                        '</form>';

                    return editButton + deleteButton + disableButton
                },
            },
        ];

        $('#export-dates-between').val($('input[name="dates_between"]').val());

        $('#three-pl-table').DataTable(
        {
            serverSide: true,
            ajax: '/three_pls/data_table',
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[0, 'desc']],
            search: {
                search: JSON.stringify( {filterArray:[
                        {columnName:"table_search", value: $('#global-search-input').val()},
                        {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                    ]
                })
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            initComplete: function()
            {
                // START Hide/Show columns
                new ShowHideColumnInitComplete('#three-pl-table', 'user-settings/hide-columns',  '3pls_table_hide_columns');
                // END Hide/Show columns
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: columns,
        });

        $(document).on('click', '#three-pl-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            window.location.href = '/three_pls/' + id + '/edit'
        });

        if(!$('#chk-custom_shipping_modal').is(':checked')) {
            $('.custom-shipping-modal-text-div').hide();
        }

        $(document).on('click', '#chk-custom_shipping_modal', function (event) {
            if($(this).is(':checked')) {
                $('.custom-shipping-modal-text-div').show();
            } else {
                $('.custom-shipping-modal-text-div').hide();
            }
        });

        let threePlId = $('#three-pl-id').val();

        $('#pricing-plan-name').on('change', function (event) {
            let pricingPlanName = $(this).val();

            let billingPeriodSelect = $('#billing-period');
            billingPeriodSelect.empty();

            if (!pricingPlanName || pricingPlanName.length === 0) {
                $('input[name="monthly_price"]').val(" ");
                $('input[name="initial_fee"]').val(" ");
                $('input[name="three_pl_billing_fee"]').val(" ");
                $('input[name="number_of_monthly_orders"]').val(" ");
                $('input[name="price_per_additional_order"]').val(" ");
            } else {
                $.get("/three_pls/" + threePlId + "/pricing_plan/" + $(this).val(), function(data) {
                    $('input[name="initial_fee"]').val(data.initial_fee);
                    $('input[name="three_pl_billing_fee"]').val(data.three_pl_billing_fee);
                    $('input[name="number_of_monthly_orders"]').val(data.number_of_monthly_orders);
                    $('input[name="price_per_additional_order"]').val(data.price_per_additional_order);

                    $.map( ['yearly', 'monthly'], function(result) {
                        let selected = result == data.billing_period;
                        billingPeriodSelect.append(new Option(capitalizeFirstLetter(result), result, selected, selected));
                    })

                    generateMonthlyPrice(threePlId, pricingPlanName, data.billing_period);
                });
            }
        }).trigger('change');

        $('#billing-period').on('change', function (event) {
            let threePlId = $('#three-pl-id').val();
            let pricingPlanName = $('#pricing-plan-name').val();
            let billingPeriod = $(this).val();

            if (pricingPlanName && billingPeriod) {
                generateMonthlyPrice(threePlId, pricingPlanName, billingPeriod);
            }
        }).trigger('change');

        function generateMonthlyPrice(threePlId, pricingPlanName, billingPeriod) {
            $.get("/three_pls/" + threePlId + "/pricing_plan/" + pricingPlanName + "/" + billingPeriod, function(data) {
                $('input[name="monthly_price"]').val(data);
            });
        }

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        $('#billing-report-table').DataTable(
        {
            serverSide: true,
            ajax: '/three_pls/' + threePlId + '/billing_report_data_table/',
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[0, 'desc']],
            search: {
                search: JSON.stringify( {filterArray:[
                        {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                    ]
                })
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: [
                {"title": "Customer", "data": "customer_name", "name": "contact_informations.name"},
                {"title": "Number of Orders", "data": "total_order", "name": "total_order"},
            ],
        });

        $('.table-datetimepicker').on('apply.daterangepicker', function(ev, picker) {
            let datesBetween = moment(picker.startDate).format('Y-MM-DD') + ' - ' + moment(picker.endDate).format('Y-MM-DD');
            $('#export-dates-between').val(datesBetween);

            $.ajax({
                method:'GET',
                url: "/three_pls/" + threePlId + "/billing_report/orders_price",
                data: {
                    dates_between: datesBetween,
                },
                success: function (results)
                {
                    $('#total-number-of-orders').text(results.totalNumberOfOrders);
                    $('#total-order-price').text(results.totalOrderPrice.toFixed(2));
                },
                error: function (xhr)
                {
                    ajaxMessageBox(xhr.responseJSON.message, false)
                }
            })
        });
    });
};
