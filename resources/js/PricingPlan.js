window.PricingPlanForm = function (event) {
    $(document).ready(function(e) {
        let columns = [
            {"title": "Name", "data": "name", "name": "pricing_plans.name"},
            {"title": "Monthly Price for Yearly Billing Period", "data": "monthly_price_for_yearly_billing_period", "name": "pricing_plans.monthly_price_for_yearly_billing_period"},
            {"title": "Monthly Price for Monthly Billing Period", "data": "monthly_price_for_monthly_billing_period", "name": "pricing_plans.monthly_price_for_monthly_billing_period"},
            {"title": "Initial Fee", "data": "initial_fee", "name": "pricing_plans.initial_fee"},
            {"title": "3PL Billing Fee", "data": "three_pl_billing_fee", "name": "pricing_plans.three_pl_billing_fee"},
            {"title": "Number of Monthly Orders", "data": "number_of_monthly_orders", "name": "pricing_plans.number_of_monthly_orders"},
            {"title": "Price Per Additional Order", "data": "price_per_additional_order", "name": "pricing_plans.price_per_additional_order"},
            {
                "orderable": false,
                "class":"text-center",
                "title": "Actions",
                "data": function (data) {
                    let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit btn-sm" style="display: inline-block"> Edit </a>';

                    let deleteButton =
                        '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                        '<input type="hidden" name="_method" value="delete">' +
                        '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                        ' <button type="button" class="btn btn-danger btn-sm" data-confirm-action="Are you sure you want to delete this plan?">Delete</button>'+
                        '</form>';

                    return editButton + deleteButton
                },
            },
        ];

        $('#pricing-plan-table').DataTable(
        {
            serverSide: true,
            ajax: '/three_pls/pricing_plans/data_table',
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[0, 'desc']],
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            initComplete: function()
            {
                // START Hide/Show columns
                new ShowHideColumnInitComplete('#pricing-plan-table', 'user-settings/hide-columns',  'pricing_plan_table_hide_columns');
                // END Hide/Show columns
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: columns,
        });

        $(document).on('click', '#pricing-plan-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            window.location.href = '/three_pls/pricing_plans/' + id + '/edit'
        });
    });
};
