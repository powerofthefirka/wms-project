window.PurchaseOrderForm = function (event) {
    $(document).ready(function(e) {
        datePicker();
        checkDeleteButton();
        deleteItemFromTableButton();
        removeItemFromTableButton();

        let customerSelect = $('.customer_id');
        let enabledForCustomer = $('.enabled-for-customer');
        let supplierSelect = $('.enabled-for-customer[name="supplier_id"]');
        let productSelect = $('.product-select');

        function toggleInputs(){
            if (customerSelect.val() === '' || customerSelect.val() ===  null) {
                enabledForCustomer.prop('disabled', true);
                supplierSelect.empty();
            } else {
                enabledForCustomer.prop('disabled', false);
            }
        }

        function changeSelectInputUrlAjax(selectInputToChange, value) {
            selectInputToChange.select2('destroy');
            selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url').replace(/\/\w+?$/, '/' + value));
            selectInputToChange.select2();
            checkForDoubles($('.product-select'));
        }

        customerSelect.on('select2:select', function () {
            supplierSelect.empty();
        });

        customerSelect.on('change', function (event) {
            toggleInputs();
        }).trigger('change');

        supplierSelect.on('change', function (event) {
            let supplierId = $(this).val();

            $('#item_container').find('tr').each(function () {
                $(this).find('.remove-purchase-order-item').trigger('click')
            })

            $('.product-select').each(function (key, item) {
                changeSelectInputUrlAjax($(item), supplierId);
            })
        });

        $(".select-vendor").on('vendors:added', function () {
            $('.auto-buttons').removeClass('d-none');
            $('.product-select').removeAttr('disabled');

            $('.product-select').each(function (key, item) {
                changeSelectInputUrlAjax($(item), $('[name="supplier_id"]').val());
            });
        })

        $(".select-vendor").on('vendors:empty', function () {
            $('.auto-buttons').addClass('d-none');
            $('.product-select').attr('disabled', true);

            $('#item_container').find('tr').each(function () {
                $(this).find('.remove-purchase-order-item').trigger('click')
            })
        })

        $(document).on('click', '.remove-purchase-order-item', function (event) {
            event.preventDefault();
            let orderItem = $(this).closest('.order-item-fields');

            orderItem.find('.ordered').val(0);
            orderItem.removeClass('d-block').addClass('d-none');
        });

        function addPurchaseOrderItem(data) {
            let lastOrderItemFields = $('.order-item-fields:not(.order-item-deleted):last');

            if (lastOrderItemFields.find('.product_id').val() == '') {
                lastOrderItemFields.remove()
            }

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            $('.order-item-fields:last').find('input[type=hidden]').remove();
            $('.order-item-fields:last').show();

            if(data) {
                Object.keys(data).map(function(key, index) {
                    let value = data[key]
                    let classToSelect = key.toString()
                    let item = orderItemFields.find('.' + classToSelect)

                    if (classToSelect == 'id') {
                        item = orderItemFields.find('.product_id')
                    }

                    if(item.is('span')) {
                        if(classToSelect == 'total_price') {
                            item.html(Number(data.ordered) * Number(data.unit_price))
                        }

                        item.html(value)
                    } else {
                        item.val(value)
                    }
                });

                let orderedInput = $(orderItemFields).find('.ordered');
                let unitPriceInput = $(orderItemFields).find('.unit_price');
                let totalPriceInput = $(orderItemFields).find('.total_price');

                $(orderItemFields).find('.purchase-item-product-name').html('Name: ' + data.name + '<br>Sku: ' + data.sku + '<br>On hand: ' + data.quantity_on_hand + '<br>Backordered: ' + data.quantity_backordered);

                countTotalProductPrice(orderedInput, unitPriceInput, totalPriceInput)

                statusMessage(
                    orderItemFields.find('.status'),
                    orderItemFields.find('.received'),
                    orderItemFields.find('.ordered')
                );
            }

            orderItemFields.closest('tr').removeClass('d-none');
        }

        productSelect.on('select2:select', function (e) {
            let data = e.params.data;
            let orderItems = $('.order-item-fields');
            let skip = false;

            orderItems.each(function() {
                if ($(this).find('.purchase-order-item-product-id input').val() == data.id) {
                    skip = true;
                }
            });

            if (!skip) {
                addPurchaseOrderItem(data);
            }

            $(this).val(null).trigger('change');
        });

        let columns = [
            {
                "title": "",
                "data": function () {
                    return '<i class="ni ni-zoom-split-in preview-button"></i>'
                },
                "name": "purchase_orders.id",
                "orderable": false
            },
            {
                "title": "Number",
                "data": function (data) {
                    let editLink = '<a href="' + data['link_edit'] + '">' + data['number'] + '</a>';

                    return editLink
                },
                "name": "purchase_orders.number"
            },
            {
                "title": "Status",
                "data": function (data) {
                    let status = data['status'].replace("_", " ");
                    let icon = 'sts-cancelled';

                    if (status == 'warehouse_complete') {
                        icon = 'sts-pending';
                    } else if (status == 'closed') {
                        icon = 'sts-fulfilled';
                    }

                    return '<span class="badge badge-dot mr-4">\n' +
                        '                      <i class="' + icon + '"></i>\n' +
                        '                      <span class="status text-capitalize">' + status + '</span>\n' +
                        '                    </span>';
                },
                "name": "purchase_orders.status"
            },
            {"title": "Created date", "data": "created_at", "name": "purchase_orders.ordered_at"},
            {"title": "Expected date", "data": "expected_at", "name": "purchase_orders.expected_at"},
            {
                "title": "Vendor",
                "name": "supplier_contact_information.name",
                "data": function (data) {
                    return '<a href="' + data.supplier['url'] + '" style="display: inline-block">' + data.supplier['name'] + '</a>'
                }
            },
            {
                "title": "Warehouse",
                "name": "warehouse_contact_information.name",
                "data": "warehouse"
            },
        ];

        // START Hide/Show columns
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#purchase-orders-table').DataTable(
            {
                serverSide: true,
                ajax: '/purchase_orders/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[1, 'desc']],
                search: {
                    search: JSON.stringify( {filterArray:[
                            {columnName:"table_search", value: $('#global-search-input').val()},
                            {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                        ]
                    })
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function()
                {
                    let tableId = "#purchase-orders-table"
                    let table = $("#orders-table")

                    table.on('draw.dt', function (){
                        $(table).css('opacity', 1)
                    })
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete(tableId, 'user-settings/hide-columns',  'purchase_order_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            }
        );

        $(document).on('click', '#purchase-orders-table .preview-button', function (event) {
            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "purchase_orders/" + id + "/preview");
        });

        function countTotalProductPrice(orderedInput, unitPriceInput, totalPriceInput) {
            $(orderedInput).on('keyup', function () {
                totalPriceInput.html(Number(orderedInput.val()) * Number(unitPriceInput.val()))
            })

            $(unitPriceInput).on('keyup', function () {
                totalPriceInput.html(Number(orderedInput.val()) * Number(unitPriceInput.val()))
            })
        }

        $('.ordered').each(function () {
            let orderedInput = $(this)
            let unitPriceInput = $(this).closest('tr').find('.unit_price')
            let totalPriceInput = $(this).closest('tr').find('.total_price')
            let receivedInput = $(this).closest('tr').find('.received')
            let statusInput = $(this).closest('tr').find('.status')

            countTotalProductPrice(orderedInput, unitPriceInput, totalPriceInput)
            statusMessage(statusInput, receivedInput, orderedInput)

            orderedInput.trigger('keyup')
        })

        function statusMessage(statusInput, receivedInput, orderedInput) {
            $(orderedInput).on('change keyup', function () {
                compareQuantity()
            })

            let compareQuantity = function () {
                let status = 'pending';
                let received = Number(receivedInput.html())
                let ordered = Number(orderedInput.val())

                if (received == 0) {
                    status = 'pending';
                } else if (received < ordered) {
                    status = 'under received';
                } else if (received == ordered) {
                    status = 'received';
                } else if (received > ordered) {
                    status = 'over received';
                }
                statusInput.html(status)
            }
        }

        function checkForDoubles(item) {
            $(item).select2({
                templateResult: function (result) {
                    let prodIds = []

                    $('#item_container').find('.product_id').each(function() {
                        prodIds.push($(this).val())
                    })

                    if(result.id) {
                        if(prodIds.includes(result.id.toString())) {
                            return ;
                        }
                    }

                    return result.text
                }
            });
        }

        $(document).on('click', '.auto-po-items', function (event) {
            event.preventDefault();

            $('#item_container').find('tr').each(function () {
                $(this).find('.remove-purchase-order-item').trigger('click')
            })

            $.get($(this).attr("data-url") + supplierSelect.val(), function(data) {
                $.map( data.results, function(result) {
                    addPurchaseOrderItem(result);
                })
            });
        });

        checkForDoubles($('.product-select'));

        let changeLogColumns = [
            {"title": "Date", "data": "created_at", "name": "object_changes.created_at"},
            {
                "title": "Purchase Order number",
                "data": function(data) {
                    return '<a href="' + data.purchase_order['url'] + '" style="display: inline-block"> ' + data.purchase_order['number'] + ' </a>'
                },
                "name": "purchase_orders.number"
            },
            {"title": "Product", "data": "product_name", "name": "purchase_order_items.name"},
            {"title": "Property", "data": "column", "name": "object_changes.column"},
            {"title": "Old Value", "data": "old_value", "name": "object_changes.old_value"},
            {"title": "New Value", "data": "new_value", "name": "object_changes.new_value"},
        ];

        if ($('#change-log-purchase-order-id').val()) {
            changeLogColumns.splice(1, 1);
        }

        $('#purchase-order-change-log-table').DataTable({
            serverSide: true,
            ajax: '/purchase_orders/log_data_table/' + $('#change-log-purchase-order-id').val(),
            responsive: true,
            pagingType: "simple_numbers",
            scrollX: true,
            pageLength: 20,
            order: [[1, 'desc']],
            search: {
                search: JSON.stringify( {filterArray:[
                        {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                    ]
                })
            },
            sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
            preDrawCallback: function(  ) {
                $('.table-card').addClass('with-loader with-opacity-background');
            },
            drawCallback: function(  ) {
                $('.table-card').removeClass('with-loader with-opacity-background');
            },
            createdRow: function( row, data, dataIndex ) {
                $(row).attr( 'data-id', data['id'] );
            },
            columns: changeLogColumns,
        });
    });
};
