window.ReturnOrderForm = function () {
    $(document).ready(function() {
        let orderSelect = $('.order-select-create');
        let customerSelect = $('.customer_id');
        let warehouseSelect = $('.warehouse-select');

        function customerSelection() {
            if ($('.customer_id').val() > 0) {
                $('.order-select-create').removeAttr('disabled');
                setCustomerToOrderUrl();
            } else {
                $('.order-select-create').attr('disabled', true);
            }
        }

        customerSelect.on('change', function (event) {
            customerSelection();
        });

        function setCustomerToOrderUrl() {
            let customerId = $('.customer_id').val();
            let orderSelect = $('.order-select-create');

            if (customerId && $(orderSelect)[0]) {
                changeSelectInputUrlAjax($(orderSelect), customerId);
            }
        }

        function changeSelectInputUrlAjax(selectInputToChange, value) {
            selectInputToChange.select2('destroy');
            let url = selectInputToChange.data('ajax--url');
            let urlLast = url.substring(url.lastIndexOf('/'));

            if (urlLast == '/filter_orders') {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url') + '/' + value);
            } else {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url').replace(/\/\w+?$/, '/' + value));
            }

            selectInputToChange.select2();
        }

        $(document).on('click', 'label[for*="shipping-method"]', function (event) {
            let carrierId = $(this).attr('data-carrier-id');
            let methodId = $(this).attr('data-method-id');
            let price = $(this).attr('data-price');
            let input = $(this).closest('input');
            let shippingMethods = $(".shipping-methods-table tr");
            let tr = $(this).closest('tr');

            $('input[name="shipping_carrier_id"]').val(carrierId);
            $('input[name="shipping_method_id"]').val(methodId);
            $('input[name="shipping_price"]').val(price);

            $("input[id*='shipping-method']").prop('checked', false);
            input.prop('checked', true);

            shippingMethods.removeClass('active');
            tr.addClass('active');

            $('a[data-target="#shipping-modal"]').text(tr.find('td:eq(1)').text() + ' - ' + tr.find('td:eq(2)').text());
        });

        function checkShipping() {
            let carrierId = $('input[name="shipping_carrier_id"]').val();
            let methodId = $('input[name="shipping_method_id"]').val();
            let button = $('input[id="shipping-method-' + carrierId + '-' + methodId + '"]');
            let tr = button.closest('tr');

            if (button.length) {
                $('a[data-target="#shipping-modal"]').text(tr.find('td:eq(1)').text() + ' - ' + tr.find('td:eq(2)').text());
                button.prop('checked', true);
            }
        }

        checkShipping();

        let columns = [
            {
                "title": "",
                "data": function () {
                    return '<i class="ni ni-zoom-split-in preview-button"></i>'
                },
                "name": "orders.id",
                "orderable": false
            },
            {
                "title": "Number RMA",
                "name": "returns.number",
                "data": function(data) {
                    return '<a href="' + data.returned_order['url'] + '" style="display: inline-block"> ' + data.returned_order['number'] + ' </a>'
                }
            },
            {
                "title": "Order Number",
                "name": "orders.number",
                "data": function(data) {
                    return '<a href="' + data.order['url'] + '" style="display: inline-block"> ' + data.order['number'] + ' </a>'
                }
            },
            {
                "title": "Customer Name",
                "name": "contact_informations.name",
                "visible": !$('body').attr('data-current-customer'),
                "data": function(data) {
                    return '<a href="' + data.order_customer['url'] + '" style="display: inline-block"> ' + data.order_customer['name'] + ' </a>'
                }
            },
            {
                "title": "Requested At",
                "name": "returns.requested_at",
                "data":'requested_at'
            },
            {
                "title": "City",
                "name": "contact_informations.city",
                "data":'city'
            },
            {
                "title": "Zip Code",
                "name": "contact_informations.zip",
                "data":'zip'
            },
            {
                "title": "Status",
                "data": function (data) {
                    let status = data['status'].replace("_", " ");
                    let icon = '';

                    if (status == 'pending') {
                        icon = 'sts-pending';
                    } else if (status == 'warehouse complete') {
                        icon = 'sts-fulfilled';
                    } else if (status == 'complete') {
                        icon = 'sts-on-hold';
                    }

                    return '<span class="badge badge-dot mr-4">\n' +
                        '<i class="' + icon + '"></i>\n' +
                        '<span class="status text-capitalize">' + status + '</span>\n' +
                        '</span>';
                },
                "name": "returns.status"
            },
        ];

        // START Hide/Show columns
        if ($('body').attr('data-current-customer')) {
            columns.forEach(function (value, number) {
                if(value.name == 'contact_informations.name') {
                    columns.splice(number, 1);
                }
            })
        }
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#returns-table').DataTable(
            {
                serverSide: true,
                ajax: '/returns/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[1, 'desc']],
                search: {
                    search: JSON.stringify( {filterArray:[
                            {columnName:"table_search", value: $('#global-search-input').val()},
                            {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                        ]
                    })
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#returns-table', 'user-settings/hide-columns',  'return_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            }
        );

        $(document).on('click', '#returns-table .preview-button', function (event) {
            let id = $(event.target).closest('tr').attr('data-id');
            Preview.loadPreview(event, "returns/" + id + "/preview");
        });

        function getOrderItems(orderId) {
            $.get("/returns/get_order_products/" + orderId, function(data, status) {
                let results = data.results;

                $.map( results, function(result) {
                    addOrderItem();

                    let orderItem = $('.order-item-fields:last');
                    let defaultSrc = orderItem.find('img').attr('data-default-src');

                    orderItem.find('input[name*="order_item_id"]').val(result.id);
                    orderItem.find('.return-item-product-name').html('Name: ' + result.name + '<br>Sku: ' + result.sku);
                    orderItem.find('.return-item-quantity-ordered').text(result.quantity);
                    orderItem.find('input[name*="quantity"]').val(0).attr('max', result.quantity);

                    orderItem.find('img').attr('src', defaultSrc);

                    orderItem.find('.is-invalid').each(function () {
                        $(this).removeClass('is-invalid')
                        $(this).closest('tr').find('.invalid-feedback').remove()
                    })


                    if (result.image) {
                        orderItem.find('img').attr('src', result.image);
                    }

                    orderItem.removeClass('d-none');
                })
            });
        }

        function addOrderItem () {
            let lastOrderItem = $('.order-item-fields').last();

            if (!lastOrderItem.hasClass('d-none')) {
                let lastOrderItemHtml = lastOrderItem[0].outerHTML;
                let index = lastOrderItemHtml.match(/\[(\d+?)\]/);
                let orderItemFields = $(lastOrderItemHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

                $('.return-items tbody').append(orderItemFields);
            }
        }

        function removeOrderItems () {
            let orderItems = $('.order-item-fields');

            orderItems.each(function(){
                let defaultSrc = $(this).find('img').attr('default-src');

                if($(this).index()){
                    $(this).remove();
                } else {
                    $(this).find('input[name*="order_item_id"]').val(0);
                    $(this).find('input[name*="name"]').val('');
                    $(this).find('input[name*="quantity"]').val(0);
                    $(this).find('img').attr('src', defaultSrc);
                    $(this).addClass('d-none');
                }
            });
        }

        orderSelect.on('select2:select', function (e) {
            let data = e.params.data;
            let orderId = data.id;

            removeOrderItems();
            getOrderItems(orderId);
        });

        customerSelect.on('select2:select', function () {
            warehouseSelect.val(null).trigger('change');
            orderSelect.val(null).trigger('change');

            removeOrderItems();

        })

        warehouseSelect.on('select2:select', function () {
            orderSelect.val(null).trigger('change');
            removeOrderItems();

        })

        customerSelection();

        $("#create-label").change(function () {
            if (this.checked) {
                $(".return-shipment").addClass('d-none');
            } else {
                $(".return-shipment").removeClass('d-none');
            }
        })

        $(document).on('click', '.return-item-check', function() {
            if (this.checked) {
                $(this).parent().parent().siblings('.return-quantity-td').removeClass('d-none');
                $(this).parent().parent().siblings('.return-unchecked-td').addClass('d-none');
            } else {
                $(this).parent().parent().siblings('.return-quantity-td').find('.reset-value').val(0);
                $(this).parent().parent().siblings('.return-quantity-td').addClass('d-none');
                $(this).parent().parent().siblings('.return-unchecked-td').removeClass('d-none');
            }
        });
    });
};
