window.BillingFeeCheckForDuplicateFees = function () {
    $(document).ready(function() {
        $('form').submit(function( event ) {
            event.preventDefault();
            let data = $(this).serializeArray()
            let url = $('#fee-duplicate-check').attr('action')
            let billingFeeFeedBack = '.billingFeeFeedBack';
            console.log(url)

            $.ajax({
                type: "POST",
                data: data,
                url:  url,
                success: function(data)
                {
                    $(billingFeeFeedBack).removeClass('alert-danger').addClass('alert-success')
                    $(billingFeeFeedBack).show().html(data.message)

                    if (data.redirect) {
                        setTimeout(function() {
                            window.location.href = data.url
                        }, 1000)
                    }
                },
                error: function(xhr, status, data)
                {
                    let errorsObject = xhr.responseJSON.errors;

                    $(billingFeeFeedBack).removeClass('alert-success').addClass('alert-danger')
                    $(billingFeeFeedBack).show().html(Object.values(errorsObject).map(function (a){return a + '<br>'}))
                }
            })
        });
    });
};
