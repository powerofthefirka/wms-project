window.CurrencyForm = function (event) {
    $(document).ready(function(e) {

        let columns = [
            {"title": "Name", "data": "name", "name": "pricing_plans.name"},
            {"title": "Code", "data": "code", "name": "pricing_plans.code"},
        ];

        $('#currency-table').DataTable(
            {
                serverSide: true,
                ajax: '/three_pls/currencies/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[0, 'desc']],
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });

        $(document).on('click', '#currency-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            let id = $(event.target).closest('tr').attr('data-id');
            window.location.href = '/three_pls/currencies/' + id + '/edit'
        });
    });
};
