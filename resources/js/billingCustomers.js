window.BillingCustomers = function () {
    $(document).ready(function() {
        $('#customer-table').DataTable(
            {
                serverSide: true,
                ajax: '/customers/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {
                        "title": "Name",
                        "data": "name",
                        "name": "contact_informations.name"
                    },
                    {"title": "Customer ID", "data": "id", "name": "customers.id"},
                    {
                        "title": "Billing Profile",
                        "data": function(data) {
                            return '<a href="' + data['billing_profile']['url'] + '" style="display: inline-block"> ' + data['billing_profile']['name'] + ' </a>';
                        },
                        "name": "customers.id"
                    },
                    {
                        "title": "Last Billed ( amount )",
                        "data": function(data) {
                            return '<a href="' + data['last_billed']['url'] + '" style="display: inline-block"> ' + data['last_billed']['amount'] + ' </a>';
                        },
                        "name": "customers.id"
                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
            }
        );

        $(document).on('click', '#customer-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                let id = $(event.target).closest('tr').attr('data-id')
                window.location.href = 'customers/' + id + '/bills'
            }
        });

        if (typeof customerId != 'undefined') {
            $('#customer-bills-table').DataTable(
                {
                    serverSide: true,
                    ajax: '/billings/customers/'+ customerId +'/bills/data_table',
                    responsive: true,
                    pagingType: "simple_numbers",
                    scrollX: true,
                    pageLength: 20,
                    search: {
                        search: $('#global-search-input').val()
                    },
                    sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                    columns: [
                        {"title": "Period Start", "data": "period_start", "name": "period_start"},
                        {"title": "Period End", "data": "period_end", "name": "period_end"},
                        {"title": "Amount", "data": "amount", "name": "amount"},
                        {"title": "Export",
                            "data": function(data) {
                                return '<a class="ignore" target="_blank" href="/bills/' + data['id'] + '/export_csv" style="display: inline-block"> Export </a>';
                            }, "name": "id"},
                        {"title": "Calculated at", "data": "calculated_at", "name": "calculated_at"},
                        {"title": "Actions",
                            "data": function (data) {
                                // <form method="POST"
                                //       action="{{route('bills.destroy', ['bill' => $bill, 'customer' => $customer])}}"
                                //       style="display: inline-block">
                                //     @csrf
                                //     @method('delete')
                                //     <button type="submit"
                                //             className="btn btn-sm btn-danger text-white recalculate">{{
                                //         __(
                                //         'Delete Bill')}}</button>
                                // </form>
                                let token = $('meta[name="csrf-token"]').attr('content');

                                let recalculateBtn = '<form method="POST" action="/bills/' + data.id + '/recalculate" style="display: inline-block">' +
                                    '<input type="hidden" name="_token" value="'+ token +'"/>' +
                                    '<button type="submit" class="btn btn-sm btn-primary text-white recalculate">Recalculate</button>' +
                                    '</form>'

                                let deleteBtn = '<form method="POST" action="/bills/' + data.id + '" style="display: inline-block">' +
                                    '<input type="hidden" name="_token" value="'+ token +'"/>' +
                                    '<input type="hidden" name="_method" value="delete"/>' +
                                    '<button type="submit" class="btn btn-sm btn-danger text-white">Delete</button>' +
                                    '</form>'

                                return recalculateBtn + deleteBtn
                            },
                            "name": "id"
                        },

                    ],
                    createdRow: function( row, data, dataIndex ) {
                        $(row).attr( 'data-id', data['id'] );
                    },
                }
            );

            $(document).on('click', '#customer-bills-table tbody tr', function (event) {
                if ($(this).find('.dataTables_empty').length) return false;

                if( document.getSelection().toString() === '' ) {
                    let id = $(event.target).closest('tr').attr('data-id')

                    if($(event.target)[0].className != 'ignore') {
                        window.location.href = '/billings/customers/' + customerId + '/bills/' + id + '/items'
                    }
                }
            });
        }

    });
};
