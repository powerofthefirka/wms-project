window.WarehouseForm = function () {
    $(document).ready(function() {
        $('#warehouses-table').DataTable(
            {
                serverSide: true,
                ajax: '/warehouses/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                initComplete: function()
                {
                    var dtable = $("#warehouses-table").dataTable().api()
                    $("#warehouse-search")
                        .unbind() // Unbind previous default bindings
                        .bind("input", function(e) { // Bind our desired behavior
                            // If the length is 3 or more characters, or the user pressed ENTER, search
                            if(this.value.length >= 3) {
                                // Call the API search function
                                dtable.search(this.value).draw();
                            }
                            // Ensure we clear the search if they backspace far enough
                            if(this.value == "") {
                                dtable.search("").draw();
                            }
                            return;
                        });
                },
                columns: [
                    {
                        "title": "Name",
                        "name": "contact_informations.name",
                        "data": function (data) {
                            return '<a data-warehouse-id="' + data.warehouse_id + '">' + data.warehouse_name + '</a>';
                        }
                    },
                    {"title": "Address", "data": "warehouse_address", "name": "contact_informations.address"},
                    {"title": "Zip", "data": "warehouse_zip", "name": "contact_informations.zip"},
                    {"title": "City", "data": "warehouse_city", "name": "contact_informations.city"},
                    {"title": "Email", "data": "warehouse_email", "name": "contact_informations.email"},
                    {"title": "Phone", "data": "warehouse_phone", "name": "contact_informations.phone"},
                    {
                        "title": "Customer Name",
                        "data": function (data) {
                            return '<a href="/customers/' + data.customer['id'] + '/edit">' + data.customer['name'] + '</a>'
                        },
                        "name": "customer_contact_information.name",
                        "visible": !$('body').attr('data-current-customer')
                    }
                ],
            });

        $(document).on('click', '#warehouses-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = '/locations/' + $(event.target).parent().find('[data-warehouse-id]').data('warehouse-id')
            }
        });

    });
};
