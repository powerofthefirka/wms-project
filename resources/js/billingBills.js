window.BillingBills= function () {
    $(document).ready(function() {
        $('#bills-table').DataTable(
            {
                serverSide: true,
                ajax: '/billings/bills/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {
                        "title": "Customer",
                        "data": function(data) {
                            return '<a href="' + data['customer']['url'] + '" style="display: inline-block"> ' + data['customer']['name'] + ' </a>';
                        },
                        "name": "bills.customer_id"
                    },
                    {
                        "title": "Billing Profile",
                        "data": function(data) {
                            return '<a href="' + data['billing_profile']['url'] + '" style="display: inline-block"> ' + data['billing_profile']['name'] + ' </a>';
                        },
                        "name": "bills.billing_profile_id"
                    },
                    {
                        "title": "Export",
                        "data": function(data) {
                            return '<a href="/bills/' + data['id'] + '/export_csv" target="_blank" style="display: inline-block"> Export </a>';
                        },
                        "name": "id"
                    },
                    {
                        "title": "Period Start",
                        "data": "period_start",
                        "name": "bills.period_start"
                    },
                    {
                        "title": "Period End",
                        "data": "period_end",
                        "name": "bills.period_end"
                    },
                    {
                        "title": "Amount",
                        "data": "amount",
                        "name": "bills.amount"
                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                    $(row).attr( 'data-customer-id', data['customer']['id'] );
                },
            }
        );

        $(document).on('click', '#bills-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                let id = $(event.target).closest('tr').attr('data-id')
                let customerId = $(event.target).closest('tr').attr('data-customer-id')

                if (typeof id !== 'undefined') {
                    window.location.href = '/billings/customers/'+customerId+'/bills/'+id+'/items'
                }
            }
        });
    });
};
