window.LocationForm = function (event) {
    $(document).ready(function(e) {

        checkDeleteButton();
        deleteItemFromTableButton();

        $('#add_item').click(function (event) {
            event.preventDefault();
            let lastOrderItemFields = $('.order-item-fields:not(.order-item-deleted):last');

            lastOrderItemFields.find('select').select2('destroy');

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            $('.order-item-fields:last').find('input[type=hidden]').remove();
            $('.order-item-fields:last').show();

            lastOrderItemFields.find('select').select2();
            orderItemFields.find('select').select2();

            checkDeleteButton();
        });

        $(document).on('click', '.transfer', function (event) {
            const product = $(this).parent().parent().find('#selected_product').val();
            $('#productId').val(product)
        });

        let warehouseId = $('#location-table').data('warehouse-id');
        $('#location-table').DataTable(
            {
                serverSide: true,
                ajax: '/locations/data_table?warehouse_id=' + warehouseId,
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                sDom: '<"top">rt<"bottom"<"col col-12"p>>',
                "language": {
                    "paginate": {
                        "previous": "<",
                        "next": ">"
                    }
                },
                initComplete: function()
                {
                    var dtable = $("#location-table").dataTable().api()
                    $("#location-search")
                        .unbind() // Unbind previous default bindings
                        .bind("input", function(e) { // Bind our desired behavior
                            // If the length is 3 or more characters, or the user pressed ENTER, search
                            if(this.value.length >= 3) {
                                // Call the API search function
                                dtable.search(this.value).draw();
                            }
                            // Ensure we clear the search if they backspace far enough
                            if(this.value == "") {
                                dtable.search("").draw();
                            }
                            return;
                        });
                },
                columns: [
                    {"title": "Name", "data": "location_name", "name": "locations.name"},
                    {
                        "title": "Pickable",
                        "name": "locations.pickable",
                        "data": function (data) {
                            return data.location_pickable === 1 ? 'YES' : 'NO';
                        },
                    },
                    {
                        "title": "Warehouse",
                        "name": "contact_informations.name",
                        "data": 'warehouse_name.name'
                    },
                    {"title": "Address", "data": "warehouse_address", "name": "contact_informations.address"},
                    {"title": "Zip", "data": "warehouse_zip", "name": "contact_informations.zip"},
                    {"title": "City", "data": "warehouse_city", "name": "contact_informations.city"},
                    {"title": "Email", "data": "warehouse_email", "name": "contact_informations.email"},
                    {"title": "Phone", "data": "warehouse_phone", "name": "contact_informations.phone"},
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            return '<a href="' + data['link_edit'] + '" class="btn btn-primary btn-sm view" style="display: inline-block"> View </a>';
                        },
                    },
                ],
            });

        $(document).on('click', '#location-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.view').attr('href')
            }
        });

    });
};
