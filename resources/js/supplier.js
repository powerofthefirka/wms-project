window.SupplierForm = function (event) {
    $(document).ready(function(e) {
        let productSelect = $('.product_id');
        let customerSelect = $('.customer_id');

        function customerSelection() {
            if(window.location.href.search('/edit') < 0) {
                if ($('.customer_id').val() > 0) {
                    productSelect.removeAttr('disabled');
                } else {
                    productSelect.attr('disabled', true);
                }
            }
        }

        $(document).on('click', '.remove-supplier-item', function (event) {
            event.preventDefault();

            let orderItem = $(this).closest('.order-item-fields');
            orderItem.find('.product_supplier').val('');
            orderItem.removeClass('d-block').addClass('d-none');
        });

        customerSelect.on('change', function (event) {
            customerSelection();
            setCustomerToProductUrl(productSelect);
            $('#item_container').find('.remove-item').trigger('click')
        }).trigger('change');

        function setCustomerToProductUrl(productSelect) {
            let customerId = customerSelect.val();

            if (customerId) {
                changeSelectInputUrlAjax($(productSelect), customerId);
            }
        }

        function changeSelectInputUrlAjax(selectInputToChange, value) {
            selectInputToChange.select2('destroy');
            let url = selectInputToChange.data('ajax--url');
            let urlLast = url.substring(url.lastIndexOf('/'));

            if (urlLast == '/filter_products') {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url') + '/' + value);
            } else {
                selectInputToChange.data('ajax--url', selectInputToChange.data('ajax--url').replace(/\/\w+?$/, '/' + value));
            }

            selectInputToChange.select2();
        }

        let columns = [
            {
                "title": "Name",
                "data": function (data) {
                    return '<a href="' + data.supplier['url'] + '">' + data.supplier['name'] + '</a>'
                },
                "name": "contact_informations.name"
            },
            {"title": "Address", "data": "supplier_address", "name": "contact_informations.address"},
            {"title": "Zip", "data": "supplier_zip", "name": "contact_informations.zip"},
            {"title": "City", "data": "supplier_city", "name": "contact_informations.city"},
            {"title": "Email", "data": "supplier_email", "name": "contact_informations.email"},
            {"title": "Phone", "data": "supplier_phone", "name": "contact_informations.phone"},
            {
                "title": "Customer",
                "name": "customer_contact_information.name",
                "visible": !$('body').attr('data-current-customer'),
                "data": function (data) {
                    return '<a href="' + data.customer['url'] + '">' + data.customer['name'] + '</a>'
                }
            },
            {"title": "Currency", "data": "currency", "name": "suppliers.currency"},
            {"title": "Internal Note", "data": "internal_note", "name": "suppliers.internal_note"},
            {"title": "Default PO Note", "data": "default_po_note", "name": "suppliers.default_po_note"},
        ];

        // START Hide/Show columns
        if ($('body').attr('data-current-customer')) {
            columns.forEach(function (value, number) {
                if(value.name == 'customer_contact_information.name') {
                    columns.splice(number, 1);
                }
            })
        }
        new ColumnVisibilityBeforeTableLoad(columns)
        // END Hide/Show columns

        $('#supplier-table').DataTable(
            {
                serverSide: true,
                ajax: '/vendors/data_table',
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                order: [[0, 'desc']],
                search: {
                    search: JSON.stringify( {filterArray:[
                            {columnName:"table_search", value: $('#global-search-input').val()},
                            {columnName:"dates_between", value: $('input[name="dates_between"]').val()}
                        ]
                    })
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                preDrawCallback: function(  ) {
                    $('.table-card').addClass('with-loader with-opacity-background');
                },
                drawCallback: function(  ) {
                    $('.table-card').removeClass('with-loader with-opacity-background');
                },
                initComplete: function()
                {
                    // START Hide/Show columns
                    new ShowHideColumnInitComplete('#supplier-table', 'user-settings/hide-columns',  'suppliers_table_hide_columns');
                    // END Hide/Show columns
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr( 'data-id', data['id'] );
                },
                columns: columns,
            });


        checkDeleteButton();
        deleteItemFromTableButton();
        removeItemFromTableButton();

        function addProduct() {
            let lastOrderItemFields = $('.order-item-fields:not(.order-item-deleted):last');

            if (lastOrderItemFields.find('.product_supplier').val() == '') {
                lastOrderItemFields.remove()
            }

            let orderItemFieldsHtml = lastOrderItemFields[0].outerHTML;
            let index = orderItemFieldsHtml.match(/\[(\d+?)\]/);
            let orderItemFields = $(orderItemFieldsHtml.replace(/\[\d+?\]/g, '[' + (parseInt(index[1]) + 1) + ']'));

            $('#item_container').append(orderItemFields);
            orderItemFields.find('span').html('');
        }

        productSelect.on('select2:select', function (e) {
            let data = e.params.data;

            addProduct();

            let orderItems = $('.order-item-fields')
            let orderItem = orderItems.last();

            orderItem.find('.product_supplier').val(data.id);
            orderItem.find('.product_name').html(data.text);
            orderItem.removeClass('d-none');

            productSelect.val(null).trigger('change');
        });

        function checkForDoubles(item) {
            item.select2({
                templateResult: function (result) {
                    let prodIds = []

                    $('#item_container').find('.product_supplier').each(function(){
                        prodIds.push($(this).val())
                    })

                    if(result.id) {
                        if(prodIds.includes(result.id.toString())) {
                            return ;
                        }
                    }

                    return result.text
                }
            });
        }

        checkForDoubles(productSelect);
        customerSelection();

    });
};
