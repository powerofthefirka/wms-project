window.CustomerForm = function () {
    $(document).ready(function() {
        let shippingMethods = $('.shipping-methods');
        let locations = $('.locations');

        $('#customer-table').DataTable(
            {
                serverSide: true,
                ajax: {
                    url: '/customers/data_table',
                    data: {
                        'include_integration_dates' : true
                    }
                },
                responsive: true,
                pagingType: "simple_numbers",
                scrollX: true,
                pageLength: 20,
                search: {
                    search: $('#global-search-input').val()
                },
                sDom: '<"top">rt<"bottom"<"col col-12"ip>>',
                columns: [
                    {"title": "Name", "data": "name", "name": "contact_informations.name"},
                    {"title": "Products Updated At", "data": "date_related_data.product_fetched_at", "name": "date_related_data.product_fetched_at"},
                    {"title": "Orders Updated At", "data": "date_related_data.order_fetched_at", "name": "date_related_data.order_fetched_at"},
                    {"title": "PO updated at", "data": "date_related_data.purchase_order_fetched_at", "name": "date_related_data.purchase_order_fetched_at"},
                    {"title": "Returns updated at", "data": "date_related_data.return_fetched_at", "name": "date_related_data.return_fetched_at"},
                    {"title": "shipments updated at", "data": "date_related_data.shipment_fetched_at", "name": "date_related_data.shipment_fetched_at"},
                    {"title": "Inventory updated at", "data": "date_related_data.inventory_changes_fetched_at", "name": "date_related_data.inventory_changes_fetched_at"},
                    {
                        "orderable": false,
                        "class":"text-center",
                        "title": "Actions",
                        "data": function (data) {
                            let integrationStatusButton = '';
                            if (data['active_integration_edit'].length > 0) {
                                integrationStatusButton = '<a href="' + data['active_integration_edit'] + '" class="btn btn-' + (data['active_integration'] ? 'danger' : 'success' ) + ' edit btn-sm" style="display: inline-block">' + (data['active_integration'] ? 'Disable Integration' : 'Enable Integration') + '</a>';
                            }

                            let editButton = '<a href="' + data['link_edit'] + '" class="btn btn-primary edit btn-sm" style="display: inline-block"> Edit </a>';
                            let disableButton = '<a href="' + data['link_disable'] + '" class="btn btn-' + (data['active'] ? 'danger' : 'primary' ) + ' edit btn-sm ml-2" style="display: inline-block"> ' + (data['active'] ? 'Disable' : 'Enable' ) + ' </a>';

                            let deleteButton =
                                '<form action="' + data.link_delete.url + '" method="post" style="display: inline-block">' +
                                '<input type="hidden" name="_method" value="delete">' +
                                '<input type="hidden" name="_token" value=' + data.link_delete.token + '>' +
                                ' <button type="button" class="btn btn-danger btn-sm" data-confirm-action="Are you sure you want to delete this customer?">Delete</button>'+
                                '</form>';

                            return editButton + integrationStatusButton + deleteButton + disableButton
                        },
                    },
                ],
            }
        );

        $(document).on('click', '#customer-table tbody tr', function (event) {
            if ($(this).find('.dataTables_empty').length) return false;

            if( document.getSelection().toString() === '' ) {
                window.location.href = $(event.target).parent().find('.edit').attr('href')
            }
        });

        if(!$('#chk-custom_shipping_modal').is(':checked')) {
            $('.custom-shipping-modal-text-div').hide();
        }

        $(document).on('click', '#chk-custom_shipping_modal', function (event) {
            if($(this).is(':checked')) {
                $('.custom-shipping-modal-text-div').show();
            } else {
                $('.custom-shipping-modal-text-div').hide();
            }
        });

        $('.3pl_id').on('change', function () {
            shippingMethods.empty();
            locations.empty();

            generateShippingMethods($(this).val());
            generateLocations($(this).val());
        });

        if ($('#3pl_id').length) {
            shippingMethods.empty();
            locations.empty();

            generateShippingMethods($('#3pl_id').val());
            generateLocations($('#3pl_id').val());
        }

        function generateShippingMethods(threePl) {
            $.get("/three_pls/" + threePl + "/all_shipping_methods", function(data, status){
                $.map( data, function(result) {
                    for (let i = 0; i < result.length; i++) {
                        let method = result[i];

                        shippingMethods.append('<div class="custom-control custom-checkbox custom-checkbox-success">' +
                        '<input class= "custom-control-input" type="checkbox" id="chk-shipping_methods[' + i + ']" name="shipping_methods[]" value="' + method.id + '" checked>' +
                        '<label class="custom-control-label" for="chk-shipping_methods[' + i + ']">' + method.text + '</label>' +
                        '</div>');
                    }
                })
            });
        }

        function generateLocations(threePl) {
            $.get("/three_pls/" + threePl + "/all_locations", function(data, status){
                $.map( data, function(result) {
                    for (let i = 0; i < result.length; i++) {
                        let location = result[i];

                        locations.append('<div class="custom-control custom-checkbox custom-checkbox-success">' +
                        '<input class= "custom-control-input" type="checkbox" id="chk-locations[' + i + ']" name="locations[]" value="' + location.id + '" checked>' +
                        '<label class="custom-control-label" for="chk-locations[' + i + ']">' + location.text + '</label>' +
                        '</div>');
                    }
                })
            });
        }
    });
};
