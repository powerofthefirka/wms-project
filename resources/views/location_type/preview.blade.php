<h3 class="mb-3" style="display: inline-block">{{__('Location Type')}}:</h3>
<a href="{{route('location_types.editable_preview', ['location_types.editablePreview' => $locationType])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Name')}}: <strong>{{$locationType->name}}</strong></li>
    <li class="list-group-item">{{__('3PL')}}: <strong>{{$locationType->threePl->contactInformation->name}}</strong></li>
    <li class="list-group-item">{{__('WMS ID')}}: <strong>{{$locationType->wms_id}}</strong></li>
    <li class="list-group-item">{{__('Daily Storage Cost')}}: <strong>{{$locationType->daily_storage_cost}}</strong></li>
</ul>
<div class="row">
    <div class="col text-right">
        <a href="{{route('location_types.edit',['location_type' => $locationType])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('location_types.destroy', ['location_type' => $locationType, 'id' => $locationType->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this Location Type?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
