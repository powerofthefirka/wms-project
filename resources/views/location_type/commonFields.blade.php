@include('shared.forms.ajaxSelect', [
    'url' => route('location_types.filter_3pls'),
    'name' => '3pl_id',
    'className' => 'ajax-user-input',
    'placeholder' => __('Search'),
    'label' => __('3PL'),
    'hidden' => session('customer_id') ? 'd-none' : '',
    'default' => [
        'id' => $locationType->threePl->id ?? old('3pl_id') ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->id,
        'text' => $locationType->threePl->contactInformation->name ?? \App\Models\ThreePl::find(old('3pl_id'))->contactInformation->name ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->contactInformation->name ?? ''
    ]
])
@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $locationType->name ?? ''
])
@include('shared.forms.input', [
    'name' => 'wms_id',
    'label' => __('WMS ID'),
    'value' => $locationType->wms_id ?? ''
])
@include('shared.forms.input', [
    'type' => 'number',
    'name' => 'daily_storage_cost',
    'label' => __('Daily Storage Cost'),
    'value' => $locationType->daily_storage_cost ?? 0
])
