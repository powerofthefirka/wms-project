<h3 class="mb-3">{{__('Quick Edit Location Type')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('location_types.update', ['locationType' => $locationType, 'id' => $locationType->id]) }}"
      autocomplete="off"
>
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'name',
                'label' => __('Name'),
                'value' => $locationType->name ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('location_types.filter_3pls'),
                'name' => '3pl_id',
                'className' => 'ajax-user-input',
                'placeholder' => __('Search'),
                'label' => __('3PL'),
                'hidden' => session('customer_id') ? 'd-none' : '',
                'default' => [
                    'id' => $locationType->threePl->id ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->id ?? old('3pl_id'),
                    'text' => $locationType->threePl->contactInformation->name ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->contactInformation->name ?? ''
                ]
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'wms_id',
                'label' => __('WMS ID'),
                'value' => $locationType->wms_id ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'type' => 'number',
                'name' => 'daily_storage_cost',
                'label' => __('Daily Storage Cost'),
                'value' => $locationType->daily_storage_cost ?? 0
            ])
        </li>
    </ul>
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="location-type-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
