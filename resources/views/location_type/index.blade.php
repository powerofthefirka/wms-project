@extends('layouts.app', ['title' => __('Location Type')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h3 class="mb-0">{{ __('Location Types') }}</h3>
                            </div>
                            <div class="col-6 text-right">
                                <a href="{{ route('location_types.create') }}" class="btn btn-secondary btn-sm">
                                    <i class="ci ci-more"></i>
                                    {{ __('Add Location Type') }}
                                </a>
                                @include('shared.modals.showHideTableColumns')
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="location-type-table" style="width: 100% !important;">
                            <thead class="thead-light"></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new Preview();
        new LocationType();
    </script>
@endpush

