@extends('layouts.app', ['title' => __('Location Type')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Location Type') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('location_types.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="location-type-form" method="post" action="{{ route('location_types.update', ['locationType' => $locationType, 'id' => $locationType->id]) }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('Location Type information') }}</h6>
                            {{ method_field('PUT') }}
                            @include('location_type.commonFields', [
                                'locationType' => $locationType
                            ])
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
