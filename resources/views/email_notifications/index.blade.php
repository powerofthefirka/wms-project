<div class="row align-items-center">
    <div class="col-12 mt-2">
        @include('alerts.success')
        @include('alerts.errors')
    </div>
</div>
<div class="card m-0 mt-2">
    <form method="post" action="{{ route('profiles.email_notifications') }}" autocomplete="off"
          enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="card-body p-0">
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[backorder]"
                       name="email_notifications[backorder]"
                    {{userHasEmailNotification('backorder') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[backorder]">
                    Backorder notification
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[order_cancelled]"
                       name="email_notifications[order_cancelled]"
                    {{userHasEmailNotification('order_cancelled') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[order_cancelled]">
                    Order Cancelled notification
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[order_fulfilled]"
                       name="email_notifications[order_fulfilled]"
                    {{userHasEmailNotification('order_fulfilled') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[order_fulfilled]">
                    Order Fulfilled notification
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[order_on_hold]"
                       name="email_notifications[order_on_hold]"
                    {{userHasEmailNotification('order_on_hold') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[order_on_hold]">
                    Order On Hold notification
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[order_pending]"
                       name="email_notifications[order_pending]"
                    {{userHasEmailNotification('order_pending') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[order_pending]">
                    Order Pending notification
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox"
                       id="email_notifications[order_priority]"
                       name="email_notifications[order_priority]"
                    {{userHasEmailNotification('order_priority') ? 'checked' : ''}}>
                <label class="form-check-label" for="email_notifications[order_priority]">
                    Order Priority notification
                </label>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </form>
</div>
