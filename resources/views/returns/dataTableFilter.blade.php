<div class="col-12">
    <a class="btn btn-sm btn-primary btn-block text-white mb-4 d-block d-sm-none toggle-filter-button">
        <span>{{ __('Show filter') }}</span>
        <span>{{ __('Close filter') }}</span>
    </a>
</div>
<div class="col-12 d-none d-sm-block filter">
    <div class="card">
        <div class="card-header">
            <h3>Filter sources</h3>
        </div>
        <div class="card-body pt-0 pb-0">
            <div class="row">
                <input type="hidden" id="table-id" value="returns-table">
                <div class="form-group col-12 col-md-3">
                    <input type="text" name="table_search"
                           class="form-control table_filter" placeholder="{{ __('Search returns') }}">
                </div>
                <div class="form-group col-12 col-md-3 calendar-input">
                    <input
                        type="text"
                        name="dates_between"
                        class="table-datetimepicker table_filter form-control"
                        placeholder="Dates between"
                        value=""
                    >
                </div>
                <div class="form-group col-12 col-md-3">
                    <select name="ordering" class="form-control ordering">
                        <option value="">
                            {{ __('Sort') }}
                        </option>
                        @foreach(\App\Models\Return_::FILTERABLE_COLUMNS as $item)
                            <option value="{{ $item['column'] . ',' . $item['dir']}}">{{$item['title']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-md-3">
                    <button class="col-12 btn btn-primary text-white">{{ __('Filter Now') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('table-addons-js')
    <script>
        new DataTableAddons();
        let statusesUrl = '/returns/total_returns_by_statuses'
        new FilterBoxes();
    </script>
@endpush
