<h3 class="mb-3">{{__('Quick Edit Return')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('returns.update', ['return' => $return]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    @csrf
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
            'name' => 'number',
            'label' => __('Number'),
            'value' => $return->number ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
            'name' => 'reason',
            'label' => __('Return reason'),
            'value' => $return->reason ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
            'name' => '',
            'label' => __('Order'),
            'value' => $return->order->number ?? '',
            'readOnly' => 'readonly'
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
            'name' => '',
            'label' => __('Order Customer'),
            'value' => $return->order->customer->contactInformation['name'] ?? '',
            'readOnly' => 'readonly'
            ])
        </li>
    </ul>
    <h3 class="mb-3">{{__('Return Items')}}:</h3>

    @foreach($return->returnItems as $key => $returnItem)
        <ul class="list-group mb-3">
            <li class="list-group-item">
                <input type="hidden" name="return_items[{{$key}}][return_item_id]" value="{{$returnItem['id'] }}">
                @include('shared.forms.ajaxSelect', [
                    'url' => route('returns.filter_order_products', [ 'orderId' => $return->order_id ]),
                    'name' => 'return_items[' . $key . '][order_item_id]',
                    'className' => 'ajax-user-input orderProduct',
                    'placeholder' => __('Search'),
                    'label' => __('Product'),
                    'default' => [
                        'id' => $returnItem->id ,
                        'text' => 'SKU: ' . $returnItem->product->sku . ', NAME: ' . $returnItem->product->name
                    ]
                ])
                @include('shared.forms.input', [
                  'name' => 'return_items[' . $key . '][quantity]',
                  'label' => __('Quantity'),
                  'type' => 'number',
                  'step'=> 1,
                  'min' => 'min=0',
                  'oninput' => "oninput=validity.valid||(value='');",
                  'class' => 'reset-value reset_on_delete',
                  'value' => $returnItem->quantity
               ])
                @include('shared.forms.input', [
                   'name' => 'return_items[' . $key . '][quantity_received]',
                   'label' => __('Quantity received'),
                   'type' => 'number',
                   'step'=> 1,
                   'min' => 'min=0',
                   'oninput' => "oninput=validity.valid||(value='');",
                   'class' => 'reset-value reset_on_delete',
                   'value' => 0
               ])
            </li>
        </ul>
    @endforeach
    <div class="row">
        <div class="col text-right">
            <button id="preview-submit-button" data-table_id="returns-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
