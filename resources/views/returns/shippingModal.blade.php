<div class="modal fade" id="shipping-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="shipping-modal">{{ __('Shipping') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <fieldset>
                    <div class="table-responsive">
                        <table class="table align-items-center m-0 shipping-methods-table">
                            <tbody>
                            @foreach($carriers as $carrier)
                                <tr>
                                    <td class="border-0">
                                        <img class="avatar rounded" src="{{ $carrier['image'] }}">
                                    </td>
                                    <td class="border-0 max-width-150 overflow-hidden">
                                        {{ $carrier['text'] }}
                                    </td>
                                    <td class="border-0 max-width-150 overflow-hidden">
                                        {{ $carrier['method_text'] }}
                                    </td>
                                    <td class="border-0">
                                        {{ $carrier['cost_with_currency'] }}
                                    </td>
                                    <td class="border-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="shipping-method-{{ $carrier['id'] }}-{{ $carrier['method_id'] }}">
                                            <label class="custom-control-label" for="shipping-method-{{ $carrier['id'] }}-{{ $carrier['method_id'] }}" data-carrier-id="{{ $carrier['id'] }}" data-method-id="{{ $carrier['method_id'] }}" data-price="{{ $carrier['method_cost'] }}"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-none">
                        @include('shared.forms.input', [
                            'name' => 'shipping_carrier_id',
                            'type' => 'number',
                            'label' => 'Shipping Carrier',
                            'value' => $return->shipping_carrier_id ?? old('shipping_carrier_id') ?? ''
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_method_id',
                            'type' => 'number',
                            'label' => 'Shipping Method',
                            'value' => $return->shipping_method_id ?? old('shipping_method_id') ?? ''
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_price',
                            'label' => 'Shipping Price',
                            'value' => $return->shipping_price ?? old('shipping_price') ?? '0'
                         ])
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>
