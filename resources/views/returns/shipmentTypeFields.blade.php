<a href="#" data-toggle="modal" data-target="#shipping-modal" class="mb-2 col col-12 btn btn-primary text-white {{$errors->has('shipping_method_id') ? 'bg-danger' : ''}}">
    {{ __('Add shipping') }}
</a>
