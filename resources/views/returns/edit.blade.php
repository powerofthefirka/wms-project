@extends('layouts.app', ['title' => __('Returns'), 'submenu' => 'returns.menu'])

@section('content')
    <div class="container-fluid">
        <form class="" method="post" action="{{ route('returns.update', ['return' => $return]) }}" autocomplete="off" novalidate>
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Order Return Information') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-12">
                                    <div class="form-group {{session('customer_id') ? 'd-none' : ''}}">
                                        <label class="form-control-label">{{ __('Customer') }}</label><br>
                                        <span>{{$return->order->customer->contactInformation->name}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">{{ __('Warehouse') }}</label><br>
                                        <span>{{$return->warehouse->contactInformation->name ?? ''}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">{{ __('Order') }}</label><br>
                                        <span>{{$return->order->number}}</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">{{ __('Return Identification (RMA#)') }}</label><br>
                                        <span>{{$return->number}}</span>
                                    </div>
                                    @include('shared.forms.input', [
                                       'name' => 'reason',
                                       'label' => __('Return reason'),
                                       'value' => $return->reason
                                    ])
                                    @include('returns.returnInformationFields')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Products') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center overflow-hidden">
                                <thead class="">
                                <tr>
                                    <th scope="col">{{ __('Image') }}</th>
                                    <th scope="col">{{ __('Product') }}</th>
                                    <th scope="col">{{ __('Quantity ordered') }}</th>
                                    <th scope="col">{{ __('Quantity') }}</th>
                                    <th scope="col">{{ __('Quantity returned') }}
                                </tr>
                                </thead>
                                <tbody class="list" id="item_container">
                                @foreach($return->returnItems as $key => $returnItem)
                                    @include('returns.orderItemLine')
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @include('returns.shippingModal')
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('returns.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        new ReturnOrderForm();
    </script>
@endpush
