<div class="custom-control custom-checkbox mb-4 ml-4">
    <input type="hidden" name="create_label" value="1">
    <input type="checkbox" class="custom-control-input" id="create-label" name="create_label" value="0" {{ !empty($return) ? ($return->create_label ? '' : 'checked') : (empty($createMode) ? '' : (empty(old()['create_label']) ? 'checked' : '')) }}>
    <label class="custom-control-label" for="create-label">{{ __('Customer is Providing their own label') }}</label>
</div>