<h3 class="mb-3" style="display: inline-block">{{__('Return')}}:</h3>
<a href="{{route('returns.editable_preview', ['return' => $return])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Number')}}: <strong>{{$return->number}}</strong></li>
    <li class="list-group-item">{{__('Order')}}: <strong>{{$return->order->number}}</strong></li>
    <li class="list-group-item">{{__('Order Customer')}}: <strong>{{$return->order->customer->contactInformation->name}}</strong></li>
</ul>
<h3 class="mb-3">{{__('Return Items')}}:</h3>
@foreach($return->returnItems as $item)
    <ul class="list-group mb-3">
        <li class="list-group-item">
            {{__('Sku')}}: <strong>{{$item->product->sku}}</strong><br>
            {{__('Name')}}: <strong>{{$item->product->name}}</strong><br>
            {{__('Quantity')}}: <strong>{{$item->quantity}}</strong><br>
        </li>
    </ul>
@endforeach
<div class="row">
    <div class="col text-right">
        <a href="{{route('returns.edit',['return' => $return])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('returns.destroy', ['return' => $return, 'id' => $return->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this return?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
