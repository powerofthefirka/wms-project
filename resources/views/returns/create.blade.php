@extends('layouts.app', ['title' => __('Returns'), 'submenu' => 'returns.menu'])

@section('content')
    <div class="container-fluid">
        <form class="" method="post" action="{{ route('returns.store') }}" autocomplete="off" novalidate>
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <div class="row align-items-center sts sts-fulfilled card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-orders"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 1') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Order Return Information') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        @if(old('order_id'))
                                            @php
                                                $order = \App\Models\Order::find(old('order_id'))
                                            @endphp
                                        @endif
                                        <div class="col col-12">
                                            @include('shared.dropdowns.searchByCustomer', [
                                                'default' => $return->warehouse->customer->id ?? old('customer_id'),
                                                'defaultWarehouse' => $return->warehouse_id ?? old('warehouse_id'),
                                                'defaultVendor' => '',
                                                'warehouseList' => true,
                                                'vendorList' => false,
                                                ])
                                            @include('shared.forms.ajaxSelect', [
                                               'url' => route('returns.filter_orders', ['customer' => session('customer_id') ?? old('customer_id')]),
                                               'name' => 'order_id',
                                               'className' => 'ajax-user-input order-select-create',
                                               'placeholder' => __('Search'),
                                               'label' => __('Order'),
                                               'default' => [
                                                    'id' => $return->order_id ?? old('order_id') ?? '',
                                                    'text' => $return->order->number ?? $order->number ?? ''
                                                ]
                                            ])
                                            @include('shared.forms.input', [
                                               'name' => 'reason',
                                               'label' => __('Return reason')
                                            ])
                                            @include('returns.returnInformationFields')
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pt-2">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="headingTwo"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <div class="row align-items-center sts sts-pending card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-location"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 2') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Products') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body p-0">
                                    <div class="table-responsive return-items">
                                        <table class="table align-items-center overflow-hidden">
                                            <thead class="">
                                            <tr>
                                                <th scope="col">{{ __('Image') }}</th>
                                                <th scope="col">{{ __('Product') }}</th>
                                                <th scope="col">{{ __('Quantity ordered') }}</th>
                                                <th scope="col" class="width-20p">{{ __('Quantity') }}</th>
                                                <th scope="col" class="width-10p">{{ __('Select') }}</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count( old()['return_items'] ?? [] ) > 0)
                                                @foreach(old()['return_items'] as $key => $returnItem)
                                                    @php
                                                        $orderItem = \App\Models\OrderItem::find($returnItem['order_item_id']);
                                                    @endphp
                                                    @include('returns.orderItemLine', ['createMode' => true, 'hasQty' => $returnItem['quantity'] > 0 ? true : false])
                                                @endforeach
                                            @else
                                                @include('returns.orderItemLine', ['createMode' => true])
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
                                                {{ __('Back') }}
                                            </a>
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @include('returns.customerCondition', ['createMode' => true])

                        <div class="card return-shipment {{empty(old()['create_label']) ? 'd-none' : ''}}">
                            <div class="card-header collapsed" id="headingThree"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <div class="row align-items-center sts sts-backorder card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-delivery"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 3') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Shipment') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body p-0">
                                    <fieldset>
                                        <div class="table-responsive">
                                            <table class="table align-items-center m-0 shipping-methods-table">
                                                <tbody>
                                                @foreach($carriers as $carrier)
                                                    <tr>
                                                        <td class="border-0">
                                                            <img class="avatar rounded" src="{{ $carrier['image'] }}">
                                                        </td>
                                                        <td class="border-0 max-width-150 overflow-hidden">
                                                            {{ $carrier['text'] }}
                                                        </td>
                                                        <td class="border-0 max-width-150 overflow-hidden">
                                                            {{ $carrier['method_text'] }}
                                                        </td>
                                                        <td class="border-0">
                                                            {{ $carrier['cost_with_currency'] }}
                                                        </td>
                                                        <td class="border-0">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="shipping-method-{{ $carrier['id'] }}-{{ $carrier['method_id'] }}">
                                                                <label class="custom-control-label" for="shipping-method-{{ $carrier['id'] }}-{{ $carrier['method_id'] }}" data-carrier-id="{{ $carrier['id'] }}" data-method-id="{{ $carrier['method_id'] }}" data-price="{{ $carrier['method_cost'] }}"></label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="d-none">
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_carrier_id',
                                                'type' => 'number',
                                                'label' => 'Shipping Carrier',
                                                'value' => $return->shipping_carrier_id ?? old('shipping_carrier_id') ?? ''
                                            ])
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_method_id',
                                                'type' => 'number',
                                                'label' => 'Shipping Method',
                                                'value' => $return->shipping_method_id ?? old('shipping_method_id') ?? ''
                                            ])
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_price',
                                                'label' => 'Shipping Price',
                                                'value' => $return->shipping_price ?? old('shipping_price') ?? '0'
                                             ])
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="card-body">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
                                                {{ __('Back') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('returns.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ReturnOrderForm();
    </script>
@endpush
