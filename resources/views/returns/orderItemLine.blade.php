<tr id="item[{{$key ?? 0}}]"
class="order-item-fields {{ ($returnItem ?? false) ? '' : 'd-none' }} {{ isset($returnItem->orderItem->product->name) || isset($orderItem->name) ? '' : 'd-none' }}"
>
    <input type="hidden" name="return_items[{{$key ?? 0}}][return_item_id]" value="{{ isset($returnItem->id) ? $returnItem->id : '' }}">
    <input type="hidden" name="return_items[{{($key ?? 0)}}][order_item_id]" value="{{ isset($returnItem->order_item_id) ? $returnItem->order_item_id : old('return_items.' . ($key ?? 0) . '.order_item_id') }}">
    <td class="border-0 pr-0">
        <img class="avatar rounded" data-default-src="/images/product-placeholder.png"
             src="
             @if(isset($orderItem->product))
             {{ $orderItem ? $orderItem->product->productImages->first()->source ?? '' : \App\Models\Return_::DEFAULT_IMAGE }}
             @elseif(isset($returnItem->orderItem) && !empty($returnItem->orderItem->product))
             {{ $returnItem->orderItem->product->productImages->first()->source ?? \App\Models\Return_::DEFAULT_IMAGE }}
             @else
             {{\App\Models\Return_::DEFAULT_IMAGE}}
             @endif
                 ">
    </td>
    <td class="return-item-product-name min-width min-width-100 custom-box">
        {{ __('Name') }}: {{  $returnItem->orderItem->product->name ?? $orderItem->name ?? '' }}<br>
        {{ __('SKU') }}: {{  $returnItem->orderItem->product->sku ?? $orderItem->sku ?? '' }}<br>
    </td>
    <td class="border-0">
        <span class="return-item-quantity-ordered text-body">{{ $returnItem->orderItem->quantity ?? $orderItem->quantity ?? '' }}</span>
    </td>
    <td class="border-0 {{empty($createMode) ? '' : (empty($hasQty)?'d-none':'')}} return-quantity-td">
        @include('shared.forms.input', [
          'name' => 'return_items[' . ($key ?? 0) . '][quantity]',
          'label' => '',
          'type' => 'number',
          'step'=> 1,
          'min' => 'min=0',
          'oninput' => "oninput=validity.valid||(value='');",
          'class' => 'reset-value reset_on_delete',
          'value' => $returnItem->quantity ?? $orderItem->quantity ?? 0
       ])
    </td>
    <td class="border-0 {{empty($createMode) ? 'd-none' : (empty($hasQty)?'':'d-none')}} return-unchecked-td">&nbsp;</td>
    <td class="{{$createMode ?? 'd-none'}}">
        <div class="custom-control custom-checkbox custom-checkbox-success">
            <input class="custom-control-input return-item-check" id="return_items[{{($key ?? 0)}}][order_item_check]" type="checkbox" {{ !empty($hasQty)?'checked':'' }}>
            <label class="custom-control-label" for="return_items[{{($key ?? 0)}}][order_item_check]"></label>
        </div>
    </td>
    @if(isset($returnItem) && \Illuminate\Support\Facades\Request::is('*/edit'))
        <td>
            <span class="text-body">{{$returnItem->quantity_received ?? 0}}</span>
        </td>
    @endif
</tr>
