<div class="row">
   <div class="col col-12 col-xl-6">
      @include('shared.forms.input', [
         'name' => 'weight',
         'label' => __('Weight'),
         'value' => $return->weight ?? 5
      ])
   </div>
   <div class="col col-12 col-xl-6">
      @include('shared.forms.input', [
         'name' => 'width',
         'label' => __('Width'),
         'value' => $return->width ?? 5
      ])
   </div>
</div>
<div class="row">
   <div class="col col-12 col-xl-6">
      @include('shared.forms.input', [
         'name' => 'height',
         'label' => __('Height'),
         'value' => $return->height ?? 5
      ])
   </div>
   <div class="col col-12 col-xl-6">
      @include('shared.forms.input', [
         'name' => 'length',
         'label' => __('Length'),
         'value' => $return->length ?? 5
      ])
   </div>
</div>