<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('returns.index') }}" class="nav-link {{ current_page(route('returns.index')) }}">
                    <span class="nav-link-text">{{ __('All Returns') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('returns.create') }}" class="nav-link {{ current_page(route('returns.create')) }}">
                    <span class="nav-link-text">{{ __('Create Return') }}</span>
                </a>
            </li>
            <li class="nav-item d-none">
                <a href="#" class="nav-link">
                    <span class="nav-link-text">{{ __('Return Items') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
