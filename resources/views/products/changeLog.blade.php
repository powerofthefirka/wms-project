<div class="row">
    <input type="hidden" id="change-log-product-id" value="{{$product->id}}">
    <div class="col-12">
        <a class="btn btn-sm btn-primary btn-block text-white mb-4 d-block d-sm-none toggle-filter-button">
            <span>{{ __('Show filter') }}</span>
            <span>{{ __('Close filter') }}</span>
        </a>
    </div>
    <div class="col-12 d-none d-sm-block filter">
        <div class="card table-card">
            <div class="card-header">
                <h3>Change Log</h3>
            </div>
            @if($filter)
                @include('products.changeLogDataTableFilter')
            @endif
            <div class="table-responsive p-0">
                <table class="table align-items-center table-hover col-12 p-0" id="product-change-log-table" style="width: 100% !important;">
                    <thead class=""></thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
