<div class="col col-12 col-xl-4 mt-2">
    <a data-type="{{ App\Models\Product::PRODUCT_TYPE_PICK_AND_PACK }}" class="btn-custom-3 col product-type-button">
        <img src="/svg/box.svg" />
        <p>{{ __('Regular Product') }}</p>
    </a>
</div>
<div class="col col-12 col-xl-4 mt-2">
    <a data-type="{{ App\Models\Product::PRODUCT_TYPE_SLAP_A_LABEL }}" class="btn-custom-3 col product-type-button">
        <img src="/svg/tag.svg" />
        <p>{{ __('Insert product') }}</p>
    </a>
</div>
<div class="col col-12 col-xl-4 mt-2">
    <a data-type="{{ App\Models\Product::PRODUCT_TYPE_INSERT }}" class="btn-custom-3 col product-type-button">
        <img src="/svg/tray.svg" />
        <p>{{ __('Combined product') }}</p>
    </a>
</div>
<div class="d-none">
    @include('shared.forms.input', [
    'name' => 'product_type',
    'label' => __('Product Type'),
    'value' => $product->product_type ?? ''
    ])
</div>
