<tr class="order-item-fields {{($supplier ?? false) ? '' : 'd-none'}}">
    <td style="white-space: unset">
        <input type="hidden" class="customer_supplier reset_on_delete" name="supplier_product[suppliers][{{$key ?? 0}}]"
               value="{{$supplier['id'] ?? ''}}"
        >
        <span class="text-body supplier_name">{{$supplier->contactInformation->name ?? ''}}</span>
    </td>
    <td class="text-right">
        <a class="btn btn-primary btn-sm text-white remove-item-button">
            ×
        </a>
    </td>
</tr>
