@extends('layouts.app', ['title' => __('Product Change Log'), 'submenu' => 'products.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <input type="hidden" id="change-log-product-id" value="">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                </div>
            </div>
            <div class="col-12">
                <a class="btn btn-sm btn-primary btn-block text-white mb-4 d-block d-sm-none toggle-filter-button">
                    <span>{{ __('Show filter') }}</span>
                    <span>{{ __('Close filter') }}</span>
                </a>
            </div>
            <div class="col-12 d-none d-sm-block filter">
                <div class="card">
                    <div class="card-header">
                        <h3>Filter sources</h3>
                    </div>
                    @include('products.changeLogDataTableFilter')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h3 class="mb-0">{{ __('Product Change Log') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="product-change-log-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ProductForm();
    </script>
@endpush
