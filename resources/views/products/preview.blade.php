<h3 class="mb-3" style="display: inline-block">{{__('Product')}}:</h3>
<a href="{{route('products.editable_preview', ['product' => $product])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    @if(count($product->productImages))
        <li class="list-group-item">
            <img alt="{{__('Product Image')}}" onerror="this.src='/images/product-placeholder.png'" src="{{ $product->productImages[0]->source }}" />
        </li>
    @endif
    <li class="list-group-item">{{__('Sku')}}: <strong>{{$product->sku}}</strong></li>
    <li class="list-group-item">{{__('Name')}}: <strong>{{$product->name}}</strong></li>
    <li class="list-group-item">{{__('Barcode (UPC or other)')}}: <strong>{{$product->barcode}}</strong></li>
    <li class="list-group-item">{{__('Price')}}: <strong>{{with_currency($product->customer, $product->price)}}</strong></li>
    <li class="list-group-item">{{__('Weight')}}: <strong>{{with_weight_unit($product->customer, $product->weight)}}</strong></li>
    <li class="list-group-item">{{__('Width')}}: <strong>{{with_dimension_unit($product->customer, $product->width)}}</strong></li>
    <li class="list-group-item">{{__('Height')}}: <strong>{{with_dimension_unit($product->customer, $product->height)}}</strong></li>
    <li class="list-group-item">{{__('Length')}}: <strong>{{with_dimension_unit($product->customer, $product->length)}}</strong></li>
    <li class="list-group-item">{{__('Notes')}}: <strong>{{$product->notes}}</strong></li>
    <li class="list-group-item">{{__('Replacement Value')}}: <strong>{{with_currency($product->customer, $product->replacement_value)}}</strong></li>
    <li class="list-group-item">{{__('Customs Price')}}: <strong>{{with_currency($product->customer, $product->customs_price)}}</strong></li>
    <li class="list-group-item">{{__('Country Of Origin')}}: <strong>{{$product->country_of_origin}}</strong></li>
    <li class="list-group-item">{{__('HS Code')}}: <strong>{{$product->hs_code}}</strong></li>
    <li class="list-group-item {{ session('customer_id') ? 'd-none' : '' }}">{{__('Customer')}}: <strong>{{ $product->customer->contactInformation->name ?? '' }}</strong></li>
    <li class="list-group-item">{{__('Quantity on hand')}}: <strong>{{$product->quantity_on_hand}}</strong></li>
    <li class="list-group-item">{{__('Quantity allocated')}}: <strong>{{$product->quantity_allocated}}</strong></li>
    <li class="list-group-item">{{__('Quantity available')}}: <strong>{{$product->quantity_available}}</strong></li>
    <li class="list-group-item">{{__('Product type')}}: <strong>{{$product->product_type}}</strong></li>
</ul>
<div class="row">
    <div class="col text-right">
        <a href="{{route('products.edit',['product' => $product])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('products.destroy', ['product' => $product, 'id' => $product->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this product?">{{__('Delete')}}</button>
        </form>
    </div>
</div>

