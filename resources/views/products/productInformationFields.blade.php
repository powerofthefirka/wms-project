<div class="form-group {{session('customer_id') ? 'd-none' : ''}}">
    @if(\Illuminate\Support\Facades\Request::is('*/edit'))
        <label class="form-control-label">{{__('Customer')}}</label>
        <span class="form-control form-control-sm">{{ $product->customer->contactInformation->name ?? '' }}</span>
    @else
        @include('shared.forms.ajaxSelect', [
        'url' => route('products.filter_customers'),
        'name' => 'customer_id',
        'className' => 'ajax-user-input customer_id',
        'placeholder' => 'Search',
        'label' => 'Customer',
        'readonly' => isset($product->customer_id) ? 'true' : null,
        'hidden' => session('customer_id') ? 'd-none' : '',
        'default' => [
            'id' => $product->customer->id ?? session('customer_id') ?? old('customer_id'),
            'text' => $product->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
        ]
    ])
    @endif
</div>
@include('shared.forms.input', [
   'name' => 'sku',
   'label' => __('Sku'),
   'value' => $product->sku ?? '',
   'readOnly' => $product->sku ?? '' ? 'readonly' : ''
])
@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $product->name ?? ''
])
@include('shared.forms.input', [
    'name' => 'barcode',
    'label' => __('Barcode (UPC or other)'),
    'value' => $product->barcode ?? ''
])
@include('shared.forms.input', [
    'name' => 'price',
    'label' => __('Price'),
    'value' => $product->price ?? ''
])
@include('shared.forms.input', [
    'name' => 'weight',
    'label' => __('Weight'),
    'value' => $product->weight ?? ''
])
{{--
<div class="row">
    <div class="col col-12 col-xl-4 col-lg-6">
        <div class="form-group">
            <label class="form-control-label">{{ __('lb') }}</label>
            <input class="form-control form-control-sm" placeholder="{{ __('lb') }}" />
        </div>
    </div>
    <div class="col col-12 col-xl-4 col-lg-6">
        <div class="form-group">
            <label class="form-control-label">{{ __('oz') }}</label>
            <input class="form-control form-control-sm" placeholder="{{ __('oz') }}" />
        </div>
    </div>
</div>
--}}
<div class="row">
    <div class="col col-12 col-xl-4">
        @include('shared.forms.input', [
            'name' => 'width',
            'label' => __('Width'),
            'value' => $product->width ?? ''
        ])
    </div>
    <div class="col col-12 col-xl-4">
        @include('shared.forms.input', [
            'name' => 'height',
            'label' => __('Height'),
            'value' => $product->height ?? ''
        ])
    </div>
    <div class="col col-12 col-xl-4">
        @include('shared.forms.input', [
            'name' => 'length',
            'label' => __('Length'),
            'value' => $product->length ?? ''
        ])
    </div>
</div>
@include('shared.forms.input', [
    'name' => 'notes',
    'label' => __('Notes'),
    'value' => $product->notes ?? ''
])
@include('shared.forms.input', [
    'name' => 'replacement_value',
    'label' => __('Replacement Value'),
    'value' => $product->replacement_value ?? '',
    'class' => 'mb--4'
])
