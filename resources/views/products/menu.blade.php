<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('products.index') }}" class="nav-link {{ current_page(route('products.index')) }}">
                    <span class="nav-link-text">{{ __('Products') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('products.create') }}" class="nav-link {{ current_page(route('products.create')) }}">
                    <span class="nav-link-text">{{ __('Create Product') }}</span>
                </a>
            </li>
            <li class="nav-item d-none">
                <a href="#" class="nav-link">
                    <span class="nav-link-text">{{ __('Bulk Changes') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
