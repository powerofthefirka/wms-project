@extends('layouts.app', ['title' => __('Product Management'), 'submenu' => 'products.menu'])

@section('content')
    <div class="container-fluid">
        <form id="product-form" method="post" action="{{ route('products.update', [ 'product' => $product ]) }}"
              autocomplete="off"
              enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Upload Product Images') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('shared.forms.dropzone', [
                                'url' => route('products.update', ['product' => $product]),
                                'isMultiple' => true,
                                'redirect' => route('products.edit', ['product' => $product]),
                                'formId' => 'product-form',
                                'buttonId' => 'submit-button',
                                'images' => $product->productImages
                            ])
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('International Orders') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <fieldset>
                                @include('products.internationalOrdersFields', [
                                    'product' => $product
                                ])
                            </fieldset>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Vendors Information') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-0">
                            <fieldset>
                                <div class="row">
                                    <div class="col col-12 mb--2">
                                        @include('shared.forms.ajaxSelect', [
                                            'url' => route('purchase_orders.filter_suppliers',
                                                ['customer' => $product->customer->id ?? session('customer_id') ?? old('customer_id')]),
                                            'name' => '',
                                            'className' => 'ajax-user-input supplier_select',
                                            'placeholder' => __('Search for a vendor to add'),
                                            'labelClass' => 'd-block',
                                            'label' => '',
                                            'default' => [
                                               'id' => $supplier['id'] ?? '',
                                               'text' => $supplier->contactInformation->name ?? ''
                                           ]
                                        ])
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="card-body p-0">
                            <fieldset>
                                <div class="table-responsive">
                                    <input type="hidden" id="product" value="{{$product->id}}">
                                    <table class="col-12 table align-items-center table-flush">
                                        <thead class="">
                                        <tr>
                                            <th scope="col">{{ __('Vendor') }}</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="item_container">
                                        @if( count(old()['supplier_product']['suppliers'] ?? []) > 0 )
                                            @foreach(old()['supplier_product']['suppliers'] as $key => $supplierId)
                                                @php
                                                    $supplier = App\Models\Supplier::find($supplierId ?? null);
                                                @endphp
                                                @include('products.supplierLine')
                                            @endforeach
                                        @else
                                            @if($product->suppliers->count() == 0)
                                                @include('products.supplierLine')
                                            @endif
                                            @foreach($product->suppliers as $key => $supplier)
                                                @include('products.supplierLine')
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Product Information') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <fieldset>
                                @include('products.productInformationFields', [
                                    'product' => $product
                                ])
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('products.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" id="submit-button" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @if(auth()->user()->isAdmin())
            @include('products.changeLog', ['filter' => false])
        @endif
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ProductForm();
    </script>
@endpush
