<div class="card-body pt-0 pb-0">
    <div class="row">
        <input type="hidden" id="table-id" value="product-change-log-table">
        <div class="form-group col-12 col-md-6 calendar-input">
            <input
                type="text"
                name="dates_between"
                class="table-datetimepicker table_filter form-control"
                placeholder="Dates between"
                value=""
            >
        </div>
        <div class="form-group col-12 col-md-6">
            <button class="col-12 btn btn-primary text-white">{{ __('Filter Now') }}</button>
        </div>
    </div>
</div>
@push('table-addons-js')
    <script>
        new DataTableAddons();
    </script>
@endpush
