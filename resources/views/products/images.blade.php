<div class="row">
    @foreach($product->productImages as $i=>$productImage)
        <div class="column">
            <a href="{{ $productImage['source'] }}" target="_blank">
                <img src="{{ $productImage['source'] }}" style="height:170px; width:200px;" class="hover-shadow cursor">
            </a>
            <a data-image-id="{{ $productImage['id'] }}" class="delete-product-image-button" style="cursor: pointer">&#x274C;</a>
        </div>
    @endforeach
</div>

