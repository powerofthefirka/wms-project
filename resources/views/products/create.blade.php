@extends('layouts.app', ['title' => __('Product Management'), 'submenu' => 'products.menu'])

@section('content')
    <div class="container-fluid">
        <form id="product-form" class="w-100" method="post" action="{{ route('products.store') }}" autocomplete="off" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <div class="row align-items-center sts sts-fulfilled card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-orders"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 1') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Upload Product Images') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body pt-0">
                                    @include('shared.forms.dropzone', [
                                        'url' => route('products.store'),
                                        'isMultiple' => true,
                                        'redirect' => route('products.index'),
                                        'formId' => 'product-form',
                                        'buttonId' => 'submit-button',
                                        'images' => ''
                                    ])
                                </div>
                                <div class="card-body pt-2">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="headingTwo"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <div class="row align-items-center sts sts-pending card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-location"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 2') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('International Orders') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body pt-0 pb-0">
                                    <fieldset>
                                        @include('products.internationalOrdersFields')
                                    </fieldset>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
                                                {{ __('Back') }}
                                            </a>
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="headingThree"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <div class="row align-items-center sts sts-backorder card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-delivery"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 3') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Vendors Information') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body pt-0 pb-0">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col col-12 mb--2">
                                                @include('shared.forms.ajaxSelect', [
                                                    'url' => route('purchase_orders.filter_suppliers',  ['customer' => session('customer_id') ?? old('customer_id')]),
                                                    'name' => '',
                                                    'className' => 'ajax-user-input supplier_select',
                                                    'placeholder' => __('Search for a vendor to add'),
                                                    'labelClass' => 'd-block',
                                                    'label' => '',
                                                    'default' => [
                                                       'id' => $supplier['id'] ?? '',
                                                       'text' => $supplier->contactInformation->name ?? ''
                                                   ]
                                                ])
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="card-body p-0">
                                    <fieldset>
                                        <div class="table-responsive">
                                            <table class="col-12 table align-items-center table-flush">
                                                <thead class="">
                                                <tr>
                                                    <th scope="col">{{ __('Vendor') }}</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody id="item_container">
                                                @if( count(old('supplier_product.suppliers', [])) > 0 )
                                                    @foreach(old('supplier_product.suppliers', []) as $key => $supplierId)
                                                        @php
                                                            $supplier = App\Models\Supplier::find($supplierId ?? null);
                                                        @endphp
                                                        @include('products.supplierLine')
                                                    @endforeach
                                                @else
                                                    @include('products.supplierLine')
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
                                                {{ __('Back') }}
                                            </a>
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseFour" aria-controls="collapseFour">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="headingFour"  data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <div class="row align-items-center sts sts-on-hold card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-bag"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 4') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Product Information') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body pt-0 pb-0">
                                    <fieldset>
                                        @include('products.productInformationFields')
                                    </fieldset>
                                </div>
                                <div class="card-body">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree">
                                                {{ __('Back') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('products.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" id="submit-button" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ProductForm();
    </script>
@endpush
