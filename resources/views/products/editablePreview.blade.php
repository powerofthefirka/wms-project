<h3 class="mb-3">{{__('Quick Edit Product')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('products.update', [ 'product' => $product ]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'sku',
                'label' => __('Sku'),
                'value' => $product->sku ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'name',
                'label' => __('Name'),
                'value' => $product->name ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'barcode',
                'label' => __('Barcode (UPC or other)'),
                'value' => $product->barcode ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'price',
                'label' => __('Price') . label_format(currency($product->customer ?? null)),
                'value' => $product->price ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'weight',
                'label' => __('Weight') . label_format(weight_unit($product->customer ?? null)),
                'value' => $product->weight ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'width',
                'label' => __('Width') . label_format(dimension_unit($product->customer ?? null)),
                'value' => $product->width ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'height',
                'label' => __('Height') . label_format(dimension_unit($product->customer ?? null)),
                'value' => $product->height ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'length',
                'label' => __('Length') . label_format(dimension_unit($product->customer ?? null)),
                'value' => $product->length ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'notes',
                'label' => __('Notes'),
                'value' => $product->notes ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'replacement_value',
                'label' => __('Replacement Value') . label_format(currency($product->customer ?? null)),
                'value' => $product->replacement_value ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'customs_price',
                'label' => __('Customs Price') . label_format(currency($product->customer ?? null)),
                'value' => $product->customs_price ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'country_of_origin',
                'label' => __('Country Of Origin'),
                'value' => $product->country_of_origin ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'hs_code',
                'label' => __('HS Code'),
                'value' => $product->hs_code ?? ''
            ])
        </li>
        <li class="list-group-item {{session('customer_id') ? 'd-none' : ''}}">
            @include('shared.forms.ajaxSelect', [
                'url' => route('products.filter_customers'),
                'name' => 'customer_id',
                'className' => 'ajax-user-input',
                'placeholder' => 'Search',
                'label' => 'Customer',
                'default' => [
                    'id' => $product->customer->id ?? session('customer_id') ?? old('customer_id'),
                    'text' => $product->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
                ]
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'quantity_on_hand',
                'label' => __('Quantity on hand'),
                'value' => $product->quantity_on_hand ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'quantity_allocated',
                'label' => __('Quantity allocated'),
                'value' => $product->quantity_allocated ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'quantity_available',
                'label' => __('Quantity available'),
                'value' => $product->quantity_available ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'product_type',
                'label' => __('Product type'),
                'value' => $product->product_type ?? ''
            ])
        </li>
    </ul>
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="product-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>


