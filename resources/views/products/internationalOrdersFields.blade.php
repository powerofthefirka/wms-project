@include('shared.forms.input', [
    'name' => 'customs_price',
    'label' => __('Customs Price'),
    'value' => $product->customs_price ?? ''
])
@include('shared.forms.input', [
    'name' => 'country_of_origin',
    'label' => __('Country Of Origin'),
    'value' => $product->country_of_origin ?? ''
])
@include('shared.forms.input', [
    'name' => 'hs_code',
    'label' => __('HS Code'),
    'value' => $product->hs_code ?? ''
])
