@extends('layouts.app', ['class' => 'not-logged'])

@section('content')
    <div class="container d-flex h-100">
        <div class="row m-0 justify-content-center align-self-center">
            <div class="col col-12 col-xl-6">
                <div class="card">
                    <div class="card-body p-4 p-sm-5">
                        <div class="row">
                            <a class="col not-logged-brand text-center mb-4" href="/">
                                <img src="{{ login_logo() }}" class="navbar-brand-img" alt="...">
                            </a>
                        </div>

                        <div class="text-center text-muted mb-4">
                            {{ __('Reset password') }}
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form role="form" method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-4">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col col-12 col-sm-4 my-2 my-sm-3 text-center text-sm-left">
                                    <a href="{{ route('login') }}">
                                        {{ __('Back to login') }}
                                    </a>
                                </div>
                                <div class="col col-12 col-sm-8 text-center text-sm-right">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col not-logged-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white" target="_blank">Terms & Privacy</a>
                </li>
                <li class="nav-item">
                    <span class="nav-link text-white" target="_blank">&copy; {{ now()->year }} {{ config('app.name') }}</span>
                </li>
            </ul>
        </div>
    </div>
@endsection
