@extends('layouts.app', ['class' => 'not-logged'])

@section('content')
    <div class="container d-flex h-100">
        <div class="row m-0 justify-content-center align-self-center">
            <div class="col col-12 col-xl-6">
                <div class="card">
                    <div class="card-body p-4 p-sm-5">
                        <form role="form" method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row">
                                <a class="col not-logged-brand text-center mb-4" href="/">
                                    <img src="{{ login_logo() }}" class="navbar-brand-img" alt="...">
                                </a>
                            </div>

                            <div class="text-center text-muted mb-4">
                                {{ __('Login') }}
                            </div>

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-4">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }} mb-4">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" type="password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox mb-4">
                                <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="customCheckLogin">
                                    <span class="text-muted">{{ __('Remember me') }}</span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-sm-6 my-2 my-sm-3 text-center text-sm-left">
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}">
                                            {{ __('Forgot password?') }}
                                        </a>
                                    @endif
                                </div>
                                <div class="col col-12 col-sm-6 text-center text-sm-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="ci ci-success"></i>
                                        {{ __('Sign in') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col not-logged-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a href="terms-privacy" class="nav-link text-dark" target="_blank">{{ __('Terms & Privacy') }}</a>
                </li>
                <li class="nav-item">
                    <span class="nav-link text-dark" target="_blank">&copy; {{ now()->year }} {{ config('app.name') }}</span>
                </li>
            </ul>
        </div>
    </div>
@endsection
