@extends('layouts.app', ['class' => ''])

@section('content')
    <div class="container">
        <div class="row m-0">
            <div class="col col-12">
                <div class="card">
                    <div class="card-body p-4 p-sm-5">
                        <h1>Cookie and Privacy Policy, EU Personal Data
                        Regulation (GDPR)</h1>
                        <p>This <strong>cookie and privacy policy</strong> has been updated in <strong>May 2021</strong> to comply with the new Personal
                        Data Regulation (GDPR).</p>
                        <h2>Overview:</h2>
                        <p><strong>1.0 Owner Information and Data Manager.</strong></p>
                        <p><strong>2.0 Cookie policy</strong></p>
                        <p>2.1 What are cookies?</p>
                        <p>2. 2 The purpose of cookies on our website.</p>
                        <p>2.3 We use the following cookies.</p>
                        <p>2.4 Ad Network.</p>
                        <p><strong>3.0 Personal Information / Privacy Policy.</strong></p>
                        <p>3.1 Personal Information</p>
                        <p>3.2 Processing of personal data.</p>
                        <p>3.3 The right to be forgotten.</p>
                        <p>3.4 Exceptions for deletion of information.</p>
                        <p>3.5 Insights into personal data.</p>
                        <p>3.6 You may appeal to the Data Inspectorate.</p>
                        <p>3.7 Automagical ApS as a data processor.</p>
                        <p>4.0. Cookie and privacy policy update</p>
                        <h3>1.0. Owner Information and Data Manager</h3>
                        <p>Automagical ApS</p>
                        <p>CVR: 40292101</p>
                        <p>Lunavej 4</p>
                        <p>8600 Silkeborg</p>
                        <p>E-mail: contact @ automagical .dk</p>
                        <p>Contact person: Morten Bitsch</p>
                        <h3>2.0 Cookie policy</h3>
                        <h3>2.1. What are cookies?</h3>
                        <p>A cookie is a small text file that is stored on your computer or other device in order to recognize
                        it. Our cookies do not contain personal data. A cookie cannot contain viruses.</p>
                        <h3>2.2 . The purpose of cookies on our website.</h3>
                        <ul>
                            <li>To ensure the technical functionality so that we can remember your settings.</li>
                            <li>Visitor Analysis, to analyze how many visitors we have, and where visitors come from.</li>
                        </ul>
                        <p>The site uses cookies from a following third party that has access to the cookies in question:</p>
                        <p>See a list of third party cookies here:</p>

                        <h3>2.3 . We use the following cookies</h3>
                        <p><strong>Google Analytics</strong></p>
                        <p>Google Analytics is used to compile visitor statistics, such as how many people have visited our site,
                        what type of technology they use (eg Mac or Windows, how long they spend on the site, what page
                        they are watching, etc. This helps us to continuously improve our website. These so-called
                        " analytics " programs can also tell us how people reached this site (eg from a search engine ).</p>
                        <h3>2.5. ad Network</h3>
                        <p>This webpage does not use ad networks at this time.</p>
                        <h2>3.0. Personal Information / Privacy Policy</h2>
                        <h3>3.1. personal data</h3>
                        <p>In connection with your creation as a customer at Automagical ApS, you will be required to provide a
                            number of personal information:</p>
                        <ul>
                            <li>Your name</li>
                            <li>Company Name</li>
                            <li>Your e-mail address</li>
                            <li>Your address</li>
                            <li>Your phone number</li>
                            <li>CVR number (company number)</li>
                        </ul>
                        <p>We treat these personal information in order to provide you with support, the processing of your
                        purchase, and to both you and we comply with the Bookkeeping Act, and to comply with the
                            Marketing Act.</p>
                        <h2>3.2. Processing of personal data</h2>
                        <p><strong>Order details in 3plDashboard</strong></p>
                        <p><i>Information we process in this group: All information relating to orders going through 3plDashboard,
                                that being (but not limited to): Name, address, phone number, order items</i></p>
                        <h2>3.3. The right to be forgotten</h2>
                        <p>According to the EU Personal Data Regulation, you have the right to be forgotten</p>
                        <h2>3.4. Exceptions for deletion of information.</h2>
                        <p>According to section 10 of the Bookkeeping Act, we must keep invoices for 5 years from the end of
                        the financial year to which the material relates.
                        The material is stored in the Dinero.dk accounting program as we have a data processing agreement
                        with and all information is on a server within the EU.</p>
                        <h2>3.5. Insights into personal data</h2>
                        <p>You are entitled at any time to gain insight into the information we have about you. This is done by
                        contacting <a href="mailto:contact@automagical.dk">contact@automagical.dk</a>.
                        You have the right to change the information at any time if it is incorrect information we have about
                        you. This is done by contacting <a href="mailto:contact@automagical.dk">contact@automagical.dk</a>
                        You have the right, on your occasion, to have your personal data transferred in a legible format.
                        You are entitled to object to our processing of your personal information. This is done by
                        contacting <a href="mailto:contact@automagical.dk">contact@automagical.dk</a>
                        You may revoke the consent you have given us to process your information at any time. This is done
                        by contacting <a href="mailto:contact@automagical.dk">contact@automagical.dk</a></p>

                        <h2>3.6. You can complain to the Information Commissioner’s Office.</h2>
                        <p>If you wish to complain about Inzerted Limited’s processing of your personal information, please
                        contact Information Commissioner’s Office.</p>

                        <h2>3.7. Automagical ApS as a data processor.</h2>
                        <p>If your company has an application with Automagical ApS, we are the data processor for the
                        personal data you treat yourself, including stored data at Automagical ApS.</p>
                        <p>We note that we are not responsible for 3rd party plugins that can collect personal data from users
                        of your app or website.</p>
                        <p>examples of third party plugins:</p>
                        <ul>
                            <li>Facebook Plugin</li>
                            <li>ECommerce plugin</li>
                            <li>Open Table Plugin</li>
                            <li>Twitter Plugin</li>
                            <li>Instagram Plugin</li>
                            <li>Google Forms Plugin</li>
                            <li>Google Docs Plugin</li>
                            <li>Google Sheets Plugin</li>
                            <li>Type Form Plugin</li>
                            <li>WooCommerce Integration Plugins</li>
                        </ul>
                        <h2>4.0. Cookie and privacy policy update</h2>
                        <p><strong>Changes to the personal data policy.</strong></p>
                        <p>In case of significant changes to the Personal Data Policy, we will notify you by using visible
                        messages on our website.</p>
                        <p><strong>Last updated</strong></p>
                        <p>This cookie and privacy policy was updated on 18th May 2021</p>
                    </div>
                </div>
            </div>
            <div class="col col-12 mb-4">
                <ul class="nav">
                    <li class="nav-item">
                        <span class="nav-link text-dark" target="_blank">&copy; {{ now()->year }} {{ config('app.name') }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
