@extends('layouts.app', ['title' => __('User Management'), 'icon' => '<i class="ci ci-orders"></i>'])

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <input id="users-search" type="text" class="form-control form-control-lg form-control-alternative mb-4" placeholder="Search for users..">
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h3 class="mb-0">{{ __('Users') }}</h3>
                            </div>
                            <div class="col-6 text-right">

                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="users-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new User();
    </script>
@endpush
