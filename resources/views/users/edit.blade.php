@extends('layouts.app', ['title' => __('User Management'), 'icon' => '<i class="ci ci-orders"></i>'])

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('User Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('users.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                            <div class="col-12 mt-2">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('users.update', ['user' => $user]) }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>
                            <div class="pl-lg-4">
                                {{ method_field('PUT') }}
                                @include('shared.forms.contactInformationFields', [
                                    'name' => 'contact_information',
                                    'contactInformation' => $user->contactInformation
                                ])
                                @include('shared.forms.input', [
                                   'name' => 'email',
                                   'label' => __('User Email'),
                                   'type' => 'email',
                                   'value' => $user->email
                               ])
                                @include('shared.forms.input', [
                                    'name' => 'password',
                                    'label' => __('Password'),
                                    'type' => 'password'
                                ])
                                @include('shared.forms.input', [
                                    'name' => 'password_confirmation',
                                    'label' => __('Confirm Password'),
                                    'type' => 'password'
                                ])
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary ">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
