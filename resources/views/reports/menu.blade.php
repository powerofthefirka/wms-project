<li class="submenu {{ $class ?? '' }}" data-submenu="reports.menu">
    <ul>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-box-2 text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Shipments') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-time-alarm text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Processing Time') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-app text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Inventory Change Log') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-app text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Inventory Value') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-archive-2 text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Product Turnover') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-tag text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Sales by SKU') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="ni ni-sound-wave text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Packer Errors') }}</span>
            </a>
        </li>
    </ul>
</li>
