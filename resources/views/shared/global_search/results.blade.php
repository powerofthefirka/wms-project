@if(!count($results))
    <p class=" m-0 mt-2">{{__('No results found..')}}</p>
@else
    <div class="row">
        <div class="col col-12 col-lg-6">
            @foreach($results as $name => $result)
                @if($name == 'Inventory')
                    <a href="{{$result['linkToTable']}}"><h3 class=" m-0">{{$name}}</h3></a>
                    <ul class="p-0">
                        @foreach($result as $key => $item)
                            @if($key !== 'linkToTable' && $key !== 'count')
                                <li class="global-search-product">
                                    <a href="{{ route('products.edit',['id' => $item['id']]) }}">
                                        <div class="row"><div class="col">
                                                <img class="avatar rounded" data-default-src="/images/product-placeholder.png" src="{{$item->product->productImages[0]->source ?? "/images/product-placeholder.png"}}">
                                                <p class="m-0">
                                                    {{__('Name')}}{{': '. $item->name ?? '-'}},
                                                    {{__('Available')}}{{': '. $item->quantity_available ?? '-'}}
                                                </p>
                                                <p class="m-0">
                                                    {{__('Sku')}}{{': '. $item->sku ?? '-'}},
                                                    {{__('On hand')}}{{': '. $item->quantity_on_hand ?? '-'}},
                                                    {{__('Backordered')}}{{': '. $item->quantity_backordered ?? '-'}}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                        <li>
                            <a href="{{$result['linkToTable']}}">{{__('View all ' )}} {{ $result['count'] - 1 }} {{__(' products')}}</a>
                        </li>
                    </ul>
                @endif
            @endforeach
        </div>
        <div class="col col-12 mt-2 mt-lg-0 col-lg-6">
            <div class="col col-12">
                @foreach($results as $name => $result)
                    @if($name != 'Inventory' && $name != 'InventoryCount')
                        <a href="{{$result['linkToTable']}}"><h3 class=" m-0">{{$name}}</h3></a>
                        <ul class="p-0">
                            @foreach($result as $key => $item)
                                @if($key !== 'linkToTable' && $key !== 'count')
                                    <li>
                                        @if($name == 'Orders')
                                            <a href="{{ route('orders.edit',['id' => $item['id']]) }}">
                                                <div class="row">
                                                    <div class="col">
                                                        <p class="m-0">
                                                            {{__('Number')}}{{': '. $item->number}},
                                                            {{__('Shipping Name')}}{{': '. $item->shippingContactInformation->name ?? '-'}},
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                        @if($name == 'Returns')
                                            <a href="{{ route('returns.edit',['id' => $item['id']]) }}">
                                                <div class="row">
                                                    <div class="col">
                                                        <p class="m-0">
                                                            {{__('Number')}}{{': '. $item->number ?? '-'}},
                                                            {{__('Order number')}}{{': '. $item->order->number ?? '-'}},
                                                            {{__('Customer name')}}{{': '. $item->order->customer->contactInformation['name'] ?? '-'}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                        @if($name == 'Shipments')
                                            <div class="row">
                                                <div class="col">
                                                    <p class="m-0 ">
                                                        {{__('Id')}}{{': '. $item->id ?? '-'}},
                                                        {{__('Order')}}{{': '. $item->order_id ?? '-'}} -
                                                        <a href="{{$item->tracking_url ?? ''}}">{{__('Tracking')}}{{': '. $item->tracking_url ?? '-'}}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        @endif
                                        @if($name == 'PurchaseOrders')
                                            <a href="{{ route('purchase_orders.edit',['id' => $item['id']]) }}">
                                                <div class="row">
                                                    <div class="col">
                                                        <p class="m-0">
                                                            {{__('Purchase order number')}}{{': '. $item->number ?? '-'}},
                                                            {{__('Customer name')}}{{': '. $item->customer->contactInformation->name ?? '-'}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                            <li>
                                <a href="{{$result['linkToTable']}}">{{__('View all ' )}} {{ $result['count'] - 1 }}
                                    @if($name == 'Orders')
                                        {{__(' orders')}}
                                    @endif
                                    @if($name == 'Returns')
                                        {{__(' returns')}}
                                    @endif
                                    @if($name == 'Shipments')
                                        {{__(' shipments')}}
                                    @endif
                                    @if($name == 'PurchaseOrders')
                                        {{__(' purchase orders')}}
                                    @endif
                                </a>
                            </li>
                        </ul>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif
