<div class="col col-12 global-search {{ $searchClass ?? '' }}">
    <div class="form-group">
        <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="nc-icon nc-zoom-2"></i></span>
            </div>
            <input id="global-search-input" class="form-control form-control-sm" placeholder="{{ __('Search') }}" type="text" value="{{app('request')->input('term') ?? ''}}">
            <button type="button" class="close close-global-search-button">
                <span>×</span>
            </button>
        </div>
    </div>
    <div id="global-search-results"></div>
</div>
