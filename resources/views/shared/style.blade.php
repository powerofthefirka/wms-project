@php
    $primary_color = primary_color();
@endphp

<style type="text/css">
    a {
        color: {{ $primary_color }};
    }

    a:hover {
        color: {{ $primary_color }};
    }

    a.btn.btn-primary:not([href]):not([tabindex]) {
        color: #fff;
    }

    .btn-primary {
        border-color: {{ $primary_color }};
        background-color: {{ $primary_color }};
    }

    .btn-primary:hover {
        background-color: {{ $primary_color }};
    }

    .btn-neutral {
        color: {{ $primary_color }};
    }

    .btn-neutral:hover {
        color: {{ $primary_color }};
    }

    .table a:not(.btn),
    .table a:not(.btn):hover { color: {{ $primary_color }} }

    .nav-pills,
    .nav-link { color: {{ $primary_color }} }

    .fc-unthemed td.fc-today span {
        color: {{ $primary_color }}
    }

    .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before,
    .custom-control-input:active ~ .custom-control-label::before {
        border-color: {{ $primary_color }};
        background-color: {{ $primary_color }};
    }

    .form-control:focus {
        border-color: {{ $primary_color }};
    }

    .select2-container .select2-selection--single:focus, .select2-container--default.select2-container--focus .select2-selection--multiple:focus, .select2-container--default .select2-selection--multiple:focus, .select2-container--default .select2-search--dropdown .select2-search__field:focus {
        border-color: {{ $primary_color }};
    }

    .btn-primary:not(:disabled):not(.disabled):active, .btn-primary:not(:disabled):not(.disabled).active, .show > .btn-primary.dropdown-toggle {
        border-color: {{ $primary_color }};
        background-color: {{ $primary_color }};
    }

    .table a:not(.btn),
    .table a:not(.btn):hover { color: {{ $primary_color }} }

    .paginate_button.page-item.active a,
    .paginate_button.page-item:hover a { background-color: {{ $primary_color }}; }

    .nav-pills .nav-link,
    .nav-pills .nav-link.primary-color { color: {{ $primary_color }} }
    .nav-pills .nav-link.primary-color:hover { color: {{ $primary_color }} }

    .nav-pills .nav-link.active { background-color: {{ $primary_color }}; }

    .nav-pills .nav-link:hover {
        color: {{ $primary_color }};
    }

    .bg-gradient-default
    {
        background: linear-gradient(87deg, {{ $primary_color }} 0, {{ $primary_color }} 100%) !important;
    }

    .bg-default {
        background-color: {{ $primary_color }} !important;
    }

    .sidebar .navbar-nav .nav-item .nav-link:hover,
    .sidebar .navbar-nav .nav-item .nav-link.active {
        background: #ddd;
        background: linear-gradient(90deg, {{ $primary_color }}30 0%, #ffffff00 100%, #ffffff00 100%);
    }

    .sidebar .navbar-nav .nav-item .nav-link:hover i,
    .sidebar .navbar-nav .nav-item .nav-link.active i {
        color: {{ $primary_color }};
    }

    .sidebar .navbar-nav .nav-item .nav-link:hover:before,
    .sidebar .navbar-nav .nav-item .nav-link.active:before {
        background: {{ $primary_color }};
        opacity: 1;
    }

    .table-card .bottom .paging_simple_numbers .pagination li.previous a,
    .table-card .bottom .paging_simple_numbers .pagination li.next a,
    .table-card .bottom .paging_simple_numbers .pagination li.active a {
        background: {{ $primary_color }};
    }

    .daterangepicker td.active, .daterangepicker td.active:hover {
        background: {{ $primary_color }};
    }

    .btn-primary.disabled,
    .btn-primary:disabled {
        border-color: {{ $primary_color }};
        background-color: {{ $primary_color }};
    }

    .with-loader:before{
         border-top-color: {{ $primary_color }};
    }

    .header .global-search #global-search-results .row .col-12:last-child .col-12 {
        border-color: {{ $primary_color }};
    }

    .header .global-search .form-group.focused {
        border-color: {{ $primary_color }};
    }
</style>
