<div class="row filter-boxes">
    @foreach ($statuses as $status)
        @if(isset($status['icon']))
            <div class="col col-6 col-lg col-md-3 col-sm-4">
                <div class="card transparent transparent-with-border">
                    <a class="btn-custom col filter-by-status-button sts d-flex justify-content-center {{ $status['icon'] }}" data-filter="{{ isset($status['skip_value']) && $status['skip_value'] ? '' : $status['status'] }}">
                        <div class="row m-0 align-items-center">
                            <div class="col col-auto pl-0 pr-2 pr-lg-4 d-flex">
                                <i class="automagical-orders"></i>
                            </div>
                            <div class="col p-0 text-left">
                                <strong class="text-default">{{ $status['count'] }}</strong>
                                <p class="m-0 text-default">
                                    {{ ucwords(str_replace('_', ' ', $status['status'])) }}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    @endforeach
</div>
