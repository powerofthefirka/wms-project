<div class="modal fade confirm-dialog" id="data-export-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-icon btn-success" type="button" id="btn-all-column" autocomplete="off">
                    <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                    <span class="btn-inner--text">{{ __('Select all') }}</span>
                </button>
                <h5 class="modal-title export-modal-title">{{ __('Export Data') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('data.export' )}}" autocomplete="off">
                @csrf
                <input type="hidden" name="model" value="{{ $model }}">
                <div class="modal-body p-3">
                    <ul id="exprtable-column-list" class="list-group">
                        @foreach($data as $item)
                            <li class="list-group-item p-2">
                                <div class="custom-control custom-checkbox">
                                    <input id="column_{{ $item['column'] }}" type="checkbox" class="checkbox custom-control-input" name="data[{{ $item['column'] }}]" value="{{ $item['title'] }}">
                                    <label class="custom-control-label" for="column_{{ $item['column'] }}">{{$item['title']}}</label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary confirm-button">{{ __('Submit') }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#data-export-modal">
    {{__('Export Data')}}
</button>
@push('js')
    <script>
        new dataExportableColumn();
    </script>
@endpush