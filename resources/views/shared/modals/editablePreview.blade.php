<div class="modal fade editable-preview-modal" id="editable-preview-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-gradient-default">
            <div class="modal-header">
                <h6 class="modal-title text-white" id="modal-title-notification">{{ __('Confirmation') }}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Close') }}">
                    <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
            <div class="modal-body text-white text-center py-3">
                {{__('You have unsaved changes')}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white save">{{ __('Save') }}</button>
                <button type="button" class="btn btn-white continue-editing" data-dismiss="modal">{{ __('Continue editing') }}</button>
                <button type="button" class="btn btn-link text-white ml-auto discard-changes">{{ __('Discard Changes') }}</button>
            </div>
        </div>
    </div>
</div>

