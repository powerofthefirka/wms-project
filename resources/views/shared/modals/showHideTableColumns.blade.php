<div class="modal fade confirm-dialog" id="table-column-show-hide-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Show/Hide Columns') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-3">
                <ul id="table-column-name-list" class="list-group"></ul>
            </div>
            <div class="modal-footer">
                <button id="column_show_save" type="button" class="btn btn-primary confirm-button" data-dismiss="modal">{{ __('Save') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#table-column-show-hide-modal">
    {{__('Show Columns')}}
</button>
@push('js')
    <script>
        const hideColumns = @json($hideColumns);
    </script>
@endpush
