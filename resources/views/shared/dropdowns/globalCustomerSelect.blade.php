<div class="dropdown customer-dropdown @if (app()->user->getCustomers()->count() == 1) d-none @endif">
	@if(session('customer_id') && app()->user->getCustomers()->count() == 1)
	    <button class="btn btn-secondary" type="button" id="dropdownMenuButton" aria-expanded="false">
	        {{app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name}}
	    </button>
	@elseif(session('customer_id'))
	    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			{{app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name}}
	    </button>
	@else
		<button class="btn btn-secondary dropdown-toggle m-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			{{ __('Select a customer') }}
	    </button>
	@endif
    <div class="dropdown-menu dropdown-customer-select" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="{{ route('controller.forget_customer') }}" >All</a>
        @foreach (app()->user->getCustomers() as $customer)
            <a class="dropdown-item" href="{{ route('controller.session_set_customer', ['id' => $customer->id])}}">{{$customer->contactInformation->name}}</a>
        @endforeach
    </div>
</div>
