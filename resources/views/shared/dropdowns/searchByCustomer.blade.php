<div class="form-group search-by-customer" onautocomplete="off">
	<label class="form-control-label">{{ __('Customer') }}</label>
	<select
		{{$disabled??''}}
		name="customer_id"
		id="customer_id"
		class="form-control d-inline-block user-customer customer_id"
		data-default-warehouse="{{$defaultWarehouse}}"
		data-default-vendor="{{$defaultVendor}}"
		data-toggle="select"
		data-warehouse-class="warehouse-select"
		data-vendor-class="vendor-select"
		data-warehouse-list="{{$warehouseList}}"
		data-vendor-list="{{$vendorList}}"
	>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			@if($default && session('customer_id'))
				<option value="{{$default}}">{{app()->user->getCustomers()->where('id', $default)->first()->contactInformation->name}}</option>
			@elseif(session('customer_id'))
				<option value="{{session('customer_id')}}">{{app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name}}</option>
			@else
				@foreach (app()->user->getCustomers() as $customer)
					<option value="{{$customer->id}}" {{$customer->id == $default ? "selected='selected'" : ""}}>{{$customer->contactInformation->name}}</option>
				@endforeach
			@endif
		</div>
	</select>
</div>

<div class="form-group select-warehouse {{$warehouseList ?'':'d-none'}}" onautocomplete="off">
    <label class="form-control-label">{{ __('Warehouse') }}</label>
    <select
		name="warehouse_id"
		class="warehouse-select"
		data-placeholder="{{__('Search')}}"
		data-allow-clear="1"
		data-toggle="select"
    >
    </select>
</div>

<div class="form-group select-vendor {{$vendorList ? '':'d-none'}}" onautocomplete="off">
    <label class="form-control-label">{{ __('Vendor') }}</label>
    <select
		name="supplier_id"
		class="vendor-select enabled-for-customer"
		data-placeholder="{{__('Search')}}"
		data-allow-clear="1"
		data-toggle="select"
    >
    </select>
</div>

@push('js')
    <script>
        new SearchByCustomer();
    </script>
@endpush
