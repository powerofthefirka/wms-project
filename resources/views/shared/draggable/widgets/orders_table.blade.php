<button class="delete-widget btn btn-sm btn-danger">X</button>
<div class="card m-0 h-100 d-flex" data-shortcode="[widget_orders_table]">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <h3 class="mb-0">{{ __('Orders') }}</h3>
            </div>
            <div class="col-6 text-right">
                @include('shared.modals.showHideTableColumns', ['hideColumns' => user_settings('orders_table_hide_columns') ?? '[]'])
            </div>
        </div>
    </div>
    <div class="col-12">
        @include('alerts.success', ['class' => 'mt-2 mb-1'])
        @include('alerts.errors', ['class' => 'mt-2 mb-1'])
    </div>
    <div class="table-responsive p-0">
        <table  class="table align-items-center table-hover col-12 p-0" id="orders-table" style="width: 100% !important; height: 700px">
            <thead class="t"></thead>
            <tbody></tbody>
        </table>
    </div>
</div>
