<div class="custom-control custom-checkbox custom-checkbox-success">
    <input class="custom-control-input" name="{{$name}}" id="chk-{{$name}}" type="checkbox" {{$checked ?? false === true ? 'checked' : ''}} value="{{$value?? 1 }}">
    <label class="custom-control-label" for="chk-{{$name}}">{{$label}}</label>

    @include('alerts.feedback', ['field' => $name])
</div>
