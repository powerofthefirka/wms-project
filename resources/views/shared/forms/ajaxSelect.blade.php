@php
    $errorName = str_replace(['][', '[', ']'], ['.', '.', ''], $name);
@endphp

<div class="form-group {{$hidden ?? ''}}{{ $errors->has($errorName) ? ' has-danger' : '' }}" readonly onautocomplete="off">
    <label class="form-control-label {{$labelClass ?? ''}}">{{ $label }}</label>
    @if(isset($readonly) ?? false)
        <input name="{{ $name }}" value="{{ $default['id'] }}" class="{{$className}}" hidden>
        <input value="{{ $default['text'] }}" class="form-control form-control-sm{{ $errors->has($errorName) ? ' is-invalid' : '' }}" readonly>
    @else
        <select
            {{$atrribute ?? ''}}
            name="{{$name}}"
            class="{{$className}}{{ $errors->has($errorName) ? ' is-invalid' : '' }}"
            data-ajax--url="{{ $url }}"
            data-placeholder="{{ $placeholder }}"
            data-minimum-input-length="1"
            data-allow-clear="1"
            @if (!empty($form)) form="{{ $form }}" @endif
            data-toggle="select"
            @if (!empty($fixRouteAfter)) data-fix-route-after="{{ $fixRouteAfter }}" @endif
        >
            @if (!empty($default))
                <option
                    value="{{ $default['id'] }}"
                    selected="selected"
                >{{ $default['text'] }}</option>
            @endif
        </select>
    @endif

    @include('alerts.feedback', ['field' => $errorName])
</div>
