@include('shared.forms.input', [
    'name' => $name . '[name]',
    'label' => __('Name'),
    'value' => $contactInformation->name ?? ''
])
@include('shared.forms.input', [
   'name' => $name . '[company_name]',
   'label' => __('Company Name'),
   'value' => $contactInformation->company_name ?? ''
])
@include('shared.forms.input', [
    'name' => $name . '[address]',
    'label' => __('Address'),
    'value' => $contactInformation->address ?? ''
])
@include('shared.forms.input', [
    'name' => $name . '[address2]',
    'label' => __('Address 2'),
    'value' => $contactInformation->address2 ?? ''
])
<div class="row">
    <div class="col col-xl-6">
        @include('shared.forms.input', [
            'name' => $name . '[zip]',
            'label' => __('Zip'),
            'value' => $contactInformation->zip ?? $value ?? ''
        ])
    </div>
    <div class="col col-xl-6">
        @include('shared.forms.input', [
            'name' => $name . '[city]',
            'label' => __('City'),
            'value' => $contactInformation->city ?? $value ?? ''
        ])
    </div>
</div>
<div class="form-group">
    <label for="{{ $name . '[country_id]' }}" class="form-control-label" for="inputState">{{ __('Country') }}</label>
    <select name="{{ $name . '[country_id]' }}" id="inputState" class="form-control form-control-sm" data-live-search="true" data-live-search-placeholder="{{ __('Search..') }}">
        <option value="">{{ __('Choose..') }}</option>
        @foreach (\App\Models\Country::all() as $country)
        <option value="{{ $country->id }}" @if(($contactInformation->country_id ?? 0) == $country->id) selected @endif>{{ __( $country->title ) }}</option>
        @endforeach
    </select>
</div>
@include('shared.forms.input', [
    'name' => $name . '[email]',
    'label' => __('Contact Email'),
    'type' => 'email',
    'value' => $contactInformation->email ?? $value ?? ''
])
@include('shared.forms.input', [
    'name' => $name . '[phone]',
    'label' => __('Phone'),
    'value' => $contactInformation->phone ?? $value ?? ''
])
