@php
    $errorName = str_replace(['][', '[', ']'], ['.', '.', ''], $name);
@endphp
<div class="form-group{{ $errors->has($errorName) ? ' has-danger' : '' }}">
    <label class="form-control-label" for="input-{{ $name }}">{{ $label }}<span class="currency"></span></label>
    <input
        type="{{ $type ?? 'text' }}"
        @if (isset($autocomplete)) autocomplete="{{ $autocomplete }}" @endif
        @if ($type ?? false)
            @if($type==='number')
                step="{{$step ?? '.01'}}"
            @endif
        @endif
        name="{{ $name }}"
        id="input-{{ $name }}"
        class="{{$class??''}} form-control form-control-sm{{ $errors->has($errorName) ? ' is-invalid' : '' }}"
        placeholder="{{ $label }}"
        value="{{ old(dot($name), $value ?? '') }}"
        {{ $readOnly ?? ''}}
        {{ $disabled ?? ''}}
        {{ $min ?? ''}}
        {{ $max ?? ''}}
        {{ $oninput ?? ''}}
    >

    @include('alerts.feedback', ['field' => $errorName])
</div>
