<div id="dropzone-body"
    data-multiple="{{ $isMultiple }}"
    data-redirect="{{ $redirect }}"
    data-url="{{ $url }}"
    data-form="{{ $formId }}"
    data-button="{{ $buttonId }}"
    data-images="{{ $images }}"
    class="dropzone dropzone-multiple p-0"
>
    <div class='fallback'>
        <input name='file' type='file'/>
    </div>
</div>

@push('js')
    <script>
        new ImageDropzone();
    </script>
@endpush
