@php
    $errorName = str_replace(['][', '[', ']'], ['.', '.', ''], $name);
@endphp
<div class="form-group m-0 {{ $errors->has($errorName) ? ' has-danger' : '' }}">
    <label class="form-control-label" for="textarea-{{ $name }}">{{ $label }}</label>
    <textarea rows="5" placeholder="Write a large text here ..." class="form-control form-control-sm {{ $errors->has($name) ? ' is-invalid' : '' }}">{{ old($name, $value ?? '') }}</textarea>
    @include('alerts.feedback', ['field' => $errorName])
</div>
