@php
    $errorName = str_replace(['][', '[', ']'], ['.', '.', ''], $name);
@endphp
<div class="form-group{{ $errors->has($errorName) ? ' has-danger' : '' }}">
    <label class="form-control-label" for="input-dimensions_unit">{{ __('Dimensions Unit') }}</label>
    <select
        name="{{$name}}"
        id="input-{{$name}}"
        class="{{$class??''}} form-control form-control-sm{{ $errors->has($errorName) ? ' is-invalid' : '' }}">
        <option selected disabled>{{__('Select')}}</option>
        @foreach(\App\Models\Customer::DIMENSIONS_UNITS as $val)
            <option value="{{$val['unit']}}" {{$val['unit'] == ($selected ?? old($name)) ? 'selected' : ''}}>{{$val['title']}}</option>
        @endforeach
    </select>

    @include('alerts.feedback', ['field' => $errorName])
</div>
