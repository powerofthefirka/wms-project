@extends('layouts.app', ['title' => __('Shipping Method Mapping Management')])

@section('content')

    <div class="container-fluid mt--6">
        <div class="col-12 mt-2">
            @include('alerts.success')
            @include('alerts.errors')
        </div>
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Add Shipping Method Mapping') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('shipping_method_mapping.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('shipping_method_mapping.store') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Shipping Method Mapping') }}</h6>
                            <div class="pl-lg-4">
                                @include('shipping_method_mapping.mappingInformationFields')
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary ">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ShippingMethodMapping();
    </script>
@endpush
