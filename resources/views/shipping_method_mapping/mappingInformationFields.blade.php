@include('shared.forms.input', [
    'name' => 'shipping_method_text',
    'label' => __('Shipping Method Text'),
    'value' => isset($mapping) ? $mapping->shippingMethodText() : ''
])

@include('shared.forms.ajaxSelect', [
    'url' => route('shipping_method_mapping.filter_customers'),
    'name' => 'customer_id',
    'className' => 'ajax-user-input customer_id',
    'placeholder' => 'Search',
    'label' => 'Customer',
    'default' => [
        'id' => $mapping->customer->id ?? session('customer_id')  ?? old('customer_id'),
        'text' => $mapping->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
    ]
])

<div class="form-group">
    <label class="form-control-label">{{ __('Shipping Service') }}</label>
    <select name="shipping_service" class="form-control enabled-for-customer" data-toggle="select" data-placeholder="">
        <option value="{{$mapping->shipping_service ?? old('shipping_service') ?? 'chooseHere'}}">{{$mapping->shipping_service ?? ''}}</option>
    </select>
</div>

<div class="form-group">
    <label class="form-control-label">{{ __('Shipping Service Carrier') }}</label>
    <select name="shipping_service_carrier_id" class="form-control enabled-for-customer" data-toggle="select" data-placeholder="">
        <option value="{{$mapping->shipping_service_carrier_id ?? old('shipping_service_carrier_id') ?? 'chooseHere'}}"></option>
    </select>
</div>

<div class="form-group">
    <label class="form-control-label">{{ __('Shipping Service Method') }}</label>
    <select name="shipping_service_method_code" class="form-control enabled-for-customer" data-toggle="select" data-placeholder="">
        <option value="{{$mapping->shipping_service_method_code ?? old('shipping_service_method_code') ?? 'chooseHere'}}"></option>
    </select>
</div>
