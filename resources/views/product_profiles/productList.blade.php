<div class="modal-content product-list">
    <div class="modal-header text-center">
        <h5 class="modal-title" id="shipping-modal">{{ __('Profile') }} {{ $productProfile->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body p-0">
        <fieldset>
            <div class="table-responsive pl-0 pt-0 pr-0 pb-4 card table-card pagination-padding">
                <table class="table align-items-center table-hover col-12 p-0" id="product-table" style="width: 100% !important;">
                    <thead class="thead-light"></thead>
                    <tbody></tbody>
                </table>
            </div>
        </fieldset>
    </div>
    <div class="modal-footer text-right">
        <button type="button" class="btn btn-primary save" data-dismiss="modal">{{ __('Save') }}</button>
    </div>
</div>
