@extends('layouts.app', ['title' => __('Purchase order management'), 'submenu' => 'purchase_orders.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('product_profiles.update', ['product_profiles' => $productProfile]) }}" autocomplete="off">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Edit Product Profile') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-0 pt-0">
                            <div class="row">
                                <div class="col col-12">
                                    @include('shared.forms.input', [
                                       'name' => 'name',
                                       'label' => __('Name'),
                                       'value' => $productProfile->name
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('billings.product_profiles') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection
