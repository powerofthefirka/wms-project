@extends('layouts.app', ['title' => __('Product profiles')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                </div>
            </div>
            <div class="col col-12">
                @include('alerts.ajax_success')
                @include('alerts.ajax_error')
            </div>
            <div class="col col-12">
                <div class="card">
                    <div class="row p-2">
                        <div class="col-8">
                            @include('shared.forms.input', [
                               'name' => 'name',
                               'label' => '',
                            ])
                            @if($customers)
                                <select name="customer_id" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
                                    @foreach($customers as $customer)
                                        <option value="{{$customer->id}}">
                                            {{$customer->contactInformation->name}}
                                        </option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label></label>
                                <a href="{{route('product_profiles.store')}}"
                                   class="btn btn-primary create-product-profile form-control form-control-sm">
                                    {{__('Create Product Profile')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="product-profiles-table" style="width: 100% !important;">
                            <thead class="thead-light"></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('product_profiles.productModal')
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        let errorInputEmpty = "{{__('Name cannot be empty')}}"
        new ProductProfile();
    </script>
@endpush
