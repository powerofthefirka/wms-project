@extends('layouts.app', ['title' => __('Dashboard'), 'icon' => '<i class="ci ci-dashboard"></i>', 'class' => 'dashboard'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12">
                <div class="card card-calendar">
                    <div class="card-header pb-0">
                        <h3 class="font-weight-500"><i class="font-weight-400">{{ __('Hi') }},</i> {{ auth()->user()->contactInformation['name'] }}</h3>
                        <p>{{ __('Filter by date to get updated results') }}</p>
                    </div>
                    <div class="card-body pt-0 pb-0">
                        <form method="post" action="{{ route('user_settings.dashboard_settings') }}"
                              autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-12 col-md-4 calendar-input">
                                    <input class="form-control" type="text" name="dashboard_filter_date_start" value="{{$dashboardFilterDateStart ?? ''}}">
                                </div>
                                <div class="form-group col-12 col-md-4 calendar-input">
                                    <input class="form-control" type="text" name="dashboard_filter_date_end" value="{{$dashboardFilterDateEnd ?? ''}}">
                                </div>
                                <div class="form-group col-12 col-md-4">
                                    <button type="submit" class="col-12 btn btn-primary text-white">{{ __('Filter Now') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col col-12 widgets-container">
                @include('shared.draggable.dashboard_container')
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=&v=weekly"></script>
    <script>
        $(function() {
            let startDate = $('input[name="dashboard_filter_date_start"]');
            let startDateValue = startDate.val() ? new Date(startDate.val()) : moment().subtract(DEFAULT_DASHBOARD_DATE_RANGE, 'd').format('Y-MM-DD');

            startDate.daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: startDateValue
            });

            let endDate = $('input[name="dashboard_filter_date_end"]');
            let endDateValue = endDate.val() ? new Date(endDate.val()) : new Date();

            endDate.daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                },
                singleDatePicker: true,
                showDropdowns: true,
                startDate: endDateValue
            });
        })
    </script>
@endpush
