<li>
    <ul>
        <li class="nav-item">
            <a href="{{ route('domains.index') }}" class="nav-link {{ current_page(route('domains.index')) }}">
                <span class="nav-link-text">{{ __('Domains') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('domains.create') }}" class="nav-link {{ current_page(route('domains.create')) }}">
                <span class="nav-link-text">{{ __('Create Domain') }}</span>
            </a>
        </li>
    </ul>
</li>

