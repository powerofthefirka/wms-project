@include('shared.forms.input', [
   'name' => 'domain',
   'label' => __('Domain'),
   'value' => $domain->domain ?? ''
])
@include('shared.forms.input', [
    'name' => 'title',
    'label' => __('Title'),
    'value' => $domain->title ?? ''
])
<div class="form-group{{ $errors->has('primary_color') ? ' has-danger' : '' }}">
    <label class="form-control-label" for="">{{ __('Primary Color') }}</label>
    <input data-jscolor="" type="text" name="primary_color" id="" class="form-control form-control-sm{{ $errors->has('primary_color') ? ' is-invalid' : '' }}" placeholder="{{ __('Primary Color') }}" value="{{ $domain->primary_color ?? '' }}">
    @include('alerts.feedback', ['field' => 'primary_color'])
</div>
<div class="form-group">
    <img class="avatar" alt="Image placeholder" src="{{ $domain->logo->source ?? '/images/camera.png'}}"><br>
    <label class="form-control-label" for="logo_id">{{ __('Logo') }}</label>
    <div class="custom-file">
        <input type="file" class="custom-file-input" id="" name="logo_id">
        <label class="custom-file-label" for="logo_id">Select file</label>
    </div>
</div>
<div class="form-group">
    <img class="avatar" alt="Image placeholder" src="{{ $domain->favicon->source ?? '/images/camera.png'}}"><br>
    <label class="form-control-label" for="favicon_id">{{ __('Favicon') }}</label>
    <div class="custom-file">
        <input type="file" class="custom-file-input" id="" name="favicon_id">
        <label class="custom-file-label" for="favicon_id">Select file</label>
    </div>
</div>
