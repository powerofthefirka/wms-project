@extends('layouts.app', ['title' => __('Domain Management')])

@section('content')
    <div class="container-fluid">
        <form id="domain-form" method="post" action="{{ route('domains.update', ['threePl' => $threePl, 'domain' => $domain]) }}"
              autocomplete="off"
              enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Edit 3PL') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="nav-wrapper pt-0">
                                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                            aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('three_pls.edit', [ 'threePl' => $threePl ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('3PL') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_users', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Users</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_pricing_plan', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Pricing Plan</a>
                                    </li>
                                    @if($threePl->pricingPlan)
                                        <li class="nav-item">
                                            <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.billing_report', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-collection mr-2"></i>Billing Report</a>
                                        </li>
                                    @endif
                                    <li class="nav-item">
                                        <div class="nav-link mb-sm-3 mb-md-0 active  " id="tabs-icons-text-2-tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Domain & Theme Settings') }}</div>
                                    </li>
                                </ul>
                            </div>
                            <br>
                            <div class="row">
                                <h6 class="heading-small text-muted mb-4 col-8">{{ __('Edit Domain') }}</h6>
                                <div class="col-4 d-flex align-items-center justify-content-end">
                                    <a href="{{ route('domains.index', ['threePl' => $threePl]) }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <fieldset>
                                <input type="hidden" id="three-pl-id" name="3pl_id" value="{{$threePl->id}}">
                                @include('domains.domainInformationFields', [
                                    'domain' => $domain
                                ])
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('domains.index', ['threePl' => $threePl]) }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" id="submit-button" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new DomainForm();
    </script>
@endpush
