<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('locations.index') }}" class="nav-link {{ current_page(route('locations.index')) }}">
                    <span class="nav-link-text">{{ __('Locations') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
