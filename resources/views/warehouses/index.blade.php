@extends('layouts.app', ['title' => __('Warehouses')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="row">
            <div class="col col-12 col-xl-12 col-lg-12 ">
                <input id="warehouse-search" type="text" class="form-control form-control-lg form-control-alternative" placeholder="Search for warehous..">
            </div>
            <div class="col mt-4">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Warehouses') }}</h3>
                            </div>
                        </div>
                        <table class="table align-items-center table-hover col-12 p-0" id="warehouses-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new WarehouseForm();
    </script>
@endpush

