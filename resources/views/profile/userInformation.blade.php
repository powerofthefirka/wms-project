<form method="post" action="{{ route('profiles.update') }}" autocomplete="off"
      enctype="multipart/form-data">
    @csrf
    @method('put')

    <h6 class="heading-small text-muted">{{ __('User information') }}</h6>

    @include('alerts.success')
    @include('alerts.error_self_update', ['key' => 'not_allow_profile'])

    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
        <input type="text" name="name" id="input-name" class="form-control form-control-sm{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->contactInformation->name) }}" required autofocus>

        @include('alerts.feedback', ['field' => 'name'])
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
        <input type="email" name="email" id="input-email" class="form-control form-control-sm{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required>

        @include('alerts.feedback', ['field' => 'email'])
    </div>

    <div class="form-group">
        <label class="form-control-label" for="timezone">{{__('Timezone')}}</label>
        <select name="timezone" class="form-control form-control-sm" id="timezone" required>
            @foreach($timezones as $time)
                <option
                    {{$time['zone'] == $userTimezone ? 'selected' : ''}}
                    value="{{$time['zone'] ?? 0}}"> ({{$time['GMT_difference']. ' ) '. $time['zone']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label class="form-control-label" for="timezone">{{__('Date format')}}</label>
        <select name="date_format" class="form-control form-control-sm" id="date_format" required>
            <option
                {{$dateFormat === 'Y/m/d' ? 'selected' : ''}}
                value="Y/m/d">
                {{\Carbon\Carbon::now()->format('Y/m/d')}}
            </option>
            <option
                {{$dateFormat === 'Y-m-d' ? 'selected' : ''}}
                value="Y-m-d">
                {{\Carbon\Carbon::now()->format('Y-m-d')}}
            </option>
            <option
                {{$dateFormat === 'm/d/Y' ? 'selected' : ''}}
                value="m/d/Y">
                {{\Carbon\Carbon::now()->format('m/d/Y')}}
            </option>
        </select>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
    </div>
</form>
<hr class="my-4" />
<form method="post" action="{{ route('profiles.password') }}" autocomplete="off">
    @csrf
    @method('put')

    <h6 class="heading-small text-muted">{{ __('Password') }}</h6>

    @include('alerts.success', ['key' => 'password_status'])
    @include('alerts.error_self_update', ['key' => 'not_allow_password'])

    <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-current-password">{{ __('Current Password') }}</label>
        <input type="password" name="old_password" id="input-current-password" class="form-control form-control-sm{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="{{ __('Current Password') }}" value="" required>

        @include('alerts.feedback', ['field' => 'old_password'])
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-password">{{ __('New Password') }}</label>
        <input type="password" name="password" id="input-password" class="form-control form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('New Password') }}" value="" required>

        @include('alerts.feedback', ['field' => 'password'])
    </div>
    <div class="form-group">
        <label class="form-control-label" for="input-password-confirmation">{{ __('Confirm New Password') }}</label>
        <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-sm" placeholder="{{ __('Confirm New Password') }}" value="" required>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-primary">{{ __('Change password') }}</button>
    </div>
</form>
