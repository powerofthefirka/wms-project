<form id="update_token" method="post" action="{{ route('profiles.access_tokens') }}" autocomplete="off">
    @csrf
    @method('put')
</form>
<h6 class="heading-small text-muted">{{ __('Access Tokens') }}</h6>

@include('alerts.success', ['key' => 'access_token_status'])
@include('alerts.error_self_update', ['key' => 'not_allow_password'])

@foreach (auth()->user()->printableTokens() as $token)
    <form id="delete-token-{{$token->id}}" action="{{ route('profiles.delete_token', ['token' => $token]) }}" method="post" style="display: inline-block">
        @csrf
        @method('delete')
    </form>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="form-control-label" for="access_token_name[{{$token->id}}]">{{ __('Name') }}</label>
                <input form="update_token" type="text" class="form-control form-control-sm" name="access_token[{{$token->id}}]" id="access_token_name[{{$token->id}}]" value="{{ $token->name }}" />

                @include('alerts.feedback', ['field' => 'access_token_name'])
            </div>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <label class="form-control-label" for="access_token_token[{{$token->id}}]">{{ __('Token') }}</label>
                <div class="input-group">
                    <input type="text" class="form-control form-control-sm" id="access_token[{{$token->id}}]" value="{{ $token->accessToken['access_token'] }}" readonly="readonly" />
                    <div class="input-group-append">
                        <button data-clipboard-text="{{ $token->accessToken['access_token'] }}" class="btn btn-outline-primary" type="button">
                            <i class="fas fa-copy" alt="Copy to clipboard"></i>
                        </button>
                        <button form="delete-token-{{ $token->id }}" class="btn btn-outline-danger" type="submit" data-confirm-action="{{ __('Are you sure you want to delete this token?') }}">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach

<h6 class="heading-small text-muted">{{ __('Create new') }}</h6>

<div class="form-group">
    <label class="form-control-label" for="access_token_name">{{ __('Token name') }}</label>
    <input form="update_token" type="text" class="form-control form-control-sm" name="access_token[]" id="access_token_name" />

    @include('alerts.feedback', ['field' => 'access_token_name'])
</div>

<div class="text-right">
    <button form="update_token" type="submit" class="btn btn-primary">{{ __('Save') }}</button>
</div>
