@extends('layouts.app', ['title' => __('User Profile'), 'icon' => '<i class="ci ci-orders"></i>'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Profile') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="nav-wrapper pt-0">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text"
                                role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 active" id="user-information-tab"
                                       data-toggle="tab" href="#user-information" role="tab"
                                       aria-controls="user-information" aria-selected="true"><i
                                            class="ni ni-cloud-upload-96 mr-2"></i>User Information</a>
                                </li>
                                @if(auth()->user()->isAdmin())
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0" id="access-tokens-tab" data-toggle="tab"
                                           href="#access-tokens" role="tab" aria-controls="access-tokens2"
                                           aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Access Tokens</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0" id="webhooks-tab" data-toggle="tab"
                                           href="#webhooks" role="tab" aria-controls="webhooks" aria-selected="false"><i
                                                class="dripicons-calendar mr-2"></i>Webhooks</a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="email-notifications-tab" data-toggle="tab"
                                       href="#email-notifications" role="tab" aria-controls="email-notifications"
                                       aria-selected="false"><i class="dripicons-bell mr-2"></i>Email notifications</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="user-information" role="tabpanel"
                                 aria-labelledby="user-information-tab">
                                @include('profile.userInformation')
                            </div>
                            @if(auth()->user()->isAdmin())
                                <div class="tab-pane fade" id="access-tokens" role="tabpanel"
                                     aria-labelledby="access-tokens-tab">
                                    @include('profile.accessTokens')
                                </div>
                                <div class="tab-pane fade" id="webhooks" role="tabpanel" aria-labelledby="webhooks-tab">
                                    @include('webhooks.index')
                                </div>
                            @endif
                            <div class="tab-pane fade" id="email-notifications" role="tabpanel"
                                 aria-labelledby="email-notifications-tab">
                                @include('email_notifications.index')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

