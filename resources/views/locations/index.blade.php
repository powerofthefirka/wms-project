@extends('layouts.app', ['title' => __('Locations'), 'icon' => '<i class="ci ci-orders"></i>'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <input id="location-search" type="text" class="form-control form-control-lg form-control-alternative" placeholder="Search for location..">
            </div>
            <div class="col mt-4">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Locations') }}</h3>
                            </div>
                        </div>
                        <table class="table align-items-center table-hover col-12 p-0" id="location-table" style="width: 100% !important;" data-warehouse-id="@if (!empty($warehouse)){{ $warehouse->id }}@endif">
                            <thead class="thead-light"></thead>
                            <tbody style="cursor:pointer"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new LocationForm();
    </script>
@endpush

