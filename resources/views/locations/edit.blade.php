@extends('layouts.app', ['title' => __('Locations'), 'icon' => '<i class="ci ci-orders"></i>'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('View Location') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('locations.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="heading-small text-muted mb-4">{{ __('Warehouse information') }}</h6>
                        <h4>{{ __('Location') }}</h4>
                        <p>{{ $location->name }}</p>
                        <p>@if (empty($location->pickable)) {{ __('Not pickable') }} @else {{ __('Pickable') }} @endif</p>
                    </div>
                    <div class="table-responsive">
                        <table class="col-12 table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Product') }}</th>
                                    <th scope="col">{{ __('Quantity on hand') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="item_container">
                            @if(count($location->products) == 0 )
                                <tr class="order-item-fields">
                                    <td style="white-space: unset" colspan="2">
                                        <span>{{ __('No products in location') }}</span>
                                    </td>
                                </tr>
                            @endif
                            @foreach($location->products as $product)
                                <input type="hidden" name="location_products[{{$product->id}}][location_product_id]" value="{{$product->id}}">
                                <tr class="order-item-fields">
                                    <td style="white-space: unset">
                                        {{ 'SKU: ' . $product->product['sku'] . ', NAME: ' . $product->product['name'] }}
                                    </td>
                                    <td>
                                        {{ $product->quantity_on_hand }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new LocationForm();
    </script>
@endpush
