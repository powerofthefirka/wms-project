@extends('layouts.app', ['title' => __('Pricing Plan Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')
    <div class="container-fluid">
        <form id="pricing-plan-form" method="post" action="{{ route('pricing_plans.update', [ 'pricingPlan' => $pricingPlan ]) }}"
              autocomplete="off"
              enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Pricing Plan Information') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <fieldset>
                                @include('pricing_plans.pricingPlanInformationFields')
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('pricing_plans.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" id="submit-button" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection
