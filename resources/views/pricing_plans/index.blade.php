@extends('layouts.app', ['title' => __('Pricing Plans'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4 d-flex align-items-center">
                                <h3 class="mb-0">{{ __('Pricing Plans') }}</h3>
                            </div>
                            <div class="col-8 d-flex align-items-center justify-content-end">
                                <a href="{{ route('pricing_plans.create') }}" class="btn btn-secondary btn-sm">
                                    <i class="ci ci-more"></i>
                                    {{ __('Add Pricing Plan') }}
                                </a>
                                @include('shared.modals.showHideTableColumns')
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="pricing-plan-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new PricingPlanForm();
    </script>
@endpush