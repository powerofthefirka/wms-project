@include('shared.forms.input', [
   'name' => 'name',
   'label' => __('Name'),
   'value' => $pricingPlan->name ?? ''
])
@include('shared.forms.input', [
   'name' => 'monthly_price_for_yearly_billing_period',
   'label' => __('Monthly Price for Yearly Billing Period'),
   'value' => $pricingPlan->monthly_price_for_yearly_billing_period ?? ''
])
@include('shared.forms.input', [
   'name' => 'monthly_price_for_monthly_billing_period',
   'label' => __('Monthly Price for Monthly Billing Period'),
   'value' => $pricingPlan->monthly_price_for_monthly_billing_period ?? ''
])
@include('shared.forms.input', [
   'name' => 'initial_fee',
   'label' => __('Initial Fee'),
   'value' => $pricingPlan->initial_fee ?? ''
])
@include('shared.forms.input', [
   'name' => 'three_pl_billing_fee',
   'label' => __('3PL Billing Fee'),
   'value' => $pricingPlan->three_pl_billing_fee ?? ''
])
@include('shared.forms.input', [
   'name' => 'number_of_monthly_orders',
   'label' => __('Number of Monthly Orders'),
   'value' => $pricingPlan->number_of_monthly_orders ?? ''
])
@include('shared.forms.input', [
   'name' => 'price_per_additional_order',
   'label' => __('Price Per Additional Order'),
   'value' => $pricingPlan->price_per_additional_order ?? ''
])
