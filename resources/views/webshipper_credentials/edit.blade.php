
@extends('layouts.app', ['title' => __('Webshipper Credential Management')])

@section('content')

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Webshipper Credential') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('webshipper_credential.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card shadow">
                            <div class="col-12 mt-2">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('webshipper_credential.update', [ 'webshipperCredential' => $webshipperCredential, 'id' => $webshipperCredential->id ]) }}" autocomplete="off">
                                    @csrf
                                    <h6 class="heading-small text-muted mb-4">{{ __('Webshipper Credential information') }}</h6>
                                    <div class="pl-lg-4">
                                        {{ method_field('PUT') }}
                                        @include('webshipper_credentials.credentialInformationFields', [
                                            'webshipperCredential' => $webshipperCredential
                                        ])
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary ">{{ __('Save') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

