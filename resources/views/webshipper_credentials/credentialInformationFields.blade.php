@include('shared.forms.input', [
    'name' => 'api_base_url',
    'label' => __('API Base URL'),
    'value' => $webshipperCredential->api_base_url ?? ''
])
@include('shared.forms.input', [
    'name' => 'api_key',
    'label' => __('API Key'),
    'value' => $webshipperCredential->api_key ?? ''
])
@include('shared.forms.ajaxSelect', [
    'url' => route('webshipper_credential.filterCustomers'),
    'name' => 'customer_id',
    'className' => 'ajax-user-input',
    'placeholder' => 'Search',
    'label' => 'Customer',
    'default' => [
        'id' => $webshipperCredential->customer->id ?? session('customer_id')  ?? old('customer_id'),
        'text' => $webshipperCredential->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
    ]
])
