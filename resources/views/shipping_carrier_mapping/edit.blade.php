
@extends('layouts.app', ['title' => __('Shipping Carrier Mapping Management')])

@section('content')

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Shipping Carrier Mapping') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('shipping_carrier_mapping.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card shadow">
                            <div class="col-12 mt-2">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('shipping_carrier_mapping.update', [ 'mapping' => $mapping, 'id' => $mapping->id ]) }}" autocomplete="off">
                                    @csrf
                                    <h6 class="heading-small text-muted mb-4">{{ __('Shipping Carrier Mapping Information') }}</h6>
                                    <div class="pl-lg-4">
                                        {{ method_field('PUT') }}
                                        @include('shipping_carrier_mapping.mappingInformationFields', [
                                            'mapping' => $mapping
                                        ])
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary ">{{ __('Save') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ShippingCarrierMapping();
    </script>
@endpush
