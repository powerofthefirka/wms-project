@extends('layouts.app')

@section('content')

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Shipping Carrier Mapping') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('shipping_carrier_mapping.create') }}" class="btn btn-secondary btn-sm">{{ __('Add Shipping Carrier Mapping') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="table-responsive p-4">
                        <table class="table align-items-center table-hover col-12" id="shipping-carrier-mapping-table">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ShippingCarrierMapping();
    </script>
@endpush


