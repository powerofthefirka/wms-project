<div class="row align-items-center">
    <div class="col-12 text-right">
        <a href="{{ route('webhook.create') }}" class="btn btn-secondary btn-sm">{{ __('Add webhook') }}</a>
    </div>
    <div class="col-12 mt-2">
        @include('alerts.success', ['key' => 'webhook_status'])
        @include('alerts.errors')
    </div>
</div>
<div class="card m-0 mt-2">
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table align-items-center table-hover col-12">
                <thead class="">
                <tr>
                    <th scope="col">{{ __('Name') }}</th>
                    <th scope="col">{{ __('Operation') }}</th>
                    <th scope="col">{{ __('Url') }}</th>
                    <th scope="col">{{ __('Customer Name') }}</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($webhooks as $webhook)
                    <tr>
                        <td>{{ $webhook->name }}</td>
                        <td>{{ $webhook->operation }}</td>
                        <td>{{ $webhook->url }}</td>
                        <td><a href="{{ route('customers.edit', [ 'customer' => $webhook->customer ]) }}">{{ $webhook->customer->contactInformation->name }}</a></td>
                        <td class="text-right">
                            <a href="{{ route('webhook.edit', [ 'webhook' => $webhook ]) }}" class="btn btn-sm btn-secondary">{{ __('Edit') }}</a>
                            <form action="{{ route('webhook.destroy', ['webhook' => $webhook, 'id' => $webhook->id]) }}" method="post" style="display: inline-block">
                                @csrf
                                @method('delete')
                                <button type="button" class="btn btn-primary btn-sm text-white remove-item-button" data-confirm-action="{{ __('Are you sure you want to delete this webhook?') }}">
                                    {{ __('×') }}
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
