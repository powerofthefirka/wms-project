@include('shared.forms.ajaxSelect', [
    'url' => route('webhook.filter_customers'),
    'name' => 'customer_id',
    'className' => 'ajax-user-input',
    'placeholder' => 'Search for a user to add',
    'label' => 'Add a Customer',
    'readonly' => isset($webhook) ? 'true' : null,
    'default' => [
        'id' => $webhook->customer->id ?? session('customer_id')  ?? old('customer_id'),
        'text' => $webhook->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
    ]
])
@include('shared.forms.input', [
   'name' => 'name',
   'label' => __('Name'),
   'disabled' => isset($webhook) ? 'disabled' : '',
   'value' => $webhook->name ?? ''
])
<div class="form-group">
    <label class="form-control-label">{{ __('Object Type') }}</label>
    <select name="object_type" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
        @foreach($object_types as $key => $object_type)
            <option value="{{$object_type}}"
                @if(isset($webhook->object_type))
                    {{$webhook->object_type === $object_type ? 'selected' : ''}}
                @endif
            >{{$key}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label class="form-control-label">{{ __('Operation') }}</label>
    <select name="operation" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
        @foreach(['Store', 'Update', 'Destroy'] as $operation)
            <option value="{{$operation}}"
                @if(isset($webhook->operation))
                    {{$webhook->operation === $operation ? 'selected' : ''}}
                @endif
            >{{ __($operation) }}</option>
        @endforeach
    </select>
</div>
@include('shared.forms.input', [
   'name' => 'url',
   'label' => __('Url'),
   'value' => $webhook->url ?? ''
])
@include('shared.forms.input', [
   'name' => 'secret_key',
   'label' => __('Secret Key'),
   'value' => $webhook->secret_key ?? ''
])
