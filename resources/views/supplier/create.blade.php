@extends('layouts.app', ['title' => __('Vendor Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')

    <div class="container-fluid">
        <form method="post" action="{{ route('vendors.store') }}" autocomplete="off">
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Add Vendor') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="heading-small text-muted mb-4">{{ __('Vendor information') }}</h6>
                            @include('supplier.supplierInformationFields')
                        </div>
                        <div class="card-header pt-0 pb-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="heading-small text-muted">{{ __('Product information') }}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-0">
                            <fieldset>
                                <div class="row">
                                    <div class="col col-12 mb--2">
                                        @include('shared.forms.ajaxSelect', [
                                            'url' => route('orders.filter_products', ['customer' => session('customer_id') ?? old('customer_id')]),
                                            'name' => '',
                                            'required' => 'required',
                                            'className' => 'ajax-user-input product_id',
                                            'placeholder' => __('Search'),
                                            'label' => __('Search to add product')
                                        ])
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="col-12 table align-items-center table-flush">
                                    <thead class="">
                                    <tr>
                                        <th scope="col">{{ __('Products') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="item_container">
                                    @if(count( old()['supplier_product']['products'] ?? [] ) > 0)
                                        @foreach(old()['supplier_product']['products'] as $key => $item)
                                            @php
                                                $product = App\Models\Product::find($item ?? null);
                                            @endphp
                                            @if($product)
                                                <tr class="order-item-fields {{ $item ? '':'d-none' }}">
                                                    <td style="white-space: unset">
                                                        <input type="hidden" class="product_supplier"
                                                               name="supplier_product[products][{{$key}}]"
                                                               value="{{$product['id']}}"
                                                        >
                                                        <span class="text-body product_name">{{$product['name']}}</span>
                                                    </td>
                                                    <td class="text-right">
                                                        <a class="btn btn-primary btn-sm text-white remove-item-button remove-supplier-item">
                                                            ×
                                                        </a>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr class="order-item-fields d-none">
                                                    <td style="white-space: unset">
                                                        <input type="hidden" class="product_supplier" name="supplier_product[products][{{$key}}]">
                                                        <span class="text-body product_name"></span>
                                                    </td>
                                                    <td class="text-right">
                                                        <a class="btn btn-primary btn-sm text-white remove-item-button remove-supplier-item">
                                                            ×
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr class="order-item-fields d-none">
                                            <td style="white-space: unset">
                                                <input type="hidden" class="product_supplier" name="supplier_product[products][0]">
                                                <span class="text-body product_name"></span>
                                            </td>
                                            <td class="text-right">
                                                <a class="btn btn-primary btn-sm text-white remove-item-button remove-supplier-item">
                                                    ×
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('products.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new SupplierForm();
    </script>
@endpush
