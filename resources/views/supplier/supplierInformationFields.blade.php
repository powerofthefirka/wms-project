@include('shared.forms.contactInformationFields', [
   'name' => 'contact_information',
   'contactInformation' => $supplier->contactInformation ?? ''
])
@include('shared.forms.input', [
    'name' => 'currency',
    'label' => __('Currency'),
    'value' => $supplier->currency ?? ''
])
@include('shared.forms.input', [
    'name' => 'internal_note',
    'label' => __('Internal Note'),
    'value' => $supplier->internal_note ?? ''
])
@include('shared.forms.input', [
    'name' => 'default_po_note',
    'label' => __('Default PO Note'),
    'value' => $supplier->default_po_note ?? '',
])

@if(\Illuminate\Support\Facades\Request::is('*/edit'))
    <div class="form-group {{session('customer_id') ? 'd-none' : ''}}">
        <label class="form-control-label">{{ __('Customer') }}</label><br>
        <span>
          {{$supplier->customer->contactInformation->name}}
       </span>
    </div>
@else
    @include('shared.forms.ajaxSelect', [
     'url' => route('vendors.filter_customers'),
     'name' => 'customer_id',
     'className' => 'ajax-user-input customer_id',
     'placeholder' => __('Search'),
     'label' => __('Customer'),
     'hidden' => session('customer_id') ? 'd-none' : '',
     'default' => [
            'id' => $supplier->customer->id ?? session('customer_id') ?? old('customer_id'),
            'text' => $supplier->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
        ]
    ])
@endif
