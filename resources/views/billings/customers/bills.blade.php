@extends('layouts.app', ['title' => __('Billing'), 'submenu' => 'billings.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h3 class="mb-0"> {{$customer->contactInformation->name}} {{ __('Customer Bills') }}</h3>
                            </div>
                        </div>
                        <div class="table-responsive p-0">
                            <table class="table align-items-center table-hover col-12 p-0" id="customer-bills-table" style="width: 100% !important;">
                                <thead class=""></thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-12">
                <div class="card transparent text-right">
                    <a href="{{ route('billings.index') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                    <a href="#" data-toggle="modal" data-target="#bill-create-modal" class="btn btn-primary text-white">
                        {{ __('Create Bill') }}
                    </a>
                </div>
            </div>
            @include('billings.bills.billCreateModal')
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        let customerId = {{$customer->id}};

        new BillingCustomers();
    </script>
@endpush
