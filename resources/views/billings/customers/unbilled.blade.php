@extends('layouts.app', ['title' => __('Billing'), 'submenu' => 'billings.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_shipments" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_pos" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_po_items" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unBilled_locations" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_return_items" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_packages" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_package_items" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="card table-card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="unbilled_inventory_changes" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        let billId = {{$billId}};

        new Unbilled();
    </script>
@endpush

