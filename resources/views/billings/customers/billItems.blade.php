@extends('layouts.app', ['title' => __('Billing'), 'submenu' => 'billings.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="col col-12">
                            <div class="card transparent text-right">
                                <form method="POST"
                                      action="{{route('bills.destroy', ['bill' => $bill])}}"
                                      style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit"
                                            class="btn btn-sm btn-danger text-white recalculate">{{ __('Delete Bill') }}</button>
                                </form>
                                <form method="POST" action="{{route('bills.recalculate', ['bill' => $bill])}}"
                                      style="display: inline-block">
                                    @csrf
                                    <button type="submit"
                                            class="btn btn-sm btn-primary text-white recalculate">{{ __('Recalculate') }} </button>
                                </form>
                                <a href="#" data-toggle="modal" data-target="#add-hoc-modal"
                                   class="btn btn-sm btn-primary text-white">{{ __('Ad Hoc') }}</a>
                                @include('billings.bills.adHocModal')
                                <a target="_blank" href="/bills/{{$bill->id}}/export_csv" class="btn btn-sm btn-primary text-white">{{__('Export')}}</a>
                                <a target="_blank" href="{{route('billings.unbilled', ['bill' => $bill])}}" class="btn btn-sm btn-danger text-white">{{__('UNBILLED EXPERIMENTAL')}}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h3 class="mb-0">{{ __('Bill Items') }}</h3>
                            </div>
                        </div>
                        <div class="table-responsive p-0" style="height: 900px">
                            <table class="table align-items-center table-hover col-12 p-0"
                                   style="width: 100% !important; height: 500px">
                                <thead class="">
                                <tr>
                                    <th>Fee type</th>
                                    <th>Description</th>
                                    <th>Total Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bill->billItems as $item)
                                    <tr>
                                        <td>{{$item->billingFee->name}}</td>
                                        <td>{{$item->description}}</td>
                                        <td>{{$item->total_price}}</td>
                                    </tr>
                                    @if($item->billingFee->type == \App\Models\BillingFee::SHIPPING_RATES)
                                        @php
                                            $shippingMethods = \App\Models\ShippingMethod::whereIn('id', \Illuminate\Support\Arr::get($item->billingFee->settings, 'shipping_method', []))
                                                                                           ->orWhere('id', \Illuminate\Support\Arr::get($item->billingFee->settings, 'shipping_method_id', ''))->get()->pluck('name');
                                                                                           $shippingMethods = empty($shippingMethods) ? '' : implode(', ', $shippingMethods->toArray());
                                        @endphp
                                        <tr>
                                            <td colspan="3">
                                                <table class="table align-items-center col-12 p-0">
                                                    <thead class="">
                                                    <tr>
                                                        <th>Client Name</th>
                                                        <th>Client Order Reference</th>
                                                        <th>Delivery Name</th>
                                                        <th>Delivery Address</th>
                                                        <th>Country</th>
                                                        <th>Carrier</th>
                                                        <th>Services</th>
                                                        <th>Weight ({{\Illuminate\Support\Arr::get($item->bill->customer,'weight_unit', env('DEFAULT_WEIGHT_UNIT'))}})</th>
                                                        <th>Tracking Number</th>
                                                        <th>Number of units in shipment</th>
                                                        <th>Date Dispatched</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{$item->bill->customer->contactInformation->name}}</td>
                                                        <td>{{$item->shipment->order->number}}</td>
                                                        <td>{{$item->shipment->contactInformation->name}}</td>
                                                        <td>{{$item->shipment->contactInformation->address}}</td>
                                                        <td>{{$item->shipment->contactInformation->country->title}}</td>
                                                        <td class="{{isset($shipping_carriers[$item->billingFee->settings['shipping_carrier_id']]) ? '' :'bg-red text-white'}}">{{$shipping_carriers[$item->billingFee->settings['shipping_carrier_id']] ?? 'RECALCULATE'}}</td>
                                                        <td class="{{empty($shippingMethods) ? 'bg-red text-white' :''}}">{{empty($shippingMethods) ? 'RECALCULATE' : $shippingMethods}}</td>
                                                        <td>{{$item->shipment->getTotalShipmentWeight()}}</td>
                                                        <td>{{$item->shipment->tracking_code}}</td>
                                                        <td>{{$item->shipment->shipmentItems->sum('quantity')}}</td>
                                                        <td>{{$item->shipment->shipped_at->format(get_date_format())}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('billings.customer_bills', ['customer' => $customer]) }}"
                           class="btn btn-secondary">{{ __('Back') }}</a>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('.recalculate').on('click', function () {

                $('a, button').hide()
                $(this).html('Recalculating').show().prop('disabled', true)
                $(this).closest('form').submit()
            })
        })
    </script>
@endpush
