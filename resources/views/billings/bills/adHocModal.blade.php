<div class="modal fade" id="add-hoc-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="shipping-modal">{{ __('Ad Hoc') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{route('bills.ad_hoc', ['bill' => $bill])}}">
                @csrf
                <div class="modal-body">
                    <fieldset>
                        <div class="form-group md-3">
                            <div class="form-group">
                                <label class="form-control-label" >{{ __('Billing Fee') }}</label>
                                <select name="billing_fee_id" class="form-control form-control-sm">
                                    <option value="">{{ __('Choose..') }}</option>
                                    @foreach ($adHocs as $adHoc)
                                        <option value="{{ $adHoc->id }}">
                                            Name: {{$adHoc->name}} -
                                            Description: {{$adHoc->description}} -
                                            Unit: {{ $adHoc->settings['unit']}} -
                                            Unit Rate: {{ $adHoc->settings['rate']}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="form-control-label" >{{ __('Quantity') }}</label>
                            <input
                                type="text"
                                name="quantity"
                                class="form-control"
                                placeholder="Quantity"
                                value=""
                                autocomplete="off"
                            >
                            <label class="form-control-label" >{{ __('Date') }}</label>
                            <input
                                type="text"
                                name="period_end"
                                class="datetimepicker form-control"
                                placeholder="Date"
                                value=""
                                autocomplete="off"
                            >
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-primary shipping-method-btn">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function () {
            $('.datetimepicker').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                autoApply: false,
                autoUpdateInput:false,
                locale: {
                    format: 'Y-MM-DD'
                }
            });

            $('.datetimepicker').on('apply.daterangepicker', function(ev, picker) {
                $('.datetimepicker')
                    .val(moment(picker.startDate).format('Y-MM-DD'))
            });
        })
    </script>
@endpush
