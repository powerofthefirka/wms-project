<div class="modal fade" id="bill-create-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="shipping-modal">{{ __('Create Bill') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{route('bills.store', ['customer' => $customer])}}">
                @csrf
                <div class="modal-body pb-0">
                    <fieldset>
                        <div class="form-group md-3">
                            <label>{{__('Start Date')}}</label>
                            <input
                                type="text"
                                name="start_date"
                                class="datetimepicker form-control"
                                autocomplete="off"
                                placeholder="Dates between"
                                value={{$lastBillEndDate}}
                            >
                            <label>{{__('End Date')}}</label>
                            <input
                                type="text"
                                name="end_date"
                                class="datetimepicker form-control"
                                placeholder="Dates between"
                                autocomplete="off"
                            >
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer text-right">
                    <button type="submit" class="btn btn-primary shipping-method-btn">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function () {
            $('.datetimepicker').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                autoApply: false,
                autoUpdateInput:false,
                locale: {
                    format: 'Y-MM-DD'
                }
            });

            $('.datetimepicker').on('apply.daterangepicker', function(ev, picker) {
                $(this)
                    .val(moment(picker.startDate).format('Y-MM-DD'))
            });
        })
    </script>
@endpush
