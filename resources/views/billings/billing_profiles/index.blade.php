@extends('layouts.app', ['title' => __('Orders'), 'submenu' => 'orders.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h3 class="mb-0">{{ __('3PL Billing') }}</h3>
                            </div>
                        </div>
                        @include('billings.menuLinks', ['active' => 'billing_profiles'])
                        <div class="row">
                            <div class="col-6">
                            </div>
                            <div class="col-6 text-right">
                                <a href="{{ route('billing_profiles.create') }}" class="btn btn-secondary btn-sm">{{ __('Add billing profile') }}</a>
                            </div>
                        </div>
                        <div class="table-responsive p-0">
                            <table class="table align-items-center table-hover col-12 p-0" id="billing-profile-table" style="width: 100% !important;">
                                <thead class=""></thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        new BillingProfile();
    </script>
@endpush
