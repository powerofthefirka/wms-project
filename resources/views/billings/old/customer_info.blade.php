<div class="container-fluid">
    <div class="row">
        <div class="col-4">
            <ul class="list-unstyled">
                <li class="text-muted">
                    {{__('Customer Name')}}: {{$customer->contactInformation->name}}
                </li>
                <li class="text-muted">
                    {{__('Company Name')}}: {{$customer->contactInformation->company_name}}
                </li>
                <li class="text-muted">
                    {{__('Adress')}}: {{$customer->contactInformation->address}}
                </li>
                <li class="text-muted">
                    {{__('Adress 2')}}: {{$customer->contactInformation->address2}}
                </li>
                <li class="text-muted">
                    {{__('Zip')}}: {{$customer->contactInformation->zip}}
                </li>
                <li class="text-muted">
                    {{__('City')}}: {{$customer->contactInformation->city}}
                </li>
                <li class="text-muted">
                    {{__('Email')}}: {{$customer->contactInformation->email}}
                </li>
                <li class="text-muted">
                    {{__('Phone')}}: {{$customer->contactInformation->phone}}
                </li>
            </ul>
            <ul class="list-unstyled">
                <li class="text-muted">
                    <form method="post" action="{{ route('billing.update_customer',['customerId' => $customer->id]) }}" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="d-inline-block w-75">
                            <select name="billing_profile_id" id="input-role_id" class="form-control d-inline-block " data-toggle="select" data-placeholder="">
                                @foreach($billingProfiles as $billingProfile)
                                    <option value="{{$billingProfile->id}}" {{$customer->billing_profile_id === $billingProfile->id ? 'selected' : ''}}>{{$billingProfile->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary edit d-inline-block" > {{__('Update')}} </button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>

