<h3 class="mb-3">{{__('Billing')}}:</h3>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Name')}}: <strong>{{$customer->contactInformation->name}}</strong></li>
    <li class="list-group-item">{{__('Billing Profile')}}:
        <strong>
            <a href="{{route('billing_profiles.edit', ['billingProfile' => $customer->billingProfile ?? null])}}">{{$customer->billingProfile->name}}</a>
        </strong>
    </li>
    <li class="list-group-item">{{__('Company Name')}}: <strong>{{$customer->contactInformation->company_name}}</strong></li>
    <li class="list-group-item">{{__('Invoice Count')}}: <strong>{{$customer->invoice->count()}}</strong></li>
</ul>
