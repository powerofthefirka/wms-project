@extends('layouts.app', ['title' => __('Billing Management')])

@section('content')

    <div class="container-fluid mt--6">
        <div class="col-12 mt-2">
            @include('alerts.success')
            @include('alerts.errors')
        </div>
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Customer Billings') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('orders.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('billings.old.customer_info')

                        <form method="post" action="{{ route('billings.generate_invoice',['customerId' => $customer->id]) }}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-3">
                                        <button class="btn btn-primary edit" > Generate Invoice </button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <input type="hidden" id="customer-id" value="{{$customer->id}}">
                            <table class="table align-items-center table-hover col-12" id="customer-invoices-table">
                                <thead class=""></thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        new Billing();
    </script>
@endpush
