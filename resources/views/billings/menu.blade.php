<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('billings.customers') }}" class="nav-link {{ current_page(route('billings.customers')) }}">
                    <span class="nav-link-text">{{ __('Billing Customers') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('billings.bills') }}" class="nav-link {{ current_page(route('billings.bills')) }}">
                    <span class="nav-link-text">{{ __('Bills') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('billings.billing_profiles') }}" class="nav-link {{ current_page(route('billings.billing_profiles')) }}">
                    <span class="nav-link-text">{{ __('Billing Profiles') }}</span>
                </a>
            </li>
        <!--<li class="nav-item">
                <a href="{{ route('billings.reconcile') }}" class="nav-link {{ current_page(route('billings.reconcile')) }}">
                    <span class="nav-link-text">{{ __('Reconcile') }}</span>
                </a>
            </li>-->
        <!--<li class="nav-item">
                <a href="{{ route('billings.exports') }}" class="nav-link {{ current_page(route('billings.exports')) }}">
                    <span class="nav-link-text">{{ __('Exports') }}</span>
                </a>
            </li>-->
            <li class="nav-item">
                <a href="{{ route('billings.product_profiles') }}" class="nav-link {{ current_page(route('billings.product_profiles')) }}">
                    <span class="nav-link-text">{{ __('Product Profiles') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
