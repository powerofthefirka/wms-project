<div class="btn-group mt-1">
    <a href="{{route('billings.customers')}}" class="btn btn-primary border {{$active != 'customers' ? 'bg-light' : ''}}">{{__('Customers')}}</a>
    <a href="{{route('billings.bills')}}" class="btn btn-primary border {{$active != 'bills' ? 'bg-light' : ''}}">{{__('Bills')}}</a>
    <a href="{{route('billings.billing_profiles')}}" class="btn btn-primary border {{$active != 'billing_profiles' ? 'bg-light' : ''}}">{{__('Billing Profiles')}}</a>
{{--    <a href="{{route('billings.reconcile')}}" class="btn btn-primary border {{$active != 'reconcile' ? 'bg-light' : ''}}">{{__('Reconcile')}}</a>--}}
{{--    <a href="{{route('billings.exports')}}" class="btn btn-primary border {{$active != 'exports' ? 'bg-light' : ''}}">{{__('Exports')}}</a>--}}
    <a href="{{route('billings.product_profiles')}}" class="btn btn-primary border {{$active != 'product_profiles' ? 'bg-light' : ''}}">{{__('Product Profiles')}}</a>
</div>
