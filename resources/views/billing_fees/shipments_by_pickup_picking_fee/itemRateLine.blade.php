<tr class="order-item-fields {{isset($item['from']) ? '' : 'd-none'}}">
    <td class="row">
        <label class="col-1 pl-1">
            {{__('From')}}
        </label>
        <input
            class="form-control form-control-sm col-3 item_from"
            name="settings[item_rates][{{$key ?? 0}}][from]"
            value="{{ $item['from'] ?? 0}}"
            readonly
            >
        <label class="col-1 pl-1">
            {{__('To')}}
        </label>
        <input
            class="form-control form-control-sm col-3 item_to"
            name="settings[item_rates][{{$key ?? 0}}][to]"
            value="{{ $item['to'] ?? 1}}"
            >
    </td>
    <td>
        @include('shared.forms.input', [
            'name' => 'settings[item_rates][' . ($key ?? 0) . '][rate]',
            'type' => 'number',
            'label' => '',
            'value' =>$item['rate'] ?? 0,
            'class' => 'item_rate'
        ])
    </td>
    <td class="border-0">
        <a class="btn btn-primary btn-sm text-white remove-item">
            {{ __('×') }}
        </a>
    </td>
</tr>
