@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="form-group">
        <input id="without_profile"
               type="checkbox"
               name="{{'settings[without_profile]'}}"
               value="1"
               @if(isset($settings) || old('settings'))
                   {{(old('settings.without_profile', $settings['without_profile'] ?? false) ) ? 'checked' : ''}}
               @endif
        >
        <label class="form-check-label" for="without_profile">
            {{__('Without profile')}}
        </label>
    </div>
    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>
    <div class="table-responsive p-0">
        <input class="product_profile_selectables" type="hidden" name="settings[product_profiles]" value="{{old('settings.product_profiles', $settings['product_profiles'] ?? '[]')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Product profiles') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="product_profile_selectables" data-url="/product_profiles/data_table">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>

    @include('shared.forms.input', [
        'name' => 'settings[weight_rate]',
        'label' => __('Weight rate'),
        'value' => $settings['weight_rate'] ?? '',
        'type' => 'number'
    ])

    @include('shared.forms.input', [
        'name' => 'settings[volume_rate]',
        'label' => __('Volume rate'),
        'value' => $settings['volume_rate'] ?? '',
        'type' => 'number'
    ])
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
