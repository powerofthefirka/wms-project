@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    @include('shared.forms.input', [
        'name' => 'settings[description]',
        'label' => __('Description'),
        'value' => $settings['description'] ?? ''
    ])
    <div class="form-group">
        <label class="form-control-label">{{ __('Weight unit') }}</label>
        <select name="{{'settings[unit]'}}" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
            @foreach(\App\Models\BillingFee::AD_HOC_UNITS as $unit)
                <option value="{{$unit}}"
                @if(isset($settings) || old('settings'))
                    {{$unit === old('settings.unit', $settings['unit'] ?? false) ? 'selected' : ''}}
                @endif
                >
                    {{$unit}}
                </option>
            @endforeach
        </select>
    </div>
    @include('shared.forms.input', [
        'name' => 'settings[rate]',
        'label' => __('Rate'),
        'value' => $settings['rate'] ?? '',
        'type' => 'number'
    ])
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
