@extends('layouts.app', ['title' => __('Billing Profiles'), 'submenu' => 'billings.old.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit '. $billingFee['name'] .' Fee') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('billing_profiles.edit', ['billingProfile' => $billingProfile]) . '#' . $billingFee['type'] }}"
                                   class="btn btn-secondary btn-sm">{{ __('Back to edit') }}</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card">
                            <div class="col-12 mt-2">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <div class="card-body">
                                <form id="fee-duplicate-check" method="post"
                                      action="{{ route('billing_fees.ad_hoc.update',
                                        [
                                            'billingProfile' => $billingProfile->id,
                                            'billingFee' => $billingFee
                                        ]) }}"
                                      autocomplete="off">
                                    @csrf
                                    @include('billing_fees.ad_hoc.informationFields')
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success ">{{ __('Save') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
    @push('js')
        <script>
            new BillingFee;
        </script>
    @endpush
@endsection
