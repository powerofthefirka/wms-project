@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>

    @include('shared.forms.input', [
        'name' => 'settings[rate]',
        'label' => __('Hour rate'),
        'value' => $settings['rate'] ?? '',
        'type' => 'number'
    ])
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
