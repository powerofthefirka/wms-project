@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => old('name', $billingFee['name'] ?? '')
    ])

    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>

    <div class="table-responsive p-0">
        <input class="product_profile_selectables" type="hidden" name="settings[product_profiles]" value="{{old('settings.product_profiles', $settings['product_profiles'] ?? '[]')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Product profiles') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="product_profile_selectables" data-url="/product_profiles/data_table">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>

    <div class="form-group">
        <input
            id="without_profile"
            type="checkbox"
            name="{{'settings[without_profile]'}}"
            value="1"
        @if(isset($settings) || old('settings'))
            {{old('settings.without_profile', $settings['without_profile'] ?? false) ? 'checked' : ''}}
        @endif
        >
        <label class="form-check-label" for="without_profile">
            {{__('Applies to products without profile')}}
        </label>
    </div>
    <div class="row">
        <div class="table-responsive col-12">
        <table class="table align-items-center">
            <thead class="thead-light">
            <tr>
                <th scope="col">{{ __('From/To') }}</th>
                <th scope="col">{{ __('Rate') }}</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody id="item_container">
            <tr class="order-item-fields">
                <td class="row">
                    <label class="pl-1">{{__('First item')}}</label>
                </td>
                <td>
                    @include('shared.forms.input', [
                        'name' => 'settings[item_rates][first_item_rate]',
                        'type' => 'number',
                        'label' => '',
                        'value' => old('settings.item_rates.first_item_rate', $settings['item_rates']['first_item_rate'] ?? 0),
                        'class' => ''
                    ])
                </td>
                <td class="border-0"></td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <input
                            id="first_of_additional_skus"
                            type="checkbox"
                            name="{{'settings[first_of_additional_skus]'}}"
                            value="1"
                        @if(isset($settings) || old('settings'))
                            {{old('settings.first_of_additional_skus', $settings['first_of_additional_skus'] ?? false) ? 'checked' : ''}}
                            @endif
                        >
                        <label class="form-check-label" for="first_of_additional_skus">
                            {{__('1st item of additional SKUs')}}
                        </label>
                    </div>
                </td>
                <td>
                    @include('shared.forms.input', [
                        'name' => 'settings[first_of_additional_skus_rate]',
                        'label' => '',
                        'value' => old('settings.first_of_additional_skus_rate', $settings['first_of_additional_skus_rate'] ?? ''),
                        'type' => 'number'
                    ])
                </td>
            </tr>
            @if(count( old('settings.item_rates', [])) > 0)
                @foreach(old('settings.item_rates') as $key => $item)
                    @if(isset($item['to']))
                        @include('billing_fees.returns.itemRateLine')
                    @endif
                @endforeach
            @else
                @foreach($settings['item_rates'] ?? [] as $key => $item)
                    @if(isset($item['to']))
                        @include('billing_fees.returns.itemRateLine')
                    @endif
                @endforeach
                @if(!isset($settings['item_rates']))
                    @include('billing_fees.returns.itemRateLine')
                @endif
            @endif
            </tbody>
        </table>
        <a class="add_rate btn btn-primary btn-sm" style="display: block;margin: 5px auto;width: fit-content">{{__('+')}}</a>
        <div>
            <div class="row">
                <label class="col-2 ">{{__('Rest of the items')}}</label>
                <input
                    class="form-control form-control-sm col-8"
                    name="settings[item_rates][rest_of_the_items]"
                    value="{{old('settings.item_rates.rest_of_the_items', $settings['item_rates']['rest_of_the_items'] ?? 0)}}"
                    type="number"
                    step=".01"
                >
            </div>
        </div>
{{--        <div class="form-group">--}}
{{--            <input--}}
{{--                type="checkbox"--}}
{{--                name="{{'settings[charge_kits_as_single_sku]'}}"--}}
{{--                value="1"--}}
{{--                id="charge_kits_as_single_sku"--}}
{{--                @if(isset($settings) || old('settings'))--}}
{{--                    {{ old('settings.charge_kits_as_single_sku', $settings['charge_kits_as_single_sku'] ?? false) ? 'checked' : ''}}--}}
{{--                @endif--}}
{{--            >--}}
{{--            <label class="form-check-label" for="charge_kits_as_single_sku" >--}}
{{--                {{__('Charge kits as single SKU')}}--}}
{{--            </label>--}}
{{--        </div>--}}
    </div>
    </div>
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
