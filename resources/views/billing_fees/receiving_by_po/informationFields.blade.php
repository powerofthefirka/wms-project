@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    @include('shared.forms.input', [
        'name' => 'settings[rate]',
        'label' => __('Rate'),
        'value' => $settings['rate'] ?? '',
        'type' => 'number'
    ])
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
