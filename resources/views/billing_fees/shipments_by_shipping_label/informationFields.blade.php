@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>
    <div class="table-responsive p-0">
        <input class="carriers_methods_selectables" type="hidden" name="settings[carriers_and_methods]" value="{{old('settings.carriers_and_methods', $settings['carriers_and_methods'] ?? '[]')}}">
        <input class="carriers_methods_sub_selectables" type="hidden" name="settings[methods_selected]" value="{{old('settings.methods_selected', $settings['methods_selected'] ?? '{}')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Carriers') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="carriers_methods_selectables" data-sub-selectables="carriers_methods_sub_selectables" data-url="/billing_fees/carriers_and_methods">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="form-group">
        <input
            id="include_base_shipping_cost"
            type="checkbox"
            name="{{'settings[include_base_shipping_cost]'}}"
            value="1"
        @if(isset($settings) || old('settings'))
            {{old('settings.include_base_shipping_cost', $settings['include_base_shipping_cost'] ?? false) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="custom_boxes">
            {{__('Include the base shipping cost')}}
        </label>
    </div>
    @include('shared.forms.input', [
        'name' => 'settings[base_shipping_cost]',
        'label' => __('Base shipping cost'),
        'value' => $settings['base_shipping_cost'] ?? '',
        'type' => 'number'
    ])
    @include('shared.forms.input', [
        'name' => 'settings[percentage_of_cost]',
        'label' => __('Percentage of cost'),
        'value' => $settings['percentage_of_cost'] ?? '',
        'type' => 'number'
    ])
</div>
<div class="modal fade confirm-dialog" id="selectables-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Carrier methods') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul id="table-column-name-list" class="list-group"></ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
         let feeId = '{{$billingFee->id ?? 0}}';
         new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
