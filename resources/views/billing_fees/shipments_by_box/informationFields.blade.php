@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>
    <div class="table-responsive p-0">
        <input class="shipping_boxes_selectables" type="hidden" name="settings[shipping_boxes]" value="{{old('settings.shipping_boxes', $settings['shipping_boxes'] ?? '[]')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Shipping boxes') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="shipping_boxes_selectables" data-url="/shipping_boxes/data_table">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>
    @include('shared.forms.input', [
        'name' => 'settings[rate]',
        'label' => __('Rate'),
        'value' => $settings['rate'] ?? '',
        'type' => 'number'
    ])
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
