@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="form-check">
        <input
            class="form-check-input show-or-not-check"
            type="radio"
            name="{{'settings[charge_periodically]'}}"
            id="flexRadioDefault1"
            value="1"
            @if(isset($settings) || old('settings'))
                {{old('settings.charge_periodically', $settings['charge_periodically'] ?? false) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="flexRadioDefault1">
            {{__('Charge periodically')}}
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input show-or-not-check"
               type="radio"
               name="{{'settings[charge_periodically]'}}"
               id="flexRadioDefault2"
               value="0"
               @if(isset($settings) || old('settings.charge_periodically'))
               {{old('settings.charge_periodically', $settings['charge_periodically'] ?? false) ? '' : 'checked'}}
               @else
                   checked
               @endif
        >
        <label class="form-check-label" for="flexRadioDefault2">
            {{__('Not charge periodically')}}
        </label>
    </div>
    @include('shared.forms.input', [
        'name' => 'settings[rate]',
        'label' => __('Rate'),
        'value' => $settings['rate'] ?? '',
        'type' => 'number'
    ])
    <div class="form-group show-or-not">
        <label class="form-control-label">{{ __('Period') }}</label>
        <select name="{{'settings[period]'}}" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
            @foreach(\App\Models\BillingFee::PERIODS as $period)
                <option value="{{$period}}"
                    @if(isset($settings) || old('settings'))
                        {{$period === old('settings.period', $settings['period'] ?? '') ? 'selected' : ''}}
                    @endif
                >
                    {{$period}}
                </option>
            @endforeach
        </select>
    </div>
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
