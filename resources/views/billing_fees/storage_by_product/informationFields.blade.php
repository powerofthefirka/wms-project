@include('alerts.billingFeeFeedback')
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])
    <div class="table-responsive p-0">
        <input class="location_type_selectables" type="hidden" name="settings[location_types]" value="{{old('settings.location_types', $settings['location_types'] ?? '[]')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Location types') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="location_type_selectables" data-url="/location_types/data_table/{{$billingProfile->threePl->id}}">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="table-responsive p-0">
        <input class="product_profile_selectables" type="hidden" name="settings[product_profiles]" value="{{old('settings.product_profiles', $settings['product_profiles'] ?? '[]')}}">
        <label class="form-control-label" for="selectables-table">{{ __('Product Profiles') }}</label>
        <table class="table align-items-center table-hover col-12 p-0 selectables-table" style="width: 100% !important;" data-selectables="product_profile_selectables" data-url="/product_profiles/data_table">
            <thead class="thead-light"></thead>
            <tbody></tbody>
        </table>
    </div>

    <div class="form-group show-or-not">
        <label class="form-control-label">{{ __('Period') }}</label>
        <select name="{{'settings[period]'}}" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
            @foreach(\App\Models\BillingFee::PERIODS as $period)
                <option value="{{$period}}"
                @if(isset($settings) || old('settings'))
                    {{$period === old('settings.period', $settings['period'] ?? '') ? 'selected' : ''}}
                @endif
                >
                    {{$period}}
                </option>
            @endforeach
        </select>
    </div>

    @include('shared.forms.input', [
        'name' => 'settings[item_rate]',
        'label' => __('Item rate'),
        'value' => $settings['item_rate'] ?? '',
        'type' => 'number'
    ])
    @include('shared.forms.input', [
        'name' => 'settings[bin_rate]',
        'label' => __('Bin rate'),
        'value' => $settings['bin_rate'] ?? '',
        'type' => 'number'
    ])
    @include('shared.forms.input', [
        'name' => 'settings[volume_rate]',
        'label' => __('Volume rate'),
        'value' => $settings['volume_rate'] ?? '',
        'type' => 'number'
    ])
    <div class="form-group">
        <label class="form-control-label">{{ __('Volume unit') }}</label>
        <select name="{{'settings[volume_unit]'}}" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
            @foreach(\App\Models\BillingFee::VOLUME_UNITS as $unit)
                <option value="{{$unit}}"
                @if(isset($settings) || old('settings'))
                    {{$unit === old('settings.volume_unit', $settings['volume_unit'] ?? '') ? 'selected' : ''}}
                @endif
                >
                    {{$unit}}
                </option>
            @endforeach
        </select>
    </div>
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
    </script>
@endpush
