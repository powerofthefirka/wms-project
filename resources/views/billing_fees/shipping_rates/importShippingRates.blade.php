<div class="modal fade" id="import-shipping-rates" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <form action="{{ route( 'billing_fees.import', ['billing_profile' => $billingProfile, 'type' => $type ]) }}" method="POST" enctype="multipart/form-data" id="import-form">
                @csrf
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-default">{{ __('Import Shipping rates') }}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="file" name="file" class="form-control">
                </div>
                <div class="modal-footer">
                    <button id="submit-check" type="submit" class="btn btn-primary">{{ __('Import') }}</button>
                    <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">{{ __('Close') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#submit-check').on('click', function (e) {
                e.preventDefault();

                if (confirm('Be sure to backup existing settings (export)')) {
                    $('#import-form').submit()
                }
            })
        })
    </script>
@endpush
