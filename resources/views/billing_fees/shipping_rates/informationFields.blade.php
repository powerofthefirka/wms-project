@include('alerts.billingFeeFeedback')
@php
    $billingFee = empty($billingFee) ? new \App\Models\BillingFee() : $billingFee;
    $settings = empty($settings) ? [] : $settings;
@endphp
<h6 class="heading-small text-muted mb-4">{{ __('Fee information') }}</h6>
<div>
    @include('shared.forms.input', [
        'name' => 'name',
        'label' => __('Name'),
        'value' => $billingFee['name'] ?? ''
    ])

    @include('shared.forms.input', [
        'name' => 'settings[description]',
        'label' => __('Description'),
        'value' => $settings['description'] ?? ''
    ])

    <div class="form-group">
        <label class="form-control-label">{{__('Shipping Carrier')}}</label>
        <select class="shipping_carrier_select form-control form-control-sm" name="settings[shipping_carrier_id]">
            <option></option>
            @foreach($carriers as $carrier)
                <option @if (old('settings.shipping_carrier_id', $billingFee->getShippingCarrierFromSettings()->id ?? '') == $carrier->id) selected="selected" @endif value="{{ $carrier->id }}">{{ $carrier->name }}</option>
            @endforeach
        </select>
    </div>
    @php
        $allShippingMethods = !empty($billingFee->settings['shipping_carrier_id']) ? \App\Models\ShippingCarrier::find($billingFee->settings['shipping_carrier_id'])->shippingMethods : [];
        $selectedShippingMethods = $billingFee->getAllSelectedShippingMethods() ?? [];
    @endphp
    <select id="selected-methods-field" name="settings[shipping_method][]" class="d-none" multiple>
        @foreach($allShippingMethods as $shippingMethod)
            <option @if (!empty($selectedShippingMethods[$shippingMethod['id']])) selected="selected" @endif value="{{ $shippingMethod['id'] }}">{{ $shippingMethod['name'] }}</option>
        @endforeach
    </select>
    <div class="row pt-5 pb-5 method-list-selector">
        <div class="col-12 col-md-6 col-sm-6">
            <h4>{{ __('All Shipping Methods') }} <input id="methods-filter-input" class="methods-filter form-control form-control-sm ml-3 d-inline-block w-auto" placeholder="{{ __('Search..') }}" /></h4>
            <div id="all-methods-list" class="list-group">
                @foreach($allShippingMethods as $shippingMethod)
                    <a href="" class="all-methods-list-group-item list-group-item-action @if (!empty($selectedShippingMethods[$shippingMethod['id']])) d-none @endif" data-method_id="{{ $shippingMethod['id'] }}">{{ $shippingMethod['name'] }}</a>
                @endforeach
            </div>
        </div>
        <div class="col-12 col-md-6 col-sm-6">
            <h4>{{ __('Selected Shipping Methods') }}</h4>
            <div id="selected-methods-list" class="list-group">
                <a class="selected-methods-list-group-item empty-line list-group-item-action @if (!empty($selectedShippingMethods)) d-none @endif">{{ __('No shipping methods selected.') }}</a>
                @foreach($selectedShippingMethods ?? [] as $shippingMethod)
                    <a href="#" class="selected-methods-list-group-item list-group-item-action" data-method_id="{{ $shippingMethod->id }}">{{ $shippingMethod->name }}</a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="form-group">
        <input id="if_no_other_fee_applies"
               type="checkbox"
               name="{{'settings[if_no_other_fee_applies]'}}"
               value="1"
        @if(isset($settings) || old('settings'))
            {{(old('settings.if_no_other_fee_applies', $settings['if_no_other_fee_applies'] ?? false) ) ? 'checked' : ''}}
            @endif
        >
        <label class="form-check-label" for="if_no_other_fee_applies">
            {{__('If no other fee of this type applies…')}}
        </label>
    </div>
    <div class="all-shipping-zones">
        @foreach(Illuminate\Support\Arr::get($billingFee->settings, 'shipping_zones') ?? [] as $key => $shippingZone)
            @include('billing_fees.shipping_rates.shippingZone')
        @endforeach
        @if (empty(\Illuminate\Support\Arr::get($billingFee->settings, 'shipping_zones', [])))
            @include('billing_fees.shipping_rates.shippingZone', [
                'key' => 0
            ])
        @endif
    </div>
</div>
@push('js')
    <script>
        new BillingFeeCheckForDuplicateFees();
        new BillingFeeShippingRate();
    </script>
@endpush
