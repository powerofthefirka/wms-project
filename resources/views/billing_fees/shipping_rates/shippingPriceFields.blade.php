<div class="row shipping-price-row">
    <div class="col-3">
        @include('shared.forms.input', [
            'name' => 'settings[shipping_zones][' . $key . '][shipping_prices][' . $position . '][weight_from]',
            'label' => __('Weight From'),
            'value' => $shippingPrice['weight_from'] ?? 0,
            'type' => 'number',
            'step' => '1',
            'min' => 'min=0'
        ])
    </div>
    <div class="col-3">
        @include('shared.forms.input', [
            'name' => 'settings[shipping_zones][' . $key . '][shipping_prices][' . $position . '][weight_to]',
            'label' => __('Weight To'),
            'value' => $shippingPrice['weight_to'] ?? 0,
            'type' => 'number',
            'step' => '1',
            'min' => 'min=0'
        ])
    </div>
    <div class="col-3">
        @include('shared.forms.input', [
            'name' => 'settings[shipping_zones][' . $key . '][shipping_prices][' . $position . '][price]',
            'label' => __('Price'),
            'value' => $shippingPrice['price'] ?? ''
        ])
    </div>
    <div class="col-3">
            <label class="form-control-label" for="input-settings[shipping_prices][weight_unit]">{{ __('Weight Unit') }}</label>
        <div class="d-flex flex-row">
            <select class="form-control form-control-sm " name="settings[shipping_zones][{{$key}}][shipping_prices][{{ $position }}][weight_unit]">
                @foreach(\App\Models\BillingFee::WEIGHT_UNITS as $unit )
                    <option @if ( $unit == ($shippingPrice['weight_unit'] ?? '') ) selected="selected" @endif value="{{ $unit }}">{{ $unit }}</option>
                @endforeach
            </select>
            <a class="btn btn-primary btn-sm text-white remove-shipping-price-item remove-item-button ml-2">×</a>
        </div>
    </div>
    <hr class="mt-1 w-100" />
</div>
