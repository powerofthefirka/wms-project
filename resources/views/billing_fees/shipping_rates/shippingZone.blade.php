<div class="shadow-lg p-3 mb-5 bg-white rounded shipping-zone">
    <div class="form-group">
        <div class="row">
            <label class="col-6 col-md-6 col-sm-6 form-control-label">Shipping Zone Name</label>
            <div class="col-6 text-right"><a class="btn btn-primary btn-sm text-white remove-shipping-zone remove-item-button ml-2">×</a></div>
        </div>
        <div class="row">
            <div class="col-12"><input type="text" name="settings[shipping_zones][{{$key}}][name]" value="@if(isset($shippingZone['name'])){{$shippingZone['name']}}@else{{'Zone - ' . ($key+1)}}@endif" class="form-control form-control-sm shipping-zone-name"></div>
        </div>
    </div>
    @php
        $allCountries = \App\Models\Country::orderBy('title', 'asc')->get();
        $allSelectedCountries = $billingFee->getAllSelectedCountries() ?? [];
        $selectedCountries = $billingFee->getCountries($shippingZone['countries'] ?? []) ?? [];
    @endphp
    <select name="settings[shipping_zones][{{$key}}][countries][]" class="d-none selected-countries-field" multiple>
        @foreach($allCountries as $country)
            <option @if (!empty($selectedCountries[$country->id])) selected="selected" @endif value="{{ $country->id }}">{{ $country->title }}</option>
        @endforeach
    </select>
    <div class="row pt-5 pb-5 country-list-selector">
        <div class="col-12 col-md-6 col-sm-6">
            <h4>{{ __('All Countries') }} <input class="countries-filter countries-filter-input form-control form-control-sm ml-3 d-inline-block w-auto" placeholder="{{ __('Search..') }}" /></h4>
            <div class="list-group all-countries-list">
                @foreach($allCountries as $country)
                    <a href="" class="list-group-item list-group-item-action all-countries-country-id-{{$country->id}} @if (!empty($allSelectedCountries[$country->id])) d-none @endif" data-country_id="{{ $country->id }}">{{ $country->title }}</a>
                @endforeach
            </div>
        </div>
        <div class="col-12 col-md-6 col-sm-6">
            <h4>{{ __('Selected Countries') }}</h4>
            <div class="list-group selected-countries-list">
                <a class="list-group-item empty-line list-group-item-action @if (!empty($selectedCountries)) d-none @endif">{{ __('No countries selected.') }}</a>
                @foreach($selectedCountries ?? [] as $country)
                    <a href="#" class="list-group-item list-group-item-action" data-country_id="{{ $country->id }}">{{ $country->title }}</a>
                @endforeach
            </div>
        </div>
    </div>

    @foreach(\Illuminate\Support\Arr::get($shippingZone ?? [], 'shipping_prices', []) as $position => $shippingPrice)
        @include('billing_fees.shipping_rates.shippingPriceFields', [
            'position' => $position,
            'shippingPrice' => $shippingPrice
        ])
    @endforeach
    @if (empty(\Illuminate\Support\Arr::get($shippingZone ?? [], 'shipping_prices', [])))
        @include('billing_fees.shipping_rates.shippingPriceFields', [
            'position' => 0,
            'shippingPrice' => []
        ])
    @endif

    <div class="text-right">
        <a class="btn btn-secondary add-shipping-price-row btn-sm">{{ __('Add Shipping Price') }}</a>
    </div>

</div>