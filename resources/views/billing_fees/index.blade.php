@extends('layouts.app', ['title' => __('Product profiles')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                </div>
            </div>
            <div class="col col-12">
                @include('alerts.ajax_success')
                @include('alerts.ajax_error')
            </div>

            <div class="col col-12">
                <div class="card">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="billing-fees-table" style="width: 100% !important;">
                            <thead class="thead-light"></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        new BillingFee();
    </script>
@endpush
