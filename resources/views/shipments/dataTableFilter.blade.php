<div class="col-12">
    <a class="btn btn-sm btn-primary btn-block text-white mb-4 d-block d-sm-none toggle-filter-button">
        <span>{{ __('Show filter') }}</span>
        <span>{{ __('Close filter') }}</span>
    </a>
</div>
<div class="col-12 d-none d-sm-block filter">
    <div class="row align-items-center">
        <input type="hidden" id="table-id" value="shipment-table">
        <div class="form-group col-12 col-sm-4">
            <input type="text" name="table_search"
                   class=" form-control form-control-sm table_filter" placeholder="{{ __('Search shipments') }}">
        </div>
        <div class="form-group col-12 col-sm-4">
            <input
                type="text"
                name="dates_between"
                class="table-datetimepicker table_filter form-control form-control-sm"
                placeholder="Dates between"
                value=""
            >
        </div>
        <div class="form-group col-12 col-sm-4">
            <select class="form-control form-control-sm ordering">
                <option value="">
                    {{ __('Sort') }}
                </option>
                @foreach(\App\Models\Shipment::FILTERABLE_COLUMNS as $item)
                    <option value="{{ $item['column'] . ',' . $item['dir']}}">{{$item['title']}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
@push('table-addons-js')
    <script>
        new DataTableAddons();
    </script>
@endpush
