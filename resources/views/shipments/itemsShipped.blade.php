@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Shipped Items') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('shipments.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-xl-12">
                                <h4>{{__('Shipping Information')}}</h4>
                                @if(!$shipment->contactInformation->country)
                                    <span class="info text-md-center"><b>Missing country in contact information</b></span>
                                    <br>
                                @endif
                                <span class="h6 surtitle">{{__('Order Number:')}}</span>
                                <span class="info text-md-center">{{$shipment->order->number}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Customer Name:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->name}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Address:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->address}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Address 2:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->address2}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('E-mail:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->email}}</span>
                                <br>
                                @if($shipment->contactInformation->country)
                                    <span class="h6 surtitle">{{__('Country:')}}</span>
                                    <span class="info text-md-center">{{$shipment->contactInformation->country->title}}</span>
                                    <br>
                                @endif
                                <span class="h6 surtitle">{{__('City:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->city}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Phone:')}}</span>
                                <span class="info text-md-center">{{$shipment->contactInformation->phone}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable-basic">
                                <thead class="">
                                <tr>
                                    <th scope="col">{{ __('Product SKU') }}</th>
                                    <th scope="col">{{ __('Product Name') }}</th>
                                    <th scope="col">{{ __('Quantity Shipped') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($shipment->shipmentItems as $shipmentItem)
                                    <tr>
                                        <td>{{ $shipmentItem->product->sku }}</td>
                                        <td>{{ $shipmentItem->product->name }}</td>
                                        <td>{{ $shipmentItem->quantity }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ __('Package Information') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0">
                        @if ($shipment->shippingBoxes->count())
                            @foreach($shipment->shippingBoxes as $item)
                                <ul class="list-group mb-3">
                                    <li class="list-group-item">
                                        {{__('Name')}}: <strong>{{$item->shippingBox->name}}</strong><br>
                                        {{__('Shipping Carrier')}}: <strong>{{$item->shippingCarrier->name ?? ''}}</strong><br>
                                        {{__('Shipping Method')}}: <strong>{{$item->shippingMethod->name ?? ''}}</strong><br>
                                        {{__('Price')}}: <strong>{{$item->price}}</strong><br>
                                        {{__('Tracking Number')}}:
                                            <strong>
                                                @if(!empty($item->tracking_url))
                                                    <a href="{{ url($item->tracking_url)}}" target="_blank">{{$item->tracking_number}}</a>
                                                @else
                                                    {{$item->tracking_number}}
                                                @endif
                                            </strong><br>
                                        {{__('Status')}}: <strong>{{$item->status}}</strong><br>
                                        @if ($item->packageItems->count())
                                        <h4 class="mt-3 mb-2">{{__('Items')}}</h4>
                                        <div class="table-responsive">
                                            <div>
                                                <table class="table align-items-center">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Quantity Shipped</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="list">
                                                        @foreach($item->packageItems as $packageItem)
                                                        <tr>
                                                            <td>
                                                                <div class="flex-container">
                                                                    <div><strong>Name:</strong> {{ $packageItem->orderItem->name }}</div>
                                                                    <div><strong>SKU:</strong> {{ $packageItem->orderItem->sku }}</div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                {{ $packageItem->quantity }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @endif
                                    </li>
                                </ul>
                            @endforeach
                        @else
                            <p class="text-center">No package found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

