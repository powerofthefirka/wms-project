@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                    @include('alerts.ajax_error')
                </div>
            </div>
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            @include('shipments.dataTableFilter')
                            <div class="col-4 d-flex align-items-center">
                                <h3 class="mb-0 primary-color">{{ __('Shipments') }}</h3>
                            </div>
                            <div class="col-4 d-flex align-items-center justify-content-center">
                                @if (!empty(session('customer_id')))
                                    @include('shared.modals.dataExportModal', ['model' => 'shipment', 'data' => \App\Models\Shipment::EXPORTABLE_COLUMNS])
                                @endif
                            </div>
                            <div class="col-4 d-flex align-items-center justify-content-end">
                                @include('shared.modals.showHideTableColumns')
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="shipment-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ShipOrder();
    </script>
@endpush
