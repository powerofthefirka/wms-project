@extends('layouts.app', ['title' => __('3PL Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')

    <div class="container-fluid">
        <form method="post" action="{{ route('three_pls.update', ['threePl' => $threePl, 'id' => $threePl->id]) }}" autocomplete="off">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Edit 3PL') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="nav-wrapper pt-0">
                                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                    <li class="nav-item">
                                        <div class="nav-link mb-sm-3 mb-md-0 active  " id="tabs-icons-text-1-tab"
                                           aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>3PL</div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_users', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Users</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_pricing_plan', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Pricing Plan</a>
                                    </li>
                                    @if($threePl->pricingPlan)
                                        <li class="nav-item">
                                            <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.billing_report', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-collection mr-2"></i>Billing Report</a>
                                        </li>
                                    @endif
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('domains.index', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Domain & Theme Settings</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <br>
                            <h6 class="heading-small text-muted mb-4">{{ __('3PL information') }}</h6>
                            @include('3pls.3plInformationFields', [
                                'threePl' => $threePl
                            ])
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('three_pls.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ThreePlForm();
    </script>
@endpush