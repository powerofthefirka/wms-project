@include('shared.forms.contactInformationFields', [
   'name' => 'contact_information',
   'contactInformation' => $threePl->contactInformation ?? ''
])
@include('shared.forms.checkbox', [
    'name' => 'custom_shipping_modal',
    'label' => __('Custom Customer Shipping Modal'),
    'value' => $threePl->custom_shipping_modal ?? '',
    'checked' => isset($threePl) && $threePl->custom_shipping_modal ? 'true' : ''
])<br>
<div class="custom-shipping-modal-text-div">
   @include('shared.forms.input', [
      'name' => 'custom_shipping_modal_text',
      'label' => __('Custom Customer Shipping Modal Text'),
      'value' => $threePl->custom_shipping_modal_text ?? ''
   ])
</div>
@include('shared.forms.input', [
      'name' => 'scheduling_link',
      'label' => __('Scheduling link'),
      'value' => $threePl->scheduling_link ?? ''
   ])
