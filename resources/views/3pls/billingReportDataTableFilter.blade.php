<div class="col-12">
    <a class="btn btn-sm btn-primary btn-block text-white mb-4 d-block d-sm-none toggle-filter-button">
        <span>{{ __('Show filter') }}</span>
        <span>{{ __('Close filter') }}</span>
    </a>
</div>
<div class="col-12 d-none d-sm-block filter">
    <div class="card">
        <div class="card-body pt-0 pb-0">
            <div class="row">
                <input type="hidden" id="table-id" value="billing-report-table">
                <div class="form-group col-12 calendar-input">
                    <input
                        type="text"
                        name="dates_between"
                        class="table-datetimepicker table_filter form-control"
                        placeholder="Dates between"
                        value="{{ $threePl->pricingPlan->invoice_start_date->format('Y-m-d') }} - {{ $threePl->pricingPlan->invoice_start_date->addMonth()->format('Y-m-d') }}"
                    >
                </div>
            </div>
        </div>
    </div>
</div>
@push('table-addons-js')
    <script>
        new BillingReportDataTableAddons();
    </script>
@endpush
