@extends('layouts.app', ['title' => __('3PL Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')

    <div class="container-fluid">
        <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
            <div class="card transparent">
                @include('alerts.success')
                @include('alerts.errors')
                @include('alerts.ajax_error')
            </div>
        </div>

        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit 3PL') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-card">
                        <div class="nav-wrapper pt-0">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-1-tab"
                                        aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('three_pls.edit', [ 'threePl' => $threePl ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('3PL') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_users', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Users</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_pricing_plan', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Pricing Plan</a>
                                </li>
                                <li class="nav-item">
                                    <div class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-collection mr-2"></i>Billing Report</div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('domains.index', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Domain & Theme Settings</a>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col col-12 col-lg-9">
                                <br>
                                <div class="row">
                                    <h6 class="heading-small text-muted mb-4 col-8">{{ __('Billing information') }}</h6>
                                    <div class="col-4 text-center">
                                        <form action="{{route('three_pls.export_billing_report_to_csv', ['threePl' => $threePl])}}" method="get" style="display: inline-block">
                                            @csrf
                                            <input type="hidden" id="export-dates-between" name="export_dates_between" value="">
                                            <button type="submit" class="btn btn-secondary btn-sm">
                                                {{ __('Export Report') }}
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <input type="hidden" id="three-pl-id" name="3pl_id" value="{{$threePl->id}}">
                                @include('3pls.billingReportDataTableFilter')
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center table-hover col-12 p-0" id="billing-report-table" style="width: 100% !important;">
                                        <thead class=""></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="col col-12">
                                    <br>
                                    <div class="row">
                                        <span class="h5 surtitle text-md-right col-10">{{__('Total Number of Orders:')}}</span>
                                        <span class="info text-md-center col-2" id="total-number-of-orders">{{$totalOrdersTotalPrice['totalNumberOfOrders'] ?? ''}}</span>
                                    </div>
                                    <div class="row">
                                        <span class="h5 surtitle text-md-right col-10">{{__('Total:')}}</span>
                                        <span class="info text-md-center col-2" id="total-order-price">{{$totalOrdersTotalPrice['totalOrderPrice'] ?? ''}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-12 col-lg-3">
                                <br>
                                <h6 class="heading-small text-muted mb-4">{{ __('Pricing Plan Informationn') }}</h6>

                                <span class="h6 surtitle">{{__('Pricing Plan:')}}</span>
                                <span class="info text-md-center">{{ucfirst($threePl->pricingPlan->pricing_plan_name) ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Billing Period:')}}</span>
                                <span class="info text-md-center">{{ucfirst($threePl->pricingPlan->billing_period) ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Monthly Price:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->monthly_price ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Initial Fee:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->initial_fee ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('3PL Billing Fee:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->three_pl_billing_fee ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Number of Monthly Orders:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->number_of_monthly_orders ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Price Per Additional Order:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->price_per_additional_order ?? ''}}</span>
                                <br>
                                <span class="h6 surtitle">{{__('Invoice start date:')}}</span>
                                <span class="info text-md-center">{{$threePl->pricingPlan->invoice_start_date->format(get_date_format()) ?? ''}}</span>
                                <br>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ThreePlForm();
    </script>
@endpush
