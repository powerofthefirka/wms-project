@extends('layouts.app', ['title' => __('3PL Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')

    <div class="container-fluid ">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit 3PL') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="nav-wrapper pt-0">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                         aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('three_pls.edit', [ 'threePl' => $threePl ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('3PL') }}</a>
                                </li>
                                <li class="nav-item">
                                    <div class="nav-link mb-sm-3 mb-md-0 active  " id="tabs-icons-text-2-tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Users') }}</div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.edit_pricing_plan', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Pricing Plan</a>
                                </li>
                                @if($threePl->pricingPlan)
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('three_pls.billing_report', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-collection mr-2"></i>Billing Report</a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('domains.index', [ 'threePl' => $threePl ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-money-coins mr-2"></i>Domain & Theme Settings</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                        <br>
                        <div class="row">
                            <h6 class="heading-small text-muted mb-4 col-8">{{ __('Assigned users') }}</h6>
                        </div>
                        <div class="table-responsive p-0">
                            <form id="three-pl-user-form" action="{{route('three_pls.update_users', ['threePl' => $threePl])}}" method="post" >@csrf</form>
                            <table class="table align-items-center table-flush datatable-basic">
                                <thead class="">
                                <tr>
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Email') }}</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($threePl->users as $key => $user)
                                    <tr>
                                        <td>{{ $user->contactInformation->name ?? '' }}</td>
                                        <td>
                                            <input form="three-pl-user-form" type="hidden" name="three_pl_user[{{ $key }}][user_id]" value="{{ $user->id }}" />
                                            {{ $user->email }}
                                        </td>
                                        <td class="text-right">
                                            <form action="{{ route('three_pls.detach_user', ['threePl' => $threePl, 'user' => $user]) }}" method="post" style="display: inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="button" class="btn btn-danger" data-confirm-action="{{ __('Are you sure you want to delete this user?') }}">
                                                    {{ __('Delete') }}
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @csrf
                        @include('shared.forms.ajaxSelect', [
                            'url' => route('three_pls.filter_users', [ 'threePl' => $threePl]),
                            'name' => 'new_user_id',
                            'className' => 'ajax-user-input',
                            'placeholder' => 'Search for a user to add',
                            'label' => 'User search',
                            'form' => "three-pl-user-form"
                        ])
                    </div>
                </div>
            </div>
            <div class="col col-12">
                <div class="card transparent text-right">
                    <a href="{{ route('three_pls.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-primary" form="three-pl-user-form" >{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

@endsection

