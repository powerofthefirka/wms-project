<li>
    <ul>
        <li class="nav-item">
            <a href="{{ route('three_pls.index') }}" class="nav-link {{ current_page(route('three_pls.index'), 'three_pls.index') }}">
                <span class="nav-link-text">{{ __('3PLs') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('three_pls.create') }}" class="nav-link {{ current_page(route('three_pls.create'), 'three_pls.create') }}">
                <span class="nav-link-text">{{ __('Create 3PL') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('pricing_plans.index') }}" class="nav-link {{ current_page(route('pricing_plans.index'), 'pricing_plans.index') }}">
                <span class="nav-link-text">{{ __('3PL Pricing Plans') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('currencies.index') }}" class="nav-link {{ current_page(route('currencies.index'), 'currencies.index') }}">
                <span class="nav-link-text">{{ __('Currencies') }}</span>
            </a>
        </li>
    </ul>
</li>
