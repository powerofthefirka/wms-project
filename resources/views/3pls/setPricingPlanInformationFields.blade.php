<div class="form-group">
    <label class="form-control-label">{{ __('Pricing Plan') }}</label>
    <select name="pricing_plan_name" id="pricing-plan-name" class="form-control" data-toggle="select" data-placeholder="">
        <option value=""></option>
        @foreach($pricingPlans as $key => $plan)
            <option value="{{$plan->name}}"
            @if(isset($threePlPricingPlan))
                {{$plan->name === $threePlPricingPlan->pricing_plan_name ? 'selected' : ''}}
            @endif
            >
                {{$plan->name}}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label class="form-control-label">{{ __('Billing Period') }}</label>
    <select name="billing_period" id="billing-period" class="form-control" data-toggle="select" data-placeholder="">
        <option value=""></option>
    </select>
</div>
@include('shared.forms.input', [
    'name' => 'monthly_price',
    'label' => __('Monthly Price'),
    'value' => $threePlPricingPlan->monthly_price ?? '',
])
@include('shared.forms.input', [
    'name' => 'initial_fee',
    'label' => __('Initial Fee'),
    'value' => $threePlPricingPlan->initial_fee ?? '',
])
@include('shared.forms.input', [
    'name' => 'three_pl_billing_fee',
    'label' => __('3PL Billing Fee'),
    'value' => $threePlPricingPlan->three_pl_billing_fee ?? '',
])
@include('shared.forms.input', [
    'name' => 'number_of_monthly_orders',
    'label' => __('Number of Monthly Orders'),
    'value' => $threePlPricingPlan->number_of_monthly_orders ?? '',
])
@include('shared.forms.input', [
    'name' => 'price_per_additional_order',
    'label' => __('Price Per Additional Order'),
    'value' => $threePlPricingPlan->price_per_additional_order ?? '',
])
@include('shared.forms.input', [
    'name' => 'invoice_start_date',
    'label' => __('Invoice start date'),
    'value' => ( empty($threePlPricingPlan->invoice_start_date) ? '' : $threePlPricingPlan->invoice_start_date->format(get_date_format()) ?? '' ),
])
