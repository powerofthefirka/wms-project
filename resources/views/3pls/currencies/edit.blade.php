@extends('layouts.app', ['title' => __('Edit Currency')])

@section('content')

    <div class="container-fluid">
        <form method="post" action="{{ route('currencies.update', [ 'currency' => $currency ]) }}"
              enctype="multipart/form-data" autocomplete="off">
            @csrf
            <div class="row">
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Edit Currency') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    @include('shared.forms.input', [
                                        'name' => 'name',
                                        'label' => __('Currency name'),
                                        'type' => 'text',
                                        'value' => $currency->name ?? ''
                                    ])
                                </div>
                                <div class="col-md-6">
                                    @include('shared.forms.input', [
                                        'name' => 'code',
                                        'label' => __('Currency code'),
                                        'type' => 'text',
                                        'value' => $currency->code ?? ''
                                    ])
                                </div>
                                <div class="col col-12">
                                    <div class="card transparent text-right">
                                        <a href="{{ route('currencies.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
