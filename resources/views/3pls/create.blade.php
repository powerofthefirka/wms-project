@extends('layouts.app', ['title' => __('3PL Management'), 'icon' => '<i class="ci ci-receiving"></i>'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('three_pls.store') }}" autocomplete="off">
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Add 3PL') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="heading-small text-muted mb-4">{{ __('3PL information') }}</h6>
                            @include('3pls.3plInformationFields')
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('three_pls.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new ThreePlForm();
    </script>
@endpush