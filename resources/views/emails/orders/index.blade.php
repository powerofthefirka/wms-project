@component('mail::message')
Dear {{$name}},

Order <strong>{{$order_number}}</strong> status changed to: <strong>{{$status}}</strong>.

@component('mail::button', ['url' => URL::to('/orders').'/'.$order_id.'/edit'])
View order
@endcomponent
{{ config('app.name') }}
@endcomponent
