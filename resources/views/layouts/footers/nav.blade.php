<div class="row align-items-center justify-content-lg-between">
    <div class="col-xl-12">
        <ul class="nav nav-footer">
            <li class="nav-item">
                <a href="#" class="nav-link text-white" target="_blank">Terms & Privacy</a>
            </li>
            <li class="nav-item">
                <span class="nav-link text-white" target="_blank">&copy; {{ now()->year }} {{ config('app.name') }}</span>
            </li>
        </ul>
    </div>
</div>
