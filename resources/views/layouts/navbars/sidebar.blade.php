<nav class="sidebar">
    <a class="logo" href="{{ route('home') }}">
        <img src="{{ sidebar_logo() }}">
    </a>
    <ul class="navbar-nav">
        <div class="">
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('home'), 'home') }}" href="{{ route('home') }}">
                    <i class="automagical-dashboard"></i>
                    <span class="nav-link-text">{{ __('Dashboard') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('orders.index'), 'orders') }}" href="{{ route('orders.index') }}" data-submenu="orders.menu">
                    <i class="automagical-orders"></i>
                    <span class="nav-link-text">{{ __('Orders') }}</span>
                </a>
                @include('orders.menu')
            </li>
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('products.index'), 'products') }}" href="{{ route('products.index') }}" data-submenu="products.menu">
                    <i class="automagical-products"></i>
                    <span class="nav-link-text">{{ __('Inventory') }}</span>
                </a>
                @include('products.menu'/*, ['class' => $submenu =='products.menu'?'current':'']*/)
            </li>
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('returns.index'), 'returns') }}" href="{{ route('returns.index') }}" data-submenu="returns.menu">
                    <i class="automagical-returns"></i>
                    <span class="nav-link-text">{{ __('Returns') }}</span>
                </a>
                @include('returns.menu'/*, ['class' => $submenu =='returns.menu'?'current':'']*/)
            </li>
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('purchase_orders.index'), 'purchase_orders') }}" href="{{ route('purchase_orders.index') }}" data-submenu="purchase_orders.menu">
                    <i class="automagical-bag"></i>
                    <span class="nav-link-text">{{ __('Purchase Orders') }}</span>
                </a>
                @include('purchase_orders.menu'/*, ['class' => $submenu =='purchase_orders.menu'?'current':'']*/)
            </li>

            @if(auth()->user()->isAdmin() || auth()->user()->isCustomerAdmin())
                <li class="nav-item">
                    <a class="nav-link {{ current_page(route('billings.index'), 'billings') }}" href="{{ route('billings.index') }}" data-submenu="billings.menu">
                        <i class="automagical-dashboard"></i>
                        <span class="nav-link-text">{{ __('Billing') }}</span>
                    </a>
                    @include('billings.menu')
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ current_page(route('warehouses.index'), 'warehouses') }} {{ current_page(route('locations.index'), 'locations') }}" href="{{ route('warehouses.index') }}"  data-submenu="warehouses.menu">
                        <i class="automagical-location"></i>
                        <span class="nav-link-text">{{ __('Warehouses') }}</span>
                    </a>
                    @include('warehouses.menu')
                </li>
            @endif
        </div>
        <div class="">
            @if(auth()->user()->isAdmin())
                <li class="nav-item {{ current_page(route('customers.index'), 'customers') }}">
                    <a href="{{ route('customers.index') }}" class="nav-link d-none" data-submenu="customers.menu">
                        <i class="nc-icon nc-single-01"></i>
                        <span class="nav-link-text">{{ __('Customers') }}</span>
                    </a>
                </li>
            @endif
            @include('shared.dropdowns.globalCustomerSelect')
            @if(auth()->user()->customers->first() && !auth()->user()->isCustomerAdmin() && auth()->user()->customers->first()->contactInformation->shared_folder
                    || (session('customer_id') && app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->shared_folder))
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Shared folder">
                    <a target="_blank" href="{{session('customer_id')
                                                    ? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->shared_folder
                                                    : auth()->user()->contactInformation['shared_folder']}}" class="nav-link">
                        <i class="nc-icon nc-cloud-26"></i>
                        <span class="nav-link-text">{{ __('Shared folder') }}</span>
                    </a>
                </li>
            @endif
                @if(auth()->user()->customers->first() && !auth()->user()->isCustomerAdmin() && auth()->user()->customers->first()->threePl->scheduling_link
                    || (session('customer_id') && app()->user->getCustomers()->where('id', session('customer_id'))->first()->threePl->scheduling_link))
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Scheduling link">
                        <a target="_blank" href="{{session('customer_id')
                                                    ? app()->user->getCustomers()->where('id', session('customer_id'))->first()->threePl->scheduling_link
                                                    : auth()->user()->customers->first()->threePl->scheduling_link}}" class="nav-link">
                            <i class="nc-icon nc-link-72"></i>
                            <span class="nav-link-text">{{ __('Scheduling link') }}</span>
                        </a>
                    </li>
                @endif
            @if(auth()->user()->isAdmin())
                <li class="nav-item">
                    <a class="nav-link {{ current_page(route('three_pls.index'), 'three_pls.index') }} {{ current_page(route('three_pls.create'), 'three_pls.create') }}" href="{{ route('three_pls.index') }}">
                        <i class="automagical-settings"></i>
                        <span class="nav-link-text">{{ __('Admin Settings') }}</span>
                    </a>
                    <ul class="submenu">
                        @include('3pls.menu')
                    </ul>
                </li>
            @endif
            @if (auth()->user()->isAdmin() || auth()->user()->isCustomerAdmin())
                <li class="nav-item">
                    <a href="{{ route('customers.index') }}" class="nav-link {{ current_page(route('customers.index'), 'customers') }}">
                        <i class="automagical-profile"></i>
                        <span class="nav-link-text">{{ __('Customers') }}</span>
                    </a>
                    <ul class="submenu customer-submenu">
                        <li>
                            <ul>
                                <li class="nav-item">
                                    <a href="{{ route('customers.create') }}" class="nav-link {{ current_page(route('customers.create'), 'customers.create') }}">
                                        <span class="nav-link-text">{{ __('Create Customer') }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('users.index') }}" class="nav-link {{ current_page(route('users.index'), 'users.index') }}">
                                        <span class="nav-link-text">{{ __('Users') }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('users.create') }}" class="nav-link {{ current_page(route('users.create'), 'users.create') }}">
                                        <span class="nav-link-text">{{ __('Create User') }}</span>
                                    </a>
                                </li>
                                @if (!auth()->user()->isAdmin() && auth()->user()->isCustomerAdmin())
                                    <li class="nav-item">
                                        <a href="{{ route('customers.settings') }}" class="nav-link {{ current_page(route('customers.settings'), 'customers.settings') }}">
                                            <span class="nav-link-text">{{ __('Settings') }}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {{ current_page(route('profiles.edit'), 'profiles') }}" href="{{ route('profiles.edit') }}">
                    <i class="automagical-profile"></i>
                    <span class="nav-link-text">{{ __('Profile') }}</span>
                </a>
            </li>
            @if(auth()->user())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="automagical-logout"></i>
                        <span class="nav-link-text">{{ __('Logout') }}</span>
                    </a>
                </li>
            @endif
        </div>
    </ul>
</nav>
<div class="sidebar-overlay"></div>
