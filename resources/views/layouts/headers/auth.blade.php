<header class="header">
    <div class="row m-0">
        <a class="toggle-sidebar-button @if(Cookie::get('sidebar-expanded') == 'true') active @endif">
            <i></i>
            <i></i>
            <i></i>
        </a>
        <a class="logo" href="{{ route('home') }}">
            <img src="{{ sidebar_logo() }}">
        </a>
        @include('shared.global_search.globalSearch')
        <div class="col p-0 text-right">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link show-global-search-button d-flex align-items-center" href="#" role="button">
                        <i class="nc-icon nc-zoom-2 mt-1"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>
