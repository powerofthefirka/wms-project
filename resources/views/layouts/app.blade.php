<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ site_title() }}</title>
        <!-- Favicon -->
        <link href="{{ site_favicon() }}" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/fonts/nucleo/style.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/fonts/iconly/style.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/fonts/dripicons/webfont/webfont.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/fonts/automagical/style.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/datepicker/daterangepicker.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/dropzone/dist/min/dropzone.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" href="{{ asset('argon') }}/vendor/sweetalert2/dist/sweetalert2.min.css">

        @stack('css')

        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
        <link type="text/css" href="{{ mix('css/app.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet">

        @include('shared.style')

        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
    </head>
    <body class="{{ $class ?? '' }} @if(Cookie::get('sidebar-expanded') == 'true') sidebar-expanded @endif" data-current-customer="{{ session('customer_id') }}">
        <div class="wrapper">
            @auth()
                @include('layouts.headers.auth')
            @endauth

            @auth()
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @include('layouts.navbars.sidebar')
            @endauth

            <div class="main-content">
                @yield('content')
            </div>

            @auth()
                @include('shared.preview')
            @endauth

            @if(auth()->check())
                @include('shared.modals.confirm')
                @include('shared.modals.editablePreview')
                @include('shared.modals.import_product')
                @include('shared.modals.import_order')
                @include('shared.modals.import_purchase_order')
            @endif
        </div>

        <!-- Global Variables -->
        <script>
            let DEFAULT_DATE_RANGE = {{env('DEFAULT_DATE_RANGE')}};
            let DEFAULT_DASHBOARD_DATE_RANGE = {{env('DEFAULT_DASHBOARD_DATE_RANGE')}};
        </script>
        <script src="https://unpkg.com/@googlemaps/markerwithlabel/dist/index.min.js"></script>
        <!-- Argon JS -->
        <script src="{{ mix('/js/app.js') }}"></script>


        {{--<script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>--}}
        <script src="{{ asset('argon') }}/vendor/js-cookie/js.cookie.js"></script>
        <script src="{{ asset('argon') }}/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/lavalamp/js/jquery.lavalamp.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/jscolor/jscolor.js"></script>

        <!-- Optional JS -->
        <script src="{{ asset('argon') }}/vendor/moment/moment.js"></script>
        <script src="{{ asset('argon') }}/vendor/datepicker/daterangepicker.js"></script>
        <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/dropzone/dist/min/dropzone.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/moment/min/moment.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>
        <script src="{{ asset('argon') }}/js/demo.min.js"></script>
        <script src="{{ asset('argon') }}/js/custom.js"></script>

        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
        <script src="{{ asset('js') }}/dataTableMultiFilter.js"></script>


        @stack('js')
        @stack('table-addons-js')
        @stack('widget-js')

        {!! messaging_script() !!}
    </body>
</html>
