<div
    class="ajax-error alert alert-danger fade show m-0 {{ $class ?? '' }}"
    role="alert"
    style="display: none"
>
    {{ __('Error')}}
</div>
