<div
    class="editablePreviewAlert alert alert-success fade show {{ $class ?? '' }}"
    role="alert"
    style="display: none"
>
    {{ __('Success')}}
</div>
