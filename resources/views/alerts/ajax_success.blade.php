<div
    class="ajax-success alert alert-success fade show m-0 {{ $class ?? '' }}"
    role="alert"
    style="display: none"
>
    {{ __('Success')}}
</div>
