@include('shared.forms.ajaxSelect', [
    'url' => route('invoice_statuses.filter_customers'),
    'name' => 'customer_id',
    'className' => 'ajax-user-input',
    'placeholder' => __('Search'),
    'label' => __('Customer'),
    'default' => [
                'id' => $invoiceStatus->customer->id ?? session('sessionCustomer')['id'] ?? old('customer_id'),
                'text' => $invoiceStatus->customer->contactInformation->name ?? session('sessionCustomer')['name'] ?? ''
            ]
])
@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $invoiceStatus->name ?? ''
])
