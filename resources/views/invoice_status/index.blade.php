@extends('layouts.app', ['title' => __('Invoice Statuses'), 'submenu' => 'billings.old.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="col col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            <h3 class="mb-0">{{ __('Invoices') }}</h3>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{ route('invoice_statuses.create') }}" class="btn btn-secondary btn-sm">{{ __('Add invoice status') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @include('alerts.success', ['class' => 'mt-2 mb-1'])
                    @include('alerts.errors', ['class' => 'mt-2 mb-1'])
                </div>

                <div class="table-responsive p-0">
                    <table id="invoice-status-table" class="table align-items-center table-hover col-12 p-0" style="width: 100% !important;">
                        <thead class="">
                        <tr>
                            <th scope="col">{{ __('Name') }}</th>
                            <th scope="col">{{ __('Customer') }}</th>
                            <th scope="col" class="text-center">{{ __('Actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($invoiceStatuses as $invoiceStatus)
                            <tr>
                                <td>{{ $invoiceStatus->name }}</td>
                                <td>
                                    <a href="/customer/{{ $invoiceStatus->customer->id }}/edit">{{ $invoiceStatus->customer->contactInformation['name'] }}</a>
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('invoice_statuses.edit', [ 'invoiceStatus' => $invoiceStatus ]) }}" class="btn btn-primary edit">{{ __('Edit') }}</a>
                                    <form action="{{ route('invoice_statuses.destroy', ['invoiceStatus' => $invoiceStatus, 'id' => $invoiceStatus->id]) }}" method="post" style="display: inline-block">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class="btn btn-danger" data-confirm-action="{{ __('Are you sure you want to delete this order status?') }}">
                                            {{ __('Delete') }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new InvoiceStatus();
    </script>
@endpush

