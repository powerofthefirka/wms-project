@extends('layouts.app', ['title' => __('Invoice status Management')])

@section('content')

    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Invoice Status') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('invoice_statuses.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card shadow">
                            <div class="col-12 mt-2">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('invoice_statuses.update', ['invoiceStatus' => $invoiceStatus, 'id' => $invoiceStatus->id]) }}" autocomplete="off">
                                    @csrf
                                    <h6 class="heading-small text-muted mb-4">{{ __('Invoice status information') }}</h6>
                                    <div class="pl-lg-4">
                                        {{ method_field('PUT') }}
                                        @include('invoice_status.orderStatusInformationFields', [
                                            'invoiceStatus' => $invoiceStatus
                                        ])
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success ">{{ __('Save') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
