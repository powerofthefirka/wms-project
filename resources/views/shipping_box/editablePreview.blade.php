<h3 class="mb-3">{{__('Quick Edit Shipping Box')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('shipping_boxes.update', ['shippingBox' => $shippingBox, 'id' => $shippingBox->id]) }}"
      autocomplete="off"
>
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'name',
                'label' => __('Name'),
                'value' => $shippingBox->name ?? ''
            ])
        </li>
        <li class="list-group-item {{session('customer_id') ? 'd-none' : ''}}">
            @include('shared.forms.ajaxSelect', [
                'url' => route('shipping_carriers.filter_customers'),
                'name' => 'customer_id',
                'className' => 'ajax-user-input',
                'placeholder' => __('Search'),
                'label' => __('Customer'),
                'hidden' => session('customer_id') ? 'd-none' : '',
                'default' => [
                            'id' => $shippingBox->customer->id ?? session('customer_id') ?? old('customer_id'),
                            'text' => $shippingBox->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
                        ]
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'wms_id',
                'label' => __('WMS ID'),
                'value' => $shippingBox->wms_id ?? ''
            ])
        </li>
    </ul>
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="shipping-box-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
