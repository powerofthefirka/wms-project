@include('shared.forms.ajaxSelect', [
    'url' => route('shipping_carriers.filter_customers'),
    'name' => 'customer_id',
    'className' => 'ajax-user-input',
    'placeholder' => __('Search'),
    'label' => __('Customer'),
    'hidden' => session('customer_id') ? 'd-none' : '',
    'default' => [
        'id' => $shippingBox->customer->id ?? session('customer_id') ?? old('customer_id'),
        'text' => $shippingBox->customer->contactInformation->name ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
    ]
])
@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $shippingBox->name ?? ''
])
@include('shared.forms.input', [
    'name' => 'wms_id',
    'label' => __('WMS ID'),
    'value' => $shippingBox->wms_id ?? ''
])
