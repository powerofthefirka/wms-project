<h3 class="mb-3" style="display: inline-block">{{__('Shipping Box')}}:</h3>
<a href="{{route('shipping_boxes.editable_preview', ['shipping_boxes.editablePreview' => $shippingBox])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Name')}}: <strong>{{$shippingBox->name}}</strong></li>
    <li class="list-group-item {{session('customer_id') ? 'd-none' : ''}}">{{__('Customer Name')}}: <strong>{{$shippingBox->customer->contactInformation->name}}</strong></li>
    <li class="list-group-item">{{__('WMS ID')}}: <strong>{{$shippingBox->wms_id}}</strong></li>
</ul>
<div class="row">
    <div class="col text-right">
        <a href="{{route('shipping_boxes.edit',['shipping_box' => $shippingBox])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('shipping_boxes.destroy', ['shipping_box' => $shippingBox, 'id' => $shippingBox->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this shipping box?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
