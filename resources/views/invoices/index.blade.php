@extends('layouts.app', ['title' => __('Invoices'), 'submenu' => 'billings.old.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
            </div>
        </div>
        <div class="row">
            @foreach ($total_invoices_by_statuses as $total_invoices_by_status)
                <div class="col col-6 col-lg-2 col-md-3 col-sm-4 ">
                    <a class="btn-custom col filter-by-status-button">
                        <div class="sts sts-blue">{{ __( $total_invoices_by_status['name'] ) }}</div>
                        <div class="cnt">{{ $total_invoices_by_status['invoices_count'] }}</div>
                        <strong>
                            @if($total_invoices_by_status['invoices_count'] > 1)
                                {{ __('Invoices') }}
                            @else
                                {{ __('Invoice') }}
                            @endif
                        </strong>
                    </a>
                </div>
            @endforeach
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h3 class="mb-0">{{ __('Invoices') }}</h3>
                            </div>
                            <div class="col-6 text-right">
                                @include('shared.modals.showHideTableColumns')
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('alerts.success', ['class' => 'mt-2 mb-1'])
                        @include('alerts.errors', ['class' => 'mt-2 mb-1'])
                    </div>

                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="invoices-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
@push('js')
    <script>
        new Preview();
        new Invoice();
    </script>
@endpush
