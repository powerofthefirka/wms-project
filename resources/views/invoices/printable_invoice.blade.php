<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
            align-content: center;
        }
        body {
            padding-left: 40px;
        }
        h3 {
            text-align: center;
        }

        h4 {
            margin-bottom: 3px;
            margin-top: 3px;
        }

        h5 {
            margin-bottom: 3px;
        }

        table {
            margin: 0 auto;
            width: -webkit-fill-available;
            font-size: 11px;
        }

        table.buyer-seller{
            font-size: 11px;
            margin-bottom: 40px;
        }

        table.places {
            text-align: center;
            margin-bottom: 20px;
        }

        table.items {
            border-collapse: collapse;
            margin-bottom: 30px;
        }

        table.items tr td, table.items tr th {
            padding: 5px;
            text-align: center;
        }

        table.items * {
            border: .5px solid black;
        }

        table.items, table.items tbody {
            border-left: none;
            border-right: none;
            border-bottom: none;
        }

        table.items .no-borders, table.items .no-borders * {
            border: none;
        }

        footer {
            border-top: 1px solid black;
            position: fixed;
            bottom: -40px;
            left: 0px;
            right: 0px;
            height: 50px;
            font-size: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div>3PLD</div>
    <h3>Invoice ID: {{ $invoice->id }}</h3>
    <table class="invoice_top_info">
        <tr>
            <td style="text-align: right">
                Date: <strong>{{$invoice->date}}</strong>
                <br>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                Status: {{ $invoice->invoiceStatus['name'] }}
            </td>
        </tr>
    </table>
    <table class="items">
        <thead>
        <tr>
            <th>Title</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
            @foreach($invoice->invoiceLines as $key => $row)
                <tr>
                    <td >
                        {{ $row->title }}
                    </td>
                    <td >
                        {{ $row->cost }}
                    </td>
                </tr>
            @endforeach
            <tr>
                <th style="text-align: right">
                    TOTAL:
                </th>
                <td>
                    {{$invoice->invoiceLines->sum('cost')}}
                </td>
            </tr>
        </tbody>
    </table>
    <footer>
        Footer Information
    </footer>
</body>
</html>
