@extends('layouts.app', ['title' => __('Invoices'), 'submenu' => 'billings.old.menu'])

@section('content')
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Invoice Details') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('invoices.print_preview',['invoice' => $invoice]) }}" class="btn btn-secondary btn-sm" target="_blank">{{ __('Print Invoice') }}</a>
                                <a href="{{ route('invoices.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                    <div class="table-responsive p-4">
                        @if(empty($invoice))
                            <div>
                                Invalid invoice!!
                            </div>
                        @else
                            <form method="post" action="{{ route('invoices.update', ['invoice' => $invoice]) }}" autocomplete="off">
                                @csrf
                                {{ method_field('PUT') }}

                                <input type="hidden" name="id" value="{{$invoice->id}}">
                                <input type="hidden" name="customer_id" value="{{$invoice->customer_id}}">
                                <input type="hidden" name="billing_profile_id" value="{{$invoice->billing_profile_id}}">
                                @include('invoices.invoiceStatuses')
                                <table class="table align-items-center table-hover col-12">
                                    <caption style="caption-side: top;">Invoice ID: {{ $invoice->id }}, Date: {{ $invoice->date }}</caption>
                                    <thead class="">
                                        <th>{{__('Title')}}</th>
                                        <th>{{__('Cost')}}</th>
                                        <th></th>
                                    </thead>
                                    <tbody style="cursor:pointer" id="item_container">
                                    @foreach($invoice->invoiceLines as $key => $row)
                                        <tr class="order-item-fields">
                                            <td>
                                                <input name="invoice_lines[{{$key}}][id]" type="hidden" value="{{  $row->id }}">
                                                <input name="invoice_lines[{{$key}}][title]" class="form-control form-control-sm" type="text" value="{{ $row->title }}">
                                            </td>
                                            <td>
                                                <input name="invoice_lines[{{$key}}][cost]" class="form-control reset_on_delete" type="number" value="{{ $row->cost }}">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger delete-item">
                                                    {{ __('Delete') }}
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <button id="add_item" class="btn btn-success ">{{ __('Add more items') }}</button>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success ">{{ __('Save') }}</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new Invoice();
    </script>
@endpush
