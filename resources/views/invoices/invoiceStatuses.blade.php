@include('shared.forms.ajaxSelect', [
    'url' => route('invoices.invoice_statuses'),
    'name' => 'invoice_status_id',
    'className' => 'ajax-user-input',
    'placeholder' => __('Search'),
    'label' => __('Invoice Status'),
    'default' => [
                'id' => $invoice->invoiceStatus->id ?? old('invoice_status_id'),
                'text' => $invoice->invoiceStatus->name ?? ''
            ]
])
