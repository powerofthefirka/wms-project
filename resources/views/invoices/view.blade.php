@extends('layouts.app')

@section('content')
@component('layouts.headers.auth')
@endcomponent
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Invoice Details') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    @include('alerts.success')
                    @include('alerts.errors')
                </div>
                <div class="table-responsive p-4">
                    @if(empty($invoice))
                    <div>
                        Invalid invoice!!
                    </div>
                    @else
                    <table class="table align-items-center table-hover col-12">
                        <caption style="caption-side: top;">Invoice ID: {{ $invoice->id }}, Date: {{ $invoice->date }}</caption>
                        <thead class="">
                            <th>{{ __('Title') }}</th>
                            <th>{{ __('Cost') }}</th>
                        </thead>
                        <tbody>
                            @foreach($invoice->invoiceLines as $row)
                            <tr>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->cost }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
@endsection
