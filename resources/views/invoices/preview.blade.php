<h3 class="mb-3" style="display: inline-block">{{__('Invoice')}}:</h3>
<a href="{{route('invoices.editable_preview', ['invoice' => $invoice])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Date')}}: <strong>{{$invoice->date}}</strong></li>
    <li class="list-group-item">{{__('Customer')}}: <strong>{{$invoice->customer->contactInformation->name ?? '-'}}</strong></li>
    <li class="list-group-item">{{__('Billing Profile')}}: <strong>{{$invoice->billingProfile->name ?? '-'}}</strong></li>
    <li class="list-group-item">{{__('Direct URL')}}:
        <strong>
            <a href="{{route('invoice.direct_url', ['direct_url' => $invoice->direct_url])}}">{{$invoice->direct_url}}</a>
        </strong>
    </li>
</ul>
<h3 class="mb-3">{{__('Invoice Items')}}:</h3>
@foreach($invoice->invoiceLines as $item)
    <ul class="list-group mb-3">
        <li class="list-group-item">
            {{__('Title')}}: <strong>{{$item->title}}</strong><br>
            {{__('Cost')}}: <strong>{{$item->cost}}</strong><br>
        </li>
    </ul>
@endforeach
<div class="row">
    <div class="col text-right">
        <a href="{{route('invoices.edit',['invoice' => $invoice])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('invoices.destroy', ['invoice' => $invoice, 'id' => $invoice->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this invoice?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
