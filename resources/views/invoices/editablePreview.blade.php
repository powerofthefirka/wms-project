<h3 class="mb-3">{{__('Quick Edit Invoice')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('invoices.update', ['invoice' => $invoice]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    <input type="hidden" name="id" value="{{$invoice->id}}">
    <input type="hidden" name="customer_id" value="{{$invoice->customer_id}}">
    <input type="hidden" name="billing_profile_id" value="{{$invoice->billing_profile_id}}">
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('invoices.invoice_statuses'),
                'name' => 'invoice_status_id',
                'className' => 'ajax-user-input',
                'placeholder' => __('Search'),
                'label' => __('Invoice Status'),
                'default' => [
                            'id' => $invoice->invoiceStatus->id ?? old('invoice_status_id'),
                            'text' => $invoice->invoiceStatus->name ?? ''
                        ]
            ])
        </li>
    </ul>
    <h3 class="mb-3">{{__('Invoice Items')}}:</h3>

    @foreach($invoice->invoiceLines as $key => $row)
        <ul class="list-group mb-3">
            <li class="list-group-item">
                <input name="invoice_lines[{{$key}}][id]" type="hidden" value="{{  $row->id }}">
                @include('shared.forms.input', [
                   'name' => 'invoice_lines[' . $key . '][title]',
                   'label' => __('Title'),
                   'type' => 'text',
                   'value' => $row->title,
               ])
                @include('shared.forms.input', [
                   'name' => 'invoice_lines[' . $key . '][cost]',
                   'label' => __('Cost'),
                   'type' => 'number',
                   'value' => $row->cost
                ])
            </li>
        </ul>
    @endforeach
    <div class="row">
        <div class="col text-right">
{{--             SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="invoices-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>


