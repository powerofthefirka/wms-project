<h3 class="mb-3">{{__('Quick Edit Purchase Order')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('purchase_orders.update', ['purchaseOrder' => $purchaseOrder]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    <input type="hidden" name="customer_id" value="{{$purchaseOrder->customer->id}}">
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'number',
               'label' => __('Number'),
               'value' => $purchaseOrder->number ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('purchase_orders.filter_warehouses', ['customer' => $purchaseOrder->customer->id ?? 1]),
                'name' => 'warehouse_id',
                'className' => 'ajax-user-input warehouse_id enabled-for-customer',
                'placeholder' => __('Search'),
                'label' => __('Warehouse'),
                'default' => [
                    'id' => $purchaseOrder->warehouse->id ?? old('warehouse_id'),
                    'text' => $purchaseOrder->warehouse->contactInformation->name ?? ''
                ],
                'fixRouteAfter' => '.ajax-user-input.customer_id'
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'number',
               'label' => __('Number'),
               'value' => $purchaseOrder->number ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('purchase_orders.filter_suppliers',  ['customer' => $purchaseOrder->customer->id ?? 1]),
                'name' => 'supplier_id',
                'className' => 'ajax-user-input supplier_id enabled-for-customer',
                'placeholder' => __('Search'),
                'label' => __('Supplier'),
                'default' => [
                    'id' => $purchaseOrder->supplier->id ?? old('supplier_id'),
                    'text' => $purchaseOrder->supplier->contactInformation->name ?? ''
                ],
                'fixRouteAfter' => '.ajax-user-input.customer_id'

            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
              'name' => 'ordered_at',
              'label' => __('Ordered at'),
              'class' => 'date-picker',
              'value' => localized_date($purchaseOrder->ordered_at) ?? ''
           ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'expected_at',
               'label' => __('Expected at'),
               'class' => 'date-picker',
               'value' => localized_date($purchaseOrder->expected_at) ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'delivered_at',
               'label' => __('Delivered at'),
               'class' => 'date-picker',
               'value' => localized_date($purchaseOrder->delivered_at) ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'notes',
               'label' => __('Notes'),
               'value' => $purchaseOrder->notes ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'priority',
               'label' => __('Priority'),
               'value' => $purchaseOrder->priority ?? '0'
            ])
        </li>
    </ul>
    <h3 class="mb-3">{{__('Purchase Order Items')}}:</h3>
    @foreach($purchaseOrder->purchaseOrderItems as $key => $orderItem)
        <ul class="list-group mb-3">
            <li class="list-group-item">
                <input type="hidden" name="purchase_order_items[{{$key}}][purchase_order_item_id]" value="{{$orderItem['id'] }}">
                @include('shared.forms.ajaxSelect', [
                   'url' => route('purchase_orders.filter_products', ['purchaseOrder' => $purchaseOrder]),
                   'name' => 'purchase_order_items[' . $key . '][product_id]',
                   'className' => 'ajax-user-input',
                   'placeholder' => __('Search for a product to add'),
                   'label' => __('Product'),
                   'default' => [
                       'id' => $orderItem->product->id ?? 0,
                       'text' => 'SKU: ' . $orderItem->sku . ', NAME: ' . $orderItem->name
                   ]
               ])
                @include('shared.forms.input', [
                   'name' => 'purchase_order_items[' . $key . '][quantity]',
                   'label' => __('Quantity'),
                   'type' => 'number',
                   'value' => $orderItem->quantity,
                   'class' => 'reset_on_delete quantity',
                   'min' => 'min=0'
               ])
                @include('shared.forms.input', [
                   'name' => 'purchase_order_items[' . $key . '][quantity_received]',
                   'label' => __('Received'),
                   'type' => 'number',
                   'class' => 'reset_on_delete quantity_to_receive',
                   'value' => $orderItem->quantity_received
                ])
                @include('shared.forms.input', [
                  'name' => 'purchase_order_items[' . $key . '][quantity_sell_ahead]',
                  'label' => __('Sell ahead'),
                  'type' => 'number',
                  'class' => 'reset_on_delete quantity_sell_ahead',
                  'value' => $orderItem->sell_ahead
               ])
                @include('shared.forms.input', [
                  'name' => 'purchase_order_items[' . $key . '][unit_price]',
                  'label' => __('Unit price') . label_format(currency($purchaseOrder->customer ?? null)),
                  'type' => 'number',
                  'class' => 'reset_on_delete unit_price',
                  'value' => $orderItem->price
               ])
            </li>
        </ul>
    @endforeach
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="purchase-orders-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>


