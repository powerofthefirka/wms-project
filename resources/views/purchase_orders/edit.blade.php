@extends('layouts.app', ['title' => __('Purchase order management'), 'submenu' => 'purchase_orders.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('purchase_orders.update', ['purchaseOrder' => $purchaseOrder]) }}" autocomplete="off" novalidate>
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Edit Purchase Order') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="heading-small text-muted mb-4">{{ __('Purchase Order information') }}</h6>
                            <div class="row">
                                @include('purchase_orders.purchaseOrderInformationFields', [
                                    'purchaseOrder' => $purchaseOrder,
                                     'editPage' => $editPage
                                ])
                            </div>
                            <h6 class="heading-small text-muted m-0">{{ __('Purchase order items') }}</h6>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="col-12 table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('Product')}}</th>
                                        <th scope="col">{{ __('Status')}}</th>
                                        <th scope="col">{{ __('Ordered')}}</th>
                                        <th scope="col">{{ __('Received')}}</th>
                                        <th scope="col">{{ __('Sell ahead')}}</th>
                                        <th scope="col" for="th-currency">{{ __('Unit price')}}<span></span></th>
                                        <th scope="col" for="th-currency">{{ __('Total price')}}<span></span></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="item_container">
                                    @if(count($purchaseOrder->purchaseOrderItems ?? [] ) >  0)
                                        @foreach($purchaseOrder->purchaseOrderItems as $key => $orderItem)
                                            @include('purchase_orders.purchaseProductLine')
                                        @endforeach
                                    @else
                                        @include('purchase_orders.purchaseProductLine')
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-body pb-0 pt-0">
                                <div class="row">
                                    <div class="col col-12">
                                        @include('shared.forms.input', [
                                           'name' => 'notes',
                                           'label' => __('Notes'),
                                           'value' => $purchaseOrder->notes ?? ''
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('purchase_orders.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @if(auth()->user()->isAdmin())
            @include('purchase_orders.changeLog', [ 'filter' => false])
        @endif
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        let customer = {{$purchaseOrder->customer_id}}
        new PurchaseOrderForm();
    </script>
@endpush
