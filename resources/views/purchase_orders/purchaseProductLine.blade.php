<tr class="order-item-fields {{ ($orderItem['quantity'] ?? false) || ($orderItem['product_id'] ?? $orderItem['sku'] ?? false) ? '':'d-none' }}">
    @if (\Illuminate\Support\Facades\Route::has('*/edit'))
    <input type="hidden" name="purchase_order_items[{{$key ?? 0}}][purchase_order_item_id]" value="{{$orderItem['id'] }}">
    @endif
    <td class="d-none purchase-order-item-product-id">
        @include('shared.forms.input', [
        'name' => 'purchase_order_items[' . ($key ?? 0) . '][product_id]',
        'type' => 'number',
        'label' => '',
        'value' => $orderItem->product->id ?? '',
        'class' => 'p-0 product_id'
        ])
    </td>
    <td class="purchase-item-product-name min-width min-width-100 custom-box">
        {{ __('Name') }}: {{  $orderItem->name ?? $product->name ?? '' }}<br>
        {{ __('SKU') }}: {{  $orderItem->sku ?? $product->sku ?? '' }}<br>
        {{ __('On hand') }}: {{  $orderItem->quantity_on_hand ?? $product->quantity_on_hand ?? 0 }}<br>
        {{ __('Backordered') }}: {{  $orderItem->quantity_backordered ?? 0 }}<br>
    </td>
    <td>
        <span class="status"></span>
    </td>
    <td>
        @include('shared.forms.input', [
        'name' => 'purchase_order_items[' . ($key ?? 0) . '][quantity]',
        'label' => '',
        'type' => 'number',
        'value' => $orderItem->quantity ?? 0,
        'class' => 'reset_on_delete quantity ordered',
        'min' => 'min=' . (($orderItem->quantity ?? true) ? 1:0),
        ])
    </td>
    <td>
        <span class="received">{{$orderItem->quantity_received ?? 0}}</span>
    </td>
    <td>
        @include('shared.forms.input', [
        'name' => 'purchase_order_items[' . ($key ?? 0) . '][quantity_sell_ahead]',
        'label' => '',
        'type' => 'number',
        'value' => $orderItem->sell_ahead ?? 0,
        'min' => 'min=0',
        'class' => 'reset_on_delete quantity_sell_ahead'
        ])
    </td>
    <td>
        @include('shared.forms.input', [
        'name' => 'purchase_order_items[' . ($key ?? 0) . '][unit_price]',
        'label' => '',
        'type' => 'number',
        'value' => $orderItem->price ?? 0,
        'class' => 'reset_on_delete unit_price'
        ])
    </td>
    <td>
        <span class="total_price">{{0}}</span>
    </td>

    <td class="text-right">
        <a class="btn btn-primary btn-sm text-white remove-purchase-order-item remove-item-button">
            ×
        </a>
    </td>
</tr>
