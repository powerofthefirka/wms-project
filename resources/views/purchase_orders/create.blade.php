@extends('layouts.app', ['title' => __('Purchase order management'), 'submenu' => 'purchase_orders.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('purchase_orders.store') }}" autocomplete="off" novalidate>
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Add Purchase Order') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="heading-small text-muted mb-4">{{ __('Purchase Order information') }}</h6>
                            <div class="row">
                                @include('purchase_orders.purchaseOrderInformationFields')
                            </div>
                            <h6 class="heading-small text-muted m-0">{{ __('Purchase order items') }}</h6>
                        </div>
                        <div class="card-body pb-0">
                            <fieldset>
                                <div class="row">
                                    <div class="col col-12 mb--2">
                                        @include('shared.forms.ajaxSelect', [
                                            'url' => route('purchase_orders.filter_products', ['supplier' => session('supplier_id') ?? old('supplier_id') ?? 1]),
                                            'name' => 'product_id',
                                            'required' => 'required',
                                            'className' => 'ajax-user-input product-select',
                                            'placeholder' => __('Search'),
                                            'label' => __('Search to add product')
                                        ])
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="card-body auto-buttons d-none">
                            <button type="button" class="btn btn-primary btn-sm auto-po-items" data-url="/purchase_orders/auto_pending/">Auto Pending</button>
                            <button type="button" class="btn btn-primary btn-sm auto-po-items" data-url="/purchase_orders/auto_all/">Auto All</button>
                            <button type="button" class="btn btn-primary btn-sm auto-po-items" data-url="/purchase_orders/auto_backordered/">Auto Backordered</button>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="col-12 table align-items-center table-flush" style="width: 100% !important;">
                                    <thead class="">
                                    <tr>
                                        <th scope="col">{{ __('Product') }}</th>
                                        <th scope="col">{{ __('Status') }}</th>
                                        <th scope="col">{{ __('Ordered') }}</th>
                                        <th scope="col">{{ __('Received') }}</th>
                                        <th scope="col">{{ __('Sell ahead') }}</th>
                                        <th scope="col" for="th-currency">{{ __('Unit price') }}<span></span></th>
                                        <th scope="col" for="th-currency">{{ __('Total price') }}<span></span></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="item_container">
                                    @if(count( old()['purchase_order_items'] ?? [] ) > 0)
                                        @foreach(old()['purchase_order_items'] as $key => $orderItem)
                                            @php
                                                $product = App\Models\Product::find($orderItem['product_id'] ?? null);
                                            @endphp
                                            @include('purchase_orders.purchaseProductLine')
                                        @endforeach
                                    @else
                                        @include('purchase_orders.purchaseProductLine')
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-body pb-0 pt-0">
                                <div class="row">
                                    <div class="col col-12">
                                        @include('shared.forms.input', [
                                           'name' => 'notes',
                                           'label' => __('Notes'),
                                           'value' => $purchaseOrder->notes ?? ''
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('purchase_orders.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new PurchaseOrderForm();
    </script>
@endpush
