@extends('layouts.app', ['title' => __('Purchase Order Change Log'), 'submenu' => 'purchase_orders.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <input type="hidden" id="change-log-purchase-order-id" value="">
            <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                <div class="card transparent">
                    @include('alerts.success')
                    @include('alerts.errors')
                </div>
            </div>
            @include('purchase_orders.changeLogDataTableFilter')
            <div class="col col-12">
                <div class="card table-card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h3 class="mb-0">{{ __('Purchase Order Change Log') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="purchase-order-change-log-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new PurchaseOrderForm();
    </script>
@endpush
