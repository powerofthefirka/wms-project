<div class="col-xl-6 sizing">
{{--    @include('shared.dropdowns.searchByCustomer', [--}}
{{--        'default' => $purchaseOrder->warehouse->customer->id ?? old('customer_id'),--}}
{{--        'defaultWarehouse' => $purchaseOrder->warehouse_id ?? old('warehouse_id'),--}}
{{--        'defaultVendor' => $purchaseOrder->supplier_id ?? old('supplier_id'),--}}
{{--        'warehouseList' => true,--}}
{{--        'vendorList' => true,--}}
{{--    ])--}}
{{--    <input name="customer_id"--}}
{{--           id="customer_id" value="{{$purchaseOrder->customer_id}}" class="user-customer customer_id" >--}}

    @if(!\Illuminate\Support\Facades\Request::is('purchase_orders/create'))
        <div class="form-group">
            <label class="form-control-label">{{ __('Number') }}</label><br>
            <span>{{$purchaseOrder->number}}</span>
        </div>
        <div class="form-group">
            <label class="form-control-label">{{ __('Warehouse') }}</label><br>
            <span>{{
                $purchaseOrder->warehouse->contactInformation->name . ', ' .
                $purchaseOrder->warehouse->contactInformation->email . ', ' .
                $purchaseOrder->warehouse->contactInformation->zip . ', ' .
                $purchaseOrder->warehouse->contactInformation->city . ', ' .
                $purchaseOrder->warehouse->contactInformation->phone
                }}</span>
        </div>

        @if (!empty($purchaseOrder->supplier->contactInformation))
        <div class="form-group">
            <label class="form-control-label">{{ __('Vendor') }}</label><br>
            <span>{{
                $purchaseOrder->supplier->contactInformation->name . ', ' .
                $purchaseOrder->supplier->contactInformation->email . ', ' .
                $purchaseOrder->supplier->contactInformation->zip . ', ' .
                $purchaseOrder->supplier->contactInformation->city . ', ' .
                $purchaseOrder->supplier->contactInformation->phone
                }}</span>
        </div>
        @endif

        <div class="form-group">
            <label class="form-control-label">{{ __('Purchase Order Status') }}</label><br>
            <span>{{$purchaseOrder->status}}</span>
        </div>
    @else
        @include('shared.dropdowns.searchByCustomer', [
            'default' => $purchaseOrder->warehouse->customer->id ?? old('customer_id'),
            'defaultWarehouse' => $purchaseOrder->warehouse_id ?? old('warehouse_id'),
            'defaultVendor' => $purchaseOrder->supplier_id ?? old('supplier_id'),
            'warehouseList' => true,
            'vendorList' => true,
        ])
        @include('shared.forms.input', [
           'name' => 'number',
           'label' => __('Number'),
           'value' => $purchaseOrder->number ?? ''
        ])
    @endif
    @if(isset($editPage))
        <div class="form-group">
            <label class="form-control-label">{{ __('Shipping Carrier') }}</label><br>
            <span>{{$purchaseOrder->shippingCarrier->name ?? ''}}</span>
        </div>
        <div class="form-group">
            <label class="form-control-label">{{ __('Shipping Method') }}</label><br>
            <span>{{$purchaseOrder->shippingMethod->name ?? ''}}</span>
        </div>
    @endif
</div>
<div class="col-xl-6 sizing">
    @include('shared.forms.input', [
      'name' => 'ordered_at',
      'label' => __('Ordered at'),
      'class' => 'date-picker',
      'value' => localized_date($purchaseOrder->ordered_at ?? '')
   ])
    @include('shared.forms.input', [
       'name' => 'expected_at',
       'label' => __('Expected at'),
       'class' => 'date-picker',
       'value' => localized_date($purchaseOrder->expected_at ?? '')
    ])
    @include('shared.forms.input', [
       'name' => 'expected_at',
       'label' => __('Delivered at'),
       'value' => localized_date($purchaseOrder->delivered_at ?? ''),
       'disabled' => 'disabled'
    ])
    @include('shared.forms.input', [
       'name' => 'tracking_number',
       'label' => __('Tracking number'),
       'value' => $purchaseOrder->tracking_number ?? ''
    ])
    @include('shared.forms.input', [
       'name' => 'tracking_url',
       'label' => __('Tracking url'),
       'value' => $purchaseOrder->tracking_url ?? ''
    ])
    @include('shared.forms.input', [
       'name' => 'hours_receiving',
       'label' => __('Time spent receiving (hours)'),
       'value' => $purchaseOrder->hours_receiving ?? ''
    ])
</div>
