<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('purchase_orders.index') }}" class="nav-link {{ current_page(route('purchase_orders.index')) }}">
                    <span class="nav-link-text">{{ __('Manage Purchase Orders') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('purchase_orders.create') }}" class="nav-link {{ current_page(route('purchase_orders.create')) }}">
                    <span class="nav-link-text">{{ __('Create Purchase Order') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('vendors.index') }}" class="nav-link {{ current_page(route('vendors.index')) }}">
                    <span class="nav-link-text">{{ __('Manage Vendors') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
