<h3 class="mb-3" style="display: inline-block">{{__('Purchase Order')}}:</h3>
@include('alerts.successEditablePreview')
<a href="{{route('purchase_orders.editable_preview', ['purchaseOrder' => $purchaseOrder])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Number')}}: <strong>{{$purchaseOrder->number}}</strong></li>
    <li class="list-group-item">{{__('Warehouse')}}: <strong>{{$purchaseOrder->warehouse->contactInformation['name'] ?? '-'}}</strong></li>
    <li class="list-group-item">{{__('Vendor')}}: <strong>{{$purchaseOrder->supplier->contactInformation['name'] ?? '-'}}</strong></li>
    <li class="list-group-item">{{__('Status')}}: <strong>{{$purchaseOrder->status ?? '-'}}</strong></li>
    <li class="list-group-item">{{__('Expected at')}}: <strong>{{localized_date($purchaseOrder->expected_at)}}</strong></li>
    <li class="list-group-item">{{__('Delivered at')}}: <strong>{{localized_date($purchaseOrder->delivered_at)}}</strong></li>
    <li class="list-group-item">{{__('Notes')}}: <strong>{{$purchaseOrder->notes}}</strong></li>
    <li class="list-group-item">{{__('Total unique items')}}: <strong>{{$purchaseOrder->purchaseOrderItems->count()}}</strong></li>
    <li class="list-group-item">{{__('Total quantity')}}: <strong>{{$purchaseOrder->purchaseOrderItems->sum('quantity')}}</strong></li>
    <li class="list-group-item">{{__('Tracking number')}}:
        <strong>
            @if(!empty($purchaseOrder->tracking_url))
                <a href="{{ url($purchaseOrder->tracking_url)}}" target="_blank">{{$purchaseOrder->tracking_number}}</a>
            @else
                {{$purchaseOrder->tracking_number}}
            @endif
        </strong>
    </li>
</ul>
<h3 class="mb-3">{{__('Purchase Order Items')}}:</h3>
@foreach($purchaseOrder->purchaseOrderItems as $item)
    <ul class="list-group mb-3">
        <li class="list-group-item">
            {{__('Status')}}: <strong>
                @if($item->quantity_received == 0)
                    {{__('pending')}}
                @endif
                @if($item->quantity_received == $item->quantity)
                    {{__('received')}}
                @endif
                @if($item->quantity_received > $item->quantity)
                   {{__('over received')}}
                @endif
                @if($item->quantity_received < $item->quantity)
                   {{__('under received')}}
                @endif
            </strong><br>
            {{__('Sku')}}: <strong>{{$item->sku}}</strong><br>
            {{__('Name')}}: <strong>{{$item->name}}</strong><br>
            {{__('Quantity received')}}: <strong>{{$item->quantity_received}}</strong><br>
            {{__('Quantity')}}: <strong>{{$item->quantity}}</strong><br>
        </li>
    </ul>
@endforeach
<div class="row">
    <div class="col text-right">
        <a href="{{route('purchase_orders.edit',['purchaseOrder' => $purchaseOrder])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        @if($purchaseOrder->canCancel())
            <a href="{{route('purchase_orders.cancel', ['purchaseOrder' => $purchaseOrder])}}" class="btn btn-sm btn-danger cancel" data-table_id="purchase-orders-table">
                {{__('Cancel')}}
            </a>
        @endif
    </div>
</div>
