<h3 class="mb-3">{{__('Quick Edit Shipping Method')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('shipping_methods.update', ['shippingMethod' => $shippingMethod, 'id' => $shippingMethod->id]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'name',
                'label' => __('Name'),
                'value' => $shippingMethod->name ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('shipping_method.get_carriers'),
                'name' => 'shipping_carrier_id',
                'className' => 'ajax-user-input',
                'placeholder' => __('Search'),
                'label' => __('Shipping Carrier'),
                'default' => [
                    'id' => $shippingMethod->shippingCarrier->id ?? old('shipping_carrier_id'),
                    'text' => $shippingMethod->shippingCarrier->name ?? ''
                ]
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'title',
                'label' => __('Title'),
                'value' => $shippingMethod->title ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
               'name' => 'wms_id',
               'label' => __('WMS ID'),
               'value' => $shippingMethod->wms_id ?? ''
           ])
        </li>
    </ul>
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="shipping-method-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
