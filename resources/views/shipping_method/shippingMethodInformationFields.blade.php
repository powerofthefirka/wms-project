@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $shippingMethod->name ?? ''
])
<div class="form-group">
    <label class="form-control-label">{{ __('Shipping Carrier') }}</label>
    <select name="shipping_carrier_id" class="form-control enabled-for-3pl" data-toggle="select" data-placeholder="">
        <option value="{{$shippingMethod->shippingCarrier->id ?? old('shipping_carrier_id') ?? 'chooseHere'}}">{{$shippingMethod->shippingCarrier->name ?? ''}}</option>
    </select>
</div>
@include('shared.forms.input', [
    'name' => 'title',
    'label' => __('Title'),
    'value' => $shippingMethod->title ?? ''
])
@include('shared.forms.input', [
    'name' => 'wms_id',
    'label' => __('WMS ID'),
    'value' => $shippingMethod->wms_id ?? ''
])
@include('shared.forms.input', [
    'name' => 'cost',
    'label' => __('Cost'),
    'value' => $shippingMethod->cost ?? '0'
])
