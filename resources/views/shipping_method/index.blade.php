@extends('layouts.app', ['title' => __('Shipping Method'), 'submenu' => 'orders.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h3 class="mb-0">{{ __('Shipping Methods') }}</h3>
                            </div>
                            <div class="col-6 text-right">
                                <a href="{{ route('shipping_methods.create') }}" class="btn btn-secondary btn-sm">
                                    <i class="ci ci-more"></i>
                                    {{ __('Add Shipping Method') }}
                                </a>
                                @include('shared.modals.showHideTableColumns')
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        @include('alerts.success', ['class' => 'mt-2 mb-1'])
                        @include('alerts.errors', ['class' => 'mt-2 mb-1'])
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-hover col-12 p-0" id="shipping-method-table" style="width: 100% !important;">
                            <thead class=""></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new Preview();
        new ShippingMethod();
    </script>
@endpush

