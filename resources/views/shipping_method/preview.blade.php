<h3 class="mb-3" style="display: inline-block">{{__('Shipping Method')}}:</h3>
<a href="{{route('shipping_methods.editable_preview', ['shipping_method' => $shippingMethod])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Name')}}: <strong>{{$shippingMethod->name}}</strong></li>
    <li class="list-group-item">{{__('Carrier Name')}}: <strong>{{$shippingMethod->shippingCarrier->name}}</strong></li>
</ul>
<div class="row">
    <div class="col text-right">
        <a href="{{route('shipping_methods.edit',['shipping_method' => $shippingMethod])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('shipping_methods.destroy', ['shipping_method' => $shippingMethod, 'id' => $shippingMethod->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this shipping method?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
