<h3 class="mb-3" style="display: inline-block">{{__('Shipping Carrier')}}:</h3>
<a href="{{route('shipping_carriers.editable_preview', ['shipping_carriers.editablePreview' => $shippingCarrier])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
    <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
</a>
<ul class="list-group mb-3">
    <li class="list-group-item">{{__('Name')}}: <strong>{{$shippingCarrier->name}}</strong></li>
    <li class="list-group-item">{{__('3PL')}}: <strong>{{$shippingCarrier->threePl->contactInformation->name}}</strong></li>
</ul>
<h3 class="mb-3">{{__('Shipping Carrier Methods')}}:</h3>
@foreach($shippingCarrier->shippingMethods as $item)
    <ul class="list-group mb-3">
        <li class="list-group-item">
            {{__('Name')}}: <strong>{{$item->name}}</strong><br>
            {{__('Title')}}: <strong>{{$item->title}}</strong><br>
        </li>
    </ul>
@endforeach
<div class="row">
    <div class="col text-right">
        <a href="{{route('shipping_carriers.edit',['shipping_carrier' => $shippingCarrier])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        <form action="{{route('shipping_carriers.destroy', ['shipping_carrier' => $shippingCarrier, 'id' => $shippingCarrier->id])}}" method="post" class="d-inline-block">
            @csrf
            @method('delete')
            <button type="button" class="btn btn-sm btn-danger " data-confirm-action="Are you sure you want to delete this shipping carrier?">{{__('Delete')}}</button>
        </form>
    </div>
</div>
