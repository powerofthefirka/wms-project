@include('shared.forms.input', [
    'name' => 'name',
    'label' => __('Name'),
    'value' => $shippingCarrier->name ?? ''
])
@include('shared.forms.ajaxSelect', [
    'url' => route('shipping_carriers.filter_3pls'),
    'name' => '3pl_id',
    'className' => 'ajax-user-input',
    'placeholder' => __('Search'),
    'label' => __('3PL'),
    'hidden' => session('customer_id') ? 'd-none' : '',
    'default' => [
        'id' => $shippingCarrier->threePl->id ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->id ?? old('3pl_id'),
        'text' => $shippingCarrier->threePl->contactInformation->name ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->contactInformation->name ?? ''
    ]
])
@include('shared.forms.input', [
    'name' => 'wms_id',
    'label' => __('WMS ID'),
    'value' => $shippingCarrier->wms_id ?? ''
])
@include('shared.forms.input', [
    'name' => 'tracking_url',
    'label' => __('Tracking URL'),
    'value' => $shippingCarrier->tracking_url ?? ''
])
