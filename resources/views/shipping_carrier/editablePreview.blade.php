<h3 class="mb-3">{{__('Quick Edit Shipping Carrier')}}:</h3>
@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('shipping_carriers.update', ['shippingCarrier' => $shippingCarrier, 'id' => $shippingCarrier->id]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    <ul class="list-group mb-3">
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'name',
                'label' => __('Name'),
                'value' => $shippingCarrier->name ?? ''
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.ajaxSelect', [
                'url' => route('shipping_carriers.filter_3pls'),
                'name' => '3pl_id',
                'className' => 'ajax-user-input',
                'placeholder' => __('Search'),
                'label' => __('3PL'),
                'hidden' => session('customer_id') ? 'd-none' : '',
                'default' => [
                    'id' => $shippingCarrier->threePl->id ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->id ?? old('3pl_id'),
                    'text' => $shippingCarrier->threePl->contactInformation->name ?? \App\Models\ThreePl::whereIn('id', auth()->user()->threePlIds())->first()->contactInformation->name ?? ''
                ]
            ])
        </li>
        <li class="list-group-item">
            @include('shared.forms.input', [
                'name' => 'wms_id',
                'label' => __('WMS ID'),
                'value' => $shippingCarrier->wms_id ?? ''
            ])
        </li>
    </ul>
    <div class="row">
        <div class="col text-right">
            {{-- SET data-table_id  --}}
            <button id="preview-submit-button" data-table_id="shipping-carrier-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
