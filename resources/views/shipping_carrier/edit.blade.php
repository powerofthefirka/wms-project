@extends('layouts.app', ['title' => __('Shipping Carrier'), 'submenu' => 'orders.menu'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('alerts.success')
                @include('alerts.errors')
                @include('alerts.ajax_error')
            </div>
        </div>
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Shipping Carrier') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('shipping_carriers.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="shipping-carrier-form" method="post" action="{{ route('shipping_carriers.update', ['shippingCarrier' => $shippingCarrier, 'id' => $shippingCarrier->id]) }}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('Shipping carrier information') }}</h6>
                            {{ method_field('PUT') }}
                            @include('shared.forms.dropzone', [
                                'url' => route('shipping_carriers.update', ['shippingCarrier' => $shippingCarrier, 'id' => $shippingCarrier->id]),
                                'isMultiple' => false,
                                'redirect' => route('shipping_carriers.edit', ['shippingCarrier' => $shippingCarrier]),
                                'formId' => 'shipping-carrier-form',
                                'buttonId' => 'submit-button',
                                'images' => $shippingCarrier->image
                            ])
                            <br>
                            @include('shipping_carrier.shippingCarrierInformationFields', [
                                'shippingCarrier' => $shippingCarrier
                            ])
                            <div class="text-right">
                                <button type="button" id="submit-button" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
