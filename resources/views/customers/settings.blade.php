@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('customers.update_settings') }}">
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{auth()->user()->threePls->first()->contactInformation->name}} {{ __('Customer Settings') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('shared.forms.checkbox', [
                                'name' => 'custom_shipping_modal',
                                'label' => __('Custom Customer Shipping Modal'),
                                'checked' => auth()->user()->threePls->first()->custom_shipping_modal ? 'true' : ''
                            ])<br>
                            <div class="custom-shipping-modal-text-div">
                                @include('shared.forms.input', [
                                   'name' => 'custom_shipping_modal_text',
                                   'label' => __('Custom Customer Shipping Modal Text'),
                                   'value' => auth()->user()->threePls->first()->custom_shipping_modal_text ?? ''
                                ])
                            </div>
                            @include('shared.forms.input', [
                                  'name' => 'scheduling_link',
                                  'label' => __('Scheduling link'),
                                  'value' => auth()->user()->threePls->first()->scheduling_link ?? ''
                               ])
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new CustomerForm();
    </script>
@endpush
