<li class="submenu {{ $class ?? '' }}" data-submenu="customers.menu">
    <ul>
        <li class="nav-item">
            <a href="{{ route('customers.create') }}" class="nav-link">
                <i class="ni ni-fat-add text-gray"></i>
                <span class="nav-link-text text-gray">{{ __('Add Customer') }}</span>
            </a>
        </li>
    </ul>
</li>
