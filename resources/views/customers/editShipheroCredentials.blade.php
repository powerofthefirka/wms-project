@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')

    <div class="container-fluid ">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Customer') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                         aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('customers.edit', [ 'customer' => $customer ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('Customer') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" href="{{ route('customers.edit_users', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Users') }}</a>
                                </li>
                                <li class="nav-item">
                                    <div class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-2-tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Shiphero credentials') }}</div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_messenger_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Messenger credentials') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                        <h6 class="heading-small text-muted mb-4">{{ __('Update Shiphero Credentials') }}</h6>
                        <div class="table-responsive p-0">
                            <form id="customer-shiphero-form" action="{{ route('customers.update_shiphero_credentials', [ 'customer' => $customer ]) }}" method="post" >
                                @csrf
                                <div class="card">
                                    <div class="card-body pl-0 pr-0">
                                        @if (empty($customer->local_key))
                                            @include('shared.forms.input', [
                                                'name' => 'sync_date_from',
                                                'label' => __('Sync data from'),
                                                'type' => 'date',
                                                'value' => now()->format('Y-m-d'),
                                                'autocomplete' => 'new-password'
                                            ])
                                        @endif

                                        @include('shared.forms.input', [
                                            'name' => 'shiphero_username',
                                            'label' => __('Shiphero Username'),
                                            'type' => 'text',
                                            'autocomplete' => 'new-password'
                                        ])

                                        @include('shared.forms.input', [
                                            'name' => 'shiphero_password',
                                            'label' => __('Shiphero Password'),
                                            'type' => 'password',
                                            'autocomplete' => 'new-password'
                                        ])

                                        <h6 class="heading-small text-muted mb-4">{{ __('Or') }}</h6>

                                        @include('shared.forms.input', [
                                            'name' => 'shiphero_refresh_token',
                                            'label' => __('Shiphero Refresh Token'),
                                            'type' => 'text',
                                            'autocomplete' => 'new-password'
                                        ])

                                        @include('shared.forms.input', [
                                            'name' => 'shiphero_access_token',
                                            'label' => __('Shiphero Access Token'),
                                            'type' => 'text',
                                            'autocomplete' => 'new-password'
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-12">
                <div class="card transparent text-right">
                    <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-primary" form="customer-shiphero-form" >{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

@endsection

