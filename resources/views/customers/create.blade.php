@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('customers.store') }}" autocomplete="new-password">
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                    </div>
                </div>
                <div class="col col-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Add Customer') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (auth()->user()->isAdmin())
                                <h6 class="heading-small text-muted mb-4">{{ __('3PL information') }}</h6>
                                @include('shared.forms.ajaxSelect', [
                                    'url' => route('customers.filter_3pls'),
                                    'name' => '3pl_id',
                                    'className' => 'ajax-user-input 3pl_id',
                                    'placeholder' => __('Search'),
                                    'label' => __('3PL'),
                                    'default' => [
                                        'id' => old('3pl_id'),
                                        'text' => \App\Models\ThreePl::find(old('3pl_id'))->contactInformation->name ?? ''
                                    ]
                                ])
                            @elseif (auth()->user()->isCustomerAdmin())
                                <input type="hidden" id="3pl_id" name="3pl_id" value="{{auth()->user()->threePls->first()->id ?? ''}}">
                            @endif
                                @include('shared.forms.select', [
                                        'name' => 'currency_id',
                                        'placeholder' => __('Select currency'),
                                        'label' => __('Currency'),
                                        'options' => $currencies
                                    ])
                                @include('shared.forms.weight_unit_select', [
                                        'name' => 'weight_unit'
                                    ])
                                @include('shared.forms.dimensionsUnitSelect', [
                                        'name' => 'dimensions_unit'
                                    ])
                            <h6 class="heading-small text-muted mb-4">{{ __('Customer information') }}</h6>
                            @include('shared.forms.contactInformationFields', [
                                'name' => 'contact_information',
                            ])
                            @include('shared.forms.input', [
                                'name' => 'contact_information[shared_folder]',
                                'label' => __('Shared folder')
                            ])
                        </div>
                    </div>
                </div>
                <div class="col col-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('User credentials') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('shared.forms.input', [
                                'name' => 'password',
                                'label' => __('Password'),
                                'type' => 'password',
                                'autocomplete' => 'new-password'
                            ])

                            @include('shared.forms.input', [
                                'name' => 'password_confirmation',
                                'label' => __('Confirm Password'),
                                'type' => 'password',
                                'autocomplete' => 'new-password'
                            ])
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Shiphero Settings') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('shared.forms.input', [
                                'name' => 'sync_date_from',
                                'label' => __('Sync data from'),
                                'type' => 'date',
                                'value' => now()->format('Y-m-d'),
                                'autocomplete' => 'new-password'
                            ])

                            @include('shared.forms.input', [
                                'name' => 'shiphero_username',
                                'label' => __('Shiphero Username'),
                                'type' => 'text',
                                'autocomplete' => 'new-password'
                            ])

                            @include('shared.forms.input', [
                                'name' => 'shiphero_password',
                                'label' => __('Shiphero Password'),
                                'type' => 'password',
                                'autocomplete' => 'new-password'
                            ])

                            <h6 class="heading-small text-muted mb-4">{{ __('Or') }}</h6>

                            @include('shared.forms.input', [
                                'name' => 'shiphero_refresh_token',
                                'label' => __('Shiphero Refresh Token'),
                                'type' => 'text',
                                'autocomplete' => 'new-password'
                            ])

                            @include('shared.forms.input', [
                                'name' => 'shiphero_access_token',
                                'label' => __('Shiphero Access Token'),
                                'type' => 'text',
                                'autocomplete' => 'new-password'
                            ])
                        </div>
                    </div>
                    <!-- <div class="card d-none">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Shipping Methods') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body shipping-methods">
                            @if(count( old()['shipping_methods'] ?? [] ) > 0)
                                @foreach(\App\Models\ThreePl::find(old('3pl_id'))->shippingCarriers as $shippingCarrier)
                                    @foreach($shippingCarrier->shippingMethods as $shippingMethod)
                                        <div class="custom-control custom-checkbox custom-checkbox-success">
                                            <input class="custom-control-input" name="shipping_methods[]" id="chk-shipping_methods[{{$shippingMethod->id}}]" type="checkbox" @if(in_array($shippingMethod->id, old()['shipping_methods'])) checked @endif value="{{$shippingMethod->id}}">
                                            <label class="custom-control-label" for="chk-shipping_methods[{{$shippingMethod->id}}]">{{$shippingMethod->name}}</label>
                                        </div>
                                    @endforeach
                                @endforeach
                            @endif
                        </div>
                    </div> -->
                    <!-- <div class="card d-none">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Locations') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body locations">
                            @if(count( old()['locations'] ?? [] ) > 0)
                                @foreach(\App\Models\ThreePl::find(old('3pl_id'))->locationTypes as $locationType)
                                    @foreach($locationType->locations as $location)
                                        <div class="custom-control custom-checkbox custom-checkbox-success">
                                            <input class="custom-control-input" name="locations[]" id="chk-locations[{{$location->id}}]" type="checkbox" @if(in_array($location->id, old()['locations'])) checked @endif value="{{$location->id}}">
                                            <label class="custom-control-label" for="chk-locations[{{$location->id}}]">{{$location->name}}</label>
                                        </div>
                                    @endforeach
                                @endforeach
                            @endif
                        </div>
                    </div> -->
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new CustomerForm();
    </script>
@endpush
