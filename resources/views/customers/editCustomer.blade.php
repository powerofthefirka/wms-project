@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')

    <div class="container-fluid">
        <form method="post" action="{{ route('customers.update', [ 'customer' => $customer ]) }}" enctype="multipart/form-data" autocomplete="off">
            @csrf
            <div class="row">
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">{{ __('Edit Customer') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="nav-wrapper pt-0">
                                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                    <li class="nav-item">
                                        <div class="nav-link mb-sm-3 mb-md-0 active  " id="tabs-icons-text-1-tab"
                                           aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Customer</div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('customers.edit_users', [ 'customer' => $customer ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Users</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_shiphero_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Shiphero credentials') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_messenger_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Messenger credentials') }}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                @include('alerts.success')
                                @include('alerts.errors')
                            </div>
                            <h6 class="heading-small text-muted mb-4">{{ __('Customer information') }}</h6>
                            {{ method_field('PUT') }}
                            @include('shared.forms.contactInformationFields', [
                                'name' => 'contact_information',
                                'contactInformation' => $customer->contactInformation
                            ])
                            @include('shared.forms.input', [
                                'name' => 'contact_information[shared_folder]',
                                'label' => __('Shared folder'),
                                'value' => $customer->contactInformation->shared_folder ?? $value ?? ''
                            ])
                            @include('shared.forms.select', [
                                        'name' => 'currency_id',
                                        'placeholder' => __('Select currency'),
                                        'label' => __('Currency'),
                                        'options' => $currencies,
                                        'selected' => $customer->currency_id
                                    ])
                            @include('shared.forms.weight_unit_select', [
                                'name' => 'weight_unit',
                                'selected' => $customer->weight_unit
                            ])
                            @include('shared.forms.dimensionsUnitSelect', [
                                'name' => 'dimensions_unit',
                                'selected' => $customer->dimensions_unit
                            ])
                            <div class="form-group">
                                <label for="billing_profile_id" class="form-control-label" for="inputState">{{ __('Billing Profile') }}</label>
                                <select name="billing_profile_id" id="inputState" class="form-control form-control-sm" data-live-search="true" data-live-search-placeholder="{{ __('Search..') }}">
                                    <option value="">{{ __('Choose..') }}</option>
                                    @foreach ($billingProfiles as $billingProfile)
                                        <option value="{{ $billingProfile->id }}" @if(($customer->billing_profile_id ?? 0) == $billingProfile->id) selected @endif>{{ __( $billingProfile->name ) }}</option>
                                    @endforeach
                                </select>
                            </div>
{{--                            <!-- <div class="form-group d-none">--}}
{{--                            <label class="form-control-label">{{ __('Shipping Methods') }}</label>--}}
{{--                                @foreach($threePlShippingMethods as $shippingMethod)--}}
{{--                                    <div class="custom-control custom-checkbox custom-checkbox-success">--}}
{{--                                        <input class="custom-control-input" name="shipping_methods[]" id="chk-shipping_methods[{{$shippingMethod->id}}]" type="checkbox" @if(in_array($shippingMethod->id, $customer->shippingMethods->pluck('id')->toArray())) checked @endif value="{{$shippingMethod->id}}">--}}
{{--                                        <label class="custom-control-label" for="chk-shipping_methods[{{$shippingMethod->id}}]">{{$shippingMethod->name}}</label>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            </div> -->--}}
{{--                            <!-- <div class="form-group d-none">--}}
{{--                            <label class="form-control-label">{{ __('Locations') }}</label>--}}
{{--                                @foreach($threePlLocations as $threePlLocation)--}}
{{--                                    <div class="custom-control custom-checkbox custom-checkbox-success">--}}
{{--                                        <input class="custom-control-input" name="locations[]" id="chk-locations[{{$threePlLocation->id}}]" type="checkbox" @if(in_array($threePlLocation->id, $customer->locations->pluck('id')->toArray())) checked @endif value="{{$threePlLocation->id}}">--}}
{{--                                        <label class="custom-control-label" for="chk-locations[{{$threePlLocation->id}}]">{{$threePlLocation->name}}</label>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            </div> -->--}}
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>

@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('argon') }}/vendor/select2/dist/css/select2.min.css">
@endpush

@push('js')
    <script src="{{ asset('argon') }}/vendor/select2/dist/js/select2.min.js"></script>
    <script src="{{ asset('argon') }}/js/items.js"></script>
@endpush
