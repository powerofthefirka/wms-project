@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')

    <div class="container-fluid ">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Customer') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                         aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('customers.edit', [ 'customer' => $customer ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('Customer') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" href="{{ route('customers.edit_users', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Users') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_shiphero_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Shiphero credentials') }}</a>
                                </li>
                                <li class="nav-item">
                                    <div class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-2-tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Messenger credentials') }}</div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                        <h6 class="heading-small text-muted mb-4">{{ __('Update Messenger credentials') }}</h6>
                        <div class="table-responsive p-0">
                            <form id="customer-messenger-form" action="{{ route('customers.update_messenger_credentials', [ 'customer' => $customer ]) }}" method="post" >
                                @csrf
                                <div class="card">
                                    <div class="card-body pl-0 pr-0">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-messaging_provider">{{ __('Messaging Provider') }}<span class="currency"></span></label>
                                            <select name="messaging_provider" class="form-control form-control-sm">
                                                <option value=""></option>
                                                @foreach(\App\Components\Messenger\MessengerProvider::getMessengerProviders() as $provider => $providerName)
                                                    <option value="{{ $provider }}" @if ($provider == $customer->messaging_provider)selected="selected"@endif>{{ __($providerName) }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @include('shared.forms.input', [
                                            'name' => 'widget_token',
                                            'label' => __('Widget Token'),
                                            'value' => $customer->widget_token ?? ''
                                        ])
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-12">
                <div class="card transparent text-right">
                    <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-primary" form="customer-messenger-form" >{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

@endsection

