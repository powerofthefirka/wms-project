@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')

    <div class="container-fluid ">
        <div class="row">
            <div class="col col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Edit Customer') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                         aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('customers.edit', [ 'customer' => $customer ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('Customer') }}</a>
                                </li>
                                <li class="nav-item">
                                    <div class="nav-link mb-sm-3 mb-md-0 active  " id="tabs-icons-text-2-tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Users') }}</div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" href="{{ route('customers.edit_shiphero_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Shiphero credentials') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_messenger_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Messenger credentials') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                        <div class="row">
                            <h6 class="heading-small text-muted mb-4 col-8">{{ __('Assigned users') }}</h6>
                            <div class="col-4 text-right">
                                <a href="{{ route('customers.create_user', [ 'customer' => $customer ]) }}" class="btn btn-secondary btn-sm">{{ __('Create New User') }}</a>
                            </div>
                        </div>
                        <div class="table-responsive p-0">
                            <form id="customer-user-form" action="{{route('customers.update_users', [ 'customer' => $customer])}}" method="post" >@csrf</form>
                            <table class="table align-items-center table-flush datatable-basic">
                                <thead class="">
                                <tr>
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Email') }}</th>
                                    <th scope="col">{{ __('Role') }}</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($customer->users as $key => $user)
                                    <tr>
                                        <td>{{ $user->contactInformation->name ?? '' }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <input form="customer-user-form" type="hidden" name="customer_user[{{ $key }}][user_id]" value="{{ $user->id }}" />
                                            <select form="customer-user-form" name="customer_user[{{ $key }}][role_id]" class="form-control form-control-sm" data-toggle="select" data-placeholder="{{$user->pivot->role_id}}">
                                                @foreach($roles as $role)
                                                    @if(!empty($customer->threePl) && !in_array($user->id, $customer->threePl->users->pluck('id')->toArray()) && $role->id == \App\Models\CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)
                                                        @continue;
                                                    @endif
                                                    <option value="{{$role->id}}" {{$user->pivot->role_id === $role->id ? 'selected' : ''}}>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="text-right">
                                            <form action="{{ route('customers.detach_user', ['customer' => $customer, 'user' => $user]) }}" method="post" style="display: inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="button" class="btn btn-danger" data-confirm-action="{{ __('Are you sure you want to delete this user?') }}">
                                                    {{ __('Delete') }}
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @csrf
                        @include('shared.forms.ajaxSelect', [
                            'url' => route('customers.filter_users', [ 'customer' => $customer]),
                            'name' => 'new_user_id',
                            'className' => 'ajax-user-input',
                            'placeholder' => 'Search for a user to add',
                            'label' => 'User search',
                            'form' => "customer-user-form"
                        ])
                        <select form="customer-user-form" name="new_user_role_id" class="form-control form-control-sm" data-toggle="select" data-placeholder="">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col col-12">
                <div class="card transparent text-right">
                    <a href="{{ route('customers.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                    <button type="submit" class="btn btn-primary" form="customer-user-form" >{{ __('Submit') }}</button>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

@endsection

