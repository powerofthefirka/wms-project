@extends('layouts.app', ['title' => __('Customer Management'), 'submenu' => 'customers.menu'])

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Create User') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                        </div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                </div>
                <div class="card-body">
                	<div class="nav-wrapper pt-0">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-1-tab"
                                     aria-controls="tabs-icons-text-1" aria-selected="true" href="{{ route('customers.edit', [ 'customer' => $customer ]) }}"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('Customer') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 " id="tabs-icons-text-2-tab" href="{{ route('customers.edit_users', [ 'customer' => $customer ]) }}" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Users</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_shiphero_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Shiphero credentials') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" href="{{ route('customers.edit_messenger_credentials', [ 'customer' => $customer ]) }}" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>{{ __('Messenger credentials') }}</a>
                            </li>
                        </ul>
                    </div>
                    <form method="post" action="{{ route('customers.store_user', [ 'customer' => $customer ]) }}" autocomplete="off"
                        enctype="multipart/form-data">
                        @csrf

                        <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>
                        <div class="pl-lg-4">
                            @include('shared.forms.contactInformationFields', [
                                'name' => 'contact_information',
                            ])
                            @include('shared.forms.input', [
                                'name' => 'email',
                                'label' => __('User Email'),
                                'type' => 'email'
                            ])
                            @include('shared.forms.input', [
                                'name' => 'password',
                                'label' => __('Password'),
                                'type' => 'password'
                            ])
                            @include('shared.forms.input', [
                                'name' => 'password_confirmation',
                                'label' => __('Confirm Password'),
                                'type' => 'password'
                            ])
                            <input type="hidden" name="customer_id" value="{{$customer->id}}">
                            <input type="hidden" name="user_role_id" value="{{\App\Models\UserRole::ROLE_DEFAULT}}">
			                <div class="col col-12">
			                    <div class="card transparent text-right">
			                        <a href="{{ route('customers.create_user', [ 'customer' => $customer ]) }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
			                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
			                    </div>
			                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>
@endsection
