@extends('layouts.app', ['title' => __('Billing Profiles'), 'submenu' => 'billings.old.menu'])

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Add Billing Profile') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('billing_profiles.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                        <form method="post" action="{{ route('billing_profiles.store') }}" autocomplete="off">
                            @csrf
                            <h6 class="heading-small text-muted mb-4">{{ __('Billing Profile information') }}</h6>
                            <div class="pl-lg-4">
                                @include('billing_profiles.billingProfileInformationFields')
                                @if (auth()->user()->isAdmin())
                                    <div class="form-group">
                                        <label class="form-control-label">{{ __('3PL') }}</label>
                                        <select name="{{'3pl_id'}}" class="form-control" data-toggle="select" data-placeholder="Select 3PL">
                                            @foreach($threePls as $key => $value)
                                                <option value="{{$key}}">
                                                    {{$value}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                @elseif (auth()->user()->isCustomerAdmin())
                                    <input type="hidden" name="3pl_id" value="{{auth()->user()->threePls->first()->id ?? ''}}">
                                @endif
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success ">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
@endsection
