<div class="nav-wrapper">
    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
        @foreach(\App\Models\BillingFee::BILLLING_FEE_TYPES as $type => $info)
            <li class="nav-item m-1 p-0">
                <a class="nav-link p-1 {{$loop->first ? 'active' : ''}}" id="{{$type}}-tab" data-toggle="tab" href="#{{$type}}" role="tab" aria-controls="{{$type}}">
                    {{__($info['title'])}}
                </a>
            </li>
        @endforeach
    </ul>
</div>
<div class="card">
    <div>
        <div class="tab-content" id="myTabContent">
            @foreach(\App\Models\BillingFee::BILLLING_FEE_TYPES as $type => $info)
                <div class="tab-pane fade show {{$loop->first ? 'active' : ''}}" id="{{$type}}" role="tabpanel" aria-labelledby="{{$type}}-tab">
                        @if ($billingProfile->billingFees)
                            <ul class="list-group">

                            @foreach($billingProfile->billingFees as $billingFee)
                                    @if($billingFee['type'] === $type)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <a href="{{route('billing_fees.edit',
                                                ['billingFee' => $billingFee, 'billing_profile' => $billingProfile])}}"
                                            >
                                                {{$billingFee['name']}}
                                            </a>
                                            <span class="badge badge-pill">
                                            <form method="post"
                                                  action="{{ route('billing_fees.destroy', ['billingFee' => $billingFee, 'billingProfile' => $billingProfile]) }}"
                                                  autocomplete="off">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger">{{ __('x') }}</button>
                                            </form>
                                            </span>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                        <br>
                        <a href="{{ route( 'billing_fees.create', ['billing_profile' => $billingProfile, 'type' => $type ]) }}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
                            {{__('Add fee')}}
                        </a>

                        @if($type === \App\Models\BillingFee::SHIPPING_RATES)
                            <div class="btn-group ml-2 mb-1">
                                <a href="#" data-toggle="modal" data-target="#import-shipping-rates" class="btn btn-sm btn-primary import d-inline-block preview-quick-edit">
                                    {{__('Import')}}
                                </a>
                                <a href="{{ route( 'billing_fees.export', ['billing_profile' => $billingProfile, 'type' => $type ]) }}" class="btn btn-sm btn-primary export d-inline-block preview-quick-edit">
                                    {{__('Export')}}
                                </a>
                            </div>

                            @include('billing_fees.shipping_rates.importShippingRates')
                        @endif
                </div>
            @endforeach
        </div>
    </div>
</div>
