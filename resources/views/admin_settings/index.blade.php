@extends('layouts.app', ['title' => __('Admin Settings')])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('user_settings.set_date_time_format') }}"
              autocomplete="off"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6 d-flex align-items-center">
                                    <h3 class="mb-0">{{ __('Admin Settings') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('domain') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="domain">{{ __('Domain') }}</label>
                                <input type="text" name="domain" class=" form-control form-control-sm{{ $errors->has('domain') ? ' is-invalid' : '' }}" id="input-domain">
                            </div>
                            <div class="form-group{{ $errors->has('domain_title') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="domain_title">{{ __('Domain Title') }}</label>
                                <input type="text" name="domain_title" class=" form-control form-control-sm{{ $errors->has('domain_title') ? ' is-invalid' : '' }}" id="input-domain-title">
                            </div>
                            <div class="form-group{{ $errors->has('domain_favicon_url') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-favicon-url">{{ __('Domain Favicon') }}</label>
                                <div class="custom-file">
                                    <input type="file" name="domain_favicon_url" class="custom-file-input{{ $errors->has('domain_favicon_url') ? ' is-invalid' : '' }}" id="input-domain-favicon-url" accept="image/*">
                                    <label class="custom-file-label" for="input-domain-favicon-url">{{ __('Select favicon') }}</label>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('domain_logo_url') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-logo-url">{{ __('Domain Logo') }}</label>
                                <div class="custom-file">
                                    <input type="file" name="domain_logo_url" class="custom-file-input{{ $errors->has('domain_logo_url') ? ' is-invalid' : '' }}" id="input-domain-logo-url" accept="image/*">
                                    <label class="custom-file-label" for="input-domain-logo-url">{{ __('Select logo') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <button class="btn btn-primary text-white">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.footers.auth')
    </div>
@endsection
