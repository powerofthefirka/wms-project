{{--<a data-type="{{ App\Models\Order::SHIPMENT_TYPE_REGULAR_SHIPMENT }}" class="mb-2 col col-12 btn btn-primary text-white shipment-type-button">--}}
{{--    {{ __('Regular Shipment') }}--}}
{{--</a>--}}
{{--<a data-type="{{ App\Models\Order::SHIPMENT_TYPE_PICK_WAREHOUSE_UP }}" class="mb-2 col col-12 btn btn-secondary shipment-type-button">--}}
{{--    {{ __('Pick Warehouse Up') }}--}}
{{--</a>--}}
{{--<a data-type="{{ App\Models\Order::SHIPMENT_TYPE_CUSTOM_LABEL }}" class="col col-12 btn btn-secondary shipment-type-button">--}}
{{--    {{ __('Custom Label') }}--}}
{{--</a>--}}
{{--<div class="d-none">--}}
{{--    @include('shared.forms.input', [--}}
{{--    'name' => 'shipment_type',--}}
{{--    'label' => __('Shipment Type'),--}}
{{--    'value' => $order->shipment_type ?? ''--}}
{{--    ])--}}
{{--</div>--}}
<a href="#" data-toggle="modal" data-target="#shipping-modal" class="mb-2 col col-12 btn btn-primary text-white {{$errors->has('shipping_method_id') ? 'bg-danger' : ''}}">
    {{ __('Add shipping') }}
</a>
