<h2 class="mb-3" style="display: inline-block">{{__('Order')}}:</h2>
@include('alerts.successEditablePreview')
@if($order->status != \App\Models\Order::STATUS_FULFILLED)
    <a href="{{route('orders.editable_preview', ['order' => $order])}}" class="btn btn-sm btn-primary edit d-inline-block preview-quick-edit mb-1 ml-2">
        <i class="nc-icon nc-pencil align-middle"></i> {{__('Edit')}}
    </a>
@endif
<div class="row mb-3">
    <div class="col col-6 pb-2"><strong>{{__('Number')}}</strong><br>{{$order->number}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Shop Name')}}</strong><br>{{ $order->shop_name ?? env('MANUAL_ORDER_SHOP_NAME') }}</div>
    <div class="col col-6 pb-2"><strong>{{__('Name')}}</strong><br>{{$order->customer->contactInformation->name}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Required Shipping Date at')}}</strong><br>{{localized_date($order->required_shipping_date_at)}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Notes')}}</strong><br>{{$order->notes}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Priority')}}</strong><br>{{$order->priority ? 'YES' : 'NO'}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Shipping Price')}}</strong><br>{{with_currency($order->customer, $order->shipping_price)}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Tax')}}</strong><br>{{with_currency($order->customer, $order->tax)}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Shipping Carrier')}}</strong><br>{{ $order->shippingCarrier->name ?? '-' }}</div>
    <div class="col col-6 pb-2"><strong>{{__('Shipping Method')}}</strong><br>{{ $order->shippingMethod->name ?? '-' }}</div>
    <div class="col col-6 pb-2"><strong>{{__('Shipment Type')}}</strong><br>{{ __(\Illuminate\Support\Arr::get(\App\Models\Order::$shipmentTypeValues, $order->shipment_type, '-')) }}</div>
    <div class="col col-6 pb-2"><strong>{{__('Fraud Hold')}}</strong><br>{{$order->fraud_hold ? 'YES' : 'NO'}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Address Hold')}}</strong><br>{{$order->address_hold ? 'YES' : 'NO'}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Payment Hold')}}</strong><br>{{$order->payment_hold ? 'YES' : 'NO'}}</div>
    <div class="col col-6 pb-2"><strong>{{__('Operator Hold')}}</strong><br>{{$order->operator_hold ? 'YES' : 'NO'}}</div>
</div>
<h2 class="mb-3">{{__('Order Items')}}:</h2>
@foreach($order->orderItems as $item)
    <div class="row mb-2">
        <div class="col">
            <strong>{{__('Sku')}}:</strong> {{$item->sku}}<br>
            <strong>{{__('Name')}}:</strong> {{$item->name}}<br>
            <strong>{{__('Quantity')}}:</strong> {{$item->quantity}}<br>
            <strong>{{__('Quantity pending')}}:</strong> {{$item->quantity_pending}}<br>
        </div>
    </div>
    <hr class="m-0 p-0 mb-3">
@endforeach
<div class="row">
    <div class="col text-right">
        <a href="{{route('orders.edit',['order' => $order])}}" class="btn btn-sm btn-primary edit d-inline-block"> {{__('Edit')}} </a>
        @if($order->canCancel())
            <a href="{{route('orders.cancel', ['order' => $order])}}" class="btn btn-sm btn-danger cancel" data-table_id="orders-table">
                {{__('Cancel')}}
            </a>
        @endif
    </div>
</div>
