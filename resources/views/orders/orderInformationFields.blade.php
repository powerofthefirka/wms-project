<div class="row">
    <div class="col-xl-6 sizing shipping_contact_information">
        <h6 class="heading-small text-muted mb-2">{{ __('Shipping information') }}</h6>
        @include('shared.forms.contactInformationFields', [
            'name' => 'shipping_contact_information',
            'contactInformation' => $order->shippingContactInformation ?? ''
        ])
        @if(empty($editMode))
            <input type="checkbox" id="fill-information" checked="checked" name="differentBillingInformation">
            <label for="fill-information">{{ __('Different billing information') }}</label>
        @endif
    </div>
    <div class="col-xl-6 billing_contact_information">
        <h6 class="heading-small text-muted mb-2">{{ __('Billing information') }}</h6>
        @include('shared.forms.contactInformationFields', [
            'name' => 'billing_contact_information',
            'contactInformation' => $order->billingContactInformation ?? ''
        ])
    </div>
</div>
