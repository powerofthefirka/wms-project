<select class="form-control form-control-sm">
    @foreach($carriersAndMethods as $carrierAndMethod)
        <option value="{{ $carrierAndMethod['carrier_id'] }}{{ $carrierAndMethod['method_id'] }}" data-carrier-id="{{ $carrierAndMethod['carrier_id'] }}" data-method-id="{{ $carrierAndMethod['method_id'] }}" data-price="{{ $carrierAndMethod['method_cost'] }}">{{ $carrierAndMethod->carrier_name }} {{ $carrierAndMethod['method_title'] }} - {{ $carrierAndMethod['method_cost'] }}</option>
    @endforeach
</select>
