<div class="modal fade" id="discount-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col d-flex align-items-center p-0">
                    <h5 class="modal-title d-inline ml-0 mr-2" id="address-modal">{{ __('Discount') }}</h5><a class="btn btn-sm discount-percent-button {{ $order->discount_type ?? false ? 'btn-secondary' : 'btn-primary' }}">{{ __('Percent') }}</a><a class="btn btn-sm discount-amount-button {{ $order->discount_type ?? false ?  'btn-primary' : 'btn-secondary' }}">{{ __('Amount') }}</a>
                </div>
                <button type="button" class="close save-address" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-3">
                <fieldset>
                    <div class="row">
                        <div class="col col-12 d-none">
                            @include('shared.forms.input', [
                               'name' => 'discount_type',
                               'label' => __('Discount Type'),
                               'value' => $order->discount_type ?? '0'
                            ])
                            @include('shared.forms.input', [
                               'name' => 'discount',
                               'label' => __('Discount'),
                               'value' => $order->discount ?? '0'
                            ])
                        </div>
                        <div class="col col-12 discount-percent-holder {{ $order->discount_type ?? false ? 'd-none' : '' }}">
                            @include('shared.forms.input', [
                               'name' => 'discount_percent',
                               'label' => __('Discount Percent, %'),
                               'value' => ''
                            ])
                        </div>
                        <div class="col col-12 discount-amount-holder {{ $order->discount_type ?? false ? '' : 'd-none' }}">
                            @include('shared.forms.input', [
                               'name' => 'discount_amount',
                               'label' => __('Discount Amount'),
                               'value' => $order->discount ?? '0',
                            ])
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-primary save-discount" data-dismiss="modal">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>
