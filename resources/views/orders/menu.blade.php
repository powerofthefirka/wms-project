<ul class="submenu">
    <li>
        <ul>
            <li class="nav-item">
                <a href="{{ route('orders.index') }}" class="nav-link {{ current_page(route('orders.index')) }}">
                    <span class="nav-link-text">{{ __('Manage Orders') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('orders.create') }}" class="nav-link {{ current_page(route('orders.create')) }}">
                    <span class="nav-link-text">{{ __('Add Order') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('shipments.index') }}" class="nav-link {{ current_page(route('shipments.index')) }}">
                    <span class="nav-link-text">{{ __('Shipments') }}</span>
                </a>
            </li>
            <li class="nav-item d-none">
                <a href="{{ route('shipping_carriers.index') }}" class="nav-link {{ current_page(route('shipping_carriers.index')) }}">
                    <span class="nav-link-text">{{ __('Shipping Carriers') }}</span>
                </a>
            </li>
            <li class="nav-item d-none">
                <a href="{{ route('shipping_methods.index') }}" class="nav-link {{ current_page(route('shipping_methods.index')) }}">
                    <span class="nav-link-text">{{ __('Shipping Methods') }}</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
