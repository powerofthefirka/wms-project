@extends('layouts.app', ['title' => __('Order Management')])

@section('content')

<div class="container-fluid mt--6">
    <div class="col-12 mt-2">
        @include('alerts.success')
        @include('alerts.errors')
    </div>
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Create Order') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('orders.index') }}" class="btn btn-secondary btn-sm">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('orders.store') }}" autocomplete="off">
                        @csrf
                        <div class="row">
                            @include('orders.orderInformationFields', [
                            'checkboxForFillingInformation' => true
                            ])
                        </div>
                        <div class="pl-lg-4">
                            <div class="table-responsive">
                                <h6 class="heading-small text-muted mb-4">{{ __('Order items') }}</h6>
                                <table class="col-12 table align-items-center table-flush">
                                    <thead class="">
                                    <tr>
                                        <th scope="col">{{ __('Product') }}</th>
                                        <th scope="col">{{ __('Quantity') }}</th>
                                        <th scope="col">{{ __('Shipped Quantity') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="item_container">
                                    @if(count( old()['order_items'] ?? [] ) > 0)
                                    @foreach(old()['order_items'] as $key => $orderItem)
                                    <tr class="order-item-fields">
                                        <td style="white-space: unset">
                                            @include('shared.forms.ajaxSelect', [
                                            'url' => route('orders.filter_products'),
                                            'name' => 'order_items[' . $key . '][product_id]',
                                            'className' => 'ajax-user-input',
                                            'placeholder' => __('Search'),
                                            'labelClass' => 'd-block',
                                            'label' => '',
                                            'default' => [
                                            'id' => $orderItem['product_id'],
                                            'text' =>  ''
                                            ]
                                            ])
                                        </td>
                                        <td>
                                            @include('shared.forms.input', [
                                            'name' => 'order_items[' . $key . '][quantity]',
                                            'type' => 'number',
                                            'label' => '',
                                            'value' => $orderItem['quantity'] ?? 0,
                                            'class' => 'reset_on_delete'
                                            ])
                                        </td>
                                        <td>
                                            @include('shared.forms.input', [
                                            'name' => 'order_items[' . $key . '][quantity_shipped]',
                                            'type' => 'number',
                                            'label' => '',
                                            'value' => $orderItem['quantity_shipped'] ?? 0,
                                            'class' => 'reset_on_delete',
                                            'readOnly' => 'readonly'
                                            ])
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger delete-item">
                                                {{ __('Delete') }}
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr class="order-item-fields">
                                        <td style="white-space: unset">
                                            @include('shared.forms.ajaxSelect', [
                                            'url' => route('orders.filter_products'),
                                            'name' => 'order_items[0][product_id]',
                                            'className' => 'ajax-user-input',
                                            'placeholder' => __('Search'),
                                            'labelClass' => 'd-block',
                                            'label' => ''
                                            ])
                                        </td>
                                        <td>
                                            @include('shared.forms.input', [
                                            'name' => 'order_items[0][quantity]',
                                            'type' => 'number',
                                            'label' => '',
                                            'value' => 1,
                                            'class' => 'reset_on_delete'
                                            ])
                                        </td>
                                        <td>
                                            @include('shared.forms.input', [
                                            'name' => 'order_items[0][quantity_shipped]',
                                            'type' => 'number',
                                            'label' => '',
                                            'value' => 0,
                                            'class' => 'reset_on_delete',
                                            'readOnly' => 'readonly'
                                            ])
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger delete-item">
                                                {{ __('Delete') }}
                                            </button>
                                        </td>
                                    </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <button id="add_item" class="btn btn-primary ">{{ __('Add more items') }}</button>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary ">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script>
    new Order();
</script>
@endpush

