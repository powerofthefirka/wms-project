<div class="modal fade" id="order-reship-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content reship-modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="order-reship-modal">{{ __('Reship items') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id="order-reship" action="{{ route('orders.reship', ['order' => $order, 'orderItems' => $order->orderItems] )}}" autocomplete="off">
                @csrf
                <div class="modal-body p-0">
                    @include('alerts.ajax_error')
                    <fieldset>
                        <div class="table-responsive">
                            <table class="table align-items-center">
                                <thead class="thead-light">
                                    <tr class="order-item-header">
                                        <th scope="col">{{ __('Select') }}</th>
                                        <th scope="col">{{ __('Product') }}</th>
                                        <th scope="col" for="th-currency">{{ __('Unit Price') }}<span></span></th>
                                        <th scope="col" class="text-center">{{ __('Quantity') }}</th>
                                        <th scope="col" for="th-currency">{{ __('Total') }}<span></span></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody id="reship_item_container" class="list">
                                    @foreach($order->orderItems as $key => $orderItem)
                                        @if ($orderItem->quantity_shipped > 0)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="order_items[{{$key}}][order_item_id]" value="{{$orderItem->id}}" />
                                                </td>
                                                <td class="min-width min-width-100">
                                                    {{ __('Name') }}: {{  $product['name'] ?? $orderItem['name'] ?? '' }}<br>
                                                    {{ __('SKU') }}: {{  $product['sku'] ?? $orderItem['sku'] ?? '' }}<br>
                                                </td>
                                                <td class="order-item-price border-0 min-width min-width-100 pr-0">
                                                    <span class="text-body">{{$product['price'] ?? $orderItem['price'] ?? 0}}</span>
                                                </td>
                                                <td class="order-item-quantity border-0 min-width width-15 pr-0">
                                                    @include('shared.forms.input', [
                                                        'name' => 'order_items[' . ($key ?? 0) . '][reship_quantity]',
                                                        'type' => 'number',
                                                        'label' => '',
                                                        'value' => $orderItem['quantity_shipped'] ?? 0,
                                                        'min' => 'min=' . 1,
                                                        'class' => 'order-item-quantity-input'
                                                    ])
                                                </td>
                                                <td class="order-item-total border-0 min-width min-width-100 pr-0">
                                                    <span class="text-body">{{ !empty($orderItem) ? \Illuminate\Support\Arr::get($orderItem, 'price', 0) * \Illuminate\Support\Arr::get($orderItem, 'quantity_shipped', 0) : 0 }}</span>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" id="select-all-items" class="btn btn-success btn-lg active">{{ __('Select all') }}</button>
                    <button type="button" class="btn btn-primary order-reship-btn">{{ __('Reship') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>