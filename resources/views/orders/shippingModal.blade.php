<div class="modal fade" id="shipping-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="shipping-modal">{{ __('Shipping') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                @if($order->customer->threePl->custom_shipping_modal == '1')
                    <div class="text-center">
                        <p>{{$order->customer->threePl->custom_shipping_modal_text}}</p>
                    </div>
                @endif
                <fieldset>
                    <div class="table-responsive shipping-methods-list"></div>
                    <div class="d-none">
                        @include('shared.forms.input', [
                            'name' => 'shipping_carrier_id',
                            'type' => 'number',
                            'label' => 'Shipping Carrier',
                            'value' => $order->shipping_carrier_id ?? old('shipping_carrier_id') ?? '0'
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_method_id',
                            'type' => 'number',
                            'label' => 'Shipping Method',
                            'value' => $order->shipping_method_id ?? old('shipping_method_id') ?? '0'
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_price',
                            'label' => 'Shipping Price',
                            'value' => $order->shipping_price ?? old('shipping_price') ?? '0'
                         ])
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-primary shipping-method-btn" data-dismiss="modal">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>
