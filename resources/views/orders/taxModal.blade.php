<div class="modal fade" id="tax-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="tax-modal">{{ __('Tax') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-3">
                <fieldset>
                    <div class="row">
                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-control-label">{{ __('Tax') }}</label>
                                <input class="form-control form-control-sm" placeholder="Enter tax percent" />
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-primary save">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>

