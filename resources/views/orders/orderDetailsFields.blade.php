<fieldset>
    <div class="row align-items-end">
        <div class="col col-12 col-sm-8">
            <div class="row align-items-end">
                <div class="col col-12 col-sm-6 {{ $order->number ?? false ? 'd-none' : '' }}">
                    <div class="row">
                        <div class="col col-12">
                            @include('shared.forms.input', [
                               'name' => 'number',
                               'label' => __('Number'),
                               'value' => $order->number ?? '',
                               'readOnly' => $order->number ?? false ? 'readonly' : ''
                            ])
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-sm-6 mb-4">
                    @include('orders.orderHoldFields', [
                        'order' => $order ?? false,
                        'disabled' => isset($order) && $order->status == \App\Models\Order::STATUS_FULFILLED ? 'disabled' : ''
                    ])
                </div>
            </div>
        </div>
        <div class="col col-12 col-sm-4">
            <div class="row">
                <div class="col col-7">
                    <p class="m-0"><strong>{{ __('Subtotal') }}</strong></p>
                </div>
                <div class="col col-5 text-right">
                    <p class="m-0 overview-subtotal"><strong>0.00</strong>
                        <b></b>
                    </p>
                </div>
                <div class="col col-7">
                    <p class="m-0"><strong>{{ __('Shipping') }}</strong></p>
                </div>
                <div class="col col-5 text-right">
                    <p class="m-0 overview-shipping"><strong>0.00</strong>
                        <b></b>
                    </p>
                </div>
                <div class="col col-7">
                    @if(isset($order) && $order->status != \App\Models\Order::STATUS_FULFILLED)
                        <a href="#" data-toggle="modal" data-target="#discount-modal" class="m-0 ml--3"><i class="nc-icon nc-pencil align-middle discount-icon"></i>{{ __('Discount') }}</a>
                    @else
                        {{ __('Discount') }}
                    @endif
                </div>
                <div class="col col-5 text-right">
                    <p class="m-0 overview-discount"><strong>0.00</strong>
                        <b></b>
                    </p>
                </div>
                <div class="col col-7">
                    <p class="mb-4"><strong>{{ __('Total') }}</strong></p>
                </div>
                <div class="col col-5 text-right">
                    <p class="mb-4 overview-total"><strong>0.00</strong>
                        <b></b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</fieldset>
