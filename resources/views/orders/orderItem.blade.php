<div class="row order-item align-items-center mb-2 {{ $class ?? 'd-flex' }}">
    <div class="col col-auto order-item-image">
            <img class="avatar rounded" data-default-src="/images/product-placeholder.png" src="/images/product-placeholder.png">
    </div>
    <div class="col col-auto col-lg-3 col-auto pl-0 order-item-name">{{ $title }}</div>
    <div class="col pl-0 order-item-price">{{ $price }}</div>
    <div class="col col-lg-2 col-lg-3 pl-0 order-item-quantity">
        @include('shared.forms.input', [
            'name' => 'order_items[' . $key . '][quantity]',
            'label' => __('Quantity'),
            'type' => 'number',
            'disabled' => 'disabled',
            'class' => 'd-flex',
            'value' => 1,
            'min' => 'min=0'
        ])
    </div>
    <div class="col pl-0 order-item-total"></div>
    <div class="col d-none">
        @include('shared.forms.input', [
            'name' => 'order_items[' . $key . '][product_id]',
            'label' => __('Id'),
            'type' => 'number',
            'hidden' => 'hidden',
            'disabled' => 'disabled',
            'default' => [
                'id' => $product_id ?? 0,
                'text' =>  ''
            ]
        ])
    </div>
    <div class="col col-auto text-right">
        <a class="btn btn-primary btn-sm text-white remove-order-item">
            {{ __('×') }}
        </a>
    </div>
</div>
