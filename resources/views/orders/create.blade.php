@extends('layouts.app', ['title' => __('Order Management'), 'submenu' => 'orders.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post" action="{{ route('orders.store') }}" autocomplete="off" novalidate>
            @csrf
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12">
                    <div class="accordion" id="accordion">
                        <div class="card">
                            <div class="card-header" id="heading-one" data-toggle="collapse" data-target="#collapse-one" aria-expanded="true" aria-controls="collapse-one">
                                <div class="row align-items-center sts sts-fulfilled card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-orders"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 1') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Order Details') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse-one" class="collapse show" aria-labelledby="heading-one" data-parent="#accordion">
                                <div class="card-body pb-0">
                                    <fieldset>
                                        @include('shared.forms.ajaxSelect', [
                                            'url' => route('orders.filter_customers'),
                                            'name' => 'customer_id',
                                            'className' => 'ajax-user-input customer_id',
                                            'placeholder' => __('Search'),
                                            'label' => __('Customer'),
                                            'readonly' => isset($order->customer->id) ? 'true' : null,
                                            'hidden' => session('customer_id') ? 'd-none' : '',
                                            'default' => [
                                                'id' => $order->customer_id ?? session('customer_id') ?? old('customer_id'),
                                                'text' => $order->customer->contactInformation['name'] ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
                                            ]
                                        ])
                                        <div class="row">
                                            <div class="col col-12 mb--2">
                                                @include('shared.forms.ajaxSelect', [
                                                    'url' => route('orders.filter_products',  ['customer' => session('customer_id') ?? old('customer_id')]),
                                                    'name' => 'product_id',
                                                    'required' => 'required',
                                                    'className' => 'ajax-user-input product_id',
                                                    'placeholder' => __('Search'),
                                                    'label' => __('Search for products')
                                                ])
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <fieldset>
                                    <div class="table-responsive">
                                        <table class="table align-items-center">
                                            <thead class="t">
                                            <tr class="order-item-header">
                                                <th scope="col">{{ __('Image') }}</th>
                                                <th scope="col">{{ __('Product') }}</th>
                                                <th scope="col" for="th-currency">{{ __('Unit Price') }}<span></span></th>
                                                <th scope="col">{{ __('Quantity') }}</th>
                                                <th scope="col" for="th-currency">{{ __('Total') }}<span></span></th>
                                                <th scope="col"></th>
                                            </tr>
                                            </thead>
                                            <tbody id="item_container">
                                            @if(count( old()['order_items'] ?? [] ) > 0)
                                                @foreach(old()['order_items'] as $key => $orderItem)
                                                    @php
                                                        $product = App\Models\Product::find($orderItem['product_id'] ?? null);
                                                    @endphp
                                                    @include('orders.productLine')
                                                @endforeach
                                            @else
                                                @include('orders.productLine')
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                                <div class="card-body pt-2">
                                    @include('orders.orderDetailsFields')
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapse-two" aria-controls="collapse-two">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="heading-two"  data-toggle="collapse" data-target="#collapse-two" aria-expanded="false" aria-controls="collapse-two">
                                <div class="row align-items-center sts sts-pending card-sts w-100 border-0">
                                   <div class="col col-auto pr-0">
                                        <i class="automagical-location"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 2') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Address Information') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse-two" class="collapse" aria-labelledby="heading-two" data-parent="#accordion">
                                <div class="card-body">
                                    <fieldset>
                                        @include('orders.orderInformationFields', [
                                            'checkboxForFillingInformation' => true
                                        ])
                                    </fieldset>
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapse-one" aria-controls="collapse-one">
                                                {{ __('Back') }}
                                            </a>
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapse-three" aria-controls="collapse-three">
                                                {{ __('Next') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header collapsed" id="heading-three"  data-toggle="collapse" data-target="#collapse-three" aria-expanded="false" aria-controls="collapse-three">
                                <div class="row align-items-center sts sts-backorder card-sts w-100 border-0">
                                    <div class="col col-auto pr-0">
                                        <i class="automagical-delivery"></i>
                                    </div>
                                    <div class="col col-auto">
                                        <span class="w-100">{{ __('Step 3') }}</span>
                                        <h3 class="mb-0 w-100">{{ __('Shipment') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="collapse-three" class="collapse" aria-labelledby="heading-three" data-parent="#accordion">
                                <div class="card-body p-0">
                                    <fieldset>
                                        <div class="table-responsive shipping-methods-list"></div>
                                        <div class="d-none">
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_carrier_id',
                                                'type' => 'number',
                                                'label' => 'Shipping Carrier',
                                                'value' => $order->shipping_carrier_id ?? old('shipping_carrier_id') ?? '0'
                                            ])
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_method_id',
                                                'type' => 'number',
                                                'label' => 'Shipping Method',
                                                'value' => $order->shipping_method_id ?? old('shipping_method_id') ?? '0'
                                            ])
                                            @include('shared.forms.input', [
                                                'name' => 'shipping_price',
                                                'label' => 'Shipping Price',
                                                'value' => $order->shipping_price ?? old('shipping_price') ?? '0'
                                             ])
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="card-body">
                                    <div class="row text-right">
                                        <div class="col">
                                            <a class="btn btn-secondary" data-toggle="collapse" data-target="#collapse-two" aria-controls="collapse-two">
                                                {{ __('Back') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('orders.packagingModal')
                @include('orders.discountModal')

                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('orders.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new Order();
    </script>
@endpush

