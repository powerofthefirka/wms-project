<div class="custom-control custom-checkbox">
    <input type="hidden" name="fraud_hold" value="0">
    <input type="checkbox" class="custom-control-input" id="fraud_hold" name="fraud_hold" value="1" {{ (old('fraud_hold') || (!empty($order) && $order->fraud_hold)) ? 'checked': '' }} {{$disabled}}>
    <label class="custom-control-label" for="fraud_hold">{{ __('Fraud Hold') }}</label>
</div>
<div class="custom-control custom-checkbox">
    <input type="hidden" name="address_hold" value="0">
    <input type="checkbox" class="custom-control-input" id="address_hold" name="address_hold" value="1" {{ (old('address_hold') || (!empty($order) && $order->address_hold)) ? 'checked': '' }} {{$disabled}}>
    <label class="custom-control-label" for="address_hold">{{ __('Address Hold') }}</label>
</div>
<div class="custom-control custom-checkbox">
    <input type="hidden" name="payment_hold" value="0">
    <input type="checkbox" class="custom-control-input" id="payment_hold" name="payment_hold" value="1" {{ (old('payment_hold') || (!empty($order) && $order->payment_hold)) ? 'checked': '' }} {{$disabled}}>
    <label class="custom-control-label" for="payment_hold">{{ __('Payment Hold') }}</label>
</div>
<div class="custom-control custom-checkbox">
    <input type="hidden" name="operator_hold" value="0">
    <input type="checkbox" class="custom-control-input" id="operator_hold" name="operator_hold" value="1" {{ (old('operator_hold') || (!empty($order) && $order->operator_hold)) ? 'checked': '' }} {{$disabled}}>
    <label class="custom-control-label" for="operator_hold">{{ __('Operator Hold') }}</label>
</div>
