@include('alerts.successEditablePreview')
<form id="preview-form" method="post" action="{{ route('orders.update', ['order' => $order]) }}"
      autocomplete="off"
      enctype="multipart/form-data"
>
    @csrf
    <h3>{{__('Order Number')}}: {{ $order->number ?? '' }}</h3>
    <div class="accordion" id="accordionOrder">
        <div class="card m-0 shadow-none">
            <div class="card-header" id="heading-three" data-toggle="collapse" data-target="#collapse-three" aria-expanded="true" aria-controls="collapse-three">
                <h3 class="mb-0">{{__('Address Information')}}</h3>
            </div>
            <div id="collapse-three" class="collapse show" aria-labelledby="heading-three" data-parent="#accordionOrder">
                <div class="card-body">
                    @include('orders.orderInformationFields', [
                        'checkboxForFillingInformation' => true
                    ])
                </div>
            </div>
        </div>
        <div class="card m-0 shadow-none">
            <div class="card-header" id="heading-two" data-toggle="collapse" data-target="#collapse-two" aria-expanded="false" aria-controls="collapse-two">
                <h3 class="mb-0">{{__('Shipping Method')}}</h3>
            </div>
            <div id="collapse-two" class="collapse" aria-labelledby="heading-two" data-parent="#accordionOrder">
                <div class="card-body">
                    <div class="form-group shipping-methods-dropdown m-0"></div>
                    <div class="d-none">
                        @include('shared.forms.input', [
                            'name' => 'shipping_carrier_id',
                            'type' => 'number',
                            'label' => 'Shipping Carrier',
                            'value' => $order->shipping_carrier_id ?? old('shipping_carrier_id') ?? '0'
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_method_id',
                            'type' => 'number',
                            'label' => 'Shipping Method',
                            'value' => $order->shipping_method_id ?? old('shipping_method_id') ?? '0'
                        ])
                        @include('shared.forms.input', [
                            'name' => 'shipping_price',
                            'label' => 'Shipping Price',
                            'value' => $order->shipping_price ?? old('shipping_price') ?? '0'
                         ])
                    </div>
                </div>
            </div>
        </div>
        <div class="card m-0 shadow-none">
            <div class="card-header" id="heading-four" data-toggle="collapse" data-target="#collapse-four" aria-expanded="false" aria-controls="collapse-four">
                <h3 class="mb-0">{{__('Order Items')}}</h3>
            </div>
            <div id="collapse-four" class="collapse" aria-labelledby="heading-four" data-parent="#accordionOrder">
                <div class="card-body">
                    @foreach($order->orderItems as $key => $orderItem)
                        <input type="hidden" name="order_items[{{$key}}][order_item_id]"
                               value="{{$orderItem['id'] }}">
                        @include('shared.forms.ajaxSelect', [
                            'url' => route('orders.filter_products',  ['customer' => $order->customer->id]),
                            'name' => 'order_items[' . $key . '][product_id]',
                            'className' => 'ajax-user-input',
                            'placeholder' => __('Search'),
                            'labelClass' => 'd-block',
                            'label' => __('Sku'),
                            'default' => [
                                'id' => $orderItem->product->id ?? 0,
                                'text' => 'SKU: ' . $orderItem->sku . ', NAME: ' . $orderItem->name
                                 ]
                            ])
                        <div class="row">
                            <div class="col-4">
                                @include('shared.forms.input', [
                                   'name' => 'order_items[' . $key . '][quantity]',
                                   'type' => 'number',
                                   'label' => __('Quantity'),
                                   'value' => $orderItem->quantity,
                                   'class' => 'reset_on_delete',
                                   'min' => 'min=0',
                                   'step'=> 1,
                                   'oninput' => "oninput=validity.valid||(value='');"
                               ])
                            </div>
                            <div class="col-4">
                                @include('shared.forms.input', [
                                  'name' => 'order_items[' . $key . '][quantity_pending]',
                                  'type' => 'number',
                                  'label' => __('Quantity pending'),
                                  'value' => $orderItem->quantity_pending,
                                  'readOnly' => 'readonly'
                               ])
                            </div>
                            <div class="col-4">
                                @include('shared.forms.input', [
                                  'name' => 'order_items[' . $key . '][quantity_shipped]',
                                  'type' => 'number',
                                  'label' => __('Quantity shipped'),
                                  'value' => $orderItem->quantity_shipped,
                                  'readOnly' => 'readonly'
                               ])
                            </div>
                        </div>
                        <hr class="m-0 p-0 mb-3">
                    @endforeach
                </div>
            </div>
        </div>
        <div class="card shadow-none">
            <div class="card-header" id="heading-one" data-toggle="collapse" data-target="#collapse-one" aria-expanded="false" aria-controls="collapse-one">
                <h3 class="mb-0">{{__('Order Information')}}</h3>
            </div>
            <div id="collapse-one" class="collapse" aria-labelledby="heading-one" data-parent="#accordionOrder">
                <div class="card-body">
                    <div class="d-none">
                        @include('shared.forms.input', [
                            'name' => 'number',
                            'label' => __('Number'),
                            'value' => $order->number ?? '',
                            'readOnly' => $order->number ?? false ? 'readonly' : ''
                            ])
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="shop_name" value="{{ $order->shop_name }}">
                        <span class="form-control-label">{{__('Shop Name')}}:</span> {{ $order->shop_name ?? env('MANUAL_ORDER_SHOP_NAME')}}
                    </div>

                    @include('shared.forms.ajaxSelect', [
                        'url' => route('orders.filter_customers'),
                        'name' => 'customer_id',
                        'className' => 'ajax-user-input customer_id',
                        'placeholder' => __('Search'),
                        'label' => __('Customer Name'),
                        'readonly' => isset($order->customer->id) ? 'true' : null,
                        'hidden' => session('customer_id') ? 'd-none' : '',
                        'default' => [
                            'id' => $order->customer_id ?? session('customer_id') ?? old('customer_id'),
                            'text' => $order->customer->contactInformation['name'] ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
                        ]
                    ])

                    <div class="row">
                        <div class="col-6">
                            @include('shared.forms.input', [
                                'name' => 'required_shipping_date_at',
                                'label' => __("Required Shipping Date at"),
                                'value' => $order->required_shipping_date_at ?? '',
                                'class' => 'date-time-picker'
                            ])
                        </div>
                        <div class="col-6">
                            @include('shared.forms.input', [
                                'name' => 'shipping_date_before_at',
                                'label' => __("Shipping Date Before at"),
                                'value' => $order->shipping_date_before_at ?? '',
                                'class' => 'date-time-picker'
                            ])
                        </div>
                    </div>

                    @include('shared.forms.input', [
                        'name' => 'notes',
                        'label' => __("Notes"),
                        'value' => $order->notes ?? ''
                    ])

                    <div class="mb-3">
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="priority" value="0">
                            <input type="checkbox" class="custom-control-input" id="priority" name="priority" value="1" {{ !empty($order) ? ($order->priority ? 'checked' : '') : '' }}>
                            <label class="custom-control-label" for="priority">{{ __('Priority') }}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            @include('shared.forms.input', [
                                'name' => 'shipping_price',
                                'label' => __("Shipping Price") . label_format(currency($order->customer ?? null)),
                                'value' => $order->shipping_price ?? ''
                            ])
                        </div>
                        <div class="col-6">
                            @include('shared.forms.input', [
                                'name' => 'tax',
                                'label' => __("Tax") . label_format(currency($order->customer ?? null)),
                                'value' => $order->tax ?? ''
                            ])
                        </div>
                    </div>

                    <div class="mb-3">
                        @include('orders.orderHoldFields')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col text-right">
            <button id="preview-submit-button" data-table_id="orders-table" class="btn btn-primary">{{ __('Save') }}</button>
        </div>
    </div>
</form>
