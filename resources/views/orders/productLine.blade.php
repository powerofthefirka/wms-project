<tr class="order-item-fields {{ ($orderItem['product_id'] ?? $orderItem['sku'] ?? false) ? '':'d-none' }}">
    @if($orderItem->id ?? false)
        <input type="hidden" name="order_items[{{$key}}][order_item_id]" value="{{$orderItem->id}}">
    @endif
    <td class="d-none order-item-product-id border-0">
        @include('shared.forms.input', [
            'name' => 'order_items[' . ($key ?? 0) . '][product_id]',
            'type' => 'number',
            'label' => '',
            'value' => $orderItem['product_id'] ?? 0,
            'class' => ''
        ])
    </td>
    <td class="order-item-image border-0 pr-0">
        <img class="avatar rounded" onerror="this.src='/images/product-placeholder.png'" data-default-src="/images/product-placeholder.png"
             src="
                @if ($product ?? false)
             {{$product->productImages->first()->source ?? \App\Models\Order::DEFAULT_IMAGE}}
             @elseif ($orderItem ?? false)
             {{$orderItem->product->productImages[0]->source ?? \App\Models\Order::DEFAULT_IMAGE}}
             @else
             {{\App\Models\Order::DEFAULT_IMAGE}}
             @endif
                 "
        >
    </td>
    <td class="order-item-name min-width min-width-100 custom-box">
        {{ __('Name') }}: {{  $product['name'] ?? $orderItem['name'] ?? '' }}<br>
        {{ __('SKU') }}: {{  $product['sku'] ?? $orderItem['sku'] ?? '' }}<br>
    </td>
    <td class="order-item-price border-0 min-width min-width-100 pr-0">
        <span class="text-body">{{$product['price'] ?? $orderItem['price'] ?? 0}}</span>

    </td>
    <td class="order-item-quantity border-0 min-width min-width-100 pr-0">
        @include('shared.forms.input', [
            'name' => 'order_items[' . ($key ?? 0) . '][quantity]',
            'type' => 'number',
            'label' => '',
            'value' => $orderItem['quantity'] ?? 0,
            'min' => 'min=' . (($orderItem['quantity'] ?? true) ? 1:0),
            'step'=> 1,
            'oninput' => "oninput=validity.valid||(value='');",
            'class' => 'order-item-quantity-input',
            'disabled' => isset($order) && $order->status == \App\Models\Order::STATUS_FULFILLED ? 'disabled' : ''
        ])
    </td>
    @if($orderItem->id ?? false)
    <td class="order-item-quantity_pending border-0 min-width min-width-100 pr-0">
        <span class="text-body">{{$orderItem['quantity_pending'] ?? 0}}</span>
    </td>
    @endif
    <td class="order-item-total border-0 min-width min-width-100 pr-0">
        <span class="text-body">{{ !empty($orderItem) ? \Illuminate\Support\Arr::get($orderItem, 'price', 0) * \Illuminate\Support\Arr::get($orderItem, 'quantity', 0) : 0 }}</span>
    </td>
    <td class="border-0">
        @if(isset($order) && $order->status != \App\Models\Order::STATUS_FULFILLED)
            <a class="btn btn-primary btn-sm text-white remove-item-button remove-order-item">
                {{ __('×') }}
            </a>
        @endif
    </td>
</tr>
