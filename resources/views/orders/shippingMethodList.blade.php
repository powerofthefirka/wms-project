<div class="table-responsive shipping-methods-list">
    <table class="table align-items-center m-0 shipping-methods-table">
        <tbody>
        @foreach($carriersAndMethods as $carrierAndMethod)
            <tr>
                <td class="border-0">
                    <img class="avatar rounded" src="{{ $carrierAndMethod->image->source ?? "/images/product-placeholder.png"}}">
                </td>
                <td class="border-0 max-width-150 overflow-hidden {{$customShippingModal == '1' ? 'd-none' : ''}}">
                    {{ $carrierAndMethod->carrier_name }}
                </td>
                <td class="border-0 @if ($customShippingModal == '1') max-width-350 @else max-width-150 @endif overflow-hidden" width>
                    @if(is_numeric($carrierAndMethod['method_title']))
                        {{ \App\Models\ShippingMethod::$customShippingMethodTitle[$carrierAndMethod['method_title']] ?? $carrierAndMethod['method_title'] }}
                    @else
                        {{ $carrierAndMethod['method_title'] }}
                    @endif
                </td>
                <td class="border-0">
                    {{ $carrierAndMethod['cost_with_currency'] }}
                </td>
                <td class="border-0">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="shipping-method-{{ $carrierAndMethod['carrier_id'] }}-{{ $carrierAndMethod['method_id'] }}">
                        <label class="custom-control-label" for="shipping-method-{{ $carrierAndMethod['carrier_id'] }}-{{ $carrierAndMethod['method_id'] }}" data-carrier-id="{{ $carrierAndMethod['carrier_id'] }}" data-method-id="{{ $carrierAndMethod['method_id'] }}" data-price="{{ $carrierAndMethod['method_cost'] }}"></label>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
