<div class="modal fade" id="address-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="address-modal">{{ __('Address') }}</h5>
                <button type="button" class="close save-address" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-3">
                <fieldset>
                    @include('orders.orderInformationFields', [
                        'checkboxForFillingInformation' => true,
                        'editMode' => $editMode ?? false
                    ])
                </fieldset>
            </div>
            <div class="modal-footer text-right">
                <button type="button" class="btn btn-primary save-address" data-dismiss="modal">{{ __('Save') }}</button>
            </div>
        </div>
    </div>
</div>
