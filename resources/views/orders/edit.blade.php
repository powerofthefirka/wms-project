@extends('layouts.app', ['title' => __('Edit Order'), 'submenu' => 'orders.menu'])

@section('content')
    <div class="container-fluid">
        <form method="post"
              action="{{ route('orders.update', ['order' => $order, 'orderItems' => $order->orderItems] )}}"
              autocomplete="off" novalidate id="order-form">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col col-12 {{ session($key ?? 'status') || !$errors->isEmpty() ? 'd-block' : 'd-none' }}">
                    <div class="card transparent">
                        @include('alerts.success')
                        @include('alerts.errors')
                        @include('alerts.ajax_error')
                    </div>
                </div>
                <div class="col col-12 col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-3 d-flex align-items-center">
                                    <p class="mb-0">{{ $order->number }}</p>
                                </div>
                                <div class="col-3 text-right">
                                    <input type="hidden" name="shop_name" value="{{ $order->shop_name }}">
                                    <p class="mb-0">{{ $order->shop_name ?? env('MANUAL_ORDER_SHOP_NAME') }}</p>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-end">
                                    <h3 class="mb-0">{{ __('Order Details') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-0">
                            <div class="form-group">
                                <label for="status" class="form-control-label">Order status</label>
                                <select class="form-control form-control-sm" id="status" name="status" {{$order->status == 'fulfilled' ? 'disabled' : ''}}>
                                    @foreach(\App\Models\Order::getStatuses() as $key => $value)
                                        <option value="{{$key}}" {{$order->status == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <fieldset>
                                @include('shared.forms.ajaxSelect', [
                                    'url' => route('orders.filter_customers'),
                                    'name' => 'customer_id',
                                    'className' => 'ajax-user-input customer_id',
                                    'placeholder' => __('Search'),
                                    'label' => __('Customer'),
                                    'readonly' => isset($order->customer->id) ? 'true' : null,
                                    'hidden' => session('customer_id') ? 'd-none' : '',
                                    'default' => [
                                        'id' => $order->customer_id ?? session('customer_id') ?? old('customer_id'),
                                        'text' => $order->customer->contactInformation['name'] ?? app()->user->getCustomers()->where('id', session('customer_id'))->first()->contactInformation->name ?? ''
                                    ]
                                ])
                                <div class="row">
                                    <div class="col col-12 mb--2">
                                        @if($order->status != \App\Models\Order::STATUS_FULFILLED)
                                            @include('shared.forms.ajaxSelect', [
                                                'url' => route('orders.filter_products',  ['customer' => $order->customer->id]),
                                                'name' => 'product_id',
                                                'required' => 'required',
                                                'className' => 'ajax-user-input product_id',
                                                'placeholder' => __('Search'),
                                                'label' => __('Search to add product')
                                            ])
                                        @endif
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <fieldset>
                            <div class="table-responsive">
                                <table class="table align-items-center">
                                    <thead class="">
                                        <tr class="order-item-header">
                                            <th scope="col">{{ __('Image') }}</th>
                                            <th scope="col">{{ __('Product') }}</th>
                                            <th scope="col" for="th-currency">{{ __('Unit Price') }}<span></span></th>
                                            <th scope="col">{{ __('Quantity') }}</th>
                                            <th scope="col">{{ __('Quantity Pending') }}</th>
                                            <th scope="col" for="th-currency">{{ __('Total') }}<span></span></th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="item_container">
                                    @if(count($order->orderItems ?? [] ) > 0)
                                        @foreach($order->orderItems as $key => $orderItem)
                                            @include('orders.productLine')
                                        @endforeach
                                    @else
                                        @include('orders.productLine')
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                        <div class="card-body pt-2 pb-0">
                            @include('orders.orderDetailsFields', [
                                '$order' => $order
                            ])
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="notes">Notes</label>
                                <textarea class="form-control" name="notes" id="notes" rows="3"
                                          form="order-form" {{$order->status == \App\Models\Order::STATUS_FULFILLED ? 'disabled' : ''}}>{!! $order->notes !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Address Information') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body pb-0">
                            <fieldset>
                                <div class="row address-information">
                                    <div class="col col-12 shipping-address d-none">
                                        <h4>{{ __('Shipping Address') }}</h4>
                                        <p></p>
                                    </div>
                                    <div class="col col-12 billing-address d-none" data-is-differ-billing="{{ $order->isDifferentBillingInformation() }}">
                                        <h4>{{ __('Billing Address') }}</h4>
                                        <p></p>
                                    </div>
                                </div>
                                @if($order->status != \App\Models\Order::STATUS_FULFILLED)
                                    <p><a class="" href="#" data-toggle="modal" data-target="#address-modal">{{ __('Edit shipping address') }}</a></p>
                                @endif
                            </fieldset>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">{{ __('Shipment') }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @foreach($order->shipments as $shipment)

                                <h4>Shipment number <a href="{{route('shipments.items_shipped', $shipment->id)}}">{{$shipment->number}}</a></h4>

                                <p><strong>Trancking code: </strong> {{$shipment->tracking_code}}</p>

                                @if($shipment->tracking_url)
                                    <p><strong>Trancking url: </strong> {{$shipment->tracking_url}}</p>
                                @endif

                                {{--                                @if(!$loop->last)--}}
                                <hr>
                                {{--                                @endif--}}

                            @endforeach
                            <fieldset>
                                @include('orders.shipmentTypeFields', [
                                    '$order' => $order
                                ])
                            </fieldset>
                        </div>
                    </div>
                    @if ($order->shippedItems->count() > 0 && $order->status != \App\Models\Order::STATUS_FULFILLED)
                        <div class="card">
                            <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h3 class="mb-0">{{ __('Reship Order') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <fieldset>
                                    <a href="#" data-toggle="modal" data-target="#order-reship-modal" class="mb-2 col col-12 btn btn-primary text-white">
                                        {{ __('Reship order') }}
                                    </a>
                                </fieldset>
                            </div>
                        </div>
                    @endif
                </div>
                @include('orders.addressModal', ['editMode' => true])
                @include('orders.packagingModal')
                @include('orders.shippingModal')
                @include('orders.discountModal')
                <input type="hidden" id="custom-shipping-modal" value="{{$order->customer->threePl->custom_shipping_modal}}">
                <div class="col col-12">
                    <div class="card transparent text-right">
                        <a href="{{ route('orders.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                        <button type="submit" class="btn btn-primary" {{$order->status == \App\Models\Order::STATUS_FULFILLED ? 'disabled' : ''}}>{{ __('Submit') }}</button>
                    </div>
                </div>
            </div>
        </form>
        @include('orders.orderReshipModal')
        @if(auth()->user()->isAdmin())
            @include('orders.changeLog', [ 'filter' => false ])
        @endif
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        new Order();
    </script>
@endpush



