# 3PL-Dashboard

Source code for project 3PL-Dashboard

## Starting Docker containers

Docker configuration is stored on 3pl-dashboard-docker directory. On the first startup you need to copy env-example to .env file on 3pl-dashboard-docker directory:

    cp 3pl-dashboard-docker/env-example 3pl-dashboard-docker/.env

To start the containers run the following commands:

    cd 3pl-dashboard-docker
    docker-compose up -d


To run `artisan` commands first you need to log into workspace container. Make sure you are still in the 3pl-dashboard-docker directory and then run the following command:

    docker-compose exec -u laradock workspace bash

Then you can start using the `artisan` commands

When needed run `artisan geo:json` for updating cities table hierarchy;
 
## Running the project for the first time

After docker is up and running and you are logged into the workspace container you need to execute the following commands:

    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan migrate --seed
    php artisan passport:keys
    php artisan passport:client --personal
    chmod 600 storage/oauth*
    npm i
    
    npm run dev
    
After everything is installed:
    create in storage geo directory and newCountries.json file.
Then run:
    
       artisan geo:download
       artisan migrate
       artisan geo:seed
    

## Generating API keys

Go to http://localhost and login. After logging in go to http://localhost/profile and generate API key

## Using API

Use [postman](https://www.getpostman.com/) or similar tools to send API requests. Use the API tokens as Bearer authentication tokens.

## Testing APIs

The phpunit.xml file contains environment variables that will define how the application runs when testing. To change these settings, an .env.testing file can be created with a new database.

To execute the test cases run the following command:

    ./vendor/bin/phpunit 
 
