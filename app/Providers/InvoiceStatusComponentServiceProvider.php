<?php

namespace App\Providers;

use App\Components\InvoiceStatusComponent;
use Illuminate\Support\ServiceProvider;

class InvoiceStatusComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('invoiceStatus', function () {
            return new InvoiceStatusComponent();
        });
    }

    public function provides()
    {
        return [
            'invoiceStatus'
        ];
    }
}
