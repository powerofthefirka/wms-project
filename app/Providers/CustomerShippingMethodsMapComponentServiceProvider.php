<?php

namespace App\Providers;

use App\Components\CustomerShippingMethodsMapComponent;
use Illuminate\Support\ServiceProvider;

class CustomerShippingMethodsMapComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('customerShippingMethodsMap', function () {
            return new CustomerShippingMethodsMapComponent();
        });
    }

    public function provides()
    {
        return [
            'customerShippingMethodsMap'
        ];
    }
}
