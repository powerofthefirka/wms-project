<?php

namespace App\Providers;

use App\Components\Messenger\MessengerProvider;
use Illuminate\Support\ServiceProvider;

class MessengerComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('messengerChat', function () {
            return new MessengerProvider();
        });
    }

    public function provides()
    {
        return [
            'messengerChat'
        ];
    }
}
