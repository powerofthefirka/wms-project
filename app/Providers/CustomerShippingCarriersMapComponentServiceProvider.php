<?php

namespace App\Providers;

use App\Components\CustomerShippingCarriersMapComponent;
use Illuminate\Support\ServiceProvider;

class CustomerShippingCarriersMapComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('customerShippingCarriersMap', function () {
            return new CustomerShippingCarriersMapComponent();
        });
    }

    public function provides()
    {
        return [
            'customerShippingCarriersMap'
        ];
    }
}
