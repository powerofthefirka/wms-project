<?php

namespace App\Providers;

use App\Components\WebhookComponent;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Models\Webhook;

class WebhookComponentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('webhook_unique_store', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if (strpos($attribute, ".") !== false) {            
                $pieces = explode(".", $attribute);

                if (!isset($data[$pieces[0]]['customer_id']))
                    return true;
                $customerId = $data[$pieces[0]]['customer_id'];

            } else {
                $customerId = $data['customer_id'] ?? null;
            }
            
            $webhook = Webhook::where('name', $value)->where('customer_id', $customerId)->first();
            
            return !$webhook;
        });
    }

    public function register()
    {
        $this->app->singleton('webhook', function () {
            return new WebhookComponent();
        });
    }

    public function provides()
    {
        return [
            'webhook'
        ];
    }
}
