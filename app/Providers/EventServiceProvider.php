<?php

namespace App\Providers;

use App\Events\ObjectChange\RelatedObjectDelete;
use App\Events\OrderShipped;
use App\Listeners\UpdateOrder;
use App\Events\PurchaseOrderSaved;
use App\Listeners\ProductPoUpdate;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\ObjectChange\UpdateObject;
use App\Events\ObjectChange\RelationUpdated;
use App\Listeners\ObjectChange\ChangeObject;
use App\Events\ObjectChange\RelatedObjectSaved;
use App\Listeners\ObjectChange\ChangeRelations;
use App\Listeners\ObjectChange\ChangeRelatedObject;
use App\Listeners\ObjectChange\DeleteRelatedObject;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Webhook\SendWebhook' => [
            'App\Listeners\Webhook\SendWebhook',
        ],
        OrderShipped::class => [
            UpdateOrder::class,
        ],
        PurchaseOrderSaved::class => [
            ProductPoUpdate::class,
        ],
        UpdateObject::class => [
            ChangeObject::class,
        ],
        RelationUpdated::class => [
            ChangeRelations::class,
        ],
        RelatedObjectSaved::class => [
            ChangeRelatedObject::class,
        ],
        RelatedObjectDelete::class => [
            DeleteRelatedObject::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
