<?php

namespace App\Providers;

use App\Components\ThreePlComponent;
use Illuminate\Support\ServiceProvider;

class ThreePlComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('threePl', function () {
            return new ThreePlComponent();
        });
    }

    public function provides()
    {
        return [
            'threePl'
        ];
    }
}
