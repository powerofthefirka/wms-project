<?php

namespace App\Providers;

use App\Components\Shipping\Providers\DummyShippingProvider;
use Illuminate\Support\ServiceProvider;

class DummyShippingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('dummyShipping', function () {
            return new DummyShippingProvider();
        });
    }

    public function provides()
    {
        return [
            'dummyShipping'
        ];
    }
}
