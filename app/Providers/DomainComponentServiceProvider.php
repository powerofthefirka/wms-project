<?php

namespace App\Providers;

use App\Components\DomainComponent;
use Illuminate\Support\ServiceProvider;

class DomainComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('domain', function () {
            return new DomainComponent();
        });
    }

    public function provides()
    {
        return [
            'domain'
        ];
    }
}
