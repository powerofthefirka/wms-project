<?php

namespace App\Providers;

use App\Components\InventoryChangeComponent;
use Illuminate\Support\ServiceProvider;

class InventoryChangeComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('inventoryChange', function () {
            return new InventoryChangeComponent();
        });
    }

    public function provides()
    {
        return [
            'inventoryChange'
        ];
    }
}
