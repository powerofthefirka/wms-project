<?php

namespace App\Providers;

use App\Components\BillingFeeComponent;
use Illuminate\Support\ServiceProvider;

class BillingFeeComponentServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('billingFee', function () {
            return new BillingFeeComponent();
        });
    }

    public function provides()
    {
        return [
            'BillingFee'
        ];
    }
}
