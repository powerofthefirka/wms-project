<?php

namespace App\Providers;

use App\Components\InvoiceLineComponent;
use Illuminate\Support\ServiceProvider;

class InvoiceLineComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('invoiceLine', function () {
            return new InvoiceLineComponent();
        });
    }

    public function provides()
    {
        return [
            'invoiceLine'
        ];
    }
}
