<?php

namespace App\Providers;

use App\Models\BillingFee;
use App\Models\BillingProfile;
use App\Models\Domain;
use App\Models\InvoiceStatus;
use App\Models\UserRole;
use App\Models\User;
use App\Models\Supplier;
use App\Models\Order;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\Warehouse;
use App\Models\Location;
use App\Models\Product;
use App\Models\Webhook;
use App\Models\InventoryLog;
use App\Models\Customer;
use App\Models\WebshipperCredential;
use App\Models\CustomerShippingCarriersMap;
use App\Models\CustomerShippingMethodsMap;
use App\Models\InventoryChange;
use App\Models\Invoice;
use App\Models\InvoiceLine;
use App\Models\ShippingBox;
use App\Models\ThreePl;
use App\Policies\BillingFeePolicy;
use App\Models\LocationType;
use App\Policies\DomainPolicy;
use App\Policies\InvoicePolicy;
use App\Policies\InvoiceLinePolicy;
use App\Policies\BillingProfilePolicy;
use App\Policies\InvoiceStatusPolicy;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Policies\UserRolePolicy;
use App\Policies\UserPolicy;
use App\Policies\SupplierPolicy;
use App\Policies\OrderPolicy;
use App\Policies\PurchaseOrderPolicy;
use App\Policies\ReturnsPolicy;
use App\Policies\WarehousePolicy;
use App\Policies\LocationPolicy;
use App\Policies\ProductPolicy;
use App\Policies\WebhookPolicy;
use App\Policies\InventoryLogPolicy;
use App\Policies\CustomerPolicy;
use App\Policies\WebshipperCredentialPolicy;
use App\Policies\CustomerShippingCarriersMapPolicy;
use App\Policies\CustomerShippingMethodsMapPolicy;
use App\Policies\InventoryChangePolicy;
use App\Policies\ShippingBoxPolicy;
use App\Policies\ShippingCarrierPolicy;
use App\Policies\ShippingMethodPolicy;
use App\Policies\LocationTypePolicy;
use App\Policies\ThreePlPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        UserRole::class => UserRolePolicy::class,
        Supplier::class => SupplierPolicy::class,
        Order::class => OrderPolicy::class,
        PurchaseOrder::class => PurchaseOrderPolicy::class,
        Return_::class => ReturnsPolicy::class,
        Warehouse::class => WarehousePolicy::class,
        Location::class => LocationPolicy::class,
        Product::class => ProductPolicy::class,
        Webhook::class => WebhookPolicy::class,
        InventoryLog::class => InventoryLogPolicy::class,
        Customer::class => CustomerPolicy::class,
        WebshipperCredential::class => WebshipperCredentialPolicy::class,
        CustomerShippingCarriersMap::class => CustomerShippingCarriersMapPolicy::class,
        CustomerShippingMethodsMap::class => CustomerShippingMethodsMapPolicy::class,
        ShippingCarrier::class => ShippingCarrierPolicy::class,
        ShippingBox::class => ShippingBoxPolicy::class,
        ShippingMethod::class => ShippingMethodPolicy::class,
        BillingProfile::class => BillingProfilePolicy::class,
        Invoice::class => InvoicePolicy::class,
        InvoiceLine::class => InvoiceLinePolicy::class,
        InvoiceStatus::class => InvoiceStatusPolicy::class,
        LocationType::class => LocationTypePolicy::class,
        BillingFee::class => BillingFeePolicy::class,
        InventoryChange::class => InventoryChangePolicy::class,
        Domain::class => DomainPolicy::class,
        ThreePl::class => ThreePlPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::personalAccessClientId(1);
    }
}
