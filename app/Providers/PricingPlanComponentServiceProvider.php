<?php

namespace App\Providers;

use App\Components\PricingPlanComponent;
use Illuminate\Support\ServiceProvider;

class PricingPlanComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('pricingPlan', function () {
            return new PricingPlanComponent();
        });
    }

    public function provides()
    {
        return [
            'pricingPlan'
        ];
    }
}
