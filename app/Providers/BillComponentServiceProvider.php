<?php

namespace App\Providers;

use App\Components\BillComponent;
use Illuminate\Support\ServiceProvider;

class BillComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('bill', function () {
            return new BillComponent();
        });
    }

    public function provides()
    {
        return [
            'bill'
        ];
    }
}
