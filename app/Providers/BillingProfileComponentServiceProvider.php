<?php

namespace App\Providers;

use App\Components\BillingProfileComponent;
use Illuminate\Support\ServiceProvider;

class BillingProfileComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('billingProfile', function () {
            return new BillingProfileComponent();
        });
    }

    public function provides()
    {
        return [
            'billingProfile'
        ];
    }
}
