<?php

namespace App\Providers;

use App\Http\Routing\ResourceRegistrar;
use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        /**
         * Validate ExistsInDatabase or 0/null
         */
        Validator::extend(
            'exists_or_null',
            function ($attribute, $value, $parameters) {
                if ($value == 0 || is_null($value)) {
                    return true;
                } else {
                    $validator = Validator::make(['id' => $value], [
                        'id' => 'exists:' . implode(",", $parameters)
                    ]);

                    if ($validator->fails()) {
                        return false;
                    }

                    return true;
                }
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $resourceRegistrar = new ResourceRegistrar($this->app['router']);

        $this->app->bind('Illuminate\Routing\ResourceRegistrar', function () use ($resourceRegistrar) {
            return $resourceRegistrar;
        });
    }
}
