<?php

namespace App\Exports;

use App\Models\Customer;
use App\Models\Product;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProductExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = 'products.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $allCustomers = [];

    private $fields = [];
    private $titles = [];

    public function __construct($data = [])
    {
        list($this->fields, $this->titles) = titles_and_fields($data, Product::EXPORTABLE_COLUMNS);

        $allCustomers = Customer::all();
        $this->allCustomers = $allCustomers->keyBy('id');
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        if (!empty($this->allCustomers[$item['customer_id']])) {
            $item['customer_id'] = $this->allCustomers[$item['customer_id']]->contactInformation->name;
        }

        return $item;
    }

    public function collection()
    {
        $collection = Product::select($this->fields)->whereIn('customer_id', Auth()->user()->customerIds())->get()->toArray();

        $collection = collect(array_values($collection));

        return $collection;
    }
}
