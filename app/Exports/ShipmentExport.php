<?php

namespace App\Exports;

use App\Models\Shipment;
use App\Models\ShipmentItem;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ShipmentExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = 'shipments.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $parentIds;
    private $fields = [];
    private $titles = [];

    public function __construct($data = [])
    {
        list($this->fields, $this->titles) = titles_and_fields($data, Shipment::EXPORTABLE_COLUMNS);

        $returns = Shipment::whereHas('order', function ($query) {
            $query->where('customer_id', Auth()->user()->customerIds());
        });

        $this->parentIds = $returns->pluck('id');
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        return $item;
    }

    public function collection()
    {
        $collection = ShipmentItem::select($this->fields)
            ->whereIn('shipment_items.shipment_id', $this->parentIds)
            ->leftJoin('shipments', 'shipment_items.shipment_id', '=', 'shipments.id')
            ->join('orders', 'shipments.order_id', '=', 'orders.id')
            ->leftJoin('order_items', 'shipment_items.order_item_id', '=', 'order_items.id')
            ->join('contact_informations AS shipping_contact_information', 'orders.shipping_contact_information_id', '=', 'shipping_contact_information.id')
            ->leftJoin('countries as shipping_country', 'shipping_contact_information.country_id', '=', 'shipping_country.id')
            ->get()
            ->toArray();

        $collection = collect(array_values($collection));

        return $collection;
    }
}
