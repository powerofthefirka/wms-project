<?php

namespace App\Exports;

use App\Models\Customer;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class PurchaseOrderExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = 'purchase_orders.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $allCustomers = [];

    private $parentIds;
    private $fields = [];
    private $titles = [];

    public function __construct($data = [])
    {
        list($this->fields, $this->titles) = titles_and_fields($data, PurchaseOrder::EXPORTABLE_COLUMNS);

        $orders = PurchaseOrder::whereIn('customer_id', Auth()->user()->customerIds())->get();

        $this->parentIds = $orders->pluck('id');
        $allCustomers = Customer::all();
        $this->allCustomers = $allCustomers->keyBy('id');
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        if (!empty($this->allCustomers[$item['customer_id']])) {
            $item['customer_id'] = $this->allCustomers[$item['customer_id']]->contactInformation->name;
        }

        return $item;
    }

    public function collection()
    {
        $collection = PurchaseOrderItem::select($this->fields)
            ->whereIn('purchase_order_items.purchase_order_id', $this->parentIds)
            ->leftJoin('purchase_orders', 'purchase_order_items.purchase_order_id', '=', 'purchase_orders.id')
            ->leftJoin('suppliers', 'purchase_orders.supplier_id', '=', 'suppliers.id')
            ->leftJoin('contact_informations AS supplier_contact_information', function ($join) {
                $join->on('suppliers.id', '=', 'supplier_contact_information.object_id')
                    ->where('supplier_contact_information.object_type', Supplier::class);
            })
            ->leftJoin('warehouses', 'purchase_orders.warehouse_id', '=', 'warehouses.id')
            ->leftJoin('contact_informations AS warehouse_contact_information', function ($join) {
                $join->on('warehouses.id', '=', 'warehouse_contact_information.object_id')
                    ->where('warehouse_contact_information.object_type', Warehouse::class);
            })
            ->get()
            ->toArray();

        $collection = collect(array_values($collection));

        return $collection;
    }
}
