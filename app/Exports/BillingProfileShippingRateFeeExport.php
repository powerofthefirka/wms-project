<?php

namespace App\Exports;

use App\Models\BillingFee;
use App\Models\BillingProfile;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class BillingProfileShippingRateFeeExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = '_export.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $billingFees = [];

    private $fields = [];
    private $titles = [];

    public function __construct(BillingProfile $billingProfile, $type)
    {
        $this->fileName = $type . $this->fileName;

        $shippingRateFields = [
            'name' => 'Name',
            'description' => 'Description',
            'shipping_carrier' => 'Shipping Carrier',
            'shipping_method' => 'Shipping Method',
            'countries' => 'Countries',
            'weight_from' => 'Weight From',
            'weight_to' => 'Weight To',
            'weight_unit' => 'Weight Unit',
            'price' => 'Price',
            'shipping_zone' => 'Shipping Zone',
        ];

        list($this->fields, $this->titles) = [array_keys($shippingRateFields), $shippingRateFields];

        $this->billingFees = BillingFee::where('billing_profile_id', $billingProfile->id)->get();
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        return $item;
    }

    public function collection()
    {
        $collection = [];
        foreach ($this->billingFees as $fee) {
            if ($fee->type !== BillingFee::SHIPPING_RATES) {
                continue;
            }

            $settings = Arr::get($fee, 'settings', []);

            $shippingCarrier = ShippingCarrier::find(Arr::get($settings, 'shipping_carrier_id'))->name ?? '';

            $shippingMethods = ShippingMethod::whereIn('id', Arr::get($settings, 'shipping_method', []))->get()->pluck('name');
            $shippingMethods = empty($shippingMethods) ? '' : implode(', ', $shippingMethods->toArray());

            foreach (Arr::get($settings, 'shipping_zones', []) as $shippingZone) {
                $countries = Country::whereIn('id', Arr::get($shippingZone, 'countries', []))->get()->pluck('title');
                $countries = empty($countries) ? '' : implode(', ', $countries->toArray());
                $zoneName = Arr::get($shippingZone, 'name', '');

                foreach (Arr::get($shippingZone, 'shipping_prices', []) as $shippingPrice) {
                    $collection[] = [
                        'name' => $fee->name,
                        'description' => Arr::get($settings, 'description', 0),
                        'shipping_carrier' => $shippingCarrier,
                        'shipping_method' => $shippingMethods,
                        'countries' => $countries,
                        'weight_from' => Arr::get($shippingPrice, 'weight_from', 0),
                        'weight_to' => Arr::get($shippingPrice, 'weight_to', 0),
                        'weight_unit' => Arr::get($shippingPrice, 'weight_unit', 'g'),
                        'price' => Arr::get($shippingPrice, 'price', 0),
                        'shipping_zone' => $zoneName
                    ];
                }
            }
        }

        $collection = collect(array_values($collection));

        return $collection;
    }
}
