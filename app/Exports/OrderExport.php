<?php

namespace App\Exports;

use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrderExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = 'orders.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $allCustomers = [];

    private $parentIds;
    private $fields = [];
    private $titles = [];

    public function __construct($data = [])
    {
        list($this->fields, $this->titles) = titles_and_fields($data, Order::EXPORTABLE_COLUMNS);

        $orders = Order::whereIn('customer_id', Auth()->user()->customerIds())->get();

        $this->parentIds = $orders->pluck('id');
        $allCustomers = Customer::all();
        $this->allCustomers = $allCustomers->keyBy('id');
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        if (!empty($this->allCustomers[$item['customer_id']])) {
            $item['customer_id'] = $this->allCustomers[$item['customer_id']]->contactInformation->name;
        }

        return $item;
    }

    public function collection()
    {
        $collection = OrderItem::select($this->fields)
            ->whereIn('order_items.order_id', $this->parentIds)
            ->leftJoin('orders', 'order_items.order_id', '=', 'orders.id')
            ->leftJoin('contact_informations as shipping_contact_information', 'orders.shipping_contact_information_id', '=', 'shipping_contact_information.id')
            ->leftJoin('countries as shipping_country', 'shipping_contact_information.country_id', '=', 'shipping_country.id')
            ->leftJoin('contact_informations as billing_contact_information', 'orders.billing_contact_information_id', '=', 'billing_contact_information.id')
            ->leftJoin('countries as billing_country', 'billing_contact_information.country_id', '=', 'billing_country.id')
            ->get()
            ->toArray();

        $collection = collect(array_values($collection));

        return $collection;
    }
}
