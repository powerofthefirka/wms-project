<?php

namespace App\Exports;

use App\Models\Customer;
use App\Models\Return_;
use App\Models\ReturnItem;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReturnExport implements FromCollection, Responsable, WithMapping, WithHeadings
{
    use Exportable;

    private $fileName = 'returns.csv';

    private $writerType = Excel::CSV;

    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    private $allCustomers = [];

    private $parentIds;
    private $fields = [];
    private $titles = [];

    public function __construct($data = [])
    {
        list($this->fields, $this->titles) = titles_and_fields($data, Return_::EXPORTABLE_COLUMNS);

        $returns = Return_::whereHas('order', function ($query) {
            $query->where('customer_id', Auth()->user()->customerIds());
        });

        $this->parentIds = $returns->pluck('id');
        $allCustomers = Customer::all();
        $this->allCustomers = $allCustomers->keyBy('id');
    }

    public function headings(): array
    {
        return $this->titles;
    }

    public function map($item): array
    {
        if (!empty($this->allCustomers[$item['customer_id']])) {
            $item['customer_id'] = $this->allCustomers[$item['customer_id']]->contactInformation->name;
        }

        return $item;
    }

    public function collection()
    {
        $collection = ReturnItem::select($this->fields)
            ->whereIn('return_items.return_id', $this->parentIds)
            ->leftJoin('returns', 'return_items.return_id', '=', 'returns.id')
            ->join('orders', 'returns.order_id', '=', 'orders.id')
            ->leftJoin('order_items', 'return_items.order_item_id', '=', 'order_items.id')
            ->leftJoin('warehouses', 'returns.warehouse_id', '=', 'warehouses.id')
            ->leftJoin('contact_informations AS warehouse_contact_information', function ($join) {
                $join->on('warehouses.id', '=', 'warehouse_contact_information.object_id')
                    ->where('warehouse_contact_information.object_type', Warehouse::class);
            })
            ->get()
            ->toArray();

        $collection = collect(array_values($collection));

        return $collection;
    }
}
