<?php

namespace App\Http\Requests\Supplier;

use App\Http\Requests\FormRequest;
use App\Rules\SupplierUniqueNameCheck;
use App\Http\Requests\SupplierProduct\StoreRequest as SupplierProductRequest;
use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules($includeContactInformationRules = true)
    {
        $customerId = static::getInputField('customer_id');

        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'currency' => [
                'sometimes',
            ],
            'internal_note' => [
                'sometimes',
            ],
            'default_po_note' => [
                'sometimes',
            ],
            'supplier_product.suppliers' => [
                'sometimes',
                'exists:suppliers,id,deleted_at,NULL'

            ],
            'supplier_product.products' => [
                'sometimes',

            ]
        ];

        if ($includeContactInformationRules) {
            $contactInformationRules = ContactInformationStoreRequest::prefixedValidationRules('contact_information.');

            foreach ($contactInformationRules as $attribute => $contactInformationRule) {
                if ($attribute == 'contact_information.name') {
                    $contactInformationRules[$attribute][] = $customerId ? new SupplierUniqueNameCheck($customerId) : false;
                    continue;
                }

                if ($attribute == 'contact_information.email') {
                    continue;
                }

                foreach ($contactInformationRule as $key => $rule) {
                    if ($rule == 'required') {
                        $contactInformationRules[$attribute][$key] = 'nullable';
                    }
                }
            }

            $rules = array_merge_recursive($rules, $contactInformationRules);
        }

        return $rules;
    }
}
