<?php

namespace App\Http\Requests\Supplier;

use App\Http\Requests\FormRequest;
use App\Rules\SupplierUniqueNameCheck;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:suppliers,id,deleted_at,NULL'];

        $rules['outside_request'] = ['sometimes'];
        $rules['customer_id'] = ['sometimes'];

        foreach ($rules['contact_information.name'] as $key => $rule) {
            if (is_a($rule, SupplierUniqueNameCheck::class)) {
                unset($rules['contact_information.name'][$key]);
                break;
            }
        }

        return $rules;
    }
}
