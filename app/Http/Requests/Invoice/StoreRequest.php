<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'date' => [
                'sometimes'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'billing_profile_id' => [
                'required',
                'exists:billing_profiles,id,deleted_at,NULL'
            ],
            'direct_url' => [
                'sometimes'
            ],
            'invoice_status_id' => [
                'sometimes'
            ]
        ];
    }
}
