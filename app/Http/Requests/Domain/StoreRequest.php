<?php

namespace App\Http\Requests\Domain;

use App\Http\Requests\FormRequest;
use App\Http\Requests\Image\ImageRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            '3pl_id' => [
                'required',
                'exists:3pls,id,deleted_at,NULL'
            ],
            'domain' => [
                'required',
            ],
            'title' => [
                'required'
            ],
            'primary_color' => [
                'sometimes',
            ],
            'logo_id' => [
                'sometimes'
            ],
            'favicon_id' => [
                'sometimes'
            ]
        ];

        return $rules;
    }
}
