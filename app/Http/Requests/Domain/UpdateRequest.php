<?php

namespace App\Http\Requests\Domain;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        return $rules;
    }
}
