<?php

namespace App\Http\Requests\Domain;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required',
            ]
        ];

        return $rules;
    }
}
