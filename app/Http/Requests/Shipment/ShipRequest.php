<?php

namespace App\Http\Requests\Shipment;

use App\Http\Requests\FormRequest;
use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Http\Requests\Package\StoreRequest as PackageStoreRequest;
use App\Models\Shipment;
use App\Rules\UniqueNumberCheck;

class ShipRequest extends FormRequest
{
    public static function validationRules()
    {
        $orderId = static::getInputField('order_id');

        $rules = array_merge(
            [
                'order_id' => [
                    'sometimes',
                    'exists:orders,id,deleted_at,NULL'
                ],
                'tracking_code' => [
                    'sometimes'
                ],
                'tracking_url' => [
                    'sometimes',
                    'url'
                ],
                'webshipper_carrier_id' => [
                    'sometimes',
                    'integer'
                ],
                'webshipper_shipping_method' => [
                    'sometimes'
                ],
                'number' => [
                    "sometimes",
                    $orderId ? new UniqueNumberCheck(Shipment::class, $orderId) : false
                ],
                'shipped_at' => [
                    'sometimes'
                ]
            ],
            ShipItemRequest::prefixedValidationRules('order_items.*.'),
            PackageStoreRequest::prefixedValidationRules('shipping_boxes.*.')
        );

        $contactInformationRules = ContactInformationStoreRequest::prefixedValidationRules('contact_information.*.');

        foreach ($contactInformationRules as $attribute => $contactInformationRule) {
            foreach ($contactInformationRule as $key => $rule) {
                if ($rule == 'required') {
                    $contactInformationRules[$attribute][$key] = 'nullable';
                }

                if (strpos($rule, 'email') !== false) {
                    unset($contactInformationRules[$attribute][$key]);
                    break;
                }
            }
        }

        $rules = array_merge_recursive($rules, $contactInformationRules);

        return $rules;
    }
}
