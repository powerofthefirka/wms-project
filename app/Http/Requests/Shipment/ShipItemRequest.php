<?php

namespace App\Http\Requests\Shipment;

use App\Http\Requests\FormRequest;

class ShipItemRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'order_item_id' => [
                'required',
                'validate_order_item'
            ],
            'location_id' => [
                'sometimes',
                'exists:locations,id,deleted_at,NULL'
            ],
            'quantity' => [
                'required',
                'numeric',
            ]
        ];

        return $rules;
    }
}
