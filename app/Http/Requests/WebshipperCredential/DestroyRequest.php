<?php

namespace App\Http\Requests\WebshipperCredential;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 
                'exists:webshipper_credentials,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
