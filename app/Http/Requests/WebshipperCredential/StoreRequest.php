<?php

namespace App\Http\Requests\WebshipperCredential;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'api_base_url' => [
                'required',
                'url'
            ],
            'api_key' => [
                'required'
            ]
        ];
    }
}
