<?php

namespace App\Http\Requests\Webhook;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Arr;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['exists:webhooks,id,deleted_at,NULL'];
        
        foreach ($rules['name'] as $key => $rule) {
            if (strpos($rule, 'webhook_unique_store') !== false) {
                unset($rules['name'][$key]);
                break;
            }
        }
        
        return $rules;
    }
}
