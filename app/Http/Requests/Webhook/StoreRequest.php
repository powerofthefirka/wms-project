<?php

namespace App\Http\Requests\Webhook;

use App\Http\Requests\FormRequest;
use App\Rules\WebhookObjectTypeRule;
use Illuminate\Validation\Rule;
use App\Models\Webhook;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'customer_id' => [
                'required', 
                'exists:customers,id,deleted_at,NULL'
            ],
            'name' => [
                'required',
                'webhook_unique_store'
            ],
            'object_type' => [
                'required',
                new WebhookObjectTypeRule()
            ],
            'operation' => [
                'required',
                Rule::in([
                    Webhook::OPERATION_TYPE_STORE,
                    Webhook::OPERATION_TYPE_UPDATE,
                    Webhook::OPERATION_TYPE_DESTROY
                ]),
            ],
            'url' => [
                'required',
                'url'
            ],
            'secret_key' => [
                'required'
            ]
        ];
    }
}
