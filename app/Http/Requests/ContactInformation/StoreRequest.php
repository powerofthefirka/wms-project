<?php

namespace App\Http\Requests\ContactInformation;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'required'
            ],
            'company_name' => [
                'sometimes'
            ],
            'address' => [
                'required'
            ],
            'address2' => [
                'sometimes'
            ],
            'zip' => [
                'required'
            ],
            'country_id' => [
                'required'
            ],
            'city' => [
                'required'
            ],
            'email' => [
                'sometimes'
            ],
            'phone' => [
                'required'
            ],
            'country_name' => [
                'sometimes'
            ],
            'shared_folder' => [
                'sometimes'
            ]
        ];
    }
}
