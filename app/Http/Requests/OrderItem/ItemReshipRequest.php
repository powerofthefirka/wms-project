<?php

namespace App\Http\Requests\OrderItem;

use App\Http\Requests\FormRequest;

class ItemReshipRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'order_item_id' => [
                'sometimes',
                'exists:order_items,id,deleted_at,NULL'
            ],
            'reship_quantity' => [
                'required',
                'numeric',
                'min:1'
            ]
        ];
    }
}
