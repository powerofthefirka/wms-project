<?php

namespace App\Http\Requests\OrderItem;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'product_id' => [
                'nullable',
                'exists_or_null:products,id,deleted_at,NULL'
            ],
            'quantity' => [
                'required',
                'numeric',
                'min:0',
            ],
            'quantity_backordered' => [
                'sometimes',
                'numeric',
                'min:0',
            ],
            'sku' => [
                'sometimes'
            ],
            'quantity_shipped' => [
                'sometimes',
                'numeric'
            ],
            'name' => [
                'sometimes'
            ],
            'price' => [
                'sometimes'
            ],
            'customs_price' => [
                'sometimes'
            ],
            'barcode' => [
                'sometimes'
            ],
            'quantity_pending' => [
                'sometimes'
            ],
            'weight' => [
                'sometimes'
            ],
            'width' => [
                'sometimes'
            ],
            'height' => [
                'sometimes'
            ],
            'length' => [
                'sometimes'
            ],
            'country_of_origin' => [
                'sometimes'
            ],
            'hs_code' => [
                'nullable',
                'string'
            ],
            'product_type' => [
                'sometimes'
            ],
            'replacement_value' => [
                'sometimes'
            ]
        ];
    }
}
