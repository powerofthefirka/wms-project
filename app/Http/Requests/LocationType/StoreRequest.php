<?php

namespace App\Http\Requests\LocationType;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'required'
            ],
            'customer_id' => [
                'sometimes',
                'exists:customers,id,deleted_at,NULL'
            ],
            'wms_id' => [
                'required'
            ],
            'daily_storage_cost' => [
                'sometimes',
                'numeric',
                'min:0'
            ],
            '3pl_id' => [
                'required',
                'exists:3pls,id,deleted_at,NULL'
            ]
        ];
    }
}
