<?php

namespace App\Http\Requests\LocationType;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['required', 'exists:location_types,id,deleted_at,NULL'];

        return $rules;
    }
}
