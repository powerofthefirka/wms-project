<?php

namespace App\Http\Requests\PricingPlan;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'required'
            ],
            'billing_period' => [
                'sometimes',
            ],
            'monthly_price_for_yearly_billing_period' => [
                'sometimes',
            ],
            'monthly_price_for_monthly_billing_period' => [
                'sometimes',
            ],
            'initial_fee' => [
                'sometimes',
            ],
            'three_pl_billing_fee' => [
                'sometimes',
            ],
            'number_of_monthly_orders' => [
                'sometimes',
            ],
            'price_per_additional_order' => [
                'sometimes',
            ]
        ];
    }
}
