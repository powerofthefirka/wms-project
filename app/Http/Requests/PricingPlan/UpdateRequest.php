<?php

namespace App\Http\Requests\PricingPlan;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:pricing_plans,id,deleted_at,NULL'];

        return $rules;
    }
}
