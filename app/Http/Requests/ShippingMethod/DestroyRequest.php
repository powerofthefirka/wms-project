<?php

namespace App\Http\Requests\ShippingMethod;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 
                'exists:shipping_methods,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
