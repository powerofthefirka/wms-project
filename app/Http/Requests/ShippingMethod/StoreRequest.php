<?php

namespace App\Http\Requests\ShippingMethod;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [            
            'name' => [
                'required'
            ],
            'shipping_carrier_id' => [
                'required',
                'exists:shipping_carriers,id,deleted_at,NULL'
            ],
            'cost' => [
                'sometimes'
            ],
            'wms_id' => [
                'required'
            ],
            'title' => [
                'required'
            ]
        ];
    }
}
