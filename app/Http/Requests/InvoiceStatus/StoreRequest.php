<?php

namespace App\Http\Requests\InvoiceStatus;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'required',
                'min:3'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ]
        ];
    }
}
