<?php

namespace App\Http\Requests\InvoiceStatus;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required',
                'exists:invoice_statuses,id,deleted_at,NULL'
            ],
            'name' => [
                'required',
                'min:3'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
