<?php

namespace App\Http\Requests\PurchaseOrderItem;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'product_id' => [
                'nullable',
                'exists_or_null:products,id,deleted_at,NULL'
            ],
            'quantity' => [
                'sometimes',
                'numeric',
                'min:0'
            ],
            'purchase_order_id' => [
                'sometimes'
            ],
            'quantity_received' => [
                'sometimes'
            ],
            'quantity_sell_ahead' => [
                'sometimes'
            ],
            'unit_price' => [
                'sometimes'
            ],
            'price' => [
                'sometimes'
            ],
            'customs_price' => [
                'sometimes'
            ],
            'sku' => [
                'sometimes'
            ],
            'name' => [
                'sometimes'
            ],
            'product_name' => [
                'sometimes'
            ],
            'barcode' => [
                'sometimes'
            ],
            'weight' => [
                'sometimes'
            ],
            'width' => [
                'sometimes'
            ],
            'height' => [
                'sometimes'
            ],
            'length' => [
                'sometimes'
            ],
            'country_of_origin' => [
                'sometimes'
            ],
            'hs_code' => [
                'nullable',
                'string'
            ],
            'product_type' => [
                'sometimes'
            ],
            'replacement_value' => [
                'sometimes'
            ]
        ];
    }
}
