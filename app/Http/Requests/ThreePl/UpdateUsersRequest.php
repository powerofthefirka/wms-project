<?php

namespace App\Http\Requests\ThreePl;

use App\Http\Requests\FormRequest;

class UpdateUsersRequest extends FormRequest
{
    public static function validationRules($includeContactInformationRules = true)
    {
        $rules = [
            'new_user_id' => [
                'sometimes',
                'exists:users,id'
            ]
        ];

        return $rules;
    }
}
