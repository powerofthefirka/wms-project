<?php

namespace App\Http\Requests\ThreePl;

use App\Http\Requests\FormRequest;
use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Rules\ThreePlUniqueNameCheck;

class StoreRequest extends FormRequest
{
    public static function validationRules($includeContactInformationRules = true)
    {
        $rules = [
            'custom_shipping_modal' => [
                'sometimes',
            ],
            'custom_shipping_modal_text' => [
                'sometimes',
            ],
            'scheduling_link' => [
                'sometimes',
            ]
        ];

        if ($includeContactInformationRules) {
            $contactInformationRules = ContactInformationStoreRequest::prefixedValidationRules('contact_information.');

            foreach ($contactInformationRules as $attribute => $contactInformationRule) {
                if ($attribute == 'contact_information.name') {
                    $contactInformationRules[$attribute][] = new ThreePlUniqueNameCheck();
                    continue;
                }
            }

            $rules = array_merge_recursive($rules, $contactInformationRules);
        }

        return $rules;
    }
}
