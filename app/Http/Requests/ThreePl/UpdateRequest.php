<?php

namespace App\Http\Requests\ThreePl;

use App\Http\Requests\FormRequest;
use App\Rules\ThreePlUniqueNameCheck;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:3pls,id,deleted_at,NULL'];

        foreach ($rules['contact_information.name'] as $key => $rule) {
            if (is_a($rule, ThreePlUniqueNameCheck::class)) {
                unset($rules['contact_information.name'][$key]);
                break;
            }
        }

        return $rules;
    }
}
