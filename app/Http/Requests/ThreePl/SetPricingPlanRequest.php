<?php

namespace App\Http\Requests\ThreePl;

use App\Http\Requests\FormRequest;

class SetPricingPlanRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            '3pl_id' => [
                'required',
                'exists:3pls,id,deleted_at,NULL'
            ],
            'pricing_plan_name' => [
                'required',
            ],
            'billing_period' => [
                'sometimes',
            ],
            'monthly_price' => [
                'sometimes',
            ],
            'initial_fee' => [
                'sometimes',
            ],
            'three_pl_billing_fee' => [
                'sometimes',
            ],
            'number_of_monthly_orders' => [
                'sometimes',
            ],
            'price_per_additional_order' => [
                'sometimes',
            ],
            'invoice_start_date' => [
                'sometimes'
            ]
        ];

        return $rules;
    }
}
