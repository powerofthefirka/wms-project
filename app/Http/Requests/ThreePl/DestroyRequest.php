<?php

namespace App\Http\Requests\ThreePl;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'sometimes'
            ],
        ];

        return $rules;
    }
}
