<?php

namespace App\Http\Requests\ProductProfile;

use App\Http\Requests\FormRequest;

class EditRequest extends FormRequest
{
    public static function validationRules()
    {
        return array_merge(
            [
                'name' => [
                    'required',
                ],
            ]
        );
    }
}
