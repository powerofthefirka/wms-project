<?php

namespace App\Http\Requests\Return_;

use App\Http\Requests\ReturnItem\StoreRequest as ReturnItemStoreRequest;
use App\Http\Requests\FormRequest;
use App\Models\Return_;
use App\Rules\UniqueNumberCheck;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $orderId = static::getInputField('order_id');

        $rules = [
            'order_id' => [
                'required',
                'exists:orders,id,deleted_at,NULL'
            ],
            'number' => [
                'sometimes',
                $orderId ? new UniqueNumberCheck(Return_::class, $orderId) : false
            ],
            'requested_at' => [
                'sometimes',
                'date_format:Y-m-d H:i:s'
            ],
            'reason' => [
                'sometimes',
                'string'
            ],
            'shipping_carrier_id' => [
                'nullable',
                'exists:shipping_carriers,id,deleted_at,NULL'
            ],
            'shipping_carrier_wms_id' => [
                "sometimes"
            ],
            'shipping_method_id' => [
                'nullable',
                'exists:shipping_methods,id,deleted_at,NULL'
            ],
            'shipping_method_wms_id' => [
                "sometimes"
            ],
            'shipping_carrier_title' => [
                'sometimes'
            ],
            'shipping_method_title' => [
                'sometimes'
            ],
            'warehouse_id' => [
                'sometimes', 'exists:warehouses,id,deleted_at,NULL'
            ],
            'status' => [
                'sometimes'
            ],
            'weight' => [
                'sometimes'
            ],
            'width' => [
                'sometimes'
            ],
            'height' => [
                'sometimes'
            ],
            'length' => [
                'sometimes'
            ],
            'create_label' => [
                'sometimes',
                'boolean'
            ]
        ];

        $rules = array_merge_recursive($rules, ReturnItemStoreRequest::prefixedValidationRules('return_items.*.'));

        return $rules;
    }
}
