<?php

namespace App\Http\Requests\Return_;

use App\Http\Requests\FormRequest;

class ReceiveRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'return_item_id' => [
                'required', 
                'validate_return_item'
            ],
            'location_id' => [
                'required', 
                'exists:locations,id,deleted_at,NULL'
            ],
            'quantity_received' => [
                'required',
                'numeric'
            ]
        ];

        return $rules;
    }
}
