<?php

namespace App\Http\Requests\Return_;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 
                'exists:returns,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
