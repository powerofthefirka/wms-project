<?php

namespace App\Http\Requests\Return_;

use App\Http\Requests\FormRequest;
use App\Rules\UniqueNumberCheck;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['sometimes', 'exists:returns,id,deleted_at,NULL'];

        foreach ($rules['number'] as $key => $rule) {
            if (is_a($rule, UniqueNumberCheck::class)) {
                unset($rules['number'][$key]);
                break;
            }
        }

        foreach ($rules['order_id'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['order_id'][$key]);
                break;
            }
        }
        $rules['order_id'][] = 'sometimes';

        foreach ($rules['return_items.*.order_item_id'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['return_items.*.order_item_id'][$key]);
            }
        }
        $rules['return_items.*.order_item_id'][] = 'nullable';


        foreach ($rules['return_items.*.quantity'] as $key => $rule) {
            if (is_string($rule)) {
                if (strpos($rule, 'min:0') !== false) {
                    unset($rules['return_items.*.quantity'][$key]);
                }
                if (strpos($rule, 'not_in:0') !== false) {
                    unset($rules['return_items.*.quantity'][$key]);
                }
            }
        }

        $rules['return_items.*.return_item_id'] = ['sometimes', 'exists:return_items,id,deleted_at,NULL'];

        $rules['outside_request'] = ['sometimes'];

        return $rules;
    }
}
