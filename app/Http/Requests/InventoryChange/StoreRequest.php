<?php

namespace App\Http\Requests\InventoryChange;

use App\Http\Requests\FormRequest;
use App\Http\Requests\Location\StoreRequest as LocationStoreRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'product_id' => [
                'required',
                'exists:products,id,deleted_at,NULL'
            ],
            'location_id' => [
                'nullable',
                'exists:locations,id,deleted_at,NULL'
            ],
            'change_in_on_hand' => [
                'sometimes'
            ],
            'previous_on_hand' => [
                'sometimes'
            ],
            'changed_at' => [
                'sometimes',
                'date_format:Y-m-d H:i:s'
            ]
        ];

        $locationRules = LocationStoreRequest::prefixedValidationRules('location.');

        foreach ($locationRules as $attribute => $locationRulesItem) {
            foreach ($locationRulesItem as $key => $rule) {
                if ($rule == 'required') {
                    $locationRules[$attribute][$key] = 'sometimes';
                }
            }
        }

        $rules = array_merge_recursive($rules, $locationRules);

        return $rules;
    }
}
