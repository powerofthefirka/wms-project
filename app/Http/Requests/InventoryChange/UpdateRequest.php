<?php

namespace App\Http\Requests\InventoryChange;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['required', 'exists:inventory_changes,id,deleted_at,NULL'];

        return $rules;
    }
}
