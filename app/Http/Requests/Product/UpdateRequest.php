<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();
        $rules['customer_id'][] = 'sometimes';

        return $rules;
    }
}
