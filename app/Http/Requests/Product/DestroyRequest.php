<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'sku' => [
                'required',
            ],
            'customer_id' => [
                'exists:customers,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
