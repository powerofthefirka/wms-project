<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;
use App\Http\Requests\Image\ImageRequest;
use App\Http\Requests\SupplierProduct\StoreRequest as SupplierProductRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'sku' => [
                'required',
            ],
            'name' => [
                'sometimes',
            ],
            'price' => [
                'sometimes'
            ],
            'customs_price' => [
                'sometimes'
            ],
            'weight' => [
                'sometimes'
            ],
            'width' => [
                'sometimes'
            ],
            'height' => [
                'sometimes'
            ],
            'length' => [
                'sometimes'
            ],
            'country_of_origin' => [
                'sometimes'
            ],
            'hs_code' => [
                'nullable',
                'string'
            ],
            'notes' => [
                'sometimes'
            ],
            'quantity_on_hand' => [
                'sometimes'
            ],
            'quantity_allocated' => [
                'sometimes'
            ],
            'quantity_available' => [
                'sometimes'
            ],
            'quantity_backordered' => [
                'sometimes'
            ],
            'outside_request' => [
                'sometimes'
            ],
            'product_type' => [
                'sometimes'
            ],
            'barcode' => [
                'sometimes'
            ],
            'replacement_value' => [
                'sometimes'
            ],
            'is_kit' => [
                'sometimes'
            ],
            'supplier_product.suppliers' => [
                'sometimes'
            ],
            'supplier_product.products' => [
                'sometimes',
                'exists:products,id,deleted_at,NULL'
            ]
        ];

        $rules = array_merge_recursive($rules, ImageRequest::prefixedValidationRules('product_images.*.'));

        return $rules;
    }
}
