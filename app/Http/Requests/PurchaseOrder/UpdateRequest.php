<?php

namespace App\Http\Requests\PurchaseOrder;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:purchase_orders,id,deleted_at,NULL'];

        foreach ($rules['warehouse_id'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['warehouse_id'][$key]);
                break;
            }
        }

        foreach ($rules['purchase_order_items.*.product_id'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['purchase_order_items.*.product_id'][$key]);
            }
        }
        $rules['purchase_order_items.*.product_id'][] = 'nullable';

        foreach ($rules['purchase_order_items.*.quantity'] as $key => $rule) {
            if (strpos($rule, 'min:0') !== false) {
                unset($rules['purchase_order_items.*.quantity'][$key]);
            }
            if (strpos($rule, 'not_in:0') !== false) {
                unset($rules['purchase_order_items.*.quantity'][$key]);
            }
        }

        $rules['purchase_order_items.*.purchase_order_item_id'] = ['sometimes', 'exists:purchase_order_items,id'];

        $rules['number'][] = 'sometimes';
        $rules['customer_id'][] = 'sometimes';
        $rules['warehouse_id'][] = 'sometimes';

        $rules['outside_request'][] = 'sometimes';
        $rules['custom_id'][] = 'sometimes';
        $rules['hours_receiving'][] = ['sometimes', 'integer'];

        return $rules;
    }
}
