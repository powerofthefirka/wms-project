<?php

namespace App\Http\Requests\PurchaseOrder;

use App\Http\Requests\PurchaseOrderItem\StoreRequest as PurchaseOrderItemStoreRequest;
use App\Http\Requests\FormRequest;
use App\Models\Supplier;
use App\Models\Warehouse;
use App\Rules\BelongsToCustomer;
use Illuminate\Support\Arr;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $customerId = static::getInputField('customer_id');

        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'warehouse_id' => [
                'required',
                'exists:warehouses,id,deleted_at,NULL',
                $customerId ? new BelongsToCustomer(Warehouse::class, $customerId) : false
            ],
            'supplier_id' => [
                'sometimes',
                'nullable',
                'exists:suppliers,id,deleted_at,NULL',
                $customerId ? new BelongsToCustomer(Supplier::class, $customerId) : false
            ],
            'number' => [
                'required',
                'string'
            ],
            'ordered_at' => [
                'sometimes',
                'date_format:Y-m-d'
            ],
            'expected_at' => [
                'sometimes',
                'date_format:Y-m-d'
            ],
            'delivered_at' => [
                'sometimes',
                'date_format:Y-m-d'
            ],
            'notes' => [
                'sometimes',
                'nullable',
                'string'
            ],
            'priority' => [
                'sometimes',
                'integer'
            ],
            'shipping_carrier_id' => [
                'sometimes',
                'exists:shipping_carriers,id,deleted_at,NULL'
            ],
            'shipping_carrier_wms_id' => [
                "sometimes"
            ],
            'shipping_method_id' => [
                'sometimes',
                'exists:shipping_methods,id,deleted_at,NULL'
            ],
            'shipping_method_wms_id' => [
                "sometimes"
            ],
            'shipping_price' => [
                'sometimes'
            ],
            'discount' => [
                'sometimes'
            ],
            'tax' => [
                'sometimes'
            ],
            'shipping_carrier_title' => [
                'sometimes'
            ],
            'shipping_method_title' => [
                'sometimes'
            ],
            'tracking_number' => [
                'sometimes'
            ],
            'tracking_url' => [
                'sometimes'
            ],
            'status' => [
                'sometimes'
            ],
            'is_cancelled' => [
                'sometimes',
                'boolean'
            ],
            'hours_receiving' => [
                'sometimes',
                'integer'
            ]
        ];

        $rules = array_merge_recursive($rules, PurchaseOrderItemStoreRequest::prefixedValidationRules('purchase_order_items.*.'));

        return $rules;
    }
}
