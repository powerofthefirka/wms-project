<?php

namespace App\Http\Requests\PurchaseOrder;

use App\Http\Requests\FormRequest;
use App\Models\Supplier;
use App\Models\Warehouse;
use App\Rules\BelongsToCustomer;

class ImportRequest extends FormRequest
{
    public static function validationRules()
    {
        $customerId = static::getInputField('customer_id');

        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'warehouse_id' => [
                'required',
                'exists:warehouses,id,deleted_at,NULL',
                $customerId ? new BelongsToCustomer(Warehouse::class, $customerId) : false
            ],
            'supplier_id' => [
                'sometimes',
                'nullable',
                'exists:suppliers,id,deleted_at,NULL',
                $customerId ? new BelongsToCustomer(Supplier::class, $customerId) : false
            ],
            'number' => [
                'required',
            ],
            'ordered_at' => [
                'sometimes',
                'nullable',
                'date_format:Y-m-d H:i:s'
            ],
            'expected_at' => [
                'sometimes',
                'nullable',
                'date_format:Y-m-d H:i:s'
            ],
            'delivered_at' => [
                'sometimes',
                'nullable',
                'date_format:Y-m-d H:i:s'
            ],
            'notes' => [
                'sometimes',
                'nullable',
                'string'
            ],
            'priority' => [
                'sometimes',
                'integer'
            ],
            'shipping_carrier_id' => [
                'sometimes',
                'exists:shipping_carriers,id,deleted_at,NULL'
            ],
            'shipping_method_id' => [
                'sometimes',
                'exists:shipping_methods,id,deleted_at,NULL'
            ],
            'sku' => [
                'required',
                'exists:products'
            ],
        ];

        return $rules;
    }
}
