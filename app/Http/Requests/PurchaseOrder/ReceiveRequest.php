<?php

namespace App\Http\Requests\PurchaseOrder;

use App\Http\Requests\FormRequest;

class ReceiveRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'purchase_order_item_id' => [
                'required', 
                'validate_purchase_order_item'
            ],
            'location_id' => [
                'required', 
                'exists:locations,id,deleted_at,NULL'
            ],
            'quantity_received' => [
                'required',
                'numeric'
            ]
        ];

        return $rules;
    }
}
