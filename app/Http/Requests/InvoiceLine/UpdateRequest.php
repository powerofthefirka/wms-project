<?php

namespace App\Http\Requests\InvoiceLine;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['sometimes', 'exists:invoice_lines,id,deleted_at,NULL'];

        return $rules;
    }
}
