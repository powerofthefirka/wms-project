<?php

namespace App\Http\Requests\InvoiceLine;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required',
                'exists:invoice_lines,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
