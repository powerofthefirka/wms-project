<?php

namespace App\Http\Requests\InvoiceLine;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'invoice_id' => [
                'required',
                'exists:invoices,id,deleted_at,NULL'
            ],
            'title' => [
                'sometimes'
            ],
            'cost' => [
                'sometimes'
            ]
        ];
    }
}
