<?php

namespace App\Http\Requests\Warehouses;

use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{

    public static function validationRules($includeContactInformationRules = true)
    {
        $rules = [
            'customer_id' => [
                'sometimes',
                'exists:customers,id,deleted_at,NULL'
            ]
        ];

        if ($includeContactInformationRules) {
            $contactInformationRules = ContactInformationStoreRequest::prefixedValidationRules('contact_information.');

            foreach ($contactInformationRules as $attribute => $contactInformationRule) {
                foreach ($contactInformationRule as $key => $rule) {
                    if ($rule == 'required' && $attribute != 'contact_information.name') {
                        $contactInformationRules[$attribute][$key] = 'nullable';
                    }

                    if (strpos($rule, 'email') !== false) {
                        unset($contactInformationRules[$attribute][$key]);
                        break;
                    }
                }
            }

            $rules = array_merge_recursive($rules, $contactInformationRules);
        }

        return $rules;
    }
}
