<?php

namespace App\Http\Requests\Warehouses;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:warehouses,id,deleted_at,NULL'];

        $rules['custom_id'] = ['sometimes'];

        $rules['outside_request'] = ['sometimes'];

        return $rules;
    }
}
