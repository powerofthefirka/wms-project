<?php

namespace App\Http\Requests\Package;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'shipping_box_wms_id' => [
                'required',
            ],
            'shipping_box_name' => [
                'sometimes',
            ],
            'tracking_url' => [
                'sometimes',
            ],
            'tracking_number' => [
                'sometimes',
            ],
            'shipping_carrier_wms_id' => [
                "sometimes"
            ],
            'shipping_method_wms_id' => [
                "sometimes"
            ],
            'price' => [
                "sometimes"
            ],
            'status' => [
                "sometimes"
            ]
        ];

        $rules = array_merge_recursive($rules, PackageItemRequest::prefixedValidationRules('items.*.'));

        return $rules;
    }
}
