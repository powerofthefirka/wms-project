<?php

namespace App\Http\Requests\Package;

use App\Http\Requests\FormRequest;

class PackageItemRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'order_item_id' => [
                'required',
                'exists:order_items,id,deleted_at,NULL'
            ],
            'quantity' => [
                'required',
                'numeric',
            ]
        ];
    }
}
