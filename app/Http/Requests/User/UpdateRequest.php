<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ContactInformation\UpdateRequest as ContactInformationUpdateRequest;
use App\Http\Requests\FormRequest;
use Illuminate\Support\Arr;
use App\Models\UserRole;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules(false);

        foreach ($rules['email'] as $key => $rule) {
            if (strpos($rule, 'unique') !== false) {
                unset($rules['email'][$key]);
                break;
            }
        }

        $rules['email'][] = 'exists:users';

        unset($rules['customer_id']);

        $rules['password'] = Arr::prepend($rules['password'], 'nullable');

        foreach ($rules['password_confirmation'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['password_confirmation'][$key]);
                break;
            }
        }

        $rules['password_confirmation'] = Arr::prepend($rules['password_confirmation'], 'nullable');

        return array_merge_recursive($rules, ContactInformationUpdateRequest::prefixedValidationRules('contact_information.'));
    }
}
