<?php

namespace App\Http\Requests\SupplierProduct;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'suppliers' => [
                'sometimes',
                'exists:suppliers,id,deleted_at,NULL'

            ],
            'products' => [
                'sometimes',
                'exists:products,id,deleted_at,NULL'

            ],
            'outside_request' => [
                'sometimes'
            ]
        ];
    }
}
