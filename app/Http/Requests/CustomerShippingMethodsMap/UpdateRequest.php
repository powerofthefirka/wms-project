<?php

namespace App\Http\Requests\CustomerShippingMethodsMap;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['required', 'exists:customer_shipping_methods_maps,id,deleted_at,NULL'];

        return $rules;
    }
}
