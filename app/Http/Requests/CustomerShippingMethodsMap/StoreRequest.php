<?php

namespace App\Http\Requests\CustomerShippingMethodsMap;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'shipping_method_text' => [
                'required',
                'string'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'shipping_service' => [
                'required',
                'string'
            ],
            'shipping_service_carrier_id' => [
                'required',
                'integer'
            ],
            'shipping_service_method_code' => [
                'required',
                'string'
            ]
        ];
    }
}
