<?php

namespace App\Http\Requests\CustomerShippingMethodsMap;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 
                'exists:customer_shipping_methods_maps,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
