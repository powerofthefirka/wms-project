<?php

namespace App\Http\Requests\ShippingCarrier;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'required'
            ],
            '3pl_id' => [
                'required',
                'exists:3pls,id,deleted_at,NULL'
            ],
            'wms_id' => [
                'required'
            ],
            'tracking_url' => [
                'sometimes'
            ]
        ];
    }
}
