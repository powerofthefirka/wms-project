<?php

namespace App\Http\Requests\PickingBatch;

use App\Http\Requests\FormRequest;

class PickRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'picking_batch_item_id' => [
                'required', 
                'validate_picking_batch_item'
            ],
            'quantity' => [
                'required',
                'integer'
            ]
        ];

        return $rules;
    }
}
