<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:locations,id,deleted_at,NULL'];

        foreach ($rules['location_products.*.quantity_on_hand'] as $key => $rule) {
            if (strpos($rule, 'min:0') !== false) {
                unset($rules['location_products.*.quantity_on_hand'][$key]);
            }
            if (strpos($rule, 'not_in:0') !== false) {
                unset($rules['location_products.*.quantity_on_hand'][$key]);
            }
        }

        $rules['location_products.*.location_product_id'] = ['sometimes', 'exists:location_product,id'];

        $rules['custom_id'] = ['sometimes'];

        $rules['outside_request'] = ['sometimes'];

        return $rules;
    }
}
