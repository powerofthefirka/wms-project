<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 'exists:locations,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
