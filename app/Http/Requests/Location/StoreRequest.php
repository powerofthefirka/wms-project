<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\FormRequest;
use App\Http\Requests\LocationProduct\StoreRequest as LocationProductStoreRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'warehouse_id' => [
                'required', 'exists:warehouses,id,deleted_at,NULL'
            ],
            'name' => [
                'required'
            ],
            'pickable' => [
                'sometimes'
            ],
            'location_type_wms_id' => [
                'sometimes'
            ],
            'location_type_name' => [
                'sometimes'
            ]
        ];

        $rules = array_merge_recursive($rules, LocationProductStoreRequest::prefixedValidationRules('location_products.*.'));

        return $rules;
    }
}
