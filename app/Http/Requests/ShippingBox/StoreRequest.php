<?php

namespace App\Http\Requests\ShippingBox;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'name' => [
                'sometimes'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'wms_id' => [
                'required'
            ]
        ];
    }
}
