<?php

namespace App\Http\Requests\ShippingBox;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['required', 'exists:shipping_boxes,id,deleted_at,NULL'];

        return $rules;
    }
}
