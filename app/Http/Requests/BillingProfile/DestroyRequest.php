<?php

namespace App\Http\Requests\BillingProfile;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required',
                'exists:billing_profiles,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
