<?php

namespace App\Http\Requests\BillingProfile;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['required', 'exists:billing_profiles,id,deleted_at,NULL'];

        return $rules;
    }
}
