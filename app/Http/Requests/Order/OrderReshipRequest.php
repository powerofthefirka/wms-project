<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\FormRequest;
use App\Http\Requests\OrderItem\ItemReshipRequest;

class OrderReshipRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = array_merge_recursive(ItemReshipRequest::prefixedValidationRules('order_items.*.'));

        return $rules;
    }
}
