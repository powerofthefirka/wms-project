<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\FormRequest;
use App\Rules\BelongsToCustomer;

class ImportRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'sku' => [
                'required',
                'exists:products'
            ],
            'number' => [
                'required'
            ],
            'ordered_at' => [
                'sometimes',
                'nullable',
                'date_format:Y-m-d H:i:s'
            ],
            'required_shipping_date_at' => [
                'sometimes',
                'nullable',
                'date_format:Y-m-d H:i:s'
            ],
            'shipping_price' => [
                'sometimes'
            ],
            'tax' => [
                'sometimes'
            ],
            'discount' => [
                'sometimes'
            ],
            'discount_type' => [
                'sometimes'
            ],
            'notes' => [
                'sometimes',
                'nullable',
                'string'
            ],
            'priority' => [
                'sometimes',
                'nullable',
                'integer'
            ]
        ];

        return $rules;
    }
}
