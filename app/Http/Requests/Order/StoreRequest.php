<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\OrderItem\StoreRequest as OrderItemStoreRequest;
use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Http\Requests\FormRequest;
use App\Models\Order;
use App\Rules\BelongsToCustomer;
use App\Rules\UniqueNumberCheck;
use Illuminate\Support\Arr;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $customerId = static::getInputField('customer_id');
        $shopName = static::getInputField('shop_name') ?? env('MANUAL_ORDER_SHOP_NAME');

        $rules = [
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'number' => [
                'required',
                'string',
                $customerId ? new UniqueNumberCheck(Order::class, $customerId, $shopName) : false
            ],
            'ordered_at' => [
                'sometimes',
                'date_format:Y-m-d H:i:s'
            ],
            'required_shipping_date_at' => [
                'sometimes',
                'date_format:Y-m-d H:i:s'
            ],
            'shipping_price' => [
                'sometimes'
            ],
            'tax' => [
                'sometimes'
            ],
            'discount' => [
                'sometimes'
            ],
            'discount_type' => [
                'sometimes'
            ],
            'notes' => [
                'sometimes'
            ],
            'priority' => [
                'sometimes',
                'integer'
            ],
            'shipping_carrier_id' => [
                'sometimes',
                'exists:shipping_carriers,id,deleted_at,NULL'
            ],
            'shipping_carrier_wms_id' => [
                "sometimes"
            ],
            'shipping_method_id' => [
                'sometimes',
                'exists:shipping_methods,id,deleted_at,NULL'
            ],
            'shipping_method_wms_id' => [
                "sometimes"
            ],
            'shipping_method_title' => [
                "sometimes"
            ],
            'fraud_hold' => [
                'sometimes',
                'boolean'
            ],
            'address_hold' => [
                'sometimes',
                'boolean'
            ],
            'payment_hold' => [
                'sometimes',
                'boolean'
            ],
            'operator_hold' => [
                'sometimes',
                'boolean'
            ],
            'is_cancelled' => [
                'sometimes',
                'boolean'
            ],
            'status' => [
                'sometimes',
                'string'
            ],
            'shipping_carrier_title' => [
                'sometimes'
            ],
            'shop_name' => [
                'sometimes'
            ]
        ];

        $rules = array_merge_recursive($rules, OrderItemStoreRequest::prefixedValidationRules('order_items.*.'));

        $shippingInformationRules = ContactInformationStoreRequest::prefixedValidationRules('shipping_contact_information.');

        foreach ($shippingInformationRules as $attribute => $shippingContactInformationRules) {
            foreach ($shippingContactInformationRules as $key => $rule) {
                if ($rule == 'required') {
                    $shippingInformationRules[$attribute][$key] = 'sometimes';
                }

                if (strpos($rule, 'email') !== false) {
                    unset($shippingInformationRules[$attribute][$key]);
                    break;
                }
            }
        }

        $rules = array_merge_recursive($rules, $shippingInformationRules);

        $billingInformationRules = ContactInformationStoreRequest::prefixedValidationRules('billing_contact_information.');

        foreach ($billingInformationRules as $attribute => $billingContactInformationRules) {
            foreach ($billingContactInformationRules as $key => $rule) {
                if ($rule == 'required') {
                    $billingInformationRules[$attribute][$key] = 'nullable';
                }

                if (strpos($rule, 'email') !== false) {
                    unset($billingInformationRules[$attribute][$key]);
                    break;
                }
            }
        }

        $rules = array_merge_recursive($rules, $billingInformationRules);

        return $rules;
    }
}
