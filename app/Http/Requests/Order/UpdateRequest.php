<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Arr;
use App\Rules\UniqueNumberCheck;

class UpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = StoreRequest::validationRules();

        $rules['id'] = ['nullable', 'exists:orders,id,deleted_at,NULL'];

        foreach ($rules['number'] as $key => $rule) {
            if (is_a($rule, UniqueNumberCheck::class)) {
                unset($rules['number'][$key]);
                break;
            }
        }

        foreach ($rules['order_items.*.product_id'] as $key => $rule) {
            if (strpos($rule, 'required') !== false) {
                unset($rules['order_items.*.product_id'][$key]);
            }
        }
        $rules['order_items.*.product_id'][] = 'nullable';

        foreach ($rules['order_items.*.quantity'] as $key => $rule) {
            if (strpos($rule, 'min:0') !== false) {
                unset($rules['order_items.*.quantity'][$key]);
            }
            if (strpos($rule, 'not_in:0') !== false) {
                unset($rules['order_items.*.quantity'][$key]);
            }
        }

        $rules['order_items.*.order_item_id'] = ['sometimes', 'exists:order_items,id'];

        $rules['outside_request'] = ['sometimes'];
        $rules['custom_id'][] = 'sometimes';

        return $rules;
    }
}
