<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules($includeContactInformationRules = true)
    {
        $rules = [
            'id' => [
                'sometimes'
            ],
            '3pl_id' => [
                'required',
                'exists:3pls,id,deleted_at,NULL'
            ],
            'password' => [
                'confirmed',
                'min:6',
                'regex:/[a-zA-Z0-9@$!%*#?&]/',
            ],
            'password_confirmation' => [
                'required'
            ],
            'billing_profile_id' => [
                'sometimes'
            ],
            'currency_id' => [
                'sometimes'
            ],
            'weight_unit' => [
                'sometimes'
            ],
            'sync_date_from' => [
                'sometimes'
            ],
            'shiphero_username' => [
                'required_without:shiphero_refresh_token',
                'required_without:shiphero_access_token'
            ],
            'shiphero_password' => [
                'required_without:shiphero_refresh_token',
                'required_without:shiphero_access_token'
            ],
            'shiphero_refresh_token' => [
                'required_without:shiphero_username',
                'required_without:shiphero_password'
            ],
            'shiphero_access_token' => [
                'required_without:shiphero_username',
                'required_without:shiphero_password'
            ],
            'shipping_methods' => [
                'sometimes'
            ],
            'locations' => [
                'sometimes'
            ],
            'dimensions_unit' => [
                'sometimes'
            ]
        ];

        if ($includeContactInformationRules) {
            $rules = array_merge_recursive($rules, ContactInformationStoreRequest::prefixedValidationRules('contact_information.'));
        }

        return $rules;
    }
}
