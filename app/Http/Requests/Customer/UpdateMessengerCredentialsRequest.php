<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\FormRequest;

class UpdateMessengerCredentialsRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'messaging_provider' => [
                'sometimes'
            ],
            'widget_token' => [
                'sometimes'
            ]
        ];

        return $rules;
    }
}
