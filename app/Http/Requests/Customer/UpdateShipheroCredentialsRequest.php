<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\FormRequest;

class UpdateShipheroCredentialsRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'sync_date_from' => [
                'sometimes'
            ],
            'shiphero_username' => [
                'required_without:shiphero_refresh_token',
                'required_without:shiphero_access_token'
            ],
            'shiphero_password' => [
                'required_without:shiphero_refresh_token',
                'required_without:shiphero_access_token'
            ],
            'shiphero_refresh_token' => [
                'required_without:shiphero_username',
                'required_without:shiphero_password'
            ],
            'shiphero_access_token' => [
                'required_without:shiphero_username',
                'required_without:shiphero_password'
            ]
        ];

        return $rules;
    }
}
