<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\ContactInformation\StoreRequest as ContactInformationStoreRequest;
use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    public static function validationRules($includeContactInformationRules = true)
    {
        $rules = [
            'id' => [
                'sometimes'
            ],
            'billing_profile_id' => [
                'sometimes'
            ],
            'currency_id' => [
                'sometimes'
            ],
            'weight_unit' => [
                'sometimes'
            ],
            'shipping_methods' => [
                'sometimes'
            ],
            'locations' => [
                'sometimes'
            ],
            'dimensions_unit' => [
                'sometimes'
            ]
        ];

        if ($includeContactInformationRules) {
            $rules = array_merge_recursive($rules, ContactInformationStoreRequest::prefixedValidationRules('contact_information.'));
        }

        return $rules;
    }
}
