<?php

namespace App\Http\Requests\UserSettings;

use App\Http\Requests\FormRequest;
use App\Models\UserSetting;

class StoreDashboardSettingsRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            UserSetting::USER_SETTING_DASHBOARD_FILTER_DATE_START => [
                'sometimes',
            ],
            'dashboard_filter_date_end' => [
                'sometimes',
            ]
        ];

        return $rules;
    }
}
