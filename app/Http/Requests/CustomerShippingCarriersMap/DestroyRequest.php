<?php

namespace App\Http\Requests\CustomerShippingCarriersMap;

use App\Http\Requests\FormRequest;

class DestroyRequest extends FormRequest
{
    public static function validationRules()
    {
        $rules = [
            'id' => [
                'required', 
                'exists:customer_shipping_carriers_maps,id,deleted_at,NULL'
            ]
        ];

        return $rules;
    }
}
