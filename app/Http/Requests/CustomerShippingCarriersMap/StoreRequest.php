<?php

namespace App\Http\Requests\CustomerShippingCarriersMap;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'shipping_carrier_text' => [
                'required',
                'string'
            ],
            'customer_id' => [
                'required',
                'exists:customers,id,deleted_at,NULL'
            ],
            'shipping_service' => [
                'required',
                'string'
            ],
            'shipping_service_carrier_id' => [
                'required',
                'integer'
            ]
        ];
    }
}
