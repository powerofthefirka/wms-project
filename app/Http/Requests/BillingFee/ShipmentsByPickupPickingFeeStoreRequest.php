<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ShipmentsByPickupPickingFeeRule;

class ShipmentsByPickupPickingFeeStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::SHIPMENTS_BY_PICKUP_PICKING_FEE;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                    new ShipmentsByPickupPickingFeeRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
