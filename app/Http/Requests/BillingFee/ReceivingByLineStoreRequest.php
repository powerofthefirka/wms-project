<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ReceivingByLineRule;

class ReceivingByLineStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::RECEIVING_BY_LINE;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                    new ReceivingByLineRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
