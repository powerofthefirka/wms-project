<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ShipmentByBoxUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return ShipmentByBoxStoreRequest::validationRules();
    }
}
