<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class RecurringUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return RecurringStoreRequest::validationRules();
    }
}
