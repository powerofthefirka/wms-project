<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ShipmentsByPickingFeeRule;

class ShipmentsByPickingFeeStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::SHIPMENTS_BY_PICKING_FEE;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                    new ShipmentsByPickingFeeRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
