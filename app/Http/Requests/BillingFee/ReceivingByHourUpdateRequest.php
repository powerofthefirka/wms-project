<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ReceivingByHourUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                ],
            ]
        );
    }
}
