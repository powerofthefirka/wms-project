<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class AdHocUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return AdHocStoreRequest::validationRules();
    }
}
