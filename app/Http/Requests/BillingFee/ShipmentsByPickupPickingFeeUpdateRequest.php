<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ShipmentsByPickupPickingFeeUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return ShipmentsByPickupPickingFeeStoreRequest::validationRules();
    }
}
