<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ShippingRateRule;

class ShippingRatesImportRequest extends FormRequest
{
    public static function validationRules()
    {
        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'description' => [
                    'sometimes'
                ],
                'shipping_carrier' => [
                    'required'
                ],
                'shipping_method' => [
                    'required'
                ],
                'countries' => [
                    'sometimes'
                ],
                'weight_from' => [
                    'required'
                ],
                'weight_to' => [
                    'required'
                ],
                'weight_unit' => [
                    'required'
                ],
                'price' => [
                    'required'
                ],
            ]
        );
    }
}
