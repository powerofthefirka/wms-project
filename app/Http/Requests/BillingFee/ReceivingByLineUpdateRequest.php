<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ReceivingByLineUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return ReceivingByLineStoreRequest::validationRules();
    }
}
