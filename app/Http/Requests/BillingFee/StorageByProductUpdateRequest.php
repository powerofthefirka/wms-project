<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class StorageByProductUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return StorageByProductStoreRequest::validationRules();
    }
}
