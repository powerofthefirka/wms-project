<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ReceivingByItemRule;

class ReceivingByItemStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::RECEIVING_BY_ITEM;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                    new ReceivingByItemRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
