<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ShippingRatesUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $billingFee = request()->route('billing_fee') ?? null;

        $rules = ShippingRatesStoreRequest::validationRules();

        $rules['name'] = [
            'unique:billing_fees,name,' . ( $billingFee->id ?? '' ) . ',id,deleted_at,NULL,billing_profile_id,' . ( $billingProfile->id ?? '' )
        ];

        return $rules;
    }
}
