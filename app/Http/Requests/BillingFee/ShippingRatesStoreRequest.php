<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ShippingRateRule;

class ShippingRatesStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::SHIPPING_RATES;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required',
                    'unique:billing_fees,name,NULL,id,billing_profile_id,'. ( $billingProfile->id ?? '' )
                ],
                'settings' => [
                    'required',
                    new ShippingRateRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
