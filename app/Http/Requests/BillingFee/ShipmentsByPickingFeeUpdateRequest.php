<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ShipmentsByPickingFeeUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return ShipmentsByPickingFeeStoreRequest::validationRules();
    }
}
