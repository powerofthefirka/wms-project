<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;

class ReturnsUpdateRequest extends FormRequest
{
    public static function validationRules()
    {
        return ReturnsStoreRequest::validationRules();
    }
}
