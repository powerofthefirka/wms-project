<?php

namespace App\Http\Requests\BillingFee;

use App\Http\Requests\FormRequest;
use App\Models\BillingFee;
use App\Rules\BillingFees\ShipmenstByShippingLabelRule;

class ShipmentsByShippingLabelStoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $billingProfile = request()->route('billing_profile');
        $type = BillingFee::SHIPMENTS_BY_SHIPPING_LABEL;
        $billingFee = request()->route('billing_fee') ?? null;

        return array_merge(
            [
                'name' => [
                    'required'
                ],
                'settings' => [
                    'required',
                    new ShipmenstByShippingLabelRule($billingProfile, $type, $billingFee)
                ],
            ]
        );
    }
}
