<?php

namespace App\Http\Requests\ReturnItem;

use App\Http\Requests\FormRequest;
use App\Rules\CheckArrayKeySum;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        $array = static::getInputField('return_items');

        return [
            'order_item_id' => [
                'required',
                'exists:order_items,id,deleted_at,NULL'
            ],
            'quantity' => [
                'required',
                'integer',
                'min:0',
                $array ? new CheckArrayKeySum('quantity', $array) : false
            ],
            'quantity_received' => [
                'sometimes',
                'numeric'
            ]
        ];
    }
}
