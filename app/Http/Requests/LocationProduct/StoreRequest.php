<?php

namespace App\Http\Requests\LocationProduct;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    public static function validationRules()
    {
        return [
            'product_id' => [
                'sometimes',
                'exists:products,id,deleted_at,NULL'
            ],
            'quantity_on_hand' => [
                'required',
                'min:0'
            ],
            'location_product_id' => [
                'sometimes'
            ]
        ];
    }
}
