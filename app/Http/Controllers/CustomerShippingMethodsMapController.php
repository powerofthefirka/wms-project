<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerShippingMethodsMap\StoreRequest;
use App\Http\Requests\CustomerShippingMethodsMap\UpdateRequest;
use App\Http\Requests\CustomerShippingMethodsMap\DestroyRequest;
use App\Http\Resources\CustomerShippingMethodsMapTableResource;
use Illuminate\Http\Request;
use App\Models\CustomerShippingMethodsMap;
use App\Models\Customer;

class CustomerShippingMethodsMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shipping_method_mapping.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];
        $customerIds = Auth()->user()->customerIds();

        $mappingCollection = CustomerShippingMethodsMap::join('customers', 'customer_shipping_methods_maps.customer_id', '=', 'customers.id')
            ->join('contact_informations AS customer_contact_information', 'customers.id', '=', 'customer_contact_information.object_id')
            ->where('customer_contact_information.object_type', Customer::class)
            ->select('*', 'customer_shipping_methods_maps.*')
            ->orderBy($tableColumnName, $sortDirection);

        if (!empty($customerIds)) {
            $mappingCollection = $mappingCollection->whereIn('customer_shipping_methods_maps.customer_id', $customerIds);
        }

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $mappingCollection
                ->orWhereHas('customer.contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->orWhere('customer_shipping_methods_maps.shipping_service', 'like', $term)
                ->orWhere('customer_shipping_methods_maps.shipping_method_pattern', 'like', $term)
                ->get();

            $totalMappingCount = CustomerShippingMethodsMap::count();
            $mappingCount = $mappingCollection->count();
        } else {
            $totalMappingCount = $mappingCollection->count();
            $mappingCount = $totalMappingCount;
        }

        $mappings = $mappingCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $mappingCollection = CustomerShippingMethodsMapTableResource::collection($mappings);

        return response()->json([
            'data' => $mappingCollection,
            'recordsTotal' => $totalMappingCount,
            'recordsFiltered' => $mappingCount
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shipping_method_mapping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->customerShippingMethodsMap->store($request);

        return redirect()->route('shipping_method_mapping.index')->withStatus(__('Shipping method map successfully created.'));
    }

    public function edit(CustomerShippingMethodsMap $shippingMethodMapping)
    {
        return view('shipping_method_mapping.edit', ['mapping' => $shippingMethodMapping]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param CustomerShippingCarriersMap $shippingMethodMapping
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CustomerShippingMethodsMap $shippingMethodMapping)
    {
        app()->customerShippingMethodsMap->update($request, $shippingMethodMapping);

        return redirect()->back()->withStatus(__('Shipping method map successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param CustomerShippingMethodsMap $shippingMethodMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, CustomerShippingMethodsMap $shippingMethodMapping)
    {
        app()->customerShippingMethodsMap->destroy($request, $shippingMethodMapping);

        return redirect()->route('shipping_method_mapping.index')->withStatus(__('Shipping method map successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->customerShippingMethodsMap->filterCustomers($request);
    }

    public function carrier(Request $request, Customer $customer, $carrier)
    {
        return app()->shipment->webshipperCarrierServices($customer, $carrier);
    }
}
