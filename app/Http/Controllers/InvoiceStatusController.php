<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceStatus\DestroyRequest;
use App\Http\Requests\InvoiceStatus\StoreRequest;
use App\Http\Requests\InvoiceStatus\UpdateRequest;
use App\Models\Customer;
use App\Models\InvoiceStatus;
use Illuminate\Http\Request;

class InvoiceStatusController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(InvoiceStatus::class);
    }

    public function index()
    {
        if ($customerId = session('sessionCustomer')) {
            $invoiceStatuses = InvoiceStatus::where('customer_id',$customerId)->get();
        } else {
            $invoiceStatuses = InvoiceStatus::all();
        }

        return view('invoice_status.index', ['invoiceStatuses' => $invoiceStatuses]);
    }

    public function create()
    {
        return view('invoice_status.create');
    }

    public function store(StoreRequest $request)
    {
        app()->invoiceStatus->store($request);

        return redirect()->route('invoice_statuses.index')->withStatus(__('Invoice status successfully created.'));
    }

    public function edit(InvoiceStatus $invoiceStatus)
    {
        return view('invoice_status.edit', ['invoiceStatus' => $invoiceStatus]);
    }

    public function update(UpdateRequest $request, InvoiceStatus $invoiceStatus)
    {
        app()->invoiceStatus->update($request, $invoiceStatus);

        return redirect()->back()->withStatus(__('Invoice status successfully updated.'));
    }

    public function destroy(DestroyRequest $request ,InvoiceStatus $invoiceStatus)
    {
        app()->invoiceStatus->destroy($request, $invoiceStatus);

        return redirect()->back()->withStatus(__('Invoice status successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];


        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%' )
                        ->orWhere('company_name', 'like','%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%' )
                        ->orWhere('zip', 'like', '%' . $term . '%' )
                        ->orWhere('city', 'like', '%' . $term . '%' )
                        ->orWhere('phone', 'like', '%' . $term . '%' );
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }
}
