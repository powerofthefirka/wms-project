<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebshipperCredential\DestroyRequest;
use App\Http\Requests\WebshipperCredential\StoreRequest;
use App\Http\Requests\WebshipperCredential\UpdateRequest;
use App\Http\Resources\WebshipperCredentialTableResource;
use Illuminate\Http\Request;
use App\Models\WebshipperCredential;
use App\Models\Customer;

class WebshipperCredentialController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(WebshipperCredential::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('webshipper_credentials.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];
        $customerIds = Auth()->user()->customerIds();

        $credentialCollection = WebshipperCredential::join('customers', 'webshipper_credentials.customer_id', '=', 'customers.id')
            ->join('contact_informations AS customer_contact_information', 'customers.id', '=', 'customer_contact_information.object_id')
            ->where('customer_contact_information.object_type', Customer::class)
            ->select('*', 'webshipper_credentials.*')
            ->orderBy($tableColumnName, $sortDirection);

        if (!empty($customerIds)) {
            $credentialCollection = $credentialCollection->whereIn('webshipper_credentials.customer_id', $customerIds);
        }

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $credentialCollection
                ->orWhereHas('customer.contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->orWhere('webshipper_credentials.api_base_url', 'like', $term)
                ->orWhere('webshipper_credentials.api_key', 'like', $term)
                ->get();

            $totalCredentialCount = WebshipperCredential::count();
            $credentialCount = $credentialCollection->count();
        } else {
            $totalCredentialCount = $credentialCollection->count();
            $credentialCount = $totalCredentialCount;
        }

        $credentials = $credentialCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $credentialCollection = WebshipperCredentialTableResource::collection($credentials);

        return response()->json([
            'data' => $credentialCollection,
            'recordsTotal' => $totalCredentialCount,
            'recordsFiltered' => $credentialCount
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('webshipper_credentials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->webshipperCredential->store($request);

        return redirect()->route('webshipper_credential.index')->withStatus(__('Webshipper credential successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WebshipperCredential $webshipperCredential
     * @return \Illuminate\Http\Response
     */
    public function edit(WebshipperCredential $webshipperCredential)
    {
        return view('webshipper_credentials.edit', ['webshipperCredential' => $webshipperCredential]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest $request
     * @param  WebshipperCredential $webshipperCredential
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, WebshipperCredential $webshipperCredential)
    {
        app()->webshipperCredential->update($request, $webshipperCredential);

        return redirect()->back()->withStatus(__('Webshipper credential successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param WebshipperCredential $webshipperCredential
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, WebshipperCredential $webshipperCredential)
    {
        app()->webshipperCredential->destroy($request, $webshipperCredential);

        return redirect()->route('webshipper_credential.index')->withStatus(__('Webshipper credential successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->webshipperCredential->filterCustomers($request);
    }
}
