<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Http\Requests\Currency\StoreCurrencyRequest;
use App\Http\Resources\CurrencyResource;

class CurrencyController extends Controller
{

    public function index()
    {
        $this->checkIfAdmin();
        return view('3pls.currencies.index');
    }

    public function currenciesDataTable()
    {
        $currencies = Currency::select('currencies.*');
        $recordsTotal = $currencies->count();
        $currenciesCollection = CurrencyResource::collection($currencies->get());

        return response()->json([
            'data' => $currenciesCollection,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal
        ]);

    }

    public function create()
    {
        $this->checkIfAdmin();
        return view('3pls.currencies.create');
    }

    public function edit($id)
    {
        $this->checkIfAdmin();

        $currency = Currency::findOrFail($id);

        return view('3pls.currencies.edit', ['currency' => $currency]);
    }

    public function store(StoreCurrencyRequest $request)
    {
        $this->checkIfAdmin();

        Currency::create($request->input());

        return redirect()->route('currencies.index')->with('status', "Currency stored successfully");
    }

    public function update(StoreCurrencyRequest $request)
    {
        $this->checkIfAdmin();

        $currency = Currency::findOrFail($request->input('currency'));
        $currency->update($request->input());

        return redirect()->route('currencies.index')->with('status', 'Currency updated successfully.');
    }
}
