<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests {
        resourceAbilityMap as traitResourceAbilityMap;
    }

    use DispatchesJobs, ValidatesRequests;

    public function sessionSetCustomer($id)
    {
        Session::put('customer_id', $id);

        return redirect()->back();
    }

    public function forgetCustomer()
    {
        Session::forget('customer_id');

        return redirect()->back();
    }

    protected function resourceAbilityMap()
    {
        $resourceAbilityMap = $this->traitResourceAbilityMap();

        $resourceAbilityMap['preview'] = 'view';
        $resourceAbilityMap['editablePreview'] = 'update';

        return $resourceAbilityMap;
    }

    public function getDropDownList($items)
    {
        $rows = [];

        foreach ($items as $item) {
            $rows[] = [
                'id' => $item['id'],
                'text' => Arr::get($item['contactInformation'], 'name', 'Unknown')
            ];
        }

        return view('shared.global_search.dropdown_list', ['rows' => $rows]);
    }

    protected function checkIfAdmin()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }
    }
}
