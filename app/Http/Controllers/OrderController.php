<?php

namespace App\Http\Controllers;

use App\Exports\OrderExport;
use App\Http\Requests\Order\DestroyRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateRequest;
use App\Http\Resources\OrderTableResource;
use App\Http\Resources\ObjectChangeTableResource;
use App\Http\Resources\ToteResource;
use App\Imports\OrderImport;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ObjectChange;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Components\ShippingCarrierComponent;
use App\Http\Requests\Order\OrderReshipRequest;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    public function index()
    {
        return view('orders.index', [
            'hideColumns' => get_hidden_columns(Order::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'number'
        );

        $term = $request->input('search.value');

        $orderCollection = app()->order->search($term, empty($request->input('from_orders_late')))
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
            ->leftJoin('contact_informations AS customer_contact_information', function ($join) {
                $join->on('customers.id', '=', 'customer_contact_information.object_id')
                    ->where('customer_contact_information.object_type', Customer::class);
            })
            ->leftJoin('contact_informations', 'shipping_contact_information_id', '=', 'contact_informations.id')
            ->leftJoin('shipping_methods', 'shipping_methods.id', '=', 'orders.shipping_method_id', 'left outer')
            ->leftJoin('countries', 'countries.id', '=', 'contact_informations.country_id', 'left outer')
            ->whereNull('customers.deleted_at')
            ->with('orderItems.totes')
            ->select('*', 'orders.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($request->input('from_orders_late')) {
            $orderCollection
                ->whereNotNull('required_shipping_date_at')
                ->whereNotIn('status', [Order::STATUS_FULFILLED, Order::STATUS_CANCELLED])
                ->whereDate('required_shipping_date_at', '<', Carbon::tomorrow());
        }

        if ($term) {
            $totalOrderCount = Order::count();
            $orderCount = $orderCollection->count();
        } else {
            $totalOrderCount = $orderCollection->count();
            $orderCount = $totalOrderCount;
        }
        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $orderCollection = OrderTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'currency' => currency($orderCollection[0]->customer ?? null),
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function preview(Order $order)
    {
        return response(view('orders.preview', ['order' => $order]));
    }

    public function editablePreview(Order $order)
    {
        return response(view('orders.editablePreview', ['order' => $order]));
    }

    public function create()
    {
        return view('orders.create');
    }

    public function store(StoreRequest $request)
    {
        app()->order->store($request);

        return redirect()->route('orders.index')->withStatus(__('Order successfully created.'));
    }

    public function edit(Order $order)
    {
        return view('orders.edit', ['order' => $order]);
    }

    public function update(UpdateRequest $request, Order $order)
    {
        app()->order->update($request, $order);

        $message = __('Order successfully updated.');

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(DestroyRequest $request, Order $order)
    {
        app()->order->destroy($request, $order);

        return redirect()->route('orders.index')->withStatus(__('Order successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->order->filterCustomers($request);
    }

    public function filterProducts(Request $request, Customer $customer)
    {
        return app()->order->filterProducts($request, $customer);
    }

    public function carriers(Request $request, Order $order)
    {
        return app()->shipment->carriers($order->customer);
    }

    public function carrier(Request $request, Order $order, $carrier)
    {
        return app()->shipment->carrierServices($order->customer, $carrier);
    }

    public function totalOrdersByStatuses(Request $request)
    {
        $date = $request->input('date_filter', '');
        $array = [
            "filterArray" => [
                ["columnName" => "dates_between", "value" => $date],
                ["columnName" => "table_search", "value" => null]
            ]
        ];

        $lines = app()->order->search(json_encode($array));

        $total = $lines->count();

        $statuses = $lines
            ->groupBy('status')
            ->select(
                'status',
                DB::raw('count(id) as count')
            )
            ->where('status', '!=', 'cancelled')
            ->get()
            ->toArray();

        foreach ($statuses as $key => $status) {
            if ($status['status'] == 'pending') {
                $statuses[$key]['icon'] = Order::STATUS_ICONS['pending'];
            } else if ($status['status'] == 'fulfilled') {
                $statuses[$key]['icon'] = Order::STATUS_ICONS['fulfilled'];
            } else if ($status['status'] == 'on_hold') {
                $statuses[$key]['icon'] = Order::STATUS_ICONS['on_hold'];
            } else if ($status['status'] == 'priority') {
                $statuses[$key]['icon'] = Order::STATUS_ICONS['priority'];
            } else if ($status['status'] == 'backorder') {
                $statuses[$key]['icon'] = Order::STATUS_ICONS['backorder'];
            }
        }

        array_unshift($statuses, array(
            'status' => 'All orders',
            'icon' => Order::STATUS_ICONS['all'],
            'count' => $total,
            'skip_value' => true
        ));

        return response(view('shared.totalObjectsByStatuses', [
            'statuses' => $statuses,
            'plural' => 'Orders',
            'singular' => 'Order',
        ]));
    }

    public function export()
    {
        return new OrderExport();
    }

    public function import(Request $request)
    {
        (new OrderImport)->import(request()->file('file'));

        if (!empty(session('shippedError')) || !empty(session('orderStatuses'))) {
            $shippedError = $request->session()->pull('shippedError');
            $orderStatuses = $request->session()->pull('orderStatuses');

            return redirect()->route('orders.index')->withErrors(__($shippedError . $orderStatuses));
        }

        return redirect()->route('orders.index')->withStatus(__('Orders successfully imported.'));
    }

    public function shippingMethods(Request $request, Customer $customer)
    {
        $carriersAndMethods = app()->customer->getShippingMethodsAndCarriers($customer);

        if ($request->input('type') == 'dropdown') {
            return view('orders.shippingMethodDropdown', ['carriersAndMethods' => $carriersAndMethods]);
        }

        return view('orders.shippingMethodList', ['carriersAndMethods' => $carriersAndMethods, 'customShippingModal' => $request->input('custom_shipping_modal')]);
    }

    public function cancel(Order $order)
    {
        app()->order->cancel($order);

        $message = __('Order successfully cancelled.');

        return response()->json(['message' => $message]);
    }

    public function reship(OrderReshipRequest $request, Order $order)
    {
        app()->order->reship($request, $order);

        return redirect()->back()->withStatus(__('Order reship data successfully saved.'));
    }

    public function changeLog()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }

        return view('orders.changeLogIndex');
    }

    public function changeLogDataTable(Request $request, $orderId = false)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'object_changes.created_at'
        );

        $term = $request->input('search.value');

        $filters = json_decode($term);

        if($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if($filter->columnName === 'ordering' && !empty($filter->value)) {
                    $filterValue = explode(",", $filter->value);
                    $tableColumnName = $filterValue[0];
                    $sortDirection = $filterValue[1];
                }
            }
        }

        $changeCollection = app()->order->searchChangeLog($term, $orderId)
            ->leftJoin('orders', function($join)
             {
               $join->on('orders.id', '=', 'object_changes.object_id')
               ->where('object_changes.object_type', Order::class);
             })
            ->leftJoin('order_items', function($join)
             {
               $join->on('order_items.id', '=', 'object_changes.object_id')
               ->where('object_changes.object_type', OrderItem::class);
             })
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            if ($orderId) {
                $totalChangeCount = ObjectChange::where(function ($q) use ($orderId) {
                    $q->where('object_type', Order::class)->where('object_id', $orderId);
                })->orWhere(function ($q) use($orderId) {
                    $q->where('parent_object_type', Order::class)->where('parent_object_id', $orderId);
                })->count();
            } else {
                $totalChangeCount = ObjectChange::where(function ($q) {
                    $q->whereIn('object_type', [Order::class, OrderItem::class])->orWhere('parent_object_type', Order::class);
                })->count();
            }

            $changeCount = $changeCollection->count();
        } else {
            $totalChangeCount = $changeCollection->count();
            $changeCount = $totalChangeCount;
        }

        $changes = $changeCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $changeCollection = ObjectChangeTableResource::collection($changes);

        return response()->json([
            'data' => $changeCollection,
            'recordsTotal' => $totalChangeCount,
            'recordsFiltered' => $changeCount
        ]);
    }
}
