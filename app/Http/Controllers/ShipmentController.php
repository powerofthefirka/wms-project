<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShipmentTableResource;
use App\Models\Shipment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('shipments.index', [
            'hideColumns' => get_hidden_columns(Shipment::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'orders.number'
        );

        $term = $request->input('search.value');

        $shipmentOrdersCollection = app()->shipment->search($term)
            ->join('orders', 'shipments.order_id', '=', 'orders.id')
            ->join('contact_informations AS shipping_contact_information', 'orders.shipping_contact_information_id', '=', 'shipping_contact_information.id')
            ->leftJoin('countries', 'shipping_contact_information.country_id', '=', 'countries.id')
            ->select('*', 'shipments.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalShipmentOrderCount = Shipment::count();
            $shipmentOrderCount = $shipmentOrdersCollection->count();
        } else {
            $totalShipmentOrderCount = $shipmentOrdersCollection->count();
            $shipmentOrderCount = $totalShipmentOrderCount;
        }

        $shipmentOrders = $shipmentOrdersCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $shipmentCollection = ShipmentTableResource::collection($shipmentOrders);

        return response()->json([
            'data' => $shipmentCollection,
            'recordsTotal' => $totalShipmentOrderCount,
            'recordsFiltered' => $shipmentOrderCount,
        ]);
    }

    public function itemsShipped(Shipment $shipment)
    {
        return view('shipments.itemsShipped', ['shipment' => $shipment]);
    }
}
