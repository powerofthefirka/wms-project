<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingMethod\DestroyRequest;
use App\Http\Requests\ShippingMethod\StoreRequest;
use App\Http\Requests\ShippingMethod\UpdateRequest;
use App\Http\Resources\ShippingCarrierTableResource;
use App\Http\Resources\ShippingMethodTableResource;
use App\Models\ThreePl;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\CustomerShippingMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShippingMethodController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ShippingMethod::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customerShppingMethodIds = CustomerShippingMethod::whereIn('customer_id', Auth()->user()->customerIds())->pluck('shipping_method_id');
        $shippingMethods = ShippingMethod::whereIn('id', $customerShppingMethodIds)->where('is_visible', true)->get();

        return view('shipping_method.index', [
            'shippingMethods' => $shippingMethods,
            'hideColumns' => get_hidden_columns(ShippingMethod::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $term = $request->get('search')['value'];

        $customerShppingMethodIds = CustomerShippingMethod::whereIn('customer_id', Auth()->user()->customerIds())->pluck('shipping_method_id');

        $orderCollection = ShippingMethod::whereIn('shipping_methods.id', $customerShppingMethodIds)
            ->join('shipping_carriers', 'shipping_methods.shipping_carrier_id', '=', 'shipping_carriers.id')
            ->select('*', 'shipping_methods.*')
            ->where('is_visible', true)
            ->orderBy($tableColumnName, $sortDirection);

        if($term) {
            $totalOrderCount = ShippingMethod::count();
            $orderCount = $orderCollection->count();
        } else {
            $totalOrderCount = $orderCollection->count();
            $orderCount = $totalOrderCount;
        }

        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();


        $orderCollection = ShippingMethodTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function preview(ShippingMethod $shippingMethod)
    {
        return response(view('shipping_method.preview', ['shippingMethod' => $shippingMethod]));
    }

    public function editablePreview(ShippingMethod $shippingMethod)
    {
        return response(view('shipping_method.editablePreview', ['shippingMethod' => $shippingMethod]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('shipping_method.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->shippingMethod->store($request);

        return redirect()->route('shipping_methods.index')->withStatus(__('Shipping method successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ShippingMethod $shippingMethod)
    {
        return view('shipping_method.edit', ['shippingMethod' => $shippingMethod]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param ShippingMethod $shippingMethod
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ShippingMethod $shippingMethod)
    {
        app()->shippingMethod->update($request, $shippingMethod);

        $message = __('Shipping method successfully updated.');

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, ShippingMethod $shippingMethod)
    {
        app()->shippingMethod->destroy($request, $shippingMethod);

        return redirect()->back()->withStatus(__('Shipping method successfully deleted.'));
    }

    public function getShippingCarriers(Request $request, $threePl = false)
    {
        if ($threePl) {
            $threePlIds = array($threePl);
        } else {
            $threePlIds = Auth()->user()->threePlIds();
        }

        return app()->shippingCarrier->getShippingCarriers($request, $threePlIds);
    }
}
