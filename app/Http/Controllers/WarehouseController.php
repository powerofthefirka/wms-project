<?php

namespace App\Http\Controllers;

use App\Http\Requests\Warehouses\DestroyRequest;
use App\Http\Requests\Warehouses\StoreRequest;
use App\Http\Requests\Warehouses\UpdateRequest;
use App\Http\Resources\WarehouseTableResource;
use App\Models\Customer;
use App\Models\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Warehouse::class);
    }

    public function index()
    {
        return view('warehouses.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];
        $customerIds = Auth()->user()->customerIds();

        $warehouseCollection = Warehouse::join('customers', 'warehouses.customer_id', '=', 'customers.id')
            ->join('contact_informations AS customer_contact_information', 'customers.id', '=', 'customer_contact_information.object_id')
            ->join('contact_informations', 'warehouses.id', '=', 'contact_informations.object_id')
            ->where('customer_contact_information.object_type', Customer::class)
            ->where('contact_informations.object_type', Warehouse::class)
            ->select('*', 'warehouses.*')
            ->orderBy($tableColumnName, $sortDirection);

        $term = $request->get('search')['value'];

        if (!auth()->user()->isAdmin()) {
            $warehouseCollection = $warehouseCollection->whereIn('warehouses.customer_id', auth()->user()->customers()->where('active', 1)->pluck('customer_user.customer_id')->unique()->toArray());
        }

        if (!empty($customerIds)) {
            $warehouseCollection = $warehouseCollection->whereIn('warehouses.customer_id', $customerIds);
        }

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $warehouseCollection
                ->whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', $term)
                        ->orWhere('address', 'like', $term)
                        ->orWhere('city', 'like', $term)
                        ->orWhere('zip', 'like', $term)
                        ->orWhere('email', 'like', $term)
                        ->orWhere('phone', 'like', $term);
                })
                ->orWhereHas('customer.contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->get();

            $totalWarehouseCount = Warehouse::count();
            $warehouseCount = $warehouseCollection->count();
        } else {
            $totalWarehouseCount = $warehouseCollection->count();
            $warehouseCount = $totalWarehouseCount;
        }

        $orders = $warehouseCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $warehouseCollection = WarehouseTableResource::collection($orders);

        return response()->json([
            'data' => $warehouseCollection,
            'recordsTotal' => $totalWarehouseCount,
            'recordsFiltered' => $warehouseCount
        ]);
    }

    public function getWarehouseByCustomer(Customer $customer)
    {
        $warehouses = app()->warehouse->getWarehouseByCustomer($customer);

        return $this->getDropDownList($warehouses);
    }
}
