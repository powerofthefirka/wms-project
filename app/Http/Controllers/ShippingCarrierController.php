<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingCarrier\DestroyRequest;
use App\Http\Requests\ShippingCarrier\StoreRequest;
use App\Http\Requests\ShippingCarrier\UpdateRequest;
use App\Http\Resources\ShippingCarrierTableResource;
use App\Models\ThreePl;
use App\Models\Customer;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShippingCarrierController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ShippingCarrier::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $shippingCarriers = ShippingCarrier::whereIn('3pl_id', Auth()->user()->threePlIds())->get();

        return view('shipping_carrier.index', [
            'shippingCarriers' => $shippingCarriers,
            'hideColumns' => get_hidden_columns(ShippingCarrier::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $term = $request->get('search')['value'];

        $orderCollection = app()->shippingCarrier->search($term)
            ->join('3pls', 'shipping_carriers.3pl_id', '=', '3pls.id')
            ->join('contact_informations AS threepl_contact_information', function ($join) {
                $join->on('3pls.id', '=', 'threepl_contact_information.object_id')
                    ->where('threepl_contact_information.object_type', ThreePl::class);
            })
            ->select('*', 'shipping_carriers.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalOrderCount = ShippingCarrier::count();
            $orderCount = $orderCollection->count();
        } else {
            $totalOrderCount = $orderCollection->count();
            $orderCount = $totalOrderCount;
        }

        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();

        $orderCollection = ShippingCarrierTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function preview(ShippingCarrier $shippingCarrier)
    {
        return response(view('shipping_carrier.preview', ['shippingCarrier' => $shippingCarrier]));
    }

    public function editablePreview(ShippingCarrier $shippingCarrier)
    {
        return response(view('shipping_carrier.editablePreview', ['shippingCarrier' => $shippingCarrier]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('shipping_carrier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->shippingCarrier->store($request);

        if ($request->ajax()) {
            $request->session()->flash('status', __('Shipping carrier successfully created.'));
            return response()->json(['success' => true]);
        }

        return redirect()->route('shipping_carriers.index')->withStatus(__('Shipping carrier successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ShippingCarrier $shippingCarrier)
    {
        return view('shipping_carrier.edit', ['shippingCarrier' => $shippingCarrier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param ShippingCarrier $shippingCarrier
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ShippingCarrier $shippingCarrier)
    {
        app()->shippingCarrier->update($request, $shippingCarrier);

        $message = __('Shipping carrier successfully updated.');

        if ($request->ajax()) {
            $request->session()->flash('status', __('Shipping carrier successfully updated.'));
            return response()->json(['success' => true, 'message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, ShippingCarrier $shippingCarrier)
    {
        app()->shippingCarrier->destroy($request, $shippingCarrier);

        return redirect()->back()->withStatus(__('Shipping carrier successfully deleted.'));
    }

    public function filter3Pls(Request $request)
    {
        return app()->shippingCarrier->filter3Pls($request);
    }

    public function shippingMethods(Request $request, ShippingCarrier $shippingCarrier)
    {
        return app()->shippingMethod->getShippingMethods($request, $shippingCarrier);
    }


    public function getAllShippingMethods(ShippingCarrier $shippingCarrier)
    {
        $results = [];

        foreach ($shippingCarrier->shippingMethods as $method) {
            $results[] = [
                'id' => $method->id,
                'text' => $method->name,
                'cost' => $method->cost
            ];
        }

        return response()->json([
            'results' => $results
        ]);
    }
}
