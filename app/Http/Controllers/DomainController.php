<?php

namespace App\Http\Controllers;

use App\Http\Requests\Domain\StoreRequest;
use App\Http\Requests\Domain\UpdateRequest;
use App\Http\Resources\DomainTableResource;
use App\Models\Domain;
use App\Models\ThreePl;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DomainController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Domain::class);
    }

    public function index(ThreePl $threePl)
    {
        return view('domains.index', ['threePl' => $threePl, 'hideColumns' => get_hidden_columns(Domain::SETTINGS_HIDDEN_COLUMNS_KEY)]);
    }

    public function dataTable(Request $request, ThreePl $threePl)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'id',
            'domains.id'
        );

        $term = $request->input('search.value');

        $filters = json_decode($term);

        if($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if($filter->columnName === 'ordering' && !empty($filter->value)) {
                    $filterValue = explode(",", $filter->value);
                    $tableColumnName = $filterValue[0];
                    $sortDirection = $filterValue[1];
                }
            }
        }

        $domainCollection = app()->domain->search($term, $threePl)
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalDomainCount = Domain::count();
            $domainCount = $domainCollection->count();
        } else {
            $totalDomainCount = $domainCollection->count();
            $domainCount = $totalDomainCount;
        }

        $domains = $domainCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $domainCollection = DomainTableResource::collection($domains);

        return response()->json([
            'data' => $domainCollection,
            'recordsTotal' => $totalDomainCount,
            'recordsFiltered' => $domainCount
        ]);
    }

    public function create(ThreePl $threePl)
    {
        return view('domains.create', ['threePl' => $threePl]);
    }

    public function store(StoreRequest $request, ThreePl $threePl)
    {
        app()->domain->store($request);

        if ($request->ajax()) {
            $request->session()->flash('status', __('Domain successfully created.'));
            return response()->json(['success' => true]);
        }

        return redirect()->route('domains.index', ['threePl' => $threePl])->with(__('Domain successfully created!'));
    }

    public function edit(ThreePl $threePl, Domain $domain)
    {
        return view('domains.edit', ['threePl' => $threePl, 'domain' => $domain]);
    }

    public function update(UpdateRequest $request, ThreePl $threePl, Domain $domain)
    {
        app()->domain->update($request, $domain);

        $message = __('Domain successfully updated.');

        if ($request->ajax()) {
            $request->session()->flash('status', $message);
            return response()->json(['success' => true, 'message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(ThreePl $threePl, Domain $domain)
    {
        app()->domain->destroy(null, $domain);

        return redirect()->route('domains.index', ['threePl' => $threePl])->withStatus(__('Domain successfully deleted.'));
    }
}
