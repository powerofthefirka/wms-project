<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\UserRole;
use App\Http\Resources\UserTableResource;
use App\Models\Role;
use App\Models\User;
use App\Models\CustomerUser;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user->isAdmin() && !$user->isCustomerAdmin()) {
            return abort(403);
        }

        return view('users.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $usersCollection = User::query();

        if (!Auth::user()->isAdmin() || session('customer_id')) {
            $relateUserIds = CustomerUser::whereIn('customer_id', Auth::user()->customerIds())->pluck('user_id');
            $usersCollection = $usersCollection->whereIn('users.id', $relateUserIds);
        }

        $usersCollection = $usersCollection->join('contact_informations', 'users.id', '=', 'contact_informations.object_id')
            ->join('user_roles', 'users.user_role_id', '=', 'user_roles.id')
            ->leftJoin('customer_user', 'users.id', '=', 'customer_user.user_id')
            ->leftJoin('customer_user_roles', 'customer_user_roles.id', '=', 'customer_user.role_id')
            ->where('contact_informations.object_type', User::class)
            ->select('*', 'users.*')
            ->groupBy(['users.id'])
            ->orderBy($tableColumnName, $sortDirection);

        $usersCollection->with(['contactInformation', 'role']);

        $term = $request->get('search')['value'];

        $recordsTotal = User::count();

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $usersCollection
                ->whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', $term)
                        ->orWhere('address', 'like', $term)
                        ->orWhere('city', 'like', $term)
                        ->orWhere('zip', 'like', $term)
                        ->orWhere('email', 'like', $term)
                        ->orWhere('phone', 'like', $term);
                })
                ->get();

            $recordsFiltered = $usersCollection->get()->count();
        } else {
            $recordsFiltered = $recordsTotal;
        }

        $users = $usersCollection->skip($request->get('start'))->limit($request->get('length'))->get();

        return response()->json([
            'data' => UserTableResource::collection($users),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ]);
    }

    /**
     * Show the form for creating a new user
     *
     * @param User $user
     * @param UserRole $role
     * @return \Illuminate\View\View
     */
    public function create(User $user, UserRole $role)
    {
        $roles = UserRole::all();

        return view('users.create', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        app()->user->store($request);

        return redirect()->route('users.index')->withStatus(__('User successfully created.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param \App\Models\User $user
     * @param UserRole $role
     * @return \Illuminate\View\View
     */
    public function edit(User $user, UserRole $role)
    {
        $roles = UserRole::all();

        return view('users.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, User $user)
    {
        app()->user->update($request, $user);

        return redirect()->back()->withStatus(__('User successfully updated.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index')->withStatus(__('User successfully deleted.'));
    }
}
