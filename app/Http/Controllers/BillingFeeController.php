<?php

namespace App\Http\Controllers;

use App\Exports\BillingProfileShippingRateFeeExport;
use App\Http\Requests\BillingFee\AdHocStoreRequest;
use App\Http\Requests\BillingFee\AdHocUpdateRequest;
use App\Http\Requests\BillingFee\ReceivingByHourStoreRequest;
use App\Http\Requests\BillingFee\ReceivingByHourUpdateRequest;
use App\Http\Requests\BillingFee\ReceivingByItemStoreRequest;
use App\Http\Requests\BillingFee\ReceivingByItemUpdateRequest;
use App\Http\Requests\BillingFee\ReceivingByLineStoreRequest;
use App\Http\Requests\BillingFee\ReceivingByLineUpdateRequest;
use App\Http\Requests\BillingFee\ReceivingByPoStoreRequest;
use App\Http\Requests\BillingFee\ReceivingByPoUpdateRequest;
use App\Http\Requests\BillingFee\RecurringStoreRequest;
use App\Http\Requests\BillingFee\RecurringUpdateRequest;
use App\Http\Requests\BillingFee\ReturnsStoreRequest;
use App\Http\Requests\BillingFee\ReturnsUpdateRequest;
use App\Http\Requests\BillingFee\ShipmentByBoxStoreRequest;
use App\Http\Requests\BillingFee\ShipmentByBoxUpdateRequest;
use App\Http\Requests\BillingFee\ShipmentsByPickingFeeStoreRequest;
use App\Http\Requests\BillingFee\ShipmentsByPickingFeeUpdateRequest;
use App\Http\Requests\BillingFee\ShipmentsByPickupPickingFeeStoreRequest;
use App\Http\Requests\BillingFee\ShipmentsByPickupPickingFeeUpdateRequest;
use App\Http\Requests\BillingFee\ShipmentsByShippingLabelStoreRequest;
use App\Http\Requests\BillingFee\ShipmentsByShippingLabelUpdateRequest;
use App\Http\Requests\BillingFee\ShippingRatesStoreRequest;
use App\Http\Requests\BillingFee\ShippingRatesUpdateRequest;
use App\Http\Requests\BillingFee\StorageByLocationStoreRequest;
use App\Http\Requests\BillingFee\StorageByLocationUpdateRequest;
use App\Http\Requests\BillingFee\StorageByProductStoreRequest;
use App\Http\Requests\BillingFee\StorageByProductUpdateRequest;
use App\Http\Resources\BillingFeeTableResource;
use App\Http\Resources\CarriersAndShippingMethodsTableResource;
use App\Imports\BillingProfileShippingRateFeeImport;
use App\Models\BillingFee;
use App\Models\BillingProfile;
use App\Models\ShippingCarrier;
use App\Models\CustomerShippingMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class BillingFeeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(BillingFee::class);
    }

    public function index()
    {
        return view('billing_fees.index');
    }

    public function dataTable(Request $request)
    {
        $billingFeeCollection = BillingFee::query();
        $term = $request->input('search.value');

        if ($term) {
            $totalBillingFeeCount = BillingFee::count();
            $billingFeeCount = $billingFeeCollection->count();
        } else {
            $totalBillingFeeCount = $billingFeeCollection->count();
            $billingFeeCount = $totalBillingFeeCount;
        }

        $billingFeeCollection = $billingFeeCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $billingFeeResource = BillingFeeTableResource::collection($billingFeeCollection);

        return response()->json([
            'data' => $billingFeeResource,
            'recordsTotal' => $totalBillingFeeCount,
            'recordsFiltered' => $billingFeeCount
        ]);
    }

    public function create(BillingProfile $billingProfile, $type)
    {
        $fee = BillingFee::BILLLING_FEE_TYPES[$type];
        $carriers = ShippingCarrier::where('3pl_id', $billingProfile->threePL->id)->whereNotNull('wms_id')->groupBy('wms_id')->get();

        return view('billing_fees.' . $fee['folder'] . '.create',
            ['billingProfile' => $billingProfile, 'feeType' => $type, 'feeTitle' => $fee['title'], 'carriers' => $carriers]);
    }

    public function edit(BillingProfile $billingProfile, BillingFee $billingFee)
    {
        $folder = BillingFee::BILLLING_FEE_TYPES[$billingFee['type']]['folder'];

        $settings = $billingFee['settings'];
        $carriers = ShippingCarrier::where('3pl_id', $billingProfile->threePL->id)->whereNotNull('wms_id')->groupBy('wms_id')->get();

        return view('billing_fees.' . $folder . '.edit',
            ['billingFee' => $billingFee, 'settings' => $settings, 'billingProfile' => $billingProfile, 'carriers' => $carriers]);
    }

    public function destroy(BillingFee $billingFee, BillingProfile $billingProfile )
    {
        app()->billingFee->destroy($billingFee);

        return redirect(route('billing_profiles.edit', ['billingProfile' => $billingProfile->id]) . '#' . $billingFee['type'])->withStatus(__('Billing Fee successfully deleted.'));
    }

    public function carriersAndMethods(Request $request)
    {
        $carriers = ShippingCarrier::whereIn('3pl_id', Auth::user()->threePlIds());
        $term = $request->input('search.value');

        if ($term) {
            $totalCarrierCount = ShippingCarrier::count();
            $carrierCount = $carriers->count();
        } else {
            $totalCarrierCount = $carriers->count();
            $carrierCount = $totalCarrierCount;
        }

        $carriers = $carriers->skip($request->get('start'))->limit($request->get('length'))->get();
        $carrierCollection = CarriersAndShippingMethodsTableResource::collection($carriers);

        return response()->json([
            'data' => $carrierCollection,
            'recordsTotal' => $totalCarrierCount,
            'recordsFiltered' => $carrierCount
        ]);
    }

    public function getCarrierMethods($billingFee, $shippingCarrier)
    {
        $selectedMethods = null;
        $billingFee = BillingFee::find($billingFee);
        $shippingCarrier = ShippingCarrier::find($shippingCarrier);

        if ($billingFee) {
            $settings = $billingFee['settings'];
            $selectedMethods = Arr::get($settings, 'methods', []);
        }

        $methodList = $shippingCarrier->shippingMethods;
        $carrierId = $shippingCarrier->id;

        return response(view('billing_fees.shipments_by_shipping_label.carrierMethodList',
            [
                'selectedMethods' => $selectedMethods,
                'methodList' => $methodList,
                'carrierId' => $carrierId
            ]));
    }

    public function recurringStore(RecurringStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RECURRING, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RECURRING);
    }

    public function recurringUpdate(RecurringUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function receivingByPoStore(ReceivingByPoStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RECEIVING_BY_PO, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RECEIVING_BY_PO);
    }

    public function receivingByPoUpdate(ReceivingByPoUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function receivingByHourStore(ReceivingByHourStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RECEIVING_BY_HOUR, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RECEIVING_BY_HOUR);
    }

    public function receivingByHourUpdate(ReceivingByHourUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function receivingByItemStore(ReceivingByItemStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RECEIVING_BY_ITEM, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RECEIVING_BY_ITEM);
    }

    public function receivingByItemUpdate(ReceivingByItemUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function receivingByLineStore(ReceivingByLineStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RECEIVING_BY_LINE, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RECEIVING_BY_LINE);
    }

    public function receivingByLineUpdate(ReceivingByLineUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function storageByLocationStore(StorageByLocationStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::STORAGE_BY_LOCATION, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::STORAGE_BY_LOCATION);
    }

    public function storageByLocationUpdate(StorageByLocationUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function storageByProductStore(StorageByProductStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::STORAGE_BY_PRODUCT, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::STORAGE_BY_PRODUCT);
    }

    public function storageByProductUpdate(StorageByProductUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function shipmentByBoxtStore(ShipmentByBoxStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::SHIPMENT_BY_BOX, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPMENT_BY_BOX);
    }

    public function shipmentByBoxUpdate(ShipmentByBoxUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function shipmentsByShippingLabelStore(ShipmentsByShippingLabelStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::SHIPMENTS_BY_SHIPPING_LABEL, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPMENTS_BY_SHIPPING_LABEL);
    }

    public function shipmentsByShippingLabelUpdate(ShipmentsByShippingLabelUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function shipmentsByPickingFeeStore(ShipmentsByPickingFeeStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::SHIPMENTS_BY_PICKING_FEE, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPMENTS_BY_PICKING_FEE);
    }

    public function shipmentsByPickingFeeUpdate(ShipmentsByPickingFeeUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function shipmentsByPickupPickingFeeStore(ShipmentsByPickupPickingFeeStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::SHIPMENTS_BY_PICKUP_PICKING_FEE, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPMENTS_BY_PICKUP_PICKING_FEE);
    }

    public function shipmentsByPickupPickingFeeUpdate(ShipmentsByPickupPickingFeeUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function returnsStore(ReturnsStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::RETURNS, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::RETURNS);
    }

    public function returnsUpdate(ReturnsUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function adHocStore(AdHocStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::AD_HOC, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::AD_HOC);
    }

    public function adHocUpdate(AdHocUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(false, $billingProfile);
    }

    public function shippingRatesStore(ShippingRatesStoreRequest $request, BillingProfile $billingProfile)
    {
        app()->billingFee->store($request->validated(), BillingFee::SHIPPING_RATES, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPPING_RATES);
    }

    public function shippingRatesUpdate(ShippingRatesUpdateRequest $request, BillingProfile $billingProfile, BillingFee $billingFee)
    {
        app()->billingFee->update($request->validated(), $billingFee, $billingProfile);

        return $this->feeResponse(true, $billingProfile, BillingFee::SHIPPING_RATES);
    }

    public function feeResponse($redirect, $billingProfile, $type = null)
    {
        $message = 'Billing Fee successfully updated.';
        $url = '';

        if ($redirect) {
            $message = 'Billing Fee successfully created';
            $url = route('billing_profiles.edit', ['billingProfile' => $billingProfile->id]) . '#' . $type;
        }

        return response()->json([
            'message' => $message,
            'redirect' => $redirect,
            'url' => $url
        ]);
    }

    public function import(Request $request, BillingProfile $billingProfile, $type)
    {
        (new BillingProfileShippingRateFeeImport($billingProfile))->import(request()->file('file'));

        if (!empty(session('importError'))) {
            $importErrors = $request->session()->pull('importError');

            return redirect()->route('billing_profiles.edit', ['billingProfile' => $billingProfile->id, '#' . $type])->withErrors($importErrors);
        }

        return redirect()->route('billing_profiles.edit', ['billingProfile' => $billingProfile->id, '#' . $type])->withStatus(__('Shipping rates successfully imported.'));
    }

    public function export(BillingProfile $billingProfile, $type)
    {
        return new BillingProfileShippingRateFeeExport($billingProfile, $type);
    }

}
