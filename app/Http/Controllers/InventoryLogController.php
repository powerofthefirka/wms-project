<?php

namespace App\Http\Controllers;

use App\Http\Resources\InventoryLogTableResource;
use App\Models\User;
use App\Models\InventoryLog;
use App\Models\Location;
use App\Models\Order;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\Shipment;
use Illuminate\Http\Request;

class InventoryLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('inventory_log.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $inventoryLogCollection = InventoryLog::join('users', 'inventory_logs.user_id', '=', 'users.id')
            ->join('contact_informations AS users_contact_information', 'users.id', '=', 'users_contact_information.object_id')
            ->join('products', 'inventory_logs.product_id', '=', 'products.id')
            ->where('users_contact_information.object_type', User::class)
            ->select('*', 'inventory_logs.*')
            ->orderBy($tableColumnName, $sortDirection);

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $inventoryLogCollection
                ->whereHasMorph('source', [Return_::class, PurchaseOrder::class, Location::class], function($query, $type) use ($term){
                    if ($type === Return_::class || $type === PurchaseOrder::class || $type === Order::class) {
                         $query->where('number', 'like', $term);
                    } elseif ($type === Location::class){
                        $query->where('name', 'like', $term);
                    }
                })
                ->orWhereHasMorph('destination', [Location::class, Shipment::class, Order::class], function($query, $type) use ($term){
                    if ($type === Order::class) {
                        $query->where('number', 'like', $term);
                    } elseif ($type === Location::class){
                        $query->where('name', 'like', $term);
                    } elseif ($type === Shipment::class){
                        $query->whereHas('order', function($query) use ($term) {
                            $query->where('number', 'like', $term);
                        });
                    }
                })
                ->orWhereHas('user.contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->orWhereHas('product', function($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->orWhere('quantity', 'like', $term)
                ->get();

            $totalInventoryLogCount = InventoryLog::count();
            $inventoryLogCount = $inventoryLogCollection->count();
        } else {
            $totalInventoryLogCount = $inventoryLogCollection->count();
            $inventoryLogCount = $totalInventoryLogCount;
        }

        $inventoryLogs = $inventoryLogCollection->skip($request->get('start'))->limit($request->get('length'))->get();

        $inventoryLogCollection = InventoryLogTableResource::collection($inventoryLogs);

        return response()->json([
            'data' => $inventoryLogCollection,
            'recordsTotal' => $totalInventoryLogCount,
            'recordsFiltered' => $inventoryLogCount
        ]);

    }
}
