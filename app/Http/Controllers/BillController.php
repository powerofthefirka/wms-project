<?php

namespace App\Http\Controllers;

use App\Jobs\CalculateBillJob;
use App\Jobs\RecalculateBillJob;
use App\Models\Bill;
use App\Models\BillingProfile;
use App\Models\Customer;
use Cassandra\Custom;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('billings.bills');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->store();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $customer = Customer::find($input['customer']);

        $customerId = $customer->id;
        $billingProfileId = $customer->billing_profile_id;
        $billingProfile = BillingProfile::find($billingProfileId);
        $billingFees = $billingProfile->billingFees ?? [];

        $periodStart = $input['start_date'];

        $canCreate = app()->bill->checkPeriodConsistency($periodStart, $customerId, $billingProfileId);

        if ($canCreate && $billingProfileId && $billingFees) {
            CalculateBillJob::dispatch($input, $customer);

            return redirect()->route('billings.customer_bills',[
                'customer' => $customer
            ])->withStatus(__('Bill successfully created.'));

        } else {
            $messageBag = new MessageBag();
            $messageBag->add('error', 'Bad date or missing data in customer billing profile/billing fees/product profiles');

            return redirect()->back()->withErrors($messageBag);
        }
    }

    public function exportToCsv(Bill $bill)
    {
        return app()->bill->exportToCsv($bill);
    }

    public function adHoc(Request $request, Bill $bill) {
        app()->bill->adHoc($request, $bill);

        return redirect()->back()->withStatus(__('Ad Hoc successfully added.'));
    }

    public function recalculate(Bill $bill) {
        $bill->calculated_at = null;
        $bill->save();

        RecalculateBillJob::dispatch($bill);

        return redirect()->route('billings.customer_bills',[
            'customer' => $bill->customer_id
        ])->withStatus(__('Recalculating bill.'));
    }

    /**CustomerTableResource
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'bill edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bill $bill)
    {
        app()->bill->destroy($bill);

        return redirect()->route('billings.customer_bills',[
            'customer' => $bill->customer
        ])->withStatus(__('Bill successfully deleted.'));

    }
}
