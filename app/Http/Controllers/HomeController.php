<?php

namespace App\Http\Controllers;

use App\Exports\OrderExport;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use GuzzleHttp\Client;
use App\Models\Return_;
use App\Models\Customer;
use App\Models\Shipment;
use App\Models\Supplier;
use Carbon\CarbonPeriod;
use App\Models\UserSetting;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Exports\ProductExport;
use App\Exports\PurchaseOrderExport;
use App\Exports\ReturnExport;
use App\Exports\ShipmentExport;
use Igaster\LaravelCities\Geo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function totalRevenue()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();
        $dashboardSettings = $this->dashboardSettings();
        $from = $dateVariables['from'];
        $to = $dateVariables['to'];

        $orders = Order::whereIn('customer_id', $customerIds)
            ->whereBetween('ordered_at', [$from, $to])
            ->orderBy('ordered_at', 'asc');

        if (!empty($dashboardSettings->dashboard_filter_status)) {
            $orders = $orders->where('status', $dashboardSettings->dashboard_filter_status);
        }

        $dateDiffDays = Carbon::parse($from)->diffInDays(Carbon::parse($to));

        if ($dateDiffDays <= 31) {

            $orders = $orders
                ->select(
                    'id',
                    DB::raw("date_format(ordered_at,'%Y-%m-%d') as date"),
                    DB::raw("sum(subtotal) as revenue"),
                    DB::raw("count(id) as orders")
                )
                ->groupBy('date');
        } else {
            $orders = $orders
                ->select(
                    'id',
                    DB::raw("MONTH(ordered_at) as month"),
                    DB::raw("date_format(ordered_at,'%Y-%m') as date"),
                    DB::raw("sum(subtotal) as revenue"),
                    DB::raw("count(id) as orders")
                )
                ->groupBy('month');
        }

        $customer = Customer::whereId($customerIds)->first();
        $currency = '';

        if ($customer) {
            $currency = $customer->currency_code ?? '';
        }

        return response()->json(['data' => $orders->get(), 'currency' => $currency]);
    }

    public function totalShippedOrders()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $orders = Order::whereIn('customer_id', $customerIds)
            ->whereBetween('ordered_at', [$dateVariables['from'], $dateVariables['to']])
            ->has('shipments')
            ->withCount('shipments');

        return $orders->count();
    }

    public function totalShippedItems()
    {
        $customerIds = Auth()->user()->customerIds();

        $ordersThatHaveShipmentsIds = Order::whereIn('customer_id', $customerIds)
            ->has('shipments');

        $ordersThatHaveShipmentsIds = $ordersThatHaveShipmentsIds
            ->get()
            ->pluck('id')
            ->toArray();

        $shipments = Shipment::whereIn('order_id', $ordersThatHaveShipmentsIds)
            ->withCount('shipmentItems')
            ->get();

        return $total = $shipments->sum('shipment_items_count');
    }

    public function ordersByCountry()
    {
        $dateVariables = $this->dateVariables();
        $dashboardSettings = $this->dashboardSettings();

        $countries = app()->order->search(null)
            ->whereBetween('orders.ordered_at', [$dateVariables['from'], $dateVariables['to']]);

        if (!empty($dashboardSettings->dashboard_filter_status)) {
            $countries = $countries->where('status', $dashboardSettings->dashboard_filter_status);
        }

        $countries
            ->join('contact_informations AS shipping_contact_information', 'shipping_contact_information_id', '=', 'shipping_contact_information.id')
            ->join('countries', 'shipping_contact_information.country_id', '=', 'countries.id')
            ->where('shipping_contact_information.object_type', 'App\Models\Order')
            ->select(
                'orders.id',
                DB::raw('count(countries.id) as total_orders, countries.title, countries.code')
            )
            ->orderBy('total_orders', 'desc')
            ->limit(10)
            ->groupBy('countries.id');

        return response()->json($countries->get());
    }

    public function ordersByCities()
    {

        $dateVariables = $this->dateVariables();
        $customerIds = Auth()->user()->customerIds();
        $dashboardSettings = $this->dashboardSettings();

        $orders = app()->order->search(null)
            ->whereIn('customer_id', $customerIds)
            ->whereBetween('orders.ordered_at', [$dateVariables['from'], $dateVariables['to']]);

        if (!empty($dashboardSettings->dashboard_filter_status)) {
            $orders = $orders->where('status', $dashboardSettings->dashboard_filter_status);
        }

        $orders->whereNotNull(['shipping_lat', 'shipping_lng'])
            ->groupBy(['shipping_lat', 'shipping_lng'])
            ->leftJoin('contact_informations', 'orders.shipping_contact_information_id', '=', 'contact_informations.id')
            ->leftJoin('countries', 'contact_informations.country_id', '=', 'countries.id')
            ->select(
                'orders.id',
                'orders.shipping_lat as lat',
                'orders.shipping_lng as lng',
                DB::raw('count(orders.id) as count'),
                'contact_informations.city as title',
                'countries.code as countryCode'
            );

        if (Route::current()->uri() === 'orders/orders_by_cities_limited') {
            $limitemOrders = $orders
                ->orderBy('total_orders', 'desc')
                ->limit(10)
                ->select(
                    'orders.id',
                    DB::raw('count(orders.id) as total_orders'),
                    'contact_informations.city as title',
                    'countries.code as countryCode'
                );

            return response()->json($limitemOrders->get());
        }

        return response()->json($orders->get());
    }

    function createGeoLine($coordinates, $city, $countryCode, $gettingCountry, $totalCountByCity)
    {
        $address = urlencode($countryCode . ',' . $city);
        $client = new Client();
        $result = (string) $client->post("https://maps.googleapis.com/maps/api/geocode/json?key=" . env('GOOGLE_MAPS_API_KEY') . "&address=''.$address.'")->getBody();

        $json = json_decode($result, true);

        if ($json['status'] === 'OK') {
            $jsonResults = $json['results'][0];
            $name = $jsonResults['address_components'][0]['long_name'];
            $country = $countryCode;
            $lat = $jsonResults['geometry']['location']['lat'];
            $long = $jsonResults['geometry']['location']['lng'];
            $newGeo = new Geo;
            $newGeo->name = $name;
            $newGeo->alternames = json_encode([$city]);
            $newGeo->country = $country;
            $newGeo->lat = $lat;
            $newGeo->long = $long;
            $newGeo->parent_id = $gettingCountry['id'];
            $newGeo->level = Geo::LEVEL_PPL;
            $newGeo->save();

            $coordinates[] = [
                'location' => ['lat' => $lat, 'lng' => $long],
                'totalCountByCity' => $totalCountByCity
            ];
        }

        return $coordinates;
    }

    public function globalSearch(Request $request)
    {
        $term = $request->input('term');
        $results = [];

        $orders = app()->order->search($term)->get();
        $inventory = app()->product->search($term)->get();
        $purchaseOrders = app()->purchaseOrder->search($term)->get();
        $returns = app()->return->search($term)->get();
        $shipments = app()->shipment->search($term)->get();

        if ($inventory->count()) {
            $inventory = $inventory->splice(0, 5);
            $inventory['linkToTable'] = route('products.index', ['term' => $term]);
            $inventory['count'] = $inventory->count();
            $results['Inventory'] = $inventory;
        }

        if ($orders->count()) {
            $orders = $orders->splice(0, 3);
            $orders['linkToTable'] = route('orders.index', ['term' => $term]);
            $orders['count'] = $orders->count();
            $results['Orders'] = $orders;
        }

        if ($returns->count()) {
            $returns = $returns->splice(0, 3);
            $returns['linkToTable'] = route('returns.index', ['term' => $term]);
            $returns['count'] = $returns->count();
            $results['Returns'] = $returns;
        }

        if ($shipments->count()) {
            $shipments = $shipments->splice(0, 3);
            $shipments['linkToTable'] = route('shipments.index', ['term' => $term]);
            $shipments['count'] = $shipments->count();
            $results['Shipments'] = $shipments;
        }

        if ($purchaseOrders->count()) {
            $purchaseOrders = $purchaseOrders->splice(0, 3);
            $purchaseOrders['linkToTable'] = route('purchase_orders.index', ['term' => $term]);
            $purchaseOrders['count'] = $purchaseOrders->count();
            $results['PurchaseOrders'] = $purchaseOrders;
        }

        return response(view('shared.global_search.results', ['results' => $results]));
    }

    public function ordersReceivedCalc()
    {

        $dashboardSettings = $this->dashboardSettings();
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $todays = Order::whereIn('customer_id', $customerIds)
            ->whereBetween('ordered_at', [($dateVariables['from']), $dateVariables['to']]);


        if (!empty($dashboardSettings->dashboard_filter_status)) {
            $todays = $todays->where('status', $dashboardSettings->dashboard_filter_status);
        }

        $todays = $todays->count();

        return response()->json($todays);
    }

    public function returnsCalc()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $todays = Return_::whereBetween('requested_at', [$dateVariables['from'], $dateVariables['to']])
            ->whereHas('order', function ($q) use ($customerIds) {
                $q->whereIn('orders.customer_id', $customerIds);
            })
            ->count();

        return response()->json($todays ?? 0);
    }

    public function shipmentsCalc()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $orders = Order::whereIn('customer_id', $customerIds)
            ->whereBetween('ordered_at', [$dateVariables['from'], $dateVariables['to']])
            ->where('status', 'fulfilled');

        return response()->json($orders->count());
    }

    public function purchaseOrdersCalc()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $purchaseOrders = PurchaseOrder::whereBetween('ordered_at', [$dateVariables['from'], $dateVariables['to']])
            ->whereIn('customer_id', $customerIds);

        return response()->json(['poCount' => $purchaseOrders->count()]);
    }

    public function purchaseOrdersQuantityCalc()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();
        $purchaseOrders = PurchaseOrder::whereBetween('ordered_at', [$dateVariables['from'], $dateVariables['to']])
            ->whereIn('customer_id', $customerIds)->get();

        $totalProductsReceived = [];

        foreach ($purchaseOrders as $purchaseOrder) {
            $totalProductsReceived[] = $purchaseOrder->purchaseOrderItems->sum('quantity_received');
        }

        return response()->json(['poQuantityReceived' => array_sum($totalProductsReceived)]);
    }

    public function purchaseOrdersReceived()
    {
        $customerIds = Auth()->user()->customerIds();
        $purchaseOrdersCollection = app()->purchaseOrder->search(null)
            ->where('status', 'pending')
            ->leftJoin('suppliers', 'purchase_orders.supplier_id', '=', 'suppliers.id')
            ->leftJoin('contact_informations AS supplier_contact_information', function ($join) {
                $join->on('suppliers.id', '=', 'supplier_contact_information.object_id')
                    ->where('supplier_contact_information.object_type', Supplier::class);
            })
            ->limit(5)
            ->whereIn('suppliers.customer_id', $customerIds)
            ->select('*', 'purchase_orders.*');

        return response()->json([
            'data' => $purchaseOrdersCollection->get()->toArray(),
            'recordsTotal' => $purchaseOrdersCollection->count(),
            'recordsFiltered' => $purchaseOrdersCollection->count(),
        ]);
    }

    public function lateOrders()
    {
        $customerIds = Auth()->user()->customerIds();
        $dateVariables = $this->dateVariables();

        $orders = Order::whereIn('customer_id', $customerIds)
            ->whereNotNull('required_shipping_date_at')
            ->whereNotIn('status', [Order::STATUS_FULFILLED, Order::STATUS_CANCELLED])
            ->whereDate('required_shipping_date_at', '<', $dateVariables['tomorrow'])
            ->limit(5)
            ->select(
                'id',
                'number',
                DB::raw("DATE_FORMAT(orders.required_shipping_date_at, '%Y-%m-%d') as required_shipping_date_formatted")
            );

        return response()->json([
            'data' => $orders->get()->toArray(),
            'recordsTotal' => $orders->count(),
            'recordsFiltered' => $orders->count(),
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $orderTableColumnsHide = get_hidden_columns(Order::SETTINGS_HIDDEN_COLUMNS_KEY);
        $dashboardFilterDateStart = user_settings(UserSetting::USER_SETTING_DASHBOARD_FILTER_DATE_START);
        $dashboardFilterDateEnd = user_settings(UserSetting::USER_SETTING_DASHBOARD_FILTER_DATE_END);

        $statuses = [
            Order::STATUS_PENDING,
            Order::STATUS_FULFILLED,
            Order::STATUS_CANCELLED,
            Order::STATUS_PRIORITY,
            Order::STATUS_BACKORDER,
            Order::STATUS_ON_HOLD,
        ];

        return view('dashboard.index', [
            'hideColumns' => $orderTableColumnsHide,
            'statuses' => $statuses,
            'dashboardFilterDateStart' => $dashboardFilterDateStart,
            'dashboardFilterDateEnd' => $dashboardFilterDateEnd
        ]);
    }

    public function dashboardSettings()
    {
        $settings = [
            'dashboard_filter_date_start' => user_settings(UserSetting::USER_SETTING_DASHBOARD_FILTER_DATE_START),
            'dashboard_filter_date_end' => user_settings(UserSetting::USER_SETTING_DASHBOARD_FILTER_DATE_END)
        ];

        return $settings;
    }

    public function dateVariables()
    {
        $yesterday = Carbon::yesterday()->toDateTimeString();
        $today = Carbon::now()->subDays(env('DEFAULT_DASHBOARD_DATE_RANGE'))->toDateTimeString();
        $tomorrow = Carbon::tomorrow()->toDateTimeString();
        $dashboardSettings = $this->dashboardSettings();

        $from = empty($dashboardSettings['dashboard_filter_date_start'])
            ? $today : date($dashboardSettings['dashboard_filter_date_start']);
        $to =   empty($dashboardSettings['dashboard_filter_date_end'])
            ? $tomorrow : Carbon::parse($dashboardSettings['dashboard_filter_date_end'])->addDay()->toDate()->format('Y-m-d');

        $formatted_from = Carbon::parse($from)->timezone('UTC')->toDateTimeString();
        $formatted_to = Carbon::parse($to)->timezone('UTC')->toDateTimeString();

        return [
            'yesterday' => $yesterday,
            'today' => $today,
            'tomorrow' => $tomorrow,
            'from' => $formatted_from,
            'to' => $formatted_to,
        ];
    }

    public function export(Request $request)
    {
        $data = $request->data ?? null;

        $model = $request->model ?? null;

        if ($model == 'product') {
            return new ProductExport($data);
        } else if ($model == 'order') {
            return new OrderExport($data);
        } else if ($model == 'purchaseOrder') {
            return new PurchaseOrderExport($data);
        } else if ($model == 'return') {
            return new ReturnExport($data);
        }else if ($model == 'shipment') {
            return new ShipmentExport($data);
        }

        return redirect()->back()->withErrors(__('Something went wrong!.'));
    }
}
