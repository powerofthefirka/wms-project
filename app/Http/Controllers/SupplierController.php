<?php

namespace App\Http\Controllers;

use App\Http\Requests\Supplier\DestroyRequest;
use App\Http\Requests\Supplier\StoreRequest;
use App\Http\Requests\Supplier\UpdateRequest;
use App\Http\Resources\SupplierTableResource;
use App\Models\Customer;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    public function __construct()
    {
        // $this->authorizeResource(Supplier::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('supplier.index', [
            'hideColumns' => get_hidden_columns(Supplier::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'contact_informations.name'
        );

        $term = $request->input('search.value');

        $supplierCollection = app()->supplier->search($term)
            ->leftJoin('contact_informations AS customer_contact_information', function ($join) {
                $join->on('suppliers.customer_id', '=', 'customer_contact_information.object_id')
                    ->where('customer_contact_information.object_type', Customer::class);
            })
            ->leftJoin('contact_informations', function ($join) {
                $join->on('suppliers.id', '=', 'contact_informations.object_id')
                    ->where('contact_informations.object_type', Supplier::class);
            })
            ->select('*', 'suppliers.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalSupplierCount = Supplier::count();
            $supplierCount = $supplierCollection->count();
        } else {
            $totalSupplierCount = $supplierCollection->count();
            $supplierCount = $totalSupplierCount;
        }

        $suppliers = $supplierCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $supplierCollection = SupplierTableResource::collection($suppliers);

        return response()->json([
            'data' => $supplierCollection,
            'recordsTotal' => $totalSupplierCount,
            'recordsFiltered' => $supplierCount
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->supplier->store($request);

        return redirect()->route('vendors.index')->withStatus(__('Supplier successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Supplier $supplier
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Supplier $vendor)
    {
        return view('supplier.edit', ['supplier' => $vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Supplier $vendor)
    {
        app()->supplier->update($request, $vendor);

        return redirect()->back()->withStatus(__('Supplier successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, Supplier $vendor)
    {
        app()->supplier->destroy($request, $vendor);

        return redirect()->route('vendors.index')->withStatus(__('Supplier successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->supplier->filterCustomers($request);
    }

    public function getVendorByCustomer(Customer $customer)
    {
        $suppliers = app()->supplier->getVendorByCustomer($customer);

        return $this->getDropDownList($suppliers);
    }
}
