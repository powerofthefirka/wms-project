<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSettings\StoreDashboardSettingsRequest;
use App\Models\UserSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserSettingController extends Controller
{

    public function hideColumns(Request $request)
    {
        UserSetting::saveSettings($request->all());

        return new JsonResponse();
    }

    public function dashboardSettings(StoreDashboardSettingsRequest $request)
    {
        UserSetting::saveSettings($request->validated());

        return redirect()->back();
    }
}
