<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PurchaseOrder\StoreBatchRequest;
use App\Http\Requests\PurchaseOrder\DestroyBatchRequest;
use App\Http\Requests\PurchaseOrder\UpdateBatchRequest;
use App\Http\Requests\PurchaseOrder\ReceiveBatchRequest;
use App\Http\Requests\PurchaseOrder\FilterRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PurchaseOrderCollection;
use App\Http\Resources\PurchaseOrderResource;
use App\Http\Resources\PurchaseOrderItemCollection;
use App\Http\Resources\RevisionCollection;
use App\Models\PurchaseOrder;
use App\Models\UserRole;
use App\Models\PurchaseOrderItem;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;

class PurchaseOrderController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(PurchaseOrder::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $purchaseOrders = PurchaseOrder::whereIn('customer_id', $customerIds)->paginate();

        return new PurchaseOrderCollection($purchaseOrders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new PurchaseOrderCollection(
                app()->purchaseOrder->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder  $purchaseOrder)
    {
        return new PurchaseOrderResource($purchaseOrder);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new PurchaseOrderCollection(
                app()->purchaseOrder->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->purchaseOrder->destroyBatch($request)
            )
        );
    }

    public function receive(ReceiveBatchRequest $request, PurchaseOrder $purchaseOrder)
    {
        $this->authorize('batchReceive', $purchaseOrder);

        return response()->json(
            new PurchaseOrderItemCollection(
                app()->purchaseOrder->receiveBatch($request, $purchaseOrder)
            )
        );
    }

    public function history(PurchaseOrder $purchaseOrder)
    {
        $this->authorize('history', $purchaseOrder);

        return new RevisionCollection(app()->purchaseOrder->history($purchaseOrder));
    }

    public function itemHistory(PurchaseOrderItem $purchaseOrderItem)
    {
        $this->authorize('itemHistory', $purchaseOrderItem->purchaseOrder);

        return new RevisionCollection(app()->purchaseOrder->history($purchaseOrderItem));
    }

    public function filter(FilterRequest $request)
    {
        $customerIds = Auth()->user()->customerIds();

        $purchaseOrders = app()->purchaseOrder->filter($request, $customerIds);

        return new PurchaseOrderCollection($purchaseOrders);
    }
}
