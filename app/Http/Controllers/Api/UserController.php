<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\AccessTokenRequest;
use App\Http\Requests\User\DestroyBatchRequest;
use App\Http\Requests\User\StoreBatchRequest;
use App\Http\Requests\User\UpdateBatchRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Models\CustomerUser;
use App\Models\UserRole;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Request;
use Laravel\Passport\Token;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\WebhookCollection;
use Illuminate\Support\Arr;

class UserController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index()
    {
        $user = auth()->user();

        if ($user->user_role_id == UserRole::ROLE_ADMINISTRATOR) {
            $users = User::paginate();
        } else {
            $customerIds = $user->customers->pluck('id');
            $userIds = CustomerUser::whereIn('customer_id', $customerIds)->pluck('user_id')->unique()->toArray();
            $users = User::whereIn('id', $userIds)->paginate();
        }

        return new UserCollection($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new UserCollection(
                app()->user->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new UserCollection(
                app()->user->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyBatchRequest $request)
    {
        $users = app()->user->destroyBatch($request);

        $users = $users->map(function ($item) {
            return array_only($item, ['email']);
        });

        return response()->json(
            new ResourceCollection($users)
        );
    }

    /**
     * Display a listing of the customer resource.
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function customers(User $user)
    {
        $this->authorize('customers', $user);

        $customers = app()->customer->getUserCustomers($user);

        return new CustomerCollection($customers);
    }

    /**
     * Display a listing of the webhook resource.
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function webhooks(User $user)
    {
        $this->authorize('webhooks', $user);

        $webhooks = app()->webhook->getUserWebhooks($user);

        return new WebhookCollection($webhooks);
    }

    public function getTokens(Token $token)
    {
        $user = auth()->user();

        if ($user->user_role_id == UserRole::ROLE_ADMINISTRATOR) {
            return Token::paginate();
        } else {
            $customerIds = $user->customers->pluck('id');
            $userIds = CustomerUser::whereIn('customer_id', $customerIds)->pluck('user_id')->unique()->toArray();

            return Token::whereIn('user_id', $userIds)->paginate();
        }
    }

    public function accessTokens(AccessTokenRequest $request)
    {
        return response()->json(
            app()->user->accessTokens($request)
        );
    }

    public function deleteAccessToken(Token $token)
    {
        $this->authorize('deleteAccessToken', $token->user);

        return response()->json(
            app()->user->deleteAccessToken($token)
        );
    }
}
