<?php

namespace App\Http\Controllers\Api;

use App\Models\UserRole;
use App\Models\Invoice;
use App\Http\Controllers\ApiController;
use App\Http\Resources\InvoiceResource;
use App\Http\Resources\InvoiceCollection;
use App\Http\Requests\Invoice\StoreBatchRequest;
use App\Http\Requests\Invoice\UpdateBatchRequest;
use App\Http\Resources\InvoiceTableResource;
use App\Http\Requests\Invoice\DestroyBatchRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceController extends ApiController
{

    public function __construct()
    {
        $this->authorizeResource(Invoice::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $invoices = Invoice::paginate();

        return new InvoiceCollection($invoices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new InvoiceCollection(
                app()->invoice->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return new InvoiceResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new InvoiceCollection(
                app()->invoice->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->invoice->destroyBatch($request)
            )
        );
    }
}
