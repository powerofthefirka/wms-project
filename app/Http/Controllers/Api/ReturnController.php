<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Return_\StoreBatchRequest;
use App\Http\Requests\Return_\DestroyBatchRequest;
use App\Http\Requests\Return_\UpdateBatchRequest;
use App\Http\Requests\Return_\ReceiveBatchRequest;
use App\Http\Requests\Return_\FilterRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ReturnCollection;
use App\Http\Resources\ReturnResource;
use App\Http\Resources\ReturnItemCollection;
use App\Http\Resources\RevisionCollection;
use App\Models\Return_;
use App\Models\Order;
use App\Models\UserRole;
use App\Models\ReturnItem;
use App\Http\Controllers\ApiController;

class ReturnController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Return_::class);

        foreach ($this->middleware as $key => $value) {
            if (isset($value['middleware']) && $value['middleware'] == 'can:view,return_') {
                $this->middleware[$key]['middleware'] = 'can:view,return';
                break;
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $returns = Return_::with('order')->whereHas('order', function($order) use($customerIds) {
            $order->whereIn('customer_id', $customerIds);
            })->paginate();

        return new ReturnCollection($returns);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new ReturnCollection(
                app()->return->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Return_ $return
     * @return \Illuminate\Http\Response
     */
    public function show(Return_ $return)
    {
        $this->authorize($return);
        return new ReturnResource($return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new ReturnCollection(
                app()->return->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->return->destroyBatch($request)
            )
        );
    }

    public function receive(ReceiveBatchRequest $request, Return_ $return)
    {
        $this->authorize('batchReceive', $return);

        return response()->json(
            new ReturnItemCollection(
                app()->return->receiveBatch($request, $return)
            )
        );
    }

    public function history(Return_ $return)
    {
        $this->authorize('history', $return);

        return new RevisionCollection(app()->return->history($return));
    }

    public function itemHistory(ReturnItem $returnItem)
    {
        $this->authorize('itemHistory', $returnItem->return_);

        return new RevisionCollection(app()->return->history($returnItem));
    }

    public function filter(FilterRequest $request)
    {
        $customerIds = Auth()->user()->customerIds();

        $orderIds = Order::whereIn('customer_id', $customerIds)->pluck('id')->unique()->toArray();

        $returns = app()->return->filter($request, $orderIds);

        return new ReturnCollection($returns);
    }
}
