<?php

namespace App\Http\Controllers\Api;

use App\Models\Supplier;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\SupplierResource;
use App\Http\Resources\SupplierCollection;
use App\Http\Requests\Supplier\StoreBatchRequest;
use App\Http\Requests\Supplier\UpdateBatchRequest;
use App\Http\Requests\Supplier\DestroyBatchRequest;
use App\Http\Requests\SupplierProduct\StoreRequest as SupplierProductRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;

class SupplierController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Supplier::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $suppliers = Supplier::whereIn('customer_id', $customerIds)->paginate();

        return new SupplierCollection($suppliers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new SupplierCollection(
                app()->supplier->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Supplier $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return new SupplierResource($supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new SupplierCollection(
                app()->supplier->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->supplier->destroyBatch($request)
            )
        );
    }

    public function products(SupplierProductRequest $request, Supplier $supplier)
    {
        return response()->json(
            new SupplierResource(
                app()->supplier->products($request, $supplier)
            )
        );
    }
}
