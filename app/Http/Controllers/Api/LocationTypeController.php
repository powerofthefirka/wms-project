<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LocationType\StoreBatchRequest;
use App\Http\Requests\LocationType\UpdateBatchRequest;
use App\Http\Requests\LocationType\DestroyBatchRequest;
use App\Http\Resources\LocationTypeResource;
use App\Http\Resources\LocationTypeCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\LocationType;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class LocationTypeController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(LocationType::class);
    }

    public function index()
    {
        $locationTypes = LocationType::whereIn('3pl_id', Auth()->user()->threePlIds())->paginate();

        return new LocationTypeCollection($locationTypes);
    }

    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new LocationTypeCollection(
                app()->locationType->storeBatch($request)
            )
        );
    }

    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new LocationTypeCollection(
                app()->locationType->updateBatch($request)
            )
        );
    }

    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->locationType->destroyBatch($request)
            )
        );
    }
}
