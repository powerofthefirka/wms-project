<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CustomerShippingMethodsMap\StoreBatchRequest;
use App\Http\Requests\CustomerShippingMethodsMap\UpdateBatchRequest;
use App\Http\Requests\CustomerShippingMethodsMap\DestroyBatchRequest;
use App\Http\Resources\CustomerShippingMethodsMapCollection;
use App\Http\Resources\CustomerShippingMethodsMapResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\CustomerShippingMethodsMap;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class CustomerShippingMethodsMapController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(CustomerShippingMethodsMap::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $shippingMethodsMaps = CustomerShippingMethodsMap::whereIn('customer_id', $customerIds)->paginate();

        return new CustomerShippingMethodsMapCollection($shippingMethodsMaps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new CustomerShippingMethodsMapCollection(
                app()->customerShippingMethodsMap->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  CustomerShippingMethodsMap $customerShippingMethodsMap
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerShippingMethodsMap $customerShippingMethodsMap)
    {
        return new CustomerShippingMethodsMapResource($customerShippingMethodsMap);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new CustomerShippingMethodsMapCollection(
                app()->customerShippingMethodsMap->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->customerShippingMethodsMap->destroyBatch($request)
            )
        );
    }
}
