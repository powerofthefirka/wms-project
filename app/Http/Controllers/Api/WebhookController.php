<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Webhook\StoreBatchRequest;
use App\Http\Requests\Webhook\UpdateBatchRequest;
use App\Http\Requests\Webhook\DestroyBatchRequest;
use App\Http\Resources\WebhookResource;
use App\Http\Resources\WebhookCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Webhook;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class WebhookController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Webhook::class);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webhooks = Webhook::whereIn('customer_id', auth()->user()->customerIds())->paginate();

        return new WebhookCollection($webhooks);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new WebhookCollection(
                app()->webhook->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Webhook $webhook)
    {
        return new WebhookResource($webhook);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new WebhookCollection(
                app()->webhook->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->webhook->destroyBatch($request)
            )
        );
    }
}
