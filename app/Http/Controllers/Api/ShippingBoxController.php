<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ShippingBox\StoreBatchRequest;
use App\Http\Requests\ShippingBox\UpdateBatchRequest;
use App\Http\Requests\ShippingBox\DestroyBatchRequest;
use App\Http\Resources\ShippingBoxResource;
use App\Http\Resources\ShippingBoxCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ShippingBox;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class ShippingBoxController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(ShippingBox::class);
    }

    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $shippingBoxes = ShippingBox::whereIn('customer_id', $customerIds)->paginate();

        return new ShippingBoxCollection($shippingBoxes);
    }

    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new ShippingBoxCollection(
                app()->shippingBox->storeBatch($request)
            )
        );
    }

    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new ShippingBoxCollection(
                app()->shippingBox->updateBatch($request)
            )
        );
    }

    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->shippingBox->destroyBatch($request)
            )
        );
    }
}
