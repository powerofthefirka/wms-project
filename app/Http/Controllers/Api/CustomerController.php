<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Customer\DestroyBatchRequest;
use App\Http\Requests\Customer\StoreBatchRequest;
use App\Http\Requests\Customer\UpdateBatchRequest;
use App\Http\Requests\Customer\UpdateUsersBatchRequest;
use App\Http\Requests\Customer\UpdateUsersRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\WarehouseCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ProductCollection;
use App\Models\Customer;
use App\Models\CustomerUser;
use App\Models\User;
use App\Http\Controllers\ApiController;

class CustomerController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Customer::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate();

        return new CustomerCollection($customers);
    }

    public function listUsers(Customer $customer)
    {
        $this->authorize('users', $customer);

        $allCustomerUsers = CustomerUser::where('customer_id', $customer['id'])->get();

        return new CustomerCollection($allCustomerUsers);
    }

    public function updateUsers(UpdateUsersRequest $request, Customer $customer)
    {
        $this->authorize('updateUsers', $customer);

        return response()->json(
            new CustomerCollection(
                app()->customer->updateUsers($request, $customer)
            )
        );
    }

    public function detachUser(Customer $customer, User $user)
    {
        $this->authorize('updateUsers', $customer);

        return response()->json(
            app()->customer->detachUser($customer, $user)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new CustomerCollection(
                app()->customer->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new CustomerCollection(
                app()->customer->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->customer->destroyBatch($request)
            )
        );
    }

    /**
     * Display a listing of the warehouse resource.
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function warehouses(Customer $customer)
    {
        $this->authorize('warehouses', $customer);

        return new WarehouseCollection(app()->warehouse->getCustomerWarehouses($customer));
    }

    /**
     * Display a listing of the user resource.
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function users(Customer $customer)
    {
        $this->authorize('users', $customer);

        return new UserCollection(app()->user->getCustomerUsers($customer));
    }

    /**
     * Display a listing of the product resource.
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function products(Customer $customer)
    {
        $this->authorize('products', $customer);

        return new ProductCollection(app()->product->getCustomerProduct($customer));
    }

    /**
     * Display a listing of the webshipper carriers.
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function webshipperCarriers(Customer $customer)
    {
        $this->authorize('webshipperCarriers', $customer);

        return response()->json(
            new ResourceCollection(
                app()->shipment->webshipperCarriers($customer)
            )
        );
    }
}
