<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Warehouses\DestroyBatchRequest;
use App\Http\Requests\Warehouses\StoreBatchRequest;
use App\Http\Requests\Warehouses\UpdateBatchRequest;
use App\Http\Resources\WarehouseCollection;
use App\Http\Resources\WarehouseResource;
use App\Models\Warehouse;
use App\Models\UserRole;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WarehouseController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Warehouse::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return WarehouseCollection
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $warehouses = Warehouse::whereIn('customer_id', $customerIds)->paginate();

        return new WarehouseCollection($warehouses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new WarehouseCollection(
                app()->warehouse->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Warehouse $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        return new WarehouseResource($warehouse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new WarehouseCollection(
                app()->warehouse->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->warehouse->destroyBatch($request)
            )
        );
    }
}
