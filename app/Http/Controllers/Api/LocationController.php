<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Location\DestroyBatchRequest;
use App\Http\Requests\Location\StoreBatchRequest;
use App\Http\Requests\Location\UpdateBatchRequest;
use App\Http\Resources\LocationCollection;
use App\Http\Resources\LocationResource;
use App\Models\CustomerLocation;
use App\Models\Location;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LocationController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Location::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return LocationCollection
     */
    public function index()
    {
        $customerLocationIds = CustomerLocation::whereIn('customer_id', Auth()->user()->customerIds())->pluck('location_id');
        $locations = Location::whereIn('id', $customerLocationIds)->get();

        return new LocationCollection($locations);
    }

    /**
     * Display the specified resource.
     *
     * @param  Location $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return new LocationResource($location);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new LocationCollection(
                app()->location->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new LocationCollection(
                app()->location->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->location->destroyBatch($request)
            )
        );
    }
}
