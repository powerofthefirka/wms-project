<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\InventoryLogCollection;
use App\Http\Resources\InventoryLogResource;
use App\Models\InventoryLog;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class InventoryLogController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(InventoryLog::class);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if ($user->user_role_id == UserRole::ROLE_ADMINISTRATOR) {
            $inventoryLogs = InventoryLog::paginate();
        } else {
            $userIds = app()->user->getAllCustomerUserIds($user);

            $inventoryLogs = InventoryLog::whereIn('user_id', array_unique($userIds))->paginate();
        }

        return new InventoryLogCollection($inventoryLogs);
    }

    /**
     * Display the specified resource.
     *
     * @param  InventoryLog $inventoryLog
     * @return \Illuminate\Http\Response
     */
    public function show(InventoryLog $inventoryLog)
    {
        return new InventoryLogResource($inventoryLog);
    }
}
