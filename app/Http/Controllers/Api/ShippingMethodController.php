<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ShippingMethod\StoreBatchRequest;
use App\Http\Requests\ShippingMethod\UpdateBatchRequest;
use App\Http\Requests\ShippingMethod\DestroyBatchRequest;
use App\Http\Resources\ShippingMethodResource;
use App\Http\Resources\ShippingMethodCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\CustomerShippingMethod;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class ShippingMethodController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(ShippingMethod::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return ShippingMethodCollection
     */
    public function index()
    {
        $customerShppingMethodIds = CustomerShippingMethod::whereIn('customer_id', Auth()->user()->customerIds())->pluck('shipping_method_id');
        $shippingMethods = ShippingMethod::whereIn('id', $customerShppingMethodIds)->where('is_visible', true)->get();

        return new ShippingMethodCollection($shippingMethods);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new ShippingMethodCollection(
                app()->shippingMethod->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new ShippingMethodCollection(
                app()->shippingMethod->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->shippingMethod->destroyBatch($request)
            )
        );
    }
}
