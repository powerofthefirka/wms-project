<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\InventoryChange\StoreBatchRequest;
use App\Http\Requests\InventoryChange\UpdateBatchRequest;
use App\Http\Requests\InventoryChange\DestroyBatchRequest;
use App\Http\Resources\InventoryChangeCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\InventoryChange;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class InventoryChangeController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(InventoryChange::class);
    }

    public function index()
    {
        $user = auth()->user();

        if ($user->user_role_id == UserRole::ROLE_ADMINISTRATOR) {
            $inventoryChange = InventoryChange::paginate();
        } else {
            $customerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();

            $inventoryChange = InventoryChange::with('product')->whereHas('product', function ($q) use ($customerIds) {
                $q->whereIn('customer_id', $customerIds);
            })->paginate();
        }

        return new InventoryChangeCollection($inventoryChange);
    }

    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new InventoryChangeCollection(
                app()->inventoryChange->storeBatch($request)
            )
        );
    }

    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new InventoryChangeCollection(
                app()->inventoryChange->updateBatch($request)
            )
        );
    }

    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->inventoryChange->destroyBatch($request)
            )
        );
    }
}
