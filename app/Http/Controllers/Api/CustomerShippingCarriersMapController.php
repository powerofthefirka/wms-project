<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CustomerShippingCarriersMap\StoreBatchRequest;
use App\Http\Requests\CustomerShippingCarriersMap\UpdateBatchRequest;
use App\Http\Requests\CustomerShippingCarriersMap\DestroyBatchRequest;
use App\Http\Resources\CustomerShippingCarriersMapCollection;
use App\Http\Resources\CustomerShippingCarriersMapResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\CustomerShippingCarriersMap;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class CustomerShippingCarriersMapController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(CustomerShippingCarriersMap::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();

        $shippingCarriersMaps = CustomerShippingCarriersMap::whereIn('customer_id', $customerIds)->paginate();

        return new CustomerShippingCarriersMapCollection($shippingCarriersMaps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new CustomerShippingCarriersMapCollection(
                app()->customerShippingCarriersMap->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  CustomerShippingCarriersMap $customerShippingCarriersMap
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerShippingCarriersMap $customerShippingCarriersMap)
    {
        return new CustomerShippingCarriersMapResource($customerShippingCarriersMap);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new CustomerShippingCarriersMapCollection(
                app()->customerShippingCarriersMap->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->customerShippingCarriersMap->destroyBatch($request)
            )
        );
    }
}
