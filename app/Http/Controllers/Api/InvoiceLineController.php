<?php

namespace App\Http\Controllers\Api;

use App\Models\UserRole;
use App\Models\InvoiceLine;
use App\Http\Controllers\ApiController;
use App\Http\Resources\InvoiceLineResource;
use App\Http\Resources\InvoiceLineCollection;
use App\Http\Requests\InvoiceLine\StoreBatchRequest;
use App\Http\Requests\InvoiceLine\UpdateBatchRequest;
use App\Http\Resources\InvoiceLineTableResource;
use App\Http\Requests\InvoiceLine\DestroyBatchRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceLineController extends ApiController
{

    public function __construct()
    {
        $this->authorizeResource(InvoiceLine::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $invoiceLines = InvoiceLine::paginate();

        return new InvoiceLineCollection($invoiceLines);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new InvoiceLineCollection(
                app()->invoiceLine->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceLine $invoiceLine)
    {
        return new InvoiceLineResource($invoiceLine);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new InvoiceLineCollection(
                app()->invoiceLine->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->invoiceLine->destroyBatch($request)
            )
        );
    }
}
