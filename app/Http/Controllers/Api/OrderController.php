<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Order\StoreBatchRequest;
use App\Http\Requests\Order\DestroyBatchRequest;
use App\Http\Requests\Order\UpdateBatchRequest;
use App\Http\Requests\Order\FilterRequest;
use App\Http\Requests\Shipment\ShipRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ShipmentResource;
use App\Http\Resources\OrderItemCollection;
use App\Http\Resources\RevisionCollection;
use App\Models\Order;
use App\Models\UserRole;
use App\Models\OrderItem;
use App\Http\Controllers\ApiController;

class OrderController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();
        $orders = Order::whereIn('customer_id', $customerIds)->paginate();

        return new OrderCollection($orders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new OrderCollection(
                app()->order->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new OrderCollection(
                app()->order->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->order->destroyBatch($request)
            )
        );
    }

    public function ship(ShipRequest $request, Order $order)
    {
        $this->authorize('ship', $order);

        return response()->json(
            new ShipmentResource(
                app()->shipment->ship($request, $order)
            )
        );
    }

    public function history(Order $order)
    {
        $this->authorize('history', $order);

        return new RevisionCollection(app()->order->history($order));
    }

    public function itemHistory(OrderItem $orderItem)
    {
        $this->authorize('itemHistory', $orderItem->order);

        return new RevisionCollection(app()->order->history($orderItem));
    }

    public function filter(FilterRequest $request)
    {
        $customerIds = Auth()->user()->customerIds();

        $orders = app()->order->filter($request, $customerIds);

        return new OrderCollection($orders);
    }
}
