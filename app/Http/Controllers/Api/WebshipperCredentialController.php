<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\WebshipperCredential\StoreBatchRequest;
use App\Http\Requests\WebshipperCredential\UpdateBatchRequest;
use App\Http\Requests\WebshipperCredential\DestroyBatchRequest;
use App\Http\Resources\WebshipperCredentialCollection;
use App\Http\Resources\WebshipperCredentialResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\WebshipperCredential;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class WebshipperCredentialController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(WebshipperCredential::class);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if ($user->user_role_id == UserRole::ROLE_ADMINISTRATOR) {
            $credentials = WebshipperCredential::paginate();
        } else {
            $customerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
            $credentials = WebshipperCredential::whereIn('customer_id', $customerIds)->paginate();
        }

        return new WebshipperCredentialCollection($credentials);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new WebshipperCredentialCollection(
                app()->webshipperCredential->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  WebshipperCredential $webshipperCredential
     * @return \Illuminate\Http\Response
     */
    public function show(WebshipperCredential $webshipperCredential)
    {
        return new WebshipperCredentialResource($webshipperCredential);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new WebshipperCredentialCollection(
                app()->webshipperCredential->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->webshipperCredential->destroyBatch($request)
            )
        );
    }
}
