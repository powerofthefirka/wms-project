<?php

namespace App\Http\Controllers\Api;

use App\Models\UserRole;
use App\Models\BillingProfile;
use App\Http\Controllers\ApiController;
use App\Http\Resources\BillingProfileResource;
use App\Http\Resources\BillingProfileCollection;
use App\Http\Requests\BillingProfile\StoreBatchRequest;
use App\Http\Requests\BillingProfile\UpdateBatchRequest;
use App\Http\Resources\BillingProfileTableResource;
use App\Http\Requests\BillingProfile\DestroyBatchRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BillingProfileController extends ApiController
{

    public function __construct()
    {
        $this->authorizeResource(BillingProfile::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $billingProfiles = BillingProfile::paginate();

        return new BillingProfileCollection($billingProfiles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new BillingProfileCollection(
                app()->billingProfile->storeBatch($request)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(BillingProfile $billingProfile)
    {
        return new BillingProfileResource($billingProfile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new BillingProfileCollection(
                app()->billingProfile->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->billingProfile->destroyBatch($request)
            )
        );
    }
}
