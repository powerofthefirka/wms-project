<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\DestroyBatchRequest;
use App\Http\Requests\Product\StoreBatchRequest;
use App\Http\Requests\Product\UpdateBatchRequest;
use App\Http\Requests\Product\FilterRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\RevisionCollection;
use App\Models\UserRole;
use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Product::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate();

        return new ProductCollection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new ProductCollection(
                app()->product->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new ProductCollection(
                app()->product->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->product->destroyBatch($request)
            )
        );
    }

    public function history(Product $product)
    {
        $this->authorize('history', $product);

        return new RevisionCollection(app()->product->history($product));
    }

    public function filter(FilterRequest $request)
    {
        $customerIds = Auth()->user()->customerIds();

        $products = app()->product->filter($request, $customerIds);

        return new ProductCollection($products);
    }
}
