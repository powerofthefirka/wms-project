<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ShippingCarrier\StoreBatchRequest;
use App\Http\Requests\ShippingCarrier\UpdateBatchRequest;
use App\Http\Requests\ShippingCarrier\DestroyBatchRequest;
use App\Http\Resources\ShippingCarrierResource;
use App\Http\Resources\ShippingCarrierCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ShippingCarrier;
use App\Models\UserRole;
use App\Http\Controllers\ApiController;

class ShippingCarrierController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(ShippingCarrier::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shippingCarriers = ShippingCarrier::whereIn('3pl_id', Auth()->user()->threePlIds())->get();

        return new ShippingCarrierCollection($shippingCarriers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBatchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new ShippingCarrierCollection(
                app()->shippingCarrier->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new ShippingCarrierCollection(
                app()->shippingCarrier->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->shippingCarrier->destroyBatch($request)
            )
        );
    }
}
