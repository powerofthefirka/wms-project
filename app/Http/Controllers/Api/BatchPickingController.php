<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PickingBatch\PickingBatchRequest;
use App\Http\Requests\PickingBatch\PickRequest;
use App\Http\Resources\PickingBatchResource;
use App\Models\PickingBatch;
use App\Models\Order;
use App\Models\OrderLock;
use App\Http\Controllers\ApiController;

class BatchPickingController extends ApiController
{
    public function pickingBatches(PickingBatchRequest $request)
    {
        $quantity = $request->quantity;
        $customerId = $request->customer_id;

        $user = auth()->user();

        $orderLockIds = OrderLock::get()->pluck('order_id')->toArray();

        $orders = Order::whereNotIn('id', $orderLockIds)->where('customer_id', $request->customer_id)->get();

        return response()->json(
            new PickingBatchResource(
                app()->routeOptimizer->createPickingBatch($request, $orders, $user)
            )
        );
    }

    public function pick(PickRequest $request, PickingBatch $pickingBatch){
        return response()->json(
            new PickingBatchResource(
                app()->routeOptimizer->pick($request, $pickingBatch)
            )
        );
    }
}
