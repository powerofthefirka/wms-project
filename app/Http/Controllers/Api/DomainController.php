<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Domain\DestroyBatchRequest;
use App\Http\Requests\Domain\StoreBatchRequest;
use App\Http\Requests\Domain\UpdateBatchRequest;
use App\Http\Requests\Domain\FilterRequest;
use App\Http\Resources\DomainCollection;
use App\Http\Resources\RevisionCollection;
use App\Models\UserRole;
use App\Models\Domain;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DomainController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Domain::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domains = Domain::paginate();

        return new DomainCollection($domains);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatchRequest $request)
    {
        return response()->json(
            new DomainCollection(
                app()->domain->storeBatch($request)
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatchRequest $request)
    {
        return response()->json(
            new DomainCollection(
                app()->domain->updateBatch($request)
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyBatchRequest $request)
    {
        return response()->json(
            new ResourceCollection(
                app()->domain->destroyBatch($request)
            )
        );
    }

    public function history(Domain $domain)
    {
        $this->authorize('history', $domain);

        return new RevisionCollection(app()->domain->history($domain));
    }

    public function filter(FilterRequest $request)
    {
        $domains = app()->domain->filter($request);

        return new DomainCollection($domains);
    }
}
