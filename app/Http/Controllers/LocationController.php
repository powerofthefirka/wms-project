<?php

namespace App\Http\Controllers;

use App\Http\Requests\Location\DestroyRequest;
use App\Http\Requests\Location\UpdateRequest;
use App\Http\Resources\LocationTableResource;
use App\Models\Location;
use App\Models\Warehouse;
use App\Models\Product;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Location::class);
    }

    public function index(Warehouse $warehouse)
    {
        return view('locations.index', [
            'warehouse' => $warehouse
        ]);
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $term = $request->get('search')['value'];

        $locationCollection = app()->location->search($term)
            ->join('warehouses', 'locations.warehouse_id', '=', 'warehouses.id')
            ->join('contact_informations', 'warehouses.id', '=', 'contact_informations.object_id')
            ->where('contact_informations.object_type', Warehouse::class)
            ->select('*', 'locations.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalLocationCount = Location::count();
            $locationCount = $locationCollection->count();
        } else {
            $totalLocationCount = $locationCollection->count();
            $locationCount = $totalLocationCount;
        }

        $locations = $locationCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $locationCollection = LocationTableResource::collection($locations);

        return response()->json([
            'data' => $locationCollection,
            'recordsTotal' => $totalLocationCount,
            'recordsFiltered' => $locationCount
        ]);
    }

    public function edit(Request $request, Warehouse $warehouse, Location $location)
    {
        return view('locations.edit', ['warehouse' => $warehouse, 'location' => $location]);
    }
}
