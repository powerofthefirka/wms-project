<?php

namespace App\Http\Controllers;

use App\Components\CamelIntegration;
use App\Currency;
use App\Http\Requests\Customer\DestroyRequest;
use App\Http\Requests\Customer\StoreRequest;
use App\Http\Requests\Customer\UpdateShipheroCredentialsRequest;
use App\Http\Requests\Customer\UpdateMessengerCredentialsRequest;
use App\Http\Requests\User\StoreRequest as StoreUserRequest;
use App\Http\Requests\Customer\UpdateRequest;
use App\Http\Requests\Customer\UpdateUsersRequest;
use App\Http\Requests\Webhook\StoreRequest as WebhookStoreRequest;
use App\Http\Requests\Webhook\UpdateRequest as WebhookUpdateRequest;
use App\Http\Resources\CustomerTableResource;
use App\Models\Customer;
use App\Models\CustomerUserRole;
use App\Models\Role;
use App\Models\ThreePl;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\BillingProfile;
use App\Models\Webhook;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Customer::class);
    }

    public function index()
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->isAdmin() && !$user->isCustomerAdmin()) {
            return abort(403);
        }

        return view('customers.index');
    }

    public function create()
    {
        $billingProfiles = BillingProfile::all();
        $currencies = Currency::pluck('name', 'id');

        return view('customers.create', [
            'parentCustomer' => Customer::find(session('customer_id')),
            'billingProfiles' => $billingProfiles,
            'currencies' => $currencies
        ]);
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');
        $includeIntegrationDates = $request->get('include_integration_dates') ?? false;

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $term = $request->get('search')['value'];

        $customerCollection = app()->customer->search($term)
            ->join('contact_informations', function ($join) {
                $join->on('customers.id', '=', 'contact_informations.object_id')
                    ->where('contact_informations.object_type', Customer::class);
            })
            ->select('*', 'customers.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalCustomerCount = Customer::count();
            $customerCount = $customerCollection->count();
        } else {
            $totalCustomerCount = $customerCollection->count();
            $customerCount = $totalCustomerCount;
        }

        $customers = $customerCollection->skip($request->get('start'))->limit($request->get('length'))->get();

        if ($includeIntegrationDates) {
            $integrationDates = Customer::getAllIntegrationDates();

            foreach ($customers as &$customer) {
                if (!empty($integrationDates[$customer->id])) {
                    $customer->integration_dates = $integrationDates[$customer->id];
                }
            }
        }

        $customerCollection = CustomerTableResource::collection($customers);

        return response()->json([
            'data' => $customerCollection,
            'recordsTotal' => $totalCustomerCount,
            'recordsFiltered' => $customerCount,
        ]);
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $storeCustomerRequest = StoreRequest::make($input);
        $customer = app()->customer->store($storeCustomerRequest);

        $input['email'] = Arr::get($input, 'contact_information.email');
        $input['customer_id'] = $customer->id;
        $input['user_role_id'] = CustomerUserRole::ROLE_DEFAULT;

        if ($user = User::where('email', $input['email'])->first()) {
            $user->customers()->attach($customer->id, [
                'role_id' => CustomerUserRole::ROLE_DEFAULT
            ]);
        } else {
            $storeUserRequest = StoreUserRequest::make($input);
            app()->user->store($storeUserRequest);
        }

        $user = $customer->users()->first();
        $user->createToken('API');
        $accessToken = $user->printableTokens()->first()->accessToken['access_token'];

        $response = CamelIntegration::registerIntegration(
            $customer,
            $accessToken,
            Arr::get($input, 'shiphero_username'),
            Arr::get($input, 'shiphero_password'),
            Arr::get($input, 'shiphero_refresh_token'),
            Arr::get($input, 'shiphero_access_token'),
            Arr::get($input, 'sync_date_from')
        );

        if (!empty(Arr::get($response, 'local_key'))) {
            $customer->local_key = Arr::get($response, 'local_key');
            $customer->active_integration = 1;
            $customer->save();
        }

        //@TODO - debug if resource leak
/*        if (!empty(Arr::get($response, 'webhook_data'))) {
            foreach (Arr::get($response, 'webhook_data') as $webhookData) {
                $webhookData = json_decode($webhookData, true);

                $storeWebhookRequest = WebhookStoreRequest::make($webhookData);
                app()->webhook->store($storeWebhookRequest);
            }
        }*/

        return redirect()->back()->withStatus(__('Customer successfully created.'));
    }

    public function edit(Request $request, Customer $customer)
    {
        $customerUsersIds = $customer->users()->pluck('users.id')->toArray();
        $allUsers = User::all();
        $routeName = $request->route()->getName();
        $allRoles = CustomerUserRole::all();
        $billingProfiles = BillingProfile::where('3pl_id', $customer->threePl->id)->get();
        $currencies = Currency::pluck('name', 'id');
        $threePlShippingMethods =  app()->shippingMethod->get3plShippingMethods($customer->threePl);
        $threePlLocations =  app()->location->get3plLocations($customer->threePl);

        $data = ['customer' => $customer, 'allUsers' => $allUsers, 'customerUsersIds' => $customerUsersIds, 'roles' => $allRoles, 'billingProfiles' => $billingProfiles, 'currencies' => $currencies, 'threePlShippingMethods' => $threePlShippingMethods, 'threePlLocations' => $threePlLocations];
        if ($routeName === "customers.edit_users") {
            return view('customers.editUsers', $data);
        } else if (($routeName === "customers.create_user")){
            return view('customers.createUser', $data);
        } else {
            return view('customers.editCustomer', $data);
        }
    }

    public function update(UpdateRequest $request, Customer $customer)
    {
        app()->customer->update($request, $customer, true);

        //@TODO - debug if resource leak
//        app()->customer->update($request, $customer, true);

        /*$response = CamelIntegration::updateIntegrationWebhooks(
            $customer->local_key
        );

        if (!empty(Arr::get($response, 'webhook_data'))) {
            foreach (Arr::get($response, 'webhook_data') as $webhookData) {
                $webhookData = json_decode($webhookData, true);

                if ($webhook = Webhook::where('customer_id', $webhookData['customer_id'])->where('name', $webhookData['name'])->first()) {
                    $updateWebhookRequest = WebhookUpdateRequest::make($webhookData);
                    app()->webhook->update($updateWebhookRequest, $webhook);
                } else {
                    $storeWebhookRequest = WebhookStoreRequest::make($webhookData);
                    app()->webhook->store($storeWebhookRequest);
                }
            }
        }*/

        return redirect()->back()->withStatus(__('Customer successfully updated.'));
    }

    public function storeUser(StoreUserRequest $request, Customer $customer)
    {
        app()->user->store($request);

        return redirect()->route('customers.edit_users',['customer' => $customer])->withStatus(__('User successfully created.'));
    }

    public function editShiphero(Request $request, Customer $customer)
    {
        return view('customers.editShipheroCredentials', compact('customer'));
    }

    public function toggleIntegrationStatus(Customer $customer)
    {
        if (empty($customer->local_key)) {
            return redirect()->route('customers.index')->withStatus(__('Customer is not linked to any integration.'));
        }

        if (empty($customer->local_key)) {
            return redirect()->route('customers.index')->withStatus(__('Customer is missing local key'));
        }

        $response = CamelIntegration::updateIntegrationStatus($customer->local_key, $customer->active_integration ? 0 : 1);

        if (empty($response)) {
            return redirect()->route('customers.index')->withStatus(__('No response from server was received.'));
        }

        $integrationStatus = Arr::get($response, 'active', 0);

        if (empty($customer->active)) {
            $integrationStatus = 0;
        }

        $customer->active_integration = $integrationStatus;
        $customer->save();

        return redirect()->route('customers.index')->withStatus(__('Customer integration is now ' . ($customer->active_integration ? 'active' : 'disabled')));
    }

    public function disable(Customer $customer)
    {
        app()->customer->toggleCustomerStatus($customer, 0);

        return redirect()->route('customers.index')->withStatus(__('Customer "' . ( $customer->contactInformation->name ?? '' ) . '" disabled!'));
    }

    public function enable(Customer $customer)
    {
        app()->customer->toggleCustomerStatus($customer, 1);

        return redirect()->route('customers.index')->withStatus(__('Customer "' . ( $customer->contactInformation->name ?? '' ) . '" enabled!'));
    }

    public function updateShipheroCredentials(UpdateShipheroCredentialsRequest $request, Customer $customer)
    {
        $input = $request->validated();

        $user = $customer->users()->first();
        $accessToken = $user->printableTokens()->first()->accessToken['access_token'] ?? false;
        if (empty($accessToken)) {
            $user->createToken('API');
            $accessToken = $user->printableTokens()->first()->accessToken['access_token'];
        }

        $response = CamelIntegration::updateIntegration(
            $customer,
            $accessToken,
            Arr::get($input, 'shiphero_username'),
            Arr::get($input, 'shiphero_password'),
            Arr::get($input, 'shiphero_refresh_token'),
            Arr::get($input, 'shiphero_access_token'),
            Arr::get($input, 'sync_date_from'),
            $customer->local_key
        );

        $defaultStatus = 0;
        if (!empty(Arr::get($response, 'local_key'))) {
            $customer->local_key = Arr::get($response, 'local_key');
            $defaultStatus = 1;
        }

        $customer->active_integration = Arr::get($response, 'active', $defaultStatus);
        $customer->save();

        return redirect()->route('customers.index')->withStatus(__('Customer\'s shiphero credentials was updated successfully.'));
    }

    public function editMessenger(Request $request, Customer $customer)
    {
        return view('customers.editMessengerCredentials', compact('customer'));
    }

    public function updateMessengerCredentials(UpdateMessengerCredentialsRequest $request, Customer $customer)
    {
        $input = $request->validated();

        $customer->messaging_provider = $input['messaging_provider'];
        $customer->widget_token = $input['widget_token'];
        $customer->save();

        return redirect()->route('customers.index')->withStatus(__('Customer\'s Messenger credentials was updated successfully.'));
    }

    public function destroy(DestroyRequest $request, Customer $customer)
    {
        if (!empty($customer->local_key)) {
            CamelIntegration::updateIntegrationStatus($customer->local_key, 0);
        }

        app()->customer->destroy($request, $customer);

        return redirect()->route('customers.index')->withStatus(__('Customer successfully deleted.'));
    }

    public function detachUser(Customer $customer, User $user)
    {
        $this->authorize('updateUsers', $customer);

        app()->customer->detachUser($customer, $user);

        return redirect()->back()->withStatus(__('Customer successfully updated.'));
    }

    public function updateUsers(UpdateUsersRequest $request, Customer $customer)
    {
        $this->authorize('updateUsers', $customer);

        app()->customer->updateUsers($request, $customer);

        return redirect()->back()->withStatus(__('Customer successfully updated.'));
    }

    public function filterUsers(Request $request, Customer $customer)
    {
        return app()->customer->filterUsers($request, $customer);
    }

    public function shippingCarriers(Request $request, Customer $customer)
    {
        $threePlIds = [$customer->threePl->id];

        return app()->shippingCarrier->getShippingCarriers($request, $threePlIds);
    }

    public function getShippingMethods(Customer $customer)
    {
        return app()->customer->getShippingMethods($customer);
    }

    public function getCurrencyAndUnit(Customer $customer)
    {
        return response()->json(app()->customer->getCurrencyAndUnit($customer));
    }

    public function filter3Pls(Request $request)
    {
        return app()->customer->filter3Pls($request);
    }

    public function filterCurrencies(Request $request)
    {
        return app()->customer->filterCurrencies($request);
    }

    public function settings()
    {
        return view('customers.settings');
    }

    public function updateSettings(Request $request)
    {
        $input = $request->input();
        $input['custom_shipping_modal'] = isset($input['custom_shipping_modal']);

        auth()->user()->threePls->first()->update($input);

        return redirect()->back()->withStatus(__('3PL Settings successfully updated.'));
    }
}
