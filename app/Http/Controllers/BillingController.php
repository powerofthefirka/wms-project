<?php

namespace App\Http\Controllers;

use App\Components\BillComponent;
use App\Http\Resources\BillsTableResource;
use App\Http\Resources\CustomerBillsTableResource;
use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Customer;
use App\Models\Shipment;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BillingController extends Controller
{
    public function index()
    {
        return redirect()->route('billings.customers');
    }

    public function customers()
    {
        return view('billings.customers.index');
    }

    public function customerBillsEdit(Customer $customer)
    {
        $latestBill = Bill::where('billing_profile_id', $customer->billing_profile_id)->where('customer_id', $customer->id)->orderBy('period_end', 'desc')->first();
        $lastestLastDate = $latestBill
            ? Carbon::parse($latestBill['period_end'])->format('Y-m-d')
            : Carbon::now()->subYear();

        return view('billings.customers.bills', [
            'customer' => $customer,
            'lastBillEndDate' => $lastestLastDate
        ]);
    }

    public function customerBillsDataTable(Request $request, Customer $customer)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'period_end'
        );

        $term = $request->input('search.value');

        $billsCollection = $customer->bills()
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalBillsCount = $customer->bills()->count();
            $billsCount = $billsCollection->count();
        } else {
            $totalBillsCount = $billsCollection->count();
            $billsCount = $totalBillsCount;
        }

        $bills = $billsCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $billsCollection = CustomerBillsTableResource::collection($bills);

        return response()->json([
            'data' => $billsCollection,
            'recordsTotal' => $totalBillsCount,
            'recordsFiltered' => $billsCount
        ]);
    }

    public function customerBillItems(Customer $customer, Bill $bill)
    {
        return view('billings.customers.billItems', [
            'bill' => $bill,
            'customer' => $customer,
            'adHocs' => BillingFee::where('type', 'ad_hoc')->get(),
            'shipping_carriers' => ShippingCarrier::pluck('name', 'id'),
            'shipping_methods' => ShippingMethod::pluck('name', 'id')
        ]);
    }

    public function bills()
    {
        return view('billings.bills.index');
    }

    public function billsDataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'period_end'
        );

        $term = $request->input('search.value');

        $billsCollection = Bill::query()->orderBy($tableColumnName, $sortDirection)
            ->whereIn('customer_id', auth()->user()->customerIds());

        if ($term) {
            $totalBillsCount = Bill::count();
            $billsCount = $billsCollection->count();
        } else {
            $totalBillsCount = $billsCollection->count();
            $billsCount = $totalBillsCount;
        }

        $bills = $billsCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $billsCollection = BillsTableResource::collection($bills);

        return response()->json([
            'data' => $billsCollection,
            'recordsTotal' => $totalBillsCount,
            'recordsFiltered' => $billsCount
        ]);
    }

    public function billingProfiles()
    {
        return view('billings.billing_profiles.index');
    }

    public function reconcile()
    {
        return view('billings.reconcile.index');
    }

    public function exports()
    {
        return view('billings.exports.index');
    }

    public function productProfiles()
    {
        return view('billings.product_profiles.index', [
            'customers' => Customer::whereIn('id', auth()->user()->customerIds())->get()
        ]);
    }

    public function unbilled(Bill $bill)
    {
        return view('billings.customers.unbilled', [
            'billId' => $bill->id
        ]);
    }

    public function unBilledShipments(Request $request, Bill $bill)
    {
        return response()->json(BillComponent::unBilledShipments($request, $bill));
    }

    public function unBilledPurchaseOrders(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledPurchaseOrders($request, $bill));
    }

    public function unBilledPurchaseOrderItems(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledPurchaseOrderItems($request, $bill));
    }

    public function unBilledLocations(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledLocations($request, $bill));
    }

    public function unBilledReturnItems(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledReturnItems($request, $bill));
    }

    public function unBilledPackages(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledPackages($request, $bill));
    }

    public function unBilledPackageItems(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledPackageItems($request, $bill));
    }

    public function unBilledInventoryChanges(Request $request,Bill $bill)
    {
        return response()->json(BillComponent::unBilledInventoryChanges($request, $bill));
    }
}
