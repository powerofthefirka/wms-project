<?php

namespace App\Http\Controllers;

use App\Models\Return_;
use App\Models\Customer;
use App\Models\OrderItem;
use App\Models\ReturnItem;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Return_\StoreRequest;
use App\Http\Resources\ReturnTableResource;
use App\Components\ShippingCarrierComponent;
use App\Http\Requests\Return_\UpdateRequest;
use App\Http\Requests\Return_\DestroyRequest;
use App\Http\Requests\Return_\ReceiveBatchRequest;
use Illuminate\Support\Facades\DB;

class ReturnController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Return_::class);

        foreach ($this->middleware as $key => $value) {
            if (isset($value['middleware']) && $value['middleware'] == 'can:view,return_') {
                $this->middleware[$key]['middleware'] = 'can:view,return';
            }

            if (isset($value['middleware']) && $value['middleware'] == 'can:update,return_') {
                $this->middleware[$key]['middleware'] = 'can:update,return';
            }

            if (isset($value['middleware']) && $value['middleware'] == 'can:delete,return_') {
                $this->middleware[$key]['middleware'] = 'can:delete,return';
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('returns.index', [
            'hideColumns' => get_hidden_columns(Return_::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'returns.number'
        );

        $term = $request->input('search.value');

        $returnOrdersCollection = app()->return->search($term)->join('orders', 'returns.order_id', '=', 'orders.id')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('contact_informations', function ($join) {
                $join->on('customers.id', '=', 'contact_informations.object_id')
                    ->where('contact_informations.object_type', Customer::class);
            })
            ->select('*', 'returns.*')
            ->orderBy($tableColumnName, $sortDirection);
        $term = $request->get('search')['value'];

        if ($term) {
            $totalReturnOrderCount = Return_::count();
            $returnOrderCount = $returnOrdersCollection->count();
        } else {
            $totalReturnOrderCount = $returnOrdersCollection->count();
            $returnOrderCount = $totalReturnOrderCount;
        }

        $orders = $returnOrdersCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $returnCollection = ReturnTableResource::collection($orders);


        return response()->json([
            'data' => $returnCollection,
            'recordsTotal' => $totalReturnOrderCount,
            'recordsFiltered' => $returnOrderCount,
        ]);
    }

    public function preview(Return_ $return)
    {
        return response(view('returns.preview', ['return' => $return]));
    }

    public function editablePreview(Return_ $return)
    {
        return response(view('returns.editablePreview', ['return' => $return]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $customers = Auth()->user()->customerIds();

        $carriers = [];

        if ($customers) {
            $carriers = ShippingCarrierComponent::getShippingCarriersAndMethods($customers);
        }

        return view('returns.create', ['carriers' => $carriers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->return->store($request);

        return redirect()->route('returns.index')->withStatus(__('Return successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Return_ $return
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Return_ $return)
    {
        $customers = Auth()->user()->customerIds();

        $carriers = [];

        if ($customers) {
            $carriers = ShippingCarrierComponent::getShippingCarriersAndMethods($customers);
        }

        return view('returns.edit', ['return' => $return, 'carriers' => $carriers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Return_ $return)
    {
        app()->return->update($request, $return);

        $record = [];
        $order_item = null;
        $productId = null;

        if (!empty($request['return_items'])) {
            foreach ($request['return_items'] as $index => $item) {
                if (!empty($item['destination_id']) && !empty($item['quantity_to_receive'])) {
                    if (!isset($item['return_item_id'])) {
                        $order_item = ReturnItem::where('order_item_id', $item['order_item_id'])->where('return_id', $return->id)->latest()->first();
                    }

                    if ($itemInfo = OrderItem::find($item['order_item_id'])) {
                        $productId = $itemInfo->product_id;
                    }

                    $record[] = [
                        'source_id' => $return->id,
                        'source_type' => Return_::class,
                        'destination_id' => $item['destination_id'],
                        'destination_type' => Location::class,
                        'user_id' => Auth::user()->id,
                        'product_id' => $productId,
                        'quantity' => $item['quantity_to_receive'],
                        'return_item_id' => $item['return_item_id'] ?? $order_item->id,
                        'quantity_received' => $item['quantity_to_receive'],
                        'location_id' => $item['destination_id'],
                    ];
                }
            }
        }

        app()->return->receiveBatch(
            ReceiveBatchRequest::make($record),
            $return
        );
        $message = __('Return successfully updated.');
        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(DestroyRequest $request, Return_ $return)
    {
        app()->return->destroy($request, $return);

        return redirect()->route('returns.index')->withStatus(__('Return successfully deleted.'));
    }

    public function filterOrders(Request $request, Customer $customer)
    {
        return app()->return->filterOrders($request, $customer);
    }

    public function filterOrderProducts(Request $request, $orderId)
    {
        return app()->return->filterOrderProducts($request, $orderId);
    }

    public function getOrderProducts(Request $request, $orderId)
    {
        return app()->return->getOrderProducts($request, $orderId);
    }

    public function filterLocations(Request $request)
    {
        return app()->return->filterLocations($request);
    }

    public function totalReturnsByStatuses(Request $request)
    {
        $date = $request->input('date_filter', '');
        $array = [
            "filterArray" => [
                ["columnName" => "dates_between", "value" => $date],
                ["columnName" => "table_search", "value" => null]
            ]
        ];

        $total = app()->return->search(json_encode($array))->count();

        $statuses = app()->return->search(json_encode($array))
            ->groupBy('status')
            ->select(
                'status',
                DB::raw('count(id) as count')
            )
            ->get()
            ->toArray();

        foreach ($statuses as $key => $status) {
            if ($status['status'] == 'pending') {
                $statuses[$key]['icon'] = Return_::STATUS_ICONS['pending'];
            } else if ($status['status'] == 'warehouse_complete') {
                $statuses[$key]['icon'] = Return_::STATUS_ICONS['warehouse_complete'];
            } else if ($status['status'] == 'complete') {
                $statuses[$key]['icon'] = Return_::STATUS_ICONS['complete'];
            }
        }

        array_unshift($statuses, array(
            'status' => 'All returns',
            'icon' => Return_::STATUS_ICONS['all'],
            'count' => $total,
            'skip_value' => true
        ));

        return response(view('shared.totalObjectsByStatuses', [
            'statuses' => $statuses,
            'plural' => 'Returns',
            'singular' => 'Return',
        ]));
    }
}
