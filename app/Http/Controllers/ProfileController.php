<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccessTokenRequest;
use App\Http\Requests\ThemeRequest;
use App\Models\UserSetting;
use App\Models\Webhook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Laravel\Passport\Token;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $timezones = array();
        $timestamp = time();

        foreach(timezone_identifiers_list(\DateTimeZone::ALL) as $key => $t) {
            date_default_timezone_set($t);
            $timezones[$key]['zone'] = $t;
            $timezones[$key]['GMT_difference'] =  date('P', $timestamp);
        }

        $timezones = collect($timezones)->sortBy('GMT_difference');

        $webhooks = Webhook::whereIn('customer_id', auth()->user()->customerIds())->get();

        return view('profile.edit', [
            'webhooks' => $webhooks,
            'timezones' => $timezones,
            'userTimezone' => get_timezone(),
            'dateFormat' => get_date_format()
        ]);
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        $settings = $request->only('timezone', 'date_format');

        auth()->user()->contactInformation->update($request->only('name'));
        auth()->user()->update($request->only('email'));

        UserSetting::saveSettings($settings);

        return redirect()->route('profiles.edit', ['#user-information'])->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return redirect()->route('profiles.edit', ['#user-information'])->withPasswordStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function theme(ThemeRequest $request)
    {
        /*auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return redirect()->route('profile.edit', [ '#user-information' ])->withPasswordStatus(__('Profile successfully updated.'));*/
        //        CustomerController::where('id')

        $customer = DB::table('customers')->get()->where('id', auth()->id())->first();
        DB::update('update customers set ');
        //        dd($users);
    }

    /**
     * Update tokens
     *
     * @param AccessTokenRequest $request
     * @return mixed
     */
    public function accessTokens(AccessTokenRequest $request)
    {
        app()->user->accessTokens($request);

        return redirect()->route('profiles.edit', ['#access-tokens'])->withAccessTokenStatus(__('Access tokens successfully updated.'));
    }

    public function destroy(Token $token)
    {
        app()->user->deleteAccessToken($token);

        return redirect()->route('profiles.edit', ['#access-tokens'])->withAccessTokenStatus(__('Access tokens successfully updated.'));
    }

    public function emailNotifications(Request $request)
    {
        auth()->user()->email_notifications = json_encode(array_keys($request->input('email_notifications')));
        auth()->user()->save();

        return redirect()->back()->with('status', 'Email notifications updated successfully.');
    }
}
