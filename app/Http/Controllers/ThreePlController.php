<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\ThreePl\StoreRequest;
use App\Http\Requests\ThreePl\UpdateRequest;
use App\Http\Requests\ThreePl\DestroyRequest;
use App\Http\Requests\ThreePl\UpdateUsersRequest;
use App\Http\Requests\ThreePl\SetPricingPlanRequest;
use App\Http\Resources\ThreePlTableResource;
use App\Http\Resources\ThreePlBillingReportTableResource;
use Illuminate\Support\Arr;
use App\Models\ThreePl;
use App\Models\User;
use App\Models\PricingPlan;
use App\Models\ThreePlPricingPlan;
use App\Models\Order;
use DB;

class ThreePlController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ThreePl::class);
    }

    public function index()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }

        return view('3pls.index', [
            'hideColumns' => get_hidden_columns(ThreePl::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'contact_informations.name'
        );

        $term = $request->input('search.value');

        $threePlCollection = app()->threePl->search($term)
            ->leftJoin('contact_informations', function ($join) {
                $join->on('3pls.id', '=', 'contact_informations.object_id')
                    ->where('contact_informations.object_type', ThreePl::class);
            })
            ->select('*', '3pls.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalThreePlCount = ThreePl::count();
            $threePlCount = $threePlCollection->count();
        } else {
            $totalThreePlCount = $threePlCollection->count();
            $threePlCount = $totalThreePlCount;
        }

        $threePls = $threePlCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $threePlCollection = ThreePlTableResource::collection($threePls);

        return response()->json([
            'data' => $threePlCollection,
            'recordsTotal' => $totalThreePlCount,
            'recordsFiltered' => $threePlCount
        ]);
    }

    public function create()
    {
        return view('3pls.create');
    }

    public function store(StoreRequest $request)
    {
        app()->threePl->store($request);

        return redirect()->route('three_pls.index')->withStatus(__('3PL successfully created.'));
    }

    public function edit(Request $request, ThreePl $threePl)
    {
        $routeName = $request->route()->getName();

        if ($routeName === "three_pls.edit_users") {
            return view('3pls.editUsers', ['threePl' => $threePl]);
        } else if (($routeName === "three_pls.edit_pricing_plan")){
            $threePlPricingPlan = ThreePlPricingPlan::where('3pl_id', $threePl->id)->first();
            $pricingPlans = PricingPlan::get();

            return view('3pls.setPricingPlan', ['threePl' => $threePl, 'threePlPricingPlan' => $threePlPricingPlan, 'pricingPlans' => $pricingPlans]);
        } else if (($routeName === "three_pls.billing_report" && $threePl->pricingPlan)){

            $starDaterange = '';
            if (!empty($threePl->pricingPlan->invoice_start_date->format('Y-m-d'))) {
                $starDaterange = $threePl->pricingPlan->invoice_start_date->format('Y-m-d') . ' - ' . Carbon::parse($threePl->pricingPlan->invoice_start_date->format('Y-m-d'))->addMonth()->format('Y-m-d');
            }

            $startEndDates = app()->threePl->getBillingReportStartEndDates($starDaterange);
            $totalOrdersTotalPrice = app()->threePl->getBillingReportTotalOrdersTotalPrice($threePl, $startEndDates);

            return view('3pls.billingReport', ['threePl' => $threePl, 'totalOrdersTotalPrice' => $totalOrdersTotalPrice]);
        } else {
            return view('3pls.edit3pl', ['threePl' => $threePl]);
        }
    }

    public function update(UpdateRequest $request, ThreePl $threePl)
    {
        app()->threePl->update($request, $threePl);

        return redirect()->back()->withStatus(__('3PL successfully updated.'));
    }

    public function detachUser(ThreePl $threePl, User $user)
    {
        $this->authorize('updateUsers', $threePl);

        app()->threePl->detachUser($threePl, $user);

        return redirect()->back()->withStatus(__('3PL successfully updated.'));
    }

    public function updateUsers(UpdateUsersRequest $request, ThreePl $threePl)
    {
        $this->authorize('updateUsers', $threePl);

        app()->threePl->updateUsers($request, $threePl);

        return redirect()->back()->withStatus(__('3PL successfully updated.'));
    }

    public function filterUsers(Request $request, ThreePl $threePl)
    {
        return app()->threePl->filterUsers($request, $threePl);
    }

    public function destroy(DestroyRequest $request, ThreePl $threePl)
    {
        app()->threePl->destroy($request, $threePl);

        return redirect()->route('three_pls.index')->withStatus(__('3PL successfully deleted.'));
    }

    public function disable(ThreePl $threePl)
    {
        app()->threePl->toggleStatusThreePLStatus($threePl, 0);

        return redirect()->route('three_pls.index')->withStatus(__( '3PL "' . $threePl->contactInformation->name . '" disabled.'));
    }

    public function enable(ThreePl $threePl)
    {
        app()->threePl->toggleStatusThreePLStatus($threePl, 1);

        return redirect()->route('three_pls.index')->withStatus(__( '3PL "' . $threePl->contactInformation->name . '" enabled.'));
    }

    public function updatePricingPlan(SetPricingPlanRequest $request, ThreePl $threePl)
    {
        app()->threePl->updatePricingPlan($request, $threePl);

        return redirect()->back()->withStatus(__('Pricing plan successfully updated.'));
    }

    public function getPricingPlan(ThreePl $threePl, $pricingPlanName)
    {
        return app()->threePl->getPricingPlanDetails($threePl, $pricingPlanName);
    }

    public function getMonthlyPrice(ThreePl $threePl, $pricingPlanName, $billingPeriod)
    {
        return app()->threePl->getMonthlyPrice($threePl, $pricingPlanName, $billingPeriod);
    }

    public function billingReportDataTable(Request $request, ThreePl $threePl)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'contact_informations.name'
        );

        $term = $request->input('search.value');
        $filters = json_decode($term);
        $datesValue = "";

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $datesValue = $filter->value;
                }
            }
        }

        $startEndDates = app()->threePl->getBillingReportStartEndDates($datesValue);

        $orderCollection = Order::whereIn('customer_id', $threePl->customers->pluck('id')->toArray())
            ->whereBetween('ordered_at', [utc_date_time($startEndDates['startDate']), utc_date_time($startEndDates['endDate'])])
            ->select('customer_id', DB::raw('count(*) as total_order'))
            ->groupBy('customer_id');

        $totalOrderCount = $orderCollection->get()->count();
        $orderCount = $totalOrderCount;

        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $orderCollection = ThreePlBillingReportTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function getBillingReportOrdersPrice(Request $request, ThreePl $threePl)
    {
        $startEndDates = app()->threePl->getBillingReportStartEndDates($request->dates_between);

        return app()->threePl->getBillingReportTotalOrdersTotalPrice($threePl, $startEndDates);
    }

    public function exportBillingReportToCsv(Request $request, ThreePl $threePl)
    {
        return app()->threePl->exportBillingReportToCsv($request, $threePl);
    }

    public function getAllShippingMethods(ThreePl $threePl)
    {
        $shippingMethods =  app()->shippingMethod->get3plShippingMethods($threePl);

        $results = [];

        foreach ($shippingMethods as $method) {
            $results[] = [
                'id' => $method->id,
                'text' => $method->name,
                'cost' => $method->cost
            ];
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function getAllLocations(ThreePl $threePl)
    {
        $locations =  app()->location->get3plLocations($threePl);

        $results = [];

        foreach ($locations as $location) {
            $results[] = [
                'id' => $location->id,
                'text' => $location->name,
                'pickable' => $location->pickable
            ];
        }

        return response()->json([
            'results' => $results
        ]);
    }
}
