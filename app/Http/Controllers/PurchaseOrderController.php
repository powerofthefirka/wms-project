<?php

namespace App\Http\Controllers;

use App\Exports\PurchaseOrderExport;
use App\Http\Requests\PurchaseOrder\DestroyRequest;
use App\Http\Requests\PurchaseOrder\ReceiveBatchRequest;
use App\Http\Requests\PurchaseOrder\StoreRequest;
use App\Http\Requests\PurchaseOrder\UpdateRequest;
use App\Http\Resources\PurchaseOrderTableResource;
use App\Http\Resources\ObjectChangeTableResource;
use App\Imports\PurchaseOrderImport;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use App\Models\ObjectChange;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PurchaseOrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(PurchaseOrder::class);
    }

    public function index()
    {
        return view('purchase_orders.index', [
            'hideColumns' => get_hidden_columns(PurchaseOrder::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'number'
        );

        $term = $request->get('search')['value'];

        $purchaseOrdersCollection = app()->purchaseOrder->search($term)
            ->leftJoin('suppliers', 'purchase_orders.supplier_id', '=', 'suppliers.id')
            ->leftJoin('contact_informations AS supplier_contact_information', function ($join) {
                $join->on('suppliers.id', '=', 'supplier_contact_information.object_id')
                    ->where('supplier_contact_information.object_type', Supplier::class);
            })
            ->leftJoin('warehouses', 'purchase_orders.warehouse_id', '=', 'warehouses.id')
            ->leftJoin('contact_informations AS warehouse_contact_information', 'warehouses.id', '=', 'warehouse_contact_information.object_id')
            ->where('warehouse_contact_information.object_type', Warehouse::class)
            ->select('*', 'purchase_orders.*')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalPurchaseOrderCount = PurchaseOrder::count();
            $purchaseOrderCount = $purchaseOrdersCollection->count();
        } else {
            $totalPurchaseOrderCount = $purchaseOrdersCollection->count();
            $purchaseOrderCount = $totalPurchaseOrderCount;
        }

        $purchaseOrders = $purchaseOrdersCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $orderCollection = PurchaseOrderTableResource::collection($purchaseOrders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalPurchaseOrderCount,
            'recordsFiltered' => $purchaseOrderCount,
        ]);
    }

    public function preview(PurchaseOrder $purchaseOrder)
    {
        return response(view('purchase_orders.preview', ['purchaseOrder' => $purchaseOrder]));
    }

    public function editablePreview(PurchaseOrder $purchaseOrder)
    {
        return response(view('purchase_orders.editablePreview', ['purchaseOrder' => $purchaseOrder]));
    }

    public function create()
    {
        return view('purchase_orders.create');
    }

    public function store(StoreRequest $request)
    {
        $productSelected = false;

        if (isset($request['purchase_order_items'])) {
            foreach ($request['purchase_order_items'] as $selectedItem) {
                if (isset($selectedItem['product_id']) && $selectedItem['quantity'] > 0) {
                    $productSelected = true;
                    break;
                }
            }
        }

        if (!$productSelected) {
            return redirect()->route('purchase_orders.create')->withInput()->withErrors(__('Purchase order item is required'));
        }

        app()->purchaseOrder->store($request);

        return redirect()->route('purchase_orders.index')->withStatus(__('Order successfully created.'));
    }

    public function edit(PurchaseOrder $purchaseOrder)
    {
        return view(
            'purchase_orders.edit',
            [
                'purchaseOrder' => $purchaseOrder,
                'editPage' => true
            ]
        );
    }

    public function update(UpdateRequest $request, PurchaseOrder $purchaseOrder)
    {
        $input = $request->validated();

        app()->purchaseOrder->update(UpdateRequest::make($input), $purchaseOrder);

        $record = [];
        $purchase_order_item = null;

        foreach (Arr::get($input, 'purchase_order_items') as $index => $item) {
            if (!empty($item['destination_id']) && !empty($item['quantity_to_receive'])) {
                if (!isset($item['purchase_order_item_id'])) {
                    $purchase_order_item = PurchaseOrderItem::where('product_id', $item['product_id'])->where('purchase_order_id', $purchaseOrder->id)->first();
                }
                $record[] = [
                    'purchase_order_item_id' => $item['purchase_order_item_id'] ?? $purchase_order_item->id,
                    'quantity_received' => $item['quantity_to_receive'],
                    'location_id' => $item['destination_id'],
                ];
            }
        }

        app()->purchaseOrder->receiveBatch(
            ReceiveBatchRequest::make($record),
            $purchaseOrder
        );

        $message = __('Purchase Order successfully updated.');

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(DestroyRequest $request, PurchaseOrder $purchaseOrder)
    {
        app()->purchaseOrder->destroy($request, $purchaseOrder);

        return redirect()->route('purchase_orders.index')->withStatus(__('Purchase Order successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->purchaseOrder->filterCustomers($request);
    }

    public function filterWarehouses(Request $request, Customer $customer)
    {
        return app()->purchaseOrder->filterWarehouses($request, $customer);
    }

    public function filterSuppliers(Request $request, Customer $customer)
    {
        return app()->purchaseOrder->filterSuppliers($request, $customer);
    }

    public function filterProducts(Request $request, Supplier $supplier)
    {
        return app()->purchaseOrder->filterProducts($request, $supplier);
    }

    public function filterLocations(Request $request)
    {
        return app()->purchaseOrder->filterLocations($request);
    }

    public function getOrderStatus(Request $request, Customer $customer)
    {
        return app()->purchaseOrder->getOrderStatus($request, $customer);
    }

    public function show(PurchaseOrder $purchaseOrder)
    {
        dump($purchaseOrder->toArray());
    }

    public function export()
    {
        return new PurchaseOrderExport();
    }

    public function import(Request $request)
    {
        (new PurchaseOrderImport)->import(request()->file('file'));

        if (!empty(session('receivedError'))) {
            return redirect()->route('purchase_orders.index')->withErrors(__($request->session()->pull('receivedError')));
        }

        return redirect()->route('purchase_orders.index')->withStatus(__('Orders successfully imported.'));
    }

    public function totalPurchaseOrdersByStatuses(Request $request)
    {
        $date = $request->input('date_filter', '');
        $array = [
            "filterArray" => [
                ["columnName" => "dates_between", "value" => $date],
                ["columnName" => "table_search", "value" => null]
            ]
        ];

        $total = app()->purchaseOrder->search(json_encode($array))->count();

        $statuses = app()->purchaseOrder->search(json_encode($array))
            ->groupBy('status')
            ->select(
                'status',
                DB::raw('count(id) as count')
            )
            ->where('status', '!=', 'cancelled')
            ->get()
            ->toArray();

        foreach ($statuses as $key => $status) {
            if ($status['status'] == 'pending') {
                $statuses[$key]['icon'] = PurchaseOrder::STATUS_ICONS['pending'];
            } else if ($status['status'] == User::STATUS_CLOSED) {
                $statuses[$key]['icon'] = PurchaseOrder::STATUS_ICONS['closed'];
            }
        }

        array_unshift($statuses, array(
            'status' => 'All purchase orders',
            'icon' => PurchaseOrder::STATUS_ICONS['all'],
            'count' => $total,
            'skip_value' => true
        ));

        return response(view('shared.totalObjectsByStatuses', [
            'statuses' => $statuses,
            'plural' => 'Purchase orders',
            'singular' => 'Purchase order'
        ]));
    }

    public function cancel(PurchaseOrder $purchaseOrder)
    {
        app()->purchaseOrder->cancel($purchaseOrder);

        $message = __('Purchase Order successfully cancelled.');

        return response()->json(['message' => $message]);
    }

    public function autoPending(Request $request, Supplier $supplier)
    {
        $results = app()->purchaseOrder->autoPending($request, $supplier);

        return response()->json(['results' => $results]);
    }

    public function autoAll(Request $request, Supplier $supplier)
    {
        $results = app()->purchaseOrder->autoAll($request, $supplier);

        return response()->json(['results' => $results]);
    }

    public function autoBackordered(Request $request, Supplier $supplier)
    {
        $results = app()->purchaseOrder->autoBackordered($request, $supplier);

        return response()->json(['results' => $results]);
    }

    public function changeLog()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }

        return view('purchase_orders.changeLogIndex');
    }

    public function changeLogDataTable(Request $request, $purchaseOrderId = false)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'object_changes.created_at'
        );

        $term = $request->input('search.value');

        $filters = json_decode($term);

        if($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if($filter->columnName === 'ordering' && !empty($filter->value)) {
                    $filterValue = explode(",", $filter->value);
                    $tableColumnName = $filterValue[0];
                    $sortDirection = $filterValue[1];
                }
            }
        }

        $changeCollection = app()->purchaseOrder->searchChangeLog($term, $purchaseOrderId)
            ->leftJoin('purchase_orders', function($join)
             {
               $join->on('purchase_orders.id', '=', 'object_changes.object_id')
               ->where('object_changes.object_type', PurchaseOrder::class);
             })
            ->leftJoin('purchase_order_items', function($join)
             {
               $join->on('purchase_order_items.id', '=', 'object_changes.object_id')
               ->where('object_changes.object_type', PurchaseOrderItem::class);
             })
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            if ($purchaseOrderId) {
                $totalChangeCount = ObjectChange::where(function ($q) use ($purchaseOrderId) {
                    $q->where('object_type', PurchaseOrder::class)->where('object_id', $purchaseOrderId);
                })->orWhere(function ($q) use($purchaseOrderId) {
                    $q->where('parent_object_type', PurchaseOrder::class)->where('parent_object_id', $purchaseOrderId);
                })->count();
            } else {
                $totalChangeCount = ObjectChange::where(function ($q) {
                    $q->whereIn('object_type', [PurchaseOrder::class, PurchaseOrderItem::class])->orWhere('parent_object_type', PurchaseOrder::class);
                })->count();
            }

            $changeCount = $changeCollection->count();
        } else {
            $totalChangeCount = $changeCollection->count();
            $changeCount = $totalChangeCount;
        }

        $changes = $changeCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $changeCollection = ObjectChangeTableResource::collection($changes);

        return response()->json([
            'data' => $changeCollection,
            'recordsTotal' => $totalChangeCount,
            'recordsFiltered' => $changeCount
        ]);
    }
}
