<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\InvoiceStatus;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Http\Requests\Invoice\StoreRequest;
use App\Http\Requests\Invoice\UpdateRequest;
use App\Http\Requests\Invoice\DestroyRequest;
use App\Http\Resources\InvoiceTableResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Invoice::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('invoices.index',[
            'hideColumns' => get_hidden_columns(Invoice::SETTINGS_HIDDEN_COLUMNS_KEY),
            'total_invoices_by_statuses' => $this->total_invoices_by_statuses(),
        ]);
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $term = $request->get('search')['value'];

        $invoiceCollection =  app()->invoice->search($term)
        ->join('billing_profiles', 'billing_profile_id', '=', 'billing_profiles.id')
            ->join('contact_informations', 'customer_id', '=', 'contact_informations.object_id')
            ->join(DB::raw('
            (
                SELECT
                    invoices.id,
                    count(invoice_lines.id) as total_invoice_lines
                FROM invoices
                join invoice_lines ON invoices.id = invoice_lines.invoice_id
                group by invoices.id
             )
                 invoice_lines_info ON invoices.id = invoice_lines_info.id'
            ), function (){})
            ->where('contact_informations.object_type', Customer::class)
            ->select('*', 'invoices.*')
            ->orderBy($tableColumnName, $sortDirection);


        if ($term) {
            $totalInvoiceCount = Invoice::count();
            $invoiceCount = $invoiceCollection->count();
        } else {
            $totalInvoiceCount = $invoiceCollection->count();
            $invoiceCount = $totalInvoiceCount;
        }

        $inventoryLogs = $invoiceCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $invoiceCollection = InvoiceTableResource::collection($inventoryLogs);

        return response()->json([
            'data' => $invoiceCollection,
            'recordsTotal' => $totalInvoiceCount,
            'recordsFiltered' => $invoiceCount
        ]);
    }

    public function preview(Invoice $invoice)
    {
        return response(view('invoices.preview', ['invoice' => $invoice]));
    }

    public function editablePreview(Invoice $invoice)
    {
        return response(view('invoices.editablePreview', ['invoice' => $invoice]));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->invoice->store($request);

        return redirect()->route('invoices.index')->withStatus(__('Invoice successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Invoice $invoice
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Invoice $invoice)
    {
        return view('invoices.edit', ['invoice' => $invoice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Invoice $invoice)
    {
        app()->invoice->update($request, $invoice);

        $message = __('Invoice successfully updated.');

        if ($request->ajax()) {
            return response()->json(['message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, Invoice $invoice)
    {
        app()->invoice->destroy($request, $invoice);

        return redirect()->route('invoices.index')->withStatus(__('Invoice successfully deleted.'));
    }

    public function view($direct_url, Request $request)
    {
        $invoice = Invoice::where('direct_url', $direct_url)->with('invoiceLines')->first();

        return view('invoices.view', ['invoice' => $invoice]);
    }

    public function invoiceStatuses(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $invoiceStatuses = InvoiceStatus::where('name', 'like', '%' . $term . '%')->get();

            foreach ($invoiceStatuses as $status) {
                $results[] = [
                    'id' => $status->id,
                    'text' => $status->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function print_preview(Request $request, Invoice $invoice)
    {
        $filename = 'invoice-' . Carbon::now()->toDateTimeString();
        $html = view('invoices.printable_invoice', compact('invoice'));

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($filename,["Attachment" => false]);
    }

    public function total_invoices_by_statuses()
    {
        $statuses = [];
        $customers = Auth()->user()->customerIds();

        if($customers) {
            $statuses = InvoiceStatus::whereIn('customer_id', $customers)
                ->withCount('invoices')
                ->get()
                ->toArray();
        }

        return $statuses;
    }
}
