<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use App\Models\User;
use App\Http\Requests\UserRoleRequest;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(UserRole::class);
    }

    /**
     * Display a listing of the roles
     *
     * @param \App\Models\UserRole  $model
     * @return \Illuminate\View\View
     */
    public function index(UserRole $model)
    {
        $this->authorize('manage-users', User::class);

        return view('roles.index', ['roles' => $model->all()]);
    }

    /**
     * Show the form for creating a new role
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created role in storage
     *
     * @param  \App\Http\Requests\UserRoleRequest  $request
     * @param  \App\Models\UserRole  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRoleRequest $request, UserRole $model)
    {
        $model->create($request->all());

        return redirect()->route('roles.index')->withStatus(__('Role successfully created.'));
    }

    /**
     * Show the form for editing the specified role
     *
     * @param  \App\Models\UserRoleUserRole  $role
     * @return \Illuminate\View\View
     */
    public function edit(UserRole $role)
    {
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified role in storage
     *
     * @param  \App\Http\Requests\UserRoleRequest  $request
     * @param  \App\Models\UserRole  $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRoleRequest $request, UserRole $role)
    {
        $role->update($request->all());

        return redirect()->route('roles.index')->withStatus(__('Role successfully updated.'));
    }
}
