<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InvoiceLine;
use App\Http\Requests\InvoiceLine\StoreRequest;
use App\Http\Requests\InvoiceLine\UpdateRequest;
use App\Http\Requests\InvoiceLine\DestroyRequest;
use App\Http\Resources\InvoiceLineTableResource;

class InvoiceLineController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(InvoiceLine::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('invoice_lines.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $invoiceLineCollection = InvoiceLine::select('*', 'invoice_lines.*')
            ->orderBy($tableColumnName, $sortDirection);

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $invoiceLineCollection
                ->where('invoice_lines.title', 'like', $term)
                ->get();

            $totalInvoiceLineCount = InvoiceLine::count();
            $invoiceLineCount = $invoiceLineCollection->count();
        } else {
            $totalInvoiceLineCount = $invoiceLineCollection->count();
            $invoiceLineCount = $totalInvoiceLineCount;
        }

        $inventoryLogs = $invoiceLineCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $invoiceLineCollection = InvoiceLineTableResource::collection($inventoryLogs);

        return response()->json([
            'data' => $invoiceLineCollection,
            'recordsTotal' => $totalInvoiceLineCount,
            'recordsFiltered' => $invoiceLineCount
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('invoice_lines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->invoiceLine->store($request);

        return redirect()->route('invoice_line.index')->withStatus(__('Invoice Line successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InvoiceLine $invoiceLine
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(InvoiceLine $invoiceLine)
    {
        return view('invoice_lines.edit', ['invoiceLine' => $invoiceLine]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param InvoiceLine $invoiceLine
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, InvoiceLine $invoiceLine)
    {
        app()->invoiceLine->update($request, $invoiceLine);

        return redirect()->back()->withStatus(__('Invoice Line successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param InvoiceLine $invoiceLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, InvoiceLine $invoiceLine)
    {
        app()->invoiceLine->destroy($request, $invoiceLine);

        return redirect()->route('invoice_line.index')->withStatus(__('Invoice Line successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->invoiceLine->filterCustomers($request);
    }
}
