<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Resources\ProductTableResource;
use App\Http\Resources\ObjectChangeTableResource;
use App\Imports\ProductImport;
use App\Models\Product;
use App\Models\ObjectChange;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Product::class);
    }

    public function index()
    {
        return view('products.index', ['hideColumns' => get_hidden_columns(Product::SETTINGS_HIDDEN_COLUMNS_KEY)]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'products.name'
        );

        $term = $request->input('search.value');

        $filters = json_decode($term);

        if($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if($filter->columnName === 'ordering' && !empty($filter->value)) {
                    $filterValue = explode(",", $filter->value);
                    $tableColumnName = $filterValue[0];
                    $sortDirection = $filterValue[1];
                }
            }
        }

        $productCollection = app()->product->search($term)
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            $totalProductCount = Product::count();
            $productCount = $productCollection->count();
        } else {
            $totalProductCount = $productCollection->count();
            $productCount = $totalProductCount;
        }

        $products = $productCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $productCollection = ProductTableResource::collection($products);

        return response()->json([
            'data' => $productCollection,
            'currency' => currency($productCollection[0]->customer ?? null),
            'recordsTotal' => $totalProductCount,
            'recordsFiltered' => $productCount
        ]);
    }

    public function preview(Product $product)
    {
        return response(view('products.preview', ['product' => $product]));
    }

    public function editablePreview(Product $product)
    {
        return response(view('products.editablePreview', ['product' => $product]));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(StoreRequest $request)
    {
        app()->product->store($request);

        if ($request->ajax()) {
            $request->session()->flash('status', __('Product successfully created.'));
            return response()->json(['success' => true]);
        }

        return redirect()->route('products.index')->with(__('Product successfully created!'));
    }

    public function edit(Product $product)
    {
        return view('products.edit', ['product' => $product]);
    }

    public function update(UpdateRequest $request, Product $product)
    {
        app()->product->update($request, $product);

        $message = __('Product successfully updated.');

        if ($request->ajax()) {
            $request->session()->flash('status', $message);
            return response()->json(['success' => true, 'message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(Product $product)
    {
        app()->product->destroy(null, $product);

        return redirect()->route('products.index')->withStatus(__('Product successfully deleted.'));
    }

    public function filterCustomers(Request $request, Product $product)
    {
        return app()->product->filterCustomers($request);
    }

    public function deleteProductImage(Request $request)
    {
        return app()->product->deleteProductImage($request);
    }

    public function export()
    {
        return new ProductExport();
    }

    public function import()
    {
        (new ProductImport)->import(request()->file('file'));

        return redirect()->route('product.index')->withStatus(__('Product successfully imported.'));
    }

    public function changeLog()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }

        return view('products.changeLogIndex');
    }

    public function changeLogDataTable(Request $request, $productId = false)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'object_changes.created_at'
        );

        $term = $request->input('search.value');

        $filters = json_decode($term);

        if($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if($filter->columnName === 'ordering' && !empty($filter->value)) {
                    $filterValue = explode(",", $filter->value);
                    $tableColumnName = $filterValue[0];
                    $sortDirection = $filterValue[1];
                }
            }
        }

        $changeCollection = app()->product->searchChangeLog($term, $productId)
            ->join('products', 'object_changes.object_id', '=', 'products.id')
            ->orderBy($tableColumnName, $sortDirection);

        if ($term) {
            if ($productId) {
                $totalChangeCount = ObjectChange::where('object_type', Product::class)->where('object_id', $productId)->count();
            } else {
                $totalChangeCount = ObjectChange::where('object_type', Product::class)->count();
            }

            $changeCount = $changeCollection->count();
        } else {
            $totalChangeCount = $changeCollection->count();
            $changeCount = $totalChangeCount;
        }

        $changes = $changeCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $changeCollection = ObjectChangeTableResource::collection($changes);

        return response()->json([
            'data' => $changeCollection,
            'recordsTotal' => $totalChangeCount,
            'recordsFiltered' => $changeCount
        ]);
    }
}