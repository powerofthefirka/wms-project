<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserWidget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserWidgetController extends Controller
{
    public function getWidgets(Request $request) {
        $widgets = UserWidget::where('user_id', Auth::user()->id)
        ->where('location', $request->location)->first();

        return response()->json($widgets->grid_stack_data ?? '');
    }

    public function createUpdate(Request $request) {
        $userWidget = UserWidget::updateOrCreate(
            ['location' => $request->location, 'user_id' => Auth::user()->id],
            ['grid_stack_data' => $request->grid_stack_data]
        );

        return $userWidget;
    }
}
