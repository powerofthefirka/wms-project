<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationType\DestroyRequest;
use App\Http\Requests\LocationType\StoreRequest;
use App\Http\Requests\LocationType\UpdateRequest;
use App\Http\Resources\LocationTypeTableResource;
use App\Models\LocationType;
use App\Models\ThreePl;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LocationTypeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(LocationType::class);
    }

    public function index()
    {
        $location_types = LocationType::whereIn('3pl_id', Auth()->user()->threePlIds())->get();

        return view('location_type.index', [
            'location_types' => $location_types,
            'hideColumns' => get_hidden_columns(LocationType::HIDDEN_COLUMNS)
        ]);
    }

    public function dataTable(Request $request, $threePl = false)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'name'
        );

        $term = $request->get('search')['value'];

        $orderCollection = app()->locationType->search($term, $threePl)
            ->join('3pls', 'location_types.3pl_id', '=', '3pls.id')
            ->join('contact_informations AS threepl_contact_information', function ($join) {
                $join->on('3pls.id', '=', 'threepl_contact_information.object_id')
                    ->where('threepl_contact_information.object_type', ThreePl::class);
            })
            ->select('*', 'location_types.*')
            ->orderBy((empty($tableColumnName) ? 'location_types.name' : $tableColumnName), $sortDirection);

        if ($term) {
            $totalOrderCount = LocationType::count();
            $orderCount = $orderCollection->count();
        } else {
            $totalOrderCount = $orderCollection->count();
            $orderCount = $totalOrderCount;
        }

        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();

        $orderCollection = LocationTypeTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function preview(LocationType $locationType)
    {
        return response(view('location_type.preview', ['locationType' => $locationType]));
    }

    public function editablePreview(LocationType $locationType)
    {
        return response(view('location_type.editablePreview', ['locationType' => $locationType]));
    }

    public function create()
    {
        return view('location_type.create');
    }

    public function store(StoreRequest $request)
    {
        app()->locationType->store($request);

        return redirect()->route('location_types.index')->withStatus(__('Location Type successfully created.'));
    }

    public function edit(LocationType $locationType)
    {
        return view('location_type.edit', ['locationType' => $locationType]);
    }

    public function update(UpdateRequest $request, LocationType $locationType)
    {
        app()->locationType->update($request, $locationType);

        $message = __('Location Type successfully updated.');

        if ($request->ajax()) {
            $request->session()->flash('status', __('Location Type successfully updated.'));
            return response()->json(['success' => true, 'message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(DestroyRequest $request, LocationType $locationType)
    {
        app()->locationType->destroy($request, $locationType);

        return redirect()->back()->withStatus(__('Location Type successfully deleted.'));
    }

    public function filter3Pls(Request $request)
    {
        return app()->locationType->filter3Pls($request);
    }
}
