<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PricingPlan\StoreRequest;
use App\Http\Requests\PricingPlan\UpdateRequest;
use App\Http\Requests\PricingPlan\DestroyRequest;
use App\Http\Resources\PricingPlanTableResource;
use App\Models\PricingPlan;
use Illuminate\Support\Arr;

class PricingPlanController extends Controller
{
    public function index()
    {
        if (!auth()->user()->isAdmin()) {
            return abort(403);
        }

        return view('pricing_plans.index', [
            'hideColumns' => get_hidden_columns(PricingPlan::SETTINGS_HIDDEN_COLUMNS_KEY)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'pricing_plans.name'
        );

        $pricingPlanCollection = PricingPlan::select('pricing_plans.*')
            ->orderBy($tableColumnName, $sortDirection);

        $totalPricingPlanCount = $pricingPlanCollection->count();
        $pricingPlanCount = $totalPricingPlanCount;

        $pricingPlans = $pricingPlanCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $pricingPlanCollection = PricingPlanTableResource::collection($pricingPlans);

        return response()->json([
            'data' => $pricingPlanCollection,
            'recordsTotal' => $totalPricingPlanCount,
            'recordsFiltered' => $pricingPlanCount
        ]);
    }

    public function create(Request $request)
    {
        return view('pricing_plans.create', [
            'return_to_3pl' => $request->get('return_to_3pl', false)
        ]);
    }

    public function store(StoreRequest $request)
    {
        app()->pricingPlan->store($request);

        if ($request->get('return_to_3pl', false)) {
            return redirect()->route('three_pls.edit_pricing_plan', [ 'threePl' => $request->get('return_to_3pl') ])->withStatus(__('Pricing plan successfully created.'));
        }

        return redirect()->route('pricing_plans.index')->withStatus(__('Pricing plan successfully created.'));
    }

    public function edit(PricingPlan $pricingPlan)
    {
        return view('pricing_plans.edit', ['pricingPlan' => $pricingPlan]);
    }

    public function update(UpdateRequest $request, PricingPlan $pricingPlan)
    {
        app()->pricingPlan->update($request, $pricingPlan);

        return redirect()->back()->withStatus(__('Pricing plan successfully updated.'));
    }

    public function destroy(DestroyRequest $request, PricingPlan $pricingPlan)
    {
        app()->pricingPlan->destroy($request, $pricingPlan);

        return redirect()->back()->withStatus(__('Pricing plan successfully deleted.'));
    }
}
