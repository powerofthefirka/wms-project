<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingBox\DestroyRequest;
use App\Http\Requests\ShippingBox\StoreRequest;
use App\Http\Requests\ShippingBox\UpdateRequest;
use App\Http\Resources\ShippingBoxTableResource;
use App\Models\Customer;
use App\Models\ShippingBox;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ShippingBoxController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ShippingBox::class);
    }

    public function index()
    {
        $shipping_boxes = ShippingBox::whereIn('customer_id', Auth()->user()->customerIds())->get();

        return view('shipping_box.index', [
            'shipping_boxes' => $shipping_boxes,
            'hideColumns' => get_hidden_columns(ShippingBox::HIDDEN_COLUMNS)
        ]);
    }

    public function dataTable(Request $request)
    {
        $sortDirection = Arr::get($request->input('order'), '0.dir', 'desc');
        $tableColumnName = Arr::get(
            $request->input('columns'),
            $request->input('order')[0]['column'] . '.' . 'name',
            'name'
        );

        $term = $request->get('search')['value'];

        $orderCollection = app()->shippingBox->search($term)
            ->join('customers', 'shipping_boxes.customer_id', '=', 'customers.id')
            ->join('contact_informations AS customer_contact_information', function ($join) {
                $join->on('customers.id', '=', 'customer_contact_information.object_id')
                    ->where('customer_contact_information.object_type', Customer::class);
            })
            ->select('*', 'shipping_boxes.*')
            ->orderBy(empty($tableColumnName) ? 'shipping_boxes.name' : $tableColumnName, $sortDirection);

        if ($term) {
            $totalOrderCount = ShippingBox::count();
            $orderCount = $orderCollection->count();
        } else {
            $totalOrderCount = $orderCollection->count();
            $orderCount = $totalOrderCount;
        }

        $orders = $orderCollection->skip($request->get('start'))->limit($request->get('length'))->get();



        $orderCollection = ShippingBoxTableResource::collection($orders);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function preview(ShippingBox $shippingBox)
    {
        return response(view('shipping_box.preview', ['shippingBox' => $shippingBox]));
    }

    public function editablePreview(ShippingBox $shippingBox)
    {
        return response(view('shipping_box.editablePreview', ['shippingBox' => $shippingBox]));
    }

    public function create()
    {
        return view('shipping_box.create');
    }

    public function store(StoreRequest $request)
    {
        app()->shippingBox->store($request);

        return redirect()->route('shipping_boxes.index')->withStatus(__('Shipping box successfully created.'));
    }

    public function edit(ShippingBox $shippingBox)
    {
        return view('shipping_box.edit', ['shippingBox' => $shippingBox]);
    }

    public function update(UpdateRequest $request, ShippingBox $shippingBox)
    {
        app()->shippingBox->update($request, $shippingBox);

        $message = __('Shipping box successfully updated.');

        if ($request->ajax()) {
            $request->session()->flash('status', __('Shipping box successfully updated.'));
            return response()->json(['success' => true, 'message' => $message]);
        }

        return redirect()->back()->withStatus($message);
    }

    public function destroy(DestroyRequest $request, ShippingBox $shippingBox)
    {
        app()->shippingBox->destroy($request, $shippingBox);

        return redirect()->back()->withStatus(__('Shipping box successfully deleted.'));
    }
}
