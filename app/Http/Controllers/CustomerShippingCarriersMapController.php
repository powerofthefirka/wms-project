<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerShippingCarriersMap\DestroyRequest;
use App\Http\Requests\CustomerShippingCarriersMap\StoreRequest;
use App\Http\Requests\CustomerShippingCarriersMap\UpdateRequest;
use App\Http\Resources\CustomerShippingCarriersMapTableResource;
use Illuminate\Http\Request;
use App\Models\CustomerShippingCarriersMap;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

class CustomerShippingCarriersMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shipping_carrier_mapping.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];
        $customerIds = Auth()->user()->customerIds();

        $mappingCollection = CustomerShippingCarriersMap::join('customers', 'customer_shipping_carriers_maps.customer_id', '=', 'customers.id')
            ->join('contact_informations AS customer_contact_information', 'customers.id', '=', 'customer_contact_information.object_id')
            ->where('customer_contact_information.object_type', Customer::class)
            ->select('*', 'customer_shipping_carriers_maps.*')
            ->orderBy($tableColumnName, $sortDirection);

        if (!empty($customerIds)) {
            $mappingCollection = $mappingCollection->whereIn('customer_shipping_carriers_maps.customer_id', $customerIds);
        }

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $mappingCollection
                ->orWhereHas('customer.contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', $term);
                })
                ->orWhere('customer_shipping_carriers_maps.shipping_service', 'like', $term)
                ->orWhere('customer_shipping_carriers_maps.shipping_carrier_pattern', 'like', $term)
                ->get();

            $totalMappingCount = CustomerShippingCarriersMap::count();
            $mappingCount = $mappingCollection->count();
        } else {
            $totalMappingCount = $mappingCollection->count();
            $mappingCount = $totalMappingCount;
        }

        $mappings = $mappingCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $mappingCollection = CustomerShippingCarriersMapTableResource::collection($mappings);

        return response()->json([
            'data' => $mappingCollection,
            'recordsTotal' => $totalMappingCount,
            'recordsFiltered' => $mappingCount
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shipping_carrier_mapping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        app()->customerShippingCarriersMap->store($request);

        return redirect()->route('shipping_carrier_mapping.index')->withStatus(__('Shipping carrier map successfully created.'));
    }

    public function edit(CustomerShippingCarriersMap $shippingCarrierMapping)
    {
        return view('shipping_carrier_mapping.edit', ['mapping' => $shippingCarrierMapping]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param CustomerShippingCarriersMap $shippingCarrierMapping
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CustomerShippingCarriersMap $shippingCarrierMapping)
    {
        app()->customerShippingCarriersMap->update($request, $shippingCarrierMapping);

        return redirect()->back()->withStatus(__('Shipping carrier map successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param CustomerShippingCarriersMap $shippingCarrierMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, CustomerShippingCarriersMap $shippingCarrierMapping)
    {
        app()->customerShippingCarriersMap->destroy($request, $shippingCarrierMapping);

        return redirect()->route('shipping_carrier_mapping.index')->withStatus(__('Shipping carrier map successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->customerShippingCarriersMap->filterCustomers($request);
    }

    public function carriers(Request $request, Customer $customer)
    {
        return app()->shipment->webshipperCarriers($customer);
    }

    public function mappedCarriers(Request $request, Customer $customer, $service)
    {
        return app()->customerShippingCarriersMap->mappedCarriers($customer, $service);
    }
}
