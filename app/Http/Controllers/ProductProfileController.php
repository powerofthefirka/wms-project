<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductProfile\EditRequest;
use App\Http\Requests\ProductProfile\StoreRequest;
use App\Http\Resources\ProductProfileTableResource;
use App\Http\Resources\ProductTableResource;
use App\Models\Customer;
use App\Models\Product;
use App\Models\ProductProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductProfileController extends Controller
{
    public function index()
    {
        $customerIds = Auth()->user()->customerIds();
        $customers = Customer::whereIn('id', $customerIds)->get();

        return view('product_profiles.index', ['customers' => $customers]);
    }

    public function productList(ProductProfile $productProfile)
    {

        return response(view('product_profiles.productList', ['productProfile' => $productProfile]));
    }

    public function dataTable(Request $request)
    {
        $customerIds = Auth()->user()->customerIds();
        $productProfileCollection = ProductProfile::query();

        if (!empty($customerIds)) {
            $productProfileCollection = $productProfileCollection->whereIn('customer_id', $customerIds);
        }

        $term = $request->input('search.value');

        if ($term) {
            $totalOrderCount = ProductProfile::count();
            $orderCount = $productProfileCollection->count();
        } else {
            $totalOrderCount = $productProfileCollection->count();
            $orderCount = $totalOrderCount;
        }

        $productProfileCollection = $productProfileCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $orderCollection = ProductProfileTableResource::collection($productProfileCollection);

        return response()->json([
            'data' => $orderCollection,
            'recordsTotal' => $totalOrderCount,
            'recordsFiltered' => $orderCount
        ]);
    }

    public function productDataTable(Request $request)
    {
        $term = $request->input('search.value');

        $productCollection = app()->product->search(null);

        if ($term) {
            $totalProductCount = Product::count();
            $productCount = $productCollection->count();
        } else {
            $totalProductCount = $productCollection->count();
            $productCount = $totalProductCount;
        }

        $products = $productCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $productCollection = ProductTableResource::collection($products);

        return response()->json([
            'data' => $productCollection,
            'recordsTotal' => $totalProductCount,
            'recordsFiltered' => $productCount
        ]);
    }

    public function saveProductSelection(Request $request, ProductProfile $productProfile)
    {
        $addProducts = $request->input('addProducts', []);
        $removeProducts = $request->input('removeProducts', []);

        foreach ($removeProducts as $id) {
            $product = Product::find($id);

            $product->product_profile_id = null;
            $product->save();
        }

        foreach ($addProducts as $id) {
            $product = Product::find($id);

            $product->product_profile_id = $productProfile->id;
            $product->save();
        }

        return __('Product selection saved!');
    }


    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        if (Auth()->user()->isAdmin()) {
            $input['customer_id'] = $input['customer_id'] ?? session('customer_id');
        } else {
            $input['customer_id'] = Auth()->user()->customers()->wherePivot('user_id', '=', Auth()->user()->id)->pluck('customers.id')[0];
        }

        ProductProfile::create($input);

        return __('Product profile created');
    }

    public function edit(ProductProfile $productProfile)
    {
       return view('product_profiles.edit', ['productProfile' => $productProfile]);
    }

    public function update(EditRequest $request, ProductProfile $productProfile)
    {
        $input = $request->validated();

        $productProfile->update($input);

        return redirect()->route('product_profiles.index')->withStatus(__('Product profile successfully updated.'));;
    }

    public function destroy(ProductProfile $productProfile)
    {
        $productProfile->delete();

        return __('Product profile deleted');
    }
}
