<?php

namespace App\Http\Controllers;

use App\Models\BillingFee;
use App\Models\ThreePl;
use Illuminate\Http\Request;
use App\Models\BillingProfile;
use App\Http\Requests\BillingProfile\StoreRequest;
use App\Http\Requests\BillingProfile\UpdateRequest;
use App\Http\Requests\BillingProfile\DestroyRequest;
use App\Http\Resources\BillingProfileTableResource;

class BillingProfileController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(BillingProfile::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('billing_profiles.index');
    }

    public function dataTable(Request $request)
    {
        $tableColumns = $request->get('columns');
        $columnOrder = $request->get('order');

        $tableColumnName = $tableColumns[$columnOrder[0]['column']]['name'];
        $sortDirection = $columnOrder[0]['dir'];

        $bProfileCollection = BillingProfile::whereIn('3pl_id', auth()->user()->threePls->pluck('id'))->select('*', 'billing_profiles.*')
            ->orderBy($tableColumnName, $sortDirection);

        $term = $request->get('search')['value'];

        if ($term) {
            // TODO: sanitize term
            $term = '%' . $term . '%';

            $bProfileCollection
                ->where('billing_profiles.name', 'like', $term)
                ->get();

            $totalBProfileCount = BillingProfile::count();
            $bProfileCount = $bProfileCollection->count();
        } else {
            $totalBProfileCount = $bProfileCollection->count();
            $bProfileCount = $totalBProfileCount;
        }

        $inventoryLogs = $bProfileCollection->skip($request->get('start'))->limit($request->get('length'))->get();
        $bProfileCollection = BillingProfileTableResource::collection($inventoryLogs);

        return response()->json([
            'data' => $bProfileCollection,
            'recordsTotal' => $totalBProfileCount,
            'recordsFiltered' => $bProfileCount
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $threePls = [];

        if (auth()->user()->isAdmin()) {
            $threePls = ThreePl::with('contactInformation')->get()->pluck('contactInformation.name', 'id');
        }

        return view('billing_profiles.create', ['threePls' => $threePls]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $item = app()->billingProfile->store($request);

        return redirect()->route('billing_profiles.edit', ['billingProfile' => $item->id])->withStatus(__('Billing Profile successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BillingProfile $billingProfile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(BillingProfile $billingProfile)
    {
        if (!auth()->user()->isAdmin() && !in_array($billingProfile->threePL->id, auth()->user()->threePls->pluck('id')->toArray()))
            return redirect()->to('/billings/billing_profiles')->with('error', 'Unauthorized.');

        $threePls = [];

        if (auth()->user()->isAdmin()) {
            $threePls = ThreePl::with('contactInformation')->get()->pluck('contactInformation.name', 'id');
        }


        return view('billing_profiles.edit', ['billingProfile' => $billingProfile, 'threePls' => $threePls]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param BillingProfile $billingProfile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, BillingProfile $billingProfile)
    {
        app()->billingProfile->update($request, $billingProfile);

        return redirect()->back()->withStatus(__('Billing Profile successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param BillingProfile $billingProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, BillingProfile $billingProfile)
    {
        app()->billingProfile->destroy($request, $billingProfile);

        return redirect()->route('billing_profiles.index')->withStatus(__('Billing Profile successfully deleted.'));
    }

    public function filterCustomers(Request $request)
    {
        return app()->billingProfile->filterCustomers($request);
    }

    public function clone($id)
    {
        $billingProfile = BillingProfile::findOrFail($id);
        $clone = $billingProfile->replicate();

        $clone->name = $clone->name . " (Clone)";
        $clone->save();

        foreach ($billingProfile->billingFees as $fee) {
            $feeClone = $fee->replicate();

            $feeClone->billing_profile_id = $clone->id;
            $feeClone->save();
        }

        return redirect()->route('billing_profiles.edit', $clone->id);
    }
}
