<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class LocationTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['location_name'] = $this->name;
        $resource['location_pickable'] = $this->pickable;
        $resource['warehouse_name'] = [
            'name' =>
            Arr::get($this->warehouse->contactInformation, 'name', ''),
            'url' => route('warehouses.edit', ['warehouses' => $this->warehouse])
        ];
        $resource['warehouse_address'] = $this->warehouse->contactInformation->address;
        $resource['warehouse_zip'] = $this->warehouse->contactInformation->zip;
        $resource['warehouse_city'] = $this->warehouse->contactInformation->zip;
        $resource['warehouse_email'] = $this->warehouse->contactInformation->email;
        $resource['warehouse_phone'] = $this->warehouse->contactInformation->phone;
        $resource['link_edit'] = route('locations.edit', ['location' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('locations.destroy', ['id' => $this->id, 'location' => $this])];

        return $resource;
    }
}
