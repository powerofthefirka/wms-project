<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\CustomerShippingMethod;
use Illuminate\Support\Facades\Auth;

class CarriersAndShippingMethodsTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);        

        $resource['id'] = $this->id;
        $resource['name'] = $this->name;
        $resource['subSelectables'] = $this->shippingMethods()->select(['id', 'name'])->get();

        return $resource;
    }
}
