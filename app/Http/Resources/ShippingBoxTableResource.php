<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ShippingBoxTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] =  $this->id;
        $resource['name'] =  $this->name;
        $resource['wms_id'] = $this->wms_id;
        $resource['customer'] = ['name' => $this->customer->contactInformation->name, 'url' => route('customers.edit', ['customer' => $this->customer])];
        $resource['link_edit'] = route('shipping_boxes.edit', ['shipping_box' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('shipping_boxes.destroy', ['id' => $this->id, 'shipping_box' => $this])];

        return $resource;
    }
}
