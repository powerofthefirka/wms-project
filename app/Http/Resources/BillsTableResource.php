<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillsTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['customer'] = ['visible' => session('customer_id') == $this->customer->id ? false : true, 'url' =>route('customers.edit', ['customer' => $this->customer]), 'name' => $this->customer->contactInformation->name, 'id' => $this->customer->id];
        $resource['billing_profile'] = ['url' => route('billing_profiles.edit', ['billing_profile' => $this->billingProfile]), 'name' => $this->billingProfile->name];
        $resource['period_start'] = localized_date($this->period_start);
        $resource['period_end'] =  localized_date($this->period_end);
        $resource['amount'] = $this->amount;

        return $resource;
    }
}
