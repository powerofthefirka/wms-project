<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ReturnTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['returned_order'] = ['number' => $this->number, 'url' => route('returns.edit', ['return' => $this->id])];
        $resource['order'] = ['number' => $this->order->number, 'url' => route('orders.edit', ['order' => $this->order])];
        $resource['order_customer'] =['name' => $this->order->customer->contactInformation->name, 'url' => route('customers.edit', ['return' => $this->order->customer])];
        $resource['city'] = $this->order->customer->contactInformation->city;
        $resource['zip'] = $this->order->customer->contactInformation->zip;
        $resource['status'] = Arr::get($this, 'status', '');
        $resource['requested_at'] = localized_date($this->requested_at);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('returns.destroy', ['id' => $this->id, 'return' => $this])];

        return $resource;
    }
}
