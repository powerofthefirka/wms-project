<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\PickingBatch;

class ShippingMethodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['shipping_carrier'] = new ShippingCarrierResource($this->shippingCarrier);
        unset($resource['shipping_carrier_id']);

        return $resource;
    }
}