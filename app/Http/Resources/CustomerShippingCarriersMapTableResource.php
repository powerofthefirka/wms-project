<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerShippingCarriersMapTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['customer'] = ['url' => route('customers.edit', ['customer' => $this->customer]), 'name' => $this->customer->contactInformation->name];
        $resource['shipping_service'] = $this->shipping_service;
        $resource['carrier_name'] = $this->shippingCarrierText();
        $resource['link_edit'] =route('shipping_carrier_mapping.edit', ['shipping_carrier_mapping' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('shipping_carrier_mapping.destroy', ['id' => $this->id, 'shipping_carrier_mapping' => $this])];

        return $resource;
    }
}
