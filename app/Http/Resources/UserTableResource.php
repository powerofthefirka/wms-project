<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserTableResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        if ($this->isAdmin()) {
            $resource['customer_role'] = "N/A";
        } else if ($this->isCustomerAdmin()) {
            $resource['customer_role'] = "Administrator";
        } else {
            $resource['customer_role'] = "Member";
        }

        $resource['name'] = $this->contactInformation->name;
        $resource['email'] = $this->email;
        $resource['role'] = $this->role->name;
        $resource['link_edit'] =route('users.edit', ['user' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('users.destroy', ['id' => $this->id, 'user' => $this])];

        return $resource;
    }
}
