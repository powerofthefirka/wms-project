<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillingProfileTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['name'] = $this->name;
        $resource['monthly_cost'] = $this->monthly_cost;
        $resource['per_user_cost'] = $this->per_user_cost;
        $resource['per_purchase_order_received_cost'] = $this->per_purchase_order_received_cost;
        $resource['per_product_cost'] = $this->per_product_cost;
        $resource['per_shipment_cost'] = $this->per_shipment_cost;
        $resource['per_return_cost'] = $this->per_return_cost;
        $resource['link_edit'] =route('billing_profiles.edit', ['billing_profile' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('billing_profiles.destroy', ['id' => $this->id, 'billing_profile' => $this])];
        $resource['link_clone'] = ['token' => csrf_token(), 'url' => route('billing_profiles.clone', ['id' => $this->id, 'billing_profile' => $this])];


        return $resource;
    }
}
