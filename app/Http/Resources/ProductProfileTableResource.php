<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductProfileTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->name;
        $resource['edit'] = route('product_profiles.edit', ['product_profile' => $this]);
        $resource['delete'] = route('product_profiles.destroy', ['product_profile' => $this]);

        return $resource;
    }
}
