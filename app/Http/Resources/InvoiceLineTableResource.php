<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceLineTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['title'] = $this->title;
        $resource['invoice'] = ['url' => route('invoices.edit', ['user' => $this->invoice]), 'name' => $this->invoice->direct_url];
        $resource['cost'] = $this->cost;
        $resource['link_edit'] = route('invoice_line.edit', ['invoice_line' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('invoice_line.destroy', ['id' => $this->id, 'invoice_line' => $this])];

        return $resource;
    }
}
