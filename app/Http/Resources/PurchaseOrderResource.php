<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseOrderResource extends JsonResource
{
    protected $customId;

    public function setCustomId($value){
        $this->customId = $value;
        return $this;
    }
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['customer'] = new CustomerResource($this->customer);
        unset($resource['customer_id']);

        $resource['warehouse'] = new WarehouseResource($this->warehouse);
        unset($resource['warehouse_id']);

        $resource['supplier'] = new SupplierResource($this->supplier);
        unset($resource['supplier_id']);

        $resource['purchase_order_items'] = new PurchaseOrderItemCollection($this->purchaseOrderItems);

        $resource['shipping_carrier'] = new ShippingCarrierResource($this->shippingCarrier);
        unset($resource['shipping_carrier_id']);

        $resource['shipping_method'] = new ShippingMethodResource($this->shippingMethod);
        unset($resource['shipping_method_id']);

        $resource['custom_id'] = $this->customId;

        return $resource;
    }
}
