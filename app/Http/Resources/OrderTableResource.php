<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class OrderTableResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);
        $resource['id'] = $this->id;
        $resource['number'] = $this->number;
        $resource['status'] = $this->status;
        $resource['ordered_at'] = localized_date($this->ordered_at);
        $resource['customer'] = ['visible' => session('customer_id') == $this->customer->id ? false : true, 'url' => route('customers.edit', ['customer' => $this->customer]), 'name' => $this->customer->contactInformation->name ?? ''];
        $resource['shipping_zip'] = $this->shippingContactInformation->zip;
        $resource['shipping_city'] = $this->shippingContactInformation->city;
        $resource['shipping_country'] = Arr::get($this->shippingContactInformation->country, 'code', '');
        $resource['item_count'] = $this->item_count;
        $resource['shipping_method'] = Arr::get($this->shippingMethod, 'name', '');
        $resource['total'] = $this->total;
        $resource['total_with_currency'] =  with_currency($this->customer, $this->total);
        $resource['fraud_hold'] = $this->fraud_hold;
        $resource['address_hold'] = $this->address_hold;
        $resource['payment_hold'] = $this->payment_hold;
        $resource['operator_hold'] = $this->operator_hold;
        $resource['shop_name'] = $this->shop_name;
        $resource['link_edit'] = route('orders.edit', ['id' => $this->id]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('orders.destroy', ['id' => $this->id, 'order' => $this])];
        $resource['totes'] = array_column($this->orderItems->pluck('totes')->flatten()->toArray(), 'name');
        return $resource;
    }
}
