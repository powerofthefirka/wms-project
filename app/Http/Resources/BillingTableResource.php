<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillingTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->contactInformation->name;
        $resource['billing_profile'] = ['url' => route('billing_profiles.edit', ['billingProfile' => $this->billingProfile ?? null]), 'name' => $this->billingProfile->name ?? ''];
        $resource['company_name'] = $this->contactInformation->company_name;
        $resource['invoices'] = ['count' => $this->invoices_count, 'latest' => $this->latest_invoice];
        $resource['link_show'] =  route('billings.show', ['customer' => $this]);

        return $resource;
    }
}
