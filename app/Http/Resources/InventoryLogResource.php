<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Location;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\Shipment;

class InventoryLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['user'] = new UserResource($this->user);
        unset($resource['user_id']);

        $resource['product'] = new ProductResource($this->product);
        unset($resource['product_id']);

        switch ($resource['source_type']) {
            case Location::class:
                $resource['source'] = new LocationResource($this->source);
                break;
            case PurchaseOrder::class:
                $resource['source'] = new PurchaseOrderResource($this->source);
                break;
            case Return_::class:
                $resource['source'] = new ReturnResource($this->source);
                break;
        }        
        unset($resource['source_id']);

        switch ($resource['destination_type']) {
            case Location::class:
                $resource['destination'] = new LocationResource($this->destination);
                break;
            case Shipment::class:
                $resource['destination'] = new ShipmentResource($this->destination);
                break;
        }
        unset($resource['destination_id']);

        return $resource;
    }
}
