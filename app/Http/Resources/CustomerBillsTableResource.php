<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerBillsTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['period_start'] = localized_date($this->period_start);
        $resource['period_end'] =  localized_date($this->period_end);
        $resource['amount'] = $this->amount;
        $resource['calculated_at'] = $this->calculated_at ?? __('calculating');

        return $resource;
    }
}
