<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected $customId;

    public function setCustomId($value)
    {
        $this->customId = $value;
        return $this;
    }

    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['warehouse'] = new WarehouseResource($this->warehouse);
        unset($resource['warehouse_id']);

        $resource['locationType'] = new LocationResource($this->locationType);
        unset($resource['location_type_id']);

        $resource['custom_id'] = $this->customId;

        return $resource;
    }
}
