<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ThreePlTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['three_pl_name'] = $this->contactInformation->name;
        $resource['three_pl_address'] =  $this->contactInformation->address ?? '';
        $resource['three_pl_zip'] = $this->contactInformation->zip ?? '';
        $resource['three_pl_city'] = $this->contactInformation->city ?? '';
        $resource['three_pl_email'] = $this->contactInformation->email ?? '';
        $resource['three_pl_phone'] = $this->contactInformation->phone ?? '';
        $resource['link_edit'] =  route('three_pls.edit', ['threePl' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('three_pls.destroy', ['id' => $this->id, 'threePl' => $this])];
        $resource['active'] =  $this->active;
        $resource['link_disable'] =  route('three_pls.disable', ['threePl' => $this]);
        if (!$this->active) {
            $resource['link_disable'] =  route('three_pls.enable', ['threePl' => $this]);
        }

        return $resource;
    }
}
