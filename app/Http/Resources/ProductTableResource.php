<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ProductTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] =  $this->id;
        $resource['sku'] =  $this->sku;
        $resource['name'] =  $this->name;
        $resource['price'] = $this->price;
        $resource['price_with_currency'] =  with_currency($this->customer, $this->price);
        $resource['source'] = Arr::get($this->productImages, '0.source', 'images/product-placeholder.png');
        $resource['customs_price'] = $this->customs_price;
        $resource['weight'] = $this->weight;
        $resource['weight_with_unit'] = with_weight_unit($this->customer, $this->weight);
        $resource['width'] = $this->width;
        $resource['width_with_dimension'] = with_dimension_unit($this->customer, $this->width);
        $resource['height'] = $this->height;
        $resource['height_with_dimension'] = with_dimension_unit($this->customer, $this->height);
        $resource['length'] = $this->length;
        $resource['length_with_dimension'] = with_dimension_unit($this->customer, $this->length);
        $resource['country_of_origin'] = $this->country_of_origin;
        $resource['hs_code'] = $this->hs_code;
        $resource['notes'] = $this->notes;
        $resource['quantity_on_hand'] = $this->quantity_on_hand;
        $resource['quantity_available'] = $this->quantity_available;
        $resource['quantity_allocated'] = $this->quantity_allocated;
        $resource['quantity_backordered'] = $this->quantity_backordered;
        $resource['quantity_sell_ahead'] = $this->quantity_sell_ahead;
        $resource['quantity_on_po'] = $this->quantity_on_po;
        $resource['barcode'] = $this->barcode;

        $warehouseList = [];

        foreach ($this->location->groupBy('warehouse_id') as $location) {
            $warehouseList[] = $location->first()->warehouse->contactInformation->name;
        }

        $resource['warehouse'] = implode(',', $warehouseList);

        $resource['is_kit'] = $this->is_kit;
        $resource['replacement_value'] = $this->replacement_value;
        $resource['replacement_with_currency'] =  with_currency($this->customer, $this->replacement_value);
        $resource['customer'] = ['name' => $this->customer->contactInformation->name ?? '', 'url' => route('customers.edit', ['customer' => $this->customer])];
        $resource['product_profile_id'] = $this->product_profile_id;
        $resource['product_profile'] = $this->productProfile->name ?? '';
        $resource['link_edit'] = route('products.edit', ['product' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('products.destroy', ['id' => $this->id, 'product' => $this])];

        return $resource;
    }
}
