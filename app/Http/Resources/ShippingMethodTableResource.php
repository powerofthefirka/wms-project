<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShippingMethodTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->name;
        $resource['shipping_carrier'] = ['url' =>route('shipping_carriers.edit', ['shipping_carrier' => $this->shippingCarrier]), 'name' => $this->shippingCarrier->name];
        $resource['link_edit'] = route('shipping_methods.edit', ['id' => $this->id ]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('shipping_methods.destroy', ['id' => $this->id, 'shipping_method' => $this])];

        return $resource;
    }
}
