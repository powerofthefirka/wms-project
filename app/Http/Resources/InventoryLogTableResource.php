<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryLogTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['user'] =  ['url'=> route('users.edit',['user' => $this->user]) ,'name' =>$this->user->contactInformation->name];
        $resource['from'] =  $this->getSourceUrl();
        $resource['destination'] = $this->getDestinationUrl();
        $resource['price'] = $this->price;
        $resource['product'] = ['url' => strlen($this->product->deleted_at) == 0 ? route('products.edit',['product' => $this->product]) : '#', 'name' =>  $this->product->name];
        $resource['quantity'] = $this->quantity;

        return $resource;
    }
}
