<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['contact_information'] = new ContactInformationResource($this->contactInformation);

        $resource['billing_profile'] = new BillingProfileResource($this->billingProfile);
        unset($resource['billing_profile_id']);

        return $resource;
    }
}
