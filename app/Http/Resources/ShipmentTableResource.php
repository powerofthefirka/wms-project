<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ShipmentTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['order'] = ['id' => $this->order->id, 'number' => $this->order->number];
        $resource['shipped_at'] = localized_date($this->shipped_at);
        $resource['order_date'] = localized_date($this->order->ordered_at);
        $resource['order_shipping_email'] = $this->contactInformation->email;
        $resource['order_shipping_address'] = $this->contactInformation->address;
        $resource['order_shipping_address2'] = $this->contactInformation->address2;
        $resource['order_shipping_city'] = $this->contactInformation->city;
        $resource['order_shipping_zip'] = $this->contactInformation->zip;
        $resource['order_shipping_country'] = $this->contactInformation->country->code ?? '';
        $resource['order_shipping_company_name'] = $this->contactInformation->company_name;
        $resource['order_shipping_phone'] = $this->contactInformation->phone;
        $resource['distinct_items'] = $this->distinct_items;
        $resource['quantity_shipped'] = $this->quantity_shipped;
        $resource['line_item_total'] = $this->line_item_total;
        $resource['item_total_with_currency'] =  with_currency($this->order->customer, $this->line_item_total);

        $resource['link_view'] = ['url' => route('shipments.items_shipped', ['shipment' => $this]), 'tracking_code' => $this->tracking_code];

        return $resource;
    }
}
