<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    protected $customId;

    public function setCustomId($value)
    {
        $this->customId = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['customer'] = new CustomerResource($this->customer);
        unset($resource['customer_id']);

        $resource['order_items'] = new OrderItemCollection($this->orderItems);

        $resource['shipments'] = new ShipmentCollection($this->shipments);

        $resource['shipping_contact_information'] = new ContactInformationResource($this->shippingContactInformation);
        unset($resource['shipping_contact_information_id']);

        $resource['billing_contact_information'] = new ContactInformationResource($this->billingContactInformation);
        unset($resource['billing_contact_information_id']);

        $resource['order_lock_information'] = new OrderLockResource($this->orderLock);

        $resource['shipping_carrier'] = new ShippingCarrierResource($this->shippingCarrier);
        unset($resource['shipping_carrier_id']);

        $resource['shipping_method'] = new ShippingMethodResource($this->shippingMethod);
        unset($resource['shipping_method_id']);

        $resource['custom_id'] = $this->customId;

        return $resource;
    }
}
