<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['shipment_items'] = new ShipmentItemCollection($this->shipmentItems);

        $resource['contact_information'] = new ContactInformationResource($this->contactInformation);

        $resource['shippingBoxes'] = new PackageCollection($this->shippingBoxes);

        return $resource;
    }
}
