<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillingCustomerInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['date'] = $this->date;
        $resource['billing_profile'] = ['url' => route('billing_profiles.edit', ['billingProfile' => $this->billingProfile]), 'name' => $this->billingProfile->name];
        $resource['direct_url'] = ['url' => route('invoice.direct_url', ['direct_url' => $this->direct_url]), 'name' => $this->direct_url];
        $resource['link_edit'] = route('invoices.edit', ['invoice' => $this->id]);

        return $resource;
    }
}
