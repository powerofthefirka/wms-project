<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class DomainTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] =  $this->id;
        $resource['title'] =  $this->title;
        $resource['domain'] =  $this->domain;
        $resource['primary_color'] =  $this->primary_color;
        $resource['link_edit'] = route('domains.edit', ['three_pl' => $this->threePl->id, 'domain' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('domains.destroy', ['three_pl' => $this->threePl->id, 'id' => $this->id, 'domain' => $this])];

        return $resource;
    }
}
