<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class LocationTypeTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] =  $this->id;
        $resource['name'] =  $this->name;
        $resource['wms_id'] = $this->wms_id;
        $resource['daily_storage_cost'] = $this->daily_storage_cost;
        $resource['three_pl'] = ['name' => $this->threePl->contactInformation->name, 'url' => route('three_pls.edit', ['threePl' => $this->threePl])];
        $resource['link_edit'] = route('location_types.edit', ['location_type' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('location_types.destroy', ['id' => $this->id, 'location_type' => $this])];

        return $resource;
    }
}
