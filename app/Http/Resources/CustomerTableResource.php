<?php

namespace App\Http\Resources;

use App\Models\Customer;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->contactInformation->name;
        $resource['billing_profile'] = ['url' => route('billing_profiles.edit', ['billingProfile' => $this->billingProfile ?? null]), 'name' => $this->billingProfile->name ?? ''];
        $resource['company_name'] = $this->contactInformation->company_name;
        $resource['address'] = $this->contactInformation->address;
        $resource['address2'] = $this->contactInformation->address2;
        $resource['zip'] = $this->contactInformation->zip;
        $resource['city'] = $this->contactInformation->city;
        $resource['email'] = $this->contactInformation->email;
        $resource['phone'] = $this->contactInformation->phone;
        $resource['date_related_data'] = $this->integration_dates ?? Customer::INTEGRATION_SYNC_DATES;
        $resource['active_integration'] =  $this->active_integration;
        $resource['active_integration_edit'] =  !empty($this->local_key) ? route('customers.toggle_integration_status', ['customer' => $this]) : '';
        $resource['link_edit'] =  route('customers.edit', ['customer' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('customers.destroy', ['id' => $this->id, 'customer' => $this])];
        $resource['active'] =  $this->active;
        $resource['link_disable'] =  route('customers.disable', [ 'customer' => $this ]);
        if (!$this->active) {
            $resource['link_disable'] =  route('customers.enable', [ 'customer' => $this ]);
        }

        $bill = $this->bills()->orderBy('period_end', 'desc')->first();
        $resource['last_billed'] = ['url' => route('billings.customer_bill_items', ['customer' => $this->id, 'bill' => $bill->id ?? '']), 'amount' => $bill->amount ?? ''];

        return $resource;
    }
}
