<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\Supplier;

class ObjectChangeTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['column'] = $this->column;
        $resource['old_value'] = $this->old_value;
        $resource['new_value'] = $this->new_value;
        $resource['created_at'] = localized_date($this->created_at);

        if ($this->object_type == Product::class) {
            $resource['product'] = ['name' => $this->object->name ?? '', 'url' => route('products.edit', ['product' => $this->object])];

            if ($this->column == 'is_kit' ) {
                $resource['old_value'] = ($this->old_value == 1) ? "Yes" : "No";
                $resource['new_value'] = ($this->new_value == 1) ? "Yes" : "No";
            }

            if ($this->column == 'suppliers') {
                $oldSuppliers = "";
                $this->old_value = ltrim($this->old_value, "[");
                $this->old_value = rtrim($this->old_value, "]");

                $oldSuplierIds = explode(',', $this->old_value);
                foreach ($oldSuplierIds as $oldSuplierId) {
                    $oldSuppliers .= Supplier::find($oldSuplierId)->contactInformation->name ?? '';
                    $oldSuppliers .= ", ";
                }

                $resource['old_value'] = trim($oldSuppliers, ", ");

                $newSuppliers = "";
                $this->new_value = ltrim($this->new_value, "[");
                $this->new_value = rtrim($this->new_value, "]");

                $newSuplierIds = explode(',', $this->new_value);
                foreach ($newSuplierIds as $newSuplierId) {
                    $newSuppliers .= Supplier::find($newSuplierId)->contactInformation->name ?? '';
                    $newSuppliers .= ", ";
                }

                $resource['new_value'] = trim($newSuppliers, ", ");
            }
        } else if ($this->object_type == Order::class) {
            $resource['order'] = ['number' => $this->object->number, 'url' => route('orders.edit', ['order' => $this->object])];
            $resource['product_name'] = 'N/A';

            if ($this->column == 'shipping_carrier_id') {
                $resource['old_value'] = ShippingCarrier::find($this->old_value)->name ?? '';
                $resource['new_value'] = ShippingCarrier::find($this->new_value)->name ?? '';
            }

            if ($this->column == 'shipping_method_id') {
                $resource['old_value'] = ShippingMethod::find($this->old_value)->name ?? '';
                $resource['new_value'] = ShippingMethod::find($this->new_value)->name ?? '';
            }

            if ($this->column == 'is_cancelled' ) {
                $resource['old_value'] = ($this->old_value == 1) ? "Yes" : "No";
                $resource['new_value'] = ($this->new_value == 1) ? "Yes" : "No";
            }
        } else if ($this->object_type == OrderItem::class) {
            $resource['order'] = ['number' => $this->object->order->number ?? '', 'url' => route('orders.edit', [ 'order' => $this->object->order ?? '' ])];
            $resource['product_name'] = $this->object->name ?? '';
        } else if ($this->parent_object_type == Order::class) {
            $resource['order'] = ['number' => Order::find($this->parent_object_id)->number, 'url' => route('orders.edit', ['order' => Order::find($this->parent_object_id)])];
            $resource['product_name'] = 'N/A';
        } else if ($this->object_type == PurchaseOrder::class) {
            $resource['purchase_order'] = ['number' => $this->object->number, 'url' => route('purchase_orders.edit', ['purchase_order' => $this->object])];
            $resource['product_name'] = 'N/A';

            if ($this->column == 'supplier_id') {
                $resource['old_value'] = Supplier::find($this->old_value)->contactInformation->name ?? '';
                $resource['new_value'] = Supplier::find($this->new_value)->contactInformation->name ?? '';
            }

            if ($this->column == 'is_cancelled' ) {
                $resource['old_value'] = ($this->old_value == 1) ? "Yes" : "No";
                $resource['new_value'] = ($this->new_value == 1) ? "Yes" : "No";
            }
        } else if ($this->object_type == PurchaseOrderItem::class) {
            $resource['purchase_order'] = ['number' => $this->object->purchaseOrder->number, 'url' => route('purchase_orders.edit', ['purchase_order' => $this->object->purchaseOrder])];
            $resource['product_name'] = $this->object->name ?? '';
        }

        return $resource;
    }
}
