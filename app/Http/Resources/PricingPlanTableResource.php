<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PricingPlanTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->name;
        $resource['monthly_price_for_yearly_billing_period'] = $this->monthly_price_for_yearly_billing_period;
        $resource['monthly_price_for_monthly_billing_period'] = $this->monthly_price_for_monthly_billing_period;
        $resource['initial_fee'] = $this->initial_fee;
        $resource['three_pl_billing_fee'] = $this->three_pl_billing_fee;
        $resource['number_of_monthly_orders'] = $this->number_of_monthly_orders;
        $resource['price_per_additional_order'] = $this->price_per_additional_order;
        $resource['link_edit'] =  route('pricing_plans.edit', ['pricingPlan' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('pricing_plans.destroy', ['id' => $this->id, 'pricingPlan' => $this])];

        return $resource;
    }
}
