<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryChangeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['product'] = new ProductResource($this->product);
        unset($resource['product_id']);

        $resource['location'] = new LocationResource($this->location);
        unset($resource['location_id']);

        return $resource;
    }
}
