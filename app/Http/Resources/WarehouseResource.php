<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseResource extends JsonResource
{
    protected $customId;

    public function setCustomId($value){
        $this->customId = $value;
        return $this;
    }
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);

        $resource['customer'] = new CustomerResource($this->customer);
        unset($resource['customer_id']);

        $resource['contact_information'] = new ContactInformationResource($this->contactInformation);

        $resource['custom_id'] = $this->customId;

        return $resource;
    }
}
