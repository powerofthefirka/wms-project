<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebshipperCredentialTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['customer'] = ['url' => route('customers.edit', ['customer' => $this->customer]), 'name' => $this->customer->contactInformation->name];
        $resource['api_base_url'] = $this->api_base_url;
        $resource['api_key'] = $this->api_key;
        $resource['link_edit'] =route('webshipper_credential.edit', ['webshipper_credential' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('webshipper_credential.destroy', ['id' => $this->id, 'webshipper_credential' => $this])];

        return $resource;
    }
}
