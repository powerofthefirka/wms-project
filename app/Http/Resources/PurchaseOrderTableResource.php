<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseOrderTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['number'] = $this->number;
        $resource['status'] = $this->status;
        $resource['created_at'] = localized_date($this->ordered_at);
        $resource['expected_at'] = localized_date($this->expected_at);
        $resource['supplier'] = ['name' => $this->supplier->contactInformation->name ?? '', 'url' => route('vendors.edit', ['supplier' => $this->supplier])];
        $resource['warehouse'] = $this->warehouse->contactInformation->name;
        $resource['link_edit'] =route('purchase_orders.edit', ['purchase_order' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('purchase_orders.destroy', ['id' => $this->id, 'purchase_order' => $this])];

        return $resource;
    }
}
