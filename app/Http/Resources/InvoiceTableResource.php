<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Request;

class InvoiceTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['date'] = $this->date;
        $resource['total_invoice_lines'] = $this->total_invoice_lines;
        $resource['customer'] = ['url' => route('customers.edit', ['user' => $this->customer]), 'name' => $this->customer->contactInformation->name];
        $resource['billing_profile'] = ['url' => route('billing_profiles.edit', ['billingProfile' => $this->billingProfile]), 'name' =>
            Arr::get($this->billingProfile,'name','')];
        $resource['direct_url'] = ['url' => route('invoice.direct_url', ['direct_url' => $this->direct_url]), 'name' => $this->direct_url];
        $resource['link_edit'] =route('invoices.edit', ['invoice' => $this]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('invoices.destroy', ['id' => $this->id, 'invoice' => $this])];

        return $resource;
    }
}
