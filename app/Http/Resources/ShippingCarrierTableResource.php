<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShippingCarrierTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        unset($resource);

        $resource['id'] = $this->id;
        $resource['name'] = $this->name;
        $resource['three_pl'] = ['url' =>route('three_pls.edit', ['threePl' => $this->threePl]), 'name' => $this->threePl->contactInformation->name];
        $resource['link_edit'] = route('shipping_carriers.edit', ['id' => $this->id ]);
        $resource['link_delete'] = ['token' => csrf_token(), 'url' => route('shipping_carriers.destroy', ['id' => $this->id, 'shipping_carrier' => $this])];

        return $resource;
    }
}
