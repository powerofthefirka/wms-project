<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApiLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate(Request $request, $response)
    {
        $requestArr = $request->all();

        foreach ($requestArr as $key => $requestItem) {
            if (isset($requestArr[$key]['password'])) {
                unset($requestArr[$key]['password']);
            } elseif (isset($requestArr[$key]['password_confirmation'])) {
                unset($requestArr[$key]['password_confirmation']);
            }
        }

        Log::info('app.requests', [
            'method' => $request->getMethod(),
            'uri' => $request->getRequestUri(),
            'api-request' => $requestArr,
            'api-response' => $response
        ]);
    }
}
