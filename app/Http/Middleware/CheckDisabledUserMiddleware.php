<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckDisabledUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && !$this->isValidUser() && !$this->isValidCustomerAdministrator()) {
            Auth::logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route('login')->with('error', __('Your account has been disabled!'));
        }

        return $next($request);
    }

    private function isValidUser()
    {
        return auth()->user()->isAdmin() || auth()->user()->customers->where('active', 1)->isNotEmpty();
    }

    private function isValidCustomerAdministrator()
    {
        return auth()->user()->isCustomerAdmin() && auth()->user()->threePls->where('active', 1)->isNotEmpty();
    }

}
