<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class SessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            return $next($request);
        }

        $customers = app()->user->getCustomers();
        $sessionCustomerId = session('customer_id');

        if ($sessionCustomerId && !in_array($sessionCustomerId, $customers->pluck('id')->toArray()) && $customers->count() == 1) {
            Session::put('customer_id', $customers->first()->id);
        } elseif ($sessionCustomerId && !in_array($sessionCustomerId, $customers->pluck('id')->toArray())) {
            Session::forget('customer_id');
        } elseif (!$sessionCustomerId && $customers->count() == 1) {
            Session::put('customer_id', $customers->first()->id);
        }

        return $next($request);
    }
}
