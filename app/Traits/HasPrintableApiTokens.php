<?php

namespace App\Traits;

use App\Models\User;
use GuzzleHttp\Psr7\Response;
use Laravel\Passport\Bridge\AccessToken;
use Laravel\Passport\Bridge\Client;
use Laravel\Passport\Bridge\RefreshToken;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;

trait HasPrintableApiTokens
{
    public function printableTokens()
    {
        $tokens = $this->tokens()->get();

        foreach ($tokens as $token) {
            $token->accessToken = $this->fetchAccessTokenForClient($this, Passport::$personalAccessClientId, $token);
        }

        return $tokens;
    }

    private function fetchPassportTokenByUser(User $user, $clientId, Token $token)
    {
        $accessToken = new AccessToken($user->id);
        $accessToken->setIdentifier($token->id);
        $accessToken->setClient(new Client($clientId, null, null));
        $accessToken->setExpiryDateTime(new \DateTime($token->expires_at));

        $refreshToken = new RefreshToken();

        $refreshToken->setAccessToken($accessToken);
        $refreshToken->setExpiryDateTime(new \DateTime($token->expires_at));

        return [
            'access_token' => $accessToken,
            'refresh_token' => $refreshToken,
        ];
    }

    private function sendBearerTokenResponse(AccessToken $accessToken, RefreshToken $refreshToken)
    {
        $response = new BearerTokenResponse();
        $response->setAccessToken($accessToken);
        $response->setRefreshToken($refreshToken);

        $privateKey = new CryptKey('file://' . Passport::keyPath('oauth-private.key'));

        $response->setPrivateKey($privateKey);
        $response->setEncryptionKey(app('encrypter')->getKey());

        return $response->generateHttpResponse(new Response);
    }

    private function fetchAccessTokenForClient(User $user, $clientId, Token $token)
    {
        $passportToken = $this->fetchPassportTokenByUser($user, $clientId, $token);

        $bearerToken = $this->sendBearerTokenResponse($passportToken['access_token'], $passportToken['refresh_token']);

        $bearerToken = json_decode($bearerToken->getBody()->__toString(), true);

        return $bearerToken;
    }
}
