<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Models\CustomerUser;
use App\Models\Invoice;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\Shipment;
use http\Url;
use Illuminate\Console\Command;
use App\Http\Requests\Invoice\StoreRequest as InvoiceStoreRequest;
use App\Http\Requests\InvoiceLine\StoreRequest as InvoiceLineStoreRequest;

class InvoiceGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:generate {customer_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invoice Generator command';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function handle()
    {
        $customerId = $this->argument('customer_id');

        $customer = Customer::where('id', $customerId)->first();

        if (!$customer) {
            echo 'Invalid Customer';
            echo "\n";
            return;
        } elseif (!$customer->billing_profile_id) {
            echo 'Customer has no billing profile';
            echo "\n";
            return;
        }

        $requestData = array(
            'date' => date('Y-m-d'),
            'customer_id' => $customerId,
            'billing_profile_id' => $customer->billing_profile_id,
        );

        $storeRequest = InvoiceStoreRequest::make($requestData);
        $invoice = app()->invoice->store($storeRequest);

        $directUrl = date("Ymd") . $invoice->id;
        $invoice->update(['direct_url' => $directUrl]);

        $billingProfile = $customer->billingProfile;

        $productCount = $customer->products()->whereYear('created_at', date("Y"))->whereMonth('created_at', date("m"))->count();

        $shipmentCount = Shipment::join('orders', 'shipments.order_id', '=', 'orders.id')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->where('orders.customer_id', $customerId)
            ->whereYear('shipments.created_at', date("Y"))
            ->whereMonth('shipments.created_at', date("m"))
            ->count();

        $returnCount = Return_::join('orders', 'returns.order_id', '=', 'orders.id')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->where('orders.customer_id', $customerId)
            ->whereYear('returns.created_at', date("Y"))
            ->whereMonth('returns.created_at', date("m"))
            ->count();

        $perPurchaseOrderReceivedCount = PurchaseOrder::where('customer_id', $customerId)
            ->whereYear('created_at', date("Y"))
            ->whereMonth('created_at', date("m"))
            ->count();

        $customerUserCount = $customer->users->count();

        $usersCost = $billingProfile->per_user_cost * $customerUserCount;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => $customerUserCount . ' users',
            'cost' => $usersCost
        );

        $perPurchaseOrderReceivedCost = $billingProfile->per_purchase_order_received_cost * $perPurchaseOrderReceivedCount;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => 'Purchase order received cost ',
            'cost' => $perPurchaseOrderReceivedCost
        );

        $monthlyCost = $billingProfile->monthly_cost;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => 'Monthly cost',
            'cost' => $monthlyCost
        );

        $productCost = $billingProfile->per_product_cost * $productCount;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => $productCount . ' products / month',
            'cost' => $productCost
        );

        $shipmentCost = $billingProfile->per_shipment_cost * $shipmentCount;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => $shipmentCount . ' shipments / month',
            'cost' => $shipmentCost
        );

        $returnCost = $billingProfile->per_return_cost * $returnCount;
        $invoiceLineData[] = array(
            'invoice_id' => $invoice->id,
            'title' => $returnCount . ' returns / month',
            'cost' => $returnCost
        );

        foreach ($invoiceLineData as $row) {
            $storeRequest = InvoiceLineStoreRequest::make($row);
            $invoiceLine = app()->invoiceLine->store($storeRequest);
        }

        echo "\n";
        echo "Invoice Url: " . URL::to('/') . "/invoices/" . $invoice->direct_url . " \n";
    }
}
