<?php

namespace App\Console\Commands;

use App\Models\ContactInformation;
use App\Models\Customer;
use App\Models\CustomerUserRole;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateCustomerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('Enter customer name');
        $customer = Customer::create();
        $this->createContactInformation($customer, [
            'name' => $name
        ]);

        do {
            $name = $this->ask('Enter name of the user');
            $email = $this->ask('Enter email of the user');
            $password = Str::random();

            $user = User::create([
                'email' => $email,
                'password' => Hash::make($password)
            ]);

            $this->createContactInformation($user, [
                'name' => $name,
                'email' => $email
            ]);

            $customer->users()->syncWithoutDetaching([
                $user->id => [
                    'role_id' => CustomerUserRole::ROLE_DEFAULT
                ]
            ]);

            $user->createToken('API');
            $accessToken = $user->printableTokens()->first()->accessToken['access_token'];

            $this->output->writeln('Login details:');
            $this->output->writeln('Id: ' . $customer->id);
            $this->output->writeln('Email: ' . $email);
            $this->output->writeln('Password: ' . $password);
            $this->output->writeln('Access token: ' . $accessToken);
        } while ($this->choice('Add another user?', ['Yes', 'No'], 'No') == 'Yes');
    }

    private function createContactInformation($object, $data = []): ContactInformation
    {
        $contactInformation = ContactInformation::create($data);

        $contactInformation->object()->associate($object);
        $contactInformation->save();

        return $contactInformation;
    }
}
