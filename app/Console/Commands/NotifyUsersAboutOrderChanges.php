<?php

namespace App\Console\Commands;

use App\Mail\Orders\Backorder;
use App\Mail\Orders\OrderCancelled;
use App\Mail\Orders\OrderFulfilled;
use App\Mail\Orders\OrderOnHold;
use App\Mail\Orders\OrderPending;
use App\Mail\Orders\OrderPriority;
use App\Models\ObjectChange;
use App\Models\Order;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NotifyUsersAboutOrderChanges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:notifyUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies users about changes in order statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get all users that have email notifications for orders enabled
        $users = User::where("email_notifications", "like", "%order%")->get();

        foreach ($users as $user) {

            $object_changes = ObjectChange::where('user_id', $user->id)
                ->where('created_at', '>', $user->last_notified_at)
                ->where('object_type', 'App\Models\Order')
                ->where('column', 'status')
                ->get();

            //check if user has notifications enabled for order status change
            foreach ($object_changes as $object_change) {

                $order = Order::find($object_change->object_id);

                if (!$order) continue;

                switch (true) {
                    case $object_change->new_value == 'backorder' && hasEmailNotificationEnabled($user, 'backorder'):
                        Mail::to($user->email)->queue(new Backorder($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    case $object_change->new_value == 'fulfilled' && hasEmailNotificationEnabled($user, 'order_fulfilled'):
                        Mail::to($user->email)->queue(new OrderFulfilled($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    case $object_change->new_value == 'cancelled' && hasEmailNotificationEnabled($user, 'order_cancelled'):
                        Mail::to($user->email)->queue(new OrderCancelled($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    case $object_change->new_value == 'priority' && hasEmailNotificationEnabled($user, 'order_priority'):
                        Mail::to($user->email)->queue(new OrderPriority($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    case $object_change->new_value == 'pending' && hasEmailNotificationEnabled($user, 'order_pending'):
                        Mail::to($user->email)->queue(new OrderPending($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    case $object_change->new_value == 'on_hold' && hasEmailNotificationEnabled($user, 'order_on_hold'):
                        Mail::to($user->email)->queue(new OrderOnHold($object_change->object_id, $order->number, $user->contactInformation['name']));
                        break;
                    default:
                }

                //set new last_notified_at value
                if ($object_change->created_at > $user->last_notified_at)
                    $user->last_notified_at = $object_change->created_at;
            }

            //save last_notified_at to db
            $user->save();
        }
    }
}
