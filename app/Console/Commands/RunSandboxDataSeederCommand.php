<?php

namespace App\Console\Commands;

use App\Jobs\RunSandboxDataSeederJob;
use App\Models\Customer;
use Illuminate\Console\Command;

class RunSandboxDataSeederCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sandbox:seed {customer-id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset sandbox data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customerId = $this->argument('customer-id');

        RunSandboxDataSeederJob::dispatchNow(Customer::find($customerId));
    }
}
