<?php

namespace App\Components\Messenger;

use App\Models\Customer;
use App\Models\User;

interface MessengerProviderInterface
{
	public function render(Customer $customer, User $user);

	public function requiredFields(Customer $customer);
}
