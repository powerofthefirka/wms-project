<?php

namespace App\Components\Messenger;

use App\Components\Messenger\Providers\BeaconComponent;
use App\Components\Messenger\Providers\TawkComponent;
use App\Models\Customer;
use App\Models\User;

class MessengerProvider implements MessengerProviderInterface
{
    private static $providers = [
        TawkComponent::class => 'Tawk.to',
        BeaconComponent::class => 'HelpScout.net'
    ];

    protected $provider;

    public function initializeProvider(Customer $customer)
    {
        if (empty($customer->messaging_provider) ||
            empty(self::$providers[$customer->messaging_provider]) ||
            !class_exists($customer->messaging_provider)
        ) {
            return false;
        }

        $this->provider = new $customer->messaging_provider();

        return true;
    }

    public function render(Customer $customer, User $user)
    {
        if (!$this->initializeProvider($customer)) {
            return '';
        }

        return $this->provider->render($customer, $user);
    }

    public function requiredFields(Customer $customer)
    {
        if (!$this->initializeProvider($customer)) {
            return '';
        }

        return $this->provider->requiredFields($customer);
    }

    /**
     * @return array
     */
    public static function getMessengerProviders()
    {
        return self::$providers;
    }
}
