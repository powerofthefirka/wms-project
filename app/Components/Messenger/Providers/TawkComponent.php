<?php

namespace App\Components\Messenger\Providers;

use App\Components\Messenger\MessengerProvider;
use App\Components\Messenger\MessengerProviderInterface;
use App\Models\Customer;
use App\Models\User;

class TawkComponent extends MessengerProvider implements MessengerProviderInterface
{
	public function render(Customer $customer, User $user)
    {
		return view('messenger.tawk', [
		    'customer' => $customer,
            'user' => $user
        ]);
	}

	public function requiredFields(Customer $customer)
    {
        return [];
    }
}
