<?php

namespace App\Components;

use App\Http\Requests\Domain\DestroyBatchRequest;
use App\Http\Requests\Domain\DestroyRequest;
use App\Http\Requests\Domain\StoreBatchRequest;
use App\Http\Requests\Domain\StoreRequest;
use App\Http\Requests\Domain\UpdateBatchRequest;
use App\Http\Requests\Domain\UpdateRequest;
use App\Http\Requests\Domain\FilterRequest;
use App\Models\Domain;
use App\Models\Image;
use App\Models\ThreePl;
use Illuminate\Support\Facades\Storage;

class DomainComponent extends BaseComponent
{
    public function __construct()
    {

    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $domain = Domain::create($input);

        $images = ['logo_id', 'favicon_id'];

        foreach ($images as $image) {
            if ($request->file($image)) {
                $filename = $request->file($image)->store('public');
                $source = url(Storage::url($filename));

                $imageObj = new Image();
                $imageObj->source = $source;
                $imageObj->filename = $filename;
                $imageObj->object()->associate($domain);
                $imageObj->save();

                $input[$image] = $imageObj->id;
            }
        }

        $domain->update($input);

        return $domain;
    }

    public function update(UpdateRequest $request, Domain $domain)
    {
        $input = $request->validated();
        $images = ['logo_id', 'favicon_id'];

        foreach ($images as $image) {
            if ($request->file($image)) {
                $filename = $request->file($image)->store('public');
                $source = url(Storage::url($filename));

                $imageObj = new Image();
                $imageObj->source = $source;
                $imageObj->filename = $filename;
                $imageObj->object()->associate($domain);
                $imageObj->save();

                $input[$image] = $imageObj->id;
            }
        }

        $domain->update($input);

        return $domain;
    }

    public function destroy(DestroyRequest $request = null, Domain $domain = null)
    {
        return $domain->delete();
    }

    public function search($term, ThreePl $threePl)
    {
        $domainCollection = Domain::where('3pl_id', $threePl->id);

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $domainCollection = $domainCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $domainCollection
                ->where(function($query) use ($term) {
                    $query->where('domains.title', 'like', $term);
                });
        }

        return $domainCollection;
    }
}
