<?php

namespace App\Components;

use App\Models\BillingFee;
use App\Models\BillingProfile;

class BillingFeeComponent extends BaseComponent
{
    public function store($input, $type, BillingProfile $billingProfile)
    {
        $input['type'] = $type;
        $input['billing_profile_id'] = $billingProfile->id;

        $billingFee = BillingFee::create($input);

        return $billingFee;
    }

    public function update($input, BillingFee $billingFee)
    {
        $billingFee->update($input);

        return $billingFee;
    }

    public function destroy(BillingFee $billingFee)
    {
        $billingFee->delete();
        return $billingFee;
    }
}
