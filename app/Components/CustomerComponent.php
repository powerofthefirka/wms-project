<?php

namespace App\Components;

use App\Currency;
use App\Http\Requests\Customer\DestroyBatchRequest;
use App\Http\Requests\Customer\DestroyRequest;
use App\Http\Requests\Customer\StoreBatchRequest;
use App\Http\Requests\Customer\UpdateBatchRequest;
use App\Http\Requests\Customer\UpdateRequest;
use App\Http\Requests\Customer\UpdateUsersBatchRequest;
use App\Http\Requests\Customer\UpdateUsersRequest;
use App\Models\Customer;
use App\Models\CustomerShippingMethod;
use App\Models\CustomerUser;
use App\Models\CustomerUserRole;
use App\Models\ShippingCarrier;
use App\Models\User;
use App\Models\ThreePl;
use App\Http\Requests\Customer\StoreRequest;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp;
use Illuminate\Support\Facades\URL;

class CustomerComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $contactInformationData = Arr::get($input, 'contact_information');

        $customer = Customer::create(Arr::except($input, ['contact_information']));

        $this->createContactInformation($contactInformationData, $customer);

        if ($customer->threePl && count($customer->threePl->users) > 0) {
            $customerUserRoles = [];

            foreach ($customer->threePl->users->pluck('id') as $key => $value) {
                $customerUserRoles[$value] = ['role_id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR];
            }

            $customer->users()->syncWithoutDetaching($customerUserRoles);
        }
        
        $customer->shippingMethods()->sync(Arr::get($input, 'shipping_methods', []));
        $customer->locations()->sync(Arr::get($input, 'locations', []));

        return $customer;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Customer $customer, $syncIngnore = false)
    {
        $input = $request->validated();

        $customer->update($input);
        $customer->contactInformation->update(Arr::get($input, 'contact_information'));
        $customer->update(Arr::except($input, ['contact_information']));

        if (!$syncIngnore) {
            $customer->shippingMethods()->sync(Arr::get($input, 'shipping_methods', []));
            $customer->locations()->sync(Arr::get($input, 'locations', []));
        }

        return $customer;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $customer = Customer::where('id', $record['id'])->first();

            $responseCollection->add($this->update($updateRequest, $customer));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, Customer $customer = null)
    {
        $customer->delete();

        return ['id' => $customer->id, 'name' => $customer->contactInformation->name];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $customer = Customer::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $customer));
        }

        return $responseCollection;
    }

    public function toggleCustomerStatus(Customer $customer, $status = 1)
    {
        if (!empty($customer->local_key)) {
            $customer->active_integration = $status;
            CamelIntegration::updateIntegrationStatus($customer->local_key, 0);
        }

        $customer->active = $status;
        $customer->save();
    }

    public function detachUser(Customer $customer, User $user)
    {
        return $customer->users()->detach($user->id);
    }

    public function updateUsers(UpdateUsersRequest $request, Customer $customer)
    {
        $customerUserRoles = [];

        foreach ($request->input('customer_user', []) as $customerUser) {
            $customerUserRoles[$customerUser['user_id']] = ['role_id' => $customerUser['role_id']];
        }

        if ($newCustomerUserId = $request->input('new_user_id')) {
            $customerUserRoles[$newCustomerUserId] = ['role_id' => $request->get('new_user_role_id')];
        }

        $customer->users()->syncWithoutDetaching($customerUserRoles);

        return CustomerUser::where('customer_id', $customer->id)->get();
    }

    public function filterUsers(Request $request, Customer $customer)
    {
        $term = $request->get('term');
        $results = $usersIds = [];

        if ($term) {
            $threePlUserIds = $customer->threePl->users->pluck('id')->toArray();
            foreach ($customer->threePl->customers as $threeplCustomer) {
                $threePlUserIds = array_merge($threePlUserIds, $threeplCustomer->users->pluck('id')->toArray());
            }

            foreach ($customer->users as $users) {
                $usersIds[] = $users->id;
            }

            $users = User::whereHas('contactInformation', function ($query) use ($term) {
                // TODO: sanitize term
                $term = '%' . $term . '%';

                $query->where('name', 'like', $term)
                    ->orWhere('company_name', 'like', $term)
                    ->orWhere('email', 'like', $term)
                    ->orWhere('zip', 'like', $term)
                    ->orWhere('city', 'like', $term)
                    ->orWhere('phone', 'like', $term);
            })->whereIn('id', array_diff($threePlUserIds, $usersIds))->get();

            foreach ($users as $user) {
                $results[] = [
                    'id' => $user->id,
                    'text' => $user->contactInformation->name . ', ' . $user->contactInformation->email . ', ' . $user->contactInformation->zip . ', ' . $user->contactInformation->city . ', ' . $user->contactInformation->phone
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function getUserCustomers(User $user)
    {
        return $user->customers()->paginate();
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds(true, true);
        $customerCollection = Customer::query();

        if (!empty($customerIds)) {
            $customerCollection = $customerCollection->whereIn('customers.id', $customerIds);
        }

        if ($term) {
            $term = '%' . $term . '%';

            $customerCollection
                ->where(function ($query) use ($term) {
                    $query->whereHas('contactInformation', function ($query) use ($term) {
                        $query->where('name', 'like', $term)
                            ->orWhere('company_name', 'like', $term)
                            ->orWhere('address', 'like', $term)
                            ->orWhere('address2', 'like', $term)
                            ->orWhere('zip', 'like', $term)
                            ->orWhere('city', 'like', $term)
                            ->orWhere('email', 'like', $term)
                            ->orWhere('phone', 'like', $term);
                    });
                });
        }

        return $customerCollection;
    }

    public function getShippingMethodsAndCarriers($customer)
    {
        $carriers = ShippingCarrier::where('3pl_id', $customer->threePl->id);
        $customerShippingMethodIds = CustomerShippingMethod::where('customer_id', $customer->id)->pluck('shipping_method_id');

        $shippingMethods = $carriers
            ->join('shipping_methods', 'shipping_carriers.id', '=', 'shipping_methods.shipping_carrier_id')
            ->whereIn('shipping_methods.id', $customerShippingMethodIds)
            ->where('shipping_methods.is_visible', true)
            ->groupBy('shipping_methods.id')
            ->select(
                'shipping_carriers.id',
                'shipping_carriers.customer_id',
                'shipping_carriers.id as carrier_id',
                'shipping_carriers.name as carrier_name',
                'shipping_methods.id as method_id',
                'shipping_methods.name as method_title',
                'shipping_methods.cost as method_cost'
            )
            ->get();

        foreach ($shippingMethods as $method) {
            $method->cost_with_currency = with_currency($customer, $method->method_cost);
        }

        return $shippingMethods;
    }

    public function getCurrencyAndUnit(Customer $customer)
    {
        return [
            'dimensions_unit' => $customer->dimensions_unit,
            'weight_unit' => $customer->weight_unit,
            'currency_code' => $customer->currency ? $customer->currency->code : $customer->currency_code
        ];
    }

    public function filter3PLs(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $contactInformation = ThreePl::whereHas('contactInformation', function ($query) use ($term) {
                $query->where('name', 'like', '%' . $term . '%')
                    ->orWhere('company_name', 'like', '%' . $term . '%')
                    ->orWhere('email', 'like',  '%' . $term . '%')
                    ->orWhere('zip', 'like', '%' . $term . '%')
                    ->orWhere('city', 'like', '%' . $term . '%')
                    ->orWhere('phone', 'like', '%' . $term . '%');
            });

            if (!auth()->user()->isAdmin()) {
                $contactInformation = $contactInformation->whereHas('users', function ($query) {
                    $query->where('user_id', auth()->user()->id);
                });
            }

            foreach ($contactInformation->get() as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterCurrencies(Request $request)
    {
        $term = $request->get('term');
        $currencies = [];

        if ($term) {
            $currencies = Currency::where('name', 'like', '%'.$term.'%')
                ->orWhere('code', 'like', '%'.$term.'%')
                ->pluck('code', 'id');
        }

        return response()->json([
            'results' => $currencies
        ]);
    }
}
