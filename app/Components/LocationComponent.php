<?php

namespace App\Components;

use App\Http\Requests\Location\DestroyBatchRequest;
use App\Http\Requests\Location\DestroyRequest;
use App\Http\Requests\Location\StoreBatchRequest;
use App\Http\Requests\Location\StoreRequest;
use App\Http\Requests\Location\UpdateBatchRequest;
use App\Http\Requests\Location\UpdateRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\LocationCollection;
use App\Http\Resources\LocationResource;
use App\Models\Location;
use App\Models\LocationProduct;
use App\Models\Product;
use App\Models\ThreePl;
use App\Models\LocationType;
use App\Models\CustomerLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class LocationComponent extends BaseComponent
{
    public function __construct()
    {
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();
        $input = $this->setLocationType($input);

        // TODO: figure out a better way to deal with this
        $input['pickable'] ?? $input['pickable'] = 0;

        $locationArr = Arr::except($input, ['location_products']);

        $location = Location::create($locationArr);
        $location->warehouse->customer->locations()->syncWithoutDetaching([$location->id]);

        $locationProducts = isset($input['location_products']) ? Arr::get($input, 'location_products') : [];

        foreach ($locationProducts as $key => $item) {
            if (empty($item['product_id'])) {

                continue;
            } else {
                $item['location_id'] = $location->id;
                LocationProduct::create($item);
            }
        }

        return $location;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Location $location)
    {
        $input = $request->validated();
        $input = $this->setLocationType($input);

        // TODO: figure out a better way to deal with this
        $input['pickable'] ?? $input['pickable'] = 0;

        if (isset($input['location_products'])) {
            $this->updateLocationProducts($location, Arr::get($input, 'location_products'));
        }

        Arr::forget($input, 'location_products');

        $location->update($input);

        return $location;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $outsideRequest = false;

        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;

            $location = Location::where('id', $record['id'])->where('warehouse_id', $record['warehouse_id'])->first();

            if ($location) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $location));
            } else {
                $storeRequest = StoreRequest::make($record);
                $responseCollection->add($this->store($storeRequest));
            }
        }

        $responseCollection->transform(function (Location $loc, $key) use ($input) {
            if (isset($input[$key]['custom_id'])) {
                return (new LocationResource($loc))->setCustomId($input[$key]['custom_id']);
            }

            return (new LocationResource($loc));
        });

        return $responseCollection;
    }

    public function updateLocationProducts(Location $location, $locationProducts)
    {
        foreach ($locationProducts as $key => $item) {
            if (!isset($item['location_product_id']) && $item['quantity_on_hand'] > 0) {
                $item['location_id'] = $location->id;
                LocationProduct::create($item);
            } elseif (isset($item['location_product_id']) && $item['quantity_on_hand'] == 0) {
                LocationProduct::where('id', $item['location_product_id'])->delete();
            } elseif (isset($item['location_product_id'])) {
                LocationProduct::find($item['location_product_id'])->update(Arr::except($item, ['location_product_id']));
            }
        }
    }

    public function destroy(DestroyRequest $request = null, Location $location = null)
    {
        $location->delete();

        return ['name' => $location->name];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $location = Location::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $location));
        }

        return $responseCollection;
    }

    public function filterProducts(Request $request, Location $location)
    {
        $term = $request->get('term');

        $results = [];
        $productIds = [];

        if ($term) {
            if (isset($location->products)) {
                foreach ($location->products as $product) {
                    $productIds[] = $product->product_id;
                }
            }

            $term = '%' . $term . '%';
            $products = Product::where('name', 'like', $term)
                ->orWhere('sku', 'like', $term)->get();

            foreach ($products as $product) {
                $results[] = [
                    'id' => $product->id,
                    'text' => 'SKU: ' . $product->sku . ', NAME:' . $product->name
                ];
            }

            return response()->json([
                'results' => $results
            ]);
        }
    }

    public function search($term)
    {
        $locationCollection = Location::query();

        $customerLocationIds = CustomerLocation::whereIn('customer_id', Auth()->user()->customerIds())->pluck('location_id');

        if (!empty($customerLocationIds)) {
            $locationCollection = $locationCollection->whereIn('locations.id', $customerLocationIds);
        }

        if ($term) {
            $term = '%' . $term . '%';

            $locationCollection
            ->where(function ($query) use ($term) {
                $query->orWhereHas('warehouse.contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', $term)
                        ->orWhere('address', 'like', $term)
                        ->orWhere('city', 'like', $term)
                        ->orWhere('zip', 'like', $term)
                        ->orWhere('email', 'like', $term)
                        ->orWhere('phone', 'like', $term);
                })
                ->orWhere('locations.name', 'like', $term);
            });
        }

        return $locationCollection;
    }

    public function filterLocations(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $term = '%' . $term . '%';

            $locations = Location::where('name', 'like', $term)->get();

            foreach ($locations as $location) {
                $results[] = [
                    'id' => $location->id,
                    'text' => $location->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function get3plLocations(ThreePl $threePl)
    {
        $locationTypes = LocationType::where('3pl_id', $threePl->id)->groupBy('wms_id')->pluck('id');

        return Location::whereIn('location_type_id', $locationTypes)
            ->groupBy('name')
            ->orderBy('name', 'asc')
            ->get();
    }
}
