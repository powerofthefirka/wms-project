<?php

namespace App\Components;

use App\Http\Requests\ShippingMethod\StoreBatchRequest;
use App\Http\Requests\ShippingMethod\StoreRequest;
use App\Http\Requests\ShippingMethod\UpdateBatchRequest;
use App\Http\Requests\ShippingMethod\UpdateRequest;
use App\Http\Requests\ShippingMethod\DestroyBatchRequest;
use App\Http\Requests\ShippingMethod\DestroyRequest;
use App\Models\Customer;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\ThreePl;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Resources\ShippingMethodCollection;
use App\Http\Resources\ShippingMethodResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ShippingMethodComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $shippingMethod = ShippingMethod::create($input);

        if (session('customer_id')) {
            Customer::find(session('customer_id'))->shippingMethods()->syncWithoutDetaching([$shippingMethod->id]);
        }

        return $shippingMethod;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, ShippingMethod $shippingMethod)
    {
        $input = $request->validated();

        $shippingMethod->update($input);

        return $shippingMethod;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $shippingMethod = ShippingMethod::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $shippingMethod, false));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, ShippingMethod $shippingMethod)
    {
        $shippingMethod->delete();

        $response = ['id' => $shippingMethod->id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $shippingMethod = ShippingMethod::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $shippingMethod, false));
        }

        return $responseCollection;
    }

    public function getShippingMethods(Request $request, ShippingCarrier $shippingCarrier)
    {
        $results = [];

        foreach ($shippingCarrier->shippingMethods as $method) {
            $results[] = [
                'id' => $method->id,
                'text' => $method->name,
                'cost' => $method->cost
            ];
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function get3plShippingMethods(ThreePl $threePl)
    {
        $shippingCarriers = ShippingCarrier::where('3pl_id', $threePl->id)->groupBy('wms_id')->pluck('id');

        return ShippingMethod::whereIn('shipping_carrier_id', $shippingCarriers)
            ->groupBy('wms_id')
            ->where('is_visible', 1)
            ->orderBy('name', 'asc')
            ->get();
    }
}
