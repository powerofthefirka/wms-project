<?php

namespace App\Components;

use Illuminate\Http\Request;
use App\Models\LocationType;
use App\Models\ThreePl;
use Illuminate\Support\Collection;
use App\Http\Resources\LocationTypeResource;
use App\Http\Resources\LocationTypeCollection;
use App\Http\Requests\LocationType\StoreRequest;
use App\Http\Requests\LocationType\UpdateRequest;
use App\Http\Requests\LocationType\DestroyRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Requests\LocationType\StoreBatchRequest;
use App\Http\Requests\LocationType\UpdateBatchRequest;
use App\Http\Requests\LocationType\DestroyBatchRequest;

class LocationTypeComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $locationType = LocationType::create($input);

        return $locationType;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, LocationType $locationType)
    {
        $input = $request->validated();

        $locationType->update($input);

        return $locationType;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $locationType = LocationType::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $locationType));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, LocationType $locationType)
    {
        $locationType->delete();

        $response = ['id' => $locationType->id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $locationType = LocationType::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $locationType));
        }

        return $responseCollection;
    }

    public function search($term, $threePl = false)
    {
        if (!$threePl) {
            $threePlIds = Auth()->user()->threePlIds();
        } else {
            $threePlIds = [$threePl];
        }

        $orderCollection = LocationType::query();

        if (!empty($threePlIds)) {
            $orderCollection = $orderCollection->whereIn('location_types.3pl_id', $threePlIds);
        }

        if ($term) {
            $term = '%' . $term . '%';

            $orderCollection
                ->where(function ($query) use ($term) {
                    $query->orWhereHas('threePl.contactInformation', function ($query) use ($term) {
                        $query->where('name', 'like', $term);
                    })
                    ->orWhere('name', 'like', $term);
                });
        }

        return $orderCollection;
    }
    
    public function filter3Pls(Request $request)
    {
        $threePlIds = Auth()->user()->threePlIds();
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = ThreePl::where('id', '=', $term)->get();

            if (!$contactInformation->count()) {
                $contactInformation = ThreePl::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->where('active', 1)->whereIn('3pls.id', $threePlIds)->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }
}
