<?php

namespace App\Components;

use App\Http\Resources\InventoryLogResource;
use App\Models\InventoryLog;
use App\Models\LocationProduct;
use Illuminate\Support\Facades\DB;

class InventoryLogComponent
{
    public function updateLocationProduct($userId, $product, $quantity = null, $source, $destination, $operation = null)
    {
        $quantity = $quantity ?? 0;


        if ($operation == 'ship' || $operation == 'transfer') {
            LocationProduct::updateOrCreate(
                ['product_id' => $product->id, 'location_id' => $source->id],
                ['quantity_on_hand' => DB::raw('quantity_on_hand  - ' .  $quantity)]
            );
        }

        if ($operation == 'receive' || $operation == 'transfer') {

            LocationProduct::updateOrCreate(
                ['product_id' => $product->id, 'location_id' => $destination->id],
                ['quantity_on_hand' => DB::raw('quantity_on_hand + ' . $quantity)]
            );
        }

        if ($operation == 'ship') {

            $product->quantity_on_hand -= $quantity;
            $product->quantity_available -= $quantity;
        } elseif ($operation == 'receive') {

            $product->quantity_on_hand += $quantity;
            $product->quantity_available += $quantity;
        }

        $product->save();

        $inventoryLog = $this->createInventoryLog($userId, $product->id, $source, $destination, $quantity);

        return $inventoryLog;
    }

    public function createInventoryLog($user_id, $product_id, $source, $destination, $quantity)
    {
        $returns = null;

        if ($quantity !== '0') {
            $inventory = new InventoryLog();
            $inventory->user_id = $user_id;
            $inventory->product_id = $product_id;
            $inventory->source()->associate($source);
            $inventory->destination()->associate($destination);
            $inventory->quantity = $quantity;
            $inventory->save();

            $returns = new InventoryLogResource($inventory);
        }
        return $returns;
    }
}
