<?php

namespace App\Components;

use App\Models\InventoryChange;
use App\Models\Location;
use App\Models\LocationProduct;
use Illuminate\Support\Collection;
use App\Http\Requests\InventoryChange\StoreRequest;
use App\Http\Requests\InventoryChange\UpdateRequest;
use App\Http\Requests\InventoryChange\DestroyRequest;
use App\Http\Requests\InventoryChange\StoreBatchRequest;
use App\Http\Requests\InventoryChange\UpdateBatchRequest;
use App\Http\Requests\InventoryChange\DestroyBatchRequest;
use App\Http\Requests\Location\StoreRequest as LocationStoreRequest;

class InventoryChangeComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        if (!isset($inpu['location_id']) && isset($input['location'])) {
            $location = $this->getLcoation($input['location']);
            $input['location_id'] = $location->id;
        }

        if (isset($input['location_id'])) {
            $this->updateProductLocation($input);
        }

        $inventoryChange = InventoryChange::create($input);

        return $inventoryChange;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, InventoryChange $inventoryChange)
    {
        $input = $request->validated();

        $inventoryChange->update($input);

        return $inventoryChange;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $inventoryChange = InventoryChange::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $inventoryChange, false));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, InventoryChange $inventoryChange)
    {
        $inventoryChange->delete();

        $response = ['id' => $inventoryChange->id, 'customer_id' => $inventoryChange->product->customer_id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $inventoryChange = InventoryChange::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $inventoryChange, false));
        }

        return $responseCollection;
    }

    public function getLcoation($locationData)
    {
        $location = Location::where('name', $locationData['name'])->where('warehouse_id', $locationData['warehouse_id'])->first();

        if (!$location) {
            $locationStoreRequest = LocationStoreRequest::make($locationData);

            $location = app()->location->store($locationStoreRequest, false);
        }

        return $location;
    }

    public function updateProductLocation($input)
    {
        $locationProduct = LocationProduct::where('product_id', $input['product_id'])->where('location_id', $input['location_id'])->first();

        if ($locationProduct) {
            $locationProduct->update(['quantity_on_hand' => $input['previous_on_hand'] + $input['change_in_on_hand']]);
        } else {
            LocationProduct::create([
                'product_id' => $input['product_id'],
                'location_id' => $input['location_id'],
                'quantity_on_hand' => $input['previous_on_hand'] + $input['change_in_on_hand']
            ]);
        }
    }
}