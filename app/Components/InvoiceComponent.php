<?php

namespace App\Components;

use App\Http\Requests\Invoice\StoreRequest;
use App\Http\Requests\Invoice\StoreBatchRequest;
use App\Http\Requests\Invoice\UpdateRequest;
use App\Http\Requests\Invoice\UpdateBatchRequest;
use App\Http\Requests\Invoice\DestroyRequest;
use App\Http\Requests\Invoice\DestroyBatchRequest;
use App\Http\Requests\InvoiceLine\UpdateBatchRequest as UpdateInvoiceLineBatchRequest;
use App\Models\Invoice;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;


class InvoiceComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $invoice = Invoice::create($input);

        return $invoice;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Invoice $invoice)
    {
        $input = $request->validated();

        $invoiceLines = Arr::get($request, 'invoice_lines');

        if($invoiceLines) {
            foreach ($invoiceLines as $key => $line) {
                $invoiceLines[$key]['invoice_id'] = $invoice->id;
            }

            $updateInvoiceLineBatch = UpdateInvoiceLineBatchRequest::make($invoiceLines);

            app()->invoiceLine->updateBatch($updateInvoiceLineBatch);
        }

        $invoice->update($input);

        return $invoice;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $invoice = Invoice::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $invoice, false));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, Invoice $invoice = null)
    {
        $invoice->delete();

        return ['id' => $invoice->id, 'name' => $invoice->direct_url];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $invoice = Invoice::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $invoice));
        }

        return $responseCollection;
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $invoiceCollection = Invoice::query();

        if (!empty($customerIds)) {
            $invoiceCollection = $invoiceCollection->whereIn('invoices.customer_id', $customerIds);
        }

        if ($term) {
            $term = '%' . $term . '%';

            $invoiceCollection
                ->where(function($query) use ($term) {
                    $query->where('invoices.date', 'like', $term)
                        ->orWhere('invoices.direct_url', 'like', $term)
                        ->orWhereHas('customer.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhereHas('billingProfile', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        });
                });
        }

        return $invoiceCollection;
    }
}
