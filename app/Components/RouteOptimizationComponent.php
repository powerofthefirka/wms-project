<?php

namespace App\Components;

use App\Http\Requests\PickingBatch\PickingBatchRequest;
use App\Http\Requests\PickingBatch\PickRequest;
use App\Models\User;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderLock;
use App\Models\PickingBatch;
use App\Models\PickingBatchItem;
use App\Models\Location;

class RouteOptimizationComponent extends BaseComponent
{
    public function __construct()
    {
        $this->startLocation = 'A1';
        $this->horizontalBlockDistance = 10;

        $this->frontEndOddValue['A'] = ['number' => 1, 'block' => 1];
        $this->rearEndOddValue['A'] = ['number' => 49, 'block' => 1];
        $this->frontEndOddValue['B'] = ['number' => 49, 'block' => 1];
        $this->rearEndOddValue['B'] = ['number' => 1, 'block' => 1];

        $this->frontEndEvenValue['B'] = ['number' => 50, 'block' => 2];
        $this->rearEndEvenValue['B'] = ['number' => 2, 'block' => 2];
        $this->frontEndOddValue['C'] = ['number' => 1, 'block' => 2];
        $this->rearEndOddValue['C'] = ['number' => 49, 'block' => 2];

        $this->frontEndEvenValue['C'] = ['number' => 2, 'block' => 3];
        $this->rearEndEvenValue['C'] = ['number' => 50, 'block' => 3];
        $this->frontEndOddValue['D'] = ['number' => 49, 'block' => 3];
        $this->rearEndOddValue['D'] = ['number' => 1, 'block' => 3];

        $this->frontEndEvenValue['D'] = ['number' => 50, 'block' => 4];
        $this->rearEndEvenValue['D'] = ['number' => 2, 'block' => 4];
        $this->frontEndOddValue['E'] = ['number' => 1, 'block' => 4];
        $this->rearEndOddValue['E'] = ['number' => 49, 'block' => 4];
    }

    public function getStartLocation()
    {
        return $this->startLocation;
    }

    public function createPickingBatch(PickingBatchRequest $request, $orders, User $user)
    {
        $input = $request->validated();

        $customer = Customer::where('id', $input['customer_id'])->first();
        $quantity = $input['quantity'];

        if ($quantity > count($orders)) {
            $quantity = count($orders);
        }

        $selectedOrders = $this->getSelectedOrders($quantity, $orders);

        $items = $this->getItemsOfSelectedOrders($selectedOrders);

        $shortestPaths = $this->getAllShortestPaths($this->startLocation, $items);

        $reformedPaths = $this->reformPaths($selectedOrders, $shortestPaths);

        $shortestPaths = $reformedPaths['paths'];

        $pickingBatch = $this->store($selectedOrders, $shortestPaths, $customer, $user);

        return $pickingBatch;
    }

    public function processCommandLineOperation($quantity, $orders)
    {
        if (count($orders) == 0) {
            $selectedOrders = [];
            $shortestPaths = [];
            $totalPathDistance = '';

            return compact("selectedOrders", "shortestPaths", "totalPathDistance");
        }

        if ($quantity > count($orders)) {
            $quantity = count($orders);
        }

        $selectedOrders = $this->getSelectedOrders($quantity, $orders);

        $items = $this->getItemsOfSelectedOrders($selectedOrders);

        $shortestPaths = $this->getAllShortestPaths($this->startLocation, $items);

        $reformedPaths = $this->reformPaths($selectedOrders, $shortestPaths);

        $shortestPaths = $reformedPaths['paths'];

        $totalPathDistance = $reformedPaths['totalPathDistance'];

        return compact("selectedOrders", "shortestPaths", "totalPathDistance");
    }

    public function store($selectedOrders, $shortestPaths, Customer $customer, User $user)
    {
        $pickingBatch = PickingBatch::create([
            'customer_id' => $customer->id
        ]);

        foreach ($shortestPaths as $shortestPath) {
            $location = Location::where('name', $shortestPath['nextLocation'])->first();

            foreach ($shortestPath['orderIds'] as $orderId) {
                $orderItem = OrderItem::where('order_id', $orderId)->where('product_id', $shortestPath['productId'])->first();

                PickingBatchItem::create([
                    'picking_batch_id' => $pickingBatch->id,
                    'order_item_id' => $orderItem->id,
                    'location_id' => $location->id,
                    'quantity' => $orderItem->quantity
                ]);
            }
        }

        foreach ($selectedOrders as $order) {
            OrderLock::create([
                'order_id' => $order->id,
                'lock_type' => OrderLock::LOCK_TYPE_PICKING,
                'user_id' => $user->id
            ]);
        }

        return $pickingBatch;
    }

    public function pick(PickRequest $request, PickingBatch $pickingBatch)
    {
        $input = $request->validated();

        $pickingBatchItem = PickingBatchItem::where('id', $input['picking_batch_item_id'])->first();
        $pickingBatchItem->quantity_picked += $input['quantity'];
        $pickingBatchItem->save();

        return $pickingBatchItem->pickingBatch;
    }

    public function getSelectedOrders($quantity, $orders)
    {
        $selectedOrders = $orders->sortByDesc('true_priority')->take($quantity);

        return $selectedOrders;
    }

    public function getItemsOfSelectedOrders($orders)
    {
        $items = collect([]);

        foreach ($orders as $order) {
            $orderItems = $order->orderItems;

            $items = $items->merge($orderItems);
        }

        return $items;
    }

    public function reformPaths($orders, $paths)
    {
        $totalPathDistance = 0;

        foreach ($paths as $key => $path) {
            $orderIds = [];
            foreach ($orders as $order) {
                $item = OrderItem::where('order_id', $order->id)->where('product_id', $path['productId'])->first();

                if ($item) {
                    $orderIds[] = $order->id;
                }
            }

            $orderIdsStr = implode(', ', $orderIds);

            $paths[$key]['orderIds'] = $orderIds;
            $paths[$key]['orderIdsStr'] = $orderIdsStr;

            $totalPathDistance += $path['pathDistance'];
        }

        return compact("paths", "totalPathDistance");
    }

    public function getAllShortestPaths($startLocation, $items)
    {
        $currentLocation = $startLocation;

        foreach ($items as $key => $item) {
            $itemsArr[$item->product_id] = $item->product->location->pluck('name')->toArray();
        }

        while (count($itemsArr) > 0) {

            $shortestPathDetails = $this->getShortestPath($startLocation, $itemsArr);

            $productId = $shortestPathDetails['product_id'];
            $startLocation = $shortestPathDetails['location'];

            $paths[] = [
                'productId' => $productId,
                'currentLocation' => $currentLocation,
                'nextLocation' => $shortestPathDetails['location'],
                'pathDistance' => $shortestPathDetails['minimum']
            ];

            $currentLocation = $startLocation;

            unset($itemsArr[$productId]);
        }

        return $paths;
    }

    private function getShortestPath($startLocation, $items)
    {
        $details['minimum'] = 0;

        $temp = 0;

        foreach ($items as $key => $locations) {
            foreach ($locations as $locKey => $location) {
                $distance = $this->getDistance($startLocation, $location);

                if ($temp == 0 && $locKey == 0) {
                    $details['minimum'] = $distance;
                    $details['product_id'] = $key;
                    $details['location'] = $location;
                }

                if ($distance < $details['minimum']) {
                    $details['minimum'] = $distance;
                    $details['product_id'] = $key;
                    $details['location'] = $location;
                }

                $temp++;
            }
        }

        return $details;
    }

    private function getDistance($location1, $location2)
    {
        $sameAisle = $this->checkSameAisle($location1, $location2);

        if ($sameAisle) {
            $distance =  $this->getDistanceOfSameAisle($location1, $location2);
        } else {
            $distance =  $this->getDistanceOfDifferentAisles($location1, $location2);
        }

        return $distance;
    }

    private function getDistanceOfSameAisle($location1, $location2)
    {
        $distance = 0;

        $number1 =  $this->getNumericPart($location1);

        $number2 =  $this->getNumericPart($location2);

        $greater = $this->getGreater($number1, $number2);

        if ($this->isEven($number1) && $this->isEven($number2)) {
            $distance = abs(($number1 - $number2)) / 2;
        } else if ($this->isOdd($number1) && $this->isOdd($number2)) {
            $distance = abs(($number1 - $number2)) / 2;
        } else if ($this->isEven($greater)) {
            $distance = ceil(abs(($number1 - $number2)) / 2) - 1;
        } else {
            $distance = ceil(abs(($number1 - $number2)) / 2);
        }

        return $distance;
    }

    private function getDistanceOfDifferentAisles($location1, $location2)
    {
        $letter1 = $this->getLetterPart($location1);

        $letter2 = $this->getLetterPart($location2);

        $number1 = $this->getNumericPart($location1);

        $number2 = $this->getNumericPart($location2);

        if ($this->isEven($number1)) {
            $frontEndValue1 = $this->frontEndEvenValue[$letter1]['number'];
            $rearEndValue1 = $this->rearEndEvenValue[$letter1]['number'];
            $block1 = $this->rearEndEvenValue[$letter1]['block'];
        } else {
            $frontEndValue1 = $this->frontEndOddValue[$letter1]['number'];
            $rearEndValue1 = $this->rearEndOddValue[$letter1]['number'];
            $block1 = $this->rearEndOddValue[$letter1]['block'];
        }

        $frontRearDetails1 = $this->getFrontNearDetails($frontEndValue1, $rearEndValue1, $number1);

        if ($this->isEven($number2)) {
            $frontEndValue2 = $this->frontEndEvenValue[$letter2]['number'];
            $rearEndValue2 = $this->rearEndEvenValue[$letter2]['number'];
            $block2 = $this->rearEndEvenValue[$letter2]['block'];
        } else {
            $frontEndValue2 = $this->frontEndOddValue[$letter2]['number'];
            $rearEndValue2 = $this->rearEndOddValue[$letter2]['number'];
            $block2 = $this->rearEndOddValue[$letter2]['block'];
        }

        $frontRearDetails2 = $this->getFrontNearDetails($frontEndValue2, $rearEndValue2, $number2);

        $locationsDistance = $this->getDistanceFromSameEnd($frontRearDetails1, $frontRearDetails2);

        $blockDistance = $this->getBlockDistance($block1, $block2);

        $distance =  $locationsDistance + $blockDistance;

        return $distance;
    }

    private function getFrontNearDetails($frontEndValue, $rearEndValue, $number)
    {
        if (abs($frontEndValue - $number) <= abs($rearEndValue - $number)) {
            $distanceFromNearestEnd = (abs($frontEndValue - $number)) / 2;
            $distanceFromOtherEnd = (abs($rearEndValue - $number)) / 2;

            $nearestEnd = 'Front';
        } else {
            $distanceFromNearestEnd = (abs($rearEndValue - $number)) / 2;
            $distanceFromOtherEnd = (abs($frontEndValue - $number)) / 2;

            $nearestEnd = 'Rear';
        }

        return compact("nearestEnd", "distanceFromNearestEnd", "distanceFromOtherEnd");
    }

    private function getDistanceFromSameEnd($frontRearDetails1, $frontRearDetails2)
    {
        if ($frontRearDetails1['distanceFromNearestEnd'] <= $frontRearDetails2['distanceFromNearestEnd']) {
            $nearsetEnd = $frontRearDetails1['nearestEnd'];
            $nearsetEndDistance = $frontRearDetails1['distanceFromNearestEnd'];

            $otherLocationDistanceFromThisEnd = $frontRearDetails2['nearestEnd'] == $nearsetEnd ? $frontRearDetails2['distanceFromNearestEnd'] : $frontRearDetails2['distanceFromOtherEnd'];

            $totalDistance = $nearsetEndDistance + $otherLocationDistanceFromThisEnd;
        } else {
            $nearsetEnd = $frontRearDetails2['nearestEnd'];
            $nearsetEndDistance = $frontRearDetails2['distanceFromNearestEnd'];

            $otherLocationDistanceFromThisEnd = $frontRearDetails1['nearestEnd'] == $nearsetEnd ? $frontRearDetails1['distanceFromNearestEnd'] : $frontRearDetails1['distanceFromOtherEnd'];

            $totalDistance = $nearsetEndDistance + $otherLocationDistanceFromThisEnd;
        }

        return $totalDistance;
    }

    private function getBlockDistance($block1, $block2)
    {
        if (abs($block1 - $block2) == 0){
            return 1;
        }

        return abs($block1 - $block2) * $this->horizontalBlockDistance;
    }

    private function checkSameAisle($location1, $location2)
    {
        $letter1 = $this->getLetterPart($location1);

        $letter2 = $this->getLetterPart($location2);

        return $letter1 == $letter2;
    }

    private function getNumericPart($location)
    {
        return preg_replace('/[^0-9]/', '', $location);
    }

    private function getLetterPart($location)
    {
        return preg_replace('/[^a-zA-Z]/', '', $location);
    }

    private function isEven($number)
    {
        return $number % 2 == 0;
    }

    private function isOdd($number)
    {
        return $number % 2 != 0;
    }

    private function getGreater($number1, $number2)
    {
        if ($number1 == $number2) {
            return false;
        }

        if ($number1 > $number2) {
            return $number1;
        }

        return $number2;
    }
}
