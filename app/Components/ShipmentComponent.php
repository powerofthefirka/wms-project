<?php

namespace App\Components;

use App\Events\OrderShipped;
use App\Jobs\Webshipper\ProcessShipment;
use App\Http\Requests\Shipment\ShipRequest;
use App\Http\Requests\Shipment\ShipItemRequest;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Models\CustomerShippingCarriersMap;
use App\Models\CustomerShippingMethodsMap;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Shipment;
use App\Models\OrderItem;
use App\Models\Package;
use Illuminate\Support\Arr;
use App\Models\ShipmentItem;

class ShipmentComponent extends BaseComponent
{
    public function shipItem(ShipItemRequest $request, OrderItem $orderItem, Shipment $shipment)
    {
        $input = $request->validated();

        $shipmentItem = ShipmentItem::create([
            'shipment_id' => $shipment->id,
            'order_item_id' => $orderItem->id,
            'quantity' => Arr::get($input, 'quantity')
        ]);

        OrderShipped::dispatch($shipment);

        return $shipmentItem;
    }

    public function ship(ShipRequest $request, Order $order)
    {
        $input = $request->validated();

        //Need to work on it
        /*if (!empty($input['shipping_carrier_id']) && !empty($input['tracking_code'])) {
            $input['tracking_number'] = $input['tracking_code'];
            $input['tracking_url']  = $this->setTrackingUrl($input);
            unset($input['tracking_number']);
        }*/

        $shipment = Shipment::create([
            'order_id' => $order->id,
            'tracking_code' => $input['tracking_code'] ?? null,
            'tracking_url' => $input['tracking_url'] ?? null,
            'number' => $input['number'] ?? null,
            'shipped_at' => $input['shipped_at'] ?? null
        ]);

        if (isset($input['contact_information'][0])) {
            $contactInformationData = $input['contact_information'][0];
        } else {
            $contactInformationData = $order->shippingContactInformation->toArray();
        }

        $contactInformationData['country_id'] = $this->getCountryId($contactInformationData);
        $this->createContactInformation($contactInformationData, $shipment);

        foreach ($input['order_items'] as $record) {
            $shipItemRequest = ShipItemRequest::make($record);

            if (isset($record['order_item_id'])) {
                $orderItem = OrderItem::find($record['order_item_id']);
            } else {
                $orderItem = OrderItem::where('order_id', $input['order_id'])->where('product_id', $record['product_id'])->first();
            }

            $this->shipItem($shipItemRequest, $orderItem, $shipment);
        }

        if (!empty($input['shipping_boxes'])) {
            $packageModel = new Package();
            $packageModel->shipmentPackages($order, $shipment, $input['shipping_boxes']);
        }

        if ($order->customer->webshipperCredential && isset($input['webshipper_carrier_id'])) {
            $carrierId = $input['webshipper_carrier_id'];
            $shippingMethod = $input['webshipper_shipping_method'] ?? null;

            dispatch(new ProcessShipment($shipment, $carrierId, $shippingMethod));
        }

        return $shipment;
    }

    public function carriers(Customer $customer)
    {
        $webshipperCarriers = $this->webshipperCarriers($customer);

        $dummyCarriers = $this->dummyCarriers($customer);
        $dummyCarriers = new Collection($dummyCarriers);

        $carriers = $webshipperCarriers->merge($dummyCarriers);

        return $carriers;
    }

    public function dummyCarriers(Customer $customer)
    {
        $carriers = app()->dummyShipping->carriers($customer);

        return $carriers;
    }

    public function webshipperCarriers(Customer $customer)
    {
        if (!$customer->webshipperCredential) {
            return new Collection([]);
        }

        $carriers = app()->webshipperShipping->carriers($customer);

        if (!$carriers) {
            return new Collection([]);
        }

        $mappedCarriers = CustomerShippingCarriersMap::where('customer_id', $customer->id)->where('shipping_service', 'webshipper')->get();

        foreach ($carriers as $carrier) {
            $mapped = false;
            $mappedName = $carrier['attributes']['alias'];

            foreach ($mappedCarriers as $mappedCarrier) {
                if ($carrier['id'] == $mappedCarrier->shipping_service_carrier_id) {
                    $mapped = true;
                    $mappedName = $mappedCarrier->shippingCarrierText();

                    break;
                }
            }

            $results[] = [
                'id' => $carrier['id'],
                'alias' => $carrier['attributes']['alias'],
                'services' => $carrier['attributes']['services'],
                'mapped' => $mapped,
                'mappedName' => $mappedName
            ];
        }

        return new Collection($results);
    }

    public function carrierServices(Customer $customer, $carrier)
    {
        $services = $this->webshipperCarrierServices($customer, $carrier);

        if ($carrier == '0') {
            $dummyServices = app()->dummyShipping->carriers($customer)[0]['services'];

            $dummyServices = new Collection($dummyServices);
            $services = $services->merge($dummyServices);
        }

        return $services;
    }

    public function webshipperCarrierServices(Customer $customer, $carrier)
    {
        $results = array();

        if (!$customer->webshipperCredential) {
            return new Collection($results);
        }

        $carrier = app()->webshipperShipping->carrier($customer, $carrier);

        if (!$carrier) {
            return new Collection($results);
        }

        $mappedMethods = CustomerShippingMethodsMap::where('customer_id', $customer->id)->where('shipping_service', 'webshipper')->where('shipping_service_carrier_id', $carrier)->get();

        foreach ($carrier['attributes']['services'] as $service) {
            $mapped = false;
            $mappedName = $service['service_name'];

            foreach ($mappedMethods as $mappedMethod) {
                if ($service['service_code'] == $mappedMethod->shipping_service_method_code) {
                    $mapped = true;
                    $mappedName = $mappedMethod->shippingMethodText();

                    break;
                }
            }

            $results[] = [
                'service_code' => $service['service_code'],
                'service_name' => $service['service_name'],
                'mapped' => $mapped,
                'mappedName' => $mappedName
            ];
        }

        return new Collection($results);
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $shipmentCollection = Shipment::query();

        if (!empty($customerIds)) {
            $shipmentCollection = $shipmentCollection->whereHas('order', function ($query) use ($customerIds) {
                $query->whereIn('customer_id', $customerIds);
            });
        }

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {

            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $dates = explode(" ", $filter->value);
                    $from = Arr::get($dates, '0', '');
                    $to = Arr::get($dates, '2', '');

                    $today = Carbon::today()->toDateString();
                    $tomorrow = Carbon::tomorrow()->toDateString();

                    $shipmentCollection = $shipmentCollection->whereBetween('shipments.created_at', [
                        empty($from)
                            ? Carbon::now()->subDays(14)->toDateString() : date($from),
                        empty($to)
                            ? $tomorrow : Carbon::parse($to)->addDay()->toDate()->format('Y-m-d')
                    ]);

                    unset($filters->filterArray[$key]);
                }

                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $shipmentCollection = $shipmentCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $shipmentCollection
                ->where(function ($query) use ($term) {
                    $query->whereHas('order.shippingContactInformation', function ($query) use ($term) {
                        $query->where('email', 'like', $term)
                            ->orWhere('address', 'like', $term)
                            ->orWhere('address2', 'like', $term)
                            ->orWhere('city', 'like', $term)
                            ->orWhere('zip', 'like', $term)
                            ->orWhere('company_name', 'like', $term)
                            ->orWhere('phone', 'like', $term);
                    })
                        ->whereHas('order.shippingContactInformation.country', function ($query) use ($term) {
                            $query->where('code', 'like', $term);
                        })
                        ->orWhereHas('order', function ($query) use ($term) {
                            $query->where('number', 'like', $term);
                        })
                        ->orWhereHas('order', function ($query) use ($term) {
                            $query->where('number', 'like', $term);
                        })
                        ->orWhere('shipments.tracking_code', 'like', $term)
                        ->orWhere('shipments.distinct_items', 'like', $term)
                        ->orWhere('shipments.quantity_shipped', 'like', $term)
                        ->orWhere('shipments.tracking_code', 'like', $term)
                        ->orWhere('shipments.line_item_total', 'like', $term);
                });
        }

        return $shipmentCollection;
    }
}
