<?php

namespace App\Components;

use App\Http\Requests\PricingPlan\StoreRequest;
use App\Http\Requests\PricingPlan\UpdateRequest;
use App\Http\Requests\PricingPlan\DestroyRequest;
use App\Models\PricingPlan;

class PricingPlanComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $pricingPlan = PricingPlan::create($input);

        return $pricingPlan;
    }

    public function update(UpdateRequest $request, PricingPlan $pricingPlan)
    {
        $input = $request->validated();

        $pricingPlan->update($input);

        return $pricingPlan;
    }

    public function destroy(DestroyRequest $request = null, PricingPlan $pricingPlan = null)
    {
        $pricingPlan->delete();

        return ['id' => $pricingPlan->id, 'name' => $pricingPlan->name];
    }
}
