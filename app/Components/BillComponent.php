<?php

namespace App\Components;

use App\Components\BillingFees\ReceivingByHourBillingFeeComponent;
use App\Components\BillingFees\ReceivingByItemBillingFeeComponent;
use App\Components\BillingFees\BillingFeeInterface;
use App\Components\BillingFees\ReceivingByLineBillingFeeComponent;
use App\Components\BillingFees\ReceivingByPoBillingFeeComponent;
use App\Components\BillingFees\RecurringBillingFeeComponent;
use App\Components\BillingFees\ReturnsBillingFeeComponent;
use App\Components\BillingFees\ShipmentsByBoxBillingFeeComponent;
use App\Components\BillingFees\ShipmentsByShippingLabelBillingFeeComponent;
use App\Components\BillingFees\ShipmentsByPickingBillingFeeComponent;
use App\Components\BillingFees\ShipmentsByPickupPickingBillingFeeComponent;
use App\Components\BillingFees\ShippingRateBillingFeeComponent;
use App\Components\BillingFees\StorageByLocationBillingFeeComponent;
use App\Components\BillingFees\StorageByProductBillingFeeComponent;
use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\BillingProfile;
use App\Models\BillItem;
use App\Models\Customer;
use App\Models\InventoryChange;
use App\Models\PurchaseOrder;
use App\Models\Return_;
use App\Models\Shipment;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BillComponent extends BaseComponent
{
    private $feeTypes = [
        RecurringBillingFeeComponent::class,
        ReceivingByPoBillingFeeComponent::class,
        ReceivingByItemBillingFeeComponent::class,
        ReturnsBillingFeeComponent::class,
        ReceivingByLineBillingFeeComponent::class,
        ShipmentsByBoxBillingFeeComponent::class,
        ShipmentsByShippingLabelBillingFeeComponent::class,
        StorageByLocationBillingFeeComponent::class,
        StorageByProductBillingFeeComponent::class,
        ShipmentsByPickingBillingFeeComponent::class,
        ShipmentsByPickupPickingBillingFeeComponent::class,
        ShippingRateBillingFeeComponent::class,
        ReceivingByHourBillingFeeComponent::class
    ];

    public function store($input, Customer $customer)
    {
        $customerId = $customer->id;
        $billingProfileId = $customer->billing_profile_id;

        $periodStart = $input['start_date'];
        $periodEnd = $input['end_date'];
        $dueDate = null;
        $billingProfile = BillingProfile::find($billingProfileId);

        $bill = Bill::create([
            'customer_id' => $customerId,
            'billing_profile_id' => $billingProfileId,
            'period_start' => $periodStart,
            'period_end' => $periodEnd,
            'due_date' => $dueDate,
        ]);

        $this->billByFees($billingProfile, $bill);

        return $bill;
    }

    public function recalculate(Bill $bill) {
        $bill->billItems()->whereHas('billingFee', function ($query) {
            $query->where('type', '!=', 'ad_hoc');
        })->delete();

        $customer = Customer::find($bill->customer_id);
        $billingProfile = BillingProfile::find($customer->billing_profile_id);

        $this->billByFees($billingProfile, $bill);

        return $bill;
    }

    public function billByFees($billingProfile, $bill)
    {
        $billingFees = $billingProfile->billingFees()->orderBy('settings->if_no_other_fee_applies', 'asc')->get();

        foreach ($billingFees as $fee) {
            foreach ($this->feeTypes as $feeType) {
                /**
                 * @var $instance BillingFeeInterface
                 */
                $instance = new $feeType;

                $instance->calculate($fee, $bill);
            }
        }

        $bill->calculated_at = Carbon::now()->toDateTimeString();
        $bill->amount = $bill->billItems->sum('total_price');
        $bill->save();
    }

    public function createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd)
    {
        $rate = Arr::get($settings, 'rate', 1);

        return BillItem::create([
            'bill_id' => $bill->id,
            'billing_fee_id' => $fee->id,
            'description' => $description,
            'quantity' => $quantity,
            'unit_price' => $rate,
            'total_price' => $quantity * $rate,
            'period_end' => $periodEnd,

            'purchase_order_item_id' => $settings['purchase_order_item_id'] ?? null,
            'purchase_order_id' => $settings['purchase_order_id'] ?? null,
            'return_item_id' => $settings['return_item_id'] ?? null,
            'package_id' => $settings['package_id'] ?? null,
            'package_item_id' => $settings['package_item_id'] ?? null,
            'shipment_id' => $settings['shipment_id'] ?? null,
            'location_type_id' => $settings['location_type_id'] ?? null,
            'inventory_change_id' => $settings['inventory_change_id'] ?? null,
        ]);
    }

    public function checkPeriodConsistency($start, $customerId, $billingProfileId)
    {
        $pass = true;
        $latestBill = Bill::where('billing_profile_id', $billingProfileId)->where('customer_id', $customerId)->orderBy('period_end', 'desc')->first();

        if ($latestBill) {
            $pass = Carbon::parse($start)->eq(Carbon::parse($latestBill['period_end']));
        }

        return $pass;
    }

    public function adHoc (Request $request, Bill $bill)
    {
        $input = $request->all();
        $fee = BillingFee::find($input['billing_fee_id']);
        $settings = $fee->settings;
        $quantity = $input['quantity'];
        $periodEnd = $input['period_end'];
        $description = $fee->name;

        $this->createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd);

        $bill->amount = $bill->billItems->sum('total_price');
        $bill->calculated_at = Carbon::now()->toDateTimeString();
        $bill->save();
    }

    public function exportToCsv(Bill $bill)
    {
        $fileName = $bill->customer->contactInformation->name . ' ' . localized_date($bill->period_start) . ' - ' . localized_date($bill->period_end)  . '.csv';
        $billItems = $bill->billItems;

        $shippingCarriers = ShippingCarrier::pluck('name', 'id');
        $shippingMethods = ShippingMethod::pluck('name', 'id');

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $columns = [
            'Type (charge/bill item)',
            'Name (charge/bill item)',
            'Description (charge/bill item)',
            'Quantity (charge/bill item)',
            'Unit Price (charge/bill item)',
            'Total Price (charge/bill item)',
            'Item Period end (charge/bill item)',

            'SKU (Product)',
            'Name (Product)',
            'Weight (Product)',
            'Height (Product)',
            'Length (Product)',
            'Width (Product)',

            'Reason (Return)',
            'Quantity received (Return)',
            'Shipping carrier title (Return)',
            'Shipping method title (Return)',
            'Shipping address (Return)',

            'Previous on hand (inventory change)',
            'Change in on hand (inventory change)',

            'Shipment',

            'Customer',
            'Bill id',
            'Bill Period Start',
            'Bill Period End',

            'Client Name',
            'Client Order Reference',
            'Delivery Name',
            'Delivery Address',
            'Country',
            'Carrier',
            'Service',
            'Total weight ('. ($bill->customer->weight_unit ??  env('DEFAULT_WEIGHT_UNIT')) . ')',
            'Tracking Number',
            'Number of units in shipment',
            'Date Dispatched'
        ];

        $callback = function() use($billItems, $columns, $shippingCarriers, $shippingMethods) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($billItems as $item) {
                $product  = $item->purchaseOrderItem
                        ?? $item->returnItem->orderItem
                        ?? $item->packageItem->orderItem
                        ?? $item->inventoryChange->product
                        ?? null;

                $shipment = $this->getBillItemShipment($item);

                $order = $this->getBillItemOrder($item);

                fputcsv($file, [
                    $item->billingFee->type ?? '',
                    $item->billingFee->name ?? '',
                    $item->description ?? '',
                    $item->quantity ?? '',
                    $item->unit_price ?? '',
                    $item->total_price ?? '',
                    $item->period_end ?? '',

                    $product->sku ?? '',
                    $product->name ?? '',
                    $product->weight ?? '',
                    $product->height ?? '',
                    $product->length ?? '',
                    $product->width ?? '',

                    $item->returnItem->return_->reason ?? '',
                    $item->returnItem->quantity_received ?? '',
                    $item->returnItem->return_->shipping_carrier_title ?? '',
                    $item->returnItem->return_->shipping_method_title ?? '',
                    $item->returnItem->return_->order->shippingContactInformation->address ?? '',

                    $item->inventoryChange->previous_on_hand ?? '',
                    $item->inventoryChange->change_in_on_hand ?? '',

                    $shipment->number ?? '',

                    $item->bill->customer->contactInformation->name ?? '',
                    $item->bill->id ?? '',
                    $item->bill->period_start ?? '',
                    $item->bill->period_end ?? '',

                    $item->bill->customer->contactInformation->name ?? '',
                    $order ? ($order instanceof PurchaseOrder ? '[PO] ' . $order->number : $order->number) : '',
                    $shipment ? $shipment->contactInformation->name : '',
                    $shipment ? $shipment->contactInformation->address : '',
                    $shipment ? $shipment->contactInformation->country->title : '',
                    array_key_exists('shipping_carrier_id', $item->billingFee->settings) ? $shippingCarriers[$item->billingFee->settings['shipping_carrier_id']] : '',
                    array_key_exists('shipping_method', $item->billingFee->settings) || array_key_exists('shipping_method_id', $item->billingFee->settings)
                        ? $this->getShippingMethods($item, $shippingMethods)
                        : '',
                    $shipment ? $shipment->getTotalShipmentWeight() : '',
                    $shipment->tracking_code ?? '',
                    $shipment ? $shipment->shipmentItems->sum('quantity') : '',
                    $shipment ? $shipment->shipped_at->format(get_date_format()) : ''
                    ]
                );
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    private function getBillItemShipment($item)
    {
        switch (!null) {
            case $item->shipment:
                return $item->shipment;
            case $item->packageItem:
                return $item->packageItem->package->shipment;
            case $item->package:
                return $item->package->shipment;
            default:
                return null;
        }
    }

    private function getBillItemOrder($item)
    {
        switch (!null) {
            case $item->shipment:
                return $item->shipment->order;
            case $item->packageItem:
                return $item->packageItem->package->shipment->order;
            case $item->package:
                return $item->package->shipment->order;
            case $item->returnItem:
                return $item->returnItem->return_->order;
            case $item->purchaseOrder:
                return $item->purchaseOrder;
            case $item->purchaseOrderItem:
                return $item->purchaseOrderItem->purchaseOrder;
            default:
                return null;
        }
    }

    private function getShippingMethods($item, $shippingMethods)
    {
        $methods = '';

        if (array_key_exists('shipping_method', $item->billingFee->settings)) {
            foreach ($item->billingFee->settings['shipping_method'] as $index => $shippingMethodId) {
                $methods .= $shippingMethods[$shippingMethodId];

                if ($index + 1 != count($item->billingFee->settings['shipping_method'])) {
                    $methods .= ', ';
                }
            }
        } else {
            $methods = $shippingMethods[$item->billingFee->settings['shipping_method_id']];
        }


        return $methods;
    }

    public function destroy(Bill $bill)
    {
        $bill->delete();

        return $bill;
    }

    public static function unBilledShipments(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $shipmentIds = $bill->billItems()->whereNotNull('shipment_id')->groupBy(['shipment_id'])->get()->pluck('shipment_id')->toArray();

        $shipments = Shipment::
            whereHas('order', function ($query) use ($customerId) {
                $query->where('customer_id', $customerId);
            })
            ->whereBetween('shipped_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->whereNotIn('id', $shipmentIds);

        $count = $shipments->count();

        return [
            'data' => $shipments->skip($request->get('start'))->limit($request->get('length'))->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledPurchaseOrders(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $poIds = $bill->billItems()->whereNotNull('purchase_order_id')->groupBy(['purchase_order_id'])->get()->pluck('purchase_order_id')->toArray();

        $po = PurchaseOrder::where('customer_id', $customerId)
            ->where('status', User::STATUS_CLOSED)
            ->whereBetween('delivered_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->whereNotIn('id', $poIds);

        $count = $po->count();

        return [
            'data' => $po->skip($request->get('start'))->limit($request->get('length'))->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            ];
    }

    public static function unBilledPurchaseOrderItems(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $purchaseOrderItemIds = $bill->billItems()->whereNotNull('purchase_order_item_id')->groupBy(['purchase_order_item_id'])->get()->pluck('purchase_order_item_id')->toArray();

        $poItems = PurchaseOrder::where('customer_id', $customerId)
            ->where('status', User::STATUS_CLOSED)
            ->whereBetween('delivered_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->join('purchase_order_items', 'purchase_order_items.purchase_order_id','=', 'purchase_orders.id')
            ->whereNotIn('purchase_order_items.id', $purchaseOrderItemIds);

        $count = $poItems->count();

        return [
            'data' => $poItems
                ->skip($request->get('start'))
                ->limit($request->get('length'))
                ->select('purchase_orders.*', 'purchase_order_items.name as po_item_name')
                ->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledLocations(Request $request, Bill $bill)
    {
        $locationTypeIds = $bill->billItems()->whereNotNull('location_type_id')->groupBy(['location_type_id'])->get()->pluck('location_type_id')->toArray();

        $locations = $bill->customer
            ->warehouses()
            ->join('locations', 'locations.warehouse_id', '=', 'warehouses.id')
            ->whereNotIn('locations.location_type_id', $locationTypeIds);

        $count = $locations->count();

        return [
            'data' => $locations->skip($request->get('start'))->limit($request->get('length'))->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledReturnItems(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $returnedItemIds = $bill->billItems()->whereNotNull('return_item_id')->groupBy(['return_item_id'])->get()->pluck('return_item_id')->toArray();

        $returns = Return_::whereHas('order', function ($query) use ($customerId) {
                $query->where('customer_id', $customerId);
            })
                ->whereBetween('requested_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
                ->where('status', User::STATUS_COMPLETE)
                ->join('return_items', 'return_items.return_id', '=', 'returns.id')
                ->join('order_items', 'order_items.id', '=', 'return_items.order_item_id')
                ->whereNotIn('return_items.id', $returnedItemIds);

        $count = $returns->count();

        return [
            'data' => $returns->skip($request->get('start'))->limit($request->get('length'))
                ->select('returns.*', 'order_items.name as return_item_name')
                ->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledPackages(Request  $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $packageIds = $bill->billItems()->whereNotNull('package_id')->groupBy(['package_id'])->get()->pluck('package_id')->toArray();

        $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('shipped_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->join('packages', 'packages.shipment_id', '=', 'shipments.id')
            ->whereNotIn('packages.id', $packageIds);

        $count = $shipments->count();

        return [
            'data' => $shipments->skip($request->get('start'))->limit($request->get('length'))
                ->select('shipments.*', 'packages.tracking_number as tracking_number')
                ->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledPackageItems(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;

        $packageItemIds = $bill->billItems()->whereNotNull('package_item_id')->groupBy(['package_item_id'])->get()->pluck('package_item_id')->toArray();

        $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('shipped_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->join('packages', 'packages.shipment_id', '=', 'shipments.id')
            ->join('package_items', 'package_items.package_id', '=', 'packages.id')
            ->join('order_items', 'order_items.id', '=', 'package_items.order_item_id')
            ->whereNotIn('package_items.id', $packageItemIds);

        $count = $shipments->count();

        return [
            'data' => $shipments->skip($request->get('start'))->limit($request->get('length'))
                ->select('shipments.*', 'order_items.sku as order_item_sku')
                ->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }

    public static function unBilledInventoryChanges(Request $request, Bill $bill)
    {
        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);

        $inventoryChangeIds = $bill->billItems()->whereNotNull('inventory_change_id')->groupBy(['inventory_change_id'])->get()->pluck('inventory_change_id')->toArray();
        $customerProductsIds = $bill->customer->products->pluck('id');

        $changes = InventoryChange::whereIn('inventory_changes.product_id', $customerProductsIds)
            ->whereNotIn('id',$inventoryChangeIds)
            ->leftJoin('locations', 'locations.id', '=', 'inventory_changes.location_id')
            ->leftJoin('location_types', 'location_types.id', '=', 'locations.location_type_id')
            ->leftJoin('products', 'products.id', '=', 'inventory_changes.product_id')
            ->whereBetween('inventory_changes.changed_at', [$periodStartDate->format('Y-m-d'), $periodEndDate->format('Y-m-d')])
            ->join(DB::raw("(select
            inventory_changes.*,
            (inventory_changes.previous_on_hand + inventory_changes.change_in_on_hand) as days_sum
            from inventory_changes,
            (select *, max(id) as max_id from inventory_changes group by DATE(changed_at), product_id) max_sales
            where inventory_changes.id = max_sales.max_id) filtered_changes ON inventory_changes.id = filtered_changes.id"), function(){})
            ->select(
                'filtered_changes.*',
                'products.sku as sku',
                'locations.name as location_name',
                DB::raw("(products.width*products.height*products.length) as product_volume"),
                DB::raw("Date(filtered_changes.changed_at) as date_filtered")
            );

        $count = $changes->count();

        return [
            'data' => $changes->skip($request->get('start'))->limit($request->get('length'))
                ->select('inventory_changes.*', 'products.sku as product_sku')
                ->get(),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
        ];
    }
}
