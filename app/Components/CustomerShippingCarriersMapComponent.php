<?php

namespace App\Components;

use App\Http\Requests\CustomerShippingCarriersMap\StoreBatchRequest;
use App\Http\Requests\CustomerShippingCarriersMap\UpdateBatchRequest;
use App\Http\Requests\CustomerShippingCarriersMap\DestroyBatchRequest;
use App\Http\Requests\CustomerShippingCarriersMap\StoreRequest;
use App\Http\Requests\CustomerShippingCarriersMap\UpdateRequest;
use App\Http\Requests\CustomerShippingCarriersMap\DestroyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Models\CustomerShippingCarriersMap;
use App\Models\Customer;

class CustomerShippingCarriersMapComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $input['shipping_carrier_pattern'] = $this->generateShippingCarrierPattern($input);

        $shippingCarriersMap = CustomerShippingCarriersMap::create($input);

        return $shippingCarriersMap;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, CustomerShippingCarriersMap $customerShippingCarriersMap)
    {
        $input = $request->validated();

        $input['shipping_carrier_pattern'] = $this->generateShippingCarrierPattern($input);

        $customerShippingCarriersMap->update($input);

        return $customerShippingCarriersMap;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $customerShippingCarriersMap = CustomerShippingCarriersMap::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $customerShippingCarriersMap));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, CustomerShippingCarriersMap $customerShippingCarriersMap)
    {
        $customerShippingCarriersMap->delete();

        $response = ['id' => $customerShippingCarriersMap->id, 'customer_id' => $customerShippingCarriersMap->customer_id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $customerShippingCarriersMap = CustomerShippingCarriersMap::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $customerShippingCarriersMap));
        }

        return $responseCollection;
    }

    public function generateShippingCarrierPattern($input)
    {
        $pattern = $input['customer_id'] . ', ';
        $pattern .= $input['shipping_carrier_text'] . ', ';
        $pattern .= $input['shipping_service'] . ', ';
        $pattern .= $input['shipping_service_carrier_id'];

        return $pattern;
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%' )
                        ->orWhere('company_name', 'like','%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%' )
                        ->orWhere('zip', 'like', '%' . $term . '%' )
                        ->orWhere('city', 'like', '%' . $term . '%' )
                        ->orWhere('phone', 'like', '%' . $term . '%' );
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function mappedCarriers(Customer $customer, $service)
    {
        $mappedCarriers = CustomerShippingCarriersMap::where('customer_id', $customer->id)->where('shipping_service', $service)->get();

        $results = array();

        foreach ($mappedCarriers as $key => $carrier) {
            $results[] = [
                'id' => $carrier['id'],
                'shipping_service_carrier_id' => $carrier->shipping_service_carrier_id,
                'carrier_text' => $carrier->shippingCarrierText()
            ];
        }

        return new Collection($results);
    }
}