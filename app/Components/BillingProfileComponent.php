<?php

namespace App\Components;

use App\Http\Requests\BillingProfile\StoreRequest;
use App\Http\Requests\BillingProfile\StoreBatchRequest;
use App\Http\Requests\BillingProfile\UpdateRequest;
use App\Http\Requests\BillingProfile\UpdateBatchRequest;
use App\Http\Requests\BillingProfile\DestroyRequest;
use App\Http\Requests\BillingProfile\DestroyBatchRequest;
use App\Models\BillingProfile;
use Illuminate\Support\Collection;


class BillingProfileComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $billingProfile = BillingProfile::create($input);

        return $billingProfile;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, BillingProfile $billingProfile)
    {
        $input = $request->validated();

        $billingProfile->update($input);

        return $billingProfile;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $billingProfile = BillingProfile::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $billingProfile, false));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, BillingProfile $billingProfile = null)
    {
        $billingProfile->delete();

        return ['id' => $billingProfile->id, 'name' => $billingProfile->name];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $billingProfile = BillingProfile::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $billingProfile));
        }

        return $responseCollection;
    }
}
