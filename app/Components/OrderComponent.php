<?php

namespace App\Components;

use App\Http\Requests\Product\StoreRequest as ProductStoreRequest;
use App\Http\Requests\Order\DestroyBatchRequest;
use App\Http\Requests\Order\DestroyRequest;
use App\Http\Requests\Order\StoreBatchRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateBatchRequest;
use App\Http\Requests\Order\UpdateRequest;
use App\Http\Requests\Order\FilterRequest;
use App\Http\Requests\Order\OrderReshipRequest;
use App\Http\Resources\OrderCollection;
use App\Models\Customer;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\OrderResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Webhook;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use App\Models\ObjectChange;
use Illuminate\Support\Facades\DB;

class OrderComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();
        $input['shop_name'] = Arr::get($input, 'shop_name', env('MANUAL_ORDER_SHOP_NAME'));

        $shippingInformationData = Arr::get($input, 'shipping_contact_information');

        if ($this->getCountryId($shippingInformationData)) {
            $shippingInformationData['country_id'] = $this->getCountryId($shippingInformationData);
        }

        if ($request->get('differentBillingInformation') === 'on') {
            $billingInformationData = Arr::get($input, 'billing_contact_information');
        } else {
            $billingInformationData = Arr::get($input, 'shipping_contact_information');
        }

        if ($this->getCountryId($billingInformationData)) {
            $billingInformationData['country_id'] = $this->getCountryId($billingInformationData);
        }

        Arr::forget($input, 'shipping_contact_information');
        Arr::forget($input, 'billing_contact_information');

        $orderArr = Arr::except($input, ['order_items']);

        if (!isset($orderArr['shipping_carrier_id']) && isset($orderArr['shipping_carrier_wms_id'])) {
            $orderArr['shipping_carrier_id'] = $this->getShippingCarrierId($orderArr['customer_id'], $orderArr['shipping_carrier_wms_id']);
            Arr::forget($orderArr, 'shipping_carrier_wms_id');
        }

        if (!isset($orderArr['shipping_method_id']) && isset($orderArr['shipping_carrier_id']) && isset($orderArr['shipping_method_wms_id'])) {
            $orderArr['shipping_method_id'] = $this->getShippingMethodId($orderArr['shipping_carrier_id'], $orderArr['shipping_method_wms_id'], $orderArr['shipping_method_title'] ?? null);
            Arr::forget($orderArr, 'shipping_method_wms_id');
        }

        if (!empty($input['order_items'])) {
            $orderModel = new Order();
            $orderArr = $orderModel->updateOrder($input);
        }

        $orderArr = $this->setShippingCarrierAndMethodTitles($orderArr);

        $order = Order::create($orderArr);

        $shippingContactInformation = $this->createContactInformation($shippingInformationData, $order);
        $billingContactInformation = $this->createContactInformation($billingInformationData, $order);

        $order->shipping_contact_information_id = $shippingContactInformation->id;
        $order->billing_contact_information_id = $billingContactInformation->id;
        $order->save();

        $this->updateOrderItems($order, $input['order_items'] ?? []);

        $order->refresh();

        if ($fireWebhook == true) {
            $this->webhook(new OrderResource($order), Order::class, Webhook::OPERATION_TYPE_STORE, $order->customer_id);
        }

        $order->getMapCoordinates();

        return $order;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, Order::class, OrderCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Order $order, $fireWebhook = true)
    {
        $input = $request->validated();
        $shippingContactInformationArr = Arr::get($input, 'shipping_contact_information');

        if (isset($shippingContactInformationArr)) {
            if ($this->getCountryId($shippingContactInformationArr)) {
                $shippingContactInformationArr['country_id'] = $this->getCountryId($shippingContactInformationArr);
            }

            $order->shippingContactInformation->update(array_filter($shippingContactInformationArr));
            Arr::forget($input, 'shipping_contact_information');
        }

        $billingContactInformationArr = Arr::get($input, 'billing_contact_information');

        if (isset($billingContactInformationArr)) {
            if ($this->getCountryId($billingContactInformationArr)) {
                $billingContactInformationArr['country_id'] = $this->getCountryId($billingContactInformationArr);
            }

            $order->billingContactInformation->update(array_filter($billingContactInformationArr));
            Arr::forget($input, 'billing_contact_information');
        }

        if (isset($input['order_items'])) {
            $this->updateOrderItems($order, Arr::get($input, 'order_items'));
            $input['order_items'] = $order->orderItems->toArray();
        }

        if (!isset($input['shipping_carrier_id']) && isset($input['shipping_carrier_wms_id'])) {
            $input['shipping_carrier_id'] = $this->getShippingCarrierId($input['customer_id'], $input['shipping_carrier_wms_id']);
            Arr::forget($input, 'shipping_carrier_wms_id');
        }

        if (!isset($input['shipping_method_id']) && isset($input['shipping_carrier_id']) && isset($input['shipping_method_wms_id'])) {
            $input['shipping_method_id'] = $this->getShippingMethodId($input['shipping_carrier_id'], $input['shipping_method_wms_id'], $input['shipping_method_title'] ?? null);
            Arr::forget($input, 'shipping_method_wms_id');
        }

        if (isset($input['status']) && $input['status'] == 'priority') {
            $input['priority'] = true;
        }

        if (!empty($input['order_items'])) {
            $input = $order->updateOrder($input);
        }

        $input = $this->setShippingCarrierAndMethodTitles($input);

        Arr::forget($input, 'order_items');
        Arr::forget($input, 'customer_id');

        $order->update($input);

        $order->refresh();

        if ($fireWebhook == true) {
            $this->webhook(new OrderResource($order), Order::class, Webhook::OPERATION_TYPE_UPDATE, $order->customer_id);
        }

        $order->getMapCoordinates();

        return $order;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $outsideRequest = false;

        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;
            $record['shop_name'] = Arr::get($record, 'shop_name', env('MANUAL_ORDER_SHOP_NAME'));

            if (isset($record['id']) && $order = Order::where('id', $record['id'])->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $order, false));
            } elseif (isset($record['number']) && $order = Order::where('number', $record['number'])->where('customer_id', Arr::get($record, 'customer_id'))->where('shop_name', $record['shop_name'])->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $order, false));
            } else {
                $storeRequest = StoreRequest::make($record);
                $responseCollection->add($this->store($storeRequest, false));
            }
        }

        if (!$outsideRequest) {
            $this->batchWebhook($responseCollection, Order::class, OrderCollection::class, Webhook::OPERATION_TYPE_UPDATE);
        }

        $responseCollection->transform(function (Order $order, $key) use ($input) {
            if (isset($input[$key]['custom_id'])) {
                return (new OrderResource($order))->setCustomId($input[$key]['custom_id']);
            }

            return (new OrderResource($order));
        });

        return $responseCollection;
    }

    public function updateOrderItems(Order $order, $orderItems)
    {
        foreach ($orderItems as $key => $item) {
            if (!isset($item['order_item_id']) && empty($item['product_id']) && $orderLine = $order->orderItems->where('sku', Arr::get($item, 'sku'))->first()) {
                $item = $this->getProductInfo($item);
                $item['quantity_pending'] = Arr::get($item, 'quantity_pending', ($orderLine->quantity_pending + ($item['quantity'] - $orderLine->quantity)));

                $orderLine->update($item);
            } else if (!isset($item['order_item_id']) && empty($item['product_id'])) {
                $item['order_id'] = $order->id;
                $item['product_id'] = null;

                OrderItem::create($item);
            } else if (!isset($item['order_item_id']) && $orderLine = $order->orderItems->where('product_id', $item['product_id'])->first()) {
                $item = $this->getProductInfo($item);
                $item['quantity_pending'] = Arr::get($item, 'quantity_pending', ($orderLine->quantity_pending + ($item['quantity'] - $orderLine->quantity)));

                $orderLine->update($item);
            } else if (!isset($item['order_item_id']) && isset($item['product_id'])) {
                $item['order_id'] = $order->id;
                $item = $this->getProductInfo($item);
                $item['quantity_pending'] = Arr::get($item, 'quantity_pending', $item['quantity']);

                OrderItem::create($item);
            } else if (isset($item['order_item_id']) && $item['quantity'] == 0) {
                $orderLine = OrderItem::find($item['order_item_id']);
                $orderLine->quantity = 0;
                $orderLine->quantity_pending = 0;
                $orderLine->update();
            } else if (isset($item['order_item_id'])) {
                $orderLine = OrderItem::find($item['order_item_id']);
                $item['quantity_pending'] = Arr::get($item, 'quantity_pending', ($orderLine->quantity_pending + ($item['quantity'] - $orderLine->quantity)));
                $item = $this->getProductInfo($item);

                $orderLine->update(Arr::except($item, ['order_item_id']));
            }
        }
    }

    public function getProductInfo($item)
    {
        if (empty($item['product_id']) || $item['product_id'] == 0) {
            $item['product_id'] = null;

            return $item;
        }

        if (!empty($item['product_id']) && ($product = Product::find($item['product_id']))) {
            $item['sku'] = $item['sku'] ?? $product->sku;
            $item['name'] = $item['name'] ?? $product->name;
            $item['price'] = $item['price'] ?? $product->price;
            $item['customs_price'] = $item['customs_price'] ?? $product->customs_price ?? 0;
            $item['weight'] = $product->weight;
            $item['width'] = $product->width;
            $item['height'] = $product->height;
            $item['length'] = $product->length;
            $item['country_of_origin'] = $product->country_of_origin;
            $item['barcode'] = $item['barcode'] ?? $product->barcode;
            $item['replacement_value'] = $product->replacement_value;
            $item['product_type'] = $product->product_type;
            $item['hs_code'] = $product->hs_code;
        }

        return $item;
    }

    public function destroy(DestroyRequest $request, Order $order, $fireWebhook = true)
    {
        $order->orderItems()->delete();

        $order->delete();

        $response = ['id' => $order->id, 'number' => $order->number, 'customer_id' => $order->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, Order::class, Webhook::OPERATION_TYPE_DESTROY, $order->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $order = Order::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $order, false));
        }

        $this->batchWebhook($responseCollection, Order::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function filterCustomers(Request $request)
    {
        $customerIds = Auth()->user()->customerIds();
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if (!$contactInformation->count()) {
                $contactInformation = Customer::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->where('active', 1)->whereIn('customers.id', $customerIds)->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterProducts(Request $request, Customer $customer)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $products = $customer->products();

            $term = '%' . $term . '%';

            $products = $products->where(function ($query) use ($term) {
                $query->where('sku', 'like', $term)
                    ->orWhere('name', 'like', $term);
            })
                ->get();

            foreach ($products as $product) {
                $image = $defaultImage =  '/images/product-placeholder.png';

                if (!empty($product->productImages->first())) {
                    $image = $product->productImages->first()->source;
                }

                $results[] = [
                    'id' => $product->id,
                    'text' => $product->name,
                    'html' => '<div class="search-item d-flex align-items-center"><img class="avatar rounded" src="' . $image . '" onerror="this.src=\'' . $defaultImage . '\'"><span class="m-0 ml-2 text-default">Name: ' . $product->name . '<br>Sku: ' . $product->sku . '</span></div>',
                    'image' => $image,
                    'name' => $product->name,
                    'sku' => $product->sku,
                    'price' => $product->price
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filter(FilterRequest $request, $customerIds)
    {
        $query = Order::query();

        $query->when($request['from_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '>=', $request['from_date_created']);
        });

        $query->when($request['to_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '<=', $request['to_date_created'] . ' 23:59:59');
        });

        $query->when($request['from_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '>=', $request['from_date_updated']);
        });

        $query->when($request['to_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '<=', $request['to_date_updated'] . ' 23:59:59');
        });

        $query->when(count($customerIds) > 0, function ($q) use ($customerIds) {
            return $q->whereIn('customer_id', $customerIds);
        });

        $orders = $query->paginate();

        return $orders;
    }

    public function search($term, $setDefaultDate = true)
    {
        $customerIds = Auth()->user()->customerIds();

        $orderCollection = Order::whereIn('orders.customer_id', $customerIds);

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $dates = explode(" ", $filter->value);
                    $from = Arr::get($dates, '0', '');
                    $to = Arr::get($dates, '2', '');

                    if (!$setDefaultDate && empty($from)) {
                        continue;
                    }

                    $today = Carbon::today()->toDateString();
                    $tomorrow = Carbon::tomorrow()->toDateString();

                    $orderCollection = $orderCollection->whereBetween('orders.ordered_at', [
                        empty($from)
                            ? Carbon::now()->subDays(14)->toDateString() : date($from),
                        empty($to)
                            ? $tomorrow : Carbon::parse($to)->addDay()->toDate()->format('Y-m-d')
                    ]);

                    unset($filters->filterArray[$key]);
                }

                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ?? null;
                    unset($filters->filterArray[$key]);
                }
            }

            $orderCollection = $orderCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $orderCollection
                ->where(function ($query) use ($term) {
                    $query
                        ->whereHas('shippingContactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term)
                                ->orWhere('address', 'like', $term)
                                ->orWhere('zip', 'like', $term)
                                ->orWhere('email', 'like', $term)
                                ->orWhere('phone', 'like', $term)
                                ->orWhere('city', 'like', $term);
                        })
                        ->orWhereHas('customer.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhereHas('shippingContactInformation.country', function ($query) use ($term) {
                            $query->where('code', 'like', $term);
                        })
                        ->orWhereHas('shippingMethod', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhere('orders.status', str_replace('%', '', $term))
                        ->orWhereRaw('REPLACE(orders.status, "_", " ") = "' . str_replace(['%', '_'], ['', ' '], $term) . '"')
                        ->orWhere('item_count', 'like', $term)
                        ->orWhere('orders.total', 'like', $term)
                        ->orWhere('number', 'like', $term);
                });
        }

        return $orderCollection;
    }

    public function cancel(Order $order)
    {
        $order->cancel();

        $this->webhook(new OrderResource($order), Order::class, Webhook::OPERATION_TYPE_UPDATE, $order->customer_id);

        return $order;
    }

    public function reship(OrderReshipRequest $request, Order $order)
    {
        $input = $request->validated();

        if (isset($input['order_items'])) {
            $order->reshipItems(Arr::get($input, 'order_items'));
        }

        $order->calculateOrder();
        $order->refresh();

        $this->webhook(new OrderResource($order), Order::class, Webhook::OPERATION_TYPE_UPDATE, $order->customer_id);

        return $order;
    }

    public function searchChangeLog($term, $orderId)
    {
        if ($orderId) {
            $changeCollection = ObjectChange::where(function ($q) use ($orderId) {
                $q->where('object_type', Order::class)->where('object_id', $orderId);
            })->orWhere(function ($q) use($orderId) {
                $q->where('parent_object_type', Order::class)->where('parent_object_id', $orderId);
            });
        } else {
            $changeCollection = ObjectChange::where(function ($q) {
                $q->whereIn('object_type', [Order::class, OrderItem::class])->orWhere('parent_object_type', Order::class);
            });
        }

        return $this->filteredChangeLog($term, $changeCollection);
    }
}
