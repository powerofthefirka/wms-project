<?php

namespace App\Components;

use App\Models\Country;
use App\Models\Return_;
use App\Models\Webhook;
use App\Models\Customer;
use App\Models\Warehouse;
use App\Models\LocationType;
use App\Models\ShippingMethod;
use App\Models\ShippingCarrier;
use App\Models\ContactInformation;
use Illuminate\Support\Collection;
use App\Events\Webhook\SendWebhook;
use \Venturecraft\Revisionable\Revision;
use Illuminate\Support\Arr;
use Carbon\Carbon;

class BaseComponent
{
    protected function batchWebhook($collections, $objectType, $resourceCollection, $operation)
    {
        $customerWiseItems = [];

        foreach ($collections as $key => $item) {
            $customerId = $this->getCustomerId($item, $objectType, $operation);

            $customerWiseItems[$customerId][] = $item;
        }

        foreach ($customerWiseItems as $customerId => $items) {
            $collections = new Collection($items);

            $this->webhook(new $resourceCollection($collections), $objectType, $operation, $customerId);
        }
    }

    protected function webhook($response, $objectType, $operation, $customerId)
    {
        $customer = Customer::find($customerId);

        event(new SendWebhook($response, $customer, $objectType, $operation));
    }

    protected function getCustomerId($item, $objectType, $operation)
    {
        if ($operation == Webhook::OPERATION_TYPE_DESTROY) {
            return $item['customer_id'];
        }

        if (Location::class == $objectType) {
            $customerId = $item->warehouse->customer_id;
        } elseif (Return_::class == $objectType) {
            $customerId = $item->order->customer_id;
        } elseif (ShippingMethod::class == $objectType) {
            $customerId = $item->shippingCarrier->customer_id;
        } else {
            $customerId = $item->customer_id;
        }

        return $customerId;
    }

    public function history($object)
    {
        $revisionable_type = get_class($object);
        $revisionable_id = $object->id;

        $revisions = Revision::where('revisionable_type', $revisionable_type)->where('revisionable_id', $revisionable_id)->get();

        return $revisions;
    }

    public function createContactInformation($data, $object)
    {
        $contact = new ContactInformation();
        $contact->name = $data['name'] ?? '';
        $contact->company_name = $data['company_name'] ?? null;
        $contact->address = $data['address'] ?? '';
        $contact->address2 = $data['address2'] ?? null;
        $contact->zip = $data['zip'] ?? '';
        $contact->country_id = $data['country_id'] ?? null;
        $contact->city = $data['city'] ?? '';
        $contact->email = $data['email'] ?? '';
        $contact->phone = $data['phone'] ?? '';
        $contact->shared_folder = $data['shared_folder'] ?? '';
        $contact->object()->associate($object);
        $contact->save();

        return $contact;
    }

    public function getShippingCarrierId($customerId, $shippingCarrierWMSId)
    {
        $shippingCarrier = ShippingCarrier::where('3pl_id', Customer::find($customerId)->threePl->id)->where('wms_id', $shippingCarrierWMSId)->first();

        if (!$shippingCarrier) {
            $shippingCarrier = ShippingCarrier::create(['3pl_id' => Customer::find($customerId)->threePl->id, 'wms_id' => $shippingCarrierWMSId, 'name' => $shippingCarrierWMSId]);
        }

        return $shippingCarrier->id;
    }

    public function getShippingMethodId($shippingCarrierId, $shippingMethodWMSId, $shippingMethodTitle = null)
    {
        $shippingMethod = ShippingMethod::where('shipping_carrier_id', $shippingCarrierId)->where('wms_id', $shippingMethodWMSId)->first();

        if (!$shippingMethod) {
            $shippingMethod = ShippingMethod::create(['shipping_carrier_id' => $shippingCarrierId, 'wms_id' => $shippingMethodWMSId, 'name' => $shippingMethodWMSId, 'title' => $shippingMethodTitle]);
        }

        return $shippingMethod->id;
    }

    public function getCountryId($contactInfoArr)
    {
        if (isset($contactInfoArr['country_id'])) {
            return $contactInfoArr['country_id'];
        }

        if (!isset($contactInfoArr['country_name'])) {
            return null;
        }

        $country = Country::where('title', $contactInfoArr['country_name'])->orWhere('code', $contactInfoArr['country_name'])->first();
        return $country->id ?? null;
    }

    public function setShippingCarrierAndMethodTitles($input)
    {
        if (empty($input['shipping_carrier_title']) && !empty($input['shipping_carrier_id'])) {
            $shippingCarrier = ShippingCarrier::find($input['shipping_carrier_id']);

            if ($shippingCarrier) {
                $input['shipping_carrier_title'] = $shippingCarrier->name;
            }
        }

        if (empty($input['shipping_method_title']) && !empty($input['shipping_method_id'])) {
            $shippingMethod = ShippingMethod::find($input['shipping_method_id']);

            if ($shippingMethod) {
                $input['shipping_method_title'] = $shippingMethod->name;
            }
        }

        return $input;
    }

    public function setTrackingUrl($input)
    {
        $shippingCarrier = ShippingCarrier::find($input['shipping_carrier_id']);
        $trackingUrl = null;

        if (!empty($shippingCarrier) && !empty($shippingCarrier->tracking_url)) {
            $trackingUrl = $shippingCarrier->tracking_url . $input['tracking_number'];
        }

        return $trackingUrl;
    }

    public function productList($products)
    {
        $results = [];

        foreach ($products as $product) {
            $image = '/images/product-placeholder.png';

            if (!empty($product->productImages->first())) {
                $image = $product->productImages->first()->source;
            }

            $results[] = [
                'id' => $product->id,
                'text' => $product->name,
                'html' => '<div class="search-item d-flex align-items-center"><img class="avatar rounded" src="' . $image . '"><span class="m-0 ml-2 text-default">Name: ' . $product->name . '<br>Sku: ' . $product->sku . '</span></div>',
                "status" => 'pending',
                "sku" => $product->sku,
                "name" => $product->name,
                'quantity_on_hand' => $product->quantity_on_hand,
                'quantity_backordered' => $product->quantity_backordered,
                'ordered' => $product->ordered,
                'received' => 0,
                'quantity_sell_ahead' => $product->quantity_sell_ahead ?? 0,
                'unit_price' => $product->price,
                'total_price' => 0,
            ];
        }

        return $results;
    }

    public function setLocationType($input)
    {
        if (!empty($input['location_type_name']) && !empty($input['location_type_wms_id'])) {
            $warehouse = Warehouse::find($input['warehouse_id']);

            $locationType = LocationType::where('3pl_id', $warehouse->customer->threePl->id)
                ->where('name', $input['location_type_name'])
                ->where('wms_id', $input['location_type_wms_id'])
                ->first();

            if (empty($locationType)) {
                $locationType = LocationType::create([
                    '3pl_id' => $warehouse->customer->threePl->id,
                    'name' => $input['location_type_name'],
                    'wms_id' => $input['location_type_wms_id'],
                ]);
            }

            $input['location_type_id'] = $locationType->id;
        }

        return $input;
    }

    public function filteredChangeLog($term, $changeCollection) {
        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $dates = explode(" ", $filter->value ?? '');
                    $from = Arr::get($dates, '0', '');
                    $to = Arr::get($dates, '2', '');

                    $tomorrow = Carbon::tomorrow()->toDateString();

                    $changeCollection = $changeCollection->whereBetween('object_changes.created_at', [
                        empty($from)
                            ? Carbon::now()->subDays(14)->toDateString() : date($from),
                        empty($to)
                            ? $tomorrow : Carbon::parse($to)->addDay()->toDate()->format('Y-m-d')
                    ]);

                    unset($filters->filterArray[$key]);
                }
            }

            $changeCollection = $changeCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        return $changeCollection;
    }
}
