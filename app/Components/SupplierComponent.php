<?php

namespace App\Components;

use App\Http\Requests\Supplier\StoreBatchRequest;
use App\Http\Requests\Supplier\StoreRequest;
use App\Http\Requests\Supplier\UpdateBatchRequest;
use App\Http\Requests\Supplier\UpdateRequest;
use App\Http\Requests\Supplier\DestroyBatchRequest;
use App\Http\Requests\Supplier\DestroyRequest;
use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\SupplierCollection;
use App\Http\Resources\SupplierResource;
use Illuminate\Support\Collection;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Models\Webhook;
use Illuminate\Support\Arr;
use App\Http\Requests\SupplierProduct\StoreRequest as SupplierProductRequest;

class SupplierComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();

        $contactInformationData = Arr::get($input, 'contact_information');
        $contactInformationData['country_id'] = $this->getCountryId($contactInformationData);

        Arr::forget($input, 'contact_information');

        $supplier = Supplier::create($input);

        $this->createContactInformation($contactInformationData, $supplier);

        if (!$request->wantsJson()) {
            $input['supplier_product']['products'] = array_filter(Arr::get($input, 'supplier_product.products') ?? [], function ($a) {
                return $a !== "";
            });

            $supplier->products()->sync(Arr::get($input, 'supplier_product.products'));
        }

        if ($fireWebhook == true) {
            $this->webhook(new SupplierResource($supplier), Supplier::class, Webhook::OPERATION_TYPE_STORE, $supplier->customer_id);
        }

        return $supplier;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $storeRequest->headers = $request->headers;
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, Supplier::class, SupplierCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Supplier $supplier, $fireWebhook = true)
    {
        $input = $request->validated();

        $supplier->update(Arr::except($input, ['contact_information']));

        unset($input['contact_information']['name']);
        $input['contact_information']['country_id'] = $this->getCountryId($input['contact_information']);
        $supplier->contactInformation->update(Arr::get($input, 'contact_information'));

        if (!$request->wantsJson()) {
            $input['supplier_product']['products'] = array_filter(Arr::get($input, 'supplier_product.products') ?? [], function ($a) {
                return $a !== "";
            });

            $supplier->products()->sync(Arr::get($input, 'supplier_product.products'));
        }

        if ($fireWebhook == true) {
            $this->webhook(new SupplierResource($supplier), Supplier::class, Webhook::OPERATION_TYPE_UPDATE, $supplier->customer_id);
        }

        return $supplier;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;

            if (isset($record['id'])) {
                $supplier = Supplier::where('id', $record['id'])->first();
            } elseif (isset($record['contact_information']['name'])) {
                $supplier = $this->getSupplier($record['customer_id'], $record['contact_information']['name']);
            }

            if ($supplier) {
                $updateRequest = UpdateRequest::make($record);
                $updateRequest->headers = $request->headers;
                $responseCollection->add($this->update($updateRequest, $supplier, false));
            } else {
                $storeRequest = StoreRequest::make($record);
                $storeRequest->headers = $request->headers;
                $responseCollection->add($this->store($storeRequest, false));
            }
        }

        if (!$outsideRequest) {
            $this->batchWebhook($responseCollection, Supplier::class, SupplierCollection::class, Webhook::OPERATION_TYPE_UPDATE);
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, Supplier $supplier, $fireWebhook = true)
    {
        $supplier->delete();

        $response = ['id' => $supplier->id, 'customer_id' => $supplier->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, Supplier::class, Webhook::OPERATION_TYPE_DESTROY, $supplier->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $supplier = Supplier::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $supplier, false));
        }

        $this->batchWebhook($responseCollection, Supplier::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function getSupplier($customerId, $name)
    {
        $suppliers = Supplier::where('customer_id', $customerId)->get();

        foreach ($suppliers as $supplier) {
            if ($supplier->contactInformation->name == $name) {
                return $supplier;
            }
        }

        return false;
    }

    public function products(SupplierProductRequest $request, Supplier $supplier)
    {
        $input = $request->validated();

        $input['products'] = array_filter(Arr::get($input, 'products') ?? [], function($a) { return $a !== ""; } );

        $outsideRequest = isset($input['outside_request']) ? true : false;

        if ($outsideRequest) {
            $supplier->products()->sync($input['products'], false);
        } else {
            $supplier->products()->sync($input['products']);

            $this->webhook(new SupplierResource($supplier), Supplier::class, Webhook::OPERATION_TYPE_UPDATE, $supplier->customer_id);
        }

        return $supplier;
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $supplierCollection = Supplier::query();

        $supplierCollection = $supplierCollection->whereIn('suppliers.customer_id', $customerIds);

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $supplierCollection = $supplierCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $supplierCollection
                ->where(function ($query) use ($term) {
                    $query
                        ->whereHas('contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term)
                                ->orWhere('address', 'like', $term)
                                ->orWhere('zip', 'like', $term)
                                ->orWhere('email', 'like', $term)
                                ->orWhere('phone', 'like', $term)
                                ->orWhere('city', 'like', $term);
                        })
                        ->orWhere('currency', 'like', $term)
                        ->orWhere('internal_note', 'like', $term)
                        ->orWhere('default_po_note', 'like', $term);
                });
        }

        return $supplierCollection;
    }

    public function getVendorByCustomer(Customer $customer)
    {
        return $customer->suppliers;
    }
}
