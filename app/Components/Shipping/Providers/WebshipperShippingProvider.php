<?php

namespace App\Components\Shipping\Providers;

use App\Components\Shipping\BaseShippingProvider;
use App\Models\WebshipperCredential;
use App\Models\Customer;
use App\Models\Shipment;
use GuzzleHttp;

class WebshipperShippingProvider implements BaseShippingProvider
{
    public function carrier(Customer $customer, $carrier)
    {
        $credentials = $this->getApiCredentials($customer);
        $url = $credentials['baseUrl'] . '/carriers/' . $carrier;

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/vnd.api+json',
                'Authorization' => 'Bearer ' . $credentials['apiKey'],
            ]
        ]);

        try {
            $response = $client->request('GET', $url);
            $response = json_decode($response->getBody()->getContents(), true);

            return $response['data'];
        } catch (GuzzleHttp\Exception\ClientException $e) {

        }
        
        return false;
    }

    public function carriers(Customer $customer)
    {
        $credentials = $this->getApiCredentials($customer);
        $url = $credentials['baseUrl'] . '/carriers';

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/vnd.api+json',
                'Authorization' => 'Bearer ' . $credentials['apiKey'],
            ]
        ]);

        try {
            $response = $client->request('GET', $url);
            $response = json_decode($response->getBody()->getContents(), true);

            return $response['data'];
        } catch (GuzzleHttp\Exception\ClientException $e) {

        }
        
        return false;
    }

    public function processShipment(Shipment $shipment, $carrierId, $shippingMethod)
    {
        $credentials = $this->getApiCredentials($shipment->order->customer);
        $url = $credentials['baseUrl'] . '/shipments';

    	$requestBody = $this->getRequestBody($shipment, $carrierId, $shippingMethod);

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/vnd.api+json',
                'Authorization' => 'Bearer ' . $credentials['apiKey'],
            ]
        ]);

        try {
	        $response = $client->post($url,
	            ['body' => json_encode($requestBody)]
	        );

	        $response = json_decode($response->getBody()->getContents(), true);

            return $response['data'];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        	$response = $e->getResponse();
        }
    }

    public function getApiCredentials(Customer $customer)
    {
        $baseUrl = "";
        $apiKey = "";

    	$credentials = WebshipperCredential::where('customer_id', $customer->id)->first();

    	if ($credentials) {
            $baseUrl = rtrim($credentials->api_base_url, "/");
    		$apiKey = $credentials->api_key;
    	}

    	return compact("baseUrl", "apiKey");
    }

    public function getRequestBody(Shipment $shipment, $carrierId, $shippingMethod)
    {
    	$shippingAddress = $shipment->contactInformation;
    	$customerAddress = $shipment->order->customer->contactInformation;

    	foreach ($shipment->shipmentItems as $shipmentItem) {
    		$item['sku'] = $shipmentItem->product->sku;
    		$item['description'] = $shipmentItem->product->name;
    		$item['quantity'] = $shipmentItem->quantity;
    		$item['unit_price'] = $shipmentItem->product->price;

    		$customsLines[] = $item;
    	}

    	$request['data']['type'] = 'shipments';

        $request['data']['attributes']['reference'] = $shipment->order->number;
    	$request['data']['attributes']['service_code'] = $shippingMethod;

    	$request['data']['attributes']['packages'][]['customs_lines'] = $customsLines;

    	$deliveryAddress['att_contact'] = $shippingAddress->name;
    	$deliveryAddress['company_name'] = $shippingAddress->company_name;
    	$deliveryAddress['address_1'] = $shippingAddress->address;
    	$deliveryAddress['zip'] = $shippingAddress->zip;
    	$deliveryAddress['city'] = $shippingAddress->city;
    	$deliveryAddress['country_code'] = 'DK';
    	$deliveryAddress['email'] = $shippingAddress->email;
    	$deliveryAddress['address_type'] = 'recipient';
    	$request['data']['attributes']['delivery_address'] = $deliveryAddress;

    	$senderAddress['att_contact'] = $customerAddress->name;
    	$senderAddress['company_name'] = $customerAddress->company_name;
    	$senderAddress['address_1'] = $customerAddress->address;
    	$senderAddress['zip'] = $customerAddress->zip;
    	$senderAddress['city'] = $customerAddress->city;
    	$senderAddress['email'] = $customerAddress->email;
    	$request['data']['attributes']['sender_address'] = $senderAddress;

    	$request['data']['relationships']['carrier']['data']['id'] = $carrierId;
    	$request['data']['relationships']['carrier']['data']['type'] = 'carriers';

    	return $request;
    }
}