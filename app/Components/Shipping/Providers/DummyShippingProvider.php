<?php

namespace App\Components\Shipping\Providers;

use App\Components\Shipping\BaseShippingProvider;
use App\Models\Customer;
use App\Models\Shipment;
use GuzzleHttp;

class DummyShippingProvider implements BaseShippingProvider
{
    public function carriers(Customer $customer)
    {
        $carriers[] = [
            'id' => '0',
            'alias' => 'Dummy',
            'mapped' => false,
            'mappedName' => 'Dummy',
            'services' => [
                [
                    'service_name' => 'Dummy',
                    'service_code' => 'Dummy',
                    'mapped' => true,
                    'mappedName' => 'Dummy',
                ]
            ]
        ];
        
        return $carriers;
    }

    public function processShipment(Shipment $shipment, $carrier, $shippingMethod)
    {
    }
}