<?php

namespace App\Components\Shipping;

use App\Models\Shipment;

interface BaseShippingProvider
{
	public function processShipment(Shipment $shipment, $carrierId, $shippingMethod);
}
