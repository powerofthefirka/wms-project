<?php

namespace App\Components;

use App\Http\Requests\CustomerShippingMethodsMap\StoreBatchRequest;
use App\Http\Requests\CustomerShippingMethodsMap\UpdateBatchRequest;
use App\Http\Requests\CustomerShippingMethodsMap\DestroyBatchRequest;
use App\Http\Requests\CustomerShippingMethodsMap\StoreRequest;
use App\Http\Requests\CustomerShippingMethodsMap\UpdateRequest;
use App\Http\Requests\CustomerShippingMethodsMap\DestroyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Models\CustomerShippingMethodsMap;
use App\Models\Customer;

class CustomerShippingMethodsMapComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $input['shipping_method_pattern'] = $this->generateShippingMethodPattern($input);

        $shippingMethodsMap = CustomerShippingMethodsMap::create($input);

        return $shippingMethodsMap;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }
    
    public function update(UpdateRequest $request, CustomerShippingMethodsMap $customerShippingMethodsMap)
    {
        $input = $request->validated();

        $input['shipping_method_pattern'] = $this->generateShippingMethodPattern($input);

        $customerShippingMethodsMap->update($input);

        return $customerShippingMethodsMap;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $customerShippingMethodsMap = CustomerShippingMethodsMap::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $customerShippingMethodsMap));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, CustomerShippingMethodsMap $customerShippingMethodsMap)
    {
        $customerShippingMethodsMap->delete();

        $response = ['id' => $customerShippingMethodsMap->id, 'customer_id' => $customerShippingMethodsMap->customer_id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $customerShippingMethodsMap = CustomerShippingMethodsMap::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $customerShippingMethodsMap));
        }

        return $responseCollection;
    }

    public function generateShippingMethodPattern($input)
    {
        $pattern = $input['customer_id'] . ', ';
        $pattern .= $input['shipping_method_text'] . ', ';
        $pattern .= $input['shipping_service'] . ', ';
        $pattern .= $input['shipping_service_carrier_id'] . ', ';
        $pattern .= $input['shipping_service_method_code'];

        return $pattern;
    }
    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%' )
                        ->orWhere('company_name', 'like','%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%' )
                        ->orWhere('zip', 'like', '%' . $term . '%' )
                        ->orWhere('city', 'like', '%' . $term . '%' )
                        ->orWhere('phone', 'like', '%' . $term . '%' );
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }
}