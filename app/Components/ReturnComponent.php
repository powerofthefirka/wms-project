<?php

namespace App\Components;

use App\Http\Requests\Return_\DestroyBatchRequest;
use App\Http\Requests\Return_\DestroyRequest;
use App\Http\Requests\Return_\StoreBatchRequest;
use App\Http\Requests\Return_\StoreRequest;
use App\Http\Requests\Return_\UpdateBatchRequest;
use App\Http\Requests\Return_\UpdateRequest;
use App\Http\Requests\Return_\ReceiveBatchRequest;
use App\Http\Requests\Return_\ReceiveRequest;
use App\Http\Requests\Return_\FilterRequest;
use App\Models\Order;
use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ReturnCollection;
use App\Http\Resources\ReturnResource;
use App\Models\Customer;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use App\Models\Return_;
use App\Models\ReturnItem;
use App\Models\Webhook;
use Illuminate\Support\Facades\DB;

class ReturnComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();
        $returnArr = Arr::except($input, ['return_items']);

        if (!isset($returnArr['shipping_carrier_id']) && isset($returnArr['shipping_carrier_wms_id'])) {
            $order = Order::where('id', $returnArr['order_id'])->first();

            $returnArr['shipping_carrier_id'] = $this->getShippingCarrierId($order->customer_id, $returnArr['shipping_carrier_wms_id']);
            Arr::forget($returnArr, 'shipping_carrier_wms_id');
        }

        if (!isset($returnArr['shipping_method_id']) && isset($returnArr['shipping_carrier_id']) && isset($returnArr['shipping_method_wms_id'])) {
            $returnArr['shipping_method_id'] = $this->getShippingMethodId($returnArr['shipping_carrier_id'], $returnArr['shipping_method_wms_id']);
            Arr::forget($returnArr, 'shipping_method_wms_id');
        }

        $returnArr = $this->setShippingCarrierAndMethodTitles($returnArr);

        $return = Return_::create($returnArr);

        if (isset($input['return_items'])) {
            $this->updateReturnItems($return, $input['return_items']);
        }

        $return->refresh();

        if ($fireWebhook == true) {
            $this->webhook(new ReturnResource($return), Return_::class, Webhook::OPERATION_TYPE_STORE, $return->order->customer_id);
        }

        return $return;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, Return_::class, ReturnCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Return_ $return, $fireWebhook = true)
    {
        $input = $request->validated();

        if (isset($input['return_items'])) {
            $this->updateReturnItems($return, Arr::get($input, 'return_items'));
        }

        Arr::forget($input, 'return_items');
        Arr::forget($input, 'order_id');

        if (!isset($input['shipping_carrier_id']) && isset($input['shipping_carrier_wms_id'])) {
            $input['shipping_carrier_id'] = $this->getShippingCarrierId($return->order->customer_id, $input['shipping_carrier_wms_id']);
            Arr::forget($input, 'shipping_carrier_wms_id');
        }

        if (!isset($input['shipping_method_id']) && isset($input['shipping_carrier_id']) && isset($input['shipping_method_wms_id'])) {
            $input['shipping_method_id'] = $this->getShippingMethodId($input['shipping_carrier_id'], $input['shipping_method_wms_id']);
            Arr::forget($input, 'shipping_method_wms_id');
        }

        $input = $this->setShippingCarrierAndMethodTitles($input);

        $return->update($input);

        if ($fireWebhook == true) {
            $this->webhook(new ReturnResource($return), Return_::class, Webhook::OPERATION_TYPE_UPDATE, $return->order->customer_id);
        }

        return $return;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $outsideRequest = false;

        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;

            if (isset($record['id']) && $return = Return_::where('id', $record['id'])->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $return, false));
            } elseif (isset($record['number']) && $return = Return_::where('number', $record['number'])->where('order_id', Arr::get($record, 'order_id'))->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $return, false));
            } else {
                $storeRequest = StoreRequest::make($record);
                $responseCollection->add($this->store($storeRequest, false));
            }
        }

        if (!$outsideRequest) {
            $this->batchWebhook($responseCollection, Return_::class, ReturnCollection::class, Webhook::OPERATION_TYPE_UPDATE);
        }

        return $responseCollection;
    }

    public function updateReturnItems(Return_ $return, $items)
    {
        foreach ($items as $key => $item) {
            if (!isset($item['return_item_id']) && !isset($item['order_item_id'])) {
                continue;
            } elseif (!isset($item['return_item_id']) && $returnLine = $return->returnItems->where('order_item_id', $item['order_item_id'])->first()) {
                $returnLine->quantity = $item['quantity'] ?? $returnLine->quantity;
                $returnLine->quantity_received = Arr::get($item, 'quantity_received', 0);
                $returnLine->save();
            } elseif (!isset($item['return_item_id']) && isset($item['order_item_id']) && $item['quantity'] > 0) {
                $item['return_id'] = $return->id;

                $returnItem = ReturnItem::where('return_id', $return->id)->where('order_item_id', $item['order_item_id'])->first();

                if ($returnItem) {
                    $returnItem->quantity = Arr::get($item, 'quantity', 0);
                    $returnItem->quantity_received += Arr::get($item, 'quantity_received', 0);
                    $returnItem->update();
                } else {
                    ReturnItem::create($item);
                }
            } elseif (isset($item['return_item_id']) && $item['quantity'] == 0) {
                ReturnItem::where('id', $item['return_item_id'])->first()->delete();
            } elseif (isset($item['return_item_id'])) {
                $returnItem = ReturnItem::where('id', $item['return_item_id'])->first();

                $returnItem->order_item_id = $item['order_item_id'];
                $returnItem->quantity = $item['quantity'];
                $returnItem->quantity_received = Arr::get($item, 'quantity_received', 0);
                $returnItem->save();
            }
        }
    }

    public function destroy(DestroyRequest $request, Return_ $return, $fireWebhook = true)
    {
        $return->returnItems()->delete();

        $return->delete();

        $response = ['id' => $return->id, 'customer_id' => $return->order->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, Return_::class, Webhook::OPERATION_TYPE_DESTROY, $return->order->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $return = Return_::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $return, false));
        }

        $this->batchWebhook($responseCollection, Return_::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function receive(ReceiveRequest $request, Return_ $return, ReturnItem $returnItem)
    {
        $input = $request->validated();

        $location = Location::where('id', $input['location_id'])->first();

        app()->inventoryLog->updateLocationProduct(auth()->user()->id, $returnItem->product, $input['quantity_received'], $return, $location, 'receive');

        $returnItem->quantity_received += $input['quantity_received'];
        $returnItem->save();

        return $returnItem;
    }

    public function receiveBatch(ReceiveBatchRequest $request, Return_ $return)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $receiveRequest = ReceiveRequest::make($record);
            $returnItem = ReturnItem::find($record['return_item_id']);

            $responseCollection->add($this->receive($receiveRequest, $return, $returnItem));
        }

        return $responseCollection;
    }

    public function filterOrders(Request $request, Customer $customer)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $orders = $customer->orders();

            $orders = $orders->where('number', 'like', '%' . $term . '%')->get(['id', 'number']);

            foreach ($orders as $order) {
                if ($order->count()) {
                    $results[] = [
                        'id' => $order->id,
                        'text' => $order->number
                    ];
                }
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterOrderProducts(Request $request, $orderId)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $orderItems = OrderItem::where('order_id', '=', $orderId)->where('id', '=', $term)->get();

            if ($orderItems->count() > 0) {
                $orderItems = $orderItems;
            } else {
                $orderItems = OrderItem::where('order_id', '=', $orderId)->whereHas('product', function ($query) use ($term) {
                    $term = '%' . $term . '%';

                    $query->where('name', 'like', $term);
                    $query->orWhere('sku', 'like', $term);
                })->get();
            }
            foreach ($orderItems as $orderItem) {
                if ($orderItem->count()) {
                    $results[] = [
                        'id' => $orderItem->id,
                        'text' => 'SKU: ' . $orderItem->product->sku . ', NAME:' . $orderItem->product->name
                    ];
                }
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function getOrderProducts(Request $request, $orderId)
    {
        $results = [];

        $orderItems = OrderItem::where('order_id', '=', $orderId)->get();

        if ($orderItems->count() > 0) {
            foreach ($orderItems as $orderItem) {
                if ($orderItem->count()) {
                    $image = '';

                    if (!empty($orderItem->product) && !empty($orderItem->product->productImages->first())) {
                        $image = $orderItem->product->productImages->first()->source;
                    }

                    $results[] = [
                        'id' => $orderItem->id,
                        'name' => $orderItem->name,
                        'sku' => $orderItem->sku,
                        'image' => $image,
                        'quantity' => $orderItem->quantity
                    ];
                }
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterLocations(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $term = '%' . $term . '%';

            $locations = Location::where('name', 'like', $term)->get();

            foreach ($locations as $location) {
                $results[] = [
                    'id' => $location->id,
                    'text' => $location->name
                ];
            }

            return response()->json([
                'results' => $results
            ]);
        }
    }

    public function filter(FilterRequest $request, $orderIds)
    {
        $query = Return_::query();

        $query->when($request['from_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '>=', $request['from_date_created']);
        });

        $query->when($request['to_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '<=', $request['to_date_created'] . ' 23:59:59');
        });

        $query->when($request['from_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '>=', $request['from_date_updated']);
        });

        $query->when($request['to_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '<=', $request['to_date_updated'] . ' 23:59:59');
        });

        $query->when(count($orderIds) > 0, function ($q) use ($orderIds) {
            return $q->whereIn('order_id', $orderIds);
        });

        $returns = $query->paginate();

        return $returns;
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $returnOrdersCollection = Return_::query();

        if (!empty($customerIds)) {
            $returnOrdersCollection = $returnOrdersCollection->whereHas('order', function ($query) use ($customerIds) {
                $query->whereIn('customer_id', $customerIds);
            });
        }

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $dates = explode(" ", $filter->value);
                    $from = Arr::get($dates, '0', '');
                    $to = Arr::get($dates, '2', '');

                    $today = Carbon::today()->toDateString();
                    $tomorrow = Carbon::tomorrow()->toDateString();

                    $returnOrdersCollection = $returnOrdersCollection->whereBetween('returns.requested_at', [
                        empty($from)
                            ? Carbon::now()->subDays(14)->toDateString() : date($from),
                        empty($to)
                            ? $tomorrow : Carbon::parse($to)->addDay()->toDate()->format('Y-m-d')
                    ]);

                    unset($filters->filterArray[$key]);
                }

                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $returnOrdersCollection = $returnOrdersCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }
        if ($term) {
            $term = '%' . $term . '%';

            $returnOrdersCollection
                ->where(function ($query) use ($term) {
                    $query->whereHas('order', function ($query) use ($term) {
                        $query->where('number', 'like', $term);
                    })
                        ->orWhereHas('order.customer.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhere('returns.number', 'like', $term)
                        ->orWhereRaw('REPLACE(returns.status, "_", " ") = "' . str_replace(['%', '_'], ['', ' '], $term) . '"');
                });
        }

        return $returnOrdersCollection;
    }
}
