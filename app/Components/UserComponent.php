<?php

namespace App\Components;

use App\Http\Requests\AccessTokenRequest;
use App\Http\Requests\User\DestroyBatchRequest;
use App\Http\Requests\User\DestroyRequest;
use App\Http\Requests\User\StoreBatchRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateBatchRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\ContactInformation;
use App\Models\Customer;
use App\Models\CustomerUserRole;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Laravel\Passport\Token;

class UserComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();

        $input['password'] = \Hash::make($input['password']);

        $contactInformationData = Arr::get($input, 'contact_information');
        Arr::forget($input, 'contact_information');

        $customerId = empty($request->get('customer_id')) ? session('customer_id') : $request->get('customer_id');

        $user = User::create($input);
        if (!empty($customerId)) {
            $user->customers()->attach($customerId, [
                'role_id' => CustomerUserRole::ROLE_DEFAULT
            ]);
        }

        $this->createContactInformation($contactInformationData, $user);

        if ($fireWebhook == true) {
            $this->userWebhook(new UserResource($user), Webhook::OPERATION_TYPE_STORE, $user->customers);
        }

        return $user;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->userBatchWebhook($responseCollection, UserCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, User $user, $fireWebhook = true)
    {
        $input = $request->validated();
        if (!empty($input['password'])) {
            $input['password'] = \Hash::make($input['password']);
        } else {
            unset($input['password']);
        }

        $user->contactInformation->update($input['contact_information']);
        Arr::forget($input, 'contact_information');
        $user->update($input);

        if ($fireWebhook == true) {
            $this->userWebhook(new UserResource($user), Webhook::OPERATION_TYPE_UPDATE, $user->customers);
        }

        return $user;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $user = User::where('email', $record['email'])->first();

            $responseCollection->add($this->update($updateRequest, $user, false));
        }

        $this->userBatchWebhook($responseCollection, UserCollection::class, Webhook::OPERATION_TYPE_UPDATE);

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, User $user = null, Customer $customer = null, $fireWebhook = true)
    {
        if (!$customer) {
            $customers = $user->customers;

            $user->delete();
        } else {
            $customers = $user->customers;
            $user->customers()->detach($customer->id);

            if (!$user->customers()->count()) {
                $user->delete();
            }
        }

        $response = ['email' => $user->email, 'customers' => $customers ?? []];

        if ($fireWebhook == true) {
            $this->userWebhook($response, Webhook::OPERATION_TYPE_DESTROY, $customers);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $user = User::where('email', $record['email'])->first();
            $customer = null;

            if (!empty($record['customer_id'])) {
                $customer = Customer::find($record['customer_id']);
            }

            $responseCollection->add($this->destroy($destroyRequest, $user, $customer, false));
        }

        $this->userBatchWebhook($responseCollection, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function getCustomers()
    {
        $user = auth()->user();

        if ($user->isAdmin()) {

            $ordered = Customer::join('contact_informations', 'customers.id', '=', 'contact_informations.object_id')
                ->where('contact_informations.object_type', Customer::class)
                ->select('*', 'customers.*')
                ->orderBy('contact_informations.name', 'asc')
                ->get();

            return $ordered;
        } else {
            return $user->customers->where('active', 1);
        }
    }

    public function accessTokens(AccessTokenRequest $request)
    {
        foreach ($request->input('access_token', []) as $id => $name) {
            if ($id) {
                Token::where(['id' => $id])->update(['name' => $name]);
            } else if ($name) {
                auth()->user()->createToken($name);
            }
        }

        return true;
    }

    public function deleteAccessToken(Token $token)
    {
        $token->delete();

        return true;
    }

    public function getCustomerUsers(Customer $customer)
    {
        return $customer->users()->paginate();
    }

    public function getAllCustomerUserIds(User $user)
    {
        $userIds = array();

        $customers = $user->customers;

        $customers->each(function ($item, $key) use(&$userIds){
            $ids = $item->users->pluck('id')->unique()->toArray();

            $userIds = array_merge($userIds, $ids);
        });

        return $userIds;
    }

    public function userBatchWebhook($collections, $resourceCollection, $operation)
    {
        $customerWiseItems = [];

        foreach ($collections as $key => $item) {
            $customers = $operation == Webhook::OPERATION_TYPE_DESTROY ? $item['customers'] : $item->customers;

            if (count($customers) == 0)
                continue;

            foreach ($customers as $key => $customer) {
                $customerId = $customer->id;

                unset($item['customers']);

                $customerWiseItems[$customerId][] = $item;
            }
        }

        foreach ($customerWiseItems as $customerId => $users) {
            $collections = new Collection($users);

            $this->webhook(new $resourceCollection($collections), User::class, $operation, $customerId);
        }
    }

    protected function userWebhook($response, $operation, $customers)
    {
        if (is_null($customers))
            return false;

        foreach ($customers as $key => $customer) {
            unset($response['customers']);

            $this->webhook($response, User::class, $operation, $customer->id);
        }
    }
}
