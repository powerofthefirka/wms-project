<?php

namespace App\Components;

use App\Http\Requests\ThreePl\StoreBatchRequest;
use App\Http\Requests\ThreePl\StoreRequest;
use App\Http\Requests\ThreePl\UpdateBatchRequest;
use App\Http\Requests\ThreePl\UpdateRequest;
use App\Http\Requests\ThreePl\DestroyBatchRequest;
use App\Http\Requests\ThreePl\DestroyRequest;
use App\Http\Requests\ThreePl\UpdateUsersRequest;
use App\Http\Requests\ThreePl\SetPricingPlanRequest;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ThreePlCollection;
use App\Http\Resources\ThreePlResource;
use Illuminate\Support\Collection;
use App\Models\ThreePl;
use App\Models\User;
use App\Models\PricingPlan;
use App\Models\ThreePlPricingPlan;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use DB;

class ThreePlComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $contactInformationData = Arr::get($input, 'contact_information');
        $contactInformationData['country_id'] = $this->getCountryId($contactInformationData);

        Arr::forget($input, 'contact_information');

        if(isset($input['custom_shipping_modal'])) {
            $input['custom_shipping_modal'] = true;
            $threePl = ThreePl::create($input);
        } else {
            $threePl = ThreePl::create();
        }

        $this->createContactInformation($contactInformationData, $threePl);

        return $threePl;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $storeRequest->headers = $request->headers;
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, ThreePl $threePl)
    {
        $input = $request->validated();

        $input['contact_information']['country_id'] = $this->getCountryId($input['contact_information']);
        $threePl->contactInformation->update(Arr::get($input, 'contact_information'));

        $input['custom_shipping_modal'] = isset($input['custom_shipping_modal']) ? true : false;
        $threePl->update($input);

        return $threePl;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $threePl = ThreePl::where('id', $record['id'])->first();

            $responseCollection->add($this->update($updateRequest, $threePl));
        }

        return $responseCollection;
    }

    public function detachUser(ThreePl $threePl, User $user)
    {
        return $threePl->users()->detach($user->id);
    }

    public function destroy(DestroyRequest $request = null, ThreePl $threePl = null)
    {
        $threePl->delete();

        return ['id' => $threePl->id, 'name' => $threePl->contactInformation->name];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $threePl = ThreePl::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $threePl));
        }

        return $responseCollection;
    }

    public function toggleStatusThreePLStatus(ThreePl $threePl, $status = 1)
    {
        foreach ($threePl->customers as $customer) {
            if (!empty($customer->local_key)) {
                $customer->active_integration = $status;
                CamelIntegration::updateIntegrationStatus($customer->local_key, $status);
            }

            $customer->active = $status;
            $customer->save();
        }

        $threePl->active = $status;
        $threePl->save();
    }

    public function search($term)
    {
        $threePlCollection = ThreePl::query();

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $threePlCollection = $threePlCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $threePlCollection
                ->where(function ($query) use ($term) {
                    $query
                        ->whereHas('contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term)
                                ->orWhere('address', 'like', $term)
                                ->orWhere('zip', 'like', $term)
                                ->orWhere('email', 'like', $term)
                                ->orWhere('phone', 'like', $term)
                                ->orWhere('city', 'like', $term);
                        });
                });
        }

        return $threePlCollection;
    }

    public function filterUsers(Request $request, ThreePl $threePl)
    {
        $term = $request->get('term');
        $results = [];
        $usersIds = [];

        if ($term) {
            foreach ($threePl->users as $users) {
                $usersIds[] = $users->id;
            }
            $users = User::whereHas('contactInformation', function ($query) use ($term) {
                $term = '%' . $term . '%';

                $query->where('name', 'like', $term)
                    ->orWhere('company_name', 'like', $term)
                    ->orWhere('email', 'like', $term)
                    ->orWhere('zip', 'like', $term)
                    ->orWhere('city', 'like', $term)
                    ->orWhere('phone', 'like', $term);
            })->whereNotIn('id', $usersIds)->get();

            foreach ($users as $user) {
                $results[] = [
                    'id' => $user->id,
                    'text' => $user->contactInformation->name . ', ' . $user->contactInformation->email . ', ' . $user->contactInformation->zip . ', ' . $user->contactInformation->city . ', ' . $user->contactInformation->phone
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function updateUsers(UpdateUsersRequest $request, ThreePl $threePl)
    {
        $input = $request->validated();

        $threePlUserIds = [];

        if (isset($input['new_user_id'])) {
            $threePlUserIds[] = $input['new_user_id'];
        }

        $threePl->users()->sync($threePlUserIds, false);
    }

    public function getPricingPlanDetails(ThreePl $threePl, $pricingPlanName)
    {
        $threePlPricingPlan = ThreePlPricingPlan::where('3pl_id', $threePl->id)->where('pricing_plan_name', $pricingPlanName)->first();
        $selectedPricingPlan = PricingPlan::where('name', $pricingPlanName)->first();

        $result = [
            'initial_fee' => $threePlPricingPlan->initial_fee ?? $selectedPricingPlan->initial_fee,
            'billing_period' => $threePlPricingPlan->billing_period ?? 'yearly',
            'three_pl_billing_fee' => $threePlPricingPlan->three_pl_billing_fee ?? $selectedPricingPlan->three_pl_billing_fee,
            'number_of_monthly_orders' => $threePlPricingPlan->number_of_monthly_orders ?? $selectedPricingPlan->number_of_monthly_orders,
            'price_per_additional_order' => $threePlPricingPlan->price_per_additional_order ?? $selectedPricingPlan->price_per_additional_order,
        ];

        return $result;
    }

    public function getMonthlyPrice(ThreePl $threePl, $pricingPlanName, $billingPeriod)
    {
        $pricingPlan = ThreePlPricingPlan::where('3pl_id', $threePl->id)->where('pricing_plan_name', $pricingPlanName)->where('billing_period', $billingPeriod)->first();

        return $pricingPlan->monthly_price ?? PricingPlan::where('name', $pricingPlanName)->first()->toArray()['monthly_price_for_' . $billingPeriod . '_billing_period'];
    }

    public function updatePricingPlan(SetPricingPlanRequest $request, ThreePl $threePl)
    {
        $input = $request->validated();

        if ($threePl->pricingPlan) {
            $threePl->pricingPlan->update($input);
        } else {
            $pricingPlan = ThreePlPricingPlan::create($input);
        }

        return $pricingPlan ?? $threePl->pricingPlan;
    }

    public function getBillingReportStartEndDates($datesValue = "")
    {
        $dates = explode(" ", $datesValue);
        $startDate = Arr::get($dates, '0', '');
        $endDate = Arr::get($dates, '2', '');

        $startDate = empty($startDate) ? Carbon::now()->firstOfMonth()->toDateString() : date($startDate);
        $endDate = empty($endDate) ? Carbon::tomorrow()->toDateString() : Carbon::parse($endDate)->addDay()->toDate()->format('Y-m-d');

        return compact("startDate", "endDate");
    }

    public function getBillingReportTotalOrdersTotalPrice(ThreePl $threePl, $startEndDates)
    {
        $totalNumberOfOrders = 0;
        $totalOrderPrice = 0;

        $start = Carbon::parse($startEndDates['startDate']);
        $end = Carbon::parse($startEndDates['endDate']);

        while ($start->lessThan($end)) {
            $nextStart = Carbon::parse($start->toDateString())->addMonthsNoOverflow();

            if ($nextStart->greaterThanOrEqualTo($end)) {
                $numberOfOrders = Order::whereIn('customer_id', $threePl->customers->pluck('id')->toArray())->where('ordered_at', '>=', utc_date_time($start->toDateTimeString()))->where('ordered_at', '<', utc_date_time($end->toDateTimeString()))->count();
            } else {
                $numberOfOrders = Order::whereIn('customer_id', $threePl->customers->pluck('id')->toArray())->where('ordered_at', '>=', utc_date_time($start->toDateTimeString()))->where('ordered_at', '<', utc_date_time($nextStart->toDateTimeString()))->count();
            }

            $monthlyPrice = $threePl->pricingPlan->monthly_price;

            if ($numberOfOrders > $threePl->pricingPlan->number_of_monthly_orders) {
                $additionalOrders = $numberOfOrders - $threePl->pricingPlan->number_of_monthly_orders;
                $monthlyPrice += $additionalOrders * $threePl->pricingPlan->price_per_additional_order;
            }

            $totalOrderPrice += $monthlyPrice;
            $totalNumberOfOrders += $numberOfOrders;

            $start = $nextStart;
        }

        return compact("totalOrderPrice", "totalNumberOfOrders");
    }

    public function exportBillingReportToCsv(Request $request, ThreePl $threePl)
    {
        $startEndDates = $this->getBillingReportStartEndDates($request->export_dates_between);
        $totalOrdersTotalPrice = $this->getBillingReportTotalOrdersTotalPrice($threePl, $startEndDates);

        $fileName = $startEndDates['startDate'] . ' - ' . Carbon::parse($startEndDates['endDate'])->subDay()->toDate()->format('Y-m-d')  . '.csv';

        $orderCollection = Order::whereIn('customer_id', $threePl->customers->pluck('id')->toArray())
            ->whereBetween('ordered_at', [utc_date_time($startEndDates['startDate']), utc_date_time($startEndDates['endDate'])])
            ->select('customer_id', DB::raw('count(*) as total_order'))
            ->groupBy('customer_id')->get();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $columns = [
            'Bill Period Start',
            'Bill Period End',
            '3PL',
            'Customer',
            'Number of Orders',
        ];

        $callback = function() use($orderCollection, $columns, $startEndDates, $totalOrdersTotalPrice) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($orderCollection as $item) {
                fputcsv($file, [
                    $startEndDates['startDate'] ?? '',
                    Carbon::parse($startEndDates['endDate'])->subDay()->toDate()->format('Y-m-d') ?? '',
                    $item->customer->threePl->contactInformation->name ?? '',
                    $item->customer->contactInformation->name ?? '',
                    $item->total_order ?? ''
                ]);
            }

            fputcsv($file, ['', '', '', '', 'Total Number of Orders: ' . $totalOrdersTotalPrice['totalNumberOfOrders']]);
            fputcsv($file, ['', '', '', '', 'Total: ' . $totalOrdersTotalPrice['totalOrderPrice']]);

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
