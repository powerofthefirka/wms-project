<?php

namespace App\Components;


use App\Http\Requests\Product\DestroyBatchRequest;
use App\Http\Requests\Product\DestroyRequest;
use App\Http\Requests\Product\StoreBatchRequest;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateBatchRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Requests\Product\FilterRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Webhook;
use App\Models\Image;
use App\Models\ObjectChange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductComponent extends BaseComponent
{
    public function __construct()
    {

    }

    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();

        $input['name'] = isset($input['name']) ? $input['name'] : $input['sku'];

        if (isset($input['weight'])) {
            $input = $this->reformWeight($input);
        }

        $input = $this->reformHeightWidthLength($input);

        $product = Product::create($input);

        if (isset($input['product_images'])) {
            $this->updateProductImageURLs($product, $input['product_images']);
        }

        if ($images = $request->file('file')) {
            $this->updateProductImages($product, $images);
        }

        if (!$request->wantsJson()) {
            $input['supplier_product']['suppliers'] = array_filter(Arr::get($input, 'supplier_product.suppliers') ?? [], function($a) { return $a !== ""; } );

            $product->suppliers()->sync(Arr::get($input, 'supplier_product.suppliers', []));
        }

        if ($fireWebhook == true) {
            $this->webhook(new ProductResource($product), Product::class, Webhook::OPERATION_TYPE_STORE, $product->customer_id);
        }

        return $product;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $storeRequest->headers = $request->headers;
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, Product::class, ProductCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, Product $product = null, $fireWebhook = true)
    {
        $input = $request->validated();

        if (!$product) {
            $product = Product::where('sku', Arr::get($input, 'sku'))->where('customer_id', Arr::get($input, 'customer_id'))->first();
        }

        if (isset($input['weight'])) {
            $input = $this->reformWeight($input);
        }

        $input = $this->reformHeightWidthLength($input);

        $product->update($input);

        if (isset($input['product_images'])) {
            $this->updateProductImageURLs($product, $input['product_images']);
        }

        if ($images = $request->file('file')) {
            $this->updateProductImages($product, $images);
        }

        if (!$request->wantsJson()) {
            $input['supplier_product']['suppliers'] = array_filter(Arr::get($input, 'supplier_product.suppliers') ?? [], function($a) { return $a !== ""; } );

            $product->supplierProduct()->sync(Arr::get($input['supplier_product'], 'suppliers'));
        }

        if ($fireWebhook == true) {
            $this->webhook(new ProductResource($product), Product::class, Webhook::OPERATION_TYPE_UPDATE, $product->customer_id);
        }

        return $product;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $outsideRequest = false;

        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;

            $product = Product::where('sku', Arr::get($record, 'sku'))->where('customer_id', Arr::get($record, 'customer_id'))->first();

            if ($product) {
                $updateRequest = UpdateRequest::make($record);
                $updateRequest->headers = $request->headers;
                $responseCollection->add($this->update($updateRequest, $product, false));
            } else {
                $storeRequest = StoreRequest::make($record);
                $storeRequest->headers = $request->headers;
                $responseCollection->add($this->store($storeRequest, false));
            }
        }

        if (!$outsideRequest) {
            $this->batchWebhook($responseCollection, Product::class, ProductCollection::class, Webhook::OPERATION_TYPE_UPDATE);
        }


        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, Product $product = null, $fireWebhook = true)
    {
        if (!$product) {
            $input = $request->validated();
            $product = Product::where('sku', $input['sku'])->where('customer_id', $input['customer_id'])->first();
        }

        $product->delete();

        $response = ['sku' => $product->sku, 'customer_id' => $product->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, Product::class, Webhook::OPERATION_TYPE_DESTROY, $product->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $responseCollection->add($this->destroy($destroyRequest, null, false));
        }

        $this->batchWebhook($responseCollection, Product::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function updateProductImageURLs(Product $product, $items)
    {
        foreach ($items as $key => $item) {
            $image = Image::where('source', $item['source'])->where('object_type', Product::class)->where('object_id', $product->id)->first();

            if ($image) {
                continue;
            }

            $imageObj = new Image();
            $imageObj->source = $item['source'];
            $imageObj->filename = '';
            $imageObj->object()->associate($product);
            $imageObj->save();
        }
    }

    public function updateProductImages(Product $product, $images)
    {
        foreach ($images as $image) {
            $filename = $image->store('public');
            $source = url(Storage::url($filename));

            $imageObj = new Image();
            $imageObj->source = $source;
            $imageObj->filename = $filename;
            $imageObj->object()->associate($product);
            $imageObj->save();
        }
    }

    public function deleteProductImage(Request $request)
    {
        $id = (int)$request->get('id');
        $success = false;

        if ($image = Image::get()->find($id)) {
            if (Storage::delete($image['filename'])) {
                $success = $image->delete();
            }
        }

        return response()->json([
            'success' => $success
        ]);
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function getCustomerProduct(Customer $customer)
    {
        return $customer->products()->paginate();
    }

    public function filter(FilterRequest $request, $customerIds)
    {
        $query = Product::query();

        $query->when($request['from_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '>=', $request['from_date_created']);
        });

        $query->when($request['to_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '<=', $request['to_date_created'] . ' 23:59:59');
        });

        $query->when($request['from_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '>=', $request['from_date_updated']);
        });

        $query->when($request['to_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '<=', $request['to_date_updated'] . ' 23:59:59');
        });

        $query->when($request['location_id'], function ($q) use ($request) {
            return $q->whereHas('location', function ($loc) use ($request) {
                return $loc->where('locations.id', $request['location_id']);
            });
        });

        $query->when(count($customerIds) > 0, function ($q) use ($customerIds) {
            return $q->whereIn('customer_id', $customerIds);
        });

        $products = $query->paginate();

        return $products;
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $productCollection = Product::whereIn('products.customer_id', $customerIds);

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $productCollection = $productCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $productCollection
                ->where(function($query) use ($term) {
                    $query->whereHas('customer.contactInformation', function ($query) use ($term) {
                        $query->where('name', 'like', $term);
                    })
                    ->orWhere('products.name', 'like', $term)
                    ->orWhere('products.quantity_on_hand', 'like', $term)
                    ->orWhere('products.quantity_available', 'like', $term)
                    ->orWhere('products.quantity_allocated', 'like', $term)
                    ->orWhere('products.quantity_backordered', 'like', $term)
                    ->orWhere('products.quantity_on_po', 'like', $term)
                    ->orWhere('products.quantity_sell_ahead', 'like', $term)
                    ->orWhere('products.sku', 'like', $term)
                    ->orWhere('products.barcode', 'like', $term)
                    ->orWhere('products.weight', 'like', $term)
                    ->orWhere('products.is_kit', 'like', $term)
                    ->orWhere('products.price', 'like', $term)
                    ->orWhere('products.replacement_value', 'like', $term)
                    ->orWhere('products.notes', 'like', $term)
                    ->orWhere('products.country_of_origin', 'like', $term)
                    ->orWhere('products.hs_code', 'like', $term);
                });
        }

        return $productCollection;
    }

    public function searchChangeLog($term, $productId)
    {
        if ($productId) {
            $changeCollection = ObjectChange::where('object_type', Product::class)->where('object_id', $productId);
        } else {
            $changeCollection = ObjectChange::where('object_type', Product::class);
        }

        return $this->filteredChangeLog($term, $changeCollection);
    }

    public function reformWeight($input) {
        $weightWithUnit = $input['weight'];

        $input['weight'] = (float) filter_var( $weightWithUnit, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $input['weight_unit'] = preg_replace("/[^a-zA-Z]+/", "", $weightWithUnit);
        $input['weight_unit'] = empty($input['weight_unit']) ? 'lb' : $input['weight_unit'];

        return $input;
    }

    public function reformHeightWidthLength($input) {
        if (!isset($input['height']) && !isset($input['width']) && !isset($input['length'])) {
            return $input;
        }

        $dimensionsUnit = "";

        if (isset($input['height'])) {
            $heightWithUnit = $input['height'];
    
            $input['height'] = (float) filter_var( $heightWithUnit, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $dimensionsUnit = empty(preg_replace("/[^a-zA-Z]+/", "", $heightWithUnit)) ? $dimensionsUnit : preg_replace("/[^a-zA-Z]+/", "", $heightWithUnit);
        }

        if (isset($input['width'])) {
            $widthWithUnit = $input['width'];
    
            $input['width'] = (float) filter_var( $widthWithUnit, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $dimensionsUnit = empty(preg_replace("/[^a-zA-Z]+/", "", $widthWithUnit)) ? $dimensionsUnit : preg_replace("/[^a-zA-Z]+/", "", $widthWithUnit);
        }

        if (isset($input['length'])) {
            $lengthWithUnit = $input['length'];
    
            $input['length'] = (float) filter_var( $lengthWithUnit, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $dimensionsUnit = empty(preg_replace("/[^a-zA-Z]+/", "", $lengthWithUnit)) ? $dimensionsUnit : preg_replace("/[^a-zA-Z]+/", "", $lengthWithUnit);
        }

        $input['dimensions_unit'] = empty($dimensionsUnit) ? 'inch' : $dimensionsUnit;

        return $input;
    }
}
