<?php

namespace App\Components;

use DB;
use App\Models\Image;
use App\Models\Webhook;
use App\Models\ThreePl;
use App\Models\Customer;
use App\Models\CustomerShippingMethod;
use Illuminate\Http\Request;
use App\Models\ShippingMethod;
use App\Models\ShippingCarrier;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ShippingCarrierResource;
use App\Http\Resources\ShippingCarrierCollection;
use App\Http\Requests\ShippingCarrier\StoreRequest;
use App\Http\Requests\ShippingCarrier\UpdateRequest;
use App\Http\Requests\ShippingCarrier\DestroyRequest;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Requests\ShippingCarrier\StoreBatchRequest;
use App\Http\Requests\ShippingCarrier\UpdateBatchRequest;
use App\Http\Requests\ShippingCarrier\DestroyBatchRequest;

class ShippingCarrierComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $shippingCarrier = ShippingCarrier::create($input);

        if ($image = $request->file('file')) {
            $this->updateImage($shippingCarrier, $image);
        }

        return $shippingCarrier;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, ShippingCarrier $shippingCarrier)
    {
        $input = $request->validated();

        $shippingCarrier->update($input);

        if ($image = $request->file('file')) {
            $this->updateImage($shippingCarrier, $image);
        }

        return $shippingCarrier;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $shippingCarrier = ShippingCarrier::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $shippingCarrier));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, ShippingCarrier $shippingCarrier)
    {
        $shippingCarrier->delete();

        $response = ['id' => $shippingCarrier->id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $shippingCarrier = ShippingCarrier::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $shippingCarrier));
        }

        return $responseCollection;
    }

    public function filter3Pls(Request $request)
    {
        $threePlIds = Auth()->user()->threePlIds();
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = ThreePl::where('id', '=', $term)->get();

            if (!$contactInformation->count()) {
                $contactInformation = ThreePl::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->where('active', 1)->whereIn('3pls.id', $threePlIds)->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function updateImage(ShippingCarrier $shippingCarrier, $image)
    {
        try {
            DB::beginTransaction();

            $oldImage = $shippingCarrier->image;

            $filename = $image->store('public');
            $source = url(Storage::url($filename));

            $imageObj = new Image();
            $imageObj->source = $source;
            $imageObj->filename = $filename;
            $imageObj->object()->associate($shippingCarrier);
            $imageObj->save();

            if ($oldImage) {
                Storage::delete($oldImage['filename']);
                $oldImage->delete();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public static function getShippingCarriersAndMethods($customers)
    {
        $results = [];

        $customerShippingMethodIds = CustomerShippingMethod::where('customer_id', $customers)->pluck('shipping_method_id');
        $methods = ShippingMethod::whereIn('id', $customerShippingMethodIds)->where('is_visible', true)->get();

        foreach ($methods as $method) {
            $results[] = [
                'id' => $method->shippingCarrier->id,
                'text' => $method->shippingCarrier->name,
                'image' => $method->shippingCarrier->image->source ?? "/images/product-placeholder.png",
                'method_id' => $method->id,
                'method_text' => $method->title,
                'method_cost' => $method->cost,
                'cost_with_currency' => with_currency(session('customer_id') ? Customer::find(session('customer_id')) : '', $method->cost)
            ];
        }

        return $results;
    }

    public function getShippingCarriers(Request $request, $threePlIds)
    {
        $results = [];

        $carriers = ShippingCarrier::whereIn('3pl_id', $threePlIds);

        $term = $request->get('term');

        if (!empty($term)) {
            $carriers->where('name', 'LIKE', '%' . $term . '%');
        }

        $carriers = $carriers->get();

        foreach ($carriers as $carrier) {
            $results[] = [
                'id' => $carrier->id,
                'text' => $carrier->name,
                'image' => $carrier->image->source ?? ""
            ];
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function search($term)
    {
        $threePlIds = Auth()->user()->threePlIds();

        $orderCollection = ShippingCarrier::query();

        if (!empty($threePlIds)) {
            $orderCollection = $orderCollection->whereIn('shipping_carriers.3pl_id', $threePlIds);
        }

        if ($term) {
            $term = '%' . $term . '%';

            $orderCollection
                ->where(function ($query) use ($term) {
                    $query->orWhereHas('threePl.contactInformation', function ($query) use ($term) {
                        $query->where('name', 'like', $term);
                    })
                    ->orWhere('name', 'like', $term);
                });
        }

        return $orderCollection;
    }
}
