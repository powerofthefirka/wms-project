<?php

namespace App\Components;

use App\Models\Customer;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use GuzzleHttp;

class CamelIntegration
{
    const ENDPOINT_INTEGRATIONS = '/api/integrations';

    const ENDPOINT_ALL_INTEGRATIONS = '/api/integrations/all';

    public static function getIntegrationByLocalKey($localKey)
    {
        return self::send('GET', self::ENDPOINT_INTEGRATIONS, [
            RequestOptions::HEADERS => [
                'local_key' => $localKey
            ]
        ]);
    }

    public static function getAllIntegrations()
    {
        $integrations = self::send('GET', self::ENDPOINT_ALL_INTEGRATIONS, []);

        $integrationsKeyByCustomerId = [];
        foreach ($integrations ?? [] as $integration) {
            $integrationsKeyByCustomerId[Arr::get($integration, 'three_pld_customer_id')] = $integration;
        }

        return $integrationsKeyByCustomerId;
    }

    public static function updateIntegrationStatus($localKey, $active = 0)
    {
        return self::send('PUT', self::ENDPOINT_INTEGRATIONS, [
            RequestOptions::HEADERS => [
                'local_key' => $localKey
            ],
            RequestOptions::JSON => [
                'readonly' => 1,
                'active' => $active
            ]
        ]);
    }

    public static function updateIntegrationWebhooks($localKey)
    {
        try {
            return self::send('PUT', self::ENDPOINT_INTEGRATIONS, [
                RequestOptions::HEADERS => [
                    'local_key' => $localKey
                ],
                RequestOptions::JSON => [
                    'webhook_update' => true
                ]
            ]);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public static function updateIntegration(Customer $customer, $accessToken, $shipheroUsername, $shipheroPassword, $shipheroRefreshToken, $shipheroAccessToken, $syncDateFrom, $localKey)
    {
        if (empty($localKey)) {
            return self::registerIntegration($customer, $accessToken, $shipheroUsername, $shipheroPassword, $shipheroRefreshToken, $shipheroAccessToken, $syncDateFrom);
        }

        $data = [
            'three_pld_url' => URL::to('/') . '/',
            'three_pld_customer_id' => $customer->id,
            'three_pld_token' => $accessToken,
            'three_pld_webhook_secret' => 'secret',
            'shiphero_access_token' => $shipheroAccessToken,
            'shiphero_refresh_token' => $shipheroRefreshToken,
            'readonly' => 1,
            'active' => 1
        ];

        if (!empty($shipheroUsername) && !empty($shipheroPassword)) {
            $data['shiphero_username'] = $shipheroUsername;
            $data['shiphero_password'] = $shipheroPassword;
        }

        return self::send('PUT', self::ENDPOINT_INTEGRATIONS, [
            RequestOptions::HEADERS => [
                'local_key' => $localKey
            ],
            RequestOptions::JSON => $data
        ]);
    }

    public static function registerIntegration(Customer $customer, $accessToken, $shipheroUsername, $shipheroPassword, $shipheroRefreshToken, $shipheroAccessToken, $syncDateFrom = null)
    {
        $data = [
            'three_pld_url' => URL::to('/') . '/',
            'three_pld_customer_id' => $customer->id,
            'three_pld_token' => $accessToken,
            'three_pld_webhook_secret' => 'secret',
            'shiphero_access_token' => $shipheroAccessToken,
            'shiphero_refresh_token' => $shipheroRefreshToken,
            'sync_date_from' => empty($syncDateFrom) ? now()->format('Y-m-d') : $syncDateFrom,
            'readonly' => 1,
            'active' => 1
        ];

        if (!empty($shipheroUsername) && !empty($shipheroPassword)) {
            $data['shiphero_username'] = $shipheroUsername;
            $data['shiphero_password'] = $shipheroPassword;
        }

        try {
            return self::send('POST', self::ENDPOINT_INTEGRATIONS, [
                RequestOptions::JSON => $data
            ]);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    private static function send($method, $endpoint, $options)
    {
        $options = array_merge_recursive($options, [
            RequestOptions::HEADERS => [
                'master_api_key' => env('CAMEL_MASTER_KEY'),
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ]);

        try {
            $client = new GuzzleHttp\Client();
            $response = $client->request($method, rtrim(env('CAMEL_SERVER'), DIRECTORY_SEPARATOR) . $endpoint, $options);

            return GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleHttp\Exception\ClientException $e) {
            Log::info('[CAMEL-INTEGRATION] Request failed', [$e->getMessage()]);
        } catch (GuzzleHttp\Exception\ConnectException $e) {
            Log::info('[CAMEL-INTEGRATION] Request failed', [$e->getMessage()]);
        }

        return [];
    }
}
