<?php

namespace App\Components;

use App\Events\PurchaseOrderSaved;
use App\Http\Requests\Product\StoreRequest as ProductStoreRequest;
use App\Http\Requests\PurchaseOrder\DestroyBatchRequest;
use App\Http\Requests\PurchaseOrder\DestroyRequest;
use App\Http\Requests\PurchaseOrder\StoreBatchRequest;
use App\Http\Requests\PurchaseOrder\StoreRequest;
use App\Http\Requests\PurchaseOrder\UpdateBatchRequest;
use App\Http\Requests\PurchaseOrder\UpdateRequest;
use App\Http\Requests\PurchaseOrder\ReceiveBatchRequest;
use App\Http\Requests\PurchaseOrder\ReceiveRequest;
use App\Http\Requests\PurchaseOrder\FilterRequest;
use App\Models\ContactInformation;
use App\Models\Customer;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PurchaseOrderCollection;
use App\Http\Resources\PurchaseOrderResource;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderItem;
use App\Models\Webhook;
use App\Models\Warehouse;
use App\Models\Supplier;
use App\Models\ObjectChange;
use Illuminate\Support\Facades\DB;

class PurchaseOrderComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();

        if (!empty($input['shipping_carrier_id'])  && !empty($input['tracking_number'])) {
            $input['tracking_url']  = $this->setTrackingUrl($input);
        }

        $purchaseOrderArr = Arr::except($input, ['purchase_order_items']);

        if (!isset($purchaseOrderArr['shipping_carrier_id']) && isset($purchaseOrderArr['shipping_carrier_wms_id'])) {
            $purchaseOrderArr['shipping_carrier_id'] = $this->getShippingCarrierId($purchaseOrderArr['customer_id'], $purchaseOrderArr['shipping_carrier_wms_id']);
            Arr::forget($purchaseOrderArr, 'shipping_carrier_wms_id');
        }

        if (!isset($purchaseOrderArr['shipping_method_id']) && isset($purchaseOrderArr['shipping_carrier_id']) && isset($purchaseOrderArr['shipping_method_wms_id'])) {
            $purchaseOrderArr['shipping_method_id'] = $this->getShippingMethodId($purchaseOrderArr['shipping_carrier_id'], $purchaseOrderArr['shipping_method_wms_id']);
            Arr::forget($purchaseOrderArr, 'shipping_method_wms_id');
        }

        $purchaseOrderArr = $this->setShippingCarrierAndMethodTitles($purchaseOrderArr);

        $purchaseOrder = PurchaseOrder::create($purchaseOrderArr);

        if (isset($input['purchase_order_items'])) {
            $this->updatePurchaseOrderItems($purchaseOrder, $input['purchase_order_items']);
        }

        $purchaseOrder->refresh();

        $purchaseOrder->calculatePurchaseOrder();

        PurchaseOrderSaved::dispatch($purchaseOrder);

        if ($fireWebhook == true) {
            $this->webhook(new PurchaseOrderResource($purchaseOrder), PurchaseOrder::class, Webhook::OPERATION_TYPE_STORE, $purchaseOrder->customer_id);
        }

        return $purchaseOrder;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, PurchaseOrder::class, PurchaseOrderCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, PurchaseOrder $purchaseOrder, $fireWebhook = true)
    {
        $input = $request->validated();

        if (!empty($input['shipping_carrier_id'])  && !empty($input['tracking_number'])) {
            $input['tracking_url']  = $this->setTrackingUrl($input);
        }

        if (isset($input['purchase_order_items'])) {
            $this->updatePurchaseOrderItems($purchaseOrder, Arr::get($input, 'purchase_order_items'));
        }

        if (!isset($input['shipping_carrier_id']) && isset($input['shipping_carrier_wms_id'])) {
            $input['shipping_carrier_id'] = $this->getShippingCarrierId($input['customer_id'], $input['shipping_carrier_wms_id']);
            Arr::forget($input, 'shipping_carrier_wms_id');
        }

        if (!isset($input['shipping_method_id']) && isset($input['shipping_carrier_id']) && isset($input['shipping_method_wms_id'])) {
            $input['shipping_method_id'] = $this->getShippingMethodId($input['shipping_carrier_id'], $input['shipping_method_wms_id']);
            Arr::forget($input, 'shipping_method_wms_id');
        }

        $input = $this->setShippingCarrierAndMethodTitles($input);

        Arr::forget($input, 'purchase_order_items');
        Arr::forget($input, 'customer_id');

        $purchaseOrder->update($input);

        $purchaseOrder->refresh();

        $purchaseOrder->calculatePurchaseOrder();

        $purchaseOrder->calculatePurchaseOrder();

        PurchaseOrderSaved::dispatch($purchaseOrder);

        if ($fireWebhook == true) {
            $this->webhook(new PurchaseOrderResource($purchaseOrder), PurchaseOrder::class, Webhook::OPERATION_TYPE_UPDATE, $purchaseOrder->customer_id);
        }

        return $purchaseOrder;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $outsideRequest = false;

        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $outsideRequest = isset($record['outside_request']) ? true : false;

            if (isset($record['id']) && $purchaseOrder = PurchaseOrder::where('id', Arr::get($record, 'id'))->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $purchaseOrder, false));
            } else if (isset($record['number']) && $purchaseOrder = PurchaseOrder::where('number', $record['number'])->where('customer_id', Arr::get($record, 'customer_id'))->first()) {
                $updateRequest = UpdateRequest::make($record);
                $responseCollection->add($this->update($updateRequest, $purchaseOrder, false));
            } else {
                $storeRequest = StoreRequest::make($record);
                $responseCollection->add($this->store($storeRequest, false));
            }
        }

        if (!$outsideRequest) {
            $this->batchWebhook($responseCollection, PurchaseOrder::class, PurchaseOrderCollection::class, Webhook::OPERATION_TYPE_UPDATE);
        }

        $responseCollection->transform(function (PurchaseOrder $purchaseOrder, $key) use ($input) {
            if (isset($input[$key]['custom_id'])) {
                return (new PurchaseOrderResource($purchaseOrder))->setCustomId($input[$key]['custom_id']);
            }

            return (new PurchaseOrderResource($purchaseOrder));
        });

        return $responseCollection;
    }

    public function updatePurchaseOrderItems(PurchaseOrder $purchaseOrder, $items)
    {
        foreach ($items as $key => $item) {
            if (!isset($item['purchase_order_item_id']) && empty($item['product_id']) && $purchaseOrderLine = $purchaseOrder->purchaseOrderItems->where('sku', Arr::get($item, 'sku'))->first()) {
                $item = $this->getProductInfo($item);

                $purchaseOrderLine->update($item);
            } else if (!isset($item['purchase_order_item_id']) && empty($item['product_id'])) {
                $item['purchase_order_id'] = $purchaseOrder->id;
                $item['product_id'] = null;

                PurchaseOrderItem::create($item);
            } else if (!isset($item['purchase_order_item_id']) && $purchaseOrderLine = $purchaseOrder->purchaseOrderItems->where('product_id', $item['product_id'])->first()) {
                $item = $this->getProductInfo($item);

                $purchaseOrderLine->update($item);
            } else if (!isset($item['purchase_order_item_id']) && isset($item['product_id']) && $item['quantity'] > 0) {
                $item['purchase_order_id'] = $purchaseOrder->id;
                $item = $this->getProductInfo($item);

                PurchaseOrderItem::create($item);
            } else if (isset($item['purchase_order_item_id']) && $item['quantity'] == 0) {
                PurchaseOrderItem::where('id', $item['purchase_order_item_id'])->update(['quantity' => 0]);
            } else if (isset($item['purchase_order_item_id'])) {
                $purchaseOrderLine = PurchaseOrderItem::find($item['purchase_order_item_id']);
                $item = $this->getProductInfo($item);

                $purchaseOrderLine->update(Arr::except($item, ['purchase_order_item_id']));
            }
        }
    }

    public function getProductInfo($item)
    {
        if (empty($item['product_id']) || $item['product_id'] == 0) {
            $item['product_id'] = null;

            return $item;
        }

        if (!empty($item['product_id']) && ($product = Product::find($item['product_id']))) {
            $item['sku'] = $item['sku'] ?? $product->sku;
            $item['name'] = $item['name'] ?? $product->name;
            $item['price'] = $item['unit_price'] ?? $product->price;
            $item['sell_ahead'] = $item['quantity_sell_ahead'] ?? 0;
            $item['customs_price'] = $product->customs_price ?? 0;
            $item['weight'] = $product->weight;
            $item['width'] = $product->width;
            $item['height'] = $product->height;
            $item['length'] = $product->length;
            $item['country_of_origin'] = $product->country_of_origin;
            $item['barcode'] = $item['barcode'] ?? $product->barcode;
            $item['replacement_value'] = $product->replacement_value;
            $item['product_type'] = $product->product_type;
            $item['hs_code'] = $product->hs_code;
            $item['quantity_received'] = $item['quantity_received'] ?? 0;
        }

        return $item;
    }

    public function destroy(DestroyRequest $request, PurchaseOrder $purchaseOrder, $fireWebhook = true)
    {
        $purchaseOrder->purchaseOrderItems()->delete();

        $purchaseOrder->delete();

        $response = ['id' => $purchaseOrder->id, 'number' => $purchaseOrder->number, 'customer_id' => $purchaseOrder->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, PurchaseOrder::class, Webhook::OPERATION_TYPE_DESTROY, $purchaseOrder->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $purchaseOrder = PurchaseOrder::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $purchaseOrder, false));
        }

        $this->batchWebhook($responseCollection, PurchaseOrder::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }

    public function receive(ReceiveRequest $request, PurchaseOrder $purchaseOrder, PurchaseOrderItem $purchaseOrderItem)
    {
        $input = $request->validated();

        $location = Location::where('id', Arr::get($input, 'location_id'))->first();

        app()->inventoryLog->updateLocationProduct(auth()->user()->id, $purchaseOrderItem->product, Arr::get($input, 'quantity_received'), $purchaseOrder, $location, 'receive');

        $purchaseOrderItem->quantity_received += Arr::get($input, 'quantity_received');
        $purchaseOrderItem->save();

        return $purchaseOrderItem;
    }

    public function receiveBatch(ReceiveBatchRequest $request, PurchaseOrder $purchaseOrder)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $receiveRequest = ReceiveRequest::make($record);
            $purchaseOrderItem = PurchaseOrderItem::find($record['purchase_order_item_id']);
            $responseCollection->add($this->receive($receiveRequest, $purchaseOrder, $purchaseOrderItem));
        }

        return $responseCollection;
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function ($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%')
                        ->orWhere('company_name', 'like', '%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%')
                        ->orWhere('zip', 'like', '%' . $term . '%')
                        ->orWhere('city', 'like', '%' . $term . '%')
                        ->orWhere('phone', 'like', '%' . $term . '%');
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterWarehouses(Request $request, Customer $customer)
    {
        $term = $request->get('term');
        $results = [];
        if ($term) {

            $warehouses = $customer->warehouses()->where('id', '=', $term)->get();

            if ($warehouses->count() > 0) {
                $warehouses = $warehouses;
            } else {
                $warehouses = $customer->warehouses()->whereHas('contactInformation', function ($query) use ($term) {
                    //                // TODO: sanitize term
                    $term = '%' . $term . '%';

                    $query->where('name', 'like', $term)
                        ->orWhere('company_name', 'like', $term)
                        ->orWhere('email', 'like', $term)
                        ->orWhere('zip', 'like', $term)
                        ->orWhere('city', 'like', $term)
                        ->orWhere('phone', 'like', $term);
                })->get();
            }

            foreach ($warehouses as $warehouse) {
                if ($warehouse->count()) {
                    $results[] = [
                        'id' => $warehouse->id,
                        'text' => $warehouse->contactInformation->name
                    ];
                }
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterSuppliers(Request $request, Customer $customer)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {
            $suppliers = $customer->suppliers();

            $term = '%' . $term . '%';

            $suppliers = $suppliers->whereHas('contactInformation', function ($query) use ($term) {
                $query->where('name', 'like', $term)
                    ->orWhere('company_name', 'like', $term)
                    ->orWhere('email', 'like', $term)
                    ->orWhere('zip', 'like', $term)
                    ->orWhere('city', 'like', $term)
                    ->orWhere('phone', 'like', $term);
            })->get();

            foreach ($suppliers as $supplier) {
                if ($supplier->count()) {
                    $results[] = [
                        'id' => $supplier->id,
                        'text' => Arr::get($supplier->contactInformation, 'name')
                    ];
                }
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterProducts(Request $request, Supplier $supplier)
    {

        $term = $request->get('term');
        $results = [];

        if ($term) {

            $products = $supplier->products();
            $term = '%' . $term . '%';

            $products = $products->where(function ($query) use ($term) {
                $query->where('sku', 'like', $term)
                    ->orWhere('name', 'like', $term);
            })
                ->get();

            foreach ($products as $product) {
                $image = '/images/product-placeholder.png';

                if (!empty($product->productImages->first())) {
                    $image = $product->productImages->first()->source;
                }

                $results[] = [
                    'id' => $product->id,
                    'text' => $product->name,
                    'html' => '<div class="search-item d-flex align-items-center"><img class="avatar rounded" src="' . $image . '"><span class="m-0 ml-2 text-default">Name: ' . $product->name . '<br>Sku: ' . $product->sku . '</span></div>',
                    "status" => 'pending',
                    "sku" => $product->sku,
                    "name" => $product->name,
                    'quantity_on_hand' => $product->quantity_on_hand,
                    'quantity_backordered' => $product->quantity_backordered,
                    'ordered' => 0,
                    'received' => 0,
                    'quantity_sell_ahead' => $product->quantity_sell_ahead ?? 0,
                    'unit_price' => $product->price,
                    'total_price' => 0,
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filterLocations(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $term = '%' . $term . '%';

            $locations = Location::where('name', 'like', $term)->get();

            foreach ($locations as $location) {
                $results[] = [
                    'id' => $location->id,
                    'text' => $location->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }

    public function filter(FilterRequest $request, $customerIds)
    {
        $query = PurchaseOrder::query();

        $query->when($request['from_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '>=', $request['from_date_created']);
        });

        $query->when($request['to_date_created'], function ($q) use ($request) {
            return $q->where('created_at', '<=', $request['to_date_created'] . ' 23:59:59');
        });

        $query->when($request['from_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '>=', $request['from_date_updated']);
        });

        $query->when($request['to_date_updated'], function ($q) use ($request) {
            return $q->where('updated_at', '<=', $request['to_date_updated'] . ' 23:59:59');
        });

        $query->when(count($customerIds) > 0, function ($q) use ($customerIds) {
            return $q->whereIn('customer_id', $customerIds);
        });

        $purchaseOrders = $query->paginate();

        return $purchaseOrders;
    }

    public function search($term)
    {
        $customerIds = Auth()->user()->customerIds();

        $purchaseOrdersCollection = PurchaseOrder::query();

        if (!empty($customerIds)) {
            $purchaseOrdersCollection = $purchaseOrdersCollection->whereIn('purchase_orders.customer_id', $customerIds);
        }

        $filters = json_decode($term);

        if ($filters->filterArray ?? false) {
            foreach ($filters->filterArray as $key => $filter) {
                if ($filter->columnName === 'dates_between') {
                    $dates = explode(" ", $filter->value);
                    $from = Arr::get($dates, '0', '');
                    $to = Arr::get($dates, '2', '');

                    $today = Carbon::today()->toDateString();
                    $tomorrow = Carbon::tomorrow()->toDateString();

                    $purchaseOrdersCollection = $purchaseOrdersCollection->whereBetween('purchase_orders.ordered_at', [
                        empty($from)
                            ? Carbon::now()->subDays(14)->toDateString() : date($from),
                        empty($to)
                            ? $tomorrow : Carbon::parse($to)->addDay()->toDate()->format('Y-m-d')
                    ]);

                    unset($filters->filterArray[$key]);
                }

                if ($filter->columnName === 'table_search') {
                    $term = $filter->value ? $filter->value : null;
                    unset($filters->filterArray[$key]);
                }
            }

            $purchaseOrdersCollection = $purchaseOrdersCollection->where(function ($query) use ($filters) {
                foreach ($filters->filterArray as $filter) {
                    if ($filter->columnName !== 'ordering' && !empty($filter->value)) {
                        $query->where($filter->columnName, $filter->value);
                    }
                }
            });
        }

        if ($term) {
            $term = '%' . $term . '%';

            $purchaseOrdersCollection
                ->where(function ($query) use ($term) {
                    $query->whereHas('customer.contactInformation', function ($query) use ($term) {
                        $query->where('name', 'like', $term);
                    })
                        ->orWhereHas('warehouse.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhereHas('supplier.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })

                        ->orWhereHas('warehouse.contactInformation', function ($query) use ($term) {
                            $query->where('name', 'like', $term);
                        })
                        ->orWhereRaw('REPLACE(purchase_orders.status, "_", " ") = "' . str_replace('%', '', $term) . '"')
                        ->orWhere('number', 'like', $term);
                });
        }

        return $purchaseOrdersCollection;
    }

    public function cancel(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrder->cancel();

        $this->webhook(new PurchaseOrderResource($purchaseOrder), PurchaseOrder::class, Webhook::OPERATION_TYPE_UPDATE, $purchaseOrder->customer_id);

        return $purchaseOrder;
    }

    public function autoPending(Request $request, Supplier $supplier)
    {
        $products = $supplier->products;
        $productIds = $products->pluck('id');
        $productQty = [];

        $orderItems = OrderItem::whereIn('product_id', $productIds)
            ->select(
                'product_id',
                DB::raw("sum(order_items.quantity) as quantity"),
            )
            ->leftJoin('orders', 'order_items.order_id', '=', 'orders.id')
            ->where('orders.status', Order::STATUS_PENDING)
            ->groupBy('product_id')
            ->get();

        foreach ($orderItems as $orderItem) {
            $productQty[$orderItem->product_id] = $orderItem->quantity;
        }

        $orderedProductIds = $orderItems->pluck('product_id')->toArray();
        $productList = [];

        foreach ($products as $product) {
            if (!in_array($product->id, $orderedProductIds)) {
                continue;
            }

            $product->ordered = $productQty[$product->id];
            $productList[] = $product;
        }

        return $this->productList($productList);
    }

    public function autoAll(Request $request, Supplier $supplier)
    {
        $products = $supplier->products->where('reorder_amount', '>', 0);

        foreach ($products as $product) {
            $product->ordered = $product->reorder_amount;
        }

        return $this->productList($products);
    }

    public function autoBackordered(Request $request, Supplier $supplier)
    {
        $products = $supplier->products->where('quantity_backordered', '>', 0);

        foreach ($products as $product) {
            $product->ordered = $product->quantity_backordered;
        }

        return $this->productList($products);
    }

    public function searchChangeLog($term, $purchaseOrderId)
    {
        if ($purchaseOrderId) {
            $changeCollection = ObjectChange::where(function ($q) use ($purchaseOrderId) {
                $q->where('object_type', PurchaseOrder::class)->where('object_id', $purchaseOrderId);
            })->orWhere(function ($q) use($purchaseOrderId) {
                $q->where('parent_object_type', PurchaseOrder::class)->where('parent_object_id', $purchaseOrderId);
            });
        } else {
            $changeCollection = ObjectChange::where(function ($q) {
                $q->whereIn('object_type', [PurchaseOrder::class, PurchaseOrderItem::class])->orWhere('parent_object_type', PurchaseOrder::class);
            });
        }

        return $this->filteredChangeLog($term, $changeCollection);
    }
}
