<?php

namespace App\Components;

use App\Http\Requests\InvoiceLine\StoreRequest;
use App\Http\Requests\InvoiceLine\StoreBatchRequest;
use App\Http\Requests\InvoiceLine\UpdateRequest;
use App\Http\Requests\InvoiceLine\UpdateBatchRequest;
use App\Http\Requests\InvoiceLine\DestroyRequest;
use App\Http\Requests\InvoiceLine\DestroyBatchRequest;
use App\Models\InvoiceLine;
use Illuminate\Support\Collection;


class InvoiceLineComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $invoiceLine = InvoiceLine::create($input);

        return $invoiceLine;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request)
    {
        $input = $request->validated();

        if (isset($input['id']) && $input['cost'] == 0) {
            $invoiceLine = InvoiceLine::find($input['id'])->delete();
        }

        if (isset($input['invoice_id']) && $input['cost'] != 0) {
            $invoiceLine =  InvoiceLine::updateOrCreate(
                ['id' => $input['id'] ?? ''],
                ['title' => $input['title'], 'cost' => $input['cost'], 'invoice_id' => $input['invoice_id']]
            );
        }

        return $invoiceLine;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);

            $responseCollection->add($this->update($updateRequest, false));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request = null, InvoiceLine $invoiceLine = null)
    {
        $invoiceLine->delete();

        return ['id' => $invoiceLine->id, 'name' => $invoiceLine->title];
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();
        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $invoiceLine = InvoiceLine::where('id', $record['id'])->first();

            $responseCollection->add($this->destroy($destroyRequest, $invoiceLine));
        }

        return $responseCollection;
    }
}
