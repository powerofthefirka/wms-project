<?php

namespace App\Components;

use App\Http\Requests\WebshipperCredential\StoreBatchRequest;
use App\Http\Requests\WebshipperCredential\StoreRequest;
use App\Http\Requests\WebshipperCredential\UpdateBatchRequest;
use App\Http\Requests\WebshipperCredential\UpdateRequest;
use App\Http\Requests\WebshipperCredential\DestroyBatchRequest;
use App\Http\Requests\WebshipperCredential\DestroyRequest;
use Illuminate\Http\Request;
use App\Models\WebshipperCredential;
use App\Models\Customer;
use Illuminate\Support\Collection;

class WebshipperCredentialComponent extends BaseComponent
{
    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $webshipperCredential = WebshipperCredential::create($input);

        return $webshipperCredential;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest));
        }

        return $responseCollection;
    }

    public function update(UpdateRequest $request, WebshipperCredential $webshipperCredential)
    {
        $input = $request->validated();

        $webshipperCredential->update($input);

        return $webshipperCredential;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $webshipperCredential = WebshipperCredential::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $webshipperCredential));
        }

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, WebshipperCredential $webshipperCredential)
    {
        $webshipperCredential->delete();

        $response = ['id' => $webshipperCredential->id, 'customer_id' => $webshipperCredential->customer_id];

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $webshipperCredential = WebshipperCredential::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $webshipperCredential));
        }

        return $responseCollection;
    }

    public function filterCustomers(Request $request)
    {
        $term = $request->get('term');
        $results = [];

        if ($term) {

            $contactInformation = Customer::where('id', '=', $term)->get();

            if ($contactInformation->count() > 0) {
                $contactInformation = $contactInformation;
            } else {
                $contactInformation = Customer::whereHas('contactInformation', function($query) use ($term) {
                    $query->where('name', 'like', '%' . $term . '%' )
                        ->orWhere('company_name', 'like','%' . $term . '%')
                        ->orWhere('email', 'like',  '%' . $term . '%' )
                        ->orWhere('zip', 'like', '%' . $term . '%' )
                        ->orWhere('city', 'like', '%' . $term . '%' )
                        ->orWhere('phone', 'like', '%' . $term . '%' );
                })->get();
            }

            foreach ($contactInformation as $information) {
                $results[] = [
                    'id' => $information->id,
                    'text' => $information->contactInformation->name
                ];
            }
        }

        return response()->json([
            'results' => $results
        ]);
    }
}