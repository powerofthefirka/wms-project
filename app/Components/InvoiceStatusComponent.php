<?php

namespace App\Components;


use App\Http\Requests\InvoiceStatus\DestroyBatchRequest;
use App\Http\Requests\InvoiceStatus\DestroyRequest;
use App\Http\Requests\InvoiceStatus\StoreBatchRequest;
use App\Http\Requests\InvoiceStatus\StoreRequest;
use App\Http\Requests\InvoiceStatus\UpdateBatchRequest;
use App\Http\Requests\InvoiceStatus\UpdateRequest;
use App\Http\Resources\InvoiceStatusCollection;
use App\Http\Resources\InvoiceStatusResource;
use App\Models\InvoiceStatus;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Webhook;
use Illuminate\Support\Collection;

class InvoiceStatusComponent extends BaseComponent
{
    public function store(StoreRequest $request, $fireWebhook = true)
    {
        $input = $request->validated();

        $invoiceStatus = InvoiceStatus::create($input);

        if ($fireWebhook == true) {
            $this->webhook(new InvoiceStatusResource
                ($invoiceStatus), InvoiceStatus::class, Webhook::OPERATION_TYPE_STORE, $invoiceStatus->customer_id);
        }

        return $invoiceStatus;
    }

    public function storeBatch(StoreBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $storeRequest = StoreRequest::make($record);
            $responseCollection->add($this->store($storeRequest, false));
        }

        $this->batchWebhook($responseCollection, InvoiceStatus::class, InvoiceStatusCollection::class, Webhook::OPERATION_TYPE_STORE);

        return $responseCollection;
    }

    public function update(UpdateRequest $request, InvoiceStatus $invoiceStatus, $fireWebhook = true)
    {
        $input = $request->validated();

        $invoiceStatus->update($input);

        if ($fireWebhook == true) {
            $this->webhook(new InvoiceStatusResource($invoiceStatus), InvoiceStatus::class, Webhook::OPERATION_TYPE_UPDATE, $invoiceStatus->customer_id);
        }

        return $invoiceStatus;
    }

    public function updateBatch(UpdateBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $updateRequest = UpdateRequest::make($record);
            $invoiceStatus = InvoiceStatus::find($record['id']);

            $responseCollection->add($this->update($updateRequest, $invoiceStatus, false));
        }

        $this->batchWebhook($responseCollection, InvoiceStatus::class, InvoiceStatusResource::class, Webhook::OPERATION_TYPE_UPDATE);

        return $responseCollection;
    }

    public function destroy(DestroyRequest $request, InvoiceStatus $invoiceStatus, $fireWebhook = true)
    {
        $invoiceStatus->delete();

        $response = ['id' => $invoiceStatus->id, 'customer_id' => $invoiceStatus->customer_id];

        if ($fireWebhook == true) {
            $this->webhook($response, InvoiceStatus::class, Webhook::OPERATION_TYPE_DESTROY, $invoiceStatus->customer_id);
        }

        return $response;
    }

    public function destroyBatch(DestroyBatchRequest $request)
    {
        $responseCollection = new Collection();

        $input = $request->validated();

        foreach ($input as $record) {
            $destroyRequest = DestroyRequest::make($record);
            $invoiceStatus = InvoiceStatus::find($record['id']);

            $responseCollection->add($this->destroy($destroyRequest, $invoiceStatus, false));
        }

        $this->batchWebhook($responseCollection, InvoiceStatus::class, ResourceCollection::class, Webhook::OPERATION_TYPE_DESTROY);

        return $responseCollection;
    }
}
