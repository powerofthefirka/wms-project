<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\BillItem;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Arr;

class RecurringBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'recurring';
    /**
     * @var Carbon
     */

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $settings = $fee['settings'];
        $start = Carbon::parse($bill['period_start']);
        $end = Carbon::parse($bill['period_end']);

        if(Arr::get($settings, 'charge_periodically', false)) {
            $dateStart = $start;

            $latestBillItem = BillItem::where('billing_fee_id', $fee->id)
                ->whereHas('bill', function($query) use ($bill) {
                    $query->where('customer_id', $bill->customer_id);
                })->orderBy('period_end', 'desc')->first();

            if ($latestBillItem) {
                // if the period end is less than date start, that means days were skipped
                $uncalculatedWeek = Carbon::parse($latestBillItem['period_end'])->lt($dateStart);

                if ($uncalculatedWeek) {
                    $dateStart = $latestBillItem['period_end'];
                }
            } else {
                // no item means that date range (ex.: 2021-01-01 - 2021-01-04) wasn't enough to be a week, month
                $latestBill = Bill::where('billing_profile_id', $bill->billing_profile_id)
                    ->where('customer_id', $bill->customer_id)
                    ->where('period_end', $bill->period_start)
                    ->orderBy('period_end', 'desc')
                    ->first();

                if ($latestBill) {
                    $dateStart = $latestBill->period_start;
                }
            }

            if ($settings['period'] === 'day') {
                foreach (CarbonPeriod::create($start, '1 day', $end->copy()->subDay()) as $dt) {
                    $description = 'Daily charge for the period ' . $dt->format("Y-m-d");
                    $quantity = 1;
                    $periodEnd = $dt->format("Y-m-d");

                    app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd);
                }
            }

            if ($settings['period'] === 'week') {
                $weekArray = [];
                foreach (CarbonPeriod::create($dateStart, '1 day', $end) as $dt) {
                    $weekArray[] = $dt->format('W');
                }

                $weekArray = array_unique($weekArray);
                array_pop($weekArray);

                foreach ($weekArray as $week) {
                    $quantity = 1;
                    $description = 'Weekly charge for ' . $week . ' week.';
                    app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $bill->period_end);
                }
            }

            if ($settings['period'] === 'month') {
                foreach (CarbonPeriod::create($dateStart, '1 month', $end->copy()->subMonth()) as $dt) {
                    $quantity = 1;

                    $periodEnd = $dt->copy()->addMonth()->format("Y-m-d");
                    $description = 'Monthly charge for the period ' . $dt->copy()->format("Y-m-d") . ' - ' . $periodEnd;

                    app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd);
                }
            }
        } else {
            $quantity = 1;
            $description = 'One time charge '
                . Carbon::parse($start)->format('Y-m-d')
                . ' - '
                . Carbon::parse($end)->format('Y-m-d');

            app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $end);
        }
    }
}
