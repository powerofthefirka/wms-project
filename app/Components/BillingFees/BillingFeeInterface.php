<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;

Interface BillingFeeInterface
{
    public function calculate(BillingFee $fee, Bill $bill);
}
