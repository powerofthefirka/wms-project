<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\PurchaseOrder;
use App\Models\User;

class ReceivingByPoBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'receiving_by_po';

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $customerId = $bill->customer_id;
        $from = $bill->period_start;
        $to = $bill->period_end;
        $settings = $fee['settings'];

        $purchaseOrders = PurchaseOrder::where('customer_id', $customerId)
            ->where('status', User::STATUS_CLOSED)
            ->whereBetween('delivered_at', [$from, $to])
            ->get();

        foreach ($purchaseOrders as $order) {
            $description = 'Purchase order ' . $order->number;
            $quantity = 1;
            $settings['purchase_order_id'] = $order->id;

            app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $order->created_at);
        }
    }
}
