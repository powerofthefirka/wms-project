<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\BillItem;
use App\Models\InventoryChange;
use App\Models\Location;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class StorageByLocationBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'storage_by_location';
    public $start;
    public $end;
    public $bill;

    public function makeDateAndLocationArray($start, $end, $locationTypes, $inventoryChangesFilteredArray)
    {
        $dateAndLocationArray= [];
        $dayBefore = null;

        foreach (CarbonPeriod::create($start, '1 day', $end->copy()->subDay())->toArray() as $date) {
            $dateFormatted = $date->copy()->format('Y-m-d');
            $dateArray = array_keys(array_column($inventoryChangesFilteredArray, 'formatted_changed_at'), $dateFormatted);

            if(!empty($dateArray)) {
                $ignoreIds = [];
                $locationsFound = [];

                foreach ($dateArray as $locationArrayKey) {
                    $ignoreIds[] = $inventoryChangesFilteredArray[$locationArrayKey]['location_id'];
                    $locationsFound[] = $inventoryChangesFilteredArray[$locationArrayKey];
                }

                if (!empty($dateAndLocationArray[$dayBefore])) {
                    $dayBeforeArray = $dateAndLocationArray[$dayBefore];

                    foreach ($locationsFound as $locationFound) {
                        $keys = array_keys(array_column($dayBeforeArray, 'location_name'), $locationFound['location_name']);

                        foreach ($keys as $key) {
                            $dayBeforeArray[$key] = $locationFound;
                        }
                    }

                    $dateAndLocationArray[$dateFormatted] = $dayBeforeArray;
                } else {
                    $locations = $this->getMissingLocations($locationTypes, $ignoreIds);

                    $dateAndLocationArray[$dateFormatted] = array_merge($locations, $locationsFound);
                }
            } else {
                if (!empty($dateAndLocationArray[$dayBefore])) {
                    $dateAndLocationArray[$dateFormatted] = $dateAndLocationArray[$dayBefore];
                } else {
                    $locations = $this->getMissingLocations($locationTypes);
                    $dateAndLocationArray[$dateFormatted] = $locations;
                }
            }

            $dayBefore = $date->copy()->format('Y-m-d');
        }

        return $dateAndLocationArray;
    }

    public function getMissingLocations($locationTypes, $ignoreLocations = [])
    {
        $dateAndLocationArray= [];

        $locations = Location::whereIn('location_type_id', $locationTypes)
            ->wherehas('warehouse', function($query) {
                $query->where('customer_id', $this->bill->customer_id);
            })
            ->with('products');

        if (!empty($ignoreLocations)) {
            $locations = $locations->whereNotIn('id', $ignoreLocations);
        }

        $lastInventoryLogs = InventoryChange::
            whereIn('inventory_changes.location_id', $locations->pluck('id'))
            ->with('location')
            ->join(DB::raw("(select
                ic.*,
                (ic.previous_on_hand + ic.change_in_on_hand) as days_sum
                from inventory_changes ic,
                (select id,location_id , max(id) as max_id from inventory_changes ic2 GROUP BY DATE_FORMAT(changed_at, '%Y-%m-%d'), location_id) max_sales
                where ic.id = max_sales.max_id) filtered_changes ON inventory_changes.id = filtered_changes.id"), function(){})
            ->whereDate('inventory_changes.changed_at', '<', $this->start)
            ->orderBy('inventory_changes.changed_at', 'desc')
            ->get()
            ->groupBy('location.name');

        foreach ($lastInventoryLogs as $group) {
            $change = $group->first();

            $dateAndLocationArray[] = [
                'location_type_id' => $change->location->location_type_id,
                'location_id' => $change->location->id,
                'location_name' => $change->location->name,
                'days_sum' => $change->days_sum,
                'changed_at' => $change->changed_at,
            ];
        }

        return $dateAndLocationArray;
    }

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $this->start = $start = Carbon::parse($bill['period_start']);
        $this->end = $end = Carbon::parse($bill['period_end']);
        $this->bill = $bill;

        $dateStart = $start;

        $settings = $fee['settings'];
        $locationTypes = json_decode($settings['location_types']);

        $weekArray = [];
        $monthArray = [];

        $inventoryChangesFiltered =
            InventoryChange::
            join('locations', 'locations.id', '=', 'inventory_changes.location_id')
                ->join('location_types', 'location_types.id', '=', 'locations.location_type_id')
                ->whereIn('locations.location_type_id', $locationTypes)
                ->join('warehouses', 'warehouses.id', '=', 'locations.warehouse_id')
                ->where('warehouses.customer_id', $bill->customer_id)
                ->whereBetween('inventory_changes.changed_at', [$start, $end])
                ->where(DB::raw('inventory_changes.previous_on_hand + inventory_changes.change_in_on_hand'), '>', 0)
                ->groupBy(DB::raw('DATE_FORMAT(changed_at, "%Y-%m-%d")'))
                ->select(
                    'inventory_changes.*',
                    DB::raw(' (inventory_changes.previous_on_hand + inventory_changes.change_in_on_hand) as days_sum'),
                    'locations.name as location_name',
                    DB::raw('DATE_FORMAT(inventory_changes.changed_at, "%Y-%m-%d") as formatted_changed_at'),
                    'location_types.id as location_type_id'
                )
                ->get();

        $dateLocationArray = $this->makeDateAndLocationArray($dateStart, $end, $locationTypes, $inventoryChangesFiltered->toArray());

        $latestBillItem = BillItem::where('billing_fee_id', $fee->id)
            ->whereHas('bill', function($query) use ($bill) {
                $query->where('customer_id', $bill->customer_id);
            })->orderBy('period_end', 'desc')->first();

        if ($latestBillItem) {
            // if the period end is less than date start, that means days were skipped
            $uncalculatedWeek = Carbon::parse($latestBillItem['period_end'])->lt($dateStart);

            if ($uncalculatedWeek) {
                $dateStart = $latestBillItem['period_end'];
            }
        } else {
            // no item means that date range (ex.: 2021-01-01 - 2021-01-04) wasn't enough to be a week, month
            $latestBill = Bill::where('billing_profile_id', $bill->billing_profile_id)
                ->where('customer_id', $bill->customer_id)
                ->where('period_end', $bill->period_start)
                ->orderBy('period_end', 'desc')
                ->first();

            if ($latestBill) {
                $dateStart = $latestBill->period_start;
            }
        }

        if ($settings['period'] === 'day') {
            foreach (CarbonPeriod::create($start, '1 day', $end->copy()->subDay()) as $dt) {
                foreach ($dateLocationArray as $key => $dayItems) {
                    if($dt->format("Y-m-d") == Carbon::parse($key)->format('Y-m-d')) {
                        foreach ($dayItems as $item) {
                            if (isset($item['location_id']) && $item['days_sum'] > 0) {
                                $description = 'Daily charge for the period (Last Time Changed ' . $item['changed_at'] .') '
                                    . $dt->format("Y-m-d") . ', location: '
                                    . $item['location_name'];
                                $quantity = 1;
                                $periodEnd = $dt->format("Y-m-d");
                                $settings['location_type_id'] = $item['location_type_id'];

                                app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd);
                            }
                        }
                    }
                }
            }
        }

        if ($settings['period'] === 'week') {
            foreach ($dateLocationArray as $key => $dayItems) {
                $yearWeek = Carbon::parse($key)->format('W');
                if (!isset($weekArray[$yearWeek])) {
                    if ($item['days_sum'] > 0) {
                        $weekArray[$yearWeek] = [];
                    }
                }

                foreach ($dayItems as $item) {
                    if (isset($item['location_id'])) {
                        $weekArray[$yearWeek][$item['location_id']] = $item;
                    }
                }
            }
            // pops a week off, to make sure not to use values were day selected is in the week
            array_pop($weekArray);

            foreach ($weekArray as $week => $weekItems) {
                if(!empty($weekItems)) {
                    foreach ($weekItems as $item) {
                        $description = 'Weekly charge for (Last Time Changed ' . $item['changed_at'] .') '
                            . ' week '. $week .', location: '
                            . $item['location_name'];
                        $quantity = 1;
                        $settings['location_type_id'] = $item['location_type_id'];

                        app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $bill->period_end);
                    }
                }
            }
        }

        if ($settings['period'] === 'month') {
            foreach ($dateLocationArray as $key => $dayItems) {
                $month = Carbon::parse($key)->format('Y-m');

                if (!isset($monthArray[$month])) {
                    $monthArray[$month] = [];
                }

                foreach ($dayItems as $item) {
                    if (isset($item['location_id'])) {
                        if ($item['days_sum'] > 0) {
                            $monthArray[$month][$item['location_id']] = $item;
                        }
                    }
                }
            }

            $monthsToBillArray = CarbonPeriod::create($dateStart, '1 month', $end->copy()->subMonth())->toArray();
            foreach ($monthsToBillArray as $month) {
                $month = $month->format('Y-m');

                if (isset($monthArray[$month])) {
                    foreach ($monthArray[$month] as $item) {
                        if (!empty($item)) {
                            $periodEnd = Carbon::parse($month)->addMonth()->format("Y-m-d");
                            $settings['location_type_id'] = $item['location_type_id'];
                            $quantity = 1;

                            $description = 'Monthly charge for the period (Last Time Changed ' . $item['changed_at'] .') '
                                . Carbon::parse($month)->format("Y-m-d") . ' - ' . $periodEnd . ', location: '
                                . $item['location_name'];

                            app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $periodEnd);
                        }
                    }
                }
            }
        }
    }
}
