<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\PurchaseOrder;
use App\Models\User;
use Illuminate\Support\Arr;

class ReceivingByLineBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'receiving_by_line';
    public $volumeRate;
    public $weightRate;
    public static $billedItemIds = [];

    public function calculate(BillingFee $fee, Bill $bill)
    {
        // Calculations are made by NOT USING weight or height unit in DB PRODUCT T
        // UNITS ARE ALWAYS LBS/INCHES

        if ($fee->type != $this->feeType) {
            return;
        }

        $customerId = $bill->customer_id;
        $settings = $fee['settings'];
        $productProfiles = json_decode($settings['product_profiles']);
        $this->volumeRate = $settings['volume_rate'];
        $this->weightRate = $settings['weight_rate'];

        $purchaseOrders = PurchaseOrder::where('customer_id', $customerId)
            ->where('status', User::STATUS_CLOSED)
            ->whereBetween('ordered_at', [$bill->period_start, $bill->period_end])
            ->get();

        if (Arr::get($settings, 'if_no_other_fee_applies', false)) {
            foreach ($purchaseOrders as $po) {
                $products = $po->receivedItems()
                    ->whereNotIn('id', self::$billedItemIds)->get();

                foreach ($products as $item) {
                    $this->item($item, $bill, $fee);
                }
            }
        } else {
            foreach ($purchaseOrders as $po) {
                if (Arr::get($settings, 'without_profile', false)) {
                    $productsWithoutProfile = $po->receivedItems()->whereHas('product', function($query) use ($productProfiles) {
                        $query->whereNull('product_profile_id')
                            ->orWhereIn('product_profile_id', $productProfiles);
                    })
                        ->get();

                    foreach ($productsWithoutProfile as $item) {
                        $this->item($item, $bill, $fee);
                    }
                } else {
                    $productsWithProfile = $po->receivedItems()->whereHas('product', function($query) use ($productProfiles) {
                        $query->whereIn('product_profile_id', $productProfiles);
                    })
                        ->get();

                    foreach ($productsWithProfile as $item) {
                        $this->item($item, $bill, $fee);
                    }
                }
            }
        }
    }

    public function description($sku, $weight, $volume)
    {
        return 'SKU ' . $sku . ' (weight: ' . $weight . ' volume: ' . $volume . ')';
    }

    public function item($item, $bill, $fee)
    {
        self::$billedItemIds[] = $item->id;

        $quantity = 1;

        $width = dimensions_unit_conversion($item->width ?? 0, $item->product->dimensions_unit ?? 'inch', Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'));
        $height = dimensions_unit_conversion($item->height ?? 0, $item->product->dimensions_unit ?? 'inch', Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'));
        $length = dimensions_unit_conversion($item->length ?? 0, $item->product->dimensions_unit ?? 'inch', Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'));

        $itemVolume = $width * $height * $length;

        $volumeRate = $this->volumeRate;
        $weightRate = $this->weightRate;

        $totalVolumeCost = $itemVolume * $volumeRate;
        $totalWeightCost = $item->weight * $weightRate;

        $description = $this->description($item->sku, $item->weight, $itemVolume)
            . 'CALCULATIONS -->> '
            . ' | item volume ' . $itemVolume . ' =  ' . $width .' * '. $height .' * '. $length
            . ' | total volume cost ' . $totalVolumeCost . ' = (item volume) ' . $itemVolume . ' * (volume rate)'. $volumeRate
            . ' | total weight cost' . $totalWeightCost . ' = (weight) ' . $item->weight . ' * (weight rate) ' . $weightRate
            . ' | TOTAL: '. ($totalVolumeCost + $totalWeightCost) .' = ' . $totalVolumeCost . ' + ' . $totalWeightCost;

        app()->bill->createBillItem($description, $bill, $fee, [
            'rate' => $totalVolumeCost + $totalWeightCost,
            'purchase_order_item_id' => $item->id
        ], $quantity, $bill->period_end);
    }
}
