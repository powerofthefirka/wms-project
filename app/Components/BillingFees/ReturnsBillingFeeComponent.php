<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Return_;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ReturnsBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'returns';
    public static $billedItemIds = [];

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $from = Carbon::parse($bill['period_start']);
        $to = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;
        $settings = $fee['settings'];
        $periodEnd = $bill->period_end;

        $productProfiles = json_decode($settings['product_profiles'], true);
        $firstOfAdditionalSkus = $settings['first_of_additional_skus'] ?? 0;

        $itemsByProfile = [];
        $itemsListByProfile = [];

        $returns = Return_::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('requested_at', [$from, $to])
            ->where('status', User::STATUS_COMPLETE)
            ->get();

        foreach ($returns as $return) {
            if (Arr::get($settings, 'if_no_other_fee_applies', false)) {
                $returnItems = $return->returnItems()
                    ->whereNotIn('id', self::$billedItemIds)
                    ->get();
            } else {
                $returnItems = $return->returnItems()->whereHas('orderItem.product', function ($query) use ($productProfiles, $settings) {
                    $query->whereIn('product_profile_id', $productProfiles);

                    if(Arr::get($settings, 'without_profile', false)) {
                        $query->orWhereNull('product_profile_id');
                    }
                })
                    ->get();
            }

            foreach ($returnItems as $item) {
                if ($item->quantity_received) {
                    $sku = $item->product->sku;

                    if (empty($item->product->product_profile_id)) {
                        if ($firstOfAdditionalSkus) {
                            $itemsByProfile['no_profile'][] = $item;
                        }

                        $itemsListByProfile['no_profile'][] = array_fill(0, $item->quantity_received, ['sku' => $sku, 'id' => $item->id]);
                    } else {
                        if ($firstOfAdditionalSkus) {
                            $itemsByProfile[$item->product->product_profile_id][] = $item;
                        }

                        $itemsListByProfile[$item->product->product_profile_id][] = array_fill(0, $item->quantity_received, ['sku' => $sku, 'id' => $item->id] );
                    }
                }
            }

            foreach ($itemsListByProfile as $key => $array) {
                $itemsListByProfile[$key] = array_merge_recursive(...$array);
            }
        }

        // FIRST OF ADDITIONAL SKU
        if ($firstOfAdditionalSkus) {
            foreach ($itemsByProfile as $profile) {
                if (count($profile) > 1) {
                    $FASrate = $settings['first_of_additional_skus_rate'];
                    $description = 'First item of aditional SKU';
                    $quantity = 1;

                    app()->bill->createBillItem($description, $bill, $fee, ['rate' => $FASrate], $quantity, $periodEnd);
                }
            }

        }

        //FIRST RATE
        foreach ($itemsListByProfile as $key => $items) {
            $description = 'SKU'.$items[0]['sku'] . ' 1st item of each product profile';
            $quantity = 1;

            app()->bill->createBillItem($description, $bill, $fee, [
                'rate' => $settings['item_rates']['first_item_rate'],
                'return_item_id' => $items[0]['id'],
            ], $quantity, $periodEnd);

            self::$billedItemIds[] = $items[0]['id'];

            unset($itemsListByProfile[$key][0]);
        }

        //ALL RATES
        foreach ($itemsListByProfile as $profile => $profileItems) {
            $oldTo = 1;
            foreach ($settings['item_rates'] as $key => $rate) {
                if ($key != 'first_item_rate' && $key != 'rest_of_the_items') {
                    $length = $rate['to'] - $oldTo;

                    foreach (array_slice($itemsListByProfile[$profile], 0, $length) as $item) {
                        $description = 'SKU'.$item['sku'] .' FOR: ' . ($oldTo + 1) . ' - ' . $rate['to'];
                        $quantity = 1;

                        app()->bill->createBillItem($description, $bill, $fee, [
                            'rate' => $rate['rate'],
                            'return_item_id' => $item['id'],
                        ], $quantity, $periodEnd);

                        self::$billedItemIds[] = $item['id'];
                    }

                    for($i = intval($oldTo); $i < $rate['to']; $i++){
                        unset($itemsListByProfile[$profile][$i]);
                    }

                    $oldTo = $rate['to'] ;
                }
            }
        }

        //REST OF THE RATE
        $restOfTheItemRate = $settings['item_rates']['rest_of_the_items'];

        foreach ($itemsListByProfile as $profile => $items) {
            foreach ($items as $item) {
                $description = 'SKU'.$item['sku'] .' Rest of the items';
                $quantity = 1;

                app()->bill->createBillItem($description, $bill, $fee, [
                    'rate' => $restOfTheItemRate,
                    'return_item_id' => $item['id'],
                ], $quantity, $periodEnd);

                self::$billedItemIds[] = $item['id'];
            }
        }
    }
}
