<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\BillItem;
use App\Models\InventoryChange;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class StorageByProductBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'storage_by_product';

    function makeDateAndProductArray($start, $end, $inventoryChangesFilteredArray)
    {
        $dateAndSumArray = [];
        $productsThatAreEmpty = [];
        $oldValue = 0;

        foreach (CarbonPeriod::create($start, '1 day', $end->copy()->subDay())->toArray() as $date) {
            $dateArray = array_keys(array_column($inventoryChangesFilteredArray, 'date_filtered'), $date->format('Y-m-d'));
            // if collection has item with date
            if(!empty($dateArray)) {
                $dayBeforeArray = $dateAndSumArray[$date->copy()->subDay()->format('Y-m-d')] ?? [];
                // sets day before results on current day, to have full array
                if (!empty($dayBeforeArray)) {
                    $dateAndSumArray[$date->format('Y-m-d')] = $dayBeforeArray;
                }

                // goes thru keys and sets values on updated array
                foreach ($dateArray as $arrayKey) {
                    $result = $inventoryChangesFilteredArray[$arrayKey];
                    $resultDate = $result['date_filtered'];
                    $daySum = $result['days_sum'] == 0 ? $oldValue : $result['days_sum'];
                    //if all products were withdraw, next day the item should be 0
                    if ($result['days_sum'] == 0) {
                        $productsThatAreEmpty[$date->addDay()->format('Y-m-d')][$result['product_id']] = ['id' => $result['id'], 'daySum' => 0 ];
                    }

                    $dateAndSumArray[$resultDate][$result['product_id']] = ['id' => $result['id'], 'daySum' => $daySum ];
                    $oldValue = $result['days_sum'];
                }
            } else {
                if (!empty(array_values($dateAndSumArray) )) {
                    foreach (array_values($dateAndSumArray) as $dayItems) {
                        if (!empty($dayItems)) {
                            //copies array
                            foreach ($dayItems as $key => $item) {
                                $value = $productsThatAreEmpty[$date->format('Y-m-d')][$key] ?? $item;
                                $dateAndSumArray[$date->format('Y-m-d')][$key] =  $value;
                            }
                        } else {
                            //makes it empty, if there is nothing to copy
                            $dateAndSumArray[$date->format('Y-m-d')] =  [];
                        }
                    }
                } else {
                    //first time when there are lines in array
                    $dateAndSumArray[$date->format('Y-m-d')] =  [];
                }

            }
        }

        return $dateAndSumArray;
    }

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $start = Carbon::parse($bill['period_start']);
        $end = Carbon::parse($bill['period_end']);
        $dateStart = $start;

        $settings = $fee['settings'];
        $locationTypes = json_decode($settings['location_types']);
        $productProfiles = json_decode($settings['product_profiles']);

        $columnNameForFiltering = 'changed_at';
        $inventoryChangesFiltered =
            InventoryChange::
            leftJoin('locations', 'locations.id', '=', 'inventory_changes.location_id')
                ->leftJoin('location_types', 'location_types.id', '=', 'locations.location_type_id')
                ->leftJoin('products', 'products.id', '=', 'inventory_changes.product_id')
                ->whereIn('locations.location_type_id', $locationTypes)
                ->whereIn('products.product_profile_id', $productProfiles)
                ->whereBetween('inventory_changes.'.$columnNameForFiltering, [$start, $end])
                ->join(DB::raw("(select
                inventory_changes.*,
                (inventory_changes.previous_on_hand + inventory_changes.change_in_on_hand) as days_sum
                from inventory_changes,
                (select *, max(id) as max_id from inventory_changes group by DATE(".$columnNameForFiltering."), product_id) max_sales
                where inventory_changes.id = max_sales.max_id) filtered_changes ON inventory_changes.id = filtered_changes.id"), function(){})
                ->select(
                   'filtered_changes.*',
                    'products.sku as sku',
                    'products.dimensions_unit as dimensions_unit',
                    'locations.name as location_name',
                    DB::raw("(products.width*products.height*products.length) as product_volume"),
                    DB::raw("Date(filtered_changes.".$columnNameForFiltering.") as date_filtered")
               )
        ->get();

        $inventoryChangesFilteredArray = $inventoryChangesFiltered->toArray();

        $latestBillItem = BillItem::where('billing_fee_id', $fee->id)
            ->whereHas('bill', function($query) use ($bill) {
                $query->where('customer_id', $bill->customer_id);
            })->orderBy('period_end', 'desc')->first();

        if ($latestBillItem) {
            // if the period end is less than date start, that means days were skipped
            $uncalculatedWeek = Carbon::parse($latestBillItem['period_end'])->lt($dateStart);

            if ($uncalculatedWeek) {
                $dateStart = $latestBillItem['period_end'];
            }
        } else {
            // no item means that date range (ex.: 2021-01-01 - 2021-01-04) wasn't enough to be a week, month
            $latestBill = Bill::where('billing_profile_id', $bill->billing_profile_id)
                ->where('customer_id', $bill->customer_id)
                ->where('period_end', $bill->period_start)
                ->orderBy('period_end', 'desc')
                ->first();

            if ($latestBill) {
                $dateStart = $latestBill->period_start;
            }
        }

        $dateAndSumArray = $this->makeDateAndProductArray($start, $end, $inventoryChangesFilteredArray);

        $weekArray = [];
        $monthArray = [];

        if ($settings['period'] === 'day') {
            foreach ($dateAndSumArray as $date => $dateItems) {
                if(!empty($dateItems)) {
                    foreach ($dateItems as $productId => $item) {
                        $productArrayKey = array_search($productId, array_column($inventoryChangesFilteredArray, 'product_id'));
                        $product = $inventoryChangesFilteredArray[$productArrayKey];
                        $quantity = 1;

                        $explodedVolume = explode(' ', $settings['volume_unit']);
                        $productVolume = dimensions_unit_conversion($product['product_volume'], Arr::get($product, 'dimensions_unit') ?? Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'), end($explodedVolume), true);

                        $description = 'SKU '
                            . $product['sku']
                            . ' with a volume of ' . $productVolume
                            . ' in stored in location ' . $product['location_name']
                            . ' day ' . $date;

                        $rate = (($settings['item_rate'] * $item['daySum'])
                            + (($productVolume * $settings['volume_rate']) * $item['daySum'])
                            + $settings['bin_rate']);

                        app()->bill->createBillItem($description, $bill, $fee, [
                            'rate' => $rate,
                            'inventory_change_id' => $item['id']
                        ], $quantity, $date);
                    }
                }
            }
        }

        if ($settings['period'] === 'week') {
            foreach ($dateAndSumArray as $key => $dayItems) {
                $yearWeek = Carbon::parse($key)->format('W');
                foreach ($dayItems as $key => $item) {
                    if (($weekArray[$yearWeek][$key] ?? 0) < $item) {
                        $weekArray[$yearWeek][$key] = $item;
                    };
                }
            }

            // pops a week off, to make sure not to use values were day selected is in the week
            array_pop($weekArray);

            foreach ($weekArray as $week => $weekItems) {
                if(!empty($weekItems)) {
                    foreach ($weekItems as $productId => $item) {
                        $productArrayKey = array_search($productId, array_column($inventoryChangesFilteredArray, 'product_id'));
                        $product = $inventoryChangesFilteredArray[$productArrayKey];
                        $quantity = 1;

                        $explodedVolume = explode(' ', $settings['volume_unit']);
                        $productVolume = dimensions_unit_conversion($product['product_volume'], Arr::get($product, 'dimensions_unit') ?? Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'), end($explodedVolume), true);

                        $description = 'SKU '
                            . $product['sku']
                            . ' with a volume of ' . $productVolume
                            . ' in stored in location ' . $product['location_name']
                            . ' week ' . $week;

                        $rate = ($item['daySum'] * $settings['item_rate'])
                            + (($productVolume * $settings['volume_rate']) * $item['daySum'])
                            + $settings['bin_rate'];

                        app()->bill->createBillItem($description, $bill, $fee, [
                            'rate' => $rate,
                            'inventory_change_id' => $item['id']
                        ], $quantity, $end);
                    }
                }
            }
        }

        if ($settings['period'] === 'month') {
            // data yra menesio
            foreach ($dateAndSumArray as $key => $dayItems) {
                $month = Carbon::parse($key)->format('Y-m');

                foreach ($dayItems as $key => $item) {
                    if (($monthArray[$month][$key] ?? 0) < $item) {
                        $monthArray[$month][$key] = $item;
                    };
                    $monthArray[$month][$key] = $item;
                }
            }

            $monthsToBillArray = CarbonPeriod::create($dateStart, '1 month', $end->copy()->subMonth())->toArray();

            foreach ($monthsToBillArray as $month) {
                $month = $month->format('Y-m');
                $billableMonth = $monthArray[$month] ?? null;

                if(!empty($billableMonth)) {
                    foreach ($billableMonth as $productId => $item) {
                        $productArrayKey = array_search($productId, array_column($inventoryChangesFilteredArray, 'product_id'));
                        $product = $inventoryChangesFilteredArray[$productArrayKey];
                        $quantity = 1;

                        $explodedVolume = explode(' ', $settings['volume_unit']);
                        $productVolume = dimensions_unit_conversion($product['product_volume'], Arr::get($product, 'dimensions_unit') ?? Arr::get($bill->customer, 'dimensions_unit') ?? env('DEFAULT_DIMENSIONS_UNIT'), end($explodedVolume), true);

                        $description = 'SKU '
                            . $product['sku']
                            . ' with a volume of ' . $productVolume
                            . ' in stored in location ' . $product['location_name']
                            . ' month ' . $month;

                        $rate = ($item['daySum'] * $settings['item_rate'])
                            + (($productVolume * $settings['volume_rate']) * $item['daySum'])
                            + $settings['bin_rate'];

                        app()->bill->createBillItem($description, $bill, $fee, [
                            'rate' => $rate,
                            'inventory_change_id' => $item['id']
                        ], $quantity, Carbon::parse($month)->format("Y-m-d"));
                    }
                }
            }
        }
    }
}
