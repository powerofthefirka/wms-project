<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ShipmentsByBoxBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'shipments_by_box';
    public static $billedItemIds = [];

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $from = Carbon::parse($bill['period_start']);
        $to = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;
        $settings = $fee['settings'];
        $shippingBoxes = json_decode($settings['shipping_boxes']);


        $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('shipped_at', [$from, $to])
            ->get();

        if (Arr::get($settings, 'if_no_other_fee_applies', false)) {
            foreach ($shipments as $shipment) {
                $packages = $shipment->packages()
                    ->whereNotIn('id', self::$billedItemIds)->get();

                foreach ($packages as $package) {
                    $this->item($package, $settings, $bill, $fee);
                }
            }
        } else {
            foreach ($shipments as $shipment) {
                $packages = $shipment->packages()->whereIn('shipping_box_id', $shippingBoxes)->get();

                foreach ($packages as $package) {
                    $this->item($package, $settings, $bill, $fee);
                }
            }
        }
    }

    public function item($package, $settings, $bill, $fee)
    {
        self::$billedItemIds[] = $package->id;

        $description = 'Fee for: ' . $package->shippingBox->name . ' price: ' . $settings['rate'];
        $quantity = 1;
        $settings['package_id'] = $package->id;

        app()->bill->createBillItem($description, $bill, $fee, $settings, $quantity, $bill->period_end);
    }
}
