<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Shipment;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class ShippingRateBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'shipping_rates';

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $periodStartDate = Carbon::parse($bill['period_start']);
        $periodEndDate = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;
        $settings = $fee['settings'];

        if (empty(Arr::get($settings, 'shipping_zones'))) {
            return;
        }

        if (empty(Arr::get($settings, 'shipping_method'))) {
            return;
        }

        $carrierId = Arr::get($settings, 'shipping_carrier_id');
        $methodIds = Arr::get($settings, 'shipping_method');

        foreach (Arr::get($settings, 'shipping_zones') as $shippingZone) {
            $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
                $query->where('customer_id', $customerId);
            })->whereHas('contactInformation', function ($query) use ($shippingZone) {
                $query->whereIn('country_id', Arr::get($shippingZone, 'countries', []));
            })->whereHas('packages', function ($query) use ($shippingZone, $carrierId, $methodIds) {
                $query->where('shipping_carrier_id', $carrierId);
                $query->whereIn('shipping_method_id', $methodIds);
            })->whereBetween('shipped_at', [
                $periodStartDate->format('Y-m-d'),
                $periodEndDate->format('Y-m-d')
            ])
                ->with('packages')
                ->get();

            foreach ($shipments as $shipment) {
                $totalPackageWeight = 0;
                foreach (Arr::get($shipment, 'packages') as $package) {
                    if (Arr::get($package, 'status') == 'void') {
                        continue;
                    }

                    if (Arr::get($package, 'shipping_carrier_id') != $carrierId ||
                        !in_array(Arr::get($package, 'shipping_method_id'), $methodIds))
                    {
                        continue;
                    }

                    foreach ($package->packageItems as $packageItem) {
                        $totalPackageWeight += $packageItem->quantity * weight_unit_conversion($packageItem->product->weight, Arr::get($packageItem->product, 'weight_unit', 'lb'), Arr::get($bill->customer,'weight_unit', env('DEFAULT_WEIGHT_UNIT')));
                    }
                }

                foreach (Arr::get($shippingZone, 'shipping_prices') as $shippingPrice) {
                    $weightFrom = weight_unit_conversion(Arr::get($shippingPrice, 'weight_from', 0), Arr::get($shippingPrice, 'weight_unit'), Arr::get($bill->customer,'weight_unit', env('DEFAULT_WEIGHT_UNIT')));
                    $weightTo = weight_unit_conversion(Arr::get($shippingPrice, 'weight_to', 0), Arr::get($shippingPrice, 'weight_unit'), Arr::get($bill->customer,'weight_unit', env('DEFAULT_WEIGHT_UNIT')));

                    if ($totalPackageWeight >= $weightFrom && $totalPackageWeight < $weightTo) {

                        $feeDescription = __($fee->name . ' for matching weight range from ' . Arr::get($shippingPrice, 'weight_from', 0) . ' to ' . Arr::get($shippingPrice, 'weight_to', 0) . ' ' . Arr::get($shippingPrice, 'weight_unit', 'lb')
                            . ' shipment tracking number ' . $shipment->tracking_code ?? 'no tracking code');
                        $quantity = 1;
                        $billSettings = [
                            'rate' => (float) Arr::get($shippingPrice, 'price'),
                            'shipment_id' => $shipment->id
                        ];

                        app()->bill->createBillItem($feeDescription, $bill, $fee, $billSettings, $quantity, $periodEndDate);

                        break;
                    }
                }
            }
        }
    }
}
