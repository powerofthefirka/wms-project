<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ShipmentsByPickingBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'shipments_by_picking_fee';
    public static $billedItemIds = [];

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $from = Carbon::parse($bill['period_start']);
        $to = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;
        $settings = $fee['settings'];
        $periodEnd = $bill->period_end;

        $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('shipped_at', [$from, $to])
            ->with('packages')
            ->get();

        foreach ($shipments as $shipment) {
            $this->createBillItems($shipment, $settings, $bill, $fee, $periodEnd);
        }

        //BASE ORDER PRICE
        if (isset($settings['use_base_order_price']) && $settings['use_base_order_price'] && $settings['base_order_price'] > 0) {
            foreach ($shipments->groupBy('order_id') as $order_id => $shipments) {
                $description = 'Base order price for order number ' . $shipments[0]->order->number;
                $quantity = 1;

                app()->bill->createBillItem($description, $bill, $fee, [
                    'rate' => $settings['base_order_price']
                ], $quantity, $periodEnd);
            }
        }
    }

    public function createBillItems($shipment, $settings, $bill, $fee, $periodEnd)
    {
        $firstOfAdditionalSkus = $settings['first_of_additional_skus'] ?? 0;
        $itemsByProfile = [];
        $itemsListByProfile = [];
        $productProfiles = json_decode($settings['product_profiles'], true);

        foreach ($shipment->packages as $package) {
            if (Arr::get($settings, 'if_no_other_fee_applies', false)) {
                $packageItemsFiltered = $package->packageItems()
                    ->whereNotIn('id', self::$billedItemIds)
                    ->get();
            } else {
                $packageItemsFiltered = $package->packageItems()->whereHas('orderItem.product', function ($query) use ($productProfiles, $settings) {
                    $query->whereIn('product_profile_id', $productProfiles);

                    if(Arr::get($settings, 'without_profile', false)) {
                        $query->orWhereNull('product_profile_id');
                    }
                })->get();
            }

        }

        foreach ($packageItemsFiltered as $item) {
            if ($item->quantity > 0) {
                $sku = $item->orderitem->product->sku;

                if (empty($item->orderItem->product->product_profile_id)) {
                    if ($firstOfAdditionalSkus) {
                        $itemsByProfile['no_profile'][] = $item;
                    }

                    $itemsListByProfile['no_profile'][] = array_fill(0, $item->quantity, ['sku' => $sku, 'id' => $item->id] );
                } else {
                    if ($firstOfAdditionalSkus) {
                        $itemsByProfile[$item->orderItem->product->product_profile_id][] = $item;
                    }

                    $itemsListByProfile[$item->orderItem->product->product_profile_id][] = array_fill(0, $item->quantity, ['sku' => $sku, 'id' => $item->id] );
                }
            }
        }

        foreach ($itemsListByProfile as $profile => $items) {
            $itemsListByProfile[$profile] = array_merge_recursive(...$items);
        }

        // FIRST OF ADDITIONAL SKU
        if ($firstOfAdditionalSkus) {
            foreach ($itemsByProfile as $profile) {
                if (count($profile) > 1) {
                    $FASrate = $settings['first_of_additional_skus_rate'];
                    $description = 'Shipment Number: '. $shipment->number . ' | First item of additional SKU';
                    $quantity = 1;

                    app()->bill->createBillItem($description, $bill, $fee, [
                        'rate' => $FASrate,
                    ], $quantity, $periodEnd);
                }
            }

        }

        //FIRST RATE
        foreach ($itemsListByProfile as $key => $items) {
            $description = 'Shipment Number: '. $shipment->number . ' | SKU'.$items[0]['sku'] . ' 1st item of each product profile';
            $quantity = 1;

            app()->bill->createBillItem($description, $bill, $fee, [
                'rate' => $settings['item_rates']['first_item_rate'],
                'package_item_id' => $items[0]['id'],
            ], $quantity, $periodEnd);

            self::$billedItemIds[] = $items[0]['id'];

            unset($itemsListByProfile[$key][0]);
        }

        //ALL RATES
        foreach ($itemsListByProfile as $profile => $profileItems) {
            $oldTo = 1;
            foreach ($settings['item_rates'] as $key => $rate) {
                if ($key != 'first_item_rate' && $key != 'rest_of_the_items' && !empty($rate['to'])) {
                    $length = $rate['to'] - $oldTo;

                    foreach (array_slice($itemsListByProfile[$profile], 0, $length) as $item) {
                        $description = 'Shipment Number: '. $shipment->number . ' | SKU'.$item['sku'] .' FOR: ' . ($oldTo + 1) . ' - ' . $rate['to'];
                        $quantity = 1;

                        app()->bill->createBillItem($description, $bill, $fee, [
                            'rate' => $rate['rate'],
                            'package_item_id' => $item['id'],
                        ], $quantity, $periodEnd);
                        self::$billedItemIds[] = $item['id'];
                    }

                    for($i = intval($oldTo); $i < $rate['to']; $i++){
                        unset($itemsListByProfile[$profile][$i]);
                    }

                    $oldTo = $rate['to'] ;
                }
            }
        }

        //REST OF THE RATE
        $restOfTheItemRate = $settings['item_rates']['rest_of_the_items'];

        foreach ($itemsListByProfile as $profile => $items) {
            foreach ($items as $item) {
                $description = 'Shipment Number: '. $shipment->number . ' SKU'.$item['sku'] .' Rest of the items';
                $quantity = 1;

                app()->bill->createBillItem($description, $bill, $fee, [
                    'rate' => $restOfTheItemRate,
                    'package_item_id' => $item['id'],
                ], $quantity, $periodEnd);

                self::$billedItemIds[] = $item['id'];
            }
        }
    }
}
