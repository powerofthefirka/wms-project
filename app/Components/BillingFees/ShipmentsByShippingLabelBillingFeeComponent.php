<?php

namespace App\Components\BillingFees;

use App\Models\Bill;
use App\Models\BillingFee;
use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ShipmentsByShippingLabelBillingFeeComponent implements BillingFeeInterface
{
    public $feeType = 'shipments_by_shipping_label';
    public static $billedItemIds = [];

    public function calculate(BillingFee $fee, Bill $bill)
    {
        if ($fee->type != $this->feeType) {
            return;
        }

        $from = Carbon::parse($bill['period_start']);
        $to = Carbon::parse($bill['period_end']);
        $customerId = $bill->customer_id;
        $settings = $fee['settings'];
        $methodsSelected = json_decode($settings['methods_selected'], true);

        $shipments = Shipment::whereHas('order', function ($query) use ($customerId) {
            $query->where('customer_id', $customerId);
        })
            ->whereBetween('shipped_at', [$from, $to])
            ->get();

        if (Arr::get($settings, 'if_no_other_fee_applies', false)) {
            foreach ($shipments as $shipment) {
                $packages = $shipment->packages()
                    ->whereNotIn('id', self::$billedItemIds)->get();

                foreach ($packages as $package) {
                    $this->item($package, $settings, $bill, $fee);
                }
            }
        } else {
            foreach ($shipments as $shipment) {
                foreach ($shipment->packages as $package) {
                    $methodId = $package->shipping_method_id;

                    if ($methodId) {
                        if (count(Arr::get($methodsSelected, $package->shipping_carrier_id, [])) && in_array($methodId, $methodsSelected[$package->shipping_carrier_id]) ) {
                            $this->item($package, $settings, $bill, $fee);
                        }
                    }
                }
            }
        }
    }

    public function item($package, $settings, $bill, $fee)
    {
        self::$billedItemIds[] = $package->id;

        $price = $package->price;
        $baseShippingCost = Arr::get($settings, 'include_base_shipping_cost') ? $settings['base_shipping_cost'] : 0;
        $percentageOfCost = $settings['percentage_of_cost'] / 100;

        $total = $baseShippingCost + ($price * $percentageOfCost);

        $description = 'Shipping label for carrier '
            . $package->shippingCarrier->name . ', method ' . $package->shippingMethod->name
            . 'CALCULATIONS -->> '
            . 'TOTAL ' . $total . ' = ' . ' (shipping cost)' . $baseShippingCost .  ' + (price)' . $price . ' * ' . ' (percentage of cost)' . $percentageOfCost;

        $quantity = 1;

        app()->bill->createBillItem($description, $bill, $fee, ['rate' => $total], $quantity, $bill->period_end);
    }

}
