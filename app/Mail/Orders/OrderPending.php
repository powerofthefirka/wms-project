<?php

namespace App\Mail\Orders;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPending extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $order_id;
    protected $order_number;
    protected $customer_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order_id, $order_number, $customer_name)
    {
        $this->order_id = $order_id;
        $this->order_number = $order_number;
        $this->customer_name = $customer_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.index', [
            'order_id' => $this->order_id,
            'order_number' => $this->order_number,
            'status' => 'pending',
            'name' => $this->customer_name
        ]);
    }
}
