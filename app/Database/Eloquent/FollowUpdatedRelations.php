<?php

namespace App\Database\Eloquent;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Libraries\Decorator;
use App\Events\ObjectChange\RelationUpdated;

trait FollowUpdatedRelations
{
    /**
     * The default error bag.
     *
     * @var string
     */
    protected $updatedRelations = [];

    /**
     * Check if the belongs to many relation has been updated
     * @param  BelongsToMany $relation
     * @param  array         $syncResult Result of the `sync` method call
     * @return boolean
     */
    protected function hasBeenUpdated(BelongsToMany $relation, array $syncResult)
    {
        if (isset($syncResult['attached']) && count($syncResult['attached']) > 0) {
            $this->updatedRelations[$relation->getRelationName()] = true;
            RelationUpdated::dispatch($relation, $syncResult['attached'], $syncResult['detached']);
        } elseif (isset($syncResult['detached']) && count($syncResult['detached']) > 0) {
            $this->updatedRelations[$relation->getRelationName()] = true;
            RelationUpdated::dispatch($relation, $syncResult['attached'], $syncResult['detached']);
        }
    }

    /**
     * Decorate a BelongsToMany to listen to relation update
     * @param  BelongsToMany $relation
     * @return Decorator
     */
    protected function decorateBelongsToMany(BelongsToMany $relation)
    {
        $decorator = new Decorator($relation);
        $decorator->decorate('sync', function ($decorated, $arguments) {
            $updates = call_user_func_array([$decorated, 'sync'], $arguments);
            $this->hasBeenUpdated($decorated, $updates);
            return $updates;
        });

        return $decorator;
    }

    /**
     * Retrieve the list of dirty relations
     * @return array
     */
    public function getDirtyRelations()
    {
        return $this->updatedRelations;
    }
}
