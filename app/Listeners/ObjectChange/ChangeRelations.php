<?php

namespace App\Listeners\ObjectChange;

use App\Events\ObjectChange\UpdateObject;
use App\Models\ObjectChange;
use App\Events\ObjectChange\RelationUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeRelations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateObject  $event
     * @return void
     */
    public function handle(RelationUpdated $event)
    {
        if (empty($event->data->get())) {
            return;
        }

        $modelIds = $event->data->get()->pluck('id')->toArray();
        $foreignModel = $event->data->first();

        $getDirty = [];
        $getOriginal = [];

        if ($foreignModel instanceof \App\Models\Supplier) {
            $getDirty = ['suppliers' => json_encode($modelIds)];

            $oldValues = array_merge($event->detached, $modelIds);
            $oldValues = array_diff($oldValues, $event->attached);

            $getOriginal = ['suppliers' => json_encode($oldValues)];
        }

        $productChange = new ObjectChange();
        $productChange->objectChange($foreignModel->pivot->pivotParent, $getDirty, $getOriginal);
    }
}
