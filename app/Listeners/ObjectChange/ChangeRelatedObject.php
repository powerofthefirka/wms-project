<?php

namespace App\Listeners\ObjectChange;

use App\Events\ObjectChange\UpdateObject;
use App\Models\ObjectChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ObjectChange\RelatedObjectSaved;
use App\Models\Product;
use Illuminate\Support\Arr;

class ChangeRelatedObject
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateObject  $event
     * @return void
     */
    public function handle(RelatedObjectSaved $event)
    {
        if (empty($event->model) || !in_array(route_method(), ['PUT', 'PATCH'])) {
            return;
        }

        if ($event->model instanceof \App\Models\Image && $event->model->object_type != Product::class) {
            return;
        }

        $getDirty = Arr::except($event->model->getDirty(), ['id', 'created_at', 'updated_at', 'order_id', 'return_id', 'purchase_order_id', 'shipment_id']);
        $getOriginal = array_fill_keys(array_keys($getDirty), null);

        $productChange = new ObjectChange();
        $productChange->objectChange($event->model, $getDirty, $getOriginal);
    }
}
