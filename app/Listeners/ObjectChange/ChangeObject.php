<?php

namespace App\Listeners\ObjectChange;

use App\Models\ObjectChange;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ObjectChange\UpdateObject;
use App\Models\Order;
use App\Models\Shipment;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeObject
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateObject  $event
     * @return void
     */
    public function handle(UpdateObject $event)
    {
        if (!in_array(route_method(), ['PUT', 'PATCH'])) {
            return;
        }

        if ($event->model instanceof \App\Models\ContactInformation && !in_array($event->model->object_type, [Order::class, Shipment::class])) {
            return;
        }

        $productChange = new ObjectChange();
        $productChange->objectChange($event->model, $event->model->getDirty(), $event->model->getOriginal());
    }
}
