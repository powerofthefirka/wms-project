<?php

namespace App\Listeners\ObjectChange;

use Illuminate\Support\Arr;
use App\Models\ObjectChange;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ObjectChange\UpdateObject;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ObjectChange\RelatedObjectDelete;
use App\Models\Product;

class DeleteRelatedObject
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateObject  $event
     * @return void
     */
    public function handle(RelatedObjectDelete $event)
    {
        if (empty($event->model)) {
            return;
        }

        if ($event->model instanceof \App\Models\Image && $event->model->object_type != Product::class) {
            return;
        }

        $getOriginal = Arr::except($event->model->getOriginal(), ['id', 'created_at', 'updated_at', 'order_id', 'return_id', 'purchase_order_id', 'shipment_id']);
        $getDirty = array_fill_keys(array_keys($getOriginal), null);

        $productChange = new ObjectChange();
        $productChange->objectChange($event->model, $getDirty, $getOriginal);
    }
}
