<?php

namespace App\Listeners;

use App\Models\Product;
use App\Events\PurchaseOrderSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductPoUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PurchaseOrderSaved  $event
     * @return void
     */
    public function handle(PurchaseOrderSaved $event)
    {
        foreach ($event->purchaseOrder->purchaseOrderItems as $purchaseOrderItem) {
            if (!empty($purchaseOrderItem->product_id)) {
                $purchaseOrderItem->product->calculatePurchaseOrderQuantities();
            }
        }
    }
}
