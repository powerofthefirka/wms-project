<?php

namespace App\Listeners\Webhook;

use App\Events\Webhook\SendWebhook as SendWebhookEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\WebhookServer\WebhookCall;
use App\Models\Webhook;

class SendWebhook implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendWebhook  $event
     * @return void
     */
    public function handle(SendWebhookEvent $event)
    {
        $response = $event->response;
        $customer = $event->customer;
        $objectType = $event->objectType;
        $operation = $event->operation;

        $this->callWebhookJob($response, $customer, $objectType, $operation);
    }

    private function callWebhookJob($response, $customer, $objectType, $operation){       
        $webhooks = Webhook::where('object_type', $objectType)->where('operation', $operation)->where('customer_id', $customer->id)->get();

        foreach ($webhooks as $key => $webhook) {
            $url = $webhook->url;

            WebhookCall::create()
               ->url($url)
               ->payload(['payload' => json_encode($response)])
               ->useSecret($webhook->secret_key)
               ->dispatch();
        }
    }
}
