<?php

namespace App\Listeners;

use App\Models\Shipment;
use App\Models\OrderItem;
use App\Events\OrderShipped;
use App\Models\ShipmentItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderShipped  $event
     * @return void
     */
    public function handle(OrderShipped $event)
    {
        $shipments = Shipment::where('order_id', $event->shipment->order_id)->get();

        $shipmentProduct['quantity'] = [];
        $shipmentProduct['price'] = [];
        $productShipped = [];

        foreach ($shipments as $shipment) {
            foreach ($shipment->shipmentItems as $shipmentItem) {
                $productShipped[$shipmentItem->order_item_id] = empty($productShipped[$shipmentItem->order_item_id]) ? $shipmentItem->quantity : ($productShipped[$shipmentItem->order_item_id] + $shipmentItem->quantity);

                if ($shipment->id == $event->shipment->id) {
                    $shipmentProduct['quantity'][$shipmentItem->order_item_id] = empty($shipmentProduct['quantity'][$shipmentItem->order_item_id]) ? $shipmentItem->quantity : ($shipmentProduct['quantity'][$shipmentItem->order_item_id] + $shipmentItem->quantity);
                }
            }
        }

        $orderItems = OrderItem::where('order_id', $event->shipment->order_id)->get();

        foreach ($orderItems as $orderItem) {
            if (!empty($productShipped[$orderItem->id])) {
                $orderItem->quantity_shipped = $productShipped[$orderItem->id];
                $orderItem->quantity_pending = $orderItem->quantity - $orderItem->quantity_shipped;
                $orderItem->update();

                if (sizeof($shipmentProduct['quantity']) > 0 && ($orderItem->order_id == $event->shipment->order_id)) {
                    $shipmentProduct['price'][$orderItem->id] = $orderItem->price;
                }
            }
        }

        $event->shipment->order->calculateOrder();

        if (sizeof($shipmentProduct['quantity']) > 0) {
            // $this->calculateShipment($event->shipment, $shipmentProduct);
        }
    }

    public function calculateShipment(Shipment $shipment, $shipmentProduct)
    {
        $lineItemTotal = 0;
        $quantityShipped = 0;

        foreach ($shipmentProduct['quantity'] as $product => $quantity) {
            $lineItemTotal += $shipmentProduct['price'][$product] * $quantity;
            $quantityShipped += $quantity;
        }

        $shipment->distinct_items = sizeof($shipmentProduct['quantity']);
        $shipment->quantity_shipped = $quantityShipped;
        $shipment->line_item_total = $lineItemTotal;

        $shipment->update();
    }
}
