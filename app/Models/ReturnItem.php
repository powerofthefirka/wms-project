<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\ObjectChange\RelatedObjectSaved;
use App\Events\ObjectChange\RelatedObjectDelete;
use \Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\ReturnItem
 *
 * @property int $id
 * @property int $return_id
 * @property int $product_id
 * @property float $quantity
 * @property float $quantity_received
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\Return_ $return_
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereQuantityReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereReturnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReturnItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReturnItem withoutTrashed()
 * @mixin \Eloquent
 * @property int $order_item_id
 * @property-read \App\Models\OrderItem $orderItem
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|ReturnItem whereOrderItemId($value)
 */
class ReturnItem extends Model
{
    use RevisionableTrait;

    use SoftDeletes;

    protected $fillable = [
        'return_id',
        'order_item_id',
        'quantity',
        'quantity_received'
    ];

    protected $dispatchesEvents = [
        'created' => RelatedObjectSaved::class,
        'deleting' => RelatedObjectDelete::class,
        'updating' => UpdateObject::class,
    ];

    public function return_()
    {
        return $this->belongsTo(Return_::class, 'return_id', 'id')->withTrashed();
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class)->withTrashed();
    }

    public function product()
    {
        return $this->orderItem->product();
    }
}
