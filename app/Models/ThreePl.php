<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ThreePl extends Model
{
    use SoftDeletes;

    protected $table = '3pls';

    const SETTINGS_HIDDEN_COLUMNS_KEY = '3pls_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'contact_informations.name',
            'title' => 'Name A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.name',
            'title' => 'Name Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.address',
            'title' => 'Address A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.address',
            'title' => 'Address Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.email',
            'title' => 'Email A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.email',
            'title' => 'Email Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.phone',
            'title' => 'Phone 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.phone',
            'title' => 'Phone 9-0',
            'dir' => 'desc'
        ]
    ];

    protected $fillable = [
        'custom_shipping_modal',
        'custom_shipping_modal_text',
        'scheduling_link'
    ];

    public function contactInformation()
    {
        return $this->morphOne(ContactInformation::class, 'object');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, '3pl_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, '3pl_user', '3pl_id', 'user_id')->using(ThreePlUser::class);
    }

    public function pricingPlan()
    {
        return $this->hasOne(ThreePlPricingPlan::class, '3pl_id', 'id');
    }

    public function domains()
    {
        return $this->hasMany(Domain::class, '3pl_id', 'id');
    }

    public function billingProfiles()
    {
        return $this->hasMany(BillingProfile::class, '3pl_id');
    }

    public function shippingCarriers()
    {
        return $this->hasMany(ShippingCarrier::class, '3pl_id');
    }

    public function hasUser($userId)
    {
        return $this->users->contains('id', $userId);
    }

    public function locationTypes()
    {
        return $this->hasMany(LocationType::class, '3pl_id');
    }
}
