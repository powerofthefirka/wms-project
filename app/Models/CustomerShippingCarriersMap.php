<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CustomerShippingCarriersMap
 *
 * @property int $id
 * @property int $customer_id
 * @property string $shipping_carrier_pattern
 * @property string $shipping_service
 * @property int $shipping_service_carrier_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap newQuery()
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingCarriersMap onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereShippingCarrierPattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereShippingService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereShippingServiceCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingCarriersMap whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingCarriersMap withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingCarriersMap withoutTrashed()
 * @mixin \Eloquent
 */
class CustomerShippingCarriersMap extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'customer_id',
        'shipping_carrier_pattern',
        'shipping_service',
        'shipping_service_carrier_id'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shippingCarrierText()
    {
        $pattern = $this->shipping_carrier_pattern;

        $pieces = explode(",", $pattern);

        return trim($pieces[1]);
    }
}
