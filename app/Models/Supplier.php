<?php

namespace App\Models;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Supplier
 *
 * @property int $id
 * @property int $customer_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\ContactInformation $contactInformation
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Supplier onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereContactInformationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Supplier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Supplier withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Supplier withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $currency
 * @property string|null $internal_note
 * @property string|null $default_po_note
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereDefaultPoNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supplier whereInternalNote($value)
 */
class Supplier extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'suppliers_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'contact_informations.name',
            'title' => 'Name A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.name',
            'title' => 'Name Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.address',
            'title' => 'Address A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.address',
            'title' => 'Address Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.email',
            'title' => 'Email A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.email',
            'title' => 'Email Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.phone',
            'title' => 'Phone 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.phone',
            'title' => 'Phone 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'suppliers.currency',
            'title' => 'Currency A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'suppliers.currency',
            'title' => 'Currency Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'suppliers.internal_note',
            'title' => 'Internal Note A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'suppliers.internal_note',
            'title' => 'Internal Note Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'suppliers.default_po_note',
            'title' => 'Default PO Note A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'suppliers.default_po_note',
            'title' => 'Default PO Note Z-A',
            'dir' => 'desc'
        ],
    ];

    protected $cascadeDeletes = [
        'contactInformation'
    ];

    protected $fillable = [
        'customer_id',
        'currency',
        'internal_note',
        'default_po_note'
    ];

    public function contactInformation()
    {
        return $this->morphOne(ContactInformation::class, 'object');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'supplier_product');
    }

    public function getProducts()
    {
        return $this->hasMany(SupplierProduct::class);
    }
}
