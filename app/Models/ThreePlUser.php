<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ThreePlUser extends Pivot
{
    protected $table = '3pl_user';

    protected $fillable = [
        '3pl_id',
        'user_id',
        'active'
    ];
}
