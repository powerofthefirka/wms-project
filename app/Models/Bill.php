<?php

namespace App\Models;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Bill extends Model
{
    use SoftDeletes, CascadeSoftDeletes, RevisionableTrait;

    protected $cascadeDeletes = [
        'billItems'
    ];

    protected $fillable = [
        'customer_id',
        'billing_profile_id',
        'period_start',
        'period_end',
        'due_date',
        'amount'
    ];

    public function billItems()
    {
        return $this->hasMany(BillItem::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function billingProfile()
    {
        return $this->belongsTo(BillingProfile::class, 'billing_profile_id')->withTrashed();
    }

}
