<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tote extends Model
{
    protected $fillable = [
        'name',
        'barcode',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItems()
    {
        return $this->belongsToMany(OrderItem::class);
    }
}
