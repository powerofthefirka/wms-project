<?php

namespace App\Models;

class Domain extends Model
{
    const SETTINGS_HIDDEN_COLUMNS_KEY = 'domains_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'domains.title',
            'title' => 'Title A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'domains.title',
            'title' => 'Title Z-A',
            'dir' => 'desc'
        ]
    ];

    protected $fillable = [
        'domain',
        'title',
        'primary_color',
        'logo_id',
        'favicon_id',
        '3pl_id'
    ];

    public function logo()
    {
        return $this->belongsTo(Image::class, 'logo_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'logo_id', 'id')->withTrashed();
    }

    public function favicon()
    {
        return $this->belongsTo(Image::class, 'favicon_id', 'id')->withTrashed();
    }

    public function threePl()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id', 'id');
    }
}
