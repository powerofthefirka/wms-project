<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CustomerShippingMethodsMap
 *
 * @property int $id
 * @property int $customer_id
 * @property string $shipping_method_pattern
 * @property string $shipping_service
 * @property int $shipping_service_carrier_id
 * @property string $shipping_service_method_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap newQuery()
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingMethodsMap onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereShippingMethodPattern($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereShippingService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereShippingServiceCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereShippingServiceMethodCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerShippingMethodsMap whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingMethodsMap withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CustomerShippingMethodsMap withoutTrashed()
 * @mixin \Eloquent
 */
class CustomerShippingMethodsMap extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'customer_id',
        'shipping_method_pattern',
        'shipping_service',
        'shipping_service_carrier_id',
        'shipping_service_method_code'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shippingMethodText()
    {
        $pattern = $this->shipping_method_pattern;

        $pieces = explode(",", $pattern);

        return trim($pieces[1]);
    }
}
