<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\CustomerUser
 *
 * @property int $id
 * @property int $customer_id
 * @property int $user_id
 * @property int $role_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomerUser whereUserId($value)
 * @mixin \Eloquent
 */
class CustomerUser extends Pivot
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(CustomerUserRole::class);
    }
}
