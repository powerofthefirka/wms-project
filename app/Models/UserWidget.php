<?php

namespace App\Models;


/**
 * App\Models\UserWidget
 *
 * @property int $id
 * @property int $user_id
 * @property string $location
 * @property string $grid_stack_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereGridStackData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserWidget whereUserId($value)
 * @mixin \Eloquent
 */
class UserWidget extends Model
{
    CONST WIDGET_LIST = [
        '[widget_orders_received]' => [
            'view' => 'shared.draggable.widgets.orders_received',
            'title' => 'Orders Received',
        ],
        '[widget_orders_shipped]' => [
            'view' => 'shared.draggable.widgets.orders_shipped',
            'title' => 'Orders Shipped'
        ],
        '[widget_returns]' => [
            'view' => 'shared.draggable.widgets.returns',
            'title' => 'Returns'
        ],
        '[widget_orders_late]' => [
            'view' => 'shared.draggable.widgets.orders_late',
            'title' => 'Late Orders'
        ],
        '[widget_revenue_over_time]' => [
            'view' => 'shared.draggable.widgets.revenue_over_time',
            'title' => 'Revenue Over Time'
        ],
        '[widget_order_by_country]' => [
            'view' => 'shared.draggable.widgets.map',
            'title' => 'Orders By Country'
        ],
        '[widget_average_lead_time_orders]' => [
            'view' => 'shared.draggable.widgets.average_lead_time_orders',
            'title' => 'Average lead time on orders from created to shipped'
        ],
        '[widget_purchase_orders_coming_in]' => [
            'view' => 'shared.draggable.widgets.purchase_orders_coming_in',
            'title' => 'Purchase Orders Received'
        ],
        '[widget_purchase_orders_received]' => [
            'view' => 'shared.draggable.widgets.purchase_orders_received',
            'title' => 'Purchase orders coming in'
        ],
        '[widget_purchase_orders_received_quantity]' => [
            'view' => 'shared.draggable.widgets.purchase_orders_received_quantity',
            'title' => 'Purchase orders received quantity'
        ],

        // orders received

//        '[widget_orders_table]' => 'shared.draggable.widgets.orders_table',
//        '[widget_orders_table_dashboard]' => 'shared.draggable.widgets.orders_table_dashboard',
//        '[widget_revenue]' => 'shared.draggable.widgets.revenue',
//        '[widget_items_shipped]' => 'shared.draggable.widgets.total_items_shipped',
//        '[widget_warehouse_space_used]' => 'shared.draggable.widgets.warehouse_space_used',
//        '[widget_average_lead_time_returns]' => 'shared.draggable.widgets.average_lead_time_returns',
//        '[widget_purchase_orders_coming_in]' => 'shared.draggable.widgets.purchase_orders_coming_in',
//        '[widget_purchase_orders_received]' => 'shared.draggable.widgets.purchase_orders_received',
    ];

    protected $fillable = ['location', 'grid_stack_data', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
