<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use App\Database\Eloquent\FollowUpdatedRelations;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property int $customer_id
 * @property string $sku
 * @property string $name
 * @property float $price
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogs
 * @property-read int|null $inventory_logs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $location
 * @property-read int|null $location_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $orderItem
 * @property-read int|null $order_item_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrderItem[] $purchaseOrderLine
 * @property-read int|null $purchase_order_line_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ShipmentItem[] $shipmentItem
 * @property-read int|null $shipment_item_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $customs_price
 * @property string|null $weight
 * @property string|null $width
 * @property string|null $height
 * @property string|null $length
 * @property string|null $country_of_origin
 * @property int|null $hs_code
 * @property int $quantity_on_hand
 * @property int $quantity_allocated
 * @property int $quantity_available
 * @property int $product_type
 * @property string|null $barcode
 * @property string|null $replacement_value
 * @property int|null $quantity_backordered
 * @property int|null $is_kit
 * @property int|null $quantity_on_po
 * @property int|null $quantity_sell_ahead
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $productImages
 * @property-read int|null $product_images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Supplier[] $suppliers
 * @property-read int|null $suppliers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCountryOfOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCustomsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereHsCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsKit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityAllocated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityBackordered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityOnHand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantityOnPo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantitySellAhead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereReplacementValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereWidth($value)
 */

class Product extends Model
{
    use FollowUpdatedRelations;

    const PRODUCT_TYPE_PICK_AND_PACK = 0;
    const PRODUCT_TYPE_SLAP_A_LABEL = 1;
    const PRODUCT_TYPE_INSERT = 2;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'products_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'products.name',
            'title' => 'Name A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.name',
            'title' => 'Name Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_on_hand',
            'title' => 'On Hand 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_on_hand',
            'title' => 'On Hand 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_available',
            'title' => 'Available 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_available',
            'title' => 'Available 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_allocated',
            'title' => 'Allocated 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_allocated',
            'title' => 'Allocated 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_backordered',
            'title' => 'Backorder 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_backordered',
            'title' => 'Backorder 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_on_po',
            'title' => 'On PO 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_on_po',
            'title' => 'On PO 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.quantity_sell_ahead',
            'title' => 'Sell Ahead 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.quantity_sell_ahead',
            'title' => 'Sell Ahead 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.sku',
            'title' => 'Sku 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.sku',
            'title' => 'Sku 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.barcode',
            'title' => 'Barcode 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.barcode',
            'title' => 'Barcode 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'warehouses',
            'title' => 'Warehouses A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'warehouses',
            'title' => 'Warehouses Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.weight',
            'title' => 'Weight 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.weight',
            'title' => 'Weight 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.is_kit',
            'title' => 'Kit A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.is_kit',
            'title' => 'Kit Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.price',
            'title' => 'Price 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.price',
            'title' => 'Price 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.replacement_value',
            'title' => 'Value 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.replacement_value',
            'title' => 'Value 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.notes',
            'title' => 'Product note A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.notes',
            'title' => 'Product note Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.country_of_origin',
            'title' => 'Country of manufacture A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.country_of_origin',
            'title' => 'Country of manufacture Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'products.hs_code',
            'title' => 'Tariff Code A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'products.hs_code',
            'title' => 'Tariff Code Z-A',
            'dir' => 'desc'
        ],
    ];

    const EXPORTABLE_COLUMNS = [
        [
            'column' => 'name',
            'title' => 'Name'
        ],
        [
            'column' => 'sku',
            'title' => 'Sku'
        ],
        [
            'column' => 'customer_id',
            'title' => 'Customer ID'
        ],
        [
            'column' => 'product_type',
            'title' => 'Product Type'
        ],
        [
            'column' => 'price',
            'title' => 'Price'
        ],
        [
            'column' => 'customs_price',
            'title' => 'Customs Price'
        ],
        [
            'column' => 'weight',
            'title' => 'Weight'
        ],
        [
            'column' => 'width',
            'title' => 'Width'
        ],
        [
            'column' => 'height',
            'title' => 'Height'
        ],
        [
            'column' => 'length',
            'title' => 'Length'
        ],
        [
            'column' => 'country_of_origin',
            'title' => 'Country of manufacture'
        ],
        [
            'column' => 'hs_code',
            'title' => 'Tariff Code'
        ],
        [
            'column' => 'notes',
            'title' => 'Notes'
        ],
        [
            'column' => 'quantity_on_hand',
            'title' => 'On Hand'
        ],
        [
            'column' => 'quantity_available',
            'title' => 'Available'
        ],
        [
            'column' => 'quantity_allocated',
            'title' => 'Allocated'
        ],
        [
            'column' => 'quantity_backordered',
            'title' => 'Backorder'
        ],
        [
            'column' => 'quantity_on_po',
            'title' => 'On PO'
        ],
        [
            'column' => 'quantity_sell_ahead',
            'title' => 'Sell Ahead'
        ],
        [
            'column' => 'barcode',
            'title' => 'Barcode'
        ],
        [
            'column' => 'is_kit',
            'title' => 'Kit'
        ],
        [
            'column' => 'replacement_value',
            'title' => 'Value'
        ],
    ];

    use RevisionableTrait;
    use SoftDeletes;

    protected $fillable = [
        'sku',
        'name',
        'price',
        'customs_price',
        'weight',
        'width',
        'height',
        'length',
        'country_of_origin',
        'hs_code',
        'notes',
        'customer_id',
        'quantity_on_hand',
        'quantity_allocated',
        'quantity_available',
        'quantity_backordered',
        'product_type',
        'barcode',
        'replacement_value',
        'is_kit',
        'reorder_amount',
        'weight_unit',
        'dimensions_unit'
    ];

    protected $dispatchesEvents = [
        'updating' => UpdateObject::class,
    ];

    public function setProductTypeAttribute($type)
    {
        $this->attributes['product_type'] = $type ?? self::PRODUCT_TYPE_PICK_AND_PACK;
    }

    public function purchaseOrderLine()
    {
        return $this->hasMany(PurchaseOrderItem::class);
    }

    public function shipmentItem()
    {
        return $this->hasMany(ShipmentItem::class);
    }

    public function orderItem()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function location()
    {
        return $this->belongsToMany(Location::class)->using(LocationProduct::class)->withPivot(['quantity_on_hand']);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function inventoryLogs()
    {
        return $this->hasMany(InventoryLog::class);
    }

    public function productImages()
    {
        return $this->morphMany(Image::class, 'object');
    }

    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class, 'supplier_product');
    }

    public function supplierProduct()
    {
        return $this->decorateBelongsToMany(
            $this->belongsToMany(Supplier::class, 'supplier_product')
        );
    }

    public function productProfile()
    {
        return $this->belongsTo(ProductProfile::class, 'product_profile_id', 'id');
    }

    public function calculatePurchaseOrderQuantities()
    {
        $purchaseOrders = PurchaseOrder::whereNotIn('status', ['closed', 'cancelled'])
            ->with(['purchaseOrderItems' => function ($query) {
                $query->where('product_id', $this->id);
            }])
            ->get();

        $productPoQty = 0;
        $productSellAhead = 0;

        foreach ($purchaseOrders as $purchaseOrder) {
            foreach ($purchaseOrder->purchaseOrderItems as $purchaseOrderItem) {
                if ($purchaseOrderItem->product_id == $this->id) {
                    $productPoQty += $purchaseOrderItem->quantity;
                    $productSellAhead += $purchaseOrderItem->sell_ahead;
                }
            }
        }

        $this->quantity_on_po = $productPoQty;
        $this->quantity_sell_ahead = $productSellAhead;

        $this->update();
    }

    public function totes()
    {
        return $this->belongsToMany(Tote::class);
    }
}
