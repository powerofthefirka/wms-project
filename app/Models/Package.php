<?php

namespace App\Models;

use App\Components\BaseComponent;
use App\Models\Order;
use App\Models\Shipment;
use Illuminate\Support\Arr;
use App\Http\Requests\ShippingBox\StoreRequest as ShippingBoxRequest;

/**
 * App\Models\Package
 *
 * @property int $shipping_box_id
 * @property int $shipment_id
 * @property string|null $tracking_url
 * @property string|null $tracking_number
 * @method static \Illuminate\Database\Eloquent\Builder|Package newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package query()
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereShippingBoxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereShipmentId($value)
 * @mixin \Eloquent
 */
class Package extends Model
{
    protected $fillable = [
        'shipping_box_id',
        'shipment_id',
        'tracking_url',
        'tracking_number',
        'shipping_carrier_id',
        'shipping_method_id',
        'price',
        'status'
    ];

    public function packageItems()
    {
        return $this->hasMany(PackageItem::class);
    }

    public function shippingCarrier()
    {
        return $this->belongsTo(ShippingCarrier::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class);
    }

    public function shippingBox()
    {
        return $this->belongsTo(ShippingBox::class);
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function shipmentPackages(Order $order, Shipment $shipment, $shippingBoxes)
    {
        foreach ($shippingBoxes as $row) {
            $shippingBox = ShippingBox::where('customer_id', $order->customer_id)->where('wms_id', $row['shipping_box_wms_id'])->first();

            if (empty($shippingBox)) {
                $shippingBoxRequest = ShippingBoxRequest::make([
                    'customer_id' => $order->customer_id,
                    'name' => $row['shipping_box_name'],
                    'wms_id' => $row['shipping_box_wms_id']
                ]);

                $shippingBox = app()->shippingBox->store($shippingBoxRequest);
            }

            $this->savePackageAndPackageItems($order, $shippingBox, $shipment, $row);
        }

        return $shipment;
    }

    public function savePackageAndPackageItems($order, $shippingBox, $shipment, $row)
    {
        $baseComponent = new BaseComponent();
        $shippingCarrierId = isset($row['shipping_carrier_wms_id']) ? $baseComponent->getShippingCarrierId($order->customer_id, $row['shipping_carrier_wms_id']) : null;
        $shippingMethodId = isset($row['shipping_method_wms_id']) ? $baseComponent->getShippingMethodId($shippingCarrierId, $row['shipping_method_wms_id']) : null;

        $package = self::create([
            'shipping_box_id' => $shippingBox->id,
            'shipment_id' => $shipment->id,
            'tracking_url' => Arr::get($row, 'tracking_url', null),
            'tracking_number' => Arr::get($row, 'tracking_number', null),
            'shipping_carrier_id' => $shippingCarrierId,
            'shipping_method_id' => $shippingMethodId,
            'price' => Arr::get($row, 'price', 0),
            'status' => Arr::get($row, 'status', null),
        ]);

        if (!empty($row['items'])) {
            foreach ($row['items'] as $record) {
                PackageItem::create([
                    'package_id' => $package->id,
                    'order_item_id' => Arr::get($record, 'order_item_id'),
                    'quantity' => Arr::get($record, 'quantity')
                ]);
            }
        }
    }
}
