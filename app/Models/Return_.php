<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * App\Models\Return_
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogSources
 * @property-read int|null $inventory_log_sources_count
 * @property-read \App\Models\Order $order
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Return_ newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Return_ newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Return_ onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Return_ query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Return_ withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Return_ withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $order_id
 * @property string $number
 * @property \Illuminate\Support\Carbon|null $requested_at
 * @property string $reason
 * @property int|null $shipping_carrier_id
 * @property int|null $shipping_method_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $shipping_carrier_title
 * @property string|null $shipping_method_title
 * @property string|null $status
 * @property int|null $warehouse_id
 * @property string|null $weight
 * @property string|null $width
 * @property string|null $height
 * @property string|null $length
 * @property int $create_label
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReturnItem[] $returnItems
 * @property-read int|null $return_items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\ShippingCarrier|null $shippingCarrier
 * @property-read \App\Models\ShippingMethod|null $shippingMethod
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \App\Models\Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereCreateLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereRequestedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereShippingCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereShippingCarrierTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereShippingMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereShippingMethodTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereWarehouseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Return_ whereWidth($value)
 */
class Return_ extends Model
{
    use SoftDeletes, CascadeSoftDeletes, RevisionableTrait;

    protected $table = 'returns';

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'return_table_hide_columns';

    const DEFAULT_IMAGE = '/images/product-placeholder.png';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'returns.number',
            'title' => 'Number 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'returns.number',
            'title' => 'Number 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'orders.number',
            'title' => 'Order Number 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'orders.number',
            'title' => 'Order Number 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.name',
            'title' => 'Customer Name 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.name',
            'title' => 'Customer Name 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'returns.requested_at',
            'title' => 'Requested At (oldest first)',
            'dir' => 'asc'
        ],
        [
            'column' => 'returns.requested_at',
            'title' => 'Requested At 9-0 (newest first)',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City At A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.city',
            'title' => 'City At Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'contact_informations.zip',
            'title' => 'Zip Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'returns.status',
            'title' => 'Status A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'returns.status',
            'title' => 'Status Z-A',
            'dir' => 'desc'
        ]
    ];

    const EXPORTABLE_COLUMNS = [
        [
            'column' => 'returns.number as return_number',
            'title' => 'Return Number'
        ],
        [
            'column' => 'orders.number as order_number',
            'title' => 'Order Number'
        ],
        [
            'column' => 'orders.customer_id',
            'title' => 'Customer ID'
        ],
        [
            'column' => 'returns.requested_at',
            'title' => 'Requested Date'
        ],
        [
            'column' => 'returns.status',
            'title' => 'Status'
        ],
        [
            'column' => 'returns.reason',
            'title' => 'Reason'
        ],
        [
            "column" => "returns.shipping_carrier_title",
            "title" => "Shipping Carrier"
        ],
        [
            "column" => "returns.shipping_method_title",
            "title" => "Shipping Method"
        ],
        [
            "column" => "warehouse_contact_information.name as warehouse_name",
            "title" => "Warehouse"
        ],
        [
            'column' => 'returns.weight',
            'title' => 'Weight'
        ],
        [
            'column' => 'returns.width',
            'title' => 'Width'
        ],
        [
            'column' => 'returns.height',
            'title' => 'Height'
        ],
        [
            'column' => 'returns.length',
            'title' => 'Length'
        ],
        [
            'column' => 'returns.create_label',
            'title' => 'Create Label'
        ],
        [
            'column' => 'order_items.sku',
            'title' => 'Product Sku'
        ],
        [
            'column' => 'return_items.quantity',
            'title' => 'Quantity'
        ],
        [
            'column' => 'return_items.quantity_received',
            'title' => 'Quantity Received'
        ],
    ];

    const STATUS_ICONS = [
        'pending' => 'sts-pending',
        'warehouse_complete' => 'sts-fulfilled',
        'complete' => 'sts-on-hold',
        'all' => 'sts-default',
    ];

    protected $cascadeDeletes = [
        'returnItems'
    ];

    protected $fillable = [
        'order_id',
        'number',
        'requested_at',
        'reason',
        'shipping_carrier_id',
        'shipping_method_id',
        'shipping_carrier_title',
        'shipping_method_title',
        'status',
        'warehouse_id',
        'weight',
        'width',
        'height',
        'length',
        'create_label'
    ];

    protected $dates = [
        'requested_at'
    ];

    protected $attributes = [
        'number' => 'Pending RMA',
        'status' => 'pending',
    ];

    protected $currentDateAttributes = [
        'requested_at'
    ];

    protected $dispatchesEvents = [
        'updating' => UpdateObject::class,
    ];

    public function setShippingMethodIdAttribute($value)
    {
        $this->attributes['shipping_method_id'] = empty($value) ? null : $value;
    }
    public function setShippingCarrierIdAttribute($value)
    {
        $this->attributes['shipping_carrier_id'] = empty($value) ? null : $value;
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->withTrashed();
    }

    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }

    public function returnItems()
    {
        return $this->hasMany(ReturnItem::class, 'return_id', 'id');
    }

    public function inventoryLogSources()
    {
        return $this->morphMany(InventoryLog::class, 'source');
    }

    public function tasks()
    {
        return $this->morphMany(Task::class, 'taskable');
    }

    public function shippingCarrier()
    {
        return $this->belongsTo(ShippingCarrier::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class);
    }
}
