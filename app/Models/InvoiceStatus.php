<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\InvoiceStatus
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus newQuery()
 * @method static \Illuminate\Database\Query\Builder|InvoiceStatus onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|InvoiceStatus withTrashed()
 * @method static \Illuminate\Database\Query\Builder|InvoiceStatus withoutTrashed()
 * @mixin \Eloquent
 */
class InvoiceStatus extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'customer_id',
        'name'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
