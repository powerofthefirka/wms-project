<?php

namespace App\Models;

/**
 * App\Models\PackageItem
 *
 * @property int $id
 * @property int $package_id
 * @property float $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Package $package
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PackageItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereQuantityShipped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PackageItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PackageItem withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $order_item_id
 * @property-read \App\Models\OrderItem|null $orderItem
 * @method static \Illuminate\Database\Eloquent\Builder|PackageItem whereOrderItemId($value)
 */
class PackageItem extends Model
{
    protected $fillable = [
        'package_id',
        'order_item_id',
        'quantity'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class)->withTrashed();
    }

    public function product()
    {
        return $this->orderItem->product();
    }
}
