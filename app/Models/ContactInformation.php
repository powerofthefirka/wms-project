<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ContactInformation
 *
 * @property int $id
 * @property string $name
 * @property string|null $company_name
 * @property string $address
 * @property string|null $address2
 * @property string $zip
 * @property string $country_id
 * @property string $city
 * @property string $email
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $billingOrders
 * @property-read int|null $billing_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $customers
 * @property-read int|null $customers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $shippingOrders
 * @property-read int|null $shipping_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Supplier[] $suppliers
 * @property-read int|null $suppliers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Warehouse[] $warehouses
 * @property-read int|null $warehouses_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ContactInformation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactInformation whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ContactInformation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ContactInformation withoutTrashed()
 * @mixin \Eloquent
 * @property string $object_type
 * @property int $object_id
 * @property-read \App\Models\Order|null $billingOrder
 * @property-read \App\Models\Country|null $country
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $object
 * @property-read \App\Models\Order|null $shippingOrder
 * @method static \Illuminate\Database\Eloquent\Builder|ContactInformation whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactInformation whereObjectType($value)
 */
class ContactInformation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'company_name',
        'address',
        'address2',
        'zip',
        'country_id',
        'city',
        'email',
        'phone',
        'shared_folder'
    ];

    protected $table = 'contact_informations';

    protected $dispatchesEvents = [
        'updating' => UpdateObject::class,
    ];

    public function object()
    {
        return $this->morphTo();
    }

    public function shippingOrder()
    {
        return $this->hasOne(Order::class, 'shipping_contact_information_id', 'id');
    }

    public function billingOrder()
    {
        return $this->hasOne(Order::class, 'billing_profile_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}
