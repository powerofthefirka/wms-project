<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class PricingPlan extends Model
{
    use SoftDeletes;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'pricing_plan_table_hide_columns';

    protected $fillable = [
        'name',
        'monthly_price_for_yearly_billing_period',
        'monthly_price_for_monthly_billing_period',
        'initial_fee',
        'three_pl_billing_fee',
        'number_of_monthly_orders',
        'price_per_additional_order'
    ];
}
