<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ObjectChange extends Model
{
    protected $fillable = [
        'column',
        'old_value',
        'new_value'
    ];

    public function object()
    {
        return $this->morphTo();
    }

    public function objectChange($model, $columns, $modelOld)
    {
        $parent = $this->getParent($model);

        foreach ($columns as $key => $value) {
            if (!$this->trackingCondition($model, $modelOld, $key, $value)) {
                continue;
            }

            $productChange = new self;
            $productChange->user_id = auth()->user()->id;
            $productChange->column = $key;
            $productChange->old_value = $modelOld[$key];
            $productChange->new_value = $value;
            $productChange->parent_object_type = $parent[0];
            $productChange->parent_object_id = $parent[1];
            $productChange->parent_object_column = Arr::get($parent, 2, null);
            $productChange->object()->associate($model);
            $productChange->save();
        }
    }

    public function getParent($model)
    {
        if ($model instanceof \App\Models\OrderItem) {
            return [Order::class, $model->order->id];
        } else if ($model instanceof \App\Models\ReturnItem) {
            return [Return_::class, $model->return_->id];
        } else if ($model instanceof \App\Models\PurchaseOrderItem) {
            return [PurchaseOrder::class, $model->purchaseOrder->id];
        } else if ($model instanceof \App\Models\ShipmentItem) {
            return [Shipment::class, $model->shipment->id];
        } else if ($model instanceof \App\Models\ContactInformation) {
            if ($model->object_type == Order::class) {
                $parentObjectColumn = $model->object->shipping_contact_information_id == $model->id ? 'shipping_contact_information_id' : 'billing_contact_information_id';
                return [$model->object_type, $model->object_id, $parentObjectColumn];
            }

            return [$model->object_type, $model->object_id];
        } else if ($model instanceof \App\Models\Image) {
            return [$model->object_type, $model->object_id];
        }

        return [null, null];
    }

    public function trackingCondition($model, $modelOld, $key, $value)
    {
        if (in_array($key, ['id', 'created_at', 'updated_at', 'deleted_at']) || !in_array($key, array_keys($modelOld)) || ($modelOld[$key] == $value)) {
            return false;
        }

        if ($model instanceof \App\Models\Product && in_array($key, ['quantity_on_po', 'quantity_sell_ahead'])) {
            return false;
        }

        if ($model instanceof \App\Models\Image && in_array($key, ['object_id', 'object_type'])) {
            return false;
        }

        return true;
    }
}
