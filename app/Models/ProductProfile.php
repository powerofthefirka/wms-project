<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\This;

class ProductProfile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'customer_id'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    protected static function boot()
    {
        static::deleting(function (ProductProfile $productProfile) {
            $products = $productProfile->products;

            if (!empty($products)) {
                foreach ($products as $product) {
                    $product->product_profile_id = null;
                    $product->save();
                }
            }
        });

        parent::boot();
    }
}
