<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\ObjectChange\RelatedObjectSaved;
use App\Events\ObjectChange\RelatedObjectDelete;
use \Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\PurchaseOrderItem
 *
 * @property int $id
 * @property int $purchase_order_id
 * @property int $product_id
 * @property float $quantity
 * @property float $quantity_received
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\PurchaseOrder $purchaseOrder
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrderItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem wherePurchaseOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereQuantityReceived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrderItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrderItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrderItem withoutTrashed()
 * @mixin \Eloquent
 * @property string $sku
 * @property string $name
 * @property string $price
 * @property string $customs_price
 * @property string|null $weight
 * @property string|null $width
 * @property string|null $height
 * @property string|null $length
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $replacement_value
 * @property int $product_type
 * @property int|null $hs_code
 * @property string|null $sell_ahead
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereCountryOfOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereCustomsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereHsCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereReplacementValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereSellAhead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderItem whereWidth($value)
 */
class PurchaseOrderItem extends Model
{
    use RevisionableTrait;

    use SoftDeletes;

    protected $fillable = [
        'purchase_order_id',
        'product_id',
        'quantity',
        'quantity_received',
        'sku',
        'name',
        'price',
        'customs_price',
        'weight',
        'width',
        'height',
        'length',
        'country_of_origin',
        'barcode',
        'replacement_value',
        'product_type',
        'sell_ahead',
        'hs_code'
    ];

    protected $dispatchesEvents = [
        'created' => RelatedObjectSaved::class,
        'updating' => UpdateObject::class,
        'deleting' => RelatedObjectDelete::class,
    ];

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class)->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }
}
