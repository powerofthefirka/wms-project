<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\WebshipperCredential
 *
 * @property int $id
 * @property int $customer_id
 * @property string $api_base_url
 * @property string $api_key
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential newQuery()
 * @method static \Illuminate\Database\Query\Builder|WebshipperCredential onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential query()
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereApiBaseUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WebshipperCredential whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|WebshipperCredential withTrashed()
 * @method static \Illuminate\Database\Query\Builder|WebshipperCredential withoutTrashed()
 * @mixin \Eloquent
 */
class WebshipperCredential extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'customer_id',
        'api_base_url',
        'api_key'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
