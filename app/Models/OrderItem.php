<?php

namespace App\Models;

use App\Events\ObjectChange\RelatedObjectDelete;
use App\Events\ObjectChange\UpdateObject;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\ObjectChange\RelatedObjectSaved;
use \Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\OrderItem
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property float $quantity
 * @property float $quantity_shipped
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\Product $product
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereQuantityShipped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem withoutTrashed()
 * @mixin \Eloquent
 * @property string $sku
 * @property string $name
 * @property string $price
 * @property string $customs_price
 * @property string|null $weight
 * @property string|null $width
 * @property string|null $height
 * @property string|null $length
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $replacement_value
 * @property int $product_type
 * @property int|null $hs_code
 * @property float|null $quantity_backordered
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereCountryOfOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereCustomsPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereHsCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereQuantityBackordered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereReplacementValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereWidth($value)
 */
class OrderItem extends Model
{
    use RevisionableTrait;

    use SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'quantity_shipped',
        'quantity_backordered',
        'sku',
        'name',
        'price',
        'customs_price',
        'weight',
        'width',
        'height',
        'length',
        'country_of_origin',
        'barcode',
        'replacement_value',
        'product_type',
        'quantity_pending',
        'hs_code'
    ];

    protected $attributes = [
        'quantity_shipped' => 0
    ];

    protected $dispatchesEvents = [
        'created' => RelatedObjectSaved::class,
        'updating' => UpdateObject::class,
        'deleting' => RelatedObjectDelete::class,
    ];

    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }

    public function totes()
    {
        return $this->belongsToMany(Tote::class, 'order_item_tote',
            'order_item_id', 'totes_id');
    }
}
