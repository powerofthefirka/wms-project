<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CustomerShippingMethod extends Pivot
{
    protected $table = 'customer_shipping_methods';

    protected $fillable = [
        'customer_id',
        'shipping_method_id',
    ];
}
