<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\InvoiceLine
 *
 * @property int $id
 * @property int $invoice_id
 * @property string $title
 * @property string $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Invoice $invoice
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine newQuery()
 * @method static \Illuminate\Database\Query\Builder|InvoiceLine onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceLine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|InvoiceLine withTrashed()
 * @method static \Illuminate\Database\Query\Builder|InvoiceLine withoutTrashed()
 * @mixin \Eloquent
 */
class InvoiceLine extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'invoice_id',
        'title',
        'cost',
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

}
