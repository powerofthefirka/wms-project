<?php

namespace App\Models;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Location
 *
 * @property int $id
 * @property int $warehouse_id
 * @property string $name
 * @property int $pickable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogDestinations
 * @property-read int|null $inventory_log_destinations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogSources
 * @property-read int|null $inventory_log_sources_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\Warehouse $warehouse
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location wherePickable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereWarehouseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location withoutTrashed()
 * @mixin \Eloquent
 */
class Location extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = [
        'products'
    ];

    protected $fillable = [
        'warehouse_id',
        'name',
        'pickable',
        'location_type_id',
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->withTrashed();
    }

    public function locationType()
    {
        return $this->belongsTo(LocationType::class)->withTrashed();
    }

    public function products()
    {
        return $this->hasMany(LocationProduct::class);
    }

    public function inventoryLogSources()
    {
        return $this->morphMany(InventoryLog::class, 'source');
    }

    public function inventoryLogDestinations()
    {
        return $this->morphMany(InventoryLog::class, 'destination');
    }

    public function isPickable()
    {
        return (bool) $this->pickable;
    }
}
