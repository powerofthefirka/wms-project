<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\ObjectChange\RelatedObjectSaved;
use App\Events\ObjectChange\RelatedObjectDelete;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $object_type
 * @property int $object_id
 * @property string|null $source
 * @property string|null $filename
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $object
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Query\Builder|Image onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Image withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Image withoutTrashed()
 * @mixin \Eloquent
 */
class Image extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'source',
        'filename'
    ];

    protected $dispatchesEvents = [
        'created' => RelatedObjectSaved::class,
        'deleting' => RelatedObjectDelete::class,
    ];

    public function object()
    {
        return $this->morphTo();
    }
}
