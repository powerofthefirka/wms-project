<?php

namespace App\Models;

use Illuminate\Support\Arr;
use App\Events\ObjectChange\UpdateObject;
use Igaster\LaravelCities\Geo;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $customer_id
 * @property int $shipping_contact_information_id
 * @property int $billing_contact_information_id
 * @property string $number
 * @property \Illuminate\Support\Carbon|null $ordered_at
 * @property \Illuminate\Support\Carbon|null $required_shipping_date_at
 * @property string|null $notes
 * @property int $priority
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\ContactInformation $billingContactInformation
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogDestinations
 * @property-read int|null $inventory_log_destinations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $orderItems
 * @property-read int|null $order_items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Return_[] $returns
 * @property-read int|null $returns_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shipment[] $shipment
 * @property-read int|null $shipment_count
 * @property-read \App\Models\ContactInformation $shippingContactInformation
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBillingContactInformationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOrderedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRequiredShippingDateAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereShippingContactInformationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order withoutTrashed()
 * @mixin \Eloquent
 * @property string $shipping_price
 * @property string $tax
 * @property string $discount
 * @property int|null $shipping_carrier_id
 * @property int|null $shipping_method_id
 * @property string|null $shipping_carrier_title
 * @property string|null $shipping_method_title
 * @property int $shipment_type
 * @property string|null $subtotal
 * @property string|null $total
 * @property int $is_cancelled
 * @property string $shop_name
 * @property string $status
 * @property int $fraud_hold
 * @property int $address_hold
 * @property int $payment_hold
 * @property int $operator_hold
 * @property string|null $shipping_lat
 * @property string|null $shipping_lng
 * @property int $item_count
 * @property int $discount_type
 * @property-read mixed $age
 * @property-read mixed $multiplied_priority
 * @property-read mixed $true_priority
 * @property-read \App\Models\OrderLock|null $orderLock
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shipment[] $shipments
 * @property-read int|null $shipments_count
 * @property-read \App\Models\ShippingCarrier|null $shippingCarrier
 * @property-read \App\Models\ShippingMethod|null $shippingMethod
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDiscountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereFraudHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsCancelled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereItemCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOperatorHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShipmentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingCarrierTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingMethodTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotal($value)
 */
class Order extends Model
{
    use SoftDeletes, CascadeSoftDeletes, RevisionableTrait;

    const SHIPMENT_TYPE_REGULAR_SHIPMENT  = 0;
    const SHIPMENT_TYPE_PICK_WAREHOUSE_UP = 1;
    const SHIPMENT_TYPE_CUSTOM_LABEL      = 2;

    const STATUS_PENDING = 'pending';
    const STATUS_FULFILLED = 'fulfilled';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_PRIORITY = 'priority';
    const STATUS_BACKORDER = 'backorder';
    const STATUS_ON_HOLD = 'on_hold';

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'orders_table_hide_columns';

    const DEFAULT_IMAGE = '/images/product-placeholder.png';

    const FILTERABLE_COLUMNS = [
        [
            "column" => "orders.status",
            "title" => "Status A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "orders.status",
            "title" => "Status Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.number",
            "title" => "Order Number 0-9",
            "dir" => "asc"
        ],
        [
            "column" => "orders.number",
            "title" => "Order Number 9-0",
            "dir" => "desc"
        ],
        [
            "column" => "orders.ordered_at",
            "title" => "Date (oldest first)",
            "dir" => "asc"
        ],
        [
            "column" => "orders.ordered_at",
            "title" => "Date (newest first)",
            "dir" => "desc"
        ],
        [
            "column" => "customer_contact_information.name",
            "title" => "Customer A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "customer_contact_information.name",
            "title" => "Customer Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.total",
            "title" => "Total 0-9",
            "dir" => "asc"
        ],
        [
            "column" => "orders.total",
            "title" => "Total 9-0",
            "dir" => "desc"
        ],
        [
            "column" => "orders.item_count",
            "title" => "Items 0-9",
            "dir" => "asc"
        ],
        [
            "column" => "orders.item_count",
            "title" => "Items 9-0",
            "dir" => "desc"
        ],
        [
            "column" => "shipping_methods.name",
            "title" => "Delivery Method A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "shipping_methods.name",
            "title" => "Delivery Method Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "contact_informations.city",
            "title" => "City A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "contact_informations.city",
            "title" => "City Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "contact_informations.zip",
            "title" => "Zip Code A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "contact_informations.zip",
            "title" => "Zip Code Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "countries.code",
            "title" => "Country A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "countries.code",
            "title" => "Country Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.fraud_hold",
            "title" => "Fraud Hold A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "orders.fraud_hold",
            "title" => "Fraud Hold Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.address_hold",
            "title" => "Address Hold A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "orders.address_hold",
            "title" => "Address Hold Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.payment_hold",
            "title" => "Payment Hold A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "orders.payment_hold",
            "title" => "Payment Hold Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "orders.operator_hold",
            "title" => "Operator Hold A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "orders.operator_hold",
            "title" => "Operator Hold Z-A",
            "dir" => "desc"
        ],
    ];

    const EXPORTABLE_COLUMNS = [
        [
            "column" => "orders.number",
            "title" => "Order Number"
        ],
        [
            "column" => "orders.ordered_at",
            "title" => "Date"
        ],
        [
            "column" => "orders.status",
            "title" => "Status"
        ],
        [
            "column" => "orders.customer_id",
            "title" => "Customer ID"
        ],
        [
            "column" => "orders.shipping_carrier_title",
            "title" => "Shipping Carrier"
        ],
        [
            "column" => "orders.shipping_method_title",
            "title" => "Shipping Method"
        ],
        [
            "column" => "orders.required_shipping_date_at",
            "title" => "Shipping Date"
        ],
        [
            "column" => "orders.shipping_price",
            "title" => "Shipping Price"
        ],
        [
            "column" => "orders.tax",
            "title" => "Tax"
        ],
        [
            "column" => "orders.discount",
            "title" => "Discount"
        ],
        [
            "column" => "orders.notes",
            "title" => "Notes"
        ],
        [
            "column" => "orders.priority",
            "title" => "Priority"
        ],
        [
            "column" => "shipping_contact_information.name as shipping_name",
            "title" => "Shipping Name"
        ],
        [
            "column" => "shipping_contact_information.company_name as shipping_company",
            "title" => "Shipping Company Name"
        ],
        [
            "column" => "shipping_contact_information.address as shipping_address",
            "title" => "Shipping Address"
        ],
        [
            "column" => "shipping_contact_information.address2 as shipping_address2",
            "title" => "Shipping Address 2"
        ],
        [
            "column" => "shipping_contact_information.zip as shipping_zip",
            "title" => "Shipping ZIP"
        ],
        [
            "column" => "shipping_contact_information.city as shipping_city",
            "title" => "Shipping City"
        ],
        [
            "column" => "shipping_country.title as shipping_country_title",
            "title" => "Shipping Country"
        ],
        [
            "column" => "shipping_contact_information.email as shipping_email",
            "title" => "Shipping Email"
        ],
        [
            "column" => "shipping_contact_information.phone as shipping_phone",
            "title" => "Shipping Phone"
        ],
        [
            "column" => "billing_contact_information.name as billing_name",
            "title" => "Billing Name"
        ],
        [
            "column" => "billing_contact_information.company_name as billing_company",
            "title" => "Billing Company Name"
        ],
        [
            "column" => "billing_contact_information.address as billing_address",
            "title" => "Billing Address"
        ],
        [
            "column" => "billing_contact_information.address2 as billing_address2",
            "title" => "Billing Address 2"
        ],
        [
            "column" => "billing_contact_information.zip as billing_zip",
            "title" => "Billing ZIP"
        ],
        [
            "column" => "billing_contact_information.city as billing_city",
            "title" => "Billing City"
        ],
        [
            "column" => "billing_country.title as billing_country_title",
            "title" => "Billing Country"
        ],
        [
            "column" => "billing_contact_information.email as billing_email",
            "title" => "Billing Email"
        ],
        [
            "column" => "billing_contact_information.phone as billing_phone",
            "title" => "Billing Phone"
        ],
        [
            "column" => "order_items.sku",
            "title" => "Product Sku"
        ],
        [
            "column" => "order_items.quantity",
            "title" => "Product Quantity"
        ],
    ];

    const STATUS_ICONS = [
        'pending' => 'sts-pending',
        'fulfilled' => 'sts-fulfilled',
        'on_hold' => 'sts-on-hold',
        'priority' => 'sts-priority',
        'backorder' => 'sts-backorder',
        'all' => 'sts-default',
    ];

    protected $cascadeDeletes = [
        'orderItems',
        'shipments',
        'shippingContactInformation',
        'billingContactInformation',
        'returns'
    ];

    protected $fillable = [
        'customer_id',
        'shipping_contact_information_id',
        'billing_contact_information_id',
        'number',
        'ordered_at',
        'required_shipping_date_at',
        'notes',
        'priority',
        'shipping_price',
        'tax',
        'discount',
        'discount_type',
        'shipment_type',
        'shipping_carrier_id',
        'shipping_method_id',
        'fraud_hold',
        'address_hold',
        'payment_hold',
        'operator_hold',
        'subtotal',
        'total',
        'is_cancelled',
        'status',
        'item_count',
        'shipping_carrier_title',
        'shipping_method_title',
        'shipping_lat',
        'shipping_lng',
        'shop_name'
    ];

    protected $dates = [
        'ordered_at',
        'required_shipping_date_at',
    ];

    protected $currentDateAttributes = [
        'ordered_at'
    ];

    protected $attributes = [
        'shipping_price' => 0
    ];

    protected $dispatchesEvents = [
        'updating' => UpdateObject::class,
    ];

    public static $shipmentTypeValues = [
        self::SHIPMENT_TYPE_REGULAR_SHIPMENT => 'Regular Shipment',
        self::SHIPMENT_TYPE_PICK_WAREHOUSE_UP => 'Pick Warehouse Up',
        self::SHIPMENT_TYPE_CUSTOM_LABEL => 'Custom Label',
    ];

    public function setShipmentTypeAttribute($type)
    {
        $this->attributes['shipment_type'] = $type ?? self::SHIPMENT_TYPE_REGULAR_SHIPMENT;
    }

    public function setShippingPriceAttribute($price)
    {
        $this->attributes['shipping_price'] = $price ?? 0;
    }

    public function setTaxAttribute($tax)
    {
        $this->attributes['tax'] = $tax ?? 0;
    }

    public function setDiscountAttribute($discount)
    {
        $this->attributes['discount'] = $discount ?? 0;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->with('contactInformation')->withTrashed();
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function shippedItems()
    {
        return $this->orderItems()->where('quantity_shipped', '>', 0);
    }

    public function shipments()
    {
        return $this->hasMany(Shipment::class);
    }

    public function shippingContactInformation()
    {
        return $this->belongsTo(ContactInformation::class, 'shipping_contact_information_id', 'id')->withTrashed();
    }

    public function billingContactInformation()
    {
        return $this->belongsTo(ContactInformation::class, 'billing_contact_information_id', 'id')->withTrashed();
    }

    public function returns()
    {
        return $this->hasMany(Return_::class);
    }

    public function inventoryLogDestinations()
    {
        return $this->morphMany(InventoryLog::class, 'destination')->withTrashed();
    }

    public function orderLock()
    {
        return $this->hasOne(OrderLock::class);
    }

    public function getMultipliedPriorityAttribute()
    {
        return $this->priority * 10;
    }

    public function getAgeAttribute()
    {
        if (!empty($this->created_at) && !empty($this->ordered_at)) {
            return $this->ordered_at->diffInDays($this->created_at);
        }

        return 0;
    }

    public function getTruePriorityAttribute()
    {
        return $this->MultipliedPriority + $this->Age;
    }

    public function shippingCarrier()
    {
        return $this->belongsTo(ShippingCarrier::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class);
    }

    public function backorderedItems()
    {
        return $this->orderItems()->where('quantity_backordered', '>', 0);
    }

    public function calculateOrder()
    {
        $input = $this->toArray();
        $input['order_items'] = $this->orderItems()->get()->toArray();

        $input = $this->updateOrder($input);

        $this->update($input);
    }

    public function updateOrder($input)
    {
        $subtotal = 0;
        $itemCount = 0;
        $totalQuantityPending = 0;
        $backorderedItems = 0;

        foreach ($input['order_items'] as $item) {
            $item['price'] = Arr::get($item, 'price', 0);

            if (!empty($item['product_id'])) {
                $product = Product::find($item['product_id']);
                $item['price'] = $product->price;
            }

            $itemCount += Arr::get($item, 'quantity', 0);
            $subtotal += Arr::get($item, 'quantity', 0) * Arr::get($item, 'price');

            $totalQuantityPending += Arr::get($item, 'quantity_pending', 0);

            if (!empty($item['quantity_backordered']) && $item['quantity_backordered'] > 0) {
                $backorderedItems++;
            }
        }

        $input['item_count'] = $itemCount;
        $input['subtotal'] = $subtotal;
        $input['total'] = $subtotal + Arr::get($input, 'shipping_price', 0) + Arr::get($input, 'tax', 0) - Arr::get($input, 'discount', 0);
        $input['status'] = $this->getStatus($totalQuantityPending, $backorderedItems, $input);

        return $input;
    }

    public function getStatus($totalQuantityPending, $backorderedItems, $input)
    {
        if (Arr::get($input, 'is_cancelled', false)) {
            return self::STATUS_CANCELLED;
        } else if ($totalQuantityPending <= 0) {
            return self::STATUS_FULFILLED;
        } else if (Arr::get($input, 'priority', 0) > 0) {
            return self::STATUS_PRIORITY;
        } else if ($backorderedItems > 0) {
            return self::STATUS_BACKORDER;
        } else if ((Arr::get($input, 'fraud_hold', 0) > 0) || (Arr::get($input, 'address_hold', 0) > 0) || (Arr::get($input, 'payment_hold', 0) > 0) || (Arr::get($input, 'operator_hold', 0) > 0)) {
            return self::STATUS_ON_HOLD;
        }

        return self::STATUS_PENDING;
    }

    public static function getStatuses()
    {
        return [
             self::STATUS_PENDING => 'Pending',
             self::STATUS_FULFILLED => 'Fulfilled',
             self::STATUS_CANCELLED => 'Cancelled',
             self::STATUS_PRIORITY => 'Priority',
             self::STATUS_BACKORDER => 'Backorder',
             self::STATUS_ON_HOLD => 'On hold'
        ];
    }

    public function getMapCoordinates()
    {
        $city = Arr::get($this->shippingContactInformation, 'city', '');
        $countryCode = Arr::get($this->shippingContactInformation, 'country.code', '');

        $coordinates = Geo::where('name', $city)
            ->where('country', $countryCode)
            ->first();
        if ($coordinates) {
            $this->shipping_lat = $coordinates->lat;
            $this->shipping_lng = $coordinates->long;

            $this->save();
        }
    }

    public function cancel()
    {
        $this->is_cancelled = true;
        $this->status = self::STATUS_CANCELLED;
        $this->update();
    }

    public function canCancel()
    {
        $status = $this->status;

        if ($status === self::STATUS_FULFILLED || $status === self::STATUS_CANCELLED) {
            return false;
        }

        return true;
    }

    public function isDifferentBillingInformation()
    {
        $isDifferentBillingInformation = false;

        if ($this->shippingContactInformation && $this->billingContactInformation) {
            $shippingCollection = collect($this->shippingContactInformation);
            $billingCollection = collect($this->billingContactInformation);

            $differentColumns = $shippingCollection->diffAssoc($billingCollection)->except('id', 'updated_at', 'created_at');
            $isDifferentBillingInformation = $differentColumns->count() > 0 ? true : false;
        }

        return $isDifferentBillingInformation;
    }

    public function reshipItems($orderItems)
    {
        foreach ($orderItems as $item) {
            if (empty($item['order_item_id']) || empty($item['reship_quantity'])) {
                continue;
            }

            $orderLine = OrderItem::find($item['order_item_id']);
            $orderLine->quantity_pending += Arr::get($item, 'reship_quantity');

            $orderLine->update();
        }
    }
}
