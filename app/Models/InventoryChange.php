<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\InventoryChange
 *
 * @property int $id
 * @property int $product_id
 * @property int $location_id
 * @property string $previous_on_hand
 * @property string $change_in_on_hand
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\Location $location
 * @property \Illuminate\Support\Carbon|null $changed_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryChange onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange wherePreviousOnHand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryChange whereChangeOnHand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryChange withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryChange withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|InventoryChange whereQuantity($value)
 */
class InventoryChange extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'location_id',
        'previous_on_hand',
        'change_in_on_hand',
        'changed_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(Location::class)->withTrashed();
    }
}
