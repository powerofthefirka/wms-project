<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ShippingMethod
 *
 * @property int $id
 * @property int $shipping_carrier_id
 * @property string $name
 * @property string $wms_id
 * @property string|null $title
 * @property string $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $is_visible
 * @property-read \App\Models\ShippingCarrier $shippingCarrier
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod newQuery()
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereIsVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereShippingCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereWmsId($value)
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod withoutTrashed()
 * @mixin \Eloquent
 */
class ShippingMethod extends Model
{
    use SoftDeletes;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'shipping_method_table_hide_columns';

    protected $fillable = [
        'name',
        'title',
        'shipping_carrier_id',
        'cost',
        'wms_id'
    ];

    public static $customShippingMethodTitle = [
        1 => "1/overnight",
        2 => "2 Days",
        3 => "3 Days",
        4 => "4 Days",
        99 => "99/cheapest ever"
    ];

    public function shippingCarrier()
    {
        return $this->belongsTo(ShippingCarrier::class);
    }
}
