<?php

namespace App\Models;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\BillingProfile
 *
 * @property int $id
 * @property string $name
 * @property string|null $monthly_cost
 * @property string|null $per_user_cost
 * @property string|null $per_purchase_order_received_cost
 * @property string|null $per_product_cost
 * @property string|null $per_shipment_cost
 * @property string|null $per_return_cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $customers
 * @property-read int|null $customers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoice
 * @property-read int|null $invoice_count
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile newQuery()
 * @method static \Illuminate\Database\Query\Builder|BillingProfile onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile query()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereMonthlyCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile wherePerProductCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile wherePerPurchaseOrderReceivedCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile wherePerReturnCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile wherePerShipmentCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile wherePerUserCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingProfile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|BillingProfile withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BillingProfile withoutTrashed()
 * @mixin \Eloquent
 */
class BillingProfile extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'billings_table_hide_columns';

    protected $cascadeDeletes = [
        'billingFees'
    ];

    protected $fillable = [
        'name',
        'monthly_cost',
        'per_user_cost',
        'per_purchase_order_received_cost',
        'per_product_cost',
        'per_shipment_cost',
        'per_return_cost',
        '3pl_id'
    ];

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    public function billingFees()
    {
        return $this->hasMany(BillingFee::class);
    }

    public function threePL()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id');
    }
}
