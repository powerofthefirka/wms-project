<?php

namespace App\Models;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * App\Models\UserSetting
 *
 * @property int $id
 * @property int $user_id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereValue($value)
 * @mixin \Eloquent
 */
class UserSetting extends Model
{
    const USER_SETTING_DASHBOARD_FILTER_DATE_START = 'dashboard_filter_date_start';
    const USER_SETTING_DASHBOARD_FILTER_DATE_END = 'dashboard_filter_date_end';

    protected $fillable = ['user_id', 'key', 'value'];

    static function saveSettings($settings)
    {
        $userId = Auth::user()->id;
        foreach ($settings as $setting => $value) {
            UserSetting::updateOrCreate(
                ['user_id' => $userId, 'key' => $setting],
                ['value' => $value]
            );

            Cache::put('user_setting_' . $userId . '_' . $setting, $value);
        }
    }
}
