<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CustomerLocation extends Pivot
{
    protected $table = 'customer_locations';

    protected $fillable = [
        'customer_id',
        'location_id',
    ];
}
