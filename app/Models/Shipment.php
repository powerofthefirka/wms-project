<?php

namespace App\Models;

use App\Events\OrderShipped;
use App\Events\ObjectChange\UpdateObject;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * App\Models\Shipment
 *
 * @property int $id
 * @property int $order_id
 * @property string|null $tracking_code
 * @property string|null $tracking_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Order $order
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ShipmentItem[] $shipmentItems
 * @property-read int|null $shipment_items_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shipment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereTrackingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereTrackingLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shipment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shipment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Shipment withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $shipping_carrier_id
 * @property int|null $shipping_method_id
 * @property string|null $number
 * @property string|null $shipped_at
 * @property string|null $distinct_items
 * @property string|null $quantity_shipped
 * @property string|null $line_item_total
 * @property string|null $shipping_carrier_title
 * @property string|null $shipping_method_title
 * @property-read \App\Models\ContactInformation|null $contactInformation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\ShippingCarrier|null $shippingCarrier
 * @property-read \App\Models\ShippingMethod|null $shippingMethod
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereDistinctItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereLineItemTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereQuantityShipped($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereShippedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereShippingCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereShippingCarrierTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereShippingMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shipment whereShippingMethodTitle($value)
 */
class Shipment extends Model
{
    const SETTINGS_HIDDEN_COLUMNS_KEY = 'shipments_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            'column' => 'orders.number',
            'title' => 'Order Number 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'orders.number',
            'title' => 'Order Number 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipments.shipped_at',
            'title' => 'Shipment date (oldest first)',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipments.shipped_at',
            'title' => 'Shipment date (newest first)',
            'dir' => 'desc'
        ],
        [
            'column' => 'orders.ordered_at',
            'title' => 'Order date (oldest first)',
            'dir' => 'asc'
        ],
        [
            'column' => 'orders.ordered_at',
            'title' => 'Order date (newest first)',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipments.tracking_code',
            'title' => 'Tracking Number A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipments.tracking_code',
            'title' => 'Tracking Number Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_carriers.name',
            'title' => 'Shipping carrier A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_carriers.name',
            'title' => 'Shipping carrier Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_methods.name',
            'title' => 'Shipping method A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_methods.name',
            'title' => 'Shipping method Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.email',
            'title' => 'Email A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.email',
            'title' => 'Email Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.address',
            'title' => 'Address A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.address',
            'title' => 'Address Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.address2',
            'title' => 'Address2 A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.address2',
            'title' => 'Address2 Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.city',
            'title' => 'City A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.city',
            'title' => 'City Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.zip',
            'title' => 'Zip A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.zip',
            'title' => 'Zip Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'countries.title',
            'title' => 'Country A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'countries.title',
            'title' => 'Country Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.company_name',
            'title' => 'Company A-Z',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.company_name',
            'title' => 'Company Z-A',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipping_contact_information.phone',
            'title' => 'Phone 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipping_contact_information.phone',
            'title' => 'Phone 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipments.distinct_items',
            'title' => 'Distinct items 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipments.distinct_items',
            'title' => 'Distinct items 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipments.quantity_shipped',
            'title' => 'Quantity shipped 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipments.quantity_shipped',
            'title' => 'Quantity shipped 9-0',
            'dir' => 'desc'
        ],
        [
            'column' => 'shipments.line_item_total',
            'title' => 'Line item total 0-9',
            'dir' => 'asc'
        ],
        [
            'column' => 'shipments.line_item_total',
            'title' => 'Line item total 9-0',
            'dir' => 'desc'
        ],
    ];

    const EXPORTABLE_COLUMNS = [
        [
            'column' => 'orders.number as order_number',
            'title' => 'Order Number'
        ],
        [
            'column' => 'shipments.number as shipment_number',
            'title' => 'Shipment Number'
        ],
        [
            'column' => 'shipments.shipped_at',
            'title' => 'Shipment Date'
        ],
        [
            'column' => 'orders.ordered_at',
            'title' => 'Order Date'
        ],
        [
            'column' => 'shipments.tracking_code',
            'title' => 'Tracking Number'
        ],
        [
            "column" => "shipments.tracking_url",
            "title" => "Tracking URL"
        ],
        [
            "column" => "shipping_contact_information.name as shipping_name",
            "title" => "Shipping Name"
        ],
        [
            "column" => "shipping_contact_information.company_name as shipping_company",
            "title" => "Shipping Company Name"
        ],
        [
            "column" => "shipping_contact_information.address as shipping_address",
            "title" => "Shipping Address"
        ],
        [
            "column" => "shipping_contact_information.address2 as shipping_address2",
            "title" => "Shipping Address 2"
        ],
        [
            "column" => "shipping_contact_information.zip as shipping_zip",
            "title" => "Shipping ZIP"
        ],
        [
            "column" => "shipping_contact_information.city as shipping_city",
            "title" => "Shipping City"
        ],
        [
            "column" => "shipping_country.title as shipping_country_title",
            "title" => "Shipping Country"
        ],
        [
            "column" => "shipping_contact_information.email as shipping_email",
            "title" => "Shipping Email"
        ],
        [
            "column" => "shipping_contact_information.phone as shipping_phone",
            "title" => "Shipping Phone"
        ],
        [
            'column' => 'shipments.distinct_items',
            'title' => 'Distinct Items'
        ],
        [
            'column' => 'shipments.quantity_shipped',
            'title' => 'Quantity Shipped'
        ],
        [
            'column' => 'shipments.line_item_total',
            'title' => 'Line Item Total'
        ],
        [
            'column' => 'order_items.sku',
            'title' => 'Product Sku'
        ],
        [
            'column' => 'shipment_items.quantity',
            'title' => 'Quantity'
        ],
    ];

    use SoftDeletes, CascadeSoftDeletes, RevisionableTrait;

    protected $cascadeDeletes = [
        'shipmentItems'
    ];

    protected $fillable = [
        'order_id',
        'number',
        'tracking_code',
        'tracking_url',
        'distinct_items',
        'quantity_shipped',
        'line_item_total',
        'shipped_at'
    ];

    protected $dispatchesEvents = [
        'saved' => OrderShipped::class,
        'updating' => UpdateObject::class,
    ];

    protected $dates = ['shipped_at'];

    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }

    public function shipmentItems()
    {
        return $this->hasMany(ShipmentItem::class);
    }

    public function contactInformation()
    {
        return $this->morphOne(ContactInformation::class, 'object');
    }

    public function shippingBoxes()
    {
        return $this->hasMany(Package::class);
    }

    public function packages()
    {
        return $this->hasMany(Package::class);
    }

    public function getTotalShipmentWeight()
    {
        $total = 0;

        foreach ($this->packages as $package) {
            foreach ($package->packageItems as $packageItem) {
                $total += $packageItem->quantity * weight_unit_conversion($packageItem->product->weight, Arr::get($packageItem->product, 'weight_unit', 'lb'), Arr::get($this->order->customer,'weight_unit', env('DEFAULT_WEIGHT_UNIT')));
            }
        }

        return $total;
    }
}
