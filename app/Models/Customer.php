<?php

namespace App\Models;

use App\Components\CamelIntegration;
use App\Currency;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Customer
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\ContactInformation $contactInformation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Return_[] $returns
 * @property-read int|null $returns_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Supplier[] $suppliers
 * @property-read int|null $suppliers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskType[] $taskTypes
 * @property-read int|null $task_types_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Warehouse[] $warehouses
 * @property-read int|null $warehouses_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereContactInformationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customer withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $billing_profile_id
 * @property string|null $dimensions_unit
 * @property string|null $weight_unit
 * @property string|null $currency_code
 * @property-read \App\Models\BillingProfile|null $billingProfile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoice
 * @property-read int|null $invoice_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Webhook[] $webhooks
 * @property-read int|null $webhooks_count
 * @property-read \App\Models\WebshipperCredential|null $webshipperCredential
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereBillingProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereDimensionsUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePrimaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereSecondaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereWeightUnit($value)
 */
class Customer extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    protected $cascadeDeletes = [
        'contactInformation',
        'orders',
        'warehouses',
        'purchaseOrders',
        'suppliers',
        'products',
    ];

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'local_key',
        'billing_profile_id',
        'active_integration',
        'messaging_provider',
        'widget_token',
        '3pl_id',
        'active',
        'weight_unit',
        'currency_id',
        'dimensions_unit'
    ];

    private $contactInformation;

    protected $attributes = [
        'dimensions_unit' => 'inch',
        'weight_unit' => 'lbs',
        'currency_code' => 'USD',
    ];

    const INTEGRATION_SYNC_DATES = [
        'purchase_order_fetched_at' => '',
        'shipment_fetched_at' => '',
        'product_fetched_at' => '',
        'order_fetched_at' => '',
        'inventory_changes_fetched_at' => '',
        'return_fetched_at' => ''
    ];

    const WEIGHT_UNITS = [
        'oz' => ['title' => 'oz', 'unit' => 'oz'],
        'lb' => ['title' => 'lb', 'unit' => 'lb'],
        'kg' => ['title' => 'kg', 'unit' => 'kg'],
        'g' => ['title' => 'g', 'unit' => 'g']
    ];

    const  DIMENSIONS_UNITS = [
        'inch' => ['title' => 'inch', 'unit' => 'inch'],
        'feet' => ['title' => 'feet', 'unit' => 'feet'],
        'cm' => ['title' => 'cm', 'unit' => 'cm'],
        'm' => ['title' => 'm', 'unit' => 'm']
    ];


    public function setBillingProfileIdAttribute($value)
    {
        $this->attributes['billing_profile_id'] = empty($value) ? null : $value;
    }

    public function children()
    {
        return $this->hasMany(Customer::class);
    }

    public static function getAllIntegrationDates()
    {
        $customerIntegrationSyncDates = [];
        try {
            $response = CamelIntegration::getAllIntegrations();
            if (!empty($response)) {
                foreach ($response as $customerId => $customerDates) {
                    $customerIntegrationSyncDates[$customerId] = Customer::INTEGRATION_SYNC_DATES;
                    foreach (array_keys($customerDates) as $key) {
                        try {
                            $customerIntegrationSyncDates[$customerId][$key] = empty($customerDates[$key]) ? __('In Progress') : localized_date_time($customerDates[$key]);
                        } catch (\Exception $e) {
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
        }

        return $customerIntegrationSyncDates;
    }

    public function getIntegrationDates()
    {
        $customerIntegrationSyncDates = Customer::INTEGRATION_SYNC_DATES;

        if (!empty($this->local_key)) {
            try {
                $response = CamelIntegration::getIntegrationByLocalKey($this->local_key);
                if (!empty($response)) {
                    foreach (array_keys($customerIntegrationSyncDates) as $key) {
                        $customerIntegrationSyncDates[$key] = empty($response[$key]) ? __('In Progress') : localized_date_time($response[$key]);
                    }
                }
            } catch (\Exception $exception) {
            }
        }

        return $customerIntegrationSyncDates;
    }

    public function contactInformation()
    {
        return $this->morphOne(ContactInformation::class, 'object')->withTrashed();
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->using(CustomerUser::class)->withPivot(['role_id']);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function warehouses()
    {
        return $this->hasMany(Warehouse::class);
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function returns()
    {
        return $this->hasMany(Return_::class);
    }

    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function hasUser($userId)
    {
        return $this->users->contains('id', $userId);
    }

    public function webhooks()
    {
        return $this->hasMany(Webhook::class);
    }

    public function webshipperCredential()
    {
        return $this->hasOne(WebshipperCredential::class);
    }

    public function billingProfile()
    {
        return $this->belongsTo(BillingProfile::class);
    }

    public function invoice()
    {
        return $this->hasMany(Invoice::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function threePl()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function shippingMethods()
    {
        return $this->belongsToMany(ShippingMethod::class, 'customer_shipping_methods');
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class, 'customer_locations');
    }
}
