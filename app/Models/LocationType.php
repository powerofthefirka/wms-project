<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LocationType
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property string $wms_id
 * @property string $daily_storage_cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType newQuery()
 * @method static \Illuminate\Database\Query\Builder|LocationType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationType whereWmsId($value)
 * @method static \Illuminate\Database\Query\Builder|LocationType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LocationType withoutTrashed()
 * @mixin \Eloquent
 */
class LocationType extends Model
{
    use SoftDeletes;

    const HIDDEN_COLUMNS = 'location_type_table_hide_columns';

    protected $fillable = [
        'name',
        'wms_id',
        'daily_storage_cost',
        '3pl_id'
    ];

    public function locations()
    {
        $customerLocationIds = CustomerLocation::whereIn('customer_id', Auth()->user()->customerIds())->pluck('location_id');

        return $this->hasMany(Location::class)->whereIn('id', $customerLocationIds)->orderBy('name', 'asc');
    }

    public function threePl()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
