<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Class BillingFee
 * @package App\Models
 */
class BillingFee extends Model
{
    use SoftDeletes;

    const RECURRING = 'recurring';
    const RECEIVING_BY_PO = 'receiving_by_po';
    const RECEIVING_BY_ITEM = 'receiving_by_item';
    const RECEIVING_BY_LINE = 'receiving_by_line';
    const STORAGE_BY_LOCATION = 'storage_by_location';
    const STORAGE_BY_PRODUCT = 'storage_by_product';
    const SHIPMENT_BY_BOX = 'shipments_by_box';
    const SHIPMENTS_BY_SHIPPING_LABEL = 'shipments_by_shipping_label';
    const SHIPMENTS_BY_PICKING_FEE = 'shipments_by_picking_fee';
    const SHIPMENTS_BY_PICKUP_PICKING_FEE = 'shipments_by_pickup_picking_fee';
    const RETURNS = 'returns';
    const AD_HOC = 'ad_hoc';
    const SHIPPING_RATES = 'shipping_rates';
    const RECEIVING_BY_HOUR = 'receiving_by_hour';

    const BILLLING_FEE_TYPES = [
        self::RECURRING => ['title' => 'Recurring', 'folder' => 'recurring'],
        self::RECEIVING_BY_PO => ['title' => 'Receiving By PO', 'folder' => 'receiving_by_po'],
        self::RECEIVING_BY_ITEM => ['title' => 'Receiving By Item', 'folder' => 'receiving_by_item'],
        self::RECEIVING_BY_LINE => ['title' => 'Receiving By Line', 'folder' => 'receiving_by_line'],
        self::STORAGE_BY_LOCATION => ['title' => 'Storage By Location', 'folder' => 'storage_by_location'],
        self::STORAGE_BY_PRODUCT => ['title' => 'Storage By Product', 'folder' => 'storage_by_product'],
        self::SHIPMENT_BY_BOX => ['title' => 'Shipments By Box', 'folder' => 'shipments_by_box'],
        self::SHIPMENTS_BY_SHIPPING_LABEL => ['title' => 'Shipments By Shipping Label', 'folder' => 'shipments_by_shipping_label'],
        self::SHIPMENTS_BY_PICKING_FEE => ['title' => 'Shipments By Picking Fee', 'folder' => 'shipments_by_picking_fee'],
        self::SHIPMENTS_BY_PICKUP_PICKING_FEE => ['title' => 'Shipments By Pickup Picking Fee', 'folder' => 'shipments_by_pickup_picking_fee'],
        self::RETURNS => ['title' => 'Returns', 'folder' => 'returns'],
        self::AD_HOC => ['title' => 'Ad hoc', 'folder' => 'ad_hoc'],
        self::SHIPPING_RATES => ['title' => 'Shipping rates', 'folder' => 'shipping_rates'],
        self::RECEIVING_BY_HOUR => ['title' => 'Receiving by hour', 'folder' => 'receiving_by_hour'],
    ];

    const PERIODS = [ 'day', 'week', 'month' ];
    const WEIGHT_UNITS = [ 'g', 'kg', 'oz', 'lb' ];
    const VOLUME_UNITS = [ 'cubic inch', 'cubic feet', 'cubic cm', 'cubic m' ];
    const AD_HOC_UNITS = [ 'hours', 'orders', 'purchase orders', 'units' ];

    protected $fillable = [
        'name',
        'billing_profile_id',
        'type',
        'settings',
    ];

    protected $casts = [
        'settings' => 'array'
    ];

    public function getShippingCarrierFromSettings()
    {
        $shippingCarrierId = Arr::get($this->settings, 'shipping_carrier_id');

        if (empty($shippingCarrierId)) {
            return [];
        }

        return ShippingCarrier::find($shippingCarrierId) ?? [];
    }

    public function getShippingCarrierMethodsFromSettings()
    {
        $shippingCarrier = $this->getShippingCarrierFromSettings();

        if (empty($shippingCarrier)) {
            return [];
        }

        return $shippingCarrier->shippingMethods ?? [];
    }

    public function getCountries($countryIds)
    {
        if (empty($countryIds)) {
            return [];
        }

        $countries = Country::whereIn('id', $countryIds)->get();

        return $countries->keyBy('id');
    }

    public function getAllSelectedCountries()
    {
        $countries = new Collection();

        $shippingZones = Arr::get($this->settings, 'shipping_zones', []);

        foreach ($shippingZones as $shippingZone) {
            $zoneCountryIds = $shippingZone['countries'] ?? [];
            $zoneCountries = Country::whereIn('id', $zoneCountryIds)->get();
            $countries = $countries->merge($zoneCountries);
        }

        return $countries->keyBy('id');
    }
    
    public function getAllSelectedShippingMethods()
    {
        $shippingMethodIds = Arr::get($this->settings, 'shipping_method');

        if (empty($shippingMethodIds)) {
            return [];
        }

        $shippingMethods = ShippingMethod::whereIn('id', $shippingMethodIds)->get();

        return $shippingMethods->keyBy('id');
    }
}
