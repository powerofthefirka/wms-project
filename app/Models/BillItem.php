<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'bill_id',
        'billing_fee_id',
        'description',
        'quantity',
        'unit_price',
        'total_price',
        'due_date',
        'period_end',
        'purchase_order_item_id',
        'purchase_order_id',
        'return_item_id',
        'package_id',
        'package_item_id',
        'location_type_id',
        'inventory_change_id',
        'shipment_id',
    ];

    public function bill()
    {
        return $this->belongsTo(Bill::class)->withTrashed();
    }

    public function billingFee()
    {
        return $this->belongsTo(BillingFee::class)->withTrashed();
    }

    public function purchaseOrderItem()
    {
        return $this->belongsTo(PurchaseOrderItem::class)->withTrashed();
    }

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class)->withTrashed();
    }

    public function returnItem()
    {
        return $this->belongsTo(ReturnItem::class)->withTrashed();
    }

    public function packageItem()
    {
        return $this->belongsTo(PackageItem::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function inventoryChange()
    {
        return $this->belongsTo(InventoryChange::class)->withTrashed();
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class)->withTrashed();
    }
}
