<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ShippingCarrier
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property string $wms_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ShippingMethod[] $shippingMethods
 * @property-read int|null $shipping_methods_count
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier newQuery()
 * @method static \Illuminate\Database\Query\Builder|ShippingCarrier onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingCarrier whereWmsId($value)
 * @method static \Illuminate\Database\Query\Builder|ShippingCarrier withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ShippingCarrier withoutTrashed()
 * @mixin \Eloquent
 */
class ShippingCarrier extends Model
{
    use SoftDeletes;

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'shipping_carrier_table_hide_columns';

    protected $fillable = [
        'customer_id',
        'name',
        'wms_id',
        'tracking_url',
        '3pl_id'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shippingMethods()
    {
        $customerShippingMethodIds = CustomerShippingMethod::whereIn('customer_id', Auth()->user()->customerIds())->pluck('shipping_method_id');

        return $this->hasMany(ShippingMethod::class)->whereIn('id', $customerShippingMethodIds)->where('is_visible', true)->orderBy('name', 'asc');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'object');
    }

    public function threePl()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id', 'id');
    }
}
