<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Schema;

/**
 * App\Models\InventoryLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $source_type
 * @property int $source_id
 * @property string $destination_type
 * @property int $destination_id
 * @property int $product_id
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $destination
 * @property-read \App\Models\Product $product
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $source
 * @property-read \App\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryLog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereDestinationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereDestinationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereQuantityAllocated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereQuantityAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereSourceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryLog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InventoryLog withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|InventoryLog whereQuantity($value)
 */
class InventoryLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'product_id',
        'quantity'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function source()
    {
        return $this->morphTo()->withTrashed();
    }

    public function destination()
    {
        return $this->morphTo()->withTrashed();
    }

    public function getSourceName()
    {
        return $this->source->name ?? $this->source->number ?? '#Item doesnt exist / Or is deleted !';
    }

    public function getDestinationName()
    {
        return $this->destination->name ?? $this->destination->number ?? $this->destination->order->number ?? '#Item doesnt exist / Or is deleted !';
    }

    public function getSourceUrl()
    {
        $sourceId = $this->source_id;
        $returns[] = null;

        if ($this->source_type === Return_::class)
        {
            $returns = [
                'url' => (strlen($this->source->deleted_at ) == 0 ? '/return/'. $sourceId . '/edit' : '#'),
                'name' => (strlen($this->source->deleted_at ) == 0 ? 'Return - ' . $this->getDestinationName() : 'DELETED/Return - ' . $this->getSourceName() )
            ];

             return $returns;
        }
        elseif ($this->source_type === PurchaseOrder::class)
        {
            $returns = [
                'url' => (strlen($this->source->deleted_at ) == 0 ? '/purchase_orders/'. $sourceId . '/edit' : '#'),
                'name' => (strlen($this->source->deleted_at ) == 0 ? 'Purchase Order  - ' . $this->getDestinationName() : 'DELETED/Purchase Order - ' . $this->getSourceName() )
            ];

            return $returns;
        }
        elseif ($this->source_type === Location::class)
        {
            $returns = [
                'url' => (strlen($this->source->deleted_at ) == 0 ? '/location/'. $sourceId . '/edit' : '#'),
                'name' => (strlen($this->source->deleted_at ) == 0 ? 'Location - ' . $this->getDestinationName() : 'DELETED/Location - ' . $this->getSourceName() )
            ];

            return $returns;
        }
        else {

            return $returns;
        }
    }

    public function getDestinationUrl()
    {
        $destinationId = $this->destination_id;
        $returns[] = null;

        if ($this->destination_type === Shipment::class)
        {
            $returns = [
                'url' => (strlen($this->destination->deleted_at ) == 0 ? '/shipment/'. $destinationId . '/itemsShipped' : '#'),
                'name' => (strlen($this->destination->deleted_at ) == 0 ? 'Shipment - ' . $this->getDestinationName() : 'DELETED/Shipment - ' . $this->getDestinationName() )
            ];

            return $returns;
        }
        elseif ($this->destination_type === Location::class)
        {
            $returns = [
                'url' => (strlen($this->destination->deleted_at ) == 0 ? '/location/'. $destinationId . '/edit' : '#'),
                'name' => (strlen($this->destination->deleted_at ) == 0 ? 'Location - ' . $this->getDestinationName()  : 'DELETED/Location - ' . $this->getDestinationName() )
            ];

            return $returns;
        }
        else {
            return $returns;
        }
    }
}
