<?php

namespace App\Models;

use App\Events\ObjectChange\UpdateObject;
use App\Events\PurchaseOrderSaved;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\PurchaseOrder
 *
 * @property int $id
 * @property int $customer_id
 * @property int $warehouse_id
 * @property int $supplier_id
 * @property string $number
 * @property \Illuminate\Support\Carbon|null $ordered_at
 * @property \Illuminate\Support\Carbon|null $expected_at
 * @property \Illuminate\Support\Carbon|null $delivered_at
 * @property string|null $notes
 * @property int $priority
 * @property int $is_cancelled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogSources
 * @property-read int|null $inventory_log_sources_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrderItem[] $purchaseOrderLines
 * @property-read int|null $purchase_order_lines_count
 * @property-read \App\Models\Supplier $supplier
 * @property-read \App\Models\Warehouse $warehouse
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrder onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereDeliveredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereExpectedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereOrderedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseOrder whereWarehouseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrder withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PurchaseOrder withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $shipping_carrier_id
 * @property int|null $shipping_method_id
 * @property string $shipping_price
 * @property string|null $shipping_carrier_title
 * @property string|null $shipping_method_title
 * @property string|null $status
 * @property string|null $tracking_number
 * @property string|null $tracking_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrderItem[] $purchaseOrderItems
 * @property-read int|null $purchase_order_items_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\ShippingCarrier|null $shippingCarrier
 * @property-read \App\Models\ShippingMethod|null $shippingMethod
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereShippingCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereShippingCarrierTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereShippingMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereShippingMethodTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereShippingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereTrackingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrder whereTrackingUrl($value)
 */
class PurchaseOrder extends Model
{
    use RevisionableTrait;

    use SoftDeletes;

    const STATUS_CANCELLED = 'cancelled';
    const STATUS_CLOSED = 'closed';

    const SETTINGS_HIDDEN_COLUMNS_KEY = 'purchase_order_table_hide_columns';

    const FILTERABLE_COLUMNS = [
        [
            "column" => "purchase_orders.number",
            "title" => "Number 0-9",
            "dir" => "asc"
        ],
        [
            "column" => "purchase_orders.number",
            "title" => "Number 9-0",
            "dir" => "desc"
        ],
        [
            "column" => "purchase_orders.status",
            "title" => "Status A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "purchase_orders.status",
            "title" => "Status Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "purchase_orders.ordered_at",
            "title" => "Created date (from oldest)",
            "dir" => "asc"
        ],
        [
            "column" => "purchase_orders.ordered_at",
            "title" => "Created date (from newest)",
            "dir" => "desc"
        ],
        [
            "column" => "purchase_orders.expected_at",
            "title" => "Expected date (from oldest)",
            "dir" => "asc"
        ],
        [
            "column" => "purchase_orders.expected_at",
            "title" => "Expected date (from newest)",
            "dir" => "desc"
        ],
        [
            "column" => "supplier_contact_information.name",
            "title" => "Vendor A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "supplier_contact_information.name",
            "title" => "Vendor Z-A",
            "dir" => "desc"
        ],
        [
            "column" => "warehouse_contact_information.name",
            "title" => "Warehouse A-Z",
            "dir" => "asc"
        ],
        [
            "column" => "warehouse_contact_information.name",
            "title" => "Warehouse Z-A",
            "dir" => "desc"
        ],
    ];

    const EXPORTABLE_COLUMNS = [
        [
            "column" => "purchase_orders.number",
            "title" => "Number"
        ],
        [
            "column" => "purchase_orders.status",
            "title" => "Status"
        ],
        [
            "column" => "purchase_orders.customer_id",
            "title" => "Customer ID"
        ],
        [
            "column" => "purchase_orders.ordered_at",
            "title" => "Created Date"
        ],
        [
            "column" => "purchase_orders.expected_at",
            "title" => "Expected Date",
        ],
        [
            "column" => "purchase_orders.delivered_at",
            "title" => "Delivered Date",
        ],
        [
            "column" => "purchase_orders.shipping_carrier_title",
            "title" => "Shipping Carrier"
        ],
        [
            "column" => "purchase_orders.shipping_method_title",
            "title" => "Shipping Method"
        ],
        [
            "column" => "purchase_orders.shipping_price",
            "title" => "Shipping Price"
        ],
        [
            "column" => "supplier_contact_information.name as vendor_name",
            "title" => "Vendor"
        ],
        [
            "column" => "warehouse_contact_information.name as warehouse_name",
            "title" => "Warehouse"
        ],
        [
            "column" => "purchase_orders.tracking_number",
            "title" => "Tracking Number"
        ],
        [
            "column" => "purchase_orders.tracking_url",
            "title" => "Tracking URL"
        ],
        [
            "column" => "purchase_orders.notes",
            "title" => "Notes"
        ],
        [
            "column" => "purchase_orders.priority",
            "title" => "Priority"
        ],
        [
            "column" => "purchase_order_items.sku",
            "title" => "Product Sku"
        ],
        [
            "column" => "purchase_order_items.quantity",
            "title" => "Product Quantity"
        ],
    ];

    const STATUS_ICONS = [
        'pending' => 'sts-pending',
        'closed' => 'sts-on-hold',
        'all' => 'sts-default',
    ];

    protected $fillable = [
        'customer_id',
        'warehouse_id',
        'supplier_id',
        'number',
        'status',
        'ordered_at',
        'expected_at',
        'delivered_at',
        'notes',
        'priority',
        'shipping_carrier_id',
        'shipping_method_id',
        'shipping_price',
        'shipping_carrier_title',
        'shipping_method_title',
        'tracking_number',
        'tracking_url',
        'subtotal',
        'discount',
        'tax',
        'total',
        'is_cancelled',
        'hours_receiving'
    ];

    protected $dates = [
        'ordered_at',
        'expected_at',
        'delivered_at'
    ];

    protected $attributes = [
        'status' => 'pending'
    ];

    protected $currentDateAttributes = [
        'ordered_at'
    ];

    protected $dispatchesEvents = [
        'saved' => PurchaseOrderSaved::class,
        'updating' => UpdateObject::class,
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class)->withTrashed();
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->withTrashed();
    }

    public function purchaseOrderItems()
    {
        return $this->hasMany(PurchaseOrderItem::class);
    }

    public function receivedItems()
    {
        return $this->purchaseOrderItems()->where('quantity_received', '>', 0);
    }

    public function inventoryLogSources()
    {
        return $this->morphMany(InventoryLog::class, 'source');
    }

    public function shippingCarrier()
    {
        return $this->belongsTo(ShippingCarrier::class);
    }

    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class);
    }

    public function calculatePurchaseOrder()
    {
        $subtotal = 0;

        foreach ($this->purchaseOrderItems as $item) {
            $subtotal += $item->quantity * $item->price;
        }

        $this->subtotal = $subtotal;
        $this->total = $subtotal + $this->shipping_price + $this->tax - $this->discount;

        if ($this->is_cancelled) {
            $this->status = self::STATUS_CANCELLED;
        }

        $this->update();
    }

    public function cancel()
    {
        $this->is_cancelled = true;
        $this->status = self::STATUS_CANCELLED;
        $this->update();
    }

    public function canCancel()
    {
        return in_array($this->status, [self::STATUS_CLOSED, self::STATUS_CANCELLED]) ? false : true;
    }
}
