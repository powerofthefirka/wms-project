<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Auth;

/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model query()
 * @mixin \Eloquent
 */
class Model extends EloquentModel
{
    protected $currentDateAttributes = [];

    public function getUserTimezone()
    {
        $userTimezone = UserSetting::where('user_id', Auth::user()->id)
            ->where('key', 'timezone')->first();

        return $userTimezone['value'];
    }

    protected static function boot()
    {
        parent::boot();

        self::creating(function(Model $model) {
            foreach ($model->currentDateAttributes as $currentDateAttribute) {
                if (!$model->getAttribute($currentDateAttribute)) {
                    $model->setAttribute($currentDateAttribute, now());
                }
            }
        });
    }
}
