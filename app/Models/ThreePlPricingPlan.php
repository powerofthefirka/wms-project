<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ThreePlPricingPlan extends Model
{
    use SoftDeletes;

    protected $table = '3pl_pricing_plans';

    protected $fillable = [
        '3pl_id',
        'pricing_plan_name',
        'billing_period',
        'monthly_price',
        'initial_fee',
        'three_pl_billing_fee',
        'number_of_monthly_orders',
        'price_per_additional_order',
        'invoice_start_date'
    ];

    protected $dates = ['invoice_start_date'];

    public function threePl()
    {
        return $this->belongsTo(ThreePl::class, '3pl_id', 'id');
    }
}
