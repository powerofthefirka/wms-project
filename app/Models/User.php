<?php

namespace App\Models;

use App\Notifications\PasswordReset;
use App\Traits\HasPrintableApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string|null $picture
 * @property int $user_role_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \App\Models\ContactInformation $contactInformation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $customers
 * @property-read int|null $customers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InventoryLog[] $inventoryLogs
 * @property-read int|null $inventory_logs_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\UserRole $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Datab2d72fease\Eloquent\Builder|\App\Models\User whereContactInformationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserSetting[] $user_settings
 * @property-read int|null $user_settings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserWidget[] $user_widgets
 * @property-read int|null $user_widgets_count
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserRoleId($value)
 */
class User extends Authenticatable
{
    use HasApiTokens, HasPrintableApiTokens, Notifiable, SoftDeletes;

    const STATUS_CLOSED = 'closed';
    const STATUS_COMPLETE = 'complete';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'picture' ,'user_role_id', 'active', 'email_notifications', 'last_notified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $attributes = [
        'user_role_id' => UserRole::ROLE_DEFAULT
    ];

    public function contactInformation()
    {
        return $this->morphOne(ContactInformation::class, 'object')->withTrashed();
    }

    /**
     * Get the role of the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(UserRole::class, 'user_role_id', 'id');
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class)->using(CustomerUser::class)->withPivot(['role_id']);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function inventoryLogs()
    {
        return $this->hasMany(InventoryLog::class);
    }

    /**
     * Get the path to the logo
     *
     * @return string
     */
    public function logo()
    {
        $logo = asset('argon') . env('DEFAULT_LOGO');

        if ($customer = $this->customers->first()) {
            if ($domain = $customer->threePl->domains->first()) {
                $logo = $domain->logo->source ?? $logo;
            }
        }

        return $logo;
    }

    /**
     * Check if the user has admin role
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->user_role_id == UserRole::ROLE_ADMINISTRATOR;
    }

    public function isCustomerAdmin()
    {
        if (!empty($this->customers)) {
            $customer = $this->customers->first();
            if (Arr::get($customer, 'pivot.role_id') === CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR) {
                return true;
            }
        }

        return false;
    }

/*    public function getCustomerUser()
    {
//        return $this->hasOne();
    }*/

    public function hasCustomer($customerId)
    {
        return $this->customers->contains('id', $customerId);
    }

    public function userSettings()
    {
        return $this->hasMany(UserSetting::class);
    }

    public function user_widgets()
    {
        return $this->hasMany(UserWidget::class);
    }

    public function customerIds($includeDisabled = false, $ignoreSession = false)
    {
        $user = Auth()->user();
        $sessionCustomerId = session('customer_id');

        if ($sessionCustomerId && !$ignoreSession) {
            return [$sessionCustomerId];
        } else if ($user->isAdmin()) {
            return Customer::all()->pluck('id');
        } else if ($includeDisabled) {
            return $user->customers->pluck('id')->toArray();
        }

        return $user->customers->where('active', 1)->pluck('id')->toArray();
    }

    protected static function boot()
    {
        static::created(function (User $user) {
            UserWidget::create([
                'user_id' => $user->id,
                'location' => 'dashboard',
                'grid_stack_data' => '[{"x":0,"y":0,"w":4,"h":200,"content":"[widget_orders_received]"},{"x":4,"y":0,"w":4,"h":200,"content":"[widget_orders_shipped]"},{"x":8,"y":0,"w":4,"h":200,"id":"1","noResize":true,"content":"[widget_returns]"},{"x":0,"y":200,"w":4,"h":470,"content":"[widget_orders_late]"},{"x":4,"y":200,"w":8,"h":470,"content":"[widget_revenue_over_time]"},{"x":0,"y":670,"w":12,"h":830,"id":"2","content":"[widget_order_by_country]"},{"x":0,"y":1500,"w":8,"h":400,"content":"[widget_purchase_orders_received]"},{"x":8,"y":1500,"w":4,"h":200,"content":"[widget_purchase_orders_coming_in]"},{"x":8,"y":1700,"w":4,"h":200,"content":"[widget_purchase_orders_received_quantity]"}]'
            ]);
        });

        parent::boot();
    }

    public function threePls()
    {
        return $this->belongsToMany(ThreePl::class, '3pl_user', 'user_id', '3pl_id')->using(ThreePlUser::class)->wherePivot('active', '1');
    }

    public function is3PLUser()
    {
        if ($this->threePls->count() > 0) {
            return true;
        }

        return false;
    }

    public function threePlIds()
    {
        $user = Auth()->user();
        $sessionCustomerId = session('customer_id');

        if ($sessionCustomerId) {
            return [Customer::find($sessionCustomerId)->threePl->id];
        } else if ($user->isAdmin()) {
            return ThreePl::all()->where('active', 1)->pluck('id')->toArray();
        } else {
            return $this->threePls->where('active', 1)->pluck('id')->toArray();
        }
    }

    public function hasThreePl($threePlId)
    {
        return $this->threePls->contains('id', $threePlId);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    public function Totes()
    {
        return $this->hasMany(Tote::class);
    }
}
