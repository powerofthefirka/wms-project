<?php

namespace App\Models;


/**
 * App\Models\CustomerUserRole
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerUserRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CustomerUserRole extends Model
{
    protected $fillable = ['name'];

    const ROLE_CUSTOMER_ADMINISTRATOR = 1;
    const ROLE_CUSTOMER_MEMBER = 2;

    const ROLE_DEFAULT = self::ROLE_CUSTOMER_MEMBER;
}
