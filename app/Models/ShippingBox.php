<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ShippingBox
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property string $wms_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox newQuery()
 * @method static \Illuminate\Database\Query\Builder|ShippingBox onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingBox whereWmsId($value)
 * @method static \Illuminate\Database\Query\Builder|ShippingBox withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ShippingBox withoutTrashed()
 * @mixin \Eloquent
 */
class ShippingBox extends Model
{
    use SoftDeletes;

    const HIDDEN_COLUMNS = 'shipping_box_table_hide_columns';

    protected $fillable = [
        'customer_id',
        'name',
        'wms_id',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function shipments()
    {
        return $this->hasMany(Package::class);
    }
}
