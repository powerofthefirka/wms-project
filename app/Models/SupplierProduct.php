<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\SupplierProduct
 *
 * @property int $product_id
 * @property int $supplier_id
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierProduct whereSupplierId($value)
 * @mixin \Eloquent
 */
class SupplierProduct extends Pivot
{
    protected $table = 'supplier_product';

    protected $fillable = [
        'product_id',
        'supplier_id',
    ];
}
