<?php

namespace App\Libraries;

use Closure;

class Decorator
{
    /**
     * Decorated instance
     * @var mixed
     */
    private $decorated;

    private $methods = [];

    /**
     * Decorate given instance
     * @param mixed $toDecorate
     */
    public function __construct($toDecorate)
    {
        $this->decorated = $toDecorate;
    }

    /**
     * Decorate a method
     * @param string  $name
     * @param Closure $callback Method to run instead of decorated one
     */
    public function decorate($name, Closure $callback)
    {
        $this->methods[$name] = $callback;
        return $this;
    }

    /**
     * Call a method on decorated instance
     * @param  string $name
     * @param  array  $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (isset($this->methods[$name])) {
            return call_user_func_array($this->methods[$name], [$this->decorated, $arguments]);
        }

        return call_user_func_array([$this->decorated, $name], $arguments);
    }
}
