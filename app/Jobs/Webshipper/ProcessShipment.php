<?php

namespace App\Jobs\Webshipper;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Shipment;

class ProcessShipment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shipment;
    public $carrierId;
    public $shippingMethod;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Shipment $shipment, $carrierId, $shippingMethod)
    {
        $this->shipment = $shipment;
        $this->carrierId = $carrierId;
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app()->webshipperShipping->processShipment($this->shipment, $this->carrierId, $this->shippingMethod);
    }
}