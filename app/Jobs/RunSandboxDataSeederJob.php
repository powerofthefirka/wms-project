<?php

namespace App\Jobs;

use App\Models\Customer;
use Artisan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SandboxDataSeeder;

class RunSandboxDataSeederJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $customer;

    public function __construct(Customer $customer = null)
    {
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @param $customerId
     * @return void
     */
    public function handle()
    {
        $seeder = new SandboxDataSeeder();
        $seeder->run($this->customer);
    }
}
