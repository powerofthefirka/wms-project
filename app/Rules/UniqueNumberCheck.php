<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Order;
use App\Models\Shipment;
use App\Models\Return_;

class UniqueNumberCheck implements Rule
{
    private $className;
    private $id;
    private $column;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($className, $id, $column = null)
    {
        $this->className = $className;
        $this->id = $id;
        $this->column = $column;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (Order::class == $this->className) {
            $object = $this->className::where('number', $value)->where('customer_id', $this->id)->where('shop_name', $this->column)->first();
        } elseif (Shipment::class == $this->className || Return_::class == $this->className) {
            $order = Order::where('id', $this->id)->first();

            $object = $this->className::where('number', $value)->whereIn('order_id', $order->customer->orders->pluck('id')->unique()->toArray())->first();
        }

        return $object ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('The :attribute has already been taken');
    }
}
