<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BelongsToCustomer implements Rule
{
    private $className;
    private $customerId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($className, $customerId)
    {
        $this->className = $className;
        $this->customerId = $customerId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $object = call_user_func(array($this->className, 'find'), $value);

        if ($object) {
            return $object->customer_id == $this->customerId;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __(':attribute doesn\'t belong to customer');
    }
}
