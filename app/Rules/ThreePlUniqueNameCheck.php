<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ThreePl;

class ThreePlUniqueNameCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $nameFound = false;

        $threePls = ThreePl::get();

        foreach ($threePls as $threePl) {
            if ($threePl->contactInformation->name == $value) {
                $nameFound = true;
                continue;
            }
        }

        return !$nameFound;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('The :attribute has already been taken');
    }
}
