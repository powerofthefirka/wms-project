<?php

namespace App\Rules\BillingFees;

use App\Models\BillingFee;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class ShipmenstByShippingLabelRule implements Rule
{
    private $billingProfile;
    private $type;
    private $billingFee;

    public function __construct($billingProfile, $type, $billingFee)
    {
        $this->billingProfile = $billingProfile;
        $this->type = $type;
        $this->billingFee = $billingFee;
    }

    public function passes($attribute, $value)
    {
        $fees = BillingFee::where('billing_profile_id', $this->billingProfile['id'])
            ->where('type', $this->type);

        if($this->billingFee) {
            $fees = $fees->where('id', '!=', $this->billingFee['id']);
        }

        $fees = $fees->get();

        $methodsSelected = json_decode($value['methods_selected'], true);
        $noOtherFeeApplies = Arr::get($value, 'if_no_other_fee_applies');

        foreach ($fees as $fee) {
            $settings = $fee['settings'];
            $methods = json_decode($settings['methods_selected'], true);

            if (Arr::get($settings, 'if_no_other_fee_applies') && $noOtherFeeApplies) {
                return false;
            }

            foreach ($methodsSelected as $carrier => $carrierMethods) {
                foreach ($carrierMethods as $method) {
                    $result = in_array($method, Arr::get($methods, $carrier, []) );

                    if($result) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function message()
    {
        return __('Conflicts with existing fee');
    }
}
