<?php

namespace App\Rules\BillingFees;

use App\Models\BillingFee;
use Illuminate\Contracts\Validation\Rule;

class ReceivingByPoRule implements Rule
{
    private $billingProfile;
    private $type;

    public function __construct($billingProfile, $type)
    {
        $this->billingProfile = $billingProfile;
        $this->type = $type;
    }

    public function passes($attribute, $value)
    {
        $fee = BillingFee::where('billing_profile_id', $this->billingProfile['id'])
            ->where('type', $this->type)
            ->get();

        return empty(count($fee));
    }

    public function message()
    {
        return __('Conflicts with existing fee');
    }
}
