<?php

namespace App\Rules\BillingFees;

use App\Models\BillingFee;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class ReceivingByLineRule implements Rule
{
    private $billingProfile;
    private $type;
    private $billingFee;

    public function __construct($billingProfile, $type, $billingFee)
    {
        $this->billingProfile = $billingProfile;
        $this->type = $type;
        $this->billingFee = $billingFee;
    }

    public function passes($attribute, $value)
    {
        $fees = BillingFee::where('billing_profile_id', $this->billingProfile['id'])
            ->where('type', $this->type);

        if($this->billingFee) {
            $fees = $fees->where('id', '!=', $this->billingFee['id']);
        }

        $fees = $fees->get();
        $productProfiles = $value['product_profiles'];
        $withoutProfile = Arr::get($value, 'without_profile');
        $noOtherFeeApplies = Arr::get($value, 'if_no_other_fee_applies');

        foreach ($fees as $fee) {
            $settings = $fee['settings'];
            $profiles = json_decode($settings['product_profiles']);

            if (Arr::get($settings, 'if_no_other_fee_applies') && $noOtherFeeApplies) {
                return false;
            }

            if (Arr::get($settings, 'without_profile') && $withoutProfile) {
                return false;
            }

            foreach ($productProfiles as $profile) {
                $result = in_array($profile, $profiles);

                if($result) {
                    return false;
                }
            }
        }

        return true;
    }

    public function message()
    {
        return __('Conflicts with existing fee');
    }
}
