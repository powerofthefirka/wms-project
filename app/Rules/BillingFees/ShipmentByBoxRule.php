<?php

namespace App\Rules\BillingFees;

use App\Models\BillingFee;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class ShipmentByBoxRule implements Rule
{
    private $billingProfile;
    private $type;
    private $billingFee;

    public function __construct($billingProfile, $type, $billingFee)
    {
        $this->billingProfile = $billingProfile;
        $this->type = $type;
        $this->billingFee = $billingFee;
    }

    public function passes($attribute, $value)
    {
        $fees = BillingFee::where('billing_profile_id', $this->billingProfile['id'])
            ->where('type', $this->type);

        if($this->billingFee) {
            $fees = $fees->where('id', '!=', $this->billingFee['id']);
        }

        $fees = $fees->get();
        $shippingBoxes = json_decode($value['shipping_boxes'], true);
        $noOtherFeeApplies = Arr::get($value, 'if_no_other_fee_applies');

        foreach ($fees as $fee) {
            $settings = $fee['settings'];
            $boxes = json_decode($settings['shipping_boxes']);

            if (Arr::get($settings, 'if_no_other_fee_applies') && $noOtherFeeApplies) {
                return false;
            }

            foreach ($shippingBoxes as $box) {
                $result = in_array($box, $boxes);

                if($result) {
                    return false;
                }
            }
        }

        return true;
    }

    public function message()
    {
        return __('Conflicts with existing fee');
    }
}
