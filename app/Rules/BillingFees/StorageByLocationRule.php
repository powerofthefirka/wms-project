<?php

namespace App\Rules\BillingFees;

use App\Models\BillingFee;
use Illuminate\Contracts\Validation\Rule;

class StorageByLocationRule implements Rule
{
    private $billingProfile;
    private $type;
    private $billingFee;

    public function __construct($billingProfile, $type, $billingFee)
    {
        $this->billingProfile = $billingProfile;
        $this->type = $type;
        $this->billingFee = $billingFee;
    }

    public function passes($attribute, $value)
    {
        $fees = BillingFee::where('billing_profile_id', $this->billingProfile['id'])
            ->where('type', $this->type);

        if($this->billingFee) {
            $fees = $fees->where('id', '!=', $this->billingFee['id']);
        }

        $fees = $fees->get();
        $locationTypes = json_decode($value['location_types'], true);

        foreach ($fees as $fee) {
            $settings = $fee['settings'];
            $locations = json_decode($settings['location_types']);

            foreach ($locationTypes as $location) {
                $result = in_array($location, $locations);

                if($result) {
                    return false;
                }
            }
        }

        return true;
    }

    public function message()
    {
        return __('Conflicts with existing fee');
    }
}
