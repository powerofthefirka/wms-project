<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ContactInformation;
use App\Models\Customer;
use App\Models\Supplier;

class SupplierUniqueNameCheck implements Rule
{
    private $customerId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $nameFound = false;

        $suppliers = Supplier::where('customer_id', $this->customerId)->get();

        foreach ($suppliers as $supplier) {
            if ($supplier->contactInformation->name == $value) {
                $nameFound = true;
                continue;
            }
        }

        return !$nameFound;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('The :attribute has already been taken');
    }
}
