<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckArrayKeySum implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($key, $array)
    {
        $this->key = $key;
        $this->array = $array;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $values = [];

        foreach ($this->array as $item) {
            $values[] = $item[$this->key];
        }

        $passes = array_sum($values) ? true : false;

        return $passes;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('The :attribute should be at least 1');
    }
}
