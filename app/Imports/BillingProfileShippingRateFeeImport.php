<?php

namespace App\Imports;

use App\Models\BillingFee;
use App\Models\BillingProfile;
use App\Models\ShippingCarrier;
use App\Models\ShippingMethod;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerShippingMethod;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BillingProfileShippingRateFeeImport implements ToCollection, WithHeadingRow
{
    use Importable;

    private $billingProfile;

    private $errors = [];

    private $allCountries = [];

    public function __construct(BillingProfile $billingProfile)
    {
        $this->billingProfile = $billingProfile;
        $this->allCountries = Country::all();
        $this->allCountries = $this->allCountries->keyBy('title');
    }

    public function collection(Collection $rows)
    {
        BillingFee::where('type', BillingFee::SHIPPING_RATES)
            ->where('billing_profile_id', $this->billingProfile->id)->delete();

        $rowsBasedOnCountries = [];

        foreach ($rows as $row) {
            $rowsBasedOnCountries[$row['countries']][] = $row;
        }

        foreach ($rowsBasedOnCountries as $rowsBasedOnCountry) {
            $this->process($rowsBasedOnCountry);
        }

        ksort($this->errors);
        $this->errors = array_values($this->errors);
        $this->errorMessage();
    }

    private function process($rows)
    {
        foreach ($rows as $key => $row) {
            $name = Arr::get($row, 'name');

            $shippingRate = BillingFee::where('billing_profile_id', $this->billingProfile->id)
                ->where('name', 'like', '%' . $name . '%')->first();

            if (!$shippingRate) {
                $shippingRate = new BillingFee();
                $shippingRate->name = $name;
                $shippingRate->billing_profile_id = $this->billingProfile->id;
                $shippingRate->type = BillingFee::SHIPPING_RATES;
                $shippingRate->settings = [];
            }

            $shippingRate->settings = $this->formatShippingrateSettings($row, Arr::get($shippingRate, 'settings', []));
            $shippingRate->save();

        }
    }

    private function formatShippingrateSettings($row, $settings = [])
    {
        $shippingCarrierId = $this->getShippingCarrierId(Arr::get($row, 'shipping_carrier'));

        if (empty($shippingCarrierId)) {
            $this->errors[Arr::get($row, 'shipping_carrier')] = 'Shipping carrier ' . Arr::get($row, 'shipping_carrier') . ' not found in \"' . Arr::get($row, 'name') . '\" rate!';
        }

        $shippingMethodIds = $this->getShippingMethodIds($row, $shippingCarrierId);

        if (empty($shippingMethodIds)) {
            $this->errors[Arr::get($row, 'shipping_method')] = 'Shipping method ' . Arr::get($row, 'shipping_method') . ' not found in \"' . Arr::get($row, 'name') . '\" rate!';
        }

        $countryIdsWithShippingZoneKey = $this->getCountryIdsWithShippingZoneKey($row, $settings);

        return [
            'description' => Arr::get($row, 'description', ''),
            'shipping_zones' => $this->getShippingZones($row, $settings, $countryIdsWithShippingZoneKey),
            'shipping_carrier_id' => empty($shippingCarrierId) ? 0 : $shippingCarrierId,
            'shipping_method' => empty($shippingMethodIds) ? '' : $shippingMethodIds
        ];
    }

    private function errorMessage()
    {
        if (!empty($this->errors)) {
            session(['importError' => $this->errors]);
        }
    }

    private function getShippingZones($row, $settings, $countryIdsWithShippingZoneKey)
    {
        $shippingZone = isset($countryIdsWithShippingZoneKey['shipping_zone_key']) ? Arr::get($settings['shipping_zones'], $countryIdsWithShippingZoneKey['shipping_zone_key'], []) : [];
        $shippingPrices = Arr::get($shippingZone, 'shipping_prices', []);

        $newShippingPrice = [
            'price' => str_replace(',', '.', Arr::get($row, 'price', 0)),
            'weight_from' => Arr::get($row, 'weight_from', 0),
            'weight_to' => Arr::get($row, 'weight_to', 0),
            'weight_unit' => Arr::get($row, 'weight_unit', '')
        ];

        $addNewShippingPrice = true;
        foreach ($shippingPrices as &$shippingPrice) {
            if (Arr::get($shippingPrice, 'weight_to') == Arr::get($newShippingPrice, 'weight_to') &&
                Arr::get($shippingPrice, 'weight_from') == Arr::get($newShippingPrice, 'weight_from') &&
                Arr::get($shippingPrice, 'weight_unit') == Arr::get($newShippingPrice, 'weight_unit')
            ) {
                $shippingPrice = $newShippingPrice;
                $addNewShippingPrice = false;
            }
        }

        if ($addNewShippingPrice) {
            $shippingPrices[] = $newShippingPrice;
        }

        $shippingZoneKey = $countryIdsWithShippingZoneKey['shipping_zone_key'] ?? count(Arr::get($settings, 'shipping_zones', []));
        $settings['shipping_zones'][$shippingZoneKey] = [
            'countries' => $countryIdsWithShippingZoneKey['country_ids'],
            'shipping_prices' => $shippingPrices,
            'name' => Arr::get($row, 'shipping_zone', '')
        ];

        return $settings['shipping_zones'];
    }

    private function getCountryIdsWithShippingZoneKey($row, $settings)
    {
        $rowCountryIds = $this->getCountryIds($row);
        $shippingZones = Arr::get($settings, 'shipping_zones', []);

        foreach ($shippingZones as $key => $shippingZone) {
            $countries = Arr::get($shippingZone, 'countries', []);
            if (empty(array_diff($countries, $rowCountryIds))) {
                return ['shipping_zone_key' => $key, 'country_ids' => $rowCountryIds];
            }
        }

        return ['shipping_zone_key' => null, 'country_ids' => $rowCountryIds];
    }

    private function getCountryIds($row)
    {
        $countries = explode(',', Arr::get($row, 'countries', ''));
        $countryIds = [];
        foreach ($countries as $country) {
            $country = Arr::get($this->allCountries, trim($country));
            $countryId = $country['id'] ?? null;

            $countryIds[] = $countryId;

            if (empty($countryId)) {
                $this->errors[trim($country)] = 'Country ' . trim($country) . ' not found in \"' . Arr::get($row, 'name') . '\" rate!';
            }
        }

        return $countryIds;
    }

    public function getShippingCarrierId($shippingCarrierName)
    {
        $customerIds = [session('customer_id')];
        if (empty(session('customer_id'))) {
            $customerIds = Auth()->user()->customerIds();
        }

        $threePlIds = Customer::whereIn('id', $customerIds)->pluck('3pl_id');

        return ShippingCarrier::whereIn('3pl_id', $threePlIds)->where('name', 'like', '%' . strtolower($shippingCarrierName) . '%')->pluck('id')->first();
    }

    private function getShippingMethodIds($row, $shippingCarrierId)
    {
        $customerIds = [session('customer_id')];
        if (empty(session('customer_id'))) {
            $customerIds = Auth()->user()->customerIds();
        }

        $customerShippingMethodIds = CustomerShippingMethod::where('customer_id', $customerIds)->pluck('shipping_method_id');

        $shippingMethodNames = explode(',', Arr::get($row, 'shipping_method', ''));
        $shippingMethodIds = [];

        foreach ($shippingMethodNames as $shippingMethodName) {
            $shippingMethodId = ShippingMethod::whereIn('id', $customerShippingMethodIds)->where('shipping_carrier_id', $shippingCarrierId)->where('name', strtolower(trim($shippingMethodName)))->pluck('id')->first();

            if (!$shippingMethodId) {
                $this->errors[trim($shippingMethodName)] = 'Shipping method ' . trim($shippingMethodName) . ' not found in \"' . Arr::get($row, 'name') . '\" rate!';
                continue;
            }

            $shippingMethodIds[] = $shippingMethodId;
        }

        return $shippingMethodIds;
    }
}
