<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\Supplier;
use App\Models\UserRole;
use App\Models\Warehouse;
use Illuminate\Support\Arr;
use App\Models\PurchaseOrder;
use App\Models\ShippingMethod;
use App\Models\ShippingCarrier;
use App\Models\PurchaseOrderItem;
use App\Models\ContactInformation;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Requests\PurchaseOrder\ImportRequest;

class PurchaseOrderImport implements ToCollection, WithHeadingRow
{
    use Importable;

    private $receivedOrderIds = [];
    private $customerId;

    public function collection(Collection $rows)
    {
        foreach ($rows as $record) {
            $this->customerId = $record['customer_id'];

            $orderData = $this->reformData($record);
            $orderData['sku'] = Arr::get($record, 'sku', null);

            ImportRequest::make($orderData);
        }

        $isAdmin = true;
        $user = auth()->user();

        if ($user->user_role_id != UserRole::ROLE_ADMINISTRATOR) {
            $isAdmin = false;
            $customerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
        }

        $allOrderIds = [];
        $newOrderIds = [];
        $orderItems = [];

        foreach ($rows as $row) {
            if ((!$isAdmin && !in_array($row['customer_id'], $customerIds))) {
                continue;
            }

            $order = PurchaseOrder::where('number', $row['number'])->where('customer_id', $row['customer_id'])->first();

            $orderData = $this->reformData($row);

            if ($order) {
                if ($order->receivedItems->count() > 0) {
                    if (!in_array($order->id, $this->receivedOrderIds)) {
                        $this->receivedOrderIds[] = $order->id;
                    }
                    continue;
                }

                if (!in_array($order->id, $allOrderIds)) {
                    $order->update($orderData);
                }
            } else {
                $order = PurchaseOrder::create($orderData);

                $newOrderIds[] = $order->id;
            }

            if (!in_array($order->id, $allOrderIds)) {
                $allOrderIds[] = $order->id;
            }

            $orderItems[] = [
                'purchase_order_id' => $order->id,
                'product_id'        => $this->getModelId(Product::class, $row['sku'], 'sku'),
                'quantity'          => $row['quantity']
            ];
        }

        $existingOrderIds = array_diff($allOrderIds, $newOrderIds);
        PurchaseOrderItem::whereIn('purchase_order_id', $existingOrderIds)->delete();

        $this->orderItem($orderItems);

        $this->errorMessage();
    }

    private function errorMessage()
    {
        if (sizeof($this->receivedOrderIds) > 0) {
            $orders = PurchaseOrder::whereIn('id', $this->receivedOrderIds)->pluck('number')->toArray();
            session(['receivedError' => 'Following orders are already received: ' . implode(', ', $orders) . '. ']);
        }
    }

    private function reformData($row)
    {
        return [
            'number'                            => $row['number'],
            'customer_id'                       => $row['customer_id'],
            'warehouse_id'                      => $this->getObjectId(Warehouse::class, $row['warehouse_name']),
            'supplier_id'                       => $this->getObjectId(Supplier::class, $row['supplier_name']),
            'ordered_at'                        => $row['ordered_at'] ?? null,
            'expected_at'                       => $row['expected_at'] ?? null,
            'delivered_at'                      => $row['delivered_at'] ?? null,
            'notes'                             => $row['notes'],
            'priority'                          => $row['priority'] ?? 0,
            'shipping_carrier_id'               => $this->getModelId(ShippingCarrier::class, $row['shipping_carrier_name']),
            'shipping_method_id'                => $this->getModelId(ShippingMethod::class, $row['shipping_method_name']),
        ];
    }

    public function getObjectId($model, $name = null)
    {
        $objects = ContactInformation::whereHasMorph(
            'object',
            $model,
            function (Builder $query)  use ($name) {
                $query->where('name', 'like', '%' . $name . '%');
            }
        )->get();

        if ($objects->count() > 0) {
            $object = $model::whereIn('id', $objects->pluck('object_id'))->where('customer_id', $this->customerId)->first();

            if ($object) {
                return $object->id;
            }
        }

        return null;
    }

    public function getModelId($model, $name = null, $column = 'name')
    {
        $object = $model::where($column, 'like', '%' . $name . '%')->first();

        if ($object) {
            return $object->id;
        }

        return null;
    }

    private function orderItem($orderItems)
    {
        foreach ($orderItems as $item) {
            $orderItem = PurchaseOrderItem::where('purchase_order_id', $item['purchase_order_id'])->where('product_id', $item['product_id'])->first();
            if (empty($orderItem)) {
                PurchaseOrderItem::create($item);
            }
        }
    }
}
