<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\UserRole;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToCollection, WithHeadingRow
{
    use Importable;

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.sku' => 'required',
            '*.customer_id' => [
                'exists:customers,id,deleted_at,NULL'
            ]
        ])->validate();

        $isAdmin = true;
        $user = auth()->user();

        if ($user->user_role_id != UserRole::ROLE_ADMINISTRATOR) {
            $isAdmin = false;
            $customerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
        }

        foreach ($rows as $row) {
            if (!$isAdmin && !in_array($row['customer_id'], $customerIds)) {
                continue;
            }

            $product = Product::where('sku', $row['sku'])->first();

            $productData = $this->reformData($row);

            if ($product) {
                $product->update($productData);
            } else {
                Product::create($productData);
            }
        }
    }

    private function reformData($row)
    {
        return [
            'sku'                   => $row['sku'],
            'name'                  => $row['name'] ?? $row['sku'],
            'customer_id'           => $row['customer_id'],
            'price'                 => $row['price'] ?? 0,
            'customs_price'         => $row['customs_price'],
            'weight'                => $row['weight'],
            'width'                 => $row['width'],
            'height'                => $row['height'],
            'length'                => $row['length'],
            'country_of_origin'     => $row['country_of_origin'],
            'hs_code'               => $row['hs_code'],
            'notes'                 => $row['notes'],
            'quantity_on_hand'      => $row['quantity_on_hand'] ?? 0,
            'quantity_allocated'    => $row['quantity_allocated'] ?? 0,
            'quantity_available'    => $row['quantity_available'] ?? 0,
            'product_type'          => $row['product_type'] ?? 0,
            'barcode'               => $row['barcode'],
            'replacement_value'     => $row['replacement_value']
        ];
    }
}
