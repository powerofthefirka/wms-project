<?php

namespace App\Imports;

use App\Models\Order;
use App\Models\UserRole;
use App\Models\OrderItem;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Http\Requests\Order\ImportRequest;
use App\Models\Country;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Components\BaseComponent;
use App\Models\Product;

class OrderImport implements ToCollection, WithHeadingRow
{
    use Importable;

    private $shippedOrderIds = [];

    public function collection(Collection $rows)
    {
        foreach ($rows as $record) {
            $orderData = $this->reformData($record);
            $orderData['sku'] = Arr::get($record, 'sku', null);

            ImportRequest::make($orderData);
        }

        $isAdmin = true;
        $user = auth()->user();

        if ($user->user_role_id != UserRole::ROLE_ADMINISTRATOR) {
            $isAdmin = false;
            $customerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
        }

        $allOrderIds = [];
        $newOrderIds = [];
        $orderItems = [];

        foreach ($rows as $row) {
            if ((!$isAdmin && !in_array($row['customer_id'], $customerIds))) {
                continue;
            }

            $order = Order::where('number', $row['number'])->where('customer_id', $row['customer_id'])->first();

            $orderData = $this->reformData($row);

            if ($order) {
                if ($order->shippedItems->count() > 0) {
                    if (!in_array($order->id, $this->shippedOrderIds)) {
                        $this->shippedOrderIds[] = $order->id;
                    }
                    continue;
                }

                if (!in_array($order->id, $allOrderIds)) {
                    $order->update($orderData);
                }
            } else {
                $order = Order::create($orderData);

                $shippingInformationData = $this->reformShippingInformationData($row);
                $billingInformationData = $this->reformBillingInformationData($row);

                $baseComponent = new BaseComponent();

                $shippingContactInformation = $baseComponent->createContactInformation($shippingInformationData, $order);
                $billingContactInformation = $baseComponent->createContactInformation($billingInformationData, $order);

                $order->shipping_contact_information_id = $shippingContactInformation->id;
                $order->billing_contact_information_id = $billingContactInformation->id;
                $order->save();

                $newOrderIds[] = $order->id;
            }

            if (!in_array($order->id, $allOrderIds)) {
                $allOrderIds[] = $order->id;
            }

            $orderItems[] = [
                'order_id'          => $order->id,
                'product_id'        => $this->getProductId($row['sku']),
                'quantity'          => $row['quantity'],
            ];
        }

        $existingOrderIds = array_diff($allOrderIds, $newOrderIds);
        OrderItem::whereIn('order_id', $existingOrderIds)->delete();

        $this->orderItem($orderItems);

        $this->errorMessage();
    }

    private function errorMessage()
    {
        if (sizeof($this->shippedOrderIds) > 0) {
            $orders = Order::whereIn('id', $this->shippedOrderIds)->pluck('number')->toArray();
            session(['shippedError' => 'Following orders are already shipped: ' . implode(', ', $orders) . '. ']);
        }
    }

    private function reformData($row)
    {
        return [
            'number'                            => $row['number'],
            'customer_id'                       => $row['customer_id'],
            'ordered_at'                        => Arr::get($row, 'ordered_at', null),
            'required_shipping_date_at'         => Arr::get($row, 'required_shipping_date_at', null),
            'shipping_price'                    => $row['shipping_price'] ?? 0,
            'tax'                               => $row['tax'] ?? 0,
            'discount'                          => $row['discount'] ?? 0,
            'notes'                             => Arr::get($row, 'notes', null),
            'priority'                          => $row['priority'] ?? 0,
            'shipment_type'                     => $row['shipment_type'] ?? 0
        ];
    }

    public function getProductId($sku = null)
    {
        $product = Product::where('sku', 'like', '%' . $sku . '%')->first();
        if ($product) {
            return $product->id;
        }

        return null;
    }

    public function getCountryId($title = null)
    {
        $country = Country::where('title', 'like', '%' . $title . '%')->first();
        if ($country) {
            return $country->id;
        }

        return null;
    }

    private function reformShippingInformationData($row)
    {
        return [
            'name'              => $row['shipping_name'],
            'company_name'      => $row['shipping_company_name'],
            'address'           => $row['shipping_address'],
            'address2'          => $row['shipping_address_2'],
            'zip'               => $row['shipping_zip'],
            'city'              => $row['shipping_city'],
            'country_id'        => $this->getCountryId($row['shipping_country']),
            'email'             => $row['shipping_contact_email'],
            'phone'             => $row['shipping_phone'],
        ];
    }

    private function reformBillingInformationData($row)
    {
        return [
            'name'              => $row['billing_name'],
            'company_name'      => $row['billing_company_name'],
            'address'           => $row['billing_address'],
            'address2'          => $row['billing_address_2'],
            'zip'               => $row['billing_zip'],
            'city'              => $row['billing_city'],
            'country_id'        => $this->getCountryId($row['billing_country']),
            'email'             => $row['billing_contact_email'],
            'phone'             => $row['billing_phone'],
        ];
    }

    private function orderItem($orderItems)
    {
        foreach ($orderItems as $item) {
            $orderItem = OrderItem::where('order_id', $item['order_id'])->where('product_id', $item['product_id'])->first();
            if (empty($orderItem)) {
                OrderItem::create($item);
            }
        }
    }
}
