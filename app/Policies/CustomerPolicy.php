<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Customer;
use App\Models\CustomerUser;
use App\Models\CustomerUserRole;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the customer.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Customer  $customer
     * @return mixed
     */
    public function view(User $user, Customer $customer)
    {
        return $user->hasCustomer($customer->id) || $user->isAdmin();
    }

    /**
     * Determine whether the user can create customers.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the customer.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the customer.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    public function warehouses(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return true;
    }

    public function users(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return true;
    }

    public function tasks(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return true;
    }

    public function products(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return true;
    }

    public function updateUsers(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return CustomerUser::where('user_id', $user->id)->where('customer_id', $customer->id)->where('role_id', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->first();
    }

    public function webshipperCarriers(User $user, Customer $customer)
    {
        if ($user->isAdmin()) {
            return true;
        };

        if ($user->hasCustomer($customer->id) == false) {
            return false;
        }

        return true;
    }
}
