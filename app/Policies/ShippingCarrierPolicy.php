<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ShippingCarrier;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShippingCarrierPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the shipping carrier.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShippingCarrier  $shippingCarrier
     * @return mixed
     */
    public function view(User $user, ShippingCarrier $shippingCarrier)
    {
        return $user->isAdmin() || $shippingCarrier->threePl->hasUser($user->id);
    }

    /**
     * Determine whether the user can create shipping carrier.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['3pl_id'])) {
            return $user->hasThreePl($data['3pl_id']);
        }

        return true;            
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the shipping carrier.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $shippingCarrier = ShippingCarrier::find($data['id'])) {
            if ($user->hasThreePl($shippingCarrier->threePl->id) == false) {
                return false;
            }
        }

        if (isset($data['3pl_id'])) {
            return $user->hasThreePl($data['3pl_id']);
        }
        
        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the shipping carrier.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShippingCarrier $shippingCarrier
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $shippingCarrier = ShippingCarrier::find($data['id'])) {
            return $user->hasThreePl($shippingCarrier->threePl->id);
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
