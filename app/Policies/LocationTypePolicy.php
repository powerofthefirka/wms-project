<?php

namespace App\Policies;

use App\Models\User;
use App\Models\LocationType;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationTypePolicy
{
    use HandlesAuthorization;

    public function view(User $user, LocationType $locationType)
    {
        return $user->isAdmin() || $locationType->threePl->hasUser($user->id);
    }

    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['3pl_id'])) {
            return $user->hasThreePl($data['3pl_id']);
        }

        return true;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $locationType = LocationType::find($data['id'])) {
            if ($user->hasThreePl($locationType->threePl->id) == false) {
                return false;
            }
        }

        if (isset($data['3pl_id'])) {
            return $user->hasThreePl($data['3pl_id']);
        }

        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $locationType = LocationType::find($data['id'])) {
            return $user->hasThreePl($locationType->threePl->id);
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
