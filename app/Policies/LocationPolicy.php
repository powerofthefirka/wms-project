<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Location;
use App\Models\Warehouse;
use App\Models\CustomerLocation;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Location  $location
     * @return mixed
     */
    public function view(User $user, Location $location)
    {        
        if ($user->isAdmin()) {
            return true;
        }

        return CustomerLocation::where('location_id', $location->id)->whereIn('customer_id', $user->customerIds())->exists();
    }

    /**
     * Determine whether the user can create locations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        } 

        $data = $data ? $data : app('request')->input();

        if (isset($data['warehouse_id']) && $warehouse = Warehouse::find($data['warehouse_id'])) {
            return $user->hasThreePl($warehouse->customer->threePl->id);
        }

        return true; 
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->create( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Location  $location
     * @return mixed
     */ 
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $location = Location::find($data['id'])) {
            if (!CustomerLocation::where('location_id', $location->id)->whereIn('customer_id', $user->customerIds())->exists()) {
                return false;
            }
        }

        if (isset($data['warehouse_id']) && $warehouse = Warehouse::find($data['warehouse_id'])) {
            return $user->hasThreePl($warehouse->customer->threePl->id);
        }

        return true; 
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Determine whether the user can delete the location.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Location  $location
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        } 

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $location = Location::find($data['id'])) {
            return CustomerLocation::where('location_id', $location->id)->whereIn('customer_id', $user->customerIds())->exists();
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->delete( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
