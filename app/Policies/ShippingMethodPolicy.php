<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CustomerShippingMethod;
use App\Models\ShippingMethod;
use App\Models\ShippingCarrier;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShippingMethodPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the shipping method.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShippingMethod  $shippingMethod
     * @return mixed
     */
    public function view(User $user, ShippingMethod $shippingMethod)
    {
        if ($shippingMethod->is_visible == false) {
            return false;
        }
        
        if ($user->isAdmin()) {
            return true;
        }

        return CustomerShippingMethod::where('shipping_method_id', $shippingMethod->id)->whereIn('customer_id', $user->customerIds())->exists();
    }

    /**
     * Determine whether the user can create shipping method.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['3pl_id'])) {
            if ($user->hasThreePl($data['3pl_id']) == false ) {
                return false;
            }
        }

        if (isset($data['shipping_carrier_id']) && $shippingCarrier = ShippingCarrier::find($data['shipping_carrier_id'])) {
            if ($user->hasThreePl($shippingCarrier->threePl->id) == false) {
                return false;
            }
        }

        return true;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the shipping method.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $shippingMethod = ShippingMethod::find($data['id'])) {
            if ($shippingMethod->is_visible == false) {
                return false;
            }

            if (!CustomerShippingMethod::where('shipping_method_id', $shippingMethod->id)->whereIn('customer_id', $user->customerIds())->exists()) {
                return false;
            }
        }

        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the shipping method.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $shippingMethod = ShippingMethod::find($data['id'])) {
            return CustomerShippingMethod::where('shipping_method_id', $shippingMethod->id)->whereIn('customer_id', $user->customerIds())->exists();
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
