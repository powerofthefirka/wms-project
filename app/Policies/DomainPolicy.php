<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Domain;
use Illuminate\Auth\Access\HandlesAuthorization;

class DomainPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the domain.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Domain  $domain
     * @return mixed
     */
    public function view(User $user, Domain $domain)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create domains.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        return $user->isAdmin();
    }

    public function batchStore(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the domain.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        return $user->isAdmin();
    }

    public function batchUpdate(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the domain.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        return $user->isAdmin();
    }

    public function batchDelete(User $user)
    {
        return $user->isAdmin();
    }

    public function history(User $user, Domain $domain)
    {
        return $user->isAdmin();
    }
}
