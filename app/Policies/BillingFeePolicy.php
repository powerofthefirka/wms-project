<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BillingFeePolicy
{
    use HandlesAuthorization;

    public function create(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    public function update(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin() || $user->isCustomerAdmin()) {
            return true;
        }

        return false;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
