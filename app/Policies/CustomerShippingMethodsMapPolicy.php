<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CustomerShippingMethodsMap;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerShippingMethodsMapPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the webshipper credential.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CustomerShippingMethodsMap $customerShippingMethodsMap
     * @return mixed
     */
    public function view(User $user, CustomerShippingMethodsMap $customerShippingMethodsMap)
    {
        return $user->isAdmin() || $customerShippingMethodsMap->customer->hasUser($user->id);
    }

    /**
     * Determine whether the user can create webshipper credential.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['customer_id'])) {
            return $user->hasCustomer($data['customer_id']);
        }

        return true;            
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the webshipper credential.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $customerShippingMethodsMap = CustomerShippingMethodsMap::find($data['id'])) {
            if ($user->hasCustomer($customerShippingMethodsMap->customer_id) == false) {
                return false;
            }
        }

        if (isset($data['customer_id'])) {
            return $user->hasCustomer($data['customer_id']);
        }
        
        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the webshipper credential.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CustomerShippingMethodsMap $customerShippingMethodsMap
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $customerShippingMethodsMap = CustomerShippingMethodsMap::find($data['id'])) {
            return $user->hasCustomer($customerShippingMethodsMap->customer_id);
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();
        
        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
