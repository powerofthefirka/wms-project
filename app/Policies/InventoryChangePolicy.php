<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Product;
use App\Models\InventoryChange;
use Illuminate\Auth\Access\HandlesAuthorization;

class InventoryChangePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the returns.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\InventoryChange  $returns
     * @return mixed
     */
    public function view(User $user, InventoryChange $inventoryChange)
    {
        return $user->isAdmin() || $inventoryChange->product->customer->hasUser($user->id);
    }

    /**
     * Determine whether the user can create returns.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['product_id']) && $product = Product::find($data['product_id'])) {
            return $user->hasCustomer($product->customer->id);
        }

        return true;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the returns.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['product_id']) && $product = Product::find($data['product_id'])) {
            return $user->hasCustomer($product->customer->id);
        }

        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the returns.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $inventoryChange = InventoryChange::find($data['id'])) {
            return $user->hasCustomer($inventoryChange->product->customer_id);
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
