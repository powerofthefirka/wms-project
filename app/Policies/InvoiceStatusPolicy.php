<?php

namespace App\Policies;

use App\Models\InvoiceStatus;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoiceStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the invoice status.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\InvoiceStatus  $invoiceStatus
     * @return mixed
     */
    public function view(User $user, InvoiceStatus $invoiceStatus)
    {
        return $user->isAdmin() || $invoiceStatus->customer->hasUser($user->id);
    }

    /**
     * Determine whether the user can create invoice statuses.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['customer_id'])) {
            return $user->hasCustomer($data['customer_id']);
        }

        return true;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the invoice status.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $invoiceStatus = InvoiceStatus::find($data['id'])) {
            if ($user->hasCustomer($invoiceStatus->customer_id) == false) {
                return false;
            }
        }

        if (isset($data['customer_id'])) {
            return $user->hasCustomer($data['customer_id']);
        }

        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the invoice status.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['id']) && $invoiceStatus = InvoiceStatus::find($data['id'])) {
            return $user->hasCustomer($invoiceStatus->customer_id);
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }
}
