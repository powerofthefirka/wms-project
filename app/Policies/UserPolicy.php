<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CustomerUser;
use App\Models\CustomerUserRole;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->id == $model->id) {
            return true;
        } else {
            $userCustomerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();

            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $userCustomerIds)->first();
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function create(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['customer_id'])) {
            if ($user->hasCustomer($data['customer_id']) == false) {
                return false;
            }

            return CustomerUser::where('user_id', $user->id)->where('customer_id', $data['customer_id'])->where('role_id', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->first();
        } else {
            return CustomerUser::where('user_id', $user->id)->where('role_id', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->first();
        }

        return true;
    }

    public function batchStore(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->create($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function update(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['email'])) {
            if ($user->email == $data['email'])
                return true;

            $model = User::where('email', $data['email'])->first();

            $customerIds = $user->customers()->wherePivot('role_id', '=', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->pluck('customer_user.customer_id')->unique()->toArray();

            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $customerIds)->first();
        }

        return true;
    }

    public function batchUpdate(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->update( $user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  $data
     * @return mixed
     */
    public function delete(User $user, $data = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        $data = $data ? $data : app('request')->input();

        if (isset($data['email'])) {
            if ($user->email == $data['email']) {
                return false;
            }

            $model = User::where('email', $data['email'])->first();

            $customerIds = $user->customers()->wherePivot('role_id', '=', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->pluck('customer_user.customer_id')->unique()->toArray();

            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $customerIds)->first();
        }

        return true;
    }

    public function batchDelete(User $user)
    {
        $dataArr = app('request')->input();

        foreach ($dataArr as $key => $data) {
            if ($this->delete($user, $data) == false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return $user->isAdmin() && $user->id != $model->id;
    }

    public function customers(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->id == $model->id) {
            return true;
        } else {
            $userCustomerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
            
            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $userCustomerIds)->first();
        }
    }

    public function webhooks(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->id == $model->id) {
            return true;
        } else {
            $userCustomerIds = $user->customers()->pluck('customer_user.customer_id')->unique()->toArray();
            
            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $userCustomerIds)->first();
        }
    }

    public function deleteAccessToken(User $user, User $model)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->id == $model->id) {
            return true;
        } else {
            $customerIds = $user->customers()->wherePivot('role_id', '=', CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR)->pluck('customer_user.customer_id')->unique()->toArray();
            
            return CustomerUser::where('user_id', $model->id)->whereIn('customer_id', $customerIds)->first();
        }
    }
}
