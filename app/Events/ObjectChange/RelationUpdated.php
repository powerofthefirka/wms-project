<?php

namespace App\Events\ObjectChange;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RelationUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public $attached;
    public $detached;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $attached, $detached)
    {
        $this->data = $data;
        $this->attached = $attached;
        $this->detached = $detached;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
