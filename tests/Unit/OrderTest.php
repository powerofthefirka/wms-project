<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\Order\DestroyBatchRequest;
use App\Http\Requests\Order\StoreBatchRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateBatchRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ShipmentResource;
use App\Models\User;
use App\Models\UserRole;
use App\Models\CustomerUserRole;
use App\Models\Customer;
use App\Models\Product;
use App\Models\OrderStatus;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Warehouse;
use App\Models\Location;
use App\Models\LocationProduct;
use App\Models\Shipment;
use App\Models\ContactInformation;
use DB;

class OrderTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testIndex()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $adminUser = $this->createAdministrator();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);

        $orderResource = (new OrderResource($order))->resolve();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.order.index'));

        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [],
                'links' => [],
                'meta' => []
            ]
        );

        foreach ($response->json()['data'] as $res) {
            $this->assertEmpty(array_diff_key($orderResource, $res));
        }
    }

    public function testStore()
    {     
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();
   
        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = [
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2)
        ];

        $request = StoreBatchRequest::make($data);

        $orders = app()->order->storeBatch($request);

        $user = $this->createCustomerUser($customer);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        foreach ($orders as $key => $order) {
            $this->assertInstanceOf(Order::class, $order);

            $this->assertEquals($data[$key]['number'], $order->number);

            foreach ($order->orderItems as $item) {
                $this->assertInstanceOf(OrderItem::class, $item);

                $this->assertEquals($item->order_id, $order->id);
            }

            $this->assertTrue($order->customer->users->contains('id', $user->id));

            $this->assertFalse($order->customer->users->contains('id', $guestUser->id));
        }

        $data = $this->regenerateUniqueNumber($data);

        $this->actingAs($user, 'api')->json('POST', route('api.order.store'), $data)->assertStatus(200);

        $data = $this->regenerateUniqueNumber($data);

        $this->actingAs($guestUser, 'api')->json('POST', route('api.order.store'), $data)->assertStatus(403);
    }

    public function testUpdate()
    {     
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $dataForStore = [
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2)
        ];

        $request = StoreBatchRequest::make($dataForStore);

        $orders = app()->order->storeBatch($request);

        $data = [];

        foreach ($orders as $key => $order) {
            $data[$key] = [
                "customer_id" => $customer->id,
                "number" => $order->number,
                "ordered_at" => date('Y-m-d H:i:s'),
                "required_shipping_date_at" => date('Y-m-d H:i:s'),
                "shipping_date_before_at" => date('Y-m-d H:i:s'),
                "priority" => $this->faker->numberBetween(0, 5),
                "shipping_price" => $this->faker->randomNumber(4),
                "tax" => $this->faker->randomNumber(2),
                "discount" => $this->faker->randomNumber(2),
                "shipping_contact_information" =>
                [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ],
                "billing_contact_information" =>
                [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ]
            ];

            foreach ($order->orderItems as $item) {
                $data[$key]["order_items"][] = [
                    "order_item_id" => $item->id,
                    "product_id" => $item->product_id,
                    "quantity" => $this->faker->randomNumber(1),
                    "quantity_shipped" => 0
                ];
            }
        }

        $request = UpdateBatchRequest::make($data);

        $orders = app()->order->updateBatch($request);

        $user = $this->createCustomerUser($customer);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        foreach ($orders as $key => $order) {
            $this->assertInstanceOf(Order::class, $order);

            $this->assertEquals($data[$key]['number'], $order->number);

            $this->assertEquals($data[$key]['ordered_at'], $order->ordered_at);

            foreach ($order->orderItems as $item) {
                $this->assertInstanceOf(OrderItem::class, $item);

                $this->assertEquals($item->order_id, $order->id);
            }

            $this->assertTrue($order->customer->users->contains('id', $user->id));

            $this->assertFalse($order->customer->users->contains('id', $guestUser->id));

            $this->assertTrue($user->can('update', $order));

            $this->assertFalse($guestUser->can('update', $order));
        }
    }

    public function testDestroy()
    {        
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $dataStore = [
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2)
        ];

        $request = StoreBatchRequest::make($dataStore);

        $orders = app()->order->storeBatch($request);

        $user = $this->createCustomerUser($customer);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $data = [];

        foreach ($orders as $key => $order) {
            $this->assertTrue($user->can('delete', $order));

            $this->assertFalse($guestUser->can('delete', $order));

            $data[$key] = [
                "id" => $order->id,
            ];
        }

        $request = DestroyBatchRequest::make($data);

        $ids = app()->order->destroyBatch($request);

        foreach ($ids as $key => $value) {
            $this->assertSoftDeleted('orders', ['id' => $value['id']]);
        }
    }

    public function testShip()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $user = $this->createCustomerUser($customer);

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);

        $shipment = Shipment::create([
            'order_id' => $order->id
        ]);

        $this->createContactInformation($shipment);

        $shipmentResource = (new ShipmentResource($shipment))->resolve();

        $warehouse = $this->createWarehouse($customer);

        $location1 = $this->createLocation($warehouse);

        $location2 = $this->createLocation($warehouse);

        $this->createLocationProduct($location1, $product1);

        $this->createLocationProduct($location2, $product2);

        $data = [
            "tracking_code" => str_random(12),
            "tracking_url" => $this->faker->url,
            "order_items" => [
                [
                    'order_item_id' => $order->orderItems[0]->id, 
                    'location_id' => $location1->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ],
                [
                    'order_item_id' => $order->orderItems[1]->id, 
                    'location_id' => $location2->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ]
            ],
            "contact_information" => [
                    [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ]
            ]
        ];

        $response = $this->actingAs($user, 'api')->json('POST', route('api.order.ship', $order->id), $data);

        $response->assertStatus(200);

        $this->assertEmpty(array_diff_key($shipmentResource, $response->json()));

        $user = $this->createCustomerUser($customer);

        $this->actingAs($user, 'api')->json('POST', route('api.order.ship', $order->id), $data)->assertStatus(200);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->actingAs($guestUser, 'api')->json('POST', route('api.order.ship', $order->id), $data)->assertStatus(403);
    }

    public function testShipQuantityCalculation()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $user = $this->createCustomerUser($customer);

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);

        $shipment = Shipment::create([
            'order_id' => $order->id
        ]);

        $this->createContactInformation($shipment);

        $warehouse = $this->createWarehouse($customer);

        $location1 = $this->createLocation($warehouse);

        $location2 = $this->createLocation($warehouse);

        $productLocation1 = $this->createLocationProduct($location1, $product1);

        $productLocation2 = $this->createLocationProduct($location2, $product2);

        $data = [
            "tracking_code" => str_random(12),
            "tracking_url" => $this->faker->url,
            "order_items" => [
                [
                    'order_item_id' => $order->orderItems[0]->id, 
                    'location_id' => $location1->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ],
                [
                    'order_item_id' => $order->orderItems[1]->id, 
                    'location_id' => $location2->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ]
            ],
            "contact_information" => [
                    [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ]
            ]
        ];

        $response = $this->actingAs($user, 'api')->json('POST', route('api.order.ship', $order->id), $data);

        $updatedProductLocation1 = LocationProduct::where('location_id', $location1->id)->where('product_id', $product1->id)->first();

        $updatedProductLocation2 = LocationProduct::where('location_id', $location2->id)->where('product_id', $product2->id)->first();

        $this->assertEquals(($productLocation1->quantity_on_hand - $data['order_items'][0]['quantity']), $updatedProductLocation1->quantity_on_hand);

        $this->assertEquals(($productLocation2->quantity_on_hand - $data['order_items'][1]['quantity']), $updatedProductLocation2->quantity_on_hand);

        $updatedProduct1 = Product::find($product1->id);

        $updatedProduct2 = Product::find($product2->id);

        $this->assertEquals(($product1->quantity_on_hand - $data['order_items'][0]['quantity']), $updatedProduct1->quantity_on_hand);

        $this->assertEquals(($product2->quantity_on_hand - $data['order_items'][1]['quantity']), $updatedProduct2->quantity_on_hand);
    }

    public function testHistory()
    {     
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();
        
        $customer = $this->createCustomer();

        $user = $this->createCustomerUser($customer);

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $dataForStore = [ 
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2) 
        ];

        $request = StoreBatchRequest::make($dataForStore);

        $orders = app()->order->storeBatch($request);

        foreach ($orders as $order) {
            $order->ordered_at = $this->faker->dateTimeBetween('now', '+2 days')->format('Y-m-d H:i:s');
            $order->required_shipping_date_at = $this->faker->dateTimeBetween('now', '+2 days')->format('Y-m-d H:i:s');
            $order->shipping_date_before_at = $this->faker->dateTimeBetween('now', '+2 days')->format('Y-m-d H:i:s');
            $order->priority = $this->faker->numberBetween(0, 5);

            $order->save();


            $response = $this->actingAs($user, 'api')->json('GET', route('api.order.history', $order->id));

            foreach ($response->json()['data'] as $res) {
                $this->assertEquals($res['revisionable_type'], Order::class);
                $this->assertEquals($res['revisionable_id'], $order->id);

                $key = $res['key'];

                $order = Order::find($res['revisionable_id']);

                $this->assertNotEquals($res['old_value'], $order->$key);
                $this->assertEquals($res['new_value'], $order->$key);                
            }
        }
    }

    public function testFilter()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $adminUser = $this->createAdministrator();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $dataForStore = [ 
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2) 
        ];

        $request = StoreBatchRequest::make($dataForStore);

        $orders = app()->order->storeBatch($request);

        $user = $this->createCustomerUser($customer);

        foreach ($orders as $order) {            
            foreach ($order->orderItems as $item) {
                $item->quantity = $this->faker->numberBetween(1, 9);
                $item->save();

                $response = $this->actingAs($user, 'api')->json('GET', route('api.order.itemHistory', $item->id));

                foreach ($response->json()['data'] as $res) {
                    $this->assertEquals($res['revisionable_type'], OrderItem::class);
                    $this->assertEquals($res['revisionable_id'], $item->id);

                    $key = $res['key'];

                    $item = OrderItem::find($res['revisionable_id']);

                    $this->assertNotEquals($res['old_value'], $item->$key);
                    $this->assertEquals($res['new_value'], $item->$key);                
                }
            }
        }

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);

        $orderResource = (new OrderResource($order))->resolve();

        $data = [
            'from_date_created' => $this->faker->dateTimeBetween('-15 days', 'now')->format('Y-m-d'),
            'to_date_created' => $this->faker->dateTimeBetween('now', '+15 days')->format('Y-m-d'),
            'from_date_updated' => $this->faker->dateTimeBetween('-15 days', 'now')->format('Y-m-d'),
            'to_date_updated' => $this->faker->dateTimeBetween('now', '+15 days')->format('Y-m-d')
        ];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.order.filter', $data));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [],
            'links' => [],
            'meta' => []
        ]);

        foreach ($response->json()['data'] as $res) {
            $this->assertEmpty(array_diff_key($orderResource, $res));
        }
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    } 
    
    public function createCustomerUserRoles()
    {
        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR,
            'name' => 'Customer Administrator'
        ]);

        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_MEMBER,
            'name' => 'Customer Member'
        ]); 
    }

    public function createCustomer()
    {
        $customer = Customer::create();

        $this->createContactInformation($customer);

        return $customer;
    }

    public function createAdministrator()
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_ADMINISTRATOR]);

        $this->createContactInformation($user);
        
        return $user;
    }

    public function createCustomerUser($customer)
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_DEFAULT]);

        $user->customers()->attach($customer->id, [
            'role_id' => CustomerUserRole::ROLE_DEFAULT
        ]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createProduct($customer)
    {
        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        return $product;
    }

    public function createOrderStatus($customer)
    {
        $orderStatus = factory(OrderStatus::class)->create(['customer_id' => $customer->id]);

        return $orderStatus;
    }

    public function createWarehouse($customer)
    {
        $warehouse = Warehouse::create(['customer_id' => $customer->id]);

        $this->createContactInformation($warehouse);

        return $warehouse;
    }

    public function createLocation($warehouse)
    {
        $location = factory(Location::class)->create(['warehouse_id' => $warehouse->id]);

        return $location;
    }

    public function createLocationProduct($location, $product)
    {
        $locationProduct = LocationProduct::create(['product_id' => $product->id, 'location_id' => $location->id, 'quantity_on_hand' => $this->faker->numberBetween(0, 500)]);

        return $locationProduct;
    }

    private function getOrderRequestData($customer, $orderStatus, $product1, $product2){
        $data = [
            "customer_id" => $customer->id,
            "number" => str_random(12),
            "ordered_at" => date('Y-m-d H:i:s'),
            "required_shipping_date_at" => date('Y-m-d H:i:s'),
            "shipping_date_before_at" => date('Y-m-d H:i:s'),
            "priority" => $this->faker->numberBetween(0, 5),
            "notes" => $this->faker->text,
            "shipping_price" => $this->faker->randomNumber(4),
            "tax" => $this->faker->randomNumber(2),
            "discount" => $this->faker->randomNumber(2),
            "order_items" => [
                [
                    "product_id" => $product1->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ],
                [
                    "product_id" => $product2->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ]
            ],
            "shipping_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ],
            "billing_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ]
        ];

        return $data;
    }

    private function regenerateUniqueNumber($data)
    {
        foreach ($data as $key => $value) {
            $data[$key]['number'] = str_random(12);
        }

        return $data;
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}