<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use App\Http\Resources\ProductResource;
use App\Models\User;
use App\Models\UserRole;
use App\Models\CustomerUserRole;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Warehouse;
use App\Models\Location;
use App\Models\LocationProduct;
use App\Models\ContactInformation;
use DB;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testIndex()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $productResource = (new ProductResource($product))->resolve();

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.product.index'));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [],
            'links' => [],
            'meta' => []
        ]);

        foreach ($response->json()['data'] as $res) {
            $this->assertEmpty(array_diff_key($productResource, $res));
        }
    }

    public function testStore()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $productResource = (new ProductResource($product))->resolve();

        $data = [
            [
                'customer_id' => $customer->id,
                'sku' => str_random(8),
                'name' => $this->faker->word,
                'price' => $this->faker->randomNumber(4),
                'notes' => $this->faker->text,
                'customs_price' => $this->faker->randomNumber(4),
                'weight' => $this->faker->randomNumber(2),
                'width' => $this->faker->randomNumber(2),
                'height' => $this->faker->randomNumber(2),
                'length' => $this->faker->randomNumber(2),
                'country_of_origin' => $this->faker->country,
                'hs_code' => $this->faker->randomNumber(8),
                'image' => UploadedFile::fake()->image('avatar.jpg')
            ],
            [
                'customer_id' => $customer->id,
                'sku' => str_random(8),
                'name' => $this->faker->word,
                'price' => $this->faker->randomNumber(4),
                'notes' => $this->faker->text,
                'customs_price' => $this->faker->randomNumber(4),
                'weight' => $this->faker->randomNumber(2),
                'width' => $this->faker->randomNumber(2),
                'height' => $this->faker->randomNumber(2),
                'length' => $this->faker->randomNumber(2),
                'country_of_origin' => $this->faker->country,
                'hs_code' => $this->faker->randomNumber(8),
            ]
        ];

        $user = $this->createCustomerUser($customer);

        $this->actingAs($user, 'api')->json('POST', route('api.product.store'), $data)->assertStatus(200);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->actingAs($guestUser, 'api')->json('POST', route('api.product.store'), $data)->assertStatus(403);

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('POST', route('api.product.store'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $res) {

            $this->assertEmpty(array_diff_key($productResource, $res));

            $product = Product::where('id', $res['id'])->first();

            $this->assertTrue($product->customer->users->contains('id', $user->id));

            $this->assertFalse($product->customer->users->contains('id', $guestUser->id));
        }
    }

    public function testUpdate()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $this->assertTrue($user->can('update', $product));

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->assertFalse($guestUser->can('update', $product));

        $productResource = (new ProductResource($product))->resolve();

        $data = [
            [
                'customer_id' => $customer->id,
                'sku' => $product->sku,
                'name' => $this->faker->word,
                'price' => $this->faker->randomNumber(3),
                'customs_price' => $this->faker->randomNumber(4),
                'weight' => $this->faker->randomNumber(2),
                'width' => $this->faker->randomNumber(2),
                'height' => $this->faker->randomNumber(2),
                'length' => $this->faker->randomNumber(2),
                'country_of_origin' => $this->faker->country,
                'image' => UploadedFile::fake()->image('avatar.jpg')
            ]
        ];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('PUT', route('api.product.update'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $res) {
            $this->assertEmpty(array_diff_key($productResource, $res));

            $product = Product::where('id', $res['id'])->first();

            $this->assertTrue($product->customer->users->contains('id', $user->id));

            $this->assertFalse($product->customer->users->contains('id', $guestUser->id));
        }
    }

    public function testDestroy()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $this->assertTrue($user->can('delete', $product));

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->assertFalse($guestUser->can('delete', $product));

        $data = [
            ['sku' => $product->sku, 'customer_id' => $customer->id]
        ];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('DELETE', route('api.product.destroy'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $key => $value) {
            $this->assertSoftDeleted('products', ['sku' => $value['sku']]);
        }
    }

    public function testHistory()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $productName = $product->name;

        $product->name = $this->faker->word;
        $product->price = $this->faker->randomNumber(3);
        $product->update();

        $updatedProductName = $product->name;

        $user = $this->createCustomerUser($customer);

        $response = $this->actingAs($user, 'api')->json('GET', route('api.product.history', $product->id));

        $response->assertStatus(200);

        foreach ($response->json()['data'] as $res) {
            $this->assertEquals($res['revisionable_type'], Product::class);
            $this->assertEquals($res['revisionable_id'], $product->id);

            $key = $res['key'];

            $product = Product::find($res['revisionable_id']);

            if ($key == 'price') {
                $this->assertNotEquals(floatval($res['old_value']), floatval($product->$key));
                $this->assertEquals(floatval($res['new_value']), floatval($product->$key));
            } else {
                $this->assertNotEquals($res['old_value'], $product->$key);
                $this->assertEquals($res['new_value'], $product->$key);
            }
        }
    }

    public function testFilter()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        $productResource = (new ProductResource($product))->resolve();

        $warehouse = $this->createWarehouse($customer);

        $location = $this->createLocation($warehouse);

        $locationProduct = $this->createLocationProduct($location, $product);

        $data = [
            'from_date_created' => $this->faker->dateTimeBetween('-15 days', 'now')->format('Y-m-d'),
            'to_date_created' => $this->faker->dateTimeBetween('now', '+15 days')->format('Y-m-d'),
            'from_date_updated' => $this->faker->dateTimeBetween('-15 days', 'now')->format('Y-m-d'),
            'to_date_updated' => $this->faker->dateTimeBetween('now', '+15 days')->format('Y-m-d'),
            'location_id' => $location->id
        ];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.product.filter', $data));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [],
            'links' => [],
            'meta' => []
        ]);

        foreach ($response->json()['data'] as $res) {
            $this->assertEmpty(array_diff_key($productResource, $res));
        }
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    }

    public function createCustomerUserRoles()
    {
        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR,
            'name' => 'Customer Administrator'
        ]);

        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_MEMBER,
            'name' => 'Customer Member'
        ]);
    }

    public function createCustomer()
    {
        $customer = Customer::create();

        $this->createContactInformation($customer);

        return $customer;
    }

    public function createAdministrator()
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_ADMINISTRATOR]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createCustomerUser($customer)
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_DEFAULT]);

        $user->customers()->attach($customer->id, [
            'role_id' => CustomerUserRole::ROLE_DEFAULT
        ]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createWarehouse($customer)
    {
        $warehouse = Warehouse::create(['customer_id' => $customer->id]);

        $this->createContactInformation($warehouse);

        return $warehouse;
    }

    public function createLocation($warehouse)
    {
        $location = factory(Location::class)->create(['warehouse_id' => $warehouse->id]);

        return $location;
    }

    public function createLocationProduct($location, $product)
    {
        $locationProduct = LocationProduct::create(['product_id' => $product->id, 'location_id' => $location->id]);

        return $locationProduct;
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}
