<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Resources\CustomerShippingMethodsMapResource;
use App\Models\User;
use App\Models\UserRole;
use App\Models\CustomerUserRole;
use App\Models\Customer;
use App\Models\CustomerShippingMethodsMap;
use App\Models\ContactInformation;
use DB;

class CustomerShippingMethodsMapTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testIndex()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $map = factory(CustomerShippingMethodsMap::class)->create(['customer_id' => $customer->id]);

        $mapResource = (new CustomerShippingMethodsMapResource($map))->resolve();

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.customer_shipping_methods_map.index'));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [],
            'links' => [],
            'meta' => []
        ]);

        foreach ($response->json()['data'] as $res) {
            $this->assertEmpty(array_diff_key($mapResource, $res));
        }
    }

    public function testStore()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $map = factory(CustomerShippingMethodsMap::class)->create(['customer_id' => $customer->id]);

        $mapResource = (new CustomerShippingMethodsMapResource($map))->resolve();

        $data = [
            [
                'shipping_method_text' => $this->faker->word,
                'customer_id' => $customer->id,
                'shipping_service' => $this->faker->word,
                'shipping_service_carrier_id' => $this->faker->numberBetween(1, 10),
                'shipping_service_method_code' => $this->faker->word
            ]
        ];

        $user = $this->createCustomerUser($customer);

        $this->actingAs($user, 'api')->json('POST', route('api.customer_shipping_methods_map.store'), $data)->assertStatus(200);

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->actingAs($guestUser, 'api')->json('POST', route('api.customer_shipping_methods_map.store'), $data)->assertStatus(403);

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('POST', route('api.customer_shipping_methods_map.store'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $res) {
            $this->assertEmpty(array_diff_key($mapResource, $res));

            $map = CustomerShippingMethodsMap::where('id', $res['id'])->first();

            $this->assertTrue($map->customer->users->contains('id', $user->id));

            $this->assertFalse($map->customer->users->contains('id', $guestUser->id));
        }
    }

    public function testShow()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $map = factory(CustomerShippingMethodsMap::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $this->assertTrue($user->can('view', $map));

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->assertFalse($guestUser->can('view', $map));

        $mapResource = (new CustomerShippingMethodsMapResource($map))->resolve();

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('GET', route('api.customer_shipping_methods_map.show', $map->id));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => []
        ]);

        $this->assertEmpty(array_diff_key($mapResource, $response->json()['data']));
    }

    public function testUpdate()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $map = factory(CustomerShippingMethodsMap::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $this->assertTrue($user->can('update', $map));

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->assertFalse($guestUser->can('update', $map));

        $mapResource = (new CustomerShippingMethodsMapResource($map))->resolve();

        $data = [
            [
                'id' => $map->id,
                'shipping_method_text' => $this->faker->word,
                'customer_id' => $customer->id,
                'shipping_service' => $this->faker->word,
                'shipping_service_carrier_id' => $this->faker->numberBetween(1, 10),
                'shipping_service_method_code' => $this->faker->word
            ],
            [
                'id' => $map->id,
                'shipping_method_text' => $this->faker->word,
                'customer_id' => $customer->id,
                'shipping_service' => $this->faker->word,
                'shipping_service_carrier_id' => $this->faker->numberBetween(1, 10),
                'shipping_service_method_code' => $this->faker->word
            ]
        ];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('PUT', route('api.customer_shipping_methods_map.update'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $res) {
            $this->assertEmpty(array_diff_key($mapResource, $res));

            $map = CustomerShippingMethodsMap::where('id', $res['id'])->first();

            $this->assertTrue($map->customer->users->contains('id', $user->id));

            $this->assertFalse($map->customer->users->contains('id', $guestUser->id));
        }
    }

    public function testDestroy()
    {
        $this->createUserRoles();

        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $map = factory(CustomerShippingMethodsMap::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $this->assertTrue($user->can('delete', $map));

        $guestCustomer = $this->createCustomer();

        $guestUser = $this->createCustomerUser($guestCustomer);

        $this->assertFalse($guestUser->can('delete', $map));

        $data = [['id' => $map->id]];

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('DELETE', route('api.customer_shipping_methods_map.destroy'), $data);

        $response->assertStatus(200);

        foreach ($response->json() as $key => $value) {
            $this->assertSoftDeleted('customer_shipping_methods_maps', ['id' => $value['id']]);
        }
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    } 

    public function createCustomerUserRoles()
    {
        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR,
            'name' => 'Customer Administrator'
        ]);

        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_MEMBER,
            'name' => 'Customer Member'
        ]); 
    } 

    public function createCustomer()
    {
        $customer = Customer::create();

        $this->createContactInformation($customer);

        return $customer;
    }

    public function createAdministrator()
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_ADMINISTRATOR]);

        $this->createContactInformation($user);
        
        return $user;
    }

    public function createCustomerUser($customer)
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_DEFAULT]);

        $user->customers()->attach($customer->id, [
            'role_id' => CustomerUserRole::ROLE_DEFAULT
        ]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}