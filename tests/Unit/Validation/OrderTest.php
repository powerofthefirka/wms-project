<?php

namespace Tests\Unit\Validation;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\Order\StoreBatchRequest;
use App\Http\Requests\Order\StoreRequest;
use App\Models\User;
use App\Models\UserRole;
use App\Models\CustomerUserRole;
use App\Models\Customer;
use App\Models\Product;
use App\Models\OrderStatus;
use App\Models\Warehouse;
use App\Models\Location;
use App\Models\LocationProduct;
use App\Models\Shipment;
use App\Models\ContactInformation;
use DB;

class OrderTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

	public function testStore()
    {        
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $adminUser = $this->createAdministrator();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = [
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2)
        ];

        $response = $this->actingAs($adminUser, 'api')->json('POST', route('api.order.store'), $data);

        $response->assertJsonMissingValidationErrors();
    }

    public function testUpdate()
    {     
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $dataForStore = [
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2),
            $this->getOrderRequestData($customer, $orderStatus, $product1, $product2)
        ];

        $request = StoreBatchRequest::make($dataForStore);

        $orders = app()->order->storeBatch($request);

        $data = [];

        foreach ($orders as $key => $order) {
            $data[$key] = [
                "customer_id" => $customer->id,
                "number" => $order->number,
                "ordered_at" => date('Y-m-d H:i:s'),
                "required_shipping_date_at" => date('Y-m-d H:i:s'),
                "shipping_date_before_at" => date('Y-m-d H:i:s'),
                "priority" => $this->faker->numberBetween(0, 5),
                "shipping_price" => $this->faker->randomNumber(4),
                "tax" => $this->faker->randomNumber(2),
                "discount" => $this->faker->randomNumber(2),
                "shipping_contact_information" =>
                [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ],
                "billing_contact_information" =>
                [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ]
            ];

            foreach ($order->orderItems as $item) {
                $data[$key]["order_items"][] = [
                    "order_item_id" => $item->id,
                    "product_id" => $item->product_id,
                    "quantity" => $this->faker->randomNumber(1),
                    "quantity_shipped" => 0
                ];
            }
        }

        $adminUser = $this->createAdministrator();

        $response = $this->actingAs($adminUser, 'api')->json('PUT', route('api.order.update'), $data);

        $response->assertJsonMissingValidationErrors();
    }

    public function testShip()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $adminUser = $this->createAdministrator();

        $customer = $this->createCustomer();

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);

        $shipment = Shipment::create([
            'order_id' => $order->id
        ]);

        $this->createContactInformation($shipment);

        $warehouse = $this->createWarehouse($customer);

        $location1 = $this->createLocation($warehouse);

        $location2 = $this->createLocation($warehouse);

        $this->createLocationProduct($location1, $product1);

        $this->createLocationProduct($location2, $product2);

        $data = [
            "tracking_code" => str_random(12),
            "tracking_url" => $this->faker->url,
            "order_items" => [
                [
                    'order_item_id' => $order->orderItems[0]->id, 
                    'location_id' => $location1->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ],
                [
                    'order_item_id' => $order->orderItems[1]->id, 
                    'location_id' => $location2->id, 
                    'quantity' => $this->faker->numberBetween(1, 9),
                ]
            ],
            "contact_information" => [
                    [
                    'name' => $this->faker->name,
                    'address' => $this->faker->address,
                    'zip' => $this->faker->postcode,
                    'city' => $this->faker->city,
                    'email' => $this->faker->unique()->safeEmail,
                    'phone' => $this->faker->phoneNumber
                ]
            ]
        ];

        $response = $this->actingAs($adminUser, 'api')->json('POST', route('api.order.ship', $order->id), $data);

        $response->assertJsonMissingValidationErrors();
    }

    private function getOrderRequestData($customer, $orderStatus, $product1, $product2){
        $data = [
            "customer_id" => $customer->id,
            "number" => str_random(12),
            "ordered_at" => date('Y-m-d H:i:s'),
            "required_shipping_date_at" => date('Y-m-d H:i:s'),
            "shipping_date_before_at" => date('Y-m-d H:i:s'),
            "priority" => $this->faker->numberBetween(0, 5),
            "notes" => $this->faker->text,
            "shipping_price" => $this->faker->randomNumber(4),
            "tax" => $this->faker->randomNumber(2),
            "discount" => $this->faker->randomNumber(2),
            "order_items" => [
                [
                    "product_id" => $product1->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ],
                [
                    "product_id" => $product2->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ]
            ],
            "shipping_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ],
            "billing_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ]
        ];

        return $data;
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    } 

    public function createCustomerUserRoles()
    {
        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR,
            'name' => 'Customer Administrator'
        ]);

        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_MEMBER,
            'name' => 'Customer Member'
        ]); 
    }

    public function createCustomer()
    {
        $customer = Customer::create();

        $this->createContactInformation($customer);

        return $customer;
    }

    public function createAdministrator()
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_ADMINISTRATOR]);

        $this->createContactInformation($user);
        
        return $user;
    }

    private function createProduct($customer)
    {
        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        return $product;
    }

    private function createOrderStatus($customer)
    {
        $orderStatus = factory(OrderStatus::class)->create(['customer_id' => $customer->id]);

        return $orderStatus;
    }

    public function createWarehouse($customer)
    {
        $warehouse = Warehouse::create(['customer_id' => $customer->id]);

        $this->createContactInformation($warehouse);

        return $warehouse;
    }

    public function createLocation($warehouse)
    {
        $location = factory(Location::class)->create(['warehouse_id' => $warehouse->id]);

        return $location;
    }

    public function createLocationProduct($location, $product)
    {
        LocationProduct::create(['product_id' => $product->id, 'location_id' => $location->id]);
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}