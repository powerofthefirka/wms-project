<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use LocationProductOrderTablesSeeder;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Customer;
use App\Models\PickingBatch;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ContactInformation;
use DB;

class RouteOptimizationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testOptimizedRoute()
    {
        $this->seed(LocationProductOrderTablesSeeder::class);

        $quantity = $this->faker->numberBetween(1, 5);

        $customer = Customer::first();

        $orders = Order::where('customer_id', $customer->id)->get();

        $this->createUserRoles();

        $adminUser = $this->createAdministrator();

        $data = [
            'quantity' => $quantity,
            'customer_id' => $customer->id
        ];

        $response = $this->actingAs($adminUser, 'api')->json('POST', route('api.picking_batches'), $data);

        $pickingBatch = PickingBatch::find($response->json()['id']);
        $pickingOrderItemIds = $pickingBatch->pickingBatchItems->pluck('order_item_id')->toArray();

        $selectedOrders = app()->routeOptimizer->getSelectedOrders($quantity, $orders);

        $items = app()->routeOptimizer->getItemsOfSelectedOrders($selectedOrders);

        $startLocation = app()->routeOptimizer->getStartLocation();

        $shortestPaths = app()->routeOptimizer->getAllShortestPaths($startLocation, $items);

        $reformedPaths = app()->routeOptimizer->reformPaths($selectedOrders, $shortestPaths);

        foreach ($reformedPaths['paths'] as $shortestPath) {
            foreach ($shortestPath['orderIds'] as $orderId) {
                $orderItem = OrderItem::where('order_id', $orderId)->where('product_id', $shortestPath['productId'])->first();

                $this->assertTrue(in_array($orderItem->id, $pickingOrderItemIds));
            }
        }
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    }

    public function createAdministrator()
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_ADMINISTRATOR]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }
}
