<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\Order\StoreRequest;
use App\Models\User;
use App\Models\UserRole;
use App\Models\CustomerUserRole;
use App\Models\Customer;
use App\Models\Product;
use App\Models\OrderStatus;
use App\Models\Shipment;
use App\Models\ShipmentItem;
use App\Models\Warehouse;
use App\Models\Location;
use App\Models\WebshipperCredential;
use App\Models\ContactInformation;
use DB;

class WebshipperApiEndPointTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testCarriers()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        factory(WebshipperCredential::class)->create(['customer_id' => $customer->id]);

        $user = $this->createCustomerUser($customer);

        $response = $this->actingAs($user, 'api')->json('GET', route('api.webshipper.carriers', $customer->id));

        $response->assertStatus(200);

        foreach ($response->json() as $res) {
            $this->assertArrayHasKey('id', $res);
            $this->assertArrayHasKey('alias', $res);
            $this->assertArrayHasKey('services', $res);
            $this->assertIsArray($res['services']);
        }
    }

    public function testShipment()
    {
        $this->createUserRoles();
        
        $this->createCustomerUserRoles();

        $customer = $this->createCustomer();

        $user = $this->createCustomerUser($customer);

        $product1 = $this->createProduct($customer);

        $product2 = $this->createProduct($customer);

        $orderStatus = $this->createOrderStatus($customer);

        $data = $this->getOrderRequestData($customer, $orderStatus, $product1, $product2);

        $request = StoreRequest::make($data);

        $order = app()->order->store($request);
        
        $shipment = Shipment::create([
            'order_id' => $order->id
        ]);

        $this->createContactInformation($shipment);

        $warehouse = $this->createWarehouse($customer);

        foreach ($order->orderItems as $orderItem) {
            $location = $this->createLocation($warehouse);
            $quantity = $this->faker->numberBetween(1, 9);

            $shipmentItem = ShipmentItem::create([
                'shipment_id' => $shipment->id,
                'product_id' => $orderItem->product_id,
                'quantity' => $quantity
            ]);

            app()->inventoryLog->updateLocationProduct($user->id, $orderItem->product, $quantity, $location, $shipment, 'ship');

            $orderItem->quantity_shipped += $quantity;
            $orderItem->save();
        }

        factory(WebshipperCredential::class)->create(['customer_id' => $customer->id]);

        $carriers = app()->webshipperShipping->carriers($customer);

        if (count($carriers) == 0) {
            return true;
        }

        $randIndex = array_rand($carriers);
        $carrier = $carriers[$randIndex];

        $carrierId = $carrier['id'];

        $services = $carrier['attributes']['services'];
        $serviceCode = null;

        if (count($services)) {
            $randServiceIndex = array_rand($services);
            $service = $services[$randServiceIndex];
            $serviceCode = $service['service_code'];
        }

        $webshipperShipment = app()->webshipperShipping->processShipment($shipment, $carrierId, $serviceCode);

        $this->assertEquals($webshipperShipment['type'], 'shipments');
        $this->assertEquals($webshipperShipment['attributes']['reference'], $shipment->order->number);

        foreach ($webshipperShipment['attributes']['packages'][0]['customs_lines'] as $key => $item) {
            $this->assertEquals($item['sku'], $shipment->shipmentItems[$key]->product->sku);
            $this->assertEquals($item['description'], $shipment->shipmentItems[$key]->product->name);
            $this->assertEquals($item['unit_price'], $shipment->shipmentItems[$key]->product->price);
            $this->assertEquals($item['quantity'], $shipment->shipmentItems[$key]->quantity);
        }

        $this->assertEquals($webshipperShipment['attributes']['delivery_address']['att_contact'], $shipment->contactInformation->name);
        $this->assertEquals($webshipperShipment['attributes']['delivery_address']['company_name'], $shipment->contactInformation->company_name);
        $this->assertEquals($webshipperShipment['attributes']['delivery_address']['zip'], $shipment->contactInformation->zip);
        $this->assertEquals($webshipperShipment['attributes']['delivery_address']['email'], $shipment->contactInformation->email);
        $this->assertEquals($webshipperShipment['attributes']['delivery_address']['city'], $shipment->contactInformation->city);

        $this->assertEquals($webshipperShipment['attributes']['sender_address']['att_contact'], $customer->contactInformation->name);
        $this->assertEquals($webshipperShipment['attributes']['sender_address']['company_name'], $customer->contactInformation->company_name);
        $this->assertEquals($webshipperShipment['attributes']['sender_address']['zip'], $customer->contactInformation->zip);
        $this->assertEquals($webshipperShipment['attributes']['sender_address']['email'], $customer->contactInformation->email);
        $this->assertEquals($webshipperShipment['attributes']['sender_address']['city'], $customer->contactInformation->city);
        
    }

    public function createUserRoles()
    {
        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_ADMINISTRATOR,
            'name' => 'Administrator'
        ]);

        DB::table('user_roles')->insert([
            'id' => UserRole::ROLE_DEFAULT,
            'name' => 'Member'
        ]);
    } 

    public function createCustomerUserRoles()
    {
        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_ADMINISTRATOR,
            'name' => 'Customer Administrator'
        ]);

        DB::table('customer_user_roles')->insert([
            'id' => CustomerUserRole::ROLE_CUSTOMER_MEMBER,
            'name' => 'Customer Member'
        ]); 
    } 

    public function createCustomer()
    {
        $customer = Customer::create();

        $this->createContactInformation($customer);

        return $customer;
    }

    public function createCustomerUser($customer)
    {
        $user = factory(User::class)->create(['user_role_id' => UserRole::ROLE_DEFAULT]);

        $user->customers()->attach($customer->id, [
            'role_id' => CustomerUserRole::ROLE_DEFAULT
        ]);

        $this->createContactInformation($user);

        return $user;
    }

    public function createProduct($customer)
    {
        $product = factory(Product::class)->create(['customer_id' => $customer->id]);

        return $product;
    }

    public function createWarehouse($customer)
    {
        $warehouse = Warehouse::create(['customer_id' => $customer->id]);

        $this->createContactInformation($warehouse);

        return $warehouse;
    }

    public function createLocation($warehouse)
    {
        $location = factory(Location::class)->create(['warehouse_id' => $warehouse->id]);

        return $location;
    }

    public function createOrderStatus($customer)
    {
        $orderStatus = factory(OrderStatus::class)->create(['customer_id' => $customer->id]);

        return $orderStatus;
    }

    public function createContactInformation($object)
    {
        factory(ContactInformation::class)->create([
            'object_type' => get_class($object),
            'object_id' => $object->id
        ]);
    }

    private function getOrderRequestData($customer, $orderStatus, $product1, $product2){
        $data = [
            "customer_id" => $customer->id,
            "number" => str_random(12),
            "ordered_at" => date('Y-m-d H:i:s'),
            "required_shipping_date_at" => date('Y-m-d H:i:s'),
            "shipping_date_before_at" => date('Y-m-d H:i:s'),
            "priority" => $this->faker->numberBetween(0, 5),
            "notes" => $this->faker->text,
            "shipping_price" => $this->faker->randomNumber(4),
            "tax" => $this->faker->randomNumber(2),
            "discount" => $this->faker->randomNumber(2),
            "order_items" => [
                [
                    "product_id" => $product1->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ],
                [
                    "product_id" => $product2->id,
                    "quantity" => $this->faker->numberBetween(1, 9),
                    "quantity_shipped" => 0
                ]
            ],
            "shipping_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ],
            "billing_contact_information" =>
            [
                'name' => $this->faker->name,
                'address' => $this->faker->address,
                'zip' => $this->faker->postcode,
                'city' => $this->faker->city,
                'email' => $this->faker->unique()->safeEmail,
                'phone' => $this->faker->phoneNumber
            ]
        ];

        return $data;
    }
}